<?php
	session_start();

	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");

	$idFormulario=bD($_GET["iF"]);

	$idRegistro=bD($_GET["iR"]);


	$fechaActual=date("Y-m-d")	;

	$horaActual=date("H:i");

	$idActividad=-1;
	
	if($idRegistro==-1)
		$idActividad=generarIDActividad($idFormulario);



?>

var idActividad=<?php echo $idActividad?>;

function inyeccionCodigo()
{
	gE('sp_11305').innerHTML='';
    if(esRegistroFormulario())
    {
    
    	asignarEvento(gE('_tipoProcesovch'),'change',function(cmb)
        							{
                                    	var opcion=cmb.options[cmb.selectedIndex].value;
                                        if(opcion=='5')
                                        {
                                        	oE('div_11057');
                                            oE('div_11086');
                                            oE('div_11305');
                                            gE('_cuantiaProcesoflo').setAttribute('val','');
                                        }
                                        else
                                        {
                                        	mE('div_11057');
                                            mE('div_11086');
                                            mE('div_11305');
                                            gE('_cuantiaProcesoflo').setAttribute('val','obl|flo');
                                        }
                                    }
        			);
    
    	asignarEvento(gE('_cuantiaProcesoflo'),'blur',function()
        							{
                                    	convertirCuantiaLetra();
                                    }
        			);                     
                        
        if(gE('idRegistroG').value=='-1')
        {
            if(gEx('f_sp_fechaRecepcionDemandadte')) f_sp_fechadeRecepciondte
            {
             	gEx('f_sp_fechadeRecepciondte').setValue('<?php echo $fechaActual?>');
             	gEx('f_sp_fechadeRecepciondte').fireEvent('change', gEx('f_sp_fechadeRecepciondte'), gEx('f_sp_fechadeRecepciondte').getValue());

             	gEx('f_sp_fechadeRecepciondte').fireEvent('select', gEx('f_sp_fechadeRecepciondte'));
             }

             if(gEx('f_sp_horadeRecepciontme'))
             {
	             gEx('f_sp_horadeRecepciontme').setValue('<?php echo $horaActual?>');

             	gEx('f_sp_horadeRecepciontme').fireEvent('change', gEx('f_sp_horadeRecepciontme'), gEx('f_sp_horadeRecepciontme').getValue());
             }   
             
             
                  	
        }
        else
        {
        	convertirCuantiaLetra();
        }
        
        if(gE('idRegistroG').value=='-1')
            gEN('_idActividadvch')[0].value=idActividad;
        else
            idActividad=gEN('_idActividadvch')[0].value;
        
    }
    else
    {
    	if(gE('sp_11080').innerHTML=='Fuero Sindical')
        {
        	oE('div_11057');
            oE('div_11086');
            oE('div_11305');
        }
    	convertirCuantiaLetra();	
    }
}



function convertirCuantiaLetra()
{
	var valor=(gE('_cuantiaProcesoflo') && gE('_cuantiaProcesoflo').value)?gE('_cuantiaProcesoflo').value:gE('_cuantiaProcesoflo').innerHTML;
	var arMonto=valor.split('.');
	var parteDecimal=0;
    
    
    if(arMonto.length>1)
    {
    	parteDecimal=parseInt(arMonto[1]);
    }
    
    if(parteDecimal<10)
    {
    	parteDecimal='0'+parteDecimal;
    }
    
	gE('sp_11305').innerHTML='('+covertirNumLetras(parseFloat(normalizarValor(valor)))+' '+parteDecimal+'/100 PESOS)';
}