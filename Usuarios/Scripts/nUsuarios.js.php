<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(Inicializar);

function Inicializar()
{
	generaLogin();
    new Ext.Button (
                            {
                                icon:'../images/icon_big_tick.gif',
                                cls:'x-btn-text-icon',
                                text:'Guardar',
                                width:110,
                                height:30,
                                renderTo:'contenedor1',
                                handler:function()
                                        {
                                           guardarUsuario('nUsuario');
                                        }
                                
                            }
                        )
	if(gE('vCU').value=='0')
		crearOrganigrama();
}

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	var btnGuardar=gE('btnGuardar');
	btnGuardar.click();
	
}

function guardarUsuario(form)
{
	var pwd1=gE('Contrasena');
	var pwd2=gE('Contrasena2');
	if(pwd1.value!=pwd2.value)
	{
		keyMap.disable();
		function ponerFoco()
		{
			pwd1.focus();
			keyMap.enable();
		}
		Ext.MessageBox.alert(lblAplicacion,'Las contrase&ntilde;as introducidas no concuerdan, por favor verifique..',ponerFoco);
		return;
	}
	if(gE('vCU').value=='0')
    {
        if (!validaRoles())
        {
            keyMap.disable();
            function resp()
            {
                keyMap.enable();
            }
            Ext.MessageBox.alert(lblAplicacion,'Por lo menos debe seleccionar un rol para el Usuario',resp);
            return;
        }
	}
    	
	if (!validarFormularios(form))
	{
		return;
	}
	else
	{
		var formulario=gE(form);
        if(gE('vCU').value=='0')
	        prepararComboRoles();
		formulario.submit();
	}
}


function prepararComboRoles()
{
	var comboRoles=gE('listRoles');
    var hRoles=gE('listadoRoles');
    var x;
    var roles='';
    
    for(x=0;x<comboRoles.options.length;x++)
    {
    	if(roles=='')
        	roles=comboRoles.options[x].value;
        else
    		roles+=','+comboRoles.options[x].value;
    }
    hRoles.value=roles;
    
}

function validaRoles()
{
	var combo=gE('listRoles');
    if(combo.options.length==0)
    	return false;
    else
    	return true;
}

function generaLogin()
{
	var cmbLogin=gE('cmbLogin');
    if(cmbLogin)
    {
        var idUsr=gE('idUsuario');
        limpiarCombo(cmbLogin);
        function funcTratarRespuesta()
        {
            var resp=peticion_http.responseText;
            var arrOpc=resp.split('|');
            var x;
            var opcion;
            for(x=0;x<arrOpc.length-1;x++)
            {
                opcion=document.createElement('option');
                opcion.value=arrOpc[x];
                opcion.text=arrOpc[x];
                cmbLogin.options[cmbLogin.length]=opcion;
            }
            selElemCombo(cmbLogin,gE('lblLogin').value);
        }
        obtenerDatosWeb('intermediaProcesar.php',funcTratarRespuesta, 'POST','banderaGuardar=generaLogin&idUsuario='+idUsr.value);		
	}
}




function regresar(usr)
{
	var arrParam=[['idUsuario',usr]];
	enviarFormularioDatos('intermediaMostrar.php',arrParam);	
}

function removerRol()
{
	var cmbRoles=gE('listRoles');
	var rol=cmbRoles.selectedIndex;
	if(rol==-1)
	{
		Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe elegir el rol a remover');
        return;
	}
    
    function resp(btn)
    {
    	if(btn=='yes')
        	cmbRoles[cmbRoles.selectedIndex]=null;
    }
    Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer remover este rol',resp);
    
    
}

function agregarRol()
{

	<?php
		$consulta="select concat(idRol,'_',extensionRol),nombreGrupo from 8001_roles where  idIdioma=".$_SESSION["leng"]." and vistosAdmin=1 and situacion=1 order by nombreGrupo";
		if(existeRol("'1_0'"))
			$consulta="select concat(idRol,'_',extensionRol),nombreGrupo from 8001_roles where  idIdioma=".$_SESSION["leng"]."  and situacion=1  order by nombreGrupo";
		$arrRoles=uEJ($con->obtenerFilasArreglo($consulta));
	?>
    var arrRoles=<?php echo $arrRoles;?>;
    var cmbExtensiones=crearComboExt('cmbExtensiones',[],100,35,250);
	cmbExtensiones.hide();
	var cmbRoles=crearComboExt('cmbRoles',arrRoles,100,5,250);
    function rolSeleccionado(combo,registro,indice)
    {
    	cmbExtensiones.reset();
    	var idRegistro=registro.get('id');
        var arrId=idRegistro.split('_');
        if(arrId[1]!=0)
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
					var arrExtensiones=eval(arrResp[1]);
                    cmbExtensiones.getStore().loadData(arrExtensiones);     
                    
                	cmbExtensiones.show();
		            Ext.getCmp('lblExtension').show();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=20&noTodos=true&extension='+arrId[1],true);
        
        	
        }
        else
        {
        	cmbExtensiones.hide();
            Ext.getCmp('lblExtension').hide();
        }
        
    }
    
    cmbRoles.on('select',rolSeleccionado);
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
													 	{
                                                        	id:'lblRol',
                                                            x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Rol:'
                                                        },
                                                        cmbRoles,
                                                        {
                                                        	id:'lblExtension',
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'Extensi&oacute;n:',
                                                            hidden:true
                                                        },
                                                        cmbExtensiones
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar rol',
										width:380,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var rol=cmbRoles.getValue();
                                                                var arrId=rol.split('_');
                                                                var extension='0';
                                                                if(arrId[1]!=0)
                                                                	extension=cmbExtensiones.getValue();	
                                                                if(extension=='')
                                                                {
                                                                	function resp()
                                                                    {
                                                                    	cmbExtensiones.focus();
                                                                    }
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe seleccionar una extensi&oacute;n del rol',resp);
                                                                    return;
                                                                }
                                                                var listRoles=gE('listRoles');
                                                                var codigoRol=arrId[0]+'_'+extension;
                                                                var rolExiste=existeRol('listRoles',codigoRol);
                                                                
                                                                if(!rolExiste)
                                                                {
                                                                	
                                                                	var option=document.createElement('option');
                                                                    option.value=codigoRol;
                                                                    var nExtension=cmbExtensiones.getValue();
                                                                    var txtExtension='';
                                                                    if(nExtension!='')
                                                                    {
                                                                    	txtExtension=' ('+cmbExtensiones.getRawValue()+')';
                                                                    }
                                                                    option.text=cmbRoles.getRawValue()+txtExtension;
                                                                    listRoles.options[listRoles.options.length]=option;
                                                                }
                                                                else
                                                                {
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','<?php echo $etj["msgRolYaExisteUsr"]?>')
                                                                    return;
                                                                }
                                                                
                                                                ventana.close();
																
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();

}

function existeRol(idCombo,valor)
{
	var combo=gE(idCombo);
    var x;
    for(x=0;x<combo.options.length;x++)
    {
    	if(combo.options[x].value==valor)
        	return true;
    }
    return false;

}


var nodoSel=null;
var nodoResponsable=null;

function crearOrganigrama()
{
		var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
		var cargadorArbol=new Ext.tree.TreeLoader(
                                                        {
                                                            baseParams:{
                                                                            funcion:'70',
                                                                            organigramaInst:'1',
                                                                            idUsuario:gE('idUsuario').value
                                                                            
                                                                        },
                                                            dataUrl:'../paginasFunciones/funcionesOrganigrama.php',
                                                            uiProviders:	{
                                                                            	'col': Ext.ux.tree.ColumnNodeUI
                                                                        	}
                                                        }	


		                                         )		                                        
		
                                        
		var organigrama = new Ext.ux.tree.TreeGrid	(
                                                            {
                                                                id:'tOrganigrama',
                                                                height:500,
                                                                width:960,
                                                                useArrows:true,
                                                                autoScroll:true,
                                                                animate:true,
                                                                enableDD:true,
                                                                containerScroll: true,
                                                                root:raiz,
                                                                enableSort:false,
                                                                loader: cargadorArbol,
                                                                rootVisible:false,
                                                                
                                                                draggable:false,
                                                                columns:[
                                                                			
                                                                            {
                                                                                header:'Unidades Organigrama',
                                                                                width:580,
                                                                                dataIndex:'text'
                                                                            },
                                                                            {
                                                                                header:'C&oacute;digo',
                                                                                width:110,
                                                                                dataIndex:'codDepto'
                                                                            },
                                                                            {
                                                                                header:'Cve. Departamental',
                                                                                width:110,
                                                                                dataIndex:'cveDeptal'
                                                                            },
                                                                            {
                                                                                header:'Situaci&oacute;n',
                                                                                width:80,
                                                                                dataIndex:'activo'
                                                                            }
                                                                         ],
                                                                 listeners: 	{
                                                                                    'render': 	function(tp)
                                                                                    			{
                                                                                        			
                                                                                                 }
                                                                                    }

                                                               
                                                            }
                                                    );
		
        
       

        
        var panel=new Ext.Panel	(
        							{
                                    	id:'divPanel',
                                        border:false,
                                        renderTo:'tblOrganigrama',
                                        items:	[
                                                    organigrama
                                        		]
                                        
                                    }
        						)
        organigrama.on('checkchange',nodoClick);
        organigrama.expandAll();       
}

function nodoClick(nodo,status)
{
	if((status)&&(nodoSel!=nodo))
    {
    	
		nodoSel=nodo;
        gE('adscripcion').value=nodoSel.attributes.codigoU;

        checarNodosHijos(gEx('tOrganigrama').getRootNode(),false);
        nodoSel.getUI().toggleCheck(true);
        nodoSel=null;
        
	}
    
    
	
}


function radioCambioPassChecked(rdo)
{
	var arrRadio=rdo.id.split('_');
    gE('cambiarDatosUsr').value=arrRadio[1];
}


function radioCuentaActivaChecked(rdo)
{
	var arrRadio=rdo.id.split('_');
    gE('cuentaActiva').value=arrRadio[1];
}