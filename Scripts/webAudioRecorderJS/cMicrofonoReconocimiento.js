Ext.onReady(inicializar);
function inicializar()
{
	
	if(!window.WebAudioRecorder)
	{
		
	}
}

class cMicrofonoReconocimiento
{
	constructor(config)
	{
		this.audioRecorder=null;
		this.ventanaGrabado=null;
		this.inicializado=false;
		this.funcionTraslated=config && config.funcionTraslated?config.funcionTraslated:null;
		
		
		loadScript('../Scripts/webAudioRecorderJS/WebAudioRecorder.js', function()
																		{
																			//this.inicializado=true;
																		}
											);
	}
	
	
	startRecord()
	{
		if (('mediaDevices' in navigator) && ('getUserMedia' in navigator.mediaDevices)) 
		{
			var options = { 'audio': true, 'video': false };  
			navigator.mediaDevices.getUserMedia(options).then(stream => 
																		   {
																		
																				  var getUserMediaStream = stream;
																				  var AudioContext = window.AudioContext ||  window.webkitAudioContext;
																				  var audioContext = new AudioContext({
																														  sampleRate: 16000
																														});
																				  var source = audioContext.createMediaStreamSource(stream);
																				
																				
																				  this.audioRecorder = new WebAudioRecorder(source, {
																																		  workerDir: "../Scripts/webAudioRecorderJS/",
																																		  numChannels:1,
																																		  encoding:'wav',
																																		  options:	{
																																						encodeAfterRecord: true   
																																					}
																																		}
																														);
																													
																													
																				this.audioRecorder.onComplete = (webAudioRecorder, blob) => 
																											{
																												var objPrincipal=this;
																												mostrarMensajeProcesando('Procesando entrada!!!');
																												var formData = new FormData();
																												formData.append('archivoEnvio', blob,'audio.wav');																																			
																												$.ajax('../paginasFunciones/procesarAudioMicrofono.php', {
																																												method: "POST",
																																												data: formData,
																																												processData: false,
																																												contentType: false,
																																												success: function (response) 
																																															{

																																																var arrResp=response.split('|');
																																																var resultado=arrResp[1];
																																																var objResultado=eval('['+resultado+']')[0];
																																																if(objPrincipal.funcionTraslated)
																																																	objPrincipal.funcionTraslated(objResultado);
																																																ocultarMensajeProcesando();
																																															
																																															},
																																												error: function () 
																																														{
																																														}
																																											  }
																																										   );
																												
																												
																												
																											}
																				this.audioRecorder.onError = (webAudioRecorder, err) => 
																										{
																											console.error(err);
																										}                                                            
																			
																			  
																				this.audioRecorder.startRecording();
																				
																				this.mostrarVentanaGrabando(this);
																		   }
																) 
		}
	}
							
	finishRecord()
	{	
		this.audioRecorder.finishRecording();
		this.ocultarVentanaGrabando();
	}
													
							
	cancelRecording()
	{
		this.audioRecorder.cancelRecording();
		this.ocultarVentanaGrabando();
	}

	
	mostrarVentanaGrabando(objActual)
	{
		var form = new Ext.form.FormPanel(	
											{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'label',
												items: 	[
															
														]
											}
										);
		
		this.ventanaGrabado = new Ext.Window(
												{
													title: 'Escuchando...',
													width: 500,
													height:450,
													layout: 'fit',
													plain:true,
													modal:true,
													bodyStyle:'padding:5px;',
													buttonAlign:'center',
													items: form,
													listeners : {
																show : {
																			buffer : 10,
																			fn : function() 
																			{
																			}
																		}
															},
													buttons:	[
																	{
																		text: 'Aceptar',
																		handler:function()
																				{
																					objActual.finishRecord();
																					
																				}
																	},
																	{
																		text: 'Cancelar',
																		handler:function()
																				{
																					objActual.cancelRecording();
																					
																				}
																	}
																]
												}
											);
		this.ventanaGrabado.show();	
	}
	
	
	ocultarVentanaGrabando()
	{
		this.ventanaGrabado.close();
	}
	
}





		


