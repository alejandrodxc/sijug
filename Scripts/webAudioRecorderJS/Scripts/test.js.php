
var audioRecorder;

window.onload = function() 
                {
                	 let options = { 'audio': true, 'video': false };
                      
                   
                        navigator.mediaDevices.getUserMedia(options).then(stream => 
                       {
                    
                              getUserMediaStream = stream;
                              let AudioContext = window.AudioContext ||  window.webkitAudioContext;
                              let audioContext = new AudioContext({
                                                                      sampleRate: 16000
                                                                    });
                              let source = audioContext.createMediaStreamSource(stream);
                            
                            
                              audioRecorder = new WebAudioRecorder(source, {
                                                                                  workerDir: "lib/",
                                                                                  numChannels:1,
                                                                                  encoding:'wav',
                                                                                  options:	{
                                                                                                encodeAfterRecord: true   
                                                                                            }
                                                                                }
                                                                );
                                                                
                                                                
                            audioRecorder.onComplete = (webAudioRecorder, blob) => 
                                                        {
                                                            let audioElementSource = window.URL.createObjectURL(blob);
                                                            var a = document.createElement("a");
                                                            a.href = audioElementSource;
                                                            a.download = 'audio.wav';
                                                            a.click();
                                                            /*let audioElement=document.getElementById('audio');
                                                            audioElement.src = audioElementSource;
                                                            audioElement.controls = true;*/
                                                        }
                            audioRecorder.onError = (webAudioRecorder, err) => 
                                                    {
                                                        console.error(err);
                                                    }                                                            
                        
                          
                       }
                       )
                       
                          
                
                      
                };
                
function recordClick()
{
	var btnIniciar=document.getElementById('btnIniciar');
    if(btnIniciar.innerHTML=='Inicializar')
    {
    	btnIniciar.innerHTML='Finalizar';
        audioRecorder.startRecording();

    }
    else
    {
    	btnIniciar.innerHTML='Inicializar';
         audioRecorder.finishRecording();

    }
}                