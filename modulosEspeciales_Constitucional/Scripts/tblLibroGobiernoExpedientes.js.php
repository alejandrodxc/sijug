<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	

	$consulta="SELECT valor,contenido FROM 902_opcionesFormulario WHERE idGrupoElemento=8939 ORDER BY valor";
	$arrTipoMedioControl=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT DISTINCT anio,anio FROM 7044_procesosLibrosGobierno WHERE tipoLibro=20";
	$arrAnios=$con->obtenerFilasArreglo($consulta);
	$anioActual=date('Y');
	
	
?>
var arrSituacion=[['1','Activo'],['2','Cancelado']];

var arrTipoMedioControl=<?php echo $arrTipoMedioControl?>;

var anioActual='<?php echo $anioActual?>';
var arrAnios=<?php echo $arrAnios?>;

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Registro de Medios de Control Constitucional</b></span>',
                                                items:	[
                                                            crearGridLibroGobierno()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridLibroGobierno()
{
	var cmbAnio=crearComboExt('cmbAnio',arrAnios,0,0,150);
    var pos=obtenerPosFila(gEx('cmbAnio').getStore(),'id',anioActual);
    if(pos!=-1)
    {
    	cmbAnio.setValue(anioActual);
    }
    else
    {
    	if(arrAnios.length>0)
        {
        	cmbAnio.setValue(arrAnios[arrAnios.length-1]);
        }
    }
    
    cmbAnio.on('select',function()
    					{
                        	gEx('gLibro').getStore().reload();
                        }
    			)
    
    
    
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'idFormulario'},
                                                        {name:'folio'},
		                                                {name: 'noExpediente'},
                                                        {name:'actor'},
		                                                {name:'demandado'},
                                                        {name: 'fechaRecepcion',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'observaciones'},
                                                        {name: 'datosParticipantes'},
                                                        {name: 'magistradoInstructor'},
                                                        {name: 'tipoMedioControl'},
                                                        {name: 'fechaAuto',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'fechaRegistro',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'situacion'},
                                                        {name: 'fechaCancelacion',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'motivoCancelacion'},
                                                        {name: 'responsableCancelacion'},
                                                        {name: 'iRegistroLibro'}
                                                       
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='19';
                                        proxy.baseParams.anioJudicial=cmbAnio.getValue()==''?anioActual:cmbAnio.getValue();
                                        proxy.baseParams.tLibro=20;
                                    }
                        )   
    
    var paginador=	new Ext.PagingToolbar	(
                                              {
                                                    pageSize: 100,
                                                    store: alDatos,
                                                    displayInfo: true,
                                                    disabled:false
                                                }
                                             )       
    
	var expander = new Ext.ux.grid.RowExpander({
                                                    column:2,
                                                    tpl : new Ext.Template(
                                                        '<table width="100%" >'+
                                                        '<tr><td  style="padding:10px">{datosParticipantes}</td></tr>'+
                                                        '</table>'
                                                    )
                                                }); 
       
	var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'',
                                                                width:30,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(val=='1')
                                                                            	return '<img src="../images/accept_green.png" title="Activo" Inactivo="Inactivo"/>';	
                                                                            return '<img src="../images/cancel_round.png" title="Cancelado" Inactivo="Cancelado"/>';	
                                                                        }
                                                            },
                                                            {
                                                                header:'Folio',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'folio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href="javascript:abrirFormularioProcesoLibro(\''+bE(registro.data.idFormulario)+'\',\''+bE(registro.data.idRegistro)+'\')">'+val+'</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:150,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        },
                                                                dataIndex:'fechaRegistro'
                                                            }
                                                            ,
                                                           
                                                            {
                                                                header:'No. Expediente',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'noExpediente'
                                                            },
                                                            
                                                            {
                                                                header:'Actor',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'actor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Demandado',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'demandado',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                             {
                                                                header:'Fecha en que se<br />interpuso el medio de <br />control constitucional',
                                                                width:150,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        },
                                                                dataIndex:'fechaRecepcion'
                                                            }
                                                            
                                                            ,
                                                            {
                                                                header:'Tipo de Medio de Control',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'tipoMedioControl',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrTipoMedioControl,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de auto<br />admisorio prevenci&oacute;n<br />o desechamiento',
                                                                width:150,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
	                                                                        	return val.format('d/m/Y');
                                                                        },
                                                                dataIndex:'fechaAuto'
                                                            },
                                                            {
                                                                header:'Magistrado Instructor',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'magistradoInstructor',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Observaciones',
                                                                width:400,
                                                                hidden:false,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'observaciones'
                                                            },
                                                             {
                                                                header:'Situaci&oacute;n registro',
                                                                width:150,
                                                                hidden:false,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSituacion,val);
                                                                        }
                                                                
                                                            },
                                                            
                                                            {
                                                                header:'Fecha Cancelaci&oacute;n',
                                                                width:130,
                                                                hidden:false,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
                                                                            	return val.format('d/m/Y');
                                                                        }
                                                                ,
                                                                dataIndex:'fechaCancelacion'
                                                            },
                                                            
                                                            {
                                                                header:'Motivo de la cancelaci&oacute;n',
                                                                width:400,
                                                                hidden:false,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'motivoCancelacion'
                                                            }
                                                        ]
                                              	);
                                                    
	var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gLibro',
                                                    store:alDatos,
                                                    border:false,
                                                    region:'center',
                                                    frame:false,
                                                    cm: cModelo,
                                                    bbar:[paginador],
                                                    plugins:[expander],
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    tbar:	[
                                                    			{
                                                                	xtype:'label',
                                                                    html:'<b>A&ntilde;o Judicial:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                },
                                                                cmbAnio,'-',
                                                                {
                                                                      icon:'../images/cross.png',
                                                                      id:'btnCancelar',
                                                                      disabled:true,
                                                                      cls:'x-btn-text-icon',
                                                                      text:'Cancelar registro',
                                                                      handler:function()
                                                                              {
                                                                                  mostrarVentanaCancelacion();
                                                                              }
                                                                      
                                                                  }
                                                    		],
                                                    columnLines : true,                                                    
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: false,
                                                                                        enableGrouping :false,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: false,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
	
    
    tblGrid.getStore().load(	{
    								params:	{
                                    			url:'../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php',
                                                start:0, 
                                                limit:100
                                                
                                    		}
    							}
                           )  
    
    
    tblGrid.getSelectionModel().on('rowselect',function(sm,numFila,registro)
    											{
                                                	gEx('btnCancelar').disable();
                                                	if(registro.data.situacion=='1')
                                                    	gEx('btnCancelar').enable();
                                                }
    								)

	tblGrid.getSelectionModel().on('rowdeselect',function(sm,numFila,registro)
    											{
                                                	gEx('btnCancelar').disable();
                                                	
                                                }
    								)
    
    return 	tblGrid;
}


function abrirFormularioProcesoLibro(iF,iR)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",bE(0)],['dComp',bE(accion)]];
    
    
    var obj={};
    obj.url="../modeloPerfiles/vistaDTDv3.php";
    obj.params=arrDatos;
    obj.ancho='100%';
    obj.alto='100%';
    
    abrirVentanaFancySuperior(obj);
    
    
    
}

function mostrarVentanaCancelacion()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el motivo de la cancelaci&oacute;n'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:60,
                                                            id:'txtMotivoCancelacion'
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar Registro',
										width: 650,
										height:200,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivoCancelacion').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var fila=gEx('gLibro').getSelectionModel().getSelected();
                                                                    	if(gEx('txtMotivoCancelacion').getValue()=='')
                                                                        {
                                                                        	function respAux()
                                                                            {
                                                                            	gEx('txtMotivoCancelacion').focus();
                                                                            }
                                                                            msgBox('Debe indicar el motivo de la cancelaci&oacute;n del registro',respAux);
                                                                        	return;
                                                                        }
																		function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gLibro').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php',funcAjax, 'POST','funcion=24&tL=20&s=2&c='+cv(gEx('txtMotivoCancelacion').getValue())+'&iR='+fila.data.iRegistroLibro,true);
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer marcar el registro como cancelado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}