Ext.onReady(inicializar);

function inicializar()
{
	var iProv=document.getElementById('idProv').value;
	Ext.QuickTips.init();
    var viewport = new Ext.Viewport(
    									{
        									layout:'fit',
									        items:[
                                            		{
                                                        xtype: 'grouptabpanel',
                                                        tabWidth: 190,
                                                        activeGroup: 0,
                                                        items: 	[
                                                        			{
                                                                        mainItem: 1,
                                                                        items: 	[
                                                                        			{
                                                                                        title: 'Datos Proveedor',
                                                                                        layout: 'fit',
                                                                                        iconCls: 'x-icon-tickets',
                                                                                        
                                                                                        style: 'padding: 10px;',
                                                                                        items: 	[
                                                                                        			{
                                                                                                        id:'frameCategoria',
                                                                                                        xtype:'iframepanel',
                                                                                                        height:860,
                                                                                                        border:false,
                                                                                                        autoLoad:	{	
                                                                                                                        url:'../modeloPerfiles/tblFormularios.php?idFormulario=405',
                                                                                                                        scripts:true,
                                                                                                                        params:	{
                                                                                                                                    cPagina:'sFrm=true',
                                                                                                                                    idProv:iProv
                                                                                                                                }
                                                                                                                    },
                                                                                                         loadMask:	{
                                                                                                                        msg:'Cargando'
                                                                                                                    }
                                                                                                    }
                                                                                                ]
                                                                                    }, 
                                                                                    {
                                                                                        xtype: 'portal',
                                                                                        title: 'Configuraci&oacute;n general',
                                                                                        items:	[
                                                                                        				

                                                                                                    
                                                                                     			]                    
                                                                                    },
                                                                                    {
                                                                                        title: 'Pedidos pendientes',
                                                                                        iconCls: 'x-icon-subscriptions',
                                                                                        style: 'padding: 10px;',
                                                                                        layout: 'fit',
                                                                                        items: 	[
                                                                                        			{
                                                                                                        id:'frameSeccion',
                                                                                                        xtype:'iframepanel',
                                                                                                        height:860,
                                                                                                        border:false,
                                                                                                        autoLoad:	{	
                                                                                                                        url:'../proveedores/pedidosPendientes.php',
                                                                                                                        scripts:true,
                                                                                                                        params:	{
                                                                                                                                    cPagina:'sFrm=true',
                                                                                                                                    idProv:iProv
                                                                                                                                }
                                                                                                                    },
                                                                                                         loadMask:	{
                                                                                                                        msg:'Cargando'
                                                                                                                    }
                                                                                                    }
                                                                                   				]	
                                                                                    }
                                                                                    , 
                                                                                    {
                                                                                        title: 'Agenda pedidos',
                                                                                        iconCls: 'x-icon-subscriptions',
                                                                                        style: 'padding: 10px;',
                                                                                        layout: 'fit',
                                                                                        items: 	[
                                                                                        			{
                                                                                                        id:'frameSeccion',
                                                                                                        xtype:'iframepanel',
                                                                                                        height:860,
                                                                                                        border:false,
                                                                                                        autoLoad:	{	
                                                                                                                        url:'../proveedores/agendaProveedor.php',
                                                                                                                        scripts:true,
                                                                                                                        params:	{
                                                                                                                                    cPagina:'sFrm=true',
                                                                                                                                    idProv:iProv
                                                                                                                                }
                                                                                                                    },
                                                                                                         loadMask:	{
                                                                                                                        msg:'Cargando'
                                                                                                                    }
                                                                                                    }
                                                                                   				]	
                                                                                    }
//                                                                                ]
//                                                                    },
//                                                                    {
//                                                                        mainItem: 0,
//                                                                        items: 	[
//                                                                        			{
//                                                                                        xtype: 'portal',
//                                                                                        title: 'Estructura almac&eacute;n',
//                                                                                        tabTip: 'Dashboard tabtip',
//                                                                                        items:	[
//                                                                                        				
//
//                                                                                                    
//                                                                                     			]                    
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Secciones',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameSecciones',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/tblSecciones.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }
//                                                                                ]
//                                                                    },
//                                                                    {
//                                                                        mainItem: 0,
//                                                                        items: 	[
//                                                                        			{
//                                                                                        xtype: 'portal',
//                                                                                        title: 'Pedidos',
//                                                                                        tabTip: 'Dashboard tabtip',
//                                                                                        items:	[
//                                                                                        				
//
//                                                                                                    
//                                                                                     			]                    
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Por recibir',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        listeners: 
//                                                                                        		{
//                                                                                                    activate:function()
//                                                                                                              {
//                                                                                                                Ext.getCmp('framePendientes').load(
//                                                                                                                                                      {
//                                                                                                                                                          url:'../almacen/pedidosAlmacen.php',
//                                                                                                                                                          scripts:true,
//                                                                                                                                                          params:	{
//                                                                                                                                                                        cPagina:'sFrm=true',
//                                                                                                                                                                        idAlmacen:iAlmacen
//                                                                                                                                                                   }
//                                                                                                                                                      }
//                                                                                                                                                  )
//                                                                                                              }
//                                                                                                            
//                                                                                                }  ,
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'framePendientes',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/pedidosAlmacen.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Recibidos',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        listeners: 
//                                                                                        		{
//                                                                                                    activate:function()
//                                                                                                              {
//                                                                                                                Ext.getCmp('frameRecibidos').load(
//                                                                                                                                                      {
//                                                                                                                                                          url:'../almacen/pedidosRecibidos.php',
//                                                                                                                                                          scripts:true,
//                                                                                                                                                          params:	{
//                                                                                                                                                                        cPagina:'sFrm=true',
//                                                                                                                                                                        idAlmacen:iAlmacen
//                                                                                                                                                                   }
//                                                                                                                                                      }
//                                                                                                                                                  )
//                                                                                                              }
//                                                                                                            
//                                                                                                }  ,
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameRecibidos',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/pedidosRecibidos.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Cancelados',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameCategoria',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/pedidosCancelados.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    },
//                                                                                    {
//                                                                                        title: 'Agenda de recepciones',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameCategoria',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/agendaAlmacen.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }
//                                                                                ]
//                                                                    },
//                                                                    {
//                                                                        mainItem: 0,
//                                                                        items: 	[
//                                                                        			{
//                                                                                        xtype: 'portal',
//                                                                                        title: 'Movimientos de almac&eacute;n',
//                                                                                        tabTip: 'Dashboard tabtip',
//                                                                                        items:	[
//                                                                                        				
//
//                                                                                                    
//                                                                                     			]                    
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Transferencias',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameCategoria',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/pedidosAlmacen.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }, 
//                                                                        			{
//                                                                                        title: 'Bajas',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameCategoria',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/tblCategoriaProducto.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Distribuci&oacute;n',
//                                                                                        iconCls: 'x-icon-subscriptions',
//                                                                                        tabTip: 'Subscriptions tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        layout: 'fit',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameSeccion',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/tblSecciones.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                   				]	
//                                                                                    },
//                                                                                    {
//                                                                                        title: 'Puntos de venta',
//                                                                                        iconCls: 'x-icon-subscriptions',
//                                                                                        tabTip: 'Subscriptions tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        layout: 'fit',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameSeccion',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/tblSecciones.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                   				]	
//                                                                                    }
//                                                                                ]
//                                                                    },
//                                                                    {
//                                                                        mainItem: 0,
//                                                                        items: 	[
//                                                                        			{
//                                                                                        xtype: 'portal',
//                                                                                        title: 'Otros',
//                                                                                        tabTip: 'Dashboard tabtip',
//                                                                                        items:	[
//                                                                                        				
//
//                                                                                                    
//                                                                                     			]                    
//                                                                                    }, 
//                                                                                    {
//                                                                                        title: 'Inventario',
//                                                                                        layout: 'fit',
//                                                                                        iconCls: 'x-icon-tickets',
//                                                                                        tabTip: 'Tickets tabtip',
//                                                                                        style: 'padding: 10px;',
//                                                                                        items: 	[
//                                                                                        			{
//                                                                                                        id:'frameInventario',
//                                                                                                        xtype:'iframepanel',
//                                                                                                        height:860,
//                                                                                                        border:false,
//                                                                                                        autoLoad:	{	
//                                                                                                                        url:'../almacen/inventario.php',
//                                                                                                                        scripts:true,
//                                                                                                                        params:	{
//                                                                                                                                    cPagina:'sFrm=true',
//                                                                                                                                    idAlmacen:iAlmacen
//                                                                                                                                }
//                                                                                                                    },
//                                                                                                         loadMask:	{
//                                                                                                                        msg:'Cargando'
//                                                                                                                    }
//                                                                                                    }
//                                                                                                ]
//                                                                                    }
//                                                                                   
                                                                                ]
                                                                    }
                                                              ]
												}
											]
										}
									);	
}