<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
var today = new Date().clearTime();
var arrSedes=[];               
App = function() {
    				return 	{
        						init : 	function() 
                                		{
            								Ext.BLANK_IMAGE_URL = '../images/s.gif';
                                            var idUsuario=gE('idUsuario').value;
                                            this.calendarStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                storeId: 'calendarStore',
                                                                                                root: 'calendarios',
                                                                                                idProperty: 'id',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                autoLoad: true,
                                                                                                fields: [
                                                                                                            {name:'CalendarId', mapping: 'id', type: 'string'},
                                                                                                            {name:'Title', mapping: 'title', type: 'string'}
                                                                                                        ],
                                                                                                sortInfo: 	{
                                                                                                                field: 'CalendarId',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=11;
                                                                                                                            }
                                                                                                			}
                                                                                            }
                                                                                         );
                                            
                                            this.eventStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                id: 'eventStore',
                                                                                                root: 'evts',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                fields: Ext.calendar.EventRecord.prototype.fields.getRange(),
                                                                                                sortInfo: 	{
                                                                                                                field: 'StartDate',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=12;
                                                                                                                                proxy.baseParams.idProv=gE('idProveedor').value;;
                                	
                                                                                                                            }	
                                                                                                			}
                                                                                            }
                                                                                       );
            
                                           
                                            new Ext.Viewport	(
                                                                    {
                                                                        layout: 'border',
                                                                        renderTo: 'calendar-ct',
                                                                        items: 	[
                                                                                    
                                                                                    {
                                                                                        id: 'app-center',
                                                                                        title: '...',                                                                                         region: 'center',
                                                                                        layout: 'border',
                                                                                        items: 	[
                                                                                                    {
                                                                                                        id:'app-west',
                                                                                                        region: 'west',
                                                                                                        width: 178,
                                                                                                        border: false,
                                                                                                        collapsible:true,
                                                                                                        items:	[
                                                                                                                    {
                                                                                                                        xtype: 'datepicker',
                                                                                                                        id: 'app-nav-picker',
                                                                                                                        cls: 'ext-cal-nav-picker',
                                                                                                                        listeners:	{
                                                                                                                                        'select':	{
                                                                                                                                                        fn: function(dp, dt)
                                                                                                                                                            {
                                                                                                                                                                App.calendarPanel.setStartDate(dt);
                                                                                                                                                            },
                                                                                                                                                        scope: this
                                                                                                                                                    }
                                                                                                                                    }
                                                                                                                    }
                                                                                                                ]
                                                                                                    },
                                                                                                    {
                                                                                                        xtype: 'calendarpanel',
                                                                                                        eventStore: this.eventStore,
                                                                                                        calendarStore: this.calendarStore,
                                                                                                        border: false,
                                                                                                        id:'app-calendar',
                                                                                                        region: 'center',
                                                                                                        activeItem: 1, // month view
                                                                                                        
                                                                                                        monthViewCfg:	{
                                                                                                                            showHeader: true,
                                                                                                                            showWeekLinks: true,
                                                                                                                            showWeekNumbers: false
                                                                                                                        },
                                                        
                                                                                                        
                                                                                                        showDayView: true,
                                                                                                        showWeekView: true,
                                                                                                        
                                                                                                        initComponent: 	function()
                                                                                                                        {
                                                                                                                            App.calendarPanel = this;
                                                                                                                            this.constructor.prototype.initComponent.apply(this, arguments);
                                                                                                                        },
                                                        
                                                                                                        listeners: 	{
                                                                                                                        'eventclick':	{
                                                                                                                                            fn: function(vw, rec, el)
                                                                                                                                                {
                                                                                                                                                	var idPedido=rec.data.EventId;
                                                                                                                                                    var bandera=2;
                                                                                                                                                    tb_show(lblAplicacion,'../fichas/fichaPedido.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idPedido='+idPedido+'&bandera='+bandera+'&TB_iframe=true&height=420&width=500',"","scrolling=yes");
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventover': 	function(vw, rec, el)
                                                                                                                                        {
                                                                                                                                            
                                                                                                                                        },
                                                                                                                        'eventout': function(vw, rec, el)
                                                                                                                                    {
                                                                                                                                        
                                                                                                                                    },
                                                                                                                        'eventadd': {
                                                                                                                                        fn: function(cp, rec)
                                                                                                                                            {
                                                                                                                                            	
                                                                                                                                            	
                                                                                                                                            },
                                                                                                                                        scope: this
                                                                                                                                    },
                                                                                                                        'eventupdate': {
                                                                                                                                            fn: function(cp, rec)
                                                                                                                                                {
                                                                                                                                                   	                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventdelete':	{
                                                                                                                                            fn: function(cp, rec)
                                                                                                                                                {
                                                                                                                                                	
                                                                                                                                                   
                                                                                                                                                            
                                                                                                                                                },
                                                                                                                                            scope:this
                                                                                                                                        },
                                                                                                                        'eventcancel': 	{
                                                                                                                                            fn: function(cp, rec)
                                                                                                                                            {
                                                                                                                                                
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'viewchange': 	{
                                                                                                                                            fn: function(p, vw, dateInfo)
                                                                                                                                                {
                                                                                                                                                                                                                                                                                                 },
                                                                                                                                                scope: this
                                                                                                                                        },
                                                                                                                        'dayclick': 	{
                                                                                                                                            fn: function(vw, dt, ad, el)
                                                                                                                                                {
                                                                                                                                                	alert(dt);
                                                                                                                                                	alert('ok');
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'rangeselect': {
                                                                                                                                            fn: function(win, dates, onComplete)
                                                                                                                                                {
																																				
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventmove': 	{
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventresize': {
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventdelete': {
                                                                                                                                            fn: function(win, rec)
                                                                                                                                            {
                                                                                                                                                this.eventStore.remove(rec);
                                                                                                                                               
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'initdrag': 	{
                                                                                                                                            fn: function(vw)
                                                                                                                                            {
                                                                                                                                                if(this.editWin && this.editWin.isVisible())
                                                                                                                                                {
                                                                                                                                                    //this.editWin.hide();
                                                                                                                                                }
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        }
                                                                                                                    }
                                                                                                    }
                                                                                                ]
                                                                                    }
                                                                                ]
                                                                    }
                                                               )
                                            if(gE('fechaInicial').value!='')
                                            {                   
                                                var fechaInicial=new Date(gE('fechaInicial').value);
                                               gEx('app-calendar').setStartDate(fechaInicial);                                                               
                                            }
                                        },
        
                               
                                showEditWindow : function(rec, animateTarget)
                                				{
                                    				
                                    				
                                				},
                                
                                
                                updateTitle: function(startDt, endDt)
                                			{
                                                var p = Ext.getCmp('app-center');
                                                
                                                if(startDt.clearTime().getTime() == endDt.clearTime().getTime())
                                                {
                                                    p.setTitle(startDt.format('F j, Y'));
                                                }
                                                else
                                                	if(startDt.getFullYear() == endDt.getFullYear())
                                                    {
                                                        if(startDt.getMonth() == endDt.getMonth())
                                                        {
                                                            p.setTitle(startDt.format('F j') + ' - ' + endDt.format('j, Y'));
                                                        }
                                                    	else
                                                        {
                                                        	p.setTitle(startDt.format('F j') + ' - ' + endDt.format('F j, Y'));
                                                    	}
                                                	}
                                                	else
                                                    {
                                                    	p.setTitle(startDt.format('F j, Y') + ' - ' + endDt.format('F j, Y'));
                                                	}
                                            },
                                
                                
                                showMsg: function(msg)
                                		{
                                            Ext.fly('app-msg').update(msg).removeClass('x-hidden');
                                        },
                                        
                                clearMsg: function()
                                        {
                                            Ext.fly('app-msg').update('').addClass('x-hidden');
                                        }
    						}
				}();

Ext.onReady(App.init, App);                                   



///----

var arrTipoRelacion=[];
var idGrid='';
function  crearGridUsuariosInvitados()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idUsuario'},
                                                                {name: 'nombreUsuario'},
                                                                {name: 'tipoRelacion'}
                                                            ]
                                                }
                                            );
	
    alDatos.loadData(dsDatos);
   	var almacen=new Ext.data.SimpleStore	(
											 	{
													fields:	[
															 	{name:'id'},
																{name:'nombre'},
																{name:'valorComp'}
																
															]
												}
											)
	almacen.loadData(arrTipoRelacion);                                            
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Usuario',
															width:300,
															sortable:true,
															dataIndex:'nombreUsuario'
														},
														{
															header:'Participaci&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'tipoRelacion',
                                                            editor:new Ext.form.ComboBox(
                                                            								{
                                                                                                id:'cmbTipoRelacion_'+idGrid,
                                                                                                mode:'local',
                                                                                                emptyText:'Elija una opci\u00f3n',
                                                                                                store:almacen,
                                                                                                displayField:'nombre',
                                                                                                valueField:'id',
                                                                                                editable:false,
                                                                                                typeAhead: true,
                                                                                                triggerAction: 'all',
                                                                                                lazyRender:true
                                                                                             }
                                                                                        ),
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoRelacion,val);
                                                                    }
                                                                    
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:idGrid,
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:550,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	agregarUsuario(tblGrid,idGrid);
                                                                                        
                                                                                        
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al usuario que desea remover del evento');
                                                                                            return;
                                                                                        }
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                                generarArregloUsuarios(tblGrid);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover del evento al usuario seleccionado',resp)
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );

	return 	tblGrid;
}