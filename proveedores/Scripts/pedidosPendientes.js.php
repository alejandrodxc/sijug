<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);
function inicializar()
{
    crearGridP();
}


function  crearGridP()
{
 	var idProv=gE('idProveedor').value;
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPedido'},
                                                                  {name: 'folioPedido'},
                                                                  {name: 'idAlmacen'},
                                                                  {name: 'nombreAlmacen'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'fechaAgenda'},
                                                                  {name: 'fechaSolicitada'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesProveedor.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=1;
                                        proxy.baseParams.idProv=idProv;
                                    }
                        );
   
    
  // var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'txtRazonSocial2' 
//                                                                      },
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'folioPedido' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Folio',
															width:80,
															sortable:true,
															dataIndex:'folioPedido',
                                                            align:'left'
														},
                                                        {
															header:'Almacen de Entrega',
															width:280,
															sortable:true,
                                                            align:'left',
															dataIndex:'nombreAlmacen'
														},
                                                        {
															header:'Fecha programada',
															width:120,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaRecepcion'
														},
                                                        {
															header:'Fecha Aceptada',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaAgenda'
														},
                                                        {
															header:'Fecha solicitada',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaAgenda'
														},
                                                        {
															header:'',
															width:80,
															sortable:true,
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	var cadena='';
                                                                            var fechaAct=gE('fechaActual').value;
                                                                            var fA=convertirCadenaFecha(fechaAct);
                                                                            cadena='<a href="javascript:verDetalle('+registro.get('idPedido')+','+registro.get('folioPedido')+',\''+registro.get('nombreAlmacen')+'\',\''+registro.get('fechaRecepcion')+'\',\''+registro.get('fechaAgenda')+'\')"><img height="13" width="13" src="../images/icon_document.gif" alt="Ver Detalle" title="Ver Detalle" /></a>&nbsp;&nbsp;'+
                                                                            	   '<a href="javascript:enviarFactura('+registro.get('idPedido')+','+registro.get('folioPedido')+')"><img height="13" width="13" src="../images/document_001.gif" alt="Enviar Factura" title="Enviar Factura" /></a>&nbsp;&nbsp;';
                                                                            if(registro.get('fechaAgenda')=='')
                                                                            {
                                                                                if(fA<convertirCadenaFecha(registro.get('fechaRecepcion')))
                                                                                {
                                                                                	cadena+='<a href="javascript:solicitarCita('+registro.get('idPedido')+','+registro.get('folioPedido')+',\''+bE(registro.get('nombreAlmacen'))+'\',\''+registro.get('fechaRecepcion')+'\')"><img height="13" width="13" src="../images/calendar.png" alt="Cancelar" title="Cancelar" /></a>';
                                                                                }    
                                                                            }
                                                                            return cadena;       
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPendientes',
                                                            title:'',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'pedidosP',
                                                            height:750,
                                                            width:800
                                                            //,
//                                                            plugins: [filters]
                                                        }
                                                    );
    dsRegistrosP.load()  ;
    return tblGridP;     
}

function verDetalle(idPedido,folio,nombreAlmacen,fechaC,fechaA)
{
    var gP=gridProductos(idPedido);
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaHistorial',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:65,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">No Folio: </span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:130,
                                                            y:10,
                                                            html:'<span class="letraExt">'+folio+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:40,
                                                            y:30,
                                                            html:'<span class="letraRojaSubrayada8">Entregar en:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:130,
                                                            y:30,
                                                            html:'<span class="letraExt">'+nombreAlmacen+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:35,
                                                            y:50,
                                                            html:'<span class="letraRojaSubrayada8">Fecha Limite:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:130,
                                                            y:50,
                                                            html:'<span class="letraExt">'+fechaC+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:70,
                                                            html:'<span class="letraRojaSubrayada8">Fecha Agendada:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:130,
                                                            y:70,
                                                            html:'<span class="letraExt">'+fechaA+'</span>'
                                                        },
                                                        {
                                                        	xtype:'panel',
                                                            x:10,
                                                            y:105,
                                                            items:[
                                                                        gP
                                                                   ]
                                                         }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Detalle de Pedido',
										width: 740,
										height:580,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaD',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
        ventana.show();
}

function gridProductos(idPedido)
{
    var dsRegistrosProd=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad: true,
                                                        fields: [
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'clave_Art'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'cantidad'},
                                                                  {name: 'estado'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	                                                    
	dsRegistrosProd.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=6;
                                        proxy.baseParams.idPedido=idPedido;
                                    }
                        );
   
   var suma=0;
   //dsRegistrosProd.load({callback:function()
//   								  {
//                                  	   var tamanoS=dsRegistrosProd.getCount();
//                                       var y;
//                                       for(y=0;y<tamanoS;y++)
//                                       {
//                                          var elemento=dsRegistrosProd.getAt(y);
//                                          if(elemento.get('estado')==2)
//                                          {
//                                              suma=suma+1;
//                                          }
//                                       }
//                                       
//                                       var boton=Ext.getCmp('removerB');
//                                       if(suma>0)
//                                       {
//                                            boton.show();
//                                       }
//                                       else
//                                       {
//                                            boton.hide();
//                                       }
//                                  }
//                        }); 
   
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr></table>'
                                                                        
                                                					  )
                                            });                                           
   
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
														{
															header:'Producto',
															width:550,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														}
                                                        ,
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad'
														}
                                                       // ,
//                                                        {
//															header:'Acciones',
//															width:250,
//															sortable:true,
//                                                            align:'left',
//                                                            renderer:function(val,meta,registro)
//                                                            		{
//	                                                                    	var estado=registro.get('estado');
//                                                                            
//                                                                            var checadoA='';
//                                                                            var checadoD='';
//                                                                            var muestraD='';
//                                                                            switch(estado)
//                                                                            {
//                                                                            	case '0':
//                                                                                	checadoA='';
//                                                                                    checadoD='';
//                                                                                break;
//                                                                                case '1':
//                                                                                	checadoA='checked="checked"';
//                                                                                    checadoD='';
//                                                                                    muestraD='';
//                                                                                break;
//                                                                                case '2':
//                                                                                	checadoD='checked="checked"';
//                                                                                    checadoA='';
//                                                                                    muestraD='<a href="javascript:agregarObservacion('+idPedido+','+registro.get('idProducto')+')"><img height="13" width="13" src="../images/paperpencil_48.png" alt="Agregar Observaci&oacute;n" title="Agregar Observaci&oacute;n" />Observaciones</a>';
//                                                                                break;
//                                                                            
//                                                                            }
//                                                                            return '<input type="radio" name="producto_'+registro.get('idProducto')+'[]" id="aceptado_'+registro.get('idProducto')+'" onchange="guardaTemp(this,'+idPedido+','+registro.get('idProducto')+')" '+checadoA+' />&nbsp;&nbsp;<b>Aceptado</b>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="producto_'+registro.get('idProducto')+'[]" id="detalle_'+registro.get('idProducto')+'" onchange="guardaTemp(this,'+idPedido+','+registro.get('idProducto')+')" '+checadoD+' />&nbsp;&nbsp;<b>Con detalle</b>&nbsp;&nbsp;<label id="eObserv">'+muestraD+'</label>';
//                                                                    }
//														}
													]
												);
	var tblGridProd=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'articulos',
                                                            x:0,
                                                            y:0,
                                                            title:'Art&iacute;culos',
                                                            store:dsRegistrosProd,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:700,
                                                            width:705,
                                                            plugins: [filters,expander]
                                                           // ,
//                                                            tbar:[
//                                                            	  {
//                                                                      text:'Registrar',
//                                                                      icon:'../images/add.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                          {
//                                                                              registrarPedido(idPedido,0);
//                                                                          }
//                                                                  }
//                                                                  ,
//                                                                  {
//                                                                      text:'Solicitud Vo.Bo. Adquisiciones',
//                                                                      id:'removerB',
//                                                                      icon:'../images/cancel_round.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                          {
//                                                                              registrarPedido(idPedido,3);
//                                                                          }
//                                                                  }
//                                                                 ]
                                                        }
                                                    );
    return tblGridProd;   
}

function enviarFactura(idPedido,folioP)
{
	var arreglo=[['1','Factura Electronica'],['0','Copia digital']];
    var almacen=new Ext.data.SimpleStore	(
											 	{
													fields:	[
															 	{name:'id'},
																{name:'nombre'},
																{name:'valorComp'}
															]
												}
											);
    var comboTmp=document.createElement('select');
    var cmbTipo =new Ext.form.ComboBox	(
													{
														
														id:'cmbTipo',
														x:0,
														y:0,
                                                        fieldLabel: 'Tipo de factura',
														mode:'local',
														emptyText:'Elija una opci\u00f3n',
														store:almacen,
														displayField:'nombre',
														valueField:'id',
														transform:comboTmp,
														editable:false,
														typeAhead: true,
														triggerAction: 'all',
														lazyRender:true,
														width:100,
														listWidth:200
													}
												)                                        
    almacen.loadData(arreglo);                                        
    var idProv=gE('idProveedor').value;
    var fp = new Ext.FormPanel	(
									{
										fileUpload: true,
										width: 500,
										frame: true,
										autoHeight: true,
										bodyStyle: 'padding: 10px 10px 0 10px;',
										labelWidth: 100,
										defaults: 	{
														anchor: '100%',
														msgTarget: 'side'
													},
										items:	[
                                                    cmbTipo,
                                                    {
														xtype: 'fileuploadfield',
														id: 'form-file',
														emptyText: 'Elija el archivo',
														fieldLabel: 'Archivo',
														name: 'image',
														buttonText: '',
														buttonCfg: 	{
																		iconCls: 'upload-icon'
																	}
													},
													{
														name:'cadenaF', 
														xtype: 'textfield',
														id: 'cadenaF',
														fieldLabel: 'Cadena'
													 },
                                                     {
														name:'noFactura', 
														xtype: 'textfield',
														id: 'noFactura',
														fieldLabel: 'No.Factura'
													 },
													 {
														 xtype:'hidden',
														 name:'idProv',
														 value:idProv
													 },
                                                     {
                                                     	 xtype:'hidden',
														 name:'idPedido',
														 value:idPedido
                                                     },
                                                     {
                                                     	 xtype:'hidden',
														 name:'facturaElectronica',
                                                         id:'hiddenFe',
														 value:''
                                                     }
												]
									}
								);
	
		var ventana=new Ext.Window(
							   		{
										id:'ventanaArchivo',
                                        title:'Informaci&oacute;n de factura',
										width:400,
										height:200,
										layout:'fit',
										buttonAlign:'center',
										items:[fp],
										modal:true,
										plain:true,
										listeners:
                                                    {
                                                        show:
                                                                {
                                                                    buffer:10,
                                                                    fn:function()
                                                                            {
                                                                                
                                                                            }
                                                                }
                                                    },
											buttons: 	[
															{
																text: 'Agregar',
																handler: function()
																		{
																			var tipoF=Ext.getCmp('cmbTipo').getValue();
                                                                            if(tipoF==='')
                                                                            {
                                                                            	msgBox('debe indicar el tipo de factura');
                                                                                return;
                                                                            }
                                                                            
                                                                            if(tipoF=='1')
                                                                            {
                                                                                var cadena=Ext.getCmp('cadenaF').getValue();
                                                                                if(cadena=='')
                                                                                {
                                                                                	msgBox('Con el tipo de factuta electronica debe indicar el campo cadena');
                                                                                    return;
                                                                                }
                                                                            }
                                                                            
                                                                            var noFac=Ext.getCmp('noFactura').getValue();
                                                                            if(noFac==='')
                                                                            {
                                                                            	msgBox('Debe indicar el numero de factura');
                                                                                return;
                                                                            }
                                                                            
                                                                            var hidenF=Ext.getCmp('hiddenFe');
                                                                            hidenF.setValue(tipoF);
                                                                            
                                                                            archivo=gE('form-file-file');
																			archivoName=archivo.value;
																			var extension = (archivoName.substring(archivoName.lastIndexOf("."))).toLowerCase();
																			if((fp.getForm().isValid())&&((extension==".jpg")||(extension==".gif")||(extension==".jpeg")||(extension==".bmp")||(extension==".png")||(extension==".pdf")||(extension==".doc")||(extension==".docx")||(extension==".xls")||(extension==".xml")||(extension==".txt")))
																			{
																					fp.getForm().submit	(	
																											{
																												url: '../proveedores/guardarFactura.php',
																												waitMsg: 'Cargando Archivo...',
																												success: function()
																																	{
                                                                                                                                        var almacen=Ext.getCmp('gridPendientes');
																																		almacen.getStore().load();
																																		ventana.close();
																																	},
																												failure: this.falloAccion
																											}
																										);
																			}
																			else
																			{
																				Ext.MessageBox.alert('Error de Archivo', 'El archivo ingresado no es v\u00e1lido');
																			}
																
																		}
															},
															{
																text: 'Cancelar',
																handler: function()
																		{
																			ventana.close();
																		}
															}
														]
									}
							   )
		ventana.show();
}

function solicitarCita(idPedido,folio,nAlmacen,fechaMaxima)
{
    var idProv=gE('idProveedor').value;
    var fechaActual=gE('fechaActual').value;
    fechaMaxima=Ext.util.Format.date(fechaMaxima,'d/m/Y');
    fechaActual=Ext.util.Format.date(fechaActual,'d/m/Y');
    var form1 = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
                                            id:'formulario1',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:85,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Folio:</b>'
													 },
                                                     {
													 x:120,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+folio+'</span>'
													 },
                                                     {
													 x:52,
													 y:30,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Almac&eacute;n:</b>'
													 },
                                                     {
													 x:120,
													 y:30,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+bD(nAlmacen)+'</span>'
													 },
                                                     {
													 x:30,
													 y:50,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Fecha Entrega:</b>'
													 },
                                                     {
                                                     x:120,
													 y:50,
                                                     xtype:'datefield',
													 id:'fechaAP',
                                                     maxValue:fechaMaxima,
                                                     minValue:fechaActual
                                                     },
                                                     {
													 x:50,
													 y:85,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Hora Inicio:</b>'
													 },
                                                     {
                                                     x:120,
													 y:80,
                                                     xtype:'timefield',
													 id:'horaInicio'
                                                     },
                                                     {
													 x:15,
													 y:110,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Tiempo estimado:</b>'
													 },
                                                     {
                                                     x:120,
													 y:105,
                                                     xtype:'numberfield',
                                                     width:50,
													 id:'tiempoE'
                                                     },
                                                     {
													 x:180,
													 y:110,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>(Min.)</b>'
													 }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Solicitud de fecha de entrega',
										width: 450,
										height:270,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form1,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var fechaAgendada=Ext.getCmp('fechaAP').getValue();
                                                                                    if(fechaAgendada=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar la fecha para agendar este pedido');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var horaInicio=Ext.getCmp('horaInicio').getValue();
                                                                                    
                                                                                    if(horaInicio=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar la hora de inicio');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var tiempoE=Ext.getCmp('tiempoE').getValue();
                                                                                    if(tiempoE=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el tiempo estimado');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(tiempoE==0)
                                                                                    {
                                                                                    	msgBox('El tiempo estimado no puede ser igual a <b>0</b>');
                                                                                        return;
                                                                                    }
                                                                                    fechaAgendada=Ext.util.Format.date(fechaAgendada,'Y-m-d');
                                                                                    
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var suma=0;
                                                                                             var almacen=Ext.getCmp('gridPendientes').getStore();
    		  																				 almacen.reload(); 
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=29&idPedido='+idPedido+'&fechaAgendada='+fechaAgendada+'&horaInicio='+horaInicio+'&tiempoE='+tiempoE,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();                
}