<?php
	session_start();
	
	$idSucursal=base64_decode($_GET["idSucursal"]);
	
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="select f.nombreBanSma,b.idBanco,b.idSucursal,b.sucursal,s.cuenta,s.idCentroCosto,s.idProyecto,s.idCuenta,g.codigo from 601_sucursales_banco b, 602_cuenta_banco s, 600_bancos f, 506_centroscosto g where (s.idSucursal=b.idSucursal and f.idBanco=b.idBanco and s.idCentroCosto=g.idCentroCosto) order by b.sucursal";
	$arrSucursales=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrSucursales?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'nombreBanSma'},
                                                                    {name: 'idBanco'},
                                                                    {name: 'idSucursal'},
                                                                    {name: 'sucursal'},
                                                                    {name: 'cuenta'},
                                                                    {name: 'idCentroCosto'},
                                                                    {name: 'idProyecto'},
                                                                    {name: 'idCuenta'},
                                                                    {name: 'codigo'},
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Banco',
															width:200,
															sortable:true,
															dataIndex:'nombreBanSma'
														},
														{
															header:'Sucursal',
															width:200,
															sortable:true,
															dataIndex:'sucursal'
														},
                                                        {
															header:'Cuenta',
															width:90,
															sortable:true,
															dataIndex:'cuenta'
														},
                                                        {
															header:'Centro Costo',
															width:70,
															sortable:true,
															dataIndex:'codigo'
														},
                                                        													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:650,
                                                            renderTo:'tblCuentas',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nueva cuenta',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	/*var arrParam=[['idCuenta','-1'],['idSucursal','<?php echo $idSucursal?>']];
                                                                                    	enviarFormularioDatos('catalogoCuentas.php',arrParam);*/
                                                                                        window.parent.nuevaCuenta();
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar cuenta',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar la cuenta a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idCuenta=fila.get('idCuenta');
                                                                                       /*var arrParam=[['idCuenta',idCuenta]];
                                                                                    	enviarFormularioDatos('catalogoCuentas.php',arrParam);*/
                                                                                        window.parent.modificarCuenta(idCuenta);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Saldos y movimientos',
                                                                            icon:'../images/application.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idCuenta','-1'],['idSucursal','<?php echo $idSucursal?>']];
                                                                                    	enviarFormularioDatos('catalogoCuentas.php',arrParam);
                                                                                    }
                                                                        },
                                                            		]
                                                            
                                                        }
                                                    );
	
}

