<?php
	session_start();
	
	$idBanco=base64_decode($_GET["idBanco"]);
	
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="select b.idBanco,b.idSucursal,b.sucursal,b.ciudad,b.contacto,s.nombreBanSma from 601_sucursales_banco b, 600_bancos s where s.idBanco=b.idBanco order by b.sucursal";
	$arrSucursales=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrSucursales?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idBanco'},
                                                                    {name: 'idSucursal'},
                                                                    {name: 'sucursal'},
                                                                    {name: 'ciudad'},
                                                                    {name: 'contacto'},
                                                                    {name: 'contacto'},
                                                                    {name: 'nombreBanSma'},
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Banco',
															width:250,
															sortable:true,
															dataIndex:'nombreBanSma'
														},
														{
															header:'Sucursal',
															width:200,
															sortable:true,
															dataIndex:'sucursal'
														},
                                                        {
															header:'Ciudad',
															width:120,
															sortable:true,
															dataIndex:'ciudad'
														},
                                                        {
															header:'Contacto',
															width:120,
															sortable:true,
															dataIndex:'contacto'
														},
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:750,
                                                            renderTo:'tblSucursales',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nueva sucursal',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	//var arrParam=[['idSucursal','-1'],['idBanco','<?php echo $idBanco?>']];
                                                                                    	//enviarFormularioDatos('catalogoSucursales.php',arrParam);
                                                                                        window.parent.nuevaSucursal();
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar sucursal',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar la sucursal a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idSucursal=fila.get('idSucursal');
                                                                                       window.parent.modificarSucursal(idSucursal);
                                                                                       //var arrParam=[['idSucursal',idSucursal]];
                                                                                    	//enviarFormularioDatos('catalogoSucursales.php',arrParam);
                                                                                    }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
}

