<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="select b.idBanco,b.nombreBan,b.nombreBanSma,s.nombreStatus from 600_bancos b,4005_status s where s.idStatus=b.status order by b.nombreBan";
	$arrBancos=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrBancos?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idBanco'},
                                                                    {name: 'nombreBanco'},
                                                                    {name: 'nombreCorto'},
                                                                    {name: 'situacion'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Banco',
															width:250,
															sortable:true,
															dataIndex:'nombreBanco'
														},
														{
															header:'Nombre corto',
															width:200,
															sortable:true,
															dataIndex:'nombreCorto'
														},
                                                        {
															header:'Situac&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'situacion'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:500,
                                                            width:650,
                                                            renderTo:'tblBancos',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Agregar banco',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idBanco','-1']];
                                                                                    	enviarFormularioDatos('../bancos/catalogoBancos.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar banco',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar el banco a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idBanco=fila.get('idBanco');
                                                                                       var arrParam=[['idBanco',idBanco]];
                                                                                    	enviarFormularioDatos('../bancos/catalogoBancos.php',arrParam);
                                                                                    }
                                                                        },
                                                                        
                                                                        {
                                                                        	text:'Eliminar banco',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar el banco que desea eliminar');
                                                                                            return;
                                                                                       }
                                                                                       function resp(btn)
                                                                                       {
                                                                                       		if(btn=='yes')
                                                                                            {
                                                                                            	var idBanco=fila.get('idBanco');
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGrid.getStore().remove(fila);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=99&idBanco='+idBanco,true);
                                                                                            }
                                                                                       		
                                                                                       }
                                                                                       msgBox('Est&aacute; seguro de querer remover el banco seleccionado?',resp);
                                                                                       
                                                                                       
                                                                                    }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
}

