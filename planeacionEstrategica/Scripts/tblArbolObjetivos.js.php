<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var oSeleccionado=null;
Ext.onReady(inicializar);

function inicializar()
{
	new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                               	tbar:	[
                                                            {
                                                                icon:'../images/especial.png',
                                                                cls:'x-btn-text-icon',
                                                                hidden:gE('sL').value=='1',
                                                                id:'btnAgregarObjetivo',
                                                                disabled:gE('objetivoCentral').value=='1',
                                                                text:'Agregar Objetivo Central',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaObjetivoCentral();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Agregar Fin',
                                                                id:'btnAgregarFin',
                                                                hidden:gE('sL').value=='1',
                                                                disabled:gE('objetivoCentral').value=='0',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaFin();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/area.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Agregar Medio',
                                                                id:'btnAgregarMedio',
                                                                hidden:gE('sL').value=='1',
                                                                disabled:gE('objetivoCentral').value=='0',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaMedio();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/delete.png',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnRemover',
                                                                disabled:true,
                                                                hidden:gE('sL').value=='1',
                                                                text:'Remover elemento seleccionado',
                                                                handler:function()
                                                                        {
                                                                            removerElementoSeleccionado();
                                                                        }
                                                                
                                                            }
                                                            
                                                        ],
                                                items:	[
                                                			
                                                            new Ext.ux.IFrameComponent({ 
                                                                                              id: 'frameContenido', 
                                                                                              anchor:'100% 100%',
                                                                                              region:'center',
                                                                                              loadFuncion:function(iFrame)
                                                                                                          {
                                                                                                              
                                                                                                          },
    
                                                                                              url: '../paginasFunciones/white.php',
                                                                                              style: 'width:100%;height:100%' 
                                                                                        })
                                                        ]
                                            }
                                         ]
                            }
                        )   

	gEx('frameContenido').load	(
    								{
                                    	url:'../planeacionEstrategica/canvasArbolObjetivos.php',
                                        params:	{
                                        			idFormulario: gE('idFormulario').value,
                                                    idRegistro: gE('idRegistro').value,
                                                    cPagina:'sFrm=true'
                                        		}
    								}
    							)
                        
}

function mostrarVentanaObjetivoCentral(iP,valorTexto)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
	                                                        
                                                        	x:10,
                                                            y:10,
                                                            html:'Objetvo Central:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:valorTexto?escaparBR(valorTexto,true):'',
                                                            id:'txtObjetivoCentral'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: iP?'Editar Objetivo Central':'Agregar Objetivo Central',
										width: 650,
										height:225,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtObjetivoCentral').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var txtObjetivoCentral=gEx('txtObjetivoCentral');
                                                                        if(txtObjetivoCentral.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtObjetivoCentral.focus();
                                                                            }
                                                                            msgBox('Debe indicar el objetivo central a agregar/modificar',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idRegistro":"'+(iP?iP:-1)+'","iFormulario":"'+gE('idFormulario').value+'","iRegistro":"'+gE('idRegistro').value+'","objetivoCentral":"'+cv(gEx('txtObjetivoCentral').getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                redibujarEstructura(iP?iP:-1,'P');
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=43&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaFin(iP,valorTexto)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
	                                                        
                                                        	x:10,
                                                            y:10,
                                                            html:'Fin:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:valorTexto?escaparBR(valorTexto,true):'',
                                                            id:'txtFin'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: iP?'Editar Fin':'Agregar Fin',
										width: 650,
										height:225,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtFin').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var txtFin=gEx('txtFin');
                                                                        if(txtFin.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtFin.focus();
                                                                            }
                                                                            msgBox('Debe indicar el fin que desea agregar/modificar',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idRegistro":"'+(iP?iP:-1)+'","iFormulario":"'+gE('idFormulario').value+'","iRegistro":"'+gE('idRegistro').value+'","fin":"'+cv(gEx('txtFin').getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                redibujarEstructura(iP?iP:-1,'E');
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=44&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaMedio(iP,valorTexto)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
	                                                        
                                                        	x:10,
                                                            y:10,
                                                            html:'Medio:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:valorTexto?escaparBR(valorTexto,true):'',
                                                            id:'txtMedio'
                                                        },
                                                        crearGridActividadesMedios(iP?iP:-1)
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: iP?'Editar Medio':'Agregar Medio',
										width: 650,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMedio').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var txtMedio=gEx('txtMedio');
                                                                        if(txtMedio.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtMedio.focus();
                                                                            }
                                                                            msgBox('Debe indicar el medio que desea agregar/modificar',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        var arrActividades='';
                                                                        var x;
                                                                        var f;
                                                                        var obj='';
                                                                        for(x=0;x<gEx('gActividades').getStore().getCount();x++)
                                                                        {
                                                                        	f=gEx('gActividades').getStore().getAt(x);
                                                                            obj='{"idActividad":"'+f.data.idActividad+'","actividad":"'+cv(f.data.actividad)+'"}';
                                                                            if(arrActividades=='')
                                                                            	arrActividades=obj;
                                                                            else
                                                                            	arrActividades+=','+obj;
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        var cadObj='{"idRegistro":"'+(iP?iP:-1)+'","iFormulario":"'+gE('idFormulario').value+
                                                                        	'","iRegistro":"'+gE('idRegistro').value+'","medio":"'+cv(gEx('txtMedio').getValue())+
                                                                            '","arrActividades":['+arrActividades+']}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                redibujarEstructura(iP?iP:-1,'C');
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=45&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function redibujarEstructura(idEditado,tNodo)
{
	desHabilitarBotonEliminar();
    gEx('frameContenido').getFrameWindow().obtenerInformacionArbol(idEditado,tNodo);                
        
    
}

function setObjetoSeleccionado(objSeleccionado)
{
	oSeleccionado=objSeleccionado;
}

function editarObjeto(objSeleccionado)
{
	if(gE('sL').value=='1')
    {
    	return;
    }
	oSeleccionado=objSeleccionado;
	switch(objSeleccionado.tipo)
    {
    	case 'M':
        	mostrarVentanaMedio(objSeleccionado.id,objSeleccionado.texto);
        break;
        case 'F':
        	mostrarVentanaFin(objSeleccionado.id,objSeleccionado.texto);
        break;
        case 'O':
        	mostrarVentanaObjetivoCentral(objSeleccionado.id,objSeleccionado.texto);
        break;
        
    }
}

function habilitarBotonEliminar()
{
	gEx('btnRemover').enable();
}

function desHabilitarBotonEliminar()
{
	gEx('btnRemover').disable();
}

function removerElementoSeleccionado()
{
	var cadObj='{"tipo":"'+oSeleccionado.tipo+'","idRegistro":"'+oSeleccionado.id+'"}';
	var lblLeyenda='';
	switch(oSeleccionado.tipo)
    {
    	case 'M':
        	lblLeyenda='Est&aacute; seguro de querer remover el medio seleccionada?';
        break;
        case 'F':
        	lblLeyenda='Est&aacute; seguro de querer remover el fin seleccionado?';
        break;
        case 'O':
        	lblLeyenda='Est&aacute; seguro de querer remover el objetivo seleccionado?';
        break;
        
    }
    
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    redibujarEstructura(-1,0);
                    oSeleccionado=null;
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=46&cadObj='+cadObj,true);
        }
    }
    msgConfirm(lblLeyenda,resp);
}

function crearGridActividadesMedios(iM)
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idActividad'},
		                                                {name: 'actividad'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'actividad', direction: 'ASC'},
                                                            groupField: 'actividad',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='47';
                                        proxy.baseParams.idMedio=iM;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Actividad',
                                                                width:550,
                                                                sortable:true,
                                                                dataIndex:'actividad',
                                                                renderer:function(val,meta)
                                                                		{
                                                                        	meta.attr='style="height: auto;white-space: normal;padding: 6px 6px 6px 10px;"';
                                                                         	return val;
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gActividades',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                x:10,
                                                                y:135,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar actividad',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaAgregarActividad();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                             {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Modificar actividad',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la actividad que desea modificar');
                                                                                            	return;
                                                                                            }
                                                                                            mostrarVentanaAgregarActividad(fila);
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover actividad',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la actividad que desea remover');
                                                                                            	return;
                                                                                            }
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	tblGrid.getStore().remove(fila);
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover la actividad seleccionada?',resp);
                                                                                        }
                                                                                        
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                width:600,
                                                                height:245,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,                                                                
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
	
}

function mostrarVentanaAgregarActividad(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Actividad:'
                                                        },
                                                        {
                                                        	xtype:'textarea',
                                                            width:650,
                                                            height:80,
                                                            x:10,
                                                            y:40,
                                                            value:fila?escaparBR(fila.data.actividad,true):'',
                                                            id:'txtActividad'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: fila?'Modificar actividad':'Agregar actividad',
										width: 700,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtActividad').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtActividad=gEx('txtActividad');
                                                                        if(txtActividad.getValue()=='')
                                                                        {
                                                                        	function respAux()
                                                                            {
                                                                            	txtActividad.focus();
                                                                            }
                                                                            msBox('Debe ingresar la actividad que desea agrega/modificar',respAux);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(fila)
                                                                        {
                                                                        	fila.set('actividad',txtActividad.getValue());
                                                                        }
                                                                        else
                                                                        {
                                                                        
                                                                            var reg=crearRegistro	(
                                                                                                        [{name:'idActividad'},{name:'actividad'}]
                                                                                                    );
                                                                            
                                                                            var r=new reg	(
                                                                                                {
                                                                                                    idActividad:-1,
                                                                                                    actividad:txtActividad.getValue()
                                                                                                }
                                                                                            )
                                                                      	
                                                                        
                                                                        	gEx('gActividades').getStore().add(r);
                                                                        }
                                                                        ventanaAM.close();
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function sinObjetivoCentral()
{
	gEx('btnAgregarObjetivo').enable();
    gEx('btnAgregarFin').disable();
    gEx('btnAgregarMedio').disable();
    
	gEx('btnRemover').disable();
}

function conObjetivoCentral()
{
	gEx('btnAgregarObjetivo').disable();
    gEx('btnAgregarFin').enable();
    gEx('btnAgregarMedio').enable();
    
	gEx('btnRemover').disable();
}