<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["iF"];
	$idRegistro=$_GET["iR"];
	
	
?>
var valorAjusteY=0;
var contextCanvas=null;
var objSeleccionado=null;
var arrObjetos=[];
var tamTotalY=0;
var idObjetivoCentral='';
var objetivoCentral='';
var posYCausa=0;
var tamLetra=10;
var fuente=tamLetra+"pt arial";
var tamInterlineado=tamLetra*1.5;

var margenDerecho=10;
var margenIzquierdo=10;
var margenSuperior=15;
var margenInferior=15;


var separacionBloque=25;
var tamMinimoCanvas=900;
var margenDibujo=140;
var tamBloque=200;
var separacionInterBloque=90;
var baseCalculo;
var margenPerdidaDibujo;


var arrFines=[];
var nFines=0;

var arrMedios=[];
var nMedios=0;
var tamTotal=0;

var maxYActividades=0;
var maxYMedio=0;
var maxYFin=0;

var arrObjMedios=[];
var arrObjFines=[];
var objObjetivoCentral={};

var anchoCanvas=0;
var altoCanvas=0;
var tamObjetivo=0;

Ext.onReady(inicializar);

function inicializar()
{
	var canvas = document.getElementById("myCanvas");
    
    anchoCanvas=obtenerDimensionesNavegador()[1]-30;
    altoCanvas=obtenerDimensionesNavegador()[0]-15;
   	inicializarEventoClickCanvas(canvas);
	obtenerInformacionArbol(-1,0);    
}

function dibujarMapa()
{

	if(idObjetivoCentral=='-1')
    {
    	return;
    }
	arrObjMedios=[];
    arrObjFines=[];
    arrObjetos=[];
    
	var canvas = document.getElementById("myCanvas");
	var ctx = canvas.getContext("2d");
    contextCanvas=ctx;
    
    ctx.font=fuente;
   
    
    baseCalculo=nFines>nMedios?nFines:nMedios;
    
    var anchoMaximo= tamBloque* baseCalculo+(separacionBloque*(baseCalculo-1))+margenDibujo;
        
    if(anchoCanvas<anchoMaximo)
    {
    	anchoCanvas=anchoMaximo;
    }
    
   	var  maxTamObjetivoCentralX=anchoCanvas*(0.5);
    


   
    //Causas
   
    var ctAux=0;
    var tamY;
    var tamY
    var x=0;
    var obj;
    //Causas
   	var txtActividades='';
    for(x=0;x<nMedios;x++)
    {
    	txtActividades='';
    	tamY=estimarWrapText(escaparBR(arrMedios[x][1],true),tamBloque-margenIzquierdo-margenDerecho,tamInterlineado,ctx);
        tamY+=tamLetra+margenInferior;
        if(maxYMedio<tamY)
        	maxYMedio=tamY;
            
        for(ctAux=0; ctAux<arrMedios[x][2].length;ctAux++)
        {
        	if(txtActividades=='')
            	txtActividades='- '+arrMedios[x][2][ctAux][1];
            else
            	txtActividades+='@@@- '+arrMedios[x][2][ctAux][1];
        }
        
        
        if(txtActividades!='')
        {
        	txtActividades=txtActividades.replace(/<br \/>/gi,'@@@');
            tamY=estimarWrapText(txtActividades,tamBloque-margenIzquierdo-margenDerecho,tamInterlineado,ctx);
            tamY+=tamLetra+margenInferior;
            if(maxYActividades<tamY)
                maxYActividades=tamY;
		}
                    
        obj=	{
        			texto:Ext.util.Format.stripTags(arrMedios[x][1].trim(),true),
                    posX:0,
                    posY:0,
                    tipo:'M',
                    id:arrMedios[x][0],
                    actividades:txtActividades,
                    color:'#FFB758'
        		};
        arrObjMedios.push(obj);
        
    }
   
    for(x=0;x<nFines;x++)
    {
    	tamY=estimarWrapText(escaparBR(arrFines[x][1],true),tamBloque-margenIzquierdo-margenDerecho,tamInterlineado,ctx);
        tamY+=tamLetra+margenInferior;
        if(maxYFin<tamY)
        	maxYFin=tamY;
            
       	obj=	{
                    texto:Ext.util.Format.stripTags(arrFines[x][1].trim(),true),
                    posX:0,
                    posY:0,
                    tipo:'F',
                    id:arrFines[x][0],
                    color:'#9FFC9F'
        		};
        arrObjFines.push(obj);     
            
        
    }

    objetivoCentral=Ext.util.Format.stripTags(objetivoCentral.trim());
    
    var tamReal=ctx.measureText(objetivoCentral,true);
    
    if(tamReal.width<maxTamObjetivoCentralX)
    {
    	tamObjetivo=tamReal.width+margenIzquierdo+margenDerecho;
    }
    else
    {
    	tamObjetivo=maxTamObjetivoCentralX;
    }
    
    maxObjetivoCentral=estimarWrapText(objetivoCentral,tamObjetivo-margenIzquierdo-margenDerecho,tamInterlineado,ctx);
    maxObjetivoCentral+=tamLetra+(margenInferior*2);
    
    tamTotalY=(maxYActividades+(maxYActividades>0?separacionInterBloque:0))+maxYMedio+maxYFin+maxObjetivoCentral+(separacionInterBloque*2)+(margenDibujo);
    var tamCuadro=(maxYActividades+(maxYActividades>0?separacionInterBloque:0))+maxYMedio+maxYFin+maxObjetivoCentral+(separacionInterBloque*2);
    var diferenciaY=tamTotalY-tamCuadro;

    canvas.width=anchoCanvas;
    canvas.height=tamTotalY;
       
    
    var posYCentral=(tamTotalY/2)-diferenciaY;
    
    var posXCentral=(anchoCanvas/2)-(tamObjetivo/2);
    
    objObjetivoCentral.texto=objetivoCentral;
    objObjetivoCentral.posX=posXCentral;
    objObjetivoCentral.posY=posYCentral;
    objObjetivoCentral.anchoX=tamObjetivo;
    objObjetivoCentral.id=idObjetivoCentral==''?'-1':idObjetivoCentral;
    objObjetivoCentral.anchoY=maxObjetivoCentral;
    objObjetivoCentral.posXFinal= objObjetivoCentral.posX + objObjetivoCentral.anchoX;
    objObjetivoCentral.posYFinal=objObjetivoCentral.posY + objObjetivoCentral.anchoY;
    
   	objObjetivoCentral.color='#AFF9FC';
    objObjetivoCentral.tipo='O';
    dibujarObjCanvas(objObjetivoCentral,ctx);
    arrObjetos.push(objObjetivoCentral);
  
    var posInicial=0;
    var calculoPosicion=(tamBloque*arrObjMedios.length)+(separacionBloque*(arrObjMedios.length-1));
    posInicial=(anchoCanvas/2)-(calculoPosicion/2);
    
    
    for(x=0;x<arrObjMedios.length;x++)
    {    	
    	arrObjMedios[x].posX=posInicial;
    	arrObjMedios[x].posY=((objObjetivoCentral.posY+objObjetivoCentral.anchoY)+separacionInterBloque);
        arrObjMedios[x].anchoX=tamBloque;
        arrObjMedios[x].anchoY=maxYMedio;
        
        arrObjMedios[x].posXFinal= arrObjMedios[x].posX + arrObjMedios[x].anchoX;
    	arrObjMedios[x].posYFinal=arrObjMedios[x].posY + arrObjMedios[x].anchoY;
        
        posInicial+=tamBloque+separacionBloque;        
        dibujarObjCanvas(arrObjMedios[x],ctx);
        arrObjetos.push(arrObjMedios[x]);
    }
    
    
    calculoPosicion=(tamBloque*arrObjFines.length)+(separacionBloque*(arrObjFines.length-1));
    posInicial=(anchoCanvas/2)-(calculoPosicion/2);
    for(x=0;x<arrObjFines.length;x++)
    {
    	arrObjFines[x].posX=posInicial;
    	arrObjFines[x].posY=(objObjetivoCentral.posY-separacionInterBloque-maxYFin);
        arrObjFines[x].anchoX=tamBloque;
        arrObjFines[x].anchoY=maxYFin;
        
        arrObjFines[x].posXFinal= arrObjFines[x].posX + arrObjFines[x].anchoX;
    	arrObjFines[x].posYFinal=arrObjFines[x].posY + arrObjFines[x].anchoY;
        
        posInicial+=tamBloque+separacionBloque;
        dibujarObjCanvas(arrObjFines[x],ctx);
        arrObjetos.push(arrObjFines[x]);
    }
    


    dibujarConectores(ctx);

	if(gE('idNodoEditado').value!='-1')
    {
		switch(gE('tipoNodo').value)
        {
        	case 'M':
            	for(x=0;x<nMedios;x++)
                {
                    if(arrObjMedios[x].id==gE('idNodoEditado').value)
                    {
                    	objSeleccionado=arrObjMedios[x];
            			dibujarObjCanvasSeleccionado(arrObjMedios[x],contextCanvas);
                    	break;
                    }
                    
                }
            break;
            case 'F':
            	for(x=0;x<nFines;x++)
                {
                    if(arrObjFines[x].id==gE('idNodoEditado').value)
                    {
                    	objSeleccionado=arrObjFines[x];
            			dibujarObjCanvasSeleccionado(arrObjFines[x],contextCanvas);
                    	break;
                    }
                    
                }
            break;
            case 'O':
            	if(objObjetivoCentral.id!='-1')
                {
                    objSeleccionado=objObjetivoCentral;
                    dibujarObjCanvasSeleccionado(objObjetivoCentral,contextCanvas);
                }
            break;
        }
    }
    
}



function dibujarObjCanvas(o,ctx)
{
	if(o)
    {
    	var colorLinea='000';
    	ctx.font=fuente;	
       
        ctx.strokeStyle = "#000";
        ctx.lineWidth = 4;
        ctx.strokeRect(o.posX,o.posY,o.anchoX,o.anchoY);
        
    	ctx.fillStyle=o.color;
        ctx.fillRect(o.posX,o.posY,o.anchoX,o.anchoY);
        
        ctx.fillStyle="#000";
        wrapText(ctx,o.texto,o.posX+margenDerecho,o.posY+margenSuperior,o.anchoX-margenIzquierdo,tamInterlineado);
        
        if((o.tipo=='M')&&(o.actividades!=''))
 		{
        	
        	var oMedio=	{}
	        oMedio.texto=o.actividades;
            oMedio.posX=o.posX;
            oMedio.posY=o.posYFinal+separacionInterBloque;
            oMedio.anchoX=tamBloque;
            oMedio.id=0;
            oMedio.anchoY=maxYActividades;
            oMedio.posXFinal= oMedio.posX + oMedio.anchoX;
            oMedio.posYFinal=oMedio.posY + oMedio.anchoY;
            
            oMedio.color='#F3F393';
            oMedio.tipo='A';
            
            dibujarObjCanvas(oMedio,ctx);
            var posIniX=oMedio.posX+(oMedio.anchoX/2);
            var posY=o.posYFinal;
            var posYFinal=oMedio.posY;
            dibujaLinea(ctx,posIniX,posY,posIniX,posYFinal,colorLinea);
            
        }       
        
    }
}

function dibujarObjCanvasSeleccionado(o,ctx)
{
	if(o)
    {
    	ctx.font=fuente;	
    	window.parent.setObjetoSeleccionado(objSeleccionado);
    	ctx.fillStyle=o.color;
        ctx.fillRect(o.posX,o.posY,o.anchoX,o.anchoY);
        
        ctx.strokeStyle = "#900";
        ctx.lineWidth = 4;
        ctx.strokeRect(o.posX,o.posY,o.anchoX,o.anchoY);
        
	    ctx.fillStyle="#000";
        wrapText(ctx,o.texto,o.posX+margenDerecho,o.posY+margenSuperior,o.anchoX-margenIzquierdo,tamInterlineado);
        
        switch(o.tipo)
        {
        	case 'M':
            	window.parent.habilitarBotonEliminar();
            break;
            case 'F':
            	window.parent.habilitarBotonEliminar();
            break;
            case 'O':
            	if((arrObjMedios.length+arrObjFines.length)==0)
	            	window.parent.habilitarBotonEliminar();
            break;
        }
        
    }
    else
    {
    	window.parent.setObjetoSeleccionado(null);
    }
}

function estimarWrapText(text,maxWidth,lineHeight,context)
{
	var y=0;
    
    var arrReglones=text.split('@@@');
    var x;
    for(x=0;x<arrReglones.length;x++)
    {
    	if(x>0)
        {
        	y += (lineHeight*2);
        }
    	text=arrReglones[x];
        var words = text.split(' ');
        var line = '';
        for(var n = 0; n < words.length; n++) 
        {
            var testLine = line + words[n] + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) 
            {
                line = words[n] + ' ';
                y += lineHeight;
            }
            else 
            {
                line = testLine;
            }
        }
        
        
        
    }
	return y;
}

function wrapText(context,text,x,y,maxWidth,lineHeight)
{
	context.font=fuente;	
	var arrReglones=text.split('@@@');
    var xCt;
    for(xCt=0;xCt<arrReglones.length;xCt++)
    {
    	if(xCt>0)
        {
        	y += (lineHeight*2);
        }
    	text=arrReglones[xCt];
        var words = text.split(' ');
        var line = '';
        for(var n = 0; n < words.length; n++) 
        {
            var testLine = line + words[n] + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) 
            {
                context.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            }
            else 
            {
                line = testLine;
            }
        }
        context.fillText(line, x, y);
    
    }
    return y;
    
    
}

function dibujarConectores(ctx)
{
	var colorLinea='000';
	var diferenciaY;
    var x;
    
	if(arrObjMedios.length>0)
    {
    	if(arrObjMedios.length==1)
        {
        	var posIniX=objObjetivoCentral.posX+(objObjetivoCentral.anchoX/2);
            var posIniY=objObjetivoCentral.posY+objObjetivoCentral.anchoY;
            var posFinX=arrObjMedios[0].posX+(arrObjMedios[0].anchoX/2);
            var posFinY=arrObjMedios[0].posY;
            dibujaLinea(ctx,posIniX,posIniY,posFinX,posFinY,colorLinea);
        }
        else
        {
        	diferenciaY=(arrObjMedios[0].posY-(objObjetivoCentral.posY+objObjetivoCentral.anchoY))/2;
            
					
            var posIniX=arrObjMedios[0].posX+(arrObjMedios[0].anchoX/2);
            var posFinX=arrObjMedios[arrObjMedios.length-1].posX+(arrObjMedios[arrObjMedios.length-1].anchoX/2);
            var posY=arrObjMedios[0].posY-diferenciaY;
           
            dibujaLinea(ctx,posIniX,posY,posFinX,posY,colorLinea);
            
            
            for(x=0;x<arrObjMedios.length;x++)
            {
            	posIniX=arrObjMedios[x].posX+(arrObjMedios[x].anchoX/2);
            	dibujaLinea(ctx,posIniX,posY,posIniX,arrObjMedios[x].posY,colorLinea);
            }
            posIniX=objObjetivoCentral.posX+(objObjetivoCentral.anchoX/2);
            dibujaLinea(ctx,posIniX,posY,posIniX,objObjetivoCentral.posY+objObjetivoCentral.anchoY,colorLinea);
        }
    }

	if(arrObjFines.length>0)
    {
    	if(arrObjFines.length==1)
        {
        	var posIniX=objObjetivoCentral.posX+(objObjetivoCentral.anchoX/2);
            var posIniY=objObjetivoCentral.posY;
            var posFinX=arrObjFines[0].posX+(arrObjFines[0].anchoX/2);
            var posFinY=arrObjFines[0].posY+arrObjFines[0].anchoY;
            dibujaLinea(ctx,posIniX,posIniY,posFinX,posFinY,colorLinea);
        }
        else
        {
        	diferenciaY=(objObjetivoCentral.posY-(arrObjFines[0].posY+arrObjFines[0].anchoY))/2;
            var posIniX=arrObjFines[0].posX+(arrObjFines[0].anchoX/2);
            var posFinX=arrObjFines[arrObjFines.length-1].posX+(arrObjFines[arrObjFines.length-1].anchoX/2);
            var posY=objObjetivoCentral.posY-diferenciaY;
           
            dibujaLinea(ctx,posIniX,posY,posFinX,posY,colorLinea);
            
            for(x=0;x<arrObjFines.length;x++)
            {
            	posIniX=arrObjFines[x].posX+(arrObjFines[x].anchoX/2);
            	dibujaLinea(ctx,posIniX,posY,posIniX,(arrObjFines[x].posY+arrObjFines[x].anchoY),colorLinea);
            }
            posIniX=objObjetivoCentral.posX+(objObjetivoCentral.anchoX/2);
            dibujaLinea(ctx,posIniX,posY,posIniX,objObjetivoCentral.posY,colorLinea);
        }
    }
	
}

function dibujaLinea(ctx,posXIni,posYIni,posXFin,posYFin,color)
{
	ctx.moveTo(posXIni,posYIni);
    ctx.lineTo(posXFin,posYFin);
    ctx.strokeStyle = "#"+(color?color:'000');
    ctx.stroke();

}

function inicializarEventoClickCanvas(e)
{
    
    var  elemLeft = e.offsetLeft,
         elemTop = e.offsetTop,
         context = e.getContext('2d'),
         elements = [];
    

    e.addEventListener('click', function(event) 
                                {
                                	window.parent.desHabilitarBotonEliminar();
                                	dibujarObjCanvas(objSeleccionado,contextCanvas);
                                	objSeleccionado=null;
                                	var encontrado=false;
                                   	var xVal = event.pageX - elemLeft,
                                   	yVal = event.pageY - elemTop;
                                   	var x;
                                   	var o;
                                   	for(x=0;x<arrObjetos.length;x++)
                                   	{
                                   		o=arrObjetos[x];
                                   		if((xVal>=o.posX) &&(xVal<=o.posXFinal))
                                        {
                                        	if((yVal>=o.posY) &&(yVal<=o.posYFinal))
                                            {
                                            	objSeleccionado=o;
                                            	encontrado=true;
                                                
                                                dibujarObjCanvasSeleccionado(objSeleccionado,contextCanvas);
                                            	break;
                                            }
                                        }
                                   	}
                                    
                                    
                                    
                                   
                                }, false);
                                
                                
	
    e.addEventListener('dblclick', function(event) 
                                {
                                	window.parent.desHabilitarBotonEliminar();
                                	dibujarObjCanvas(objSeleccionado,contextCanvas);
                                	objSeleccionado=null;
                                	var encontrado=false;
                                   	var xVal = event.pageX - elemLeft,
                                   	yVal = event.pageY - elemTop;
                                   	var x;
                                   	var o;
                                   	for(x=0;x<arrObjetos.length;x++)
                                   	{
                                   		o=arrObjetos[x];
                                   		if((xVal>=o.posX) &&(xVal<=o.posXFinal))
                                        {
                                        	if((yVal>=o.posY) &&(yVal<=o.posYFinal))
                                            {
                                            	objSeleccionado=o;
                                            	encontrado=true;
                                                dibujarObjCanvasSeleccionado(objSeleccionado,contextCanvas);
                                                window.parent.editarObjeto(objSeleccionado);
                                            }
                                        }
                                   	}
                                    
                                    
                                    
                                   
                                }, false);
                                    
}
      
function borrarMapa(idEditado,tNodo)
{
	var canvas = document.getElementById("myCanvas");
	var ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	gE('idNodoEditado').value=idEditado;
    gE('tipoNodo').value=tipoNodo;
    
} 

function obtenerInformacionArbol(idEditado,tNodo)
{
    function funcAjax(peticion_http)
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            
                var obj=eval('['+arrResp[1]+']')[0];
                arrMedios=obj.arrMedios;
                nMedios=arrMedios.length;
                arrFines=obj.arrFines;
                nFines=arrFines.length;
                idObjetivoCentral=obj.arrObjetivo.length>0?obj.arrObjetivo[0][0]:'-1';
                objetivoCentral=obj.arrObjetivo.length>0?obj.arrObjetivo[0][1]:'';
                borrarMapa(idEditado,tNodo);
                dibujarMapa();
                if(idObjetivoCentral=='-1')
                {
                	window.parent.sinObjetivoCentral();
                }
                else
                {
                	window.parent.conObjetivoCentral();
                }
             
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWebV2('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=52&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value,true);	
}