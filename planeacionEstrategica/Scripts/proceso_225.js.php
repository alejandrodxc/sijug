<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["iF"];
	$idRegistro=$_GET["iR"];
	
	$consulta="SELECT codigoInstitucion FROM _539_tablaDinamica WHERE id__539_tablaDinamica=".$idRegistro;
	$cveArea=$con->obtenerValor($consulta);
?>
var cveArea=<?php echo $cveArea?>;
var arrFrecuenciaMedicion=[['1','Mensual'],['2','Bimestral'],['3','Trimestral'],['4','Cuatrimestral'],['6','Semestral'],['12','Anual']];
function mostrarVentanaImportarIndicador()
{
	
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			crearGridIndicadoresImportacion()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Importar Indicadores',
										width: 950,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var filas=gEx('gGridIndicadorImportacion').getSelectionModel().getSelections();
                                                                        
                                                                        var x;
                                                                        var f;
                                                                        var listaIndicadores='';
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	f=filas[x];
                                                                            
                                                                            if(listaIndicadores=='')
                                                                            {
                                                                            	listaIndicadores=f.data.idIndicador;
                                                                            }
                                                                            else
                                                                            	listaIndicadores+=','+f.data.idIndicador;
                                                                            
                                                                        }
                                                                        
                                                                        if(listaIndicadores=='')
                                                                        {	msgBox('Debe seleccionar almenos un indicador a importar');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idRegistro":"<?php echo $idRegistro?>","listaIndicadores":"'+listaIndicadores+'"}';
                                                                        
                                                                        
                                                                        function funcAjax(peticion_http)
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('frameGrid_550').getFrameWindow().recargarContenedorCentral();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWebV2('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=57&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridIndicadoresImportacion()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idIndicador'},
                                                        {name:'nombreIndicador'},
                                                        {name: 'objetivoIndicador'},
                                                        {name: 'ciclosUso'},
                                                        {name: 'frecuenciaMedicion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                 
                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreIndicador', direction: 'ASC'},
                                                            groupField: 'nombreIndicador',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='55';
                                        proxy.baseParams.cveArea=cveArea;
                                    }
                        )   
    
    var chkRow=new Ext.grid.CheckboxSelectionModel();   
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
                                                            header:'Nombre del indicador',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'nombreIndicador',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	meta.attr='style=height:auto;min-height:21px;white-space:normal;';
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                        },
                                                        {
                                                            header:'Objetivo del Indicador',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'objetivoIndicador',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	meta.attr='style=height:auto;min-height:21px;white-space:normal;';
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                        },
                                                        {
                                                            header:'Frecuencia de medición',
                                                            width:150,
                                                            sortable:true,
                                                            dataIndex:'frecuenciaMedicion',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFrecuenciaMedicion,val);
                                                                    }
                                                        },
                                                        
                                                         
                                                        {
                                                            header:'Usado en',
                                                            width:150,
                                                            sortable:true,
                                                            dataIndex:'ciclosUso',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '--';
                                                                    	return val;
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gGridIndicadorImportacion',
                                                            store:alDatos,
                                                            x:0,
                                                            y:0,
                                                            frame:false,
                                                            border:true,
                                                            cm: cModelo,
                                                            height:370,
                                                            width:920,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            sm:chkRow,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}