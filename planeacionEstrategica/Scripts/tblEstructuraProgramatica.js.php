<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idPlanInstitucional,nombrePlan FROM 3200_planesInstitucionales";
	$arrPlanesInstitucionales=$con->obtenerFilasArreglo($consulta);
	
	
	$arrCiclosFiscales="";
	for($x=date("Y");$x<=date("Y")+3;$x++)
	{
		if($arrCiclosFiscales=="")
			$arrCiclosFiscales="['".$x."','".$x."']";
		else
			$arrCiclosFiscales.=",['".$x."','".$x."']";
	}
	
	$consulta="SELECT idRegistro,nombre FROM 3500_clasificadoresObjetosGasto ORDER BY  nombre";
	$arrClasificadoresObjetosGasto=$con->obtenerFilasArreglo($consulta);
?>
var arrClasificadoresObjetosGasto=<?php echo $arrClasificadoresObjetosGasto ?>;
var arrCiclosFiscales=[<?php echo $arrCiclosFiscales?>];
var arrPlanesInstitucionales=<?php echo $arrPlanesInstitucionales?>;


var subFuncionSel=null;
var ignorarCheck=false;
var arrActividades=[];
var arrProgramasPresupuestarios=[];
var arrGrupoActividades=[];
var nodoSel;


Ext.onReady(inicializar);

function inicializar()
{
	new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px;"><b><span id="lblEstructura">'+bD(gE('nombreEstructura').value)+'</span></b></span>',
                                                tbar:	[	
                                                			{
                                                                icon:'../images/salir.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Cerrar edici&oacute;n del estructura',
                                                                handler:function()
                                                                        {
                                                                            regresarPagina();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/pencil.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Editar datos de estructura',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaCrearEstructuraProgramatica();
                                                                        }
                                                                
                                                            },'-',
                                                			{
                                                                icon:'../images/Icono_txt.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Administraci&oacute;n de cat&aacute;logos',
                                                                menu:	[
                                                                			{
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Finalidades (FI) - Funciones (F) - Sub Funciones (SF)',
                                                                                handler:function()
                                                                                        {
                                                                                           mostrarVentanaCatalogoFinalidades();
                                                                                        }
                                                                                
                                                                            },
                                                                             {
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Grupos de Actividades Institucionales',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaGruposActividadesInstitucionales();  
                                                                                        }
                                                                                
                                                                            },
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Actividades Institucionales (AI)',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaActividadesInstitucionales();  
                                                                                        }
                                                                                
                                                                            },
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Programas Presupuestarios (Pp)',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaProgramasPresupuestarios();  
                                                                                        }
                                                                                
                                                                            }
                                                                            ,
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                text:'SubProgramas Presupuestarios (SPp)',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaSubProgramasPresupuestarios();  
                                                                                        }
                                                                                
                                                                            }
                                                                		]
                                                                
                                                            }
                                                			
                                                		],
                                                items:	[
                                                            crearGridEstructura()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridEstructura()
{
	 var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				
                                                                        {type: 'string', dataIndex: 'finalidad'},
                                                                        {type: 'string', dataIndex: 'programaPresupuestario'},
                                                                        {type: 'string', dataIndex: 'funcion'},
                                                                        {type: 'string', dataIndex: 'subfuncion'},
                                                                        {type: 'string', dataIndex: 'actividadInstitucional'},
                                                                        {type: 'string', dataIndex: 'subProgramaPresupuestario'}
                                                                    ]
                                                    }
                                                ); 
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [	
                                            			{name: 'idRegistroID'},
                                               			{name:'idRegistro'},
		                                                {name: 'finalidad'},
		                                                {name:'funcion'},
		                                                {name: 'subfuncion'},
                                                        {name: 'actividadInstitucional'},
                                                        {name: 'programaPresupuestario'},
                                                        {name: 'subProgramaPresupuestario'},
                                                        {name: 'tipoRegistro'},
                                                        {name: 'denominacion'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'programaPresupuestario', direction: 'ASC'},
                                                            groupField: 'programaPresupuestario',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='34';
                                        proxy.baseParams.idEstructura=gE('idEstructura').value;
                                    }
                        )   
       
	alDatos.on('load',function(proxy)
    								{
                                    	arrGrupoActividades=proxy.reader.jsonData.arrGrupoActividades;
                                        arrActividades=proxy.reader.jsonData.arrActividades;
                                        arrProgramasPresupuestarios=proxy.reader.jsonData.arrProgramasPresupuestarios;
                                    }
                        )        
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'',
                                                            width:50,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'idRegistroID',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(registro.data.tipoRegistro=='6')
                                                                        {
                                                                        	return '<a href="javascript:removerSubPrograma(\''+bE(registro.data.idRegistroID)+'\')"><img width="13" height="13" src="../images/delete.png" title="Remover SubPrograma Presupuestario"/></a>&nbsp;&nbsp;&nbsp;'+
                                                                            		'<a href="javascript:modificarSubPrograma(\''+bE(registro.data.idRegistroID)+'\')"><img width="13" height="13" src="../images/pencil.png" title="Modificar SubPrograma Presupuestario"/></a>'
                                                                        }
                                                                    }
                                                        },
                                                        {
                                                            header:'FI',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'finalidad',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        {
                                                            header:'F',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'funcion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        {
                                                            header:'SF',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'subfuncion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        
                                                        {
                                                            header:'AI',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'actividadInstitucional',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        {
                                                            header:'Pp',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'programaPresupuestario',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        {
                                                            header:'SPp',
                                                            width:70,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'subProgramaPresupuestario',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        },
                                                        {
                                                            header:'Denominaci&oacute;n',
                                                            width:700,
                                                            sortable:true,
                                                            dataIndex:'denominacion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearFilaEstructura(val,registro);
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gEstructuraPresupuestal',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            plugins:[filters],
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}

function mostrarVentanaCatalogoFinalidades()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'border',
											defaultType: 'label',
                                            tbar:	[
                                                        {
                                                            icon:'../images/add.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAddFI',
                                                            text:'Agregar Finalidad (FI)',
                                                            handler:function()
                                                                    {
                                                                        mostrarVentanaFinalidad();
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/especial.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAddF',
                                                            disabled:true,
                                                            text:'Agregar Funci&oacute;n (F)',
                                                            handler:function()
                                                                    {
                                                                        mostrarVentanaFuncion();
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/area.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAddSF',
                                                            disabled:true,
                                                            text:'Agregar Sub Funci&oacute;n (SF)',
                                                            handler:function()
                                                                    {
                                                                        mostrarVentanaSubFuncion();
                                                                    }
                                                            
                                                        }
                                                        ,'-',
                                                        {
                                                            icon:'../images/pencil.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnEdicion',
                                                            disabled:true,
                                                            text:'Editar elemento',
                                                            handler:function()
                                                                    {
                                                                        switch(nodoSel.attributes.tipoNodo)
                                                                        {
                                                                            case '1':
                                                                                mostrarVentanaFinalidad(nodoSel);
                                                                            break;
                                                                             case '2':
                                                                                mostrarVentanaFuncion(nodoSel);
                                                                            break;
                                                                            case '3':
                                                                            	mostrarVentanaSubFuncion(nodoSel);
                                                                            break;
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/delete.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnRemoverElemento',
                                                            disabled:true,
                                                            text:'Remover elemento',
                                                            handler:function()
                                                                    {
                                                                        function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                var arrElemento=nodoSel.id.split('_');
                                                                                
                                                                                var cadObj='{"tipoElemento":"'+arrElemento[0]+'","idElemento":"'+arrElemento[1]+'"}';                                                                                    
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('tFinalidad').getRootNode().reload();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=9&cadObj='+cadObj,true);
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                    }
                                                            
                                                        }
                                                        
                                                        
                                                    ],
											items: 	[
                                            			crearGridCatalogoFinalidades()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cat&aacute;logo de finalidades(FI) - Funciones(F) - SubFunciones(SF)',
										width: 900,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridCatalogoFinalidades()
{
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
    var cargadorArbol=new Ext.tree.TreeLoader(
                                                    {
                                                        baseParams:{
                                                                        funcion:'17',
                                                                        idEstructura:gE('idEstructura').value
                                                                    },
                                                        dataUrl:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                        uiProviders:	{
                                                                            'col': Ext.ux.tree.ColumnNodeUI
                                                                        }
                                                    }	


                                             )		                                        
    
    cargadorArbol.on('beforeLoad',function(tl)
                                    {
                                        
                                        nodoSel=null;
                                        gEx('btnAddF').disable();
                                        gEx('btnAddSF').disable();
                                        gEx('gEstructuraPresupuestal').getStore().reload();
                                    }
                    )

    
                                    
    var organigrama = new Ext.ux.tree.TreeGrid	(
                                                        {
                                                            id:'tFinalidad',
                                                            region:'center',
                                                            useArrows:true,
                                                            autoScroll:false,
                                                            animate:true,
                                                            enableDD:false,
                                                            containerScroll: true,
                                                            root:raiz,
                                                            enableSort:false,
                                                            loader: cargadorArbol,
                                                            rootVisible:false,
                                                            draggable:false,
                                                            columns:[
                                                                        
                                                                        {
                                                                            header:'',
                                                                            width:100,
                                                                            dataIndex:'text'
                                                                        },
                                                                        {
                                                                            header:'Clave',
                                                                            width:60,
                                                                            dataIndex:'lblClave'
                                                                        }
                                                                        ,
                                                                        {
                                                                            header:'Denominaci&oacute;n',
                                                                            width:350,
                                                                            dataIndex:'detalle'
                                                                        },
                                                                        {
                                                                            header:'Descripci&oacute;n',
                                                                            width:800,
                                                                            dataIndex:'descripcion'
                                                                        }
                                                                     ]

                                                           
                                                        }
                                                );
    
    
   

    
    
    organigrama.on('click',nodoClick);
    
    return organigrama;
	
}

function nodoClick(nodo)
{
	nodoSel=nodo;
	gEx('btnAddF').disable();
    gEx('btnAddSF').disable();
    
    switch(nodo.attributes.tipoNodo)
    {
    	case '1':
        	gEx('btnAddF').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        case '2':
        	gEx('btnAddSF').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
        break;
        case '3':
        	gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        
    }
}

function mostrarVentanaFinalidad(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. finalidad:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'cveFinalidad',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:''
                                                        }
                                                        ,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Finalidad:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'finalidad',
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            width:440
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Finalidad:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            id:'txtDescripcion',
                                                            xtype:'textarea',
                                                            width: 540,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            height: 80
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar finalidad':'Modificar finalidad',
										width: 600,
										height:290,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cveFinalidad').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var cveFinalidad=gEx('cveFinalidad');
                                                                        var finalidad=gEx('finalidad');
                                                                        
                                                                        if(cveFinalidad.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cveFinalidad.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la finalidad',resp1);
                                                                            
                                                                            return;
                                                                        }
                                                                        
                                                                        if(finalidad.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	finalidad.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la finalidad a agregar',resp2);
                                                                            
                                                                            return;
                                                                        }
                                                                     	var idRegistro=-1;
                                                                        if(fila)
                                                                        	idRegistro=fila.attributes.idRegistro;
                                                                        var cadObj='{"idRegistro":"'+idRegistro+'","cveFinalidad":"'+cv(cveFinalidad.getValue())+
                                                                        			'","finalidad":"'+cv(finalidad.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                    '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'"}' ;
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('tFinalidad').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=18&cadObj='+cadObj,true);
                                                                        
                                                                                    
                                                                                    
                                                                                    
                                                                                    
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaFuncion(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'cveFuncion',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:''
                                                        }
                                                        ,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'funcion',
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            width:430
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            id:'txtDescripcion',
                                                            xtype:'textarea',
                                                            width: 540,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            height: 80
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar funci&oacute;n':'Modificar funci&oacute;n',
										width: 600,
										height:290,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cveFuncion').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var cveFuncion=gEx('cveFuncion');
                                                                        var funcion=gEx('funcion');
                                                                        
                                                                        if(cveFuncion.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cveFuncion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la funci&oacute;n',resp1);
                                                                            
                                                                            return;
                                                                        }
                                                                        
                                                                        if(funcion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	funcion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la funci&oacute;n a agregar',resp2);
                                                                            
                                                                            return;
                                                                        }
                                                                     	var idRegistro=-1;
                                                                        if(fila)
                                                                        	idRegistro=fila.attributes.idRegistro;
                                                                        var cadObj='{"idRegistro":"'+idRegistro+'","cveFuncion":"'+cv(cveFuncion.getValue())+
                                                                        			'","funcion":"'+cv(funcion.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                    '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'","idFinalidad":"'+nodoSel.attributes.idRegistro+'"}' ;
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('tFinalidad').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=21&cadObj='+cadObj,true);
                                                                        
                                                                                    
                                                                                    
                                                                                    
                                                                                    
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaSubFuncion(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. Sub Funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'cveSubFuncion',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:''
                                                        }
                                                        ,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Sub Funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'subFuncion',
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            width:430
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Sub Funci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            id:'txtDescripcion',
                                                            xtype:'textarea',
                                                            width: 540,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            height: 80
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar sub funci&oacute;n':'Modificar sub funci&oacute;n',
										width: 600,
										height:290,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cveSubFuncion').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var cveSubFuncion=gEx('cveSubFuncion');
                                                                        var subFuncion=gEx('subFuncion');
                                                                        
                                                                        if(cveSubFuncion.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cveSubFuncion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la sub funci&oacute;n',resp1);
                                                                            
                                                                            return;
                                                                        }
                                                                        
                                                                        if(subFuncion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	subFuncion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la sub funci&oacute;n a agregar',resp2);
                                                                            
                                                                            return;
                                                                        }
                                                                     	var idRegistro=-1;
                                                                        if(fila)
                                                                        	idRegistro=fila.attributes.idRegistro;
                                                                        var cadObj='{"idRegistro":"'+idRegistro+'","cveSubFuncion":"'+cv(cveSubFuncion.getValue())+
                                                                        			'","subFuncion":"'+cv(subFuncion.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                    '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'","idFuncion":"'+nodoSel.attributes.idRegistro+'"}' ;
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('tFinalidad').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=23&cadObj='+cadObj,true);
                                                                        
                                                                                    
                                                                                    
                                                                                    
                                                                                    
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaActividadesInstitucionales()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'border',
											defaultType: 'label',
                                            tbar:	[
                                                        {
                                                            icon:'../images/add.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAddAI',
                                                            text:'Agregar Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                        mostrarVentanaActividadInstitucional();
                                                                    }
                                                            
                                                        },'-',
                                                         {
                                                            icon:'../images/pencil.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnEditarAI',
                                                            text:'Modificar Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                    	var fila=gEx('gActividadesInstitucionales').getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar la Actividad Institucional que desea modificar');
                                                                        	return;
                                                                        }
                                                                        mostrarVentanaActividadInstitucional(fila);
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/delete.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnRemoverAI',
                                                            disabled:true,
                                                            text:'Remover Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                    	var fila=gEx('gActividadesInstitucionales').getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar la Actividad Institucional que desea remover');
                                                                        	return;
                                                                        }
                                                                        function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gActividadesInstitucionales').getStore().reload();
                                                                                		arrActividades=eval(arrResp[1]);		
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=28&idActividad='+
                                                                                			fila.data.idRegistro+'&idEstructura='+gE('idEstructura').value,true);
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                    }
                                                            
                                                        }
                                                        
                                                        
                                                    ],
											items: 	[
                                            			crearGridActividadesInstitucionales()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cat&aacute;logo de Actividades Institucionales (AI)',
										width: 900,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridActividadesInstitucionales()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
		                                                {name: 'finalidad'},
		                                                {name:'funcion'},
		                                                {name: 'subfuncion'},
                                                        {name: 'grupoActividad'},
                                                        {name: 'cveActividad'},
                                                        {name: 'actividadInstitucional'},
                                                        {name: 'idSubFuncion'},
                                                        {name: 'objetivoActividadInstitucional'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'cveActividad', direction: 'ASC'},
                                                            groupField: 'grupoActividad',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('gEstructuraPresupuestal').getStore().reload();
                                    	proxy.baseParams.funcion='22';
                                        proxy.baseParams.idEstructura=gE('idEstructura').value;
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        
                                                        {
                                                            header:'FI',
                                                            width:40,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'finalidad'
                                                        },
                                                        {
                                                            header:'F',
                                                            width:40,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'funcion'
                                                        },
                                                        {
                                                            header:'SF',
                                                            width:40,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'subfuncion'
                                                        },
                                                        
                                                        {
                                                            header:'Cve. Actividad',
                                                            width:120,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'cveActividad'
                                                        },
                                                      
                                                        {
                                                            header:'Denominaci&oacute;n',
                                                            width:600,
                                                            sortable:true,
                                                            dataIndex:'actividadInstitucional'
                                                        },
                                                        {
                                                            header:'Grupo de actividad',
                                                            width:120,
                                                            align:'center',
                                                            sortable:true,
                                                            dataIndex:'grupoActividad',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrGrupoActividades,val);
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gActividadesInstitucionales',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :true,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: true,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}

function mostrarVentanaActividadInstitucional(fila)
{
	subFuncionSel=null;
	var cmbGrupoActividades=crearComboExt('cmbGrupoActividades',arrGrupoActividades,150,5,450);
    if(fila)
    {
    	cmbGrupoActividades.setValue(fila.data.grupoActividad);
        subFuncionSel=fila.data.idSubFuncion;


    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Grupo de Actividades:'
                                                        },
                                                        cmbGrupoActividades,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Clave de la Actividad:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:80,
                                                            value:fila?fila.data.cveActividad:'',
                                                            id:'claveActividad'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Actividad Institucional:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:550,
                                                            height:40,
                                                            value:fila?escaparBR(fila.data.actividadInstitucional,true):'',
                                                            id:'actividadInstitucional'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:120,
                                                            html:'Objetivo de la Actividad:'
                                                        },
                                                         {
                                                        	x:150,
                                                            y:115,
                                                            height:50,
                                                            xtype:'textarea',
                                                            width:550,
                                                            value:fila?escaparBR(fila.data.objetivoActividadInstitucional,true):'',
                                                            id:'objetivoActividadInstitucional'
                                                        },
                                                        crearArbolSubFunciones()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar Actividad Institucional':'Modificar Actividad Institucional',
										width: 750,
										height:470,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    
                                                                    	var claveActividad=gEx('claveActividad');	
                                                                        var actividadInstitucional=gEx('actividadInstitucional');
                                                                        var objetivoActividadInstitucional=gEx('objetivoActividadInstitucional');
                                                                        
                                                                        if(cmbGrupoActividades.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbGrupoActividades.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el grupo al cual pertenece la Actividad Institucional',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(claveActividad.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	claveActividad.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la Actividad Institucional',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(actividadInstitucional.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	actividadInstitucional.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre de la Actividad Institucional',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        var arrNodos=gEx('tFinalidadCheck').getCheckNodes();
                                                                        
                                                                        if(arrNodos.length==0)
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	
                                                                            }
                                                                            msgBox('Debe seleccionar la Sub Funci&oacute;n con cual se asocia la Actividad Institucional',resp4);
                                                                        	return;
                                                                        }
                                                                        
                                                                        var idActividad=-1;
                                                                        if(fila)
                                                                        	idActividad=fila.data.idRegistro;
                                                                        var cadObj='{"idActividad":"'+idActividad+'","claveActividad":"'+cv(claveActividad.getValue())+
                                                                        		'","actividadInstitucional":"'+cv(actividadInstitucional.getValue())+'","objetivoActividadInstitucional":"'+
                                                                                cv(objetivoActividadInstitucional.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                '","grupoActividad":"'+cmbGrupoActividades.getValue()+'","subFuncion":"'+
                                                                                arrNodos[0].attributes.idRegistro+'"}';
                                                                        
																		
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gActividadesInstitucionales').getStore().reload();
                                                                                arrActividades=eval(arrResp[1]);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=27&cadObj='+cadObj,true);
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearArbolSubFunciones()
{
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
    var cargadorArbol=new Ext.tree.TreeLoader(
                                                    {
                                                        baseParams:{
                                                                        funcion:'17',
                                                                        vistaCheck:1,
                                                                        idEstructura:gE('idEstructura').value,
                                                                        nodoSel:subFuncionSel?'3_'+subFuncionSel:-1
                                                                    },
                                                        dataUrl:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                        uiProviders:	{
                                                                            'col': Ext.ux.tree.ColumnNodeUI
                                                                        }
                                                    }	


                                             )		                                        
    
   
                                    
    var organigrama = new Ext.ux.tree.TreeGrid	(
                                                        {
                                                            id:'tFinalidadCheck',
                                                            x:10,
                                                            y:180,
                                                            title:'Sub Funci&oacute;n Asociada:',
                                                            width:690,
                                                            height:200,
                                                            useArrows:true,
                                                            autoScroll:false,
                                                            animate:true,
                                                            enableDD:false,
                                                            containerScroll: true,
                                                            root:raiz,
                                                            enableSort:false,
                                                            loader: cargadorArbol,
                                                            rootVisible:false,
                                                            listeners:	{
                                                            				checkchange:function( nodo, checked )
                                                                            			{
                                                                                        	if((checked)&&(!ignorarCheck))
                                                                                            {
                                                                                            	checarNodosHijos(gEx('tFinalidadCheck').getRootNode(),false);
                                                                                                ignorarCheck=true;
                                                                                                nodo.getUI().toggleCheck(true);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                            	ignorarCheck=false;
                                                                                            }
                                                                                            
                                                                                        }
                                                            			},
                                                            		
                                                            draggable:false,
                                                            columns:[
                                                                        
                                                                        {
                                                                            header:'',
                                                                            width:120,
                                                                            dataIndex:'text'
                                                                        },
                                                                        {
                                                                            header:'Clave',
                                                                            width:55,
                                                                            dataIndex:'lblClave'
                                                                        }
                                                                        ,
                                                                        {
                                                                            header:'Denominaci&oacute;n',
                                                                            width:350,
                                                                            dataIndex:'detalle'
                                                                        }
                                                                     ]

                                                           
                                                        }
                                                );
    
    
   

    
    
    return organigrama;
	
}

function mostrarVentanaGruposActividadesInstitucionales()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'border',
											defaultType: 'label',
                                            tbar:	[
                                                        {
                                                            icon:'../images/add.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAddAI',
                                                            text:'Agregar Grupo de Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                        mostrarVentanaGrupoActividadInstitucional();
                                                                    }
                                                            
                                                        },'-',
                                                         {
                                                            icon:'../images/pencil.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnEditarAI',
                                                            text:'Modificar Grupo de Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                    	var fila=gEx('gGruposActividadesInstitucionales').getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el Grupo de Actividad Institucional que desea modificar');
                                                                        	return;
                                                                        }
                                                                        mostrarVentanaGrupoActividadInstitucional(fila);
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/delete.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnRemoverAI',
                                                            text:'Remover Grupo de Actividad Institucional',
                                                            handler:function()
                                                                    {
                                                                    	var fila=gEx('gGruposActividadesInstitucionales').getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el Grupo de Actividad Institucional que desea remover');
                                                                        	return;
                                                                        }
                                                                        function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	arrGrupoActividades=eval(arrResp[1]);
                                                                                        gEx('gGruposActividadesInstitucionales').getStore().reload();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=26&iGA='+
                                                                                			fila.data.idRegistro+'&idEstructura='+gE('idEstructura').value,true);
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer remover el Grupo de Actividad Institucional seleccionado seleccionado?',resp);
                                                                    }
                                                            
                                                        }
                                                        
                                                        
                                                    ],
											items: 	[
                                            			crearGridGruposActividadesInstitucionales()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cat&aacute;logo de Grupos Actividades Institucionales',
										width: 800,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridGruposActividadesInstitucionales()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
		                                                {name: 'cveGrupoActividad'},
		                                                {name:'grupoActividad'},
		                                                {name: 'objetivoActividad'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'cveGrupoActividad', direction: 'ASC'},
                                                            groupField: 'cveGrupoActividad',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                             	        gEx('gEstructuraPresupuestal').getStore().reload();
                                    	proxy.baseParams.funcion='24';
                                        proxy.baseParams.idEstructura=gE('idEstructura').value;
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        
                                                        {
                                                            header:'Cve. Grupo',
                                                            width:100,
                                                            sortable:true,
                                                            dataIndex:'cveGrupoActividad'
                                                        },
                                                        
                                                        {
                                                            header:'Nombre Grupo',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'grupoActividad'
                                                        },                                                        
                                                        
                                                        {
                                                            header:'Objetivo del Grupo',
                                                            width:600,
                                                            sortable:true,
                                                            dataIndex:'objetivoActividad'
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gGruposActividadesInstitucionales',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}

function mostrarVentanaGrupoActividadInstitucional(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            html:'Clave del Grupo:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:80,
                                                            value:fila?fila.data.cveGrupoActividad:'',
                                                            id:'claveGrupo'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del Grupo:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:455,
                                                            value:fila?fila.data.grupoActividad:'',
                                                            id:'nombreGrupo'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Objetivo del Grupo:'
                                                        },
                                                         {
                                                        	x:150,
                                                            y:65,
                                                            height:50,
                                                            xtype:'textarea',
                                                            width:550,
                                                            value:fila?escaparBR(fila.data.objetivoActividad,true):'',
                                                            id:'objetivoGrupo'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar Grupo de Actividad Institucional':'Modificar Grupo de Actividad Institucional',
										width: 750,
										height:230,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('claveGrupo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',                                                            
															handler: function()
																	{
																		var claveGrupo=gEx('claveGrupo');	
                                                                        var nombreGrupo=gEx('nombreGrupo');
                                                                        var objetivoGrupo=gEx('objetivoGrupo');
                                                                        
                                                                        if(claveGrupo.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	claveGrupo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave del grupo',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(nombreGrupo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	nombreGrupo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del grupo',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        var idGrupo=-1;
                                                                        if(fila)
                                                                        	idGrupo=fila.data.idRegistro;
                                                                        var cadObj='{"idGrupo":"'+idGrupo+'","claveGrupo":"'+cv(claveGrupo.getValue())+
                                                                        		'","nombreGrupo":"'+cv(nombreGrupo.getValue())+'","objetivoGrupo":"'+
                                                                                cv(objetivoGrupo.getValue())+'","idEstructura":"'+gE('idEstructura').value+'"}';
                                                                        
																		
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gGruposActividadesInstitucionales').getStore().reload();
                                                                                arrGrupoActividades=eval(arrResp[1]);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=25&cadObj='+cadObj,true);
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaProgramasPresupuestarios()
{

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'border',
											defaultType: 'label',
                                            tbar:	[
                                            			{
                                                            icon:'../images/add.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnAdd',
                                                            text:'Agregar...',
                                                            menu: 	[
                                                            			{
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddM',                                                                            
                                                                            text:'Agregar Modalidad (M)',
                                                                            handler:function()
                                                                                    {
                                                                                        mostrarVentanaAgregarModalidad();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddT',
                                                                            disabled:true,
                                                                            text:'Agregar Tipo (T)',
                                                                            handler:function()
                                                                                    {
                                                                                        mostrarVentanaAgregarTipo();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddPP',
                                                                            disabled:true,
                                                                            text:'Agregar Programa presupuestario (PP)',
                                                                            handler:function()
                                                                                    {
                                                                                        mostrarVentanaProgramaPresupuestario();
                                                                                    }
                                                                            
                                                                        }
                                                            		]
                                                            
                                                        },
                                                        '-',
                                                        {
                                                            icon:'../images/pencil.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnEdicion',
                                                            disabled:true,
                                                            text:'Editar elemento',
                                                            handler:function()
                                                                    {
                                                                        switch(nodoSel.attributes.tipoNodo)
                                                                        {
                                                                            case '1':
                                                                                mostrarVentanaAgregarModalidad(nodoSel);
                                                                            break;
                                                                             case '2':
                                                                                mostrarVentanaAgregarTipo(nodoSel);
                                                                            break;
                                                                            case '3':
                                                                            	mostrarVentanaProgramaPresupuestario(nodoSel);
                                                                            break;
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                            
                                                        },'-',
                                                        {
                                                            icon:'../images/delete.png',
                                                            cls:'x-btn-text-icon',
                                                            id:'btnRemoverElemento',
                                                            disabled:true,
                                                            text:'Remover elemento',
                                                            handler:function()
                                                                    {
                                                                        function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                var arrElemento=nodoSel.id.split('_');
                                                                                
                                                                                var cadObj='{"tipoElemento":"'+arrElemento[0]+'","idElemento":"'+arrElemento[1]+'"}';                                                                                    
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('tProgramas').getRootNode().reload();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=35&cadObj='+cadObj,true);
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                    }
                                                            
                                                        }
                                                        
                                                        
                                                    ],
											items: 	[
                                            			crearGridCatalogoProgramasPresupuestarios()
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cat&aacute;logo de Programas Presupuestarios (PP)',
										width: 900,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridCatalogoProgramasPresupuestarios()
{
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
    var cargadorArbol=new Ext.tree.TreeLoader(
                                                    {
                                                        baseParams:{
                                                                        funcion:'30',
                                                                        idEstructura:gE('idEstructura').value
                                                                    },
                                                        dataUrl:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                        uiProviders:	{
                                                                            'col': Ext.ux.tree.ColumnNodeUI
                                                                        }
                                                    }	


                                             )		                                        
    
    cargadorArbol.on('beforeLoad',function(tl)
                                    {
                                        gEx('gEstructuraPresupuestal').getStore().reload();
                                        nodoSel=null;
                                        gEx('btnAddT').disable();
                                        gEx('btnAddPP').disable();

                                        gEx('btnEdicion').disable();
                                        gEx('btnRemoverElemento').disable();
                                        
                                    }
                    )

    
                                    
    var organigrama = new Ext.ux.tree.TreeGrid	(
                                                        {
                                                            id:'tProgramas',
                                                            region:'center',
                                                            useArrows:true,
                                                            autoScroll:false,
                                                            animate:true,
                                                            enableDD:false,
                                                            containerScroll: true,
                                                            root:raiz,
                                                            enableSort:false,
                                                            loader: cargadorArbol,
                                                            rootVisible:false,
                                                            draggable:false,
                                                            columns:[
                                                                        
                                                                        {
                                                                            header:'',
                                                                            width:150,
                                                                            dataIndex:'text'
                                                                        },
                                                                        {
                                                                            header:'Clave',
                                                                            width:60,
                                                                            dataIndex:'lblClave'
                                                                        }
                                                                        ,
                                                                        {
                                                                            header:'Denominaci&oacute;n',
                                                                            width:350,
                                                                            dataIndex:'detalle'
                                                                        },
                                                                        {
                                                                            header:'Descripci&oacute;n',
                                                                            width:800,
                                                                            dataIndex:'descripcion'
                                                                        }
                                                                     ]

                                                           
                                                        }
                                                );
    
    
   

    
    
    organigrama.on('click',nodoClick2);
    
    return organigrama;
	
}

function mostrarVentanaAgregarModalidad(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. Modalidad:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'cveModalidad',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:''
                                                        }
                                                        ,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Modalidad:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'modalidad',
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            width:440
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Modalidad:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            id:'txtDescripcion',
                                                            xtype:'textarea',
                                                            width: 540,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            height: 80
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar Modalidad':'Modificar Modalidad',
										width: 600,
										height:290,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cveModalidad').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var cveModalidad=gEx('cveModalidad');
                                                                        var modalidad=gEx('modalidad');
                                                                        
                                                                        if(cveModalidad.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cveModalidad.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la modalidad',resp1);
                                                                            
                                                                            return;
                                                                        }
                                                                        
                                                                        if(modalidad.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	modalidad.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre de la modalidad',resp2);
                                                                            
                                                                            return;
                                                                        }
                                                                     	var idRegistro=-1;
                                                                        if(fila)
                                                                        	idRegistro=fila.attributes.idRegistro;
                                                                        var cadObj='{"idRegistro":"'+idRegistro+'","cveModalidad":"'+cv(cveModalidad.getValue())+
                                                                        			'","modalidad":"'+cv(modalidad.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                    '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'"}' ;
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('tProgramas').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=29&cadObj='+cadObj,true);
                                                                        
                                                                                    
                                                                                    
                                                                                    
                                                                                    
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaAgregarTipo(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. Tipo:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'cveTipo',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:''
                                                        }
                                                        ,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Tipo:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'tipo',
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            width:440
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Tipo:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            id:'txtDescripcion',
                                                            xtype:'textarea',
                                                            width: 540,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            height: 80
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar Tipo':'Modificar Tipo',
										width: 600,
										height:290,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cveTipo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var cveTipo=gEx('cveTipo');
                                                                        var tipo=gEx('tipo');
                                                                        
                                                                        if(cveTipo.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cveTipo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave del Tipo',resp1);
                                                                            
                                                                            return;
                                                                        }
                                                                        
                                                                        if(tipo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	tipo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del Tipo',resp2);
                                                                            
                                                                            return;
                                                                        }
                                                                     	var idRegistro=-1;
                                                                        if(fila)
                                                                        	idRegistro=fila.attributes.idRegistro;
                                                                        var cadObj='{"idRegistro":"'+idRegistro+'","cveTipo":"'+cv(cveTipo.getValue())+
                                                                        			'","tipo":"'+cv(tipo.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                    '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'","idModalidad":"'+
                                                                                    nodoSel.attributes.idRegistro+'"}' ;
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('tProgramas').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=31&cadObj='+cadObj,true);
                                                                        
                                                                                    
                                                                                    
                                                                                    
                                                                                    
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function nodoClick2(nodo)
{
	nodoSel=nodo;
	gEx('btnAddT').disable();
    gEx('btnAddPP').disable();

    gEx('btnEdicion').disable();
    gEx('btnRemoverElemento').disable();
    
    switch(nodo.attributes.tipoNodo)
    {
    	case '1':
        	gEx('btnAddT').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        case '2':
        	gEx('btnAddPP').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
        break;
        case '3':

        	gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        
    }
}

function mostrarVentanaProgramaPresupuestario(fila)
{

	var cmbActividades=crearComboExt('cmbGrupoActividades',arrActividades,230,5,450);
    if(fila)
    {
    	cmbActividades.setValue(fila.attributes.idActividadInstitucional);
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Actividad Institucional asociada:'
                                                        },
                                                        cmbActividades,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Clave del Programa:'
                                                        },
                                                        {
                                                        	x:230,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:80,
                                                            value:fila?fila.attributes.cveElemento:'',
                                                            id:'clavePrograma'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Programa Presupuestario:'
                                                        },
                                                        {
                                                        	x:230,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:550,
                                                            height:40,
                                                            value:fila?escaparBR(fila.attributes.detalle,true):'',
                                                            id:'programaPresupuestario'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:120,
                                                            html:'Objetivo del Programa Presupuestario:'
                                                        },
                                                         {
                                                        	x:230,
                                                            y:115,
                                                            height:50,
                                                            xtype:'textarea',
                                                            width:550,
                                                            value:fila?escaparBR(fila.attributes.descripcion,true):'',
                                                            id:'objetivoProgramaPresupuestario'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar Programa Presupuestario':'Modificar Programa Presupuestario',
										width: 830,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbActividades').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    
                                                                    	var clavePrograma=gEx('clavePrograma');	
                                                                        var programaPresupuestario=gEx('programaPresupuestario');
                                                                        var objetivoProgramaPresupuestario=gEx('objetivoProgramaPresupuestario');
                                                                        
                                                                        if(cmbActividades.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbActividades.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la Actividad Institucional con la cual se asocia el Programa Prespuestario',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(clavePrograma.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	clavePrograma.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave del Programa Presupuestario',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(programaPresupuestario.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	programaPresupuestario.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del Programa Presupuestario',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        var idProgramaPresupuestario=-1;
                                                                        if(fila)
                                                                        	idProgramaPresupuestario=fila.attributes.idRegistro;
                                                                        var cadObj='{"idProgramaPresupuestario":"'+idProgramaPresupuestario+'","clavePrograma":"'+cv(clavePrograma.getValue())+
                                                                        		'","programaPresupuestario":"'+cv(programaPresupuestario.getValue())+'","objetivoProgramaPresupuestario":"'+
                                                                                cv(objetivoProgramaPresupuestario.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                '","idActividadInstitucional":"'+cmbActividades.getValue()+'","idTipoPrograma":"'+nodoSel.attributes.idRegistro+'"}';
                                                                        
																		
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('tProgramas').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=32&cadObj='+cadObj,true);
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaSubProgramasPresupuestarios(fila)
{
	var cmbProgramasPresupuestarios=crearComboExt('cmbProgramasPresupuestarios',arrProgramasPresupuestarios,230,5,450);
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Programa presupuestario:'
                                                        },
                                                        cmbProgramasPresupuestarios,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Clave del SubPrograma:'
                                                        },
                                                        {
                                                        	x:230,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:80,
                                                            value:'',
                                                            id:'clavePrograma'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Programa Presupuestario:'
                                                        },
                                                        {
                                                        	x:230,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:550,
                                                            height:40,
                                                            value:'',
                                                            id:'programaPresupuestario'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:120,
                                                            html:'Objetivo del Programa Presupuestario:'
                                                        },
                                                         {
                                                        	x:230,
                                                            y:115,
                                                            height:50,
                                                            xtype:'textarea',
                                                            width:550,
                                                            value:'',
                                                            id:'objetivoProgramaPresupuestario'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Agregar SubPrograma Presupuestario':'Modificar SubPrograma Presupuestario',
										width: 830,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbProgramasPresupuestarios').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    
                                                                    	var clavePrograma=gEx('clavePrograma');	
                                                                        var programaPresupuestario=gEx('programaPresupuestario');
                                                                        var objetivoProgramaPresupuestario=gEx('objetivoProgramaPresupuestario');
                                                                        
                                                                        if(cmbProgramasPresupuestarios.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbProgramasPresupuestarios.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar el programa presupuestario con el cual se asocia el SubPrograma Prespuestario',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(clavePrograma.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	clavePrograma.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave del SubPrograma Presupuestario',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(programaPresupuestario.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	programaPresupuestario.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del SubPrograma Presupuestario',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        var idProgramaPresupuestario=-1;
                                                                        if(fila)
                                                                        	idProgramaPresupuestario=fila.data.idRegistroID;
                                                                        var cadObj='{"idSubProgramaPresupuestario":"'+idProgramaPresupuestario+'","clavePrograma":"'+cv(clavePrograma.getValue())+
                                                                        		'","programaPresupuestario":"'+cv(programaPresupuestario.getValue())+'","objetivoProgramaPresupuestario":"'+
                                                                                cv(objetivoProgramaPresupuestario.getValue())+'","idEstructura":"'+gE('idEstructura').value+
                                                                                '","idProgramaPresupuestario":"'+cmbProgramasPresupuestarios.getValue()+'"}';
                                                                        
																		
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gEstructuraPresupuestal').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=33&cadObj='+cadObj,true);
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	if(fila)
		obtenerDatosSubProgramaPresupuestario(fila,ventanaAM);
	else
    	ventanaAM.show();
}


function obtenerDatosSubProgramaPresupuestario(fila,ventanaAM)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	ventanaAM.show();
        	var oDatos=eval(arrResp[1])[0];
            gEx('cmbProgramasPresupuestarios').setValue(oDatos.idProgramaPresupuestario);
            gEx('clavePrograma').setValue(oDatos.cveSubPrograma);
            gEx('programaPresupuestario').setValue(escaparBR(oDatos.subProgramaPresupuestario,true));
            gEx('objetivoProgramaPresupuestario').setValue(escaparBR(oDatos.objetivoSubProgramaPresupuestario,true));
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=37&idSubPrograma='+fila.data.idRegistroID,true);
}
function formatearFilaEstructura(valor,registro)
{
	var color="";
    switch(registro.data.tipoRegistro)
    {
    	case '1':
        	color="900";
        break;
        case '2':
        	color="0C087C";
        break;
        case '3':
        	color="025816";
        break;
        case '4':
        	color="A12D0A";
        break;
        case '5':
        	color="D08402";        
        break;
        case '6':
        	color="000";
        break;
    }
	return '<span style="color: #'+color+'">'+valor+'</span>';
}

function removerSubPrograma(iS)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    gEx('gEstructuraPresupuestal').getStore().reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=36&iP='+bD(iS),true);
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover el subprograma presupuestario?',resp);
}

function modificarSubPrograma(iS)
{
	var pos=obtenerPosFila(gEx('gEstructuraPresupuestal').getStore(),'idRegistroID',bD(iS));
	var fila=gEx('gEstructuraPresupuestal').getStore().getAt(pos);
    mostrarVentanaSubProgramasPresupuestarios(fila);
    
    
}


function mostrarVentanaCrearEstructuraProgramatica(fila)
{
	
	var objEstructura=eval('['+bD(gE('objEstructura').value)+']')[0];
   
    var cmbClasificador=crearComboExt('cmbCiclo',arrClasificadoresObjetosGasto,280,215,400);
    cmbClasificador.setValue(objEstructura.clasificadorObjetoGasto);
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclosFiscales,110,155,120);
    cmbCiclo.on('select',function(cmb,registro)		
    								{
                                    	function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                var arrDatos=eval(arrResp[1]);
                                             	gEx('cmbPlanInstitucional').getStore().loadData(arrDatos);   	
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=14&ciclo='+registro.data.id,true);
                                        
                                        
                                    }
    	    				)
    cmbCiclo.setValue(objEstructura.cicloFiscal);
    var cmbPlanInstitucional=crearComboExt('cmbPlanInstitucional',[],180,185,500);
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre de la estructura:'
                                                        },
                                                        {	
                                                        	x:180,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:400,
                                                            id:'nombreEstructura',
                                                            value:decodeURIComponent(objEstructura.nombreEstructura)
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n de la estructura:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:700,
                                                            height:60,
                                                            xtype:'textarea',
                                                            value:escaparBR(decodeURIComponent(objEstructura.descripcion),true),
                                                            id:'txtDescripcion'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Ciclo fiscal:'
                                                        },
                                                        cmbCiclo,
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            html:'Plan Institucional Asociado:'
                                                        },
                                                        cmbPlanInstitucional,
                                                        {
                                                        	x:10,
                                                            y:220,
                                                            html:'Clasificador Por Objeto de Gasto Asociado:'
                                                        },
                                                        cmbClasificador
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar Estructura program&aacute;tica',
										width: 750,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nombreEstructura').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var nombreEstructura=gEx('nombreEstructura');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        
                                                                        if(nombreEstructura.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	nombreEstructura.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el nombre de la estructura program&aacute;tica',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbCiclo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbCiclo.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el ciclo fiscal al cual pertenece la estructura program&aacute;tica',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbPlanInstitucional.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbPlanInstitucional.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el plan institucional con el cual se asocia la estructura program&aacute;tica',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbClasificador.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	cmbClasificador.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el clasificador por objeto de gasto con cual se asocia la estructura program&aacute;tica',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idEstructura":"'+gE('idEstructura').value+'","nombreEstructura":"'+cv(nombreEstructura.getValue())+'","descripcion":"'+
                                                                        			cv(txtDescripcion.getValue())+'","cicloFiscal":"'+cmbCiclo.getValue()+'","planInstitucional":"'+
                                                                                    cmbPlanInstitucional.getValue()+'","clasificadorObjetoGasto":"'+cmbClasificador.getValue()+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gE('lblEstructura').innerHTML=nombreEstructura.getValue();
                                                                            	gE('objEstructura').value=bE(cadObj);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=15&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            gEx('cmbPlanInstitucional').getStore().loadData(arrDatos);  
            gEx('cmbPlanInstitucional').setValue(objEstructura.planInstitucional); 	
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=14&ciclo='+objEstructura.cicloFiscal,true);	
}
