<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion='11'";
	$arrCentrosGestores=$con->obtenerFilasArreglo($consulta);
	
	$arrRegistros="";
	$consulta="SELECT numEtapa,nombreEtapa FROM 4037_etapas WHERE idProceso=222 ORDER BY numEtapa";
	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$icono="";
		switch($fila[0])
		{
			case 1:
				$icono="control_pause.png";
			break;
			case 2:
				$icono="page_edit.png";
			break;
			case 3:
				$icono="accept_green.png";
			break;
		}
		
		$o="['".$fila[0]."','".cv($fila[0].".- ".$fila[1])."','".$icono."']";
		if($arrRegistros=="")
			$arrRegistros=$o;
		else
			$arrRegistros.=",".$o;
	}
	
?>
var arrStatus=[<?php echo $arrRegistros ?>];
var arrCentrosGestores=<?php echo $arrCentrosGestores?>;
Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                border:false,
                                                items:	[
                                                          crearGridCentrosGestores()   
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridCentrosGestores()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'centroGestor'},
		                                                {name: 'idEstado'},
		                                                {name:'idRegistro'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                 
                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'centroGestor', direction: 'ASC'},
                                                            groupField: 'centroGestor',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='48';
                                        proxy.baseParams.idConvocatoria=gE('idRegistro').value;
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'Centro Gestor',
                                                            width:550,
                                                            sortable:true,
                                                            dataIndex:'centroGestor',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCentrosGestores,val);
                                                                    }
                                                        },
                                                        {
                                                            header:'',
                                                            width:40,
                                                            sortable:true,
                                                            dataIndex:'idRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(parseInt(registro.data.idEstado)>1)
	                                                                    	return '<a href="javascript:mostrarRegistroCentro(\''+bE(val)+'\')"><img src="../images/magnifier.png" title="Ver registro del centro gestor" alt="Ver registro del centro gestor"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'Situaci&oacute;n',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'idEstado',
                                                            renderer:function(val)
                                                            		{
                                                                    	var icono='../images/';
                                                                        var pos=existeValorMatriz(arrStatus,val,0,true);
                                                                        icono+=arrStatus[pos][2];
                                                                    	return '<img width="13" height="13" src="'+icono+'"> '+arrStatus[pos][1];
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gCentrosGestores',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}

function mostrarRegistroCentro(iR)
{
	window.parent.abrirFormularioProcesoFancy(bE(539),(iR),bE(0));
}