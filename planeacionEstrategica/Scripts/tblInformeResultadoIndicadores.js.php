<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=bD($_GET["iF"]);
	$idRegistro=bD($_GET["iR"]);
	
	$consulta="SELECT id__539_tablaDinamica FROM _539_tablaDinamica WHERE idProcesoPadre=223 AND idReferencia=".$idRegistro;
	$listaMarcosTeoricos=$con->obtenerListaValores($consulta);
	
	if($listaMarcosTeoricos=="")
		$listaMarcosTeoricos=-1;
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion='11'";
	$arrCentrosGestores=$con->obtenerFilasArreglo($consulta);
	
	$arrRegistros="";
	$consulta="SELECT numEtapa,nombreEtapa FROM 4037_etapas WHERE idProceso=235 ORDER BY numEtapa";
	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$icono="";
		switch($fila[0])
		{
			case 1:
				$icono="page_edit.png";
			break;
			case 2:
				$icono="accept_green.png";
			break;
			
		}
		
		$o="['".$fila[0]."','".cv($fila[0].".- ".$fila[1])."','".$icono."']";
		if($arrRegistros=="")
			$arrRegistros=$o;
		else
			$arrRegistros.=",".$o;
	}
	
	
	$arrLeyendas[1]="Mes";
	$arrLeyendas[2]="Bimestre";
	$arrLeyendas[3]="Trimestre";
	$arrLeyendas[4]="Cuatrimestre";
	$arrLeyendas[6]="Semestre";
	$arrLeyendas[12]="Anual";	
	
	$arrPosicionOrd[1]="Primer";
	$arrPosicionOrd[2]="Segundo";
	$arrPosicionOrd[3]="Tercer";
	$arrPosicionOrd[4]="Cuarto";
	$arrPosicionOrd[5]="Quinto";
	$arrPosicionOrd[6]="Sexto";
	$arrPosicionOrd[7]="Séptimo";
	$arrPosicionOrd[8]="Octavo";
	$arrPosicionOrd[9]="Noveno";
	$arrPosicionOrd[10]="Décimo";
	$arrPosicionOrd[11]="Décimo primer";
	$arrPosicionOrd[12]="Décimo segundo";
	
	$aPeriodicidad="";
	
	foreach($arrLeyendas as $idLeyenda=>$leyenda)
	{
		$totalPeriodo=12/$idLeyenda;
		$arrPeriodos="";
		
		$consulta="SELECT COUNT(*)  FROM _572_tablaDinamica fr,539_calendarioReportesIndicadores c
				WHERE c.idReferencia IN(".$listaMarcosTeoricos.") AND c.idRegistro=fr.idRegistroCalendario 
				AND c.periodicidad=".$idLeyenda;
		
		$nLeyendas=$con->obtenerValor($consulta);
		
		
		if($nLeyendas==0)
			continue;
		
		for($p=1;$p<=$totalPeriodo;$p++)
		{
			$mesReporte=($idLeyenda*$p)-1;
	
			$mesFinal=$arrMesLetra[$mesReporte];
		
			$mesReporteInicial=$mesReporte-($idLeyenda-2);
			$mesInicial=$arrMesLetra[$mesReporteInicial-1];
			
			
			$lblPeriodo=$arrPosicionOrd[$p]." ".$arrLeyendas[$idLeyenda].($idLeyenda!=1?" (".$mesInicial." - ".$mesFinal.")":" (".$mesInicial.")");
			
			$oPeriodo="['".$p."','".$lblPeriodo."']";
			if($arrPeriodos=="")
				$arrPeriodos=$oPeriodo;
			else
				$arrPeriodos.=",".$oPeriodo;
		}
		
		$aPeriodo="['".$idLeyenda."','".$leyenda."',[".$arrPeriodos."]]";
		
		if($aPeriodicidad=="")
			$aPeriodicidad=$aPeriodo;
		else
			$aPeriodicidad.=",".$aPeriodo;
	}
	
	
	
?>
var aPeriodicidad=[<?php echo $aPeriodicidad?>];
var arrStatus=[<?php echo $arrRegistros ?>];
var arrCentrosGestores=<?php echo $arrCentrosGestores?>;
Ext.onReady(inicializar);

function inicializar()
{
	var cmbPeriodicidad=crearComboExt('cmbPeriodicidad',aPeriodicidad,0,0,250);
    
    cmbPeriodicidad.on('select',function(cmb,registro)
    							{
                                	gEx('cmbPeriodo').setValue('');
                                    gEx('cmbPeriodo').getStore().loadData(registro.data.valorComp);
                                    recargarGrid();
                                }
    				)
    
    
    
    var cmbPeriodo=crearComboExt('cmbPeriodo',[],0,0,350);
    cmbPeriodo.on('select',function(cmb,registro)
    							{
                                	
                                    recargarGrid();
                                }
    				)
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                border:false,
                                                tbar:	[
                                                			{
                                                            	xtype:'label',
                                                            	html:'<b>Periodicidad del reporte:</b>&nbsp;&nbsp;'
                                                            },
                                                            cmbPeriodicidad,
                                                            {
                                                            	xtype:'label',
                                                            	html:'&nbsp;&nbsp;<b>Periodo:</b>&nbsp;&nbsp;'
                                                            },
                                                            cmbPeriodo
                                                		],
                                                items:	[
                                                          crearGridInformeIndicadores()   
                                                        ]
                                            }
                                         ]
                            }
                        )   
                        
	if(aPeriodicidad.length==1)
    {
    	cmbPeriodicidad.setValue(aPeriodicidad[0][0]);
        dispararEventoSelectCombo('cmbPeriodicidad');
    }                        
                        
}

function crearGridInformeIndicadores()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'departamento'},
                                                        {name:'responsable'},
                                                        {name: 'fechaInicio', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'fechaTermino', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'diasRegistro'},
		                                                {name: 'idEstado'},
		                                                {name:'idRegistro'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                 
                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'departamento', direction: 'ASC'},
                                                            groupField: 'departamento',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='54';
                                        proxy.baseParams.idConvocatoria=gE('idRegistro').value;
                                        proxy.baseParams.periodicidad=gEx('cmbPeriodicidad').getValue();
                                        proxy.baseParams.noPeriodo=gEx('cmbPeriodo').getValue();
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'&Aacute;rea responsable',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'departamento',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCentrosGestores,val);
                                                                    }
                                                        },
                                                        {
                                                            header:'Responsable',
                                                            width:250,
                                                            sortable:true,
                                                            dataIndex:'responsable',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                        },
                                                        
                                                         {
                                                            header:'Fecha de Inicio',
                                                            width:110,
                                                            sortable:true,
                                                            dataIndex:'fechaInicio',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y');
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de T&eacute;rmino',
                                                            width:110,
                                                            sortable:true,
                                                            dataIndex:'fechaTermino',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
	                                                                    	return val.format('d/m/Y');
                                                                    }
                                                        },
                                                        {
                                                            header:'D&iacute;as transcurridos',
                                                            width:160,
                                                            sortable:true,
                                                            dataIndex:'diasRegistro',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '--';
                                                                    	return val;
                                                                    }
                                                        },
                                                        {
                                                            header:'',
                                                            width:40,
                                                            sortable:true,
                                                            dataIndex:'idRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	
	                                                                    	return '<a href="javascript:mostrarRegistro(\''+bE(val)+'\')"><img src="../images/magnifier.png" title="Ver registro del centro gestor" alt="Ver registro del centro gestor"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'Situaci&oacute;n',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'idEstado',
                                                            renderer:function(val)
                                                            		{
                                                                    	var icono='../images/';
                                                                        var pos=existeValorMatriz(arrStatus,val,0,true);
                                                                        icono+=arrStatus[pos][2];
                                                                    	return '<img width="13" height="13" src="'+icono+'"> '+arrStatus[pos][1];
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gCentrosGestores',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :true,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: true,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}

function mostrarRegistro (iR)
{
	window.parent.abrirFormularioProcesoFancy(bE(572),(iR),bE(0));
}


function recargarGrid()
{
	gEx('gCentrosGestores').getStore().load	(
    											{
                                                    url:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                    params:	{
                                                                funcion:54,
                                                                idConvocatoria:gE('idRegistro').value,
                                                                periodicidad:gEx('cmbPeriodicidad').getValue()==''?-1:gEx('cmbPeriodicidad').getValue(),
                                                                noPeriodo:gEx('cmbPeriodo').getValue()==''?-1:gEx('cmbPeriodo').getValue()
                                                            }
                                                }
    										)
}