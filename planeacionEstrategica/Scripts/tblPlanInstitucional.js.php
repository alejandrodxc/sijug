<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion=11 AND STATUS=1 ORDER BY unidad";
	$arrCentrosCosto=$con->obtenerFilasArreglo($consulta);
	
?>
var arrCentrosCosto=<?php echo $arrCentrosCosto?>;
var objetivoEstrategicoSel='0';
var programaEstrategicoSel='0';
var arrNivel=[];
var nodoSel=null;
Ext.onReady(inicializar);

function inicializar()
{
	var x;
    for(x=1;x<6;x++)
    {
    	arrNivel.push([x,x]);
    }
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px;"><b><span id="lblEtiqueta">'+bD(gE('nombrePlan').value)+'</span></b></span>',
                                                tbar:	[	
                                                			{
                                                                icon:'../images/salir.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Cerrar edici&oacute;n del plan',
                                                                handler:function()
                                                                        {
                                                                            regresarPagina();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/pencil.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Editar datos del Plan Institucional',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaCrearPlanInstitucional();
                                                                        }
                                                                
                                                            }
                                                            
                                                            ,'-',
                                                			{
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnAgregar',
                                                                text:'Agregar...',
                                                                menu:	[
                                                                			{
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnAddObjEst',
                                                                                disabled:true,
                                                                                text:'Agregar Objetivo Estrat&eacute;gico',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaObjetivoEstrategico();   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnAddProgEst',
                                                                                disabled:true,
                                                                                text:'Agregar Programa Estrat&eacute;gico',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaProgramaEstrategico();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnAddObjEsp',
                                                                                disabled:true,
                                                                                text:'Agregar Objetivo Espec&iacute;fico',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaObjetivoEspecifico();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnAddLineaAccion',
                                                                                disabled:true,
                                                                                text:'Agregar L&iacute;nea de Acci&oacute;n',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaLineaAccion();
                                                                                        }
                                                                                
                                                                            }
                                                                		]
                                                                
                                                            }
                                                			,'-',
                                                            {
                                                                icon:'../images/pencil.png',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnEdicion',
                                                                disabled:true,
                                                                text:'Editar elemento',
                                                                handler:function()
                                                                        {
                                                                            switch(nodoSel.attributes.tipoNodo)
                                                                            {
                                                                            	case '2':
                                                                                	mostrarVentanaObjetivoEstrategico(nodoSel);
                                                                                break;
                                                                                case '3':
                                                                                	mostrarVentanaProgramaEstrategico(nodoSel);
                                                                                break;
                                                                                case '4':
                                                                                	mostrarVentanaObjetivoEspecifico(nodoSel);
                                                                                break;
                                                                                case '5':
                                                                                	mostrarVentanaLineaAccion(nodoSel);
                                                                                break;
                                                                                
                                                                            }
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/delete.png',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnRemoverElemento',
                                                                disabled:true,
                                                                text:'Remover elemento',
                                                                handler:function()
                                                                        {
                                                                            function resp(btn)
                                                                            {
                                                                            	if(btn=='yes')
                                                                                {
                                                                                	var arrElemento=nodoSel.id.split('_');
                                                                                    
                                                                                	var cadObj='{"tipoElemento":"'+arrElemento[0]+'","idElemento":"'+arrElemento[1]+'"}';                                                                                    
                                                                                	function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                           	gEx('tPlanInstitucional').getRootNode().reload();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=9&cadObj='+cadObj,true);
                                                                                    
                                                                                }
                                                                            }
                                                                            msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                        }
                                                                
                                                            }
                                                		],
                                                items:	[
                                                            crearOrganigrama(),
                                                            crearGridFines()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearOrganigrama()
{
	var arrObjetivosEstrategicos=[['0','Todos los objetivos estrat\xE9gicos']];
    var arrProgramasEstrategicos=[['0','Todos los programas estrat\xE9gicos']];
	var cmbObjetivoEstrategico=crearComboExt('cmbObjetivoEstrategico',arrObjetivosEstrategicos,0,0,350);
    cmbObjetivoEstrategico.setValue('0');
    cmbObjetivoEstrategico.on('select',function(cmb,registro)
    									{
                                        	gEx('tPlanInstitucional').getRootNode().reload();
                                        }
                              )
    var cmbProgramaEstrategico=crearComboExt('cmbProgramaEstrategico',arrProgramasEstrategicos,0,0,350);
    cmbProgramaEstrategico.setValue('0');
   	cmbProgramaEstrategico.on('select',function(cmb,registro)
    									{
                                        	gEx('tPlanInstitucional').getRootNode().reload();
                                        }
                              )
    
		var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
		var cargadorArbol=new Ext.tree.TreeLoader(
                                                        {
                                                            baseParams:{
                                                                            funcion:'4',
                                                                            idPlan:gE('idPlan').value
                                                                        },
                                                            dataUrl:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                            uiProviders:	{
                                                                            	'col': Ext.ux.tree.ColumnNodeUI
                                                                        	}
                                                        }	


		                                         )		                                        
		
        cargadorArbol.on('beforeLoad',function(tl)
        								{
                                        	objetivoEstrategicoSel=gEx('cmbObjetivoEstrategico').getValue();
											programaEstrategicoSel=gEx('cmbProgramaEstrategico').getValue();
											tl.baseParams.idObjetivoEstrategico=objetivoEstrategicoSel;
                                            tl.baseParams.idProgramaEstrategico=programaEstrategicoSel;
											nodoSel=null;
                                        	gEx('btnAddObjEst').disable();
                                            gEx('btnAddProgEst').disable();
                                            gEx('btnAddObjEsp').disable();
                                            gEx('btnAddLineaAccion').disable();
                                            gEx('btnEdicion').disable();
                                            gEx('btnRemoverElemento').disable();
                                        }
        				)

		cargadorArbol.on('load',function(tl)
        								{
                                        	function funcAjax()
                                            {
                                                var resp=peticion_http.responseText;
                                                arrResp=resp.split('|');
                                                if(arrResp[0]=='1')
                                                {
                                                    var arrRegistros=eval(arrResp[1]);
                                                    gEx('cmbObjetivoEstrategico').getStore().loadData(arrRegistros);
                                                    gEx('cmbObjetivoEstrategico').setValue(objetivoEstrategicoSel);
                                                    gEx('cmbObjetivoEstrategico').collapse();
                                                    var pos=obtenerPosFila(gEx('cmbObjetivoEstrategico').getStore(),'id',objetivoEstrategicoSel);
                                                    if(pos==-1)
                                                    	pos=0;
                                                    gEx('cmbProgramaEstrategico').getStore().loadData(arrRegistros[pos][2]);
                                                    gEx('cmbProgramaEstrategico').setValue(programaEstrategicoSel);
                                                    gEx('cmbProgramaEstrategico').collapse();
                                                }
                                                else
                                                {
                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                }
                                            }
                                            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=38&idPlan='+gE('idPlan').value,true);
                                            
                                        
                                        }
                        )
                                        
		var organigrama = new Ext.ux.tree.TreeGrid	(
                                                            {
                                                                id:'tPlanInstitucional',
                                                                region:'center',
                                                                useArrows:true,
                                                                autoScroll:false,
                                                                animate:true,
                                                                enableDD:false,
                                                                containerScroll: true,
                                                                root:raiz,
                                                                enableSort:false,
                                                                loader: cargadorArbol,
                                                                rootVisible:false,
                                                                draggable:false,
                                                                tbar:	[
                                                                            {
                                                                                xtype:'label',
                                                                                html:'<b>Mostrar:&nbsp;&nbsp;</b>'
                                                                            },
                                                                            cmbObjetivoEstrategico,
                                                                            {
                                                                                xtype:'label',
                                                                                html:'<b>&nbsp;&nbsp;</b>'
                                                                            },
                                                                            cmbProgramaEstrategico
                                                                            
                                                                        ],
                                                                bbar:	[
                                                                			{
                                                                            	xtype:'checkbox',
                                                                                checked:true,
                                                                                id:'chkRecargar',
                                                                                boxLabel:'Recargar al agregar elementos'
                                                                            }
                                                                            
                                                                		],
                                                                
                                                                columns:[
                                                                			
                                                                            {
                                                                                header:'',
                                                                                width:300,
                                                                                dataIndex:'text'
                                                                            },
                                                                            {
                                                                                header:'',
                                                                                width:600,
                                                                                dataIndex:'detalle'
                                                                            }
                                                                         ]

                                                               
                                                            }
                                                    );
		
        
       

        
        
        organigrama.on('click',nodoClick);
        
        return organigrama;
       
}

function nodoClick(nodo)
{
	nodoSel=nodo;
	gEx('btnAddObjEst').disable();
    gEx('btnAddProgEst').disable();
    gEx('btnAddObjEsp').disable();
    gEx('btnAddLineaAccion').disable();
    gEx('btnEdicion').disable();
    gEx('btnRemoverElemento').disable();
    switch(nodo.attributes.tipoNodo)
    {
    	case '1':
        	gEx('btnAddObjEst').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        case '2':
        	gEx('btnAddProgEst').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
        break;
        case '3':
        	gEx('btnAddObjEsp').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        case '4':
        	gEx('btnAddLineaAccion').enable();
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
        case '5':
        	
            gEx('btnEdicion').enable();
            gEx('btnRemoverElemento').enable();
            
        break;
    }
}

function mostrarVentanaObjetivoEstrategico(nodo)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. Objetivo:'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:120,
                                                            value:nodo?nodo.attributes.noObjetivo:'',
                                                            id:'noObjetivo'
                                                            	
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n del objetivo estrat&eacute;gico:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:escaparBR(nodo?nodo.attributes.descripcionObjetivo:'',true),
                                                            id:'descripcionObjetivo'
                                                            	
                                                        }	
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: nodo?'Modificar objetivo estr&eacute;gico':'Agregar objetivo estr&eacute;gico',
										width: 650,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noObjetivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var noObjetivo=gEx('noObjetivo');
                                                                        var descripcionObjetivo=gEx('descripcionObjetivo');
                                                                        
                                                                        if(noObjetivo.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	noObjetivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de objetivo estrat&eacute;gico',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(descripcionObjetivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	descripcionObjetivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del objetivo estrat&eacute;gico',resp2);
                                                                            return;
                                                                        }
                                                                        var idObjetivo=-1;
                                                                        if(nodo)
                                                                        {
                                                                        	idObjetivo=nodo.attributes.idRegistro;
                                                                        }
                                                                        
                                                                        var cadObj='{"idPlan":"'+gE('idPlan').value+'","idObjetivo":"'+idObjetivo+'","noObjetivo":"'+cv(noObjetivo.getValue())+
                                                                        			'","descripcionObjetivo":"'+cv(descripcionObjetivo.getValue())+'"}';
                                                                                    
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	if(gEx('chkRecargar').getValue())
                                                                                	gEx('tPlanInstitucional').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=5&cadObj='+cadObj,true);
                                                                                                                                                            
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaProgramaEstrategico(nodo)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. de Programa:'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:120,
                                                            value:nodo?nodo.attributes.noPrograma:'',
                                                            id:'noPrograma'
                                                            	
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n del programa estrat&eacute;gico:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:escaparBR(nodo?nodo.attributes.descripcionPrograma:'',true),
                                                            id:'descripcionPrograma'
                                                            	
                                                        }	
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: nodo?'Modificar programa estr&eacute;gico':'Agregar programa estr&eacute;gico',
										width: 650,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noPrograma').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var noPrograma=gEx('noPrograma');
                                                                        var descripcionPrograma=gEx('descripcionPrograma');
                                                                        
                                                                        if(noPrograma.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	noPrograma.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de programa estrat&eacute;gico',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(descripcionPrograma.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	descripcionPrograma.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del programa estrat&eacute;gico',resp2);
                                                                            return;
                                                                        }
                                                                        var idPrograma=-1;
                                                                        if(nodo)
                                                                        {
                                                                        	idPrograma=nodo.attributes.idRegistro;
                                                                        }
                                                                        
                                                                        var cadObj='{"idPlan":"'+gE('idPlan').value+'","idPrograma":"'+idPrograma+'","noPrograma":"'+cv(noPrograma.getValue())+
                                                                        			'","descripcionPrograma":"'+cv(descripcionPrograma.getValue())+'","idObjetivoEstrategico":"'+
                                                                                    nodoSel.attributes.idRegistro+'"}';
                                                                                    
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	if(gEx('chkRecargar').getValue())
                                                                                	gEx('tPlanInstitucional').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=6&cadObj='+cadObj,true);
                                                                                                                                                            
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaObjetivoEspecifico(nodo)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. de Objetivo:'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:120,
                                                            value:nodo?nodo.attributes.noObjetivo:'',
                                                            id:'noObjetivo'
                                                            	
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n del objetivo espec&iacute;fico:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:escaparBR(nodo?nodo.attributes.descripcionObjetivo:'',true),
                                                            id:'descripcionObjetivo'
                                                            	
                                                        }	
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: nodo?'Modificar objetivo espec&iacute;fico':'Agregar objetivo espec&iacute;fico',
										width: 650,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noObjetivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var noObjetivo=gEx('noObjetivo');
                                                                        var descripcionObjetivo=gEx('descripcionObjetivo');
                                                                        
                                                                        if(noObjetivo.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	noObjetivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de objetivo espec&iacute;fico',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(descripcionObjetivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	descripcionObjetivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del objetivo espec&iacute;fico',resp2);
                                                                            return;
                                                                        }
                                                                        var idObjetivo=-1;
                                                                        if(nodo)
                                                                        {
                                                                        	idObjetivo=nodo.attributes.idRegistro;
                                                                        }
                                                                        
                                                                        var cadObj='{"idPlan":"'+gE('idPlan').value+'","idObjetivo":"'+idObjetivo+'","noObjetivo":"'+cv(noObjetivo.getValue())+
                                                                        			'","descripcionObjetivo":"'+cv(descripcionObjetivo.getValue())+'","idProgramaEstrategico":"'+
                                                                                    nodoSel.attributes.idRegistro+'"}';
                                                                                    
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	if(gEx('chkRecargar').getValue())
                                                                                	gEx('tPlanInstitucional').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=7&cadObj='+cadObj,true);
                                                                                                                                                            
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaLineaAccion(nodo)
{
	var arrDatos=[];
    var x;
    for(x=0;x<arrCentrosCosto.length;x++)
    {
    	if(nodo)
        {
        	var pos=existeValorMatriz(nodo.attributes.arrAreas,arrCentrosCosto[x][0]);
        	arrDatos.push([arrCentrosCosto[x][0],pos!=-1]);
        }
        else
    		arrDatos.push([arrCentrosCosto[x][0],false]);
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. de L&iacute;nea de acci&oacute;n:'
                                                        },
                                                        {
                                                        	x:200,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:120,
                                                            value:nodo?nodo.attributes.noLinea:'',
                                                            id:'noLinea'
                                                            	
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n de la l&iacute;nea de acci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:50,
                                                            value:escaparBR(nodo?nodo.attributes.descripcionLinea:'',true),
                                                            id:'descripcionLinea'
                                                            	
                                                        },
                                                        crearGridAreasResponsables(arrDatos)
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: nodo?'Modificar l&iacute;nea de acci&oacute;n':'Agregar l&iacute;nea de acci&oacute;n',
										width: 650,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noLinea').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var noLinea=gEx('noLinea');
                                                                        var descripcionLinea=gEx('descripcionLinea');
                                                                        
                                                                        if(noLinea.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	noLinea.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de la l&iacute;nea de acci&oacute;n',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(descripcionLinea.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	descripcionLinea.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n de la l&iacute;nea de acci&oacute;n',resp2);
                                                                            return;
                                                                        }
                                                                        var idLinea=-1;
                                                                        if(nodo)
                                                                        {
                                                                        	idLinea=nodo.attributes.idRegistro;
                                                                        }
                                                                        
                                                                        var arrAreas='';
                                                                        var x;
                                                                        var f;
                                                                        for(x=0;x<gEx('gAreasResponsables').getStore().getCount();x++)
                                                                        {
                                                                        	f=gEx('gAreasResponsables').getStore().getAt(x);
                                                                            if(f.data.areaSeleccionada)
                                                                            {
                                                                            	if(arrAreas=='')
                                                                                	arrAreas=f.data.codigoArea;
                                                                                else
                                                                                	arrAreas+=','+f.data.codigoArea;
                                                                            }
                                                                        }
                                                                        
                                                                        var cadObj='{"idPlan":"'+gE('idPlan').value+'","idLinea":"'+idLinea+'","noLinea":"'+cv(noLinea.getValue())+
                                                                        			'","descripcionLinea":"'+cv(descripcionLinea.getValue())+'","idObjetivoEspecifico":"'+
                                                                                    nodoSel.attributes.idRegistro+'","arrAreas":"'+arrAreas+'"}';
                                                                                    
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	if(gEx('chkRecargar').getValue())
                                                                               		gEx('tPlanInstitucional').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=8&cadObj='+cadObj,true);
                                                                                                                                                            
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridFines()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
		                                                {name: 'descripcionFin'},
		                                                {name:'nivelFin'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nivelFin', direction: 'DESC'},
                                                            groupField: 'nivelFin',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='10';
                                        proxy.baseParams.idPlan=gE('idPlan').value;
                                    }
                        )   
       
  var cModelo= new Ext.grid.ColumnModel   	(
                                                  [
                                                      new  Ext.grid.RowNumberer(),
                                                      
                                                      {
                                                          header:'Descripci&oacute;n del fin',
                                                          width:320,
                                                          sortable:true,
                                                          dataIndex:'descripcionFin',
                                                          render:mostrarValorDescripcion
                                                      },
                                                      {
                                                          header:'Nivel del fin',
                                                          width:150,
                                                          sortable:true,
                                                          dataIndex:'nivelFin',
                                                          renderer:function(val)
                                                          		{
                                                                	return 'Nivel del fin: '+val;
                                                                }
                                                      }
                                                  ]
                                              );
                                              
  var tblGrid=	new Ext.grid.GridPanel	(
                                                      {
                                                          id:'gFines',
                                                          store:alDatos,
                                                          region:'east',
                                                          frame:false,
                                                          width:350,
                                                          collapsible:true,
                                                          cm: cModelo,
                                                          stripeRows :true,
                                                          loadMask:true,
                                                          tbar:	[
                                                                    {
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        text:'Agregar fin',
                                                                        handler:function()
                                                                                {
                                                                                    mostrarVentanaAgregarFin();
                                                                                }
                                                                        
                                                                    },'-',
                                                                    {
                                                                        icon:'../images/pencil.png',
                                                                        cls:'x-btn-text-icon',
                                                                        text:'Modificar fin',
                                                                        handler:function()
                                                                                {
                                                                                   var fila= gEx('gFines').getSelectionModel().getSelected();
                                                                                   if(!gFines)
                                                                                   {
                                                                                   		msgBox('Debe seleccionar el fin del plan que desea modificar');
                                                                                   		return;
                                                                                   }
                                                                                   
                                                                                   mostrarVentanaAgregarFin(fila);
                                                                                }
                                                                        
                                                                    },'-',
                                                                    {
                                                                        icon:'../images/delete.png',
                                                                        cls:'x-btn-text-icon',
                                                                        text:'Remover fin',
                                                                        handler:function()
                                                                                {
                                                                                    var fila= gEx('gFines').getSelectionModel().getSelected();
                                                                                    if(!gFines)
                                                                                    {
                                                                                    	msgBox('Debe seleccionar el fin del plan que desea remover');
                                                                                        return;
                                                                                	}
                                                                                    
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    gEx('gFines').getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=12&idFin='+fila.data.idRegistro,true);
                                                                                            
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer remover el fin seleccionado?',resp);
                                                                                }
                                                                        
                                                                    }
                                                                    
                                                                ],
                                                          columnLines : true,
                                                          title:'Fines del plan',
                                                          view:new Ext.grid.GroupingView({
                                                                                              forceFit:false,
                                                                                              showGroupName: false,
                                                                                              enableGrouping :true,
                                                                                              enableNoGroups:false,
                                                                                              enableGroupingMenu:false,
                                                                                              hideGroupedColumn: true,
                                                                                              startCollapsed:false
                                                                                          })
                                                      }
                                                  );
  return 	tblGrid;	
}

function mostrarVentanaAgregarFin(filaReg)
{
	var cmbNivelFin=crearComboExt('cmbNivelFin',arrNivel,100,5,120);
    
    if(filaReg)
    {
    	cmbNivelFin.setValue(filaReg.data.nivelFin);
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nivel del fin:'
                                                        },
                                                        cmbNivelFin,
                                            			{
                                                        	x:10,
                                                            y:40,
                                                            html:'Fin del plan:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:610,
                                                            height:80,
                                                            id:'txtFinPlan',
                                                            xtype:'textarea',
                                                            value:filaReg?escaparBR(filaReg.data.descripcionFin,true):''
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: filaReg?'Modificar fin del plan':"Agregar fin del plan",
										width: 660,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtFinPlan').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var txtFinPlan=gEx('txtFinPlan');
                                                                        
																		if(cmbNivelFin.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cmbNivelFin.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nivel del fin',resp1);
                                                                            return;
                                                                        }	
                                                                        
                                                                        if(txtFinPlan.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtFinPlan.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del fin',resp2);
                                                                            return;
                                                                        }	
                                                                        
                                                                        var idFin=-1;
                                                                        
                                                                        if(filaReg)
                                                                        {
                                                                        	idFin=filaReg.data.idRegistro;
                                                                        }
                                                                        
                                                                        var cadObj='{"idFin":"'+idFin+'","idPlan":"'+gE('idPlan').value+'","nivelPlan":"'+
                                                                        			cmbNivelFin.getValue()+'","finPlan":"'+cv(txtFinPlan.getValue())+'"}';
                                                                                    
                                                                                    
                                                                                    
                                                                     	function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gFines').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=11&cadObj='+cadObj,true);
                                                                           
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}


function mostrarVentanaCrearPlanInstitucional()
{
	var objPlan=eval('['+bD(gE('objPlan').value)+']')[0];
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre del Plan:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:465,
                                                            value:decodeURIComponent(objPlan.nombrePlan),
                                                            id:'txtNombrePlan'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del Plan:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:40,
                                                            html:'Del'
                                                        },
                                                        {
                                                        	x:180,
                                                            y:35,
                                                            value:objPlan.fechaInicio,
                                                            xtype:'datefield',
                                                            id:'dteInicio'
                                                        },
                                                        {
                                                        	x:305,
                                                            y:40,
                                                            html:'al'
                                                        },
                                                        {
                                                        	x:340,
                                                            y:35,
                                                            value:objPlan.fechaTermino,
                                                            xtype:'datefield',
                                                            id:'dteFin'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Prop&oacute;sito del Plan:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:105,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            value:escaparBR(decodeURIComponent(objPlan.proposito),true),
                                                            id:'txtPropositoPlan'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar datos del Plan Institucional',
										width: 650,
										height:285,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombrePlan').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtNombrePlan=gEx('txtNombrePlan');
                                                                        var dteInicio=gEx('dteInicio');
                                                                        var dteFin=gEx('dteFin');
                                                                        var txtPropositoPlan=gEx('txtPropositoPlan');
                                                                        
                                                                        
                                                                        if(txtNombrePlan.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtNombrePlan.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del plan institucional',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteInicio.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteInicio.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicial del plan institucional',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteFin.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFin.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de t&eacute;rmino del plan institucional',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteInicio.getValue()>dteFin.getValue())
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteInicio.focus();
                                                                            }
                                                                            msgBox('La fecha inicial no puede ser mayor que la fecha de t&eacute;rmino del plan institucional',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtPropositoPlan.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	txtPropositoPlan.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el prop&oacute;sito del plan institucional',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        var cadObj='{"idPlanInstitucional":"'+gE('idPlan').value+'","nombrePlan":"'+
                                                                        		cv(txtNombrePlan.getValue())+'","fechaInicio":"'+dteInicio.getValue().format('Y-m-d')+
                                                                                '","fechaTermino":"'+dteFin.getValue().format('Y-m-d')+
                                                                                '","proposito":"'+cv(txtPropositoPlan.getValue())+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gE('lblEtiqueta').innerHTML=txtNombrePlan.getValue();
                                                                            	gE('objPlan').value=bE(cadObj);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=2&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridAreasResponsables(arrDatos)
{

	var checkColumn = new Ext.grid.CheckColumn	(
	 												{
													   header: '',
													   dataIndex: 'areaSeleccionada',
													   width: 70
													}
												);
	
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                   	{name:'codigoArea'},
		                                                			{name: 'areaSeleccionada'}
                                                                ]
                                                    }
                                                );
	   

    alDatos.loadData(arrDatos);
       
     var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                           
                                                            checkColumn,
                                                            {
                                                                header:'&Aacute;rea responsable',
                                                                width:480,
                                                                sortable:true,
                                                                dataIndex:'codigoArea',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrCentrosCosto,val);
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gAreasResponsables',
                                                                store:alDatos,
                                                                x:10,
                                                                y:135,
                                                                height:250,
                                                                width:600,
                                                                title:'&Aacute;reas responsables de su ejecuci&oacute;n',
                                                                frame:false,
                                                                cm: cModelo,
                                                                plugins:checkColumn,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true
                                                            }
                                                        );
        return 	tblGrid;
}