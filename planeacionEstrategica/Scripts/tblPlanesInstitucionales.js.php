<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Planes institucionales</b></span>',
                                                items:	[
                                                            crearGridPlanesInstitucionales()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridPlanesInstitucionales()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPlanInstitucional'},
		                                                {name: 'nombrePlan'},
		                                                {name: 'propositoPlan'},
                                                        {name: 'periodoInicioPlan', type:'date',dateFormat:'Y-m-d'},
                                                        {name: 'periodoTerminoPlan', type:'date',dateFormat:'Y-m-d'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idPlanInstitucional', direction: 'ASC'},
                                                            groupField: 'nombrePlan',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='1';
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        
                                                        {
                                                            header:'Nombre del plan',
                                                            width:380,
                                                            sortable:true,
                                                            dataIndex:'nombrePlan',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Vigencia',
                                                            width:180,
                                                            sortable:true,
                                                            dataIndex:'periodoInicioPlan',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return 'Del '+val.format('d/m/Y')+' al '+registro.data.periodoTerminoPlan.format('d/m/Y');
                                                                    }
                                                        },
                                                        
                                                        {
                                                            header:'Prop&oacute;sito',
                                                            width:600,
                                                            sortable:true,
                                                            dataIndex:'propositoPlan',
                                                            renderer:mostrarValorDescripcion
                                                        }
                                                        
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gPlanesInstitucionales',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true, 
                                                            tbar:	[
                                                            			
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',

                                                                            text:'Crear Plan Institucional',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaCrearPlanInstitucional();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar Plan Institucional',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gPlanesInstitucionales').getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el Plan Institucional a modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	abrirPlanInstitucional(fila.data.idPlanInstitucional)
                                                                                    }
                                                                            
                                                                        }
                                                                        ,'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Plan Institucional',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gPlanesInstitucionales').getSelectionModel().getSelected();
                                                                                        
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el Plan Institucional que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        gEx('gPlanesInstitucionales').getStore().reload();
                                                                                                        
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=3&idPlanInstitucional='+fila.data.idPlanInstitucional,true);
                                                                                        	}
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el Plan Institucional seleccionado?',resp);
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                            		],                                                           
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}

function mostrarVentanaCrearPlanInstitucional()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre del Plan:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:465,
                                                            id:'txtNombrePlan'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del Plan:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:40,
                                                            html:'Del'
                                                        },
                                                        {
                                                        	x:180,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteInicio'
                                                        },
                                                        {
                                                        	x:305,
                                                            y:40,
                                                            html:'al'
                                                        },
                                                        {
                                                        	x:340,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFin'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Prop&oacute;sito del Plan:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:105,
                                                            xtype:'textarea',
                                                            width:600,
                                                            height:80,
                                                            id:'txtPropositoPlan'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Crear Plan Institucional',
										width: 650,
										height:285,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombrePlan').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtNombrePlan=gEx('txtNombrePlan');
                                                                        var dteInicio=gEx('dteInicio');
                                                                        var dteFin=gEx('dteFin');
                                                                        var txtPropositoPlan=gEx('txtPropositoPlan');
                                                                        
                                                                        
                                                                        if(txtNombrePlan.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtNombrePlan.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del plan institucional',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteInicio.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteInicio.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicial del plan institucional',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteFin.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFin.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de t&eacute;rmino del plan institucional',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteInicio.getValue()>dteFin.getValue())
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteInicio.focus();
                                                                            }
                                                                            msgBox('La fecha inicial no puede ser mayor que la fecha de t&eacute;rmino del plan institucional',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtPropositoPlan.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	txtPropositoPlan.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el prop&oacute;sito del plan institucional',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        var cadObj='{"idPlanInstitucional":"-1","nombrePlan":"'+cv(txtNombrePlan.getValue())+
                                                                        		'","fechaInicio":"'+dteInicio.getValue().format('Y-m-d')+
                                                                                '","fechaTermino":"'+dteFin.getValue().format('Y-m-d')+
                                                                                '","proposito":"'+cv(txtPropositoPlan.getValue())+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	abrirPlanInstitucional(arrResp[1]);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=2&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function abrirPlanInstitucional(idPlan)
{
	var arrParam=[['idPlan',idPlan]];
    enviarFormularioDatos('../planeacionEstrategica/tblPlanInstitucional.php',arrParam);
}