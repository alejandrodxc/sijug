<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


Ext.onReady(inicializar);

function inicializar()
{
	var tamAreaTrabajo=obtenerDimensionesNavegador()[0]-30;
	
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                border:false,
                                                items:	[
                                                            {
                                                            	xtype:'tabpanel',
                                                                activeTab:0,
                                                                region:'center',
                                                                items:	[
                                                                			{
                                                                            	xtype:'panel',
                                                                                title:'A. Definici&oacute;n del problema principal',
                                                                                id:'pDefinicionProblema',
                                                                                items:	[
                                                                                			new Ext.ux.IFrameComponent({ 
                
                                                                                                                            id: 'frmDefinicionProblematica', 
                                                                                                                            anchor:'100% 100%',
                                                                                                                            loadFuncion:function(iFrame)
                                                                                                                                        {
                                                                                                                                            
                                                                                                                                            //autofitIframe(iFrame);
                                                                                                                                        },
                
                                                                                                                            url: '../paginasFunciones/white.php',
                                                                                                                            style: 'width:100%;height:100%' 
                                                                                                                    })
                                                                                		]
                                                                            },
                                                                            {
                                                                            	xtype:'panel',
                                                                                title:'B. An&aacute;lisis del problema',
                                                                                id:'pAnalisisProblema',
                                                                                listeners:	{
                                                                                				activate: function(p)
                                                                                                          {
                                                                                                             	if(!p.visualizado)
                                                                                                                {
                                                                                                                	 gEx('frmAnalisisProblema').load	(
                                                                                                                                                                {
                                                                                                                                                                    url:'../planeacionEstrategica/tblArbolProblemas.php',
                                                                                                                                                                    scripts:true,
                                                                                                                                                                    params:	{
                                                                                                                                                                                cPagina:'sFrm=true',
                                                                                                                                                                                idFormulario:546,
                                                                                                                                                                                idRegistro:gE('idRegistro').value,
                                                                                                                                                                                sL:gE('sL').value
                                                                                                                                                                                
                                                                                                                                                                            }
                                                                                                                                                                    
                                                                                                                                                                }
                                                                                                                                                            );
                                                                                                                	p.visualizado=true;
                                                                                                                }
                                                                                                          }
                                                                                            },
                                                                                items:	[
                                                                                			new Ext.ux.IFrameComponent({ 
                
                                                                                                                            id: 'frmAnalisisProblema', 
                                                                                                                            anchor:'100% 100%',
                                                                                                                            loadFuncion:function(iFrame)
                                                                                                                                        {
                                                                                                                                            
                                                                                                                                            
                                                                                                                                        },
                
                                                                                                                            url: '../paginasFunciones/white.php',
                                                                                                                            style: 'width:100%;height:' +tamAreaTrabajo+'px'
                                                                                                                    })
                                                                                		]
                                                                            },
                                                                            {
                                                                            	xtype:'panel',
                                                                                title:'C. An&aacute;lisis de Objetivos',
                                                                                id:'pAnalisisObjetivo',
                                                                                listeners:	{
                                                                                				activate: function(p)
                                                                                                          {
                                                                                                             	if(!p.visualizado)
                                                                                                                {
                                                                                                                	 gEx('frmAnalisisObjetivos').load	(
                                                                                                                                                                {
                                                                                                                                                                    url:'../planeacionEstrategica/tblArbolObjetivos.php',
                                                                                                                                                                    scripts:true,
                                                                                                                                                                    params:	{
                                                                                                                                                                                cPagina:'sFrm=true',
                                                                                                                                                                                idFormulario:546,
                                                                                                                                                                                idRegistro:gE('idRegistro').value,
                                                                                                                                                                                sL:gE('sL').value
                                                                                                                                                                                
                                                                                                                                                                            }
                                                                                                                                                                    
                                                                                                                                                                }
                                                                                                                                                            );
                                                                                                                	p.visualizado=true;
                                                                                                                }
                                                                                                          }
                                                                                            },
                                                                                items:	[
                                                                                			new Ext.ux.IFrameComponent({ 
                
                                                                                                                            id: 'frmAnalisisObjetivos', 
                                                                                                                            anchor:'100% 100%',
                                                                                                                            loadFuncion:function(iFrame)
                                                                                                                                        {
                                                                                                                                            
                                                                                                                                           
                                                                                                                                        },
                
                                                                                                                            url: '../paginasFunciones/white.php',
                                                                                                                            style: 'width:100%;height:' +tamAreaTrabajo+'px'
                                                                                                                    })
                                                                                		]
                                                                            }
                                                                		]
                                                                
                                                            }
                                                        ]
                                            }
                                         ]
                            }
                        )   

	if((gE('sL').value=='0')&&(gE('idRegistroFormulario').value=='-1'))
    {

        gEx('frmDefinicionProblematica').load	(
                                                    {
                                                        url:'../modeloPerfiles/registroFormularioV2.php',
                                                        scripts:true,
                                                        params:	{
                                                                    cPagina:'sFrm=true',
                                                                    actor:gE('actor').value,
                                                                    idFormulario:546,
                                                                    idReferencia:gE('idRegistro').value,
                                                                    idRegistro:gE('idRegistroFormulario').value,
                                                                    eJs:bE('window.parent.recargarVistaFormularioProblematica(@idRegistro);return;')
                                                                    
                                                                    
                                                                }
                                                        
                                                    }
                                                );
	}
    else
    {
    	 gEx('frmDefinicionProblematica').load	(
                                                    {
                                                        url:'../modeloPerfiles/verFichaFormularioV2.php',
                                                        scripts:true,
                                                        params:	{
                                                                    cPagina:'sFrm=true',
                                                                    idFormulario:546,
                                                                    pM:  gE('sL').value=='0'?1:0,
                                                                    pE:  0,
                                                                    idRegistro:gE('idRegistroFormulario').value
                                                                }
                                                        
                                                    }
                                                );
    }
    
   
}

function recargarVistaFormularioProblematica(iRegistro)
{
	gEx('frmDefinicionProblematica').load	(
                                                    {
                                                        url:'../modeloPerfiles/verFichaFormularioV2.php',
                                                        scripts:true,
                                                        params:	{
                                                                    cPagina:'sFrm=true',
                                                                    idFormulario:546,
                                                                    pE:  0,
                                                                    pM:  gE('sL').value=='0'?1:0,
                                                                    idRegistro:iRegistro
                                                                }
                                                        
                                                    }
                                                );
}


function recargarMenuDTD()
{
	window.parent.recargarMenuDTD();
}