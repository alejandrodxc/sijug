<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT DISTINCT o.codigoUnidad,o.unidad FROM _550_tablaDinamica i,_539_tablaDinamica fb,817_organigrama o WHERE idIndicadorBase=-1
				AND i.idReferencia=fb.id__539_tablaDinamica AND o.codigoUnidad=fb.codigoInstitucion order by o.unidad";
	$arrAreas=$con->obtenerFilasArreglo($consulta);
	
	
	$arrLeyendas[1]="Mes";
	$arrLeyendas[2]="Bimestre";
	$arrLeyendas[3]="Trimestre";
	$arrLeyendas[4]="Cuatrimestre";
	$arrLeyendas[6]="Semestre";
	$arrLeyendas[12]="Anual";	
	
	$arrPosicionOrd[1]="Primer";
	$arrPosicionOrd[2]="Segundo";
	$arrPosicionOrd[3]="Tercer";
	$arrPosicionOrd[4]="Cuarto";
	$arrPosicionOrd[5]="Quinto";
	$arrPosicionOrd[6]="Sexto";
	$arrPosicionOrd[7]="Séptimo";
	$arrPosicionOrd[8]="Octavo";
	$arrPosicionOrd[9]="Noveno";
	$arrPosicionOrd[10]="Décimo";
	$arrPosicionOrd[11]="Décimo primer";
	$arrPosicionOrd[12]="Décimo segundo";
	
	$arrPeriodicidades="";
	foreach($arrLeyendas as $idPeriodo=>$resto)
	{
		$totalPeridos=12/$idPeriodo;
		$arrPeriodos="";
		for($periodoReporte=1;$periodoReporte<=$totalPeridos;$periodoReporte++)
		{
			$lblPeriodo="";
			if($totalPeridos>1)
			{
				$mesReporte=($idPeriodo*$periodoReporte)-1;
			
				$mesFinal=$arrMesLetra[$mesReporte];
			
				$mesReporteInicial=$mesReporte-($idPeriodo-2);
				$mesInicial=$arrMesLetra[$mesReporteInicial-1];
				
				
				$lblPeriodo=$arrPosicionOrd[$periodoReporte]." ".$arrLeyendas[$idPeriodo].($mesInicial!=$mesFinal?" (".$mesInicial." - ".$mesFinal.")":" (".$mesInicial.")");
			}
			else
			{
				$lblPeriodo="Anual";
			}
			
			$oPeriodo="['".$periodoReporte."','".cv($lblPeriodo)."']";
			if($arrPeriodos=="")
				$arrPeriodos=$oPeriodo;
			else
				$arrPeriodos.=",".$oPeriodo;
		}
		$o="['".$idPeriodo."',[".$arrPeriodos."]]";
		if($arrPeriodicidades=="")
			$arrPeriodicidades=$o;
		else
			$arrPeriodicidades.=",".$o;
	}
	
?>	
var arrPeriodicidades=[<?php echo $arrPeriodicidades?>];
var arrFrecuenciaMedicion=[['1','Mensual'],['2','Bimestral'],['3','Trimestral'],['4','Cuatrimestral'],['6','Semestral'],['12','Anual']];
var arrAreas=<?php echo $arrAreas?>;
Ext.onReady(inicializar);

function inicializar()
{
	var cmdArea=crearComboExt('cmdArea',arrAreas,0,0,450);
    
    cmdArea.on('select',function(cmb,registro)		
    					{
                        	function funcAjax()
                            {
                                var resp=peticion_http.responseText;
                                arrResp=resp.split('|');
                                if(arrResp[0]=='1')
                                {
                                    var arrDatos=eval(arrResp[1]);
                                    gEx('cmbIndicador').setValue('');
                                    gEx('cmbIndicador').getStore().loadData(arrDatos);
                                    gEx('cmbPeriodos').setValue('');
                                    gEx('cmbPeriodos').getStore().removeAll();
                                    gEx('cmbCiclosFiscales').setValue('');
                                    gEx('cmbCiclosFiscales').getStore().removeAll();
                                    gEx('frameContenido').load	(
                                                                  {
                                                                      url:'../paginasFunciones/white.php'
                                                                  }
                                                              )
                                }
                                else
                                {
                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=58&cveArea='+registro.data.id,true);
                            
                        }
    		);
    
    var cmbIndicador=crearComboExt('cmbIndicador',[],0,0,370);
    cmbIndicador.on('select',	function(cmb,registro)
    					{
                       		var pos=existeValorMatriz(arrPeriodicidades,registro.data.valorComp);
                            
                        	gEx('cmbPeriodos').getStore().loadData(arrPeriodicidades[pos][1]);
                            gEx('cmbCiclosFiscales').setValue('');
                            gEx('cmbCiclosFiscales').getStore().loadData(registro.data.valorComp2);
                            gEx('frameContenido').load	(
                                                                  {
                                                                      url:'../paginasFunciones/white.php'
                                                                  }
                                                              )
                        }
    				)
    
    
    var cmbPeriodos=crearComboExt('cmbPeriodos',[],0,0,250,{multiSelect:true});
    
    var cmbCiclosFiscales=crearComboExt('cmbCiclosFiscales',[],0,0,80,{multiSelect:true});
    new Ext.Viewport(	{
                                    layout: 'border',
                                    items: [
                                                {
                                                    xtype:'panel',
                                                    region:'center',
                                                    layout:'border',
                                                    border:false,
                                                    tbar:	[
                                                                {
                                                                    xtype:'label',
                                                                    html:'<b>Nombre del &Aacute;rea:</b>&nbsp;&nbsp;'
                                                                },
                                                                cmdArea
                                                                
                                                            ],
                                                    items:	[
                                                              	{
                                                                    xtype:'panel',
                                                                    region:'center',
                                                                    tbar:	[
                                                                                {
                                                                                    xtype:'label',
                                                                                    html:'<b>Nombre del indicador:</b>&nbsp;&nbsp;'
                                                                                },
                                                                                cmbIndicador,'-',
                                                                                 {
                                                                                    xtype:'label',
                                                                                    html:'&nbsp;&nbsp;<b>Periodo a considerar:</b>&nbsp;&nbsp;'
                                                                                },
                                                                                cmbPeriodos,'-',
                                                                                 {
                                                                                    xtype:'label',
                                                                                    html:'&nbsp;&nbsp;<b>Ciclo fiscal:</b>&nbsp;&nbsp;'
                                                                                },
                                                                                cmbCiclosFiscales,
                                                                                '-',
                                                                                {
                                                                                    icon:'../images/document_go.png',
                                                                                    cls:'x-btn-text-icon',
                                                                                    text:'Generar...',
                                                                                    handler:function()
                                                                                            {
                                                                                            	if(cmdArea.getValue()=='')
                                                                                                {
                                                                                                	function resp0()
                                                                                                    {
                                                                                                    	cmdArea.focus();
                                                                                                    }
                                                                                                    msgBox('Debe seleccionar el &aacute;rea cuyo informe desea generar',resp0);
                                                                                                    return;
                                                                                                }
                                                                                                
                                                                                                if(cmbIndicador.getValue()=='')
                                                                                                {
                                                                                                	function resp()
                                                                                                    {
                                                                                                    	cmbIndicador.focus();
                                                                                                    }
                                                                                                    msgBox('Debe seleccionar el indicador cuyo informe desea generar',resp);
                                                                                                    return;
                                                                                                }
                                                                                                
                                                                                                if(cmbPeriodos.getValue()=='')
                                                                                                {
                                                                                                	function resp2()
                                                                                                    {
                                                                                                    	cmbPeriodos.focus();
                                                                                                    }
                                                                                                    msgBox('Debe seleccionar almenos un periodo cuyo informe desea generar',resp2);
                                                                                                    return;
                                                                                                }
                                                                                                
                                                                                                if(cmbCiclosFiscales.getValue()=='')
                                                                                                {
                                                                                                	function resp3()
                                                                                                    {
                                                                                                    	cmbCiclosFiscales.focus();
                                                                                                    }
                                                                                                    msgBox('Debe seleccionar almenos un ciclo fiscal cuyo informe desea generar',resp3);
                                                                                                    return;
                                                                                                }
                                                                                             	
                                                                                                gEx('frameContenido').load	(
                                                                                                								{
                                                                                                                                	url:'../planeacionEstrategica/tblGraficoInformeIndicadores.php',
                                                                                                                                    params:	{
                                                                                                                                    			cPagina:'sFrm=true',
                                                                                                                                    			area:cmdArea.getValue(),
                                                                                                                                                idIndicador:cmbIndicador.getValue(),
                                                                                                                                                periodos:cmbPeriodos.getValue(),
                                                                                                                                                ciclosFicales:cmbCiclosFiscales.getValue()
                                                                                                                                                
                                                                                                                                    		}
                                                                                                                                }
                                                                                                							)
                                                                                                   
                                                                                            }
                                                                                    
                                                                                }
                                                                            
                                                                        	],
                                                                	items:	[
                                                                    			new Ext.ux.IFrameComponent({ 

                                                                                        id: 'frameContenido', 
                                                                                        anchor:'100% 100%',
                                                                                        loadFuncion:function(iFrame)
                                                                                                    {
                                                                                                    	
                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                    },

                                                                                        url: '../paginasFunciones/white.php',
                                                                                        style: 'width:100%;height:100%' 
                                                                                })
                                                                    		]
                                                              	}
                                                            ]
                                                }
                                             ]
                                }
                            )   
}
               