<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT DISTINCT o.codigoUnidad,o.unidad FROM _550_tablaDinamica i,_539_tablaDinamica fb,817_organigrama o WHERE idIndicadorBase=-1
				AND i.idReferencia=fb.id__539_tablaDinamica AND o.codigoUnidad=fb.codigoInstitucion order by o.unidad";
	$arrAreas=$con->obtenerFilasArreglo($consulta);
	
?>	
var arrFrecuenciaMedicion=[['1','Mensual'],['2','Bimestral'],['3','Trimestral'],['4','Cuatrimestral'],['6','Semestral'],['12','Anual']];
var arrAreas=<?php echo $arrAreas?>;
Ext.onReady(inicializar);

function inicializar()
{
	var cmdArea=crearComboExt('cmdArea',arrAreas,0,0,450);
    
    cmdArea.on('select',recargarGrid);
    new Ext.Viewport(	{
                                    layout: 'border',
                                    items: [
                                                {
                                                    xtype:'panel',
                                                    region:'center',
                                                    layout:'border',
                                                    border:false,
                                                    tbar:	[
                                                                {
                                                                    xtype:'label',
                                                                    html:'<b>Nombre del &Aacute;rea:</b>&nbsp;&nbsp;'
                                                                },
                                                                cmdArea
                                                                
                                                            ],
                                                    items:	[
                                                              crearGridIndicadores()   
                                                            ]
                                                }
                                             ]
                                }
                            )   
}
                        
function crearGridIndicadores()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idIndicador'},
                                                        {name:'nombreIndicador'},
                                                        {name: 'objetivoIndicador'},
                                                        {name: 'ciclosUso'},
                                                        {name: 'frecuenciaMedicion'},
		                                                {name:'funcionCalculo'},
                                                        {name:'lblFuncionCalculo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                 
                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreIndicador', direction: 'ASC'},
                                                            groupField: 'nombreIndicador',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='55';
                                       
                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'Nombre del indicador',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'nombreIndicador',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                        },
                                                        {
                                                            header:'Objetivo del Indicador',
                                                            width:350,
                                                            sortable:true,
                                                            dataIndex:'objetivoIndicador',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                        },
                                                        {
                                                            header:'Frecuencia de<br>medición',
                                                            width:150,
                                                            sortable:true,
                                                            dataIndex:'frecuenciaMedicion',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFrecuenciaMedicion,val);
                                                                    }
                                                        },
                                                        
                                                         
                                                        {
                                                            header:'Usado en',
                                                            width:170,
                                                            sortable:true,
                                                            dataIndex:'ciclosUso',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '--';
                                                                    	return val;
                                                                    }
                                                        },
                                                        
                                                        {
                                                            header:'Funci&oacute;n de c&aacute;lculo',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'funcionCalculo',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='&nbsp;&nbsp;<a href="javascript:agregarFuncionCalculo(\''+bE(registro.data.idIndicador)+'\')"><img width="13" height="13" src="../images/pencil.png" title="Asignar funci&oacute;n de c&aacute;lculo" alt="Asignar funci&oacute;n de c&aacute;lculo">'
                                                                    	var comp2='&nbsp;&nbsp;<a href="javascript:removerFuncionCalculo(\''+bE(registro.data.idIndicador)+'\')"><img width="13" height="13" src="../images/delete.png" title="Remover funci&oacute;n de c&aacute;lculo" alt="Remover funci&oacute;n de c&aacute;lculo">'
                                                                        if(val=='-1')
                                                                        	return '(No asignado)'+comp;
                                                                    	return registro.data.lblFuncionCalculo+comp+comp2;
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gGridIndicador',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}


function recargarGrid()
{
	gEx('gGridIndicador').getStore().load	(
    											{
                                                    url:'../paginasFunciones/funcionesPlaneacionEstrategica.php',
                                                    params:	{
                                                                funcion:55,
                                                                cveArea:gEx('cmdArea').getValue()
                                                            }
                                                }
    										)
}

function agregarFuncionCalculo(i)
{
	var pos=obtenerPosFila(gEx('gGridIndicador').getStore(),'idIndicador',bD(i));
    var filaIndicador=gEx('gGridIndicador').getStore().getAt(pos);
    
	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre,ventana)
                                            {
                                            	function funcAjax()
                                                {
                                                    var resp=peticion_http.responseText;
                                                    arrResp=resp.split('|');
                                                    if(arrResp[0]=='1')
                                                    {
                                                        filaIndicador.set('lblFuncionCalculo',nombre);
                                                        filaIndicador.set('funcionCalculo',idConsulta);
                                                        
                                                        if(gEx('vAgregarExp'))
                                                            gEx('vAgregarExp').close();
                                                    }
                                                    else
                                                    {
                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                    }
                                                }
                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=56&idIndicador='+bD(i)+'&idFuncion='+idConsulta,true);
                                                
                                            	
                                            }
    mostrarVentanaExpresion(function(filaSelec,ventana)
    						{
                            	
                                
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        filaIndicador.set('lblFuncionCalculo',filaSelec.data.nombreConsulta);
                                        filaIndicador.set('funcionCalculo',filaSelec.data.idConsulta);
                                                        
                                        
                                        ventana.close();
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=56&idIndicador='+bD(i)+'&idFuncion='+filaSelec.data.idConsulta,true);
                                
                            }
    						,true);
}

function removerFuncionCalculo(i)
{
	var pos=obtenerPosFila(gEx('gGridIndicador').getStore(),'idIndicador',bD(i));
    var filaIndicador=gEx('gGridIndicador').getStore().getAt(pos);
	function resp(btn)
    {
    	if(btn=='yes')
        {
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    filaIndicador.set('lblFuncionCalculo','');
                    filaIndicador.set('funcionCalculo','-1');
                    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=56&idIndicador='+bD(i)+'&idFuncion=-1',true);
        }
   }
   msgConfirm('Est&aacute; seguro de querer remover la funci&oacute;n de calculo del indicador?',resp);
}