<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idPlanInstitucional,nombrePlan FROM 3200_planesInstitucionales";
	$arrPlanesInstitucionales=$con->obtenerFilasArreglo($consulta);
	
	
	$arrCiclosFiscales="";
	for($x=date("Y");$x<=date("Y")+3;$x++)
	{
		if($arrCiclosFiscales=="")
			$arrCiclosFiscales="['".$x."','".$x."']";
		else
			$arrCiclosFiscales.=",['".$x."','".$x."']";
	}
	
	$consulta="SELECT idRegistro,nombre FROM 3500_clasificadoresObjetosGasto ORDER BY  nombre";
	$arrClasificadoresObjetosGasto=$con->obtenerFilasArreglo($consulta);
?>
var arrClasificadoresObjetosGasto=<?php echo $arrClasificadoresObjetosGasto ?>;
var arrCiclosFiscales=[<?php echo $arrCiclosFiscales?>];
var arrPlanesInstitucionales=<?php echo $arrPlanesInstitucionales?>;

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Estructuras program&aacute;ticas</b></span>',
                                                items:	[
                                                            crearGridEstructurasProgramaticas()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridEstructurasProgramaticas()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idRegistro'},
		                                                {name: 'nombreEstructura'},
                                                        {name: 'descripcion'},
		                                                {name: 'cicloFiscal'},
		                                                {name: 'planInstitucional'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlaneacionEstrategica.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreEstructura', direction: 'ASC'},
                                                            groupField: 'cicloFiscal',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='13';

                                    }
                        )   
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        
                                                        {
                                                            header:'Nombre estructura',
                                                            width:350,
                                                            sortable:true,
                                                            dataIndex:'nombreEstructura',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Ciclo fiscal',
                                                            width:110,
                                                            sortable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return 'Ciclo fiscal: '+val;
                                                                    },
                                                            dataIndex:'cicloFiscal'
                                                        },
                                                        {
                                                            header:'Descripci&oacute;n',
                                                            width:500,
                                                            sortable:true,
                                                            dataIndex:'descripcion',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Plan Institucional asociado',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'planInstitucional',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrPlanesInstitucionales,val);
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gEstructuraProgramatica',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,
                                                            tbar:	[
                                                            			
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',

                                                                            text:'Crear Estructura Program&aacute;tica',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaCrearEstructuraProgramatica();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar Estructura Program&aacute;tica',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gEstructuraProgramatica').getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la estructura program&aacute;tica a modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	abrirEstructuraProgramatica(fila.data.idRegistro)
                                                                                    }
                                                                            
                                                                        }
                                                                        ,'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Estructura Program&aacute;tica',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gEstructuraProgramatica').getSelectionModel().getSelected();
                                                                                        
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la estructura program&aacute;tica que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        gEx('gEstructuraProgramatica').getStore().reload();
                                                                                                        
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=16&idEstructura='+fila.data.idRegistro,true);
                                                                                        	}
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la estructura program&aacute;tica seleccionada?',resp);
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                            		],    
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :true,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: true,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	

}

function mostrarVentanaCrearEstructuraProgramatica(fila)
{
	var cmbClasificador=crearComboExt('cmbCiclo',arrClasificadoresObjetosGasto,280,215,400);
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclosFiscales,110,155,120);
    cmbCiclo.on('select',function(cmb,registro)		
    								{
                                    	function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                var arrDatos=eval(arrResp[1]);
                                             	gEx('cmbPlanInstitucional').getStore().loadData(arrDatos);   	
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=14&ciclo='+registro.data.id,true);
                                        
                                        
                                    }
    	    				)
    
    var cmbPlanInstitucional=crearComboExt('cmbPlanInstitucional',[],180,185,500);
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre de la estructura:'
                                                        },
                                                        {	
                                                        	x:180,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:400,
                                                            id:'nombreEstructura'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n de la estructura:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:700,
                                                            height:60,
                                                            xtype:'textarea',
                                                            id:'txtDescripcion'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Ciclo fiscal:'
                                                        },
                                                        cmbCiclo,
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            html:'Plan Institucional Asociado:'
                                                        },
                                                        cmbPlanInstitucional,
                                                         {
                                                        	x:10,
                                                            y:220,
                                                            html:'Clasificador Por Objeto de Gasto Asociado:'
                                                        },
                                                        cmbClasificador
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !fila?'Crear Estructura program&aacute;tica':'Modificar Estructura program&aacute;tica',
										width: 750,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nombreEstructura').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var nombreEstructura=gEx('nombreEstructura');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        
                                                                        if(nombreEstructura.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	nombreEstructura.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el nombre de la estructura program&aacute;tica',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbCiclo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbCiclo.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el ciclo fiscal al cual pertenece la estructura program&aacute;tica',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbPlanInstitucional.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbPlanInstitucional.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el plan institucional con el cual se asocia la estructura program&aacute;tica',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbClasificador.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	cmbClasificador.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar el clasificador por objeto de gasto con cual se asocia la estructura program&aacute;tica',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idEstructura":"-1","nombreEstructura":"'+cv(nombreEstructura.getValue())+'","descripcion":"'+
                                                                        			cv(txtDescripcion.getValue())+'","cicloFiscal":"'+cmbCiclo.getValue()+'","planInstitucional":"'+
                                                                                    cmbPlanInstitucional.getValue()+'","clasificadorObjetoGasto":"'+cmbClasificador.getValue()+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	abrirEstructuraProgramatica(arrResp[1]);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlaneacionEstrategica.php',funcAjax, 'POST','funcion=15&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function abrirEstructuraProgramatica(idEstructura)
{
	var arrParam=[['idEstructura',idEstructura]];
    enviarFormularioDatos('../planeacionEstrategica/tblEstructuraProgramatica.php',arrParam);
}