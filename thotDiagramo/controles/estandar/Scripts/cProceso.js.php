<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

function cProceso(id,x,y)
{
	cFigura.call(this,id,x,y);
	this.ancho=100;
    this.alto=40;
    this.x2=this.x+this.ancho;
    this.y2=this.y+this.alto;
    this.xInicio=x-1;
    this.xFin=this.x2+1;
    this.yInicio=y-1;
    this.yFin=this.y2+1;
    var xAux=this.x+(this.ancho/2);
    var yAux=this.y+(this.alto/2);
   
    
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_1c',this.x,this.y,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_2c',this.x2,this.y,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_3c',this.x2,this.y2,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_4c',this.x,this.y2,this,4));
    
    
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_5c',xAux,this.y,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_6c',this.x2,yAux,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_7c',x,yAux,this,4));
    this.arrPuntosConexion.push(new cPuntoConexion(id+'_8c',xAux,this.y2,this,4));
    
    
    this.prepararCuadroOperacion();
    
}

cProceso.prototype=new cFigura();

cProceso.prototype.dibujar=function()
							{
                            	if(this.registrado)
                                {
                                    lienzo.strokeStyle = "#000000";
                                    lienzo.lineWidth=1;
                                    lienzo.beginPath();
                                    lienzo.moveTo(this.x,this.y);
                                    lienzo.lineTo(this.x2,this.y);
                                    lienzo.lineTo(this.x2,this.y2);
                                    lienzo.lineTo(this.x,this.y2);
                                    lienzo.lineTo(this.x,this.y);
                                    lienzo.closePath();
                                    lienzo.stroke();
                                    this.puntosConexionDibujados=false;
                                    this.puntosOperacionDibujados=false;
                                    
                                    
                                    
                               }
                            }
                            
                            
cProceso.prototype.dibujarSiluetaFigura=function()
							{
                            
                            	
								lienzo.strokeStyle = "#CCC";
                                lienzo.lineWidth=3;
                            	lienzo.beginPath();
                                
                                lienzo.moveTo(this.x,this.y);
                                lienzo.lineTo(this.x2,this.y);
                                lienzo.lineTo(this.x2,this.y2);
                                lienzo.lineTo(this.x,this.y2);
                                lienzo.lineTo(this.x,this.y);
                                lienzo.closePath();
								lienzo.stroke();
                                this.puntosConexionDibujados=false;
							    this.puntosOperacionDibujados=false;
                                
                                
                            }                            
          
					
cProceso.prototype.setPosX=function(valor)
                            {
                            	var desplazamiento=valor-this.x;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosX(this.arrPuntosConexion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosX(this.arrPuntosOperacion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosX(this.elementosHijos[x].getPosX()+desplazamiento);
                                }
                                
                                this.x=valor;
                                this.x2+=desplazamiento;
                                
                                this.xInicio=this.xInicio+desplazamiento;
                                this.xFin=this.xFin+desplazamiento;
                                
                                
                                
                            };


cProceso.prototype.setPosY=function(valor)
                            {	
                            	var desplazamiento=valor-this.y;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosY(this.arrPuntosConexion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosY(this.arrPuntosOperacion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosY(this.elementosHijos[x].getPosY()+desplazamiento);
                                }
                                
                                this.y=valor;
                                this.y2+=desplazamiento;
                                
                                
                                this.yInicio=this.yInicio+desplazamiento;
                                this.yFin=this.yFin+desplazamiento;
                            };


cProceso.prototype.obtenerPuntoCentral=function()
									{
                                    	var obj={};
                                        
                                        obj.x=this.x+(this.ancho/2);
                                        obj.y=this.y+(this.alto/2);	
        							    return obj;                            
                                    }


