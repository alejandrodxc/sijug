function cTexto(id,x,y)
{
	this.leyenda="Ingrese texto";
    this.font='sans-serif';
    this.tamano=11;
    this.color="000";
	cFigura.call(this,id,x,y);
    
    lienzo.fillStyle="#"+this.color;
	lienzo.font = this.tamano+"px "+this.font;
    
	this.ancho=lienzo.measureText(this.leyenda).width+12;

    this.alto=this.tamano+12;
    this.x2=this.x+this.ancho;
    this.y2=this.y+this.alto;
    this.xInicio=x-1;
    this.xFin=this.x2+1;
    this.yInicio=y-1;
    this.yFin=this.y2+1;
    var xAux=this.x+(this.ancho/2);
    var yAux=this.y+(this.alto/2);
    
    this.prepararCuadroOperacion();
    
    
}

cTexto.prototype=new cFigura();


cTexto.prototype.setTexto=function(texto)
							{
                            	this.leyenda=texto;
                                lienzo.fillStyle="#"+this.color;
                                lienzo.font = this.tamano+"px "+this.font;
                                
                                this.ancho=lienzo.measureText(this.leyenda).width+12;
                            
                                this.alto=this.tamano+12;
                                this.x2=this.x+this.ancho;
                                this.y2=this.y+this.alto;
                                this.xFin=this.x2+1;
                                this.yFin=this.y2+1;
                               
                                
                                this.prepararCuadroOperacion();
                                this.redibujar();
                            }
				

cTexto.prototype.dibujar=function()
							{
                            	if(this.registrado)
                                {
                                
                                	this.dibujarTexto();

                                    this.puntosConexionDibujados=false;
                                    this.puntosOperacionDibujados=false;
                                    
                                    
                                    
                                    
                                    
                                    
                               }
                            }
                            
                            
cTexto.prototype.dibujarSiluetaFigura=function()
							{
                            
                            	
								lienzo.strokeStyle = "#CCC";
                                lienzo.lineWidth=3;
                            	lienzo.beginPath();
                                
                                lienzo.moveTo(this.x,this.y);
                                lienzo.lineTo(this.x2,this.y);
                                lienzo.lineTo(this.x2,this.y2);
                                lienzo.lineTo(this.x,this.y2);
                                lienzo.lineTo(this.x,this.y);
                                lienzo.closePath();
								lienzo.stroke();
                                this.dibujarTexto();

                                this.puntosConexionDibujados=false;
							    this.puntosOperacionDibujados=false;
                                
                                
                            } 
                            
                            
                            
					
cTexto.prototype.setPosX=function(valor)
                            {
                            	var desplazamiento=valor-this.x;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosX(this.arrPuntosConexion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosX(this.arrPuntosOperacion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosX(this.elementosHijos[x].getPosX()+desplazamiento);
                                }
                                
                                this.x=valor;
                                this.x2+=desplazamiento;
                                
                                this.xInicio=this.xInicio+desplazamiento;
                                this.xFin=this.xFin+desplazamiento;
                                
                                
                                
                            };


cTexto.prototype.setPosY=function(valor)
                            {	
                            	var desplazamiento=valor-this.y;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosY(this.arrPuntosConexion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosY(this.arrPuntosOperacion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosY(this.elementosHijos[x].getPosY()+desplazamiento);
                                }
                                
                                this.y=valor;
                                this.y2+=desplazamiento;
                                
                                
                                this.yInicio=this.yInicio+desplazamiento;
                                this.yFin=this.yFin+desplazamiento;
                            };


cTexto.prototype.obtenerPuntoCentral=function()
									{
                                    	var obj={};
                                        
                                        obj.x=this.x+(this.ancho/2);
                                        obj.y=this.y+(this.alto/2);	
        							    return obj;                            
                                    }


cTexto.prototype.dibujarTexto=function()
								{
                                	lienzo.fillStyle="#"+this.color;
                                    lienzo.font = this.tamano+"px "+this.font;
                                    lienzo.fillText(this.leyenda,this.x+6,this.y+5+this.tamano);
                                }                 
                                
                                
cTexto.prototype.registrarFigura=function()
                                {
                                    this.registrado=true;
                                    this.dibujar();
                                }                                
                                
                                

cTexto.prototype.iniciarEdicion=function()
								{
                                	this.enEdicion=true;
                                    
                                    var tamano=this.tamano;
                                    
                                    this.controlEdicion=new Ext.form.TextArea	(
                                                                                    {
                                                                                        id:'ctr_'+id,
                                                                                        autoCreate:{tag:"textarea", autocomplete:"off", wrap:"off"},
                                                                                        width:this.ancho,
                                                                                        height:this.alto,
                                                                                        renderTo:editor.gE('tblLienzo'),
                                                                                        x:this.x,
                                                                                        enableKeyEvents:true,
                                                                                        style:	{position:'absolute',font:this.tamano+'px '+this.font},
                                                                                        value:this.leyenda,
                                                                                        hidden:true,
                                                                                        y:this.y,
                                                                                        listeners:	{
                                                                                        				keypress:function(ctrl,e)
                                                                                                        		{
                                                                                                                	
                                                                                                                	if(e.getKey()=='13')
                                                                                                                    {
                                                                                                                    	var altura=tamano+ctrl.getHeight();
                                                                                                                    	ctrl.setHeight(altura);
                                                                                                                    }
                                                                                                                }
                                                                                        			}
                                                                                    }
                                                                                )
                                    
                                    this.controlEdicion.show();
                                    this.controlEdicion.focus(200,false);
                                    

                                }


cTexto.prototype.finalizarEdicion=function(guardarCambios)
                                    {
                                    	this.enEdicion=false;
                                    	this.controlEdicion.hide();
                                        this.setTexto(this.controlEdicion.getValue());
                                        this.controlEdicion.destroy();
                                    }                                
                                