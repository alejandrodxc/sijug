<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$arrPaletas=array();
	$directorioPATH=$baseDir."/thotDiagramo/controles";
	
	$directorio=dir($directorioPATH);
	$nDirectorio=1;
	while($archivo=$directorio->read())
	{
		if(($archivo!=".")&&($archivo!=".."))
		{
			if(is_dir($directorioPATH."/".$archivo))
			{
				$archivoOpt=$archivo."___".$nDirectorio;
				
				$cadArchivo=leerContenidoArchivo($directorioPATH."/".$archivo."/".$archivo.".json");
				if($cadArchivo!="")
				{
					$oArchivo=json_decode($cadArchivo);	
					if(isset($oArchivo->titulo))
						$archivoOpt=$oArchivo->titulo."___".$nDirectorio;
				}
				
				$arrPaletas[$archivoOpt]=array();
				$directorio2=dir($directorioPATH."/".$archivo);
				while($control=$directorio2->read())
				{
					if(($control!=".")&&($control!="..")&&($control!=$archivo.".json"))
					{
						if(!is_dir($directorioPATH."/".$archivo."/".$control))
						{
							array_push($arrPaletas[$archivoOpt],$directorioPATH."/".$archivo."/".$control);
						}
					}
				}
				$nDirectorio++;
				
				
			}
		}
	}
	
	ksort($arrPaletas);
	
	$arrGrupos="";
	$arrGrupoBotones="";
	$arrScripts="";
	foreach($arrPaletas as $titulo=>$botones)
	{
		
		$aTitulo=explode("___",$titulo);
		
		$oG="['".$aTitulo[1]."','".cv($aTitulo[0])."']";
		if($arrGrupos=="")
			$arrGrupos=$oG;
		else
			$arrGrupos.=",".$oG;
		
		$arrBotones="";
		foreach($botones as $b)
		{
			
			$cadObj=leerContenidoArchivo($b);
			if($cadObj!="")
			{
				$obj=json_decode($cadObj);
				
				
				$btn="	{
							icon:'".$obj->icono."',
							width:32,
							height:32,
							tooltip:'".cv($obj->titulo)."',
							handler:function()
									{
										".$obj->funcion.";
									}
							
						}";
			
				if($arrBotones=="")		
					$arrBotones=$btn;
				else
					$arrBotones.=",".$btn;
					
				if($arrScripts=="")	
					$arrScripts="'".$obj->script."'";
				else
					$arrScripts.=",'".$obj->script."'";
					
			
			}
			
			
		}
		
		$gBoton="{
					id:'gpo_".$aTitulo[1]."',
					xtype: 'buttongroup',
					columns: 6,
					y:5,
					x:5,
					hidden:true,
					baseCls: 'x-plain',
					items: [
							".$arrBotones."	
							 ]
				}";
				
		if($arrGrupoBotones=="")				
			$arrGrupoBotones=$gBoton;
		else
			$arrGrupoBotones.=",".$gBoton;
	}
	
	
	$arrGrupos="[".$arrGrupos."]";
	
	
?>
var editor;
var lienzo;
var arrScripts=[<?php echo $arrScripts?>];
var arrGrupos=<?php echo $arrGrupos?>;
var arrObjetosCanvas;
Ext.onReady(inicializar);

function inicializar()
{
	var x;
    for(x=0;x<arrScripts.length;x++)
    {
    	includeScript(arrScripts[x]);
    }
    
    var cmbPaletas=crearComboExt('cmbPaletas',arrGrupos,0,0,200);
    cmbPaletas.setValue(arrGrupos[0][0]);
    cmbPaletas.on('select',function(cmb,registro)
    						{
                            	var x;
                                for(x=0;x<arrGrupos.length;x++)
                                {
                                	gEx('gpo_'+arrGrupos[x][0]).hide();
                                }
                                gEx('gpo_'+registro.data.id).show();
                                
                            }
    			)
	Ext.QuickTips.init();
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b></b></span>',
                                                items:	[
                                                            new Ext.ux.IFrameComponent({ 
                
                                                                                            id: 'frameContenido', 
                                                                                            anchor:'100% 100%',
                                                                                            region:'center',
                                                                                            loadFuncion:function(iFrame)
                                                                                                        {
                                                                                                            lienzo=gEx('frameContenido').getFrameWindow().gE('cLienzo').getContext("2d");
                                                                                                            arrObjetosCanvas=(gEx('frameContenido').getFrameWindow().arrObjetosCanvas);
                                                                                                            editor=gEx('frameContenido').getFrameWindow();
                                                                                                        },

                                                                                            url: '../paginasFunciones/white.php',
                                                                                            style: 'width:100%;height:100%' 
                                                                                    }),
                                                                                    
															{
                                                            	xtype:'panel',
                                                                width:220,
                                                                layout:'absolute',
                                                                baseCls: 'x-plain',
                                                                collapsible:true,
                                                                region:'west',
                                                                tbar:	[
                                                                			cmbPaletas
                                                                		],
                                                                items:	[
                                                                			<?php
																				echo $arrGrupoBotones;
																			?>
                                                                        
                                                                        ]
                                                            }                                                                                    
                                                                                    
                                                        ]
                                            }
                                         ]
                            }
                        )   
                        
                        
	gEx('frameContenido').load	(
                                    {
                                        url:'../thotDiagramo/editor.php',
                                        scripts:true,
                                        params:	{
                                                    cPagina:'sFrm=true'
                                                    
                                                   
                                                }
                                        
                                    }
                                ) 
                                
	dispararEventoSelectCombo('cmbPaletas');                                                        
                        
}


function agregarControl(obj)
{
	gEx('frameContenido').getFrameWindow().ignorarClick=true;

	gEx('frameContenido').getFrameWindow().insertarObjeto(obj);
}



function generarIDFigura()
{
	var fecha=new Date();
    
	return fecha.format('YmdHis')+'_'+generarNumAleatorio(1,10000)+'_'+generarNumAleatorio(1,10000);
}



function construirFigura(tipoFigura)
{
	var id=generarIDFigura();
    var obj=null;
    eval('obj=new '+tipoFigura+'(id,0,0);');
    agregarControl(obj);
}
