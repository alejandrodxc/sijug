<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var ignorarClick=false;
var accionMouse=0;
var objSeleccionado=null;
var lienzo;
var limitesLienzo;
var posXMouse=0;
var posYMouse=0;
var mousePresionado=false;
var arrObjetosCanvas=[];

var posXSeleccion=0;
var posYSeleccion=0;
var desplazamientoX=0;
var desplazamientoY=0;




Ext.onReady(inicializar);

function inicializar()
{	
	var c=gE('cLienzo');
    limitesLienzo=c.getBoundingClientRect();
    
    
    if(esChrome())
    {
 	
    	/*limitesLienzo.top-=2;
        limitesLienzo.bottom-=2;
        limitesLienzo.left-=2;
        limitesLienzo.right-=2;*/
        
    }
    
    lienzo=c.getContext("2d");
    
}

function obtenerCoordenadas(e)
{
	var X; 
    if(document.all && event.clientX)
    	X=event.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft); 
    else
    	if(e.pageX)
        	X=e.pageX;
        else
        	X=null; 
    
    X-=limitesLienzo.left;
    posXMouse=X;        
    var Y; 
    if(document.all && event.clientY)
    	Y=event.clientY + (document.documentElement.scrollTop || document.body.scrollTop); 
    else
    	if(e.pageY)
        	Y=e.pageY;
        else
            Y=null; 
   Y-=limitesLienzo.top;
   
   posYMouse=Y;
   
   if(mousePresionado)
   {
   		if(objSeleccionado)
        {
            objSeleccionado.limpiarAreaDibujo();
            objSeleccionado.setPosX(posXMouse-desplazamientoX);
            objSeleccionado.setPosY(posYMouse-desplazamientoY);
            objSeleccionado.dibujarSiluetaFigura();
            
        }
   		
   }
   else
   {
   		if((objSeleccionado)&&(objSeleccionado.registrado==false))
        {
        	objSeleccionado.limpiarAreaDibujo();
            objSeleccionado.setPosX(posXMouse-desplazamientoX);
            objSeleccionado.setPosY(posYMouse-desplazamientoY);
        	objSeleccionado.dibujarSiluetaFigura();
        }
   }
   
	
}

function mouseDownClick(event)
{
	mousePresionado=true;
    if(!ignorarClick)
    {
        var o=verificarMouseSobreObjeto(posXMouse,posYMouse);
        seleccionarObjeto(o);
    }
}

function mouseUpClick(event)
{
	mousePresionado=false;
    ignorarClick=false;
    if(objSeleccionado)
    {
    	objSeleccionado.limpiarAreaDibujo();
        objSeleccionado.setPosX(posXMouse-desplazamientoX);
        objSeleccionado.setPosY(posYMouse-desplazamientoY);
        objSeleccionado.dibujar();
        objSeleccionado.dibujarPuntosOperacion();
        objSeleccionado.registrarFigura();
    }
    
    
    
}

function mouseDobleClick(event)
{
	if(objSeleccionado)
    {
    	objSeleccionado.iniciarEdicion();
    }
}


function mouseLeave(event)
{
	mousePresionado=false;
}

function mouseClick(event)
{
	
}

function insertarObjeto(o)
{
	objSeleccionado=o;
    var oCentro=o.obtenerPuntoCentral();
    desplazamientoX=oCentro.x;
    desplazamientoY=oCentro.y;

	arrObjetosCanvas.push(o);
    o.dibujar();
    
}

function redibujarCanvas()
{
	
	var x;
    lienzo.clearRect(0,0,lienzo.canvas.width,lienzo.canvas.height);
    for(x=0;x<arrObjetosCanvas.length;x++)
    {
    	arrObjetosCanvas[x].dibujar();
    }
}

function verificarMouseSobreObjeto(posX,posY)
{
	var x;
    var oSeleccionado=null;
    for(x=0;x<arrObjetosCanvas.length;x++)
    {
    	if(arrObjetosCanvas[x].enLimitesFigura(posX,posY))
        {
        	
        	oSeleccionado=arrObjetosCanvas[x];
        }
        
    }
    return oSeleccionado;
}

function seleccionarObjeto(o)
{
	if(o)
    {
    	posXSeleccion=posXMouse;
        posYSeleccion=posYMouse;
    	if((objSeleccionado==null) || (objSeleccionado.id!=o.id))	
        {
        	if(objSeleccionado)
            {
            	objSeleccionado.desSeleccionarFigura();
            }
            
            objSeleccionado=o;
            desplazamientoX=posXSeleccion-objSeleccionado.getPosX();
            desplazamientoY=posYSeleccion-objSeleccionado.getPosY();
            objSeleccionado.seleccionarFigura();
        }
        else
        {
        	if((objSeleccionado.id==o.id))
            {
           	 	desplazamientoX=posXSeleccion-objSeleccionado.getPosX();
	            desplazamientoY=posYSeleccion-objSeleccionado.getPosY();
            }
        }
    }
    else
    {
    	if(objSeleccionado!=null)
        {
        	objSeleccionado.desSeleccionarFigura();
        }
        objSeleccionado=null;
    }
}

function desSeleccionarObjeto()
{
	if(objSeleccionado)
    	objSeleccionado.desSeleccionarFigura();
}

