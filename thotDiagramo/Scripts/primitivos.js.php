function cFigura(id,x,y)
{
	this.x=x;
    this.y=y;
    this.xInicio=x-1;
    this.xFin=0;
    this.yInicio=y-1;
    this.yFin=0;
    this.id=id;
    this.arrPuntosConexion=[];
    this.arrPuntosOperacion=[];
    this.elementosHijos=[];
    this.arrElementos=[];
    this.seleccionado=false;
    this.puntosConexionDibujados=false;
    this.puntosOperacionDibujados=false;
    this.visible=false;
    this.areaRedibujo=null;
    this.registrado=false;
    this.areaDibujoTemp=null;
    this.recalcularAreaDibujo=true;
    this.enEdicion=false;
}


cFigura.prototype.prepararCuadroOperacion=function ()
                                            {
                                            	var xAux;
                                                var yAux;
                                            	
                                                xAux=((this.xFin-1)-(this.xInicio+1))/2;
                                                
                                                
                                                yAux=((this.yFin-1)-(this.yInicio+1))/2;
                                            
                                            	var xMedio=this.xInicio+1+xAux;
                                                
                                                
                                                var yMedio=this.yInicio+1+yAux;
                                                if(this.arrPuntosOperacion.length>0)
                                                	this.arrPuntosOperacion=new Array();
                                                    
                                                this.arrPuntosOperacion.push(new cPuntoRotacion(id+'_1r',xMedio,this.yInicio-25,this,4));
                                                
                                                
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_1o',this.xInicio+1,this.yInicio+1,this,3));
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_5o',xMedio,this.yInicio+1,this,3));
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_2o',this.xFin-1,this.yInicio+1,this,3));
                                                
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_6o',this.xFin-1,yMedio,this,3));
                                                
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_3o',this.xFin-1,this.yFin-1,this,3));
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_8o',xMedio,this.yFin-1,this,3));
                                                
                                                
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_4o',this.xInicio+1,this.yFin-1,this,3));
                                                
                                                
                                                this.arrPuntosOperacion.push(new cPuntoOperacion(id+'_7o',this.xInicio+1,yMedio,this,3));
                                               
                                               
                                               
                                               
                                               
                                               
                                            }

cFigura.prototype.dibujar=function()
                            {
                            	
                            };


cFigura.prototype.setPosX=function(valor)
                            {
                            	var desplazamiento=valor-this.x;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosX(this.arrPuntosConexion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosX(this.arrPuntosOperacion[x].getPosX()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosX(this.elementosHijos[x].getPosX()+desplazamiento);
                                }
                                
                                this.x=valor;
                                
                                this.xInicio=this.xInicio+desplazamiento;
                                this.xFin=this.xFin+desplazamiento;
                                
                                
                                
                            };


cFigura.prototype.getPosX=function()
                            {	
                            	return this.x;
                            };

cFigura.prototype.setPosY=function(valor)
                            {	
                            	var desplazamiento=valor-this.y;
                                var x;
                                for(x=0;x<this.arrPuntosConexion.length;x++)
                                {
                                    this.arrPuntosConexion[x].setPosY(this.arrPuntosConexion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.arrPuntosOperacion.length;x++)
                                {
                                    this.arrPuntosOperacion[x].setPosY(this.arrPuntosOperacion[x].getPosY()+desplazamiento);
                                }
                                
                                for(x=0;x<this.elementosHijos.length;x++)
                                {
                                    this.elementosHijos[x].setPosY(this.elementosHijos[x].getPosY()+desplazamiento);
                                }
                                
                                this.y=valor;
                                this.yInicio=this.yInicio+desplazamiento;
                                this.yFin=this.yFin+desplazamiento;
                            };


cFigura.prototype.getPosY=function()
                            {	
                            	return this.y;
                            };

cFigura.prototype.enLimitesFigura=function(posX,posY)
                                {
                                    if((posX>=this.xInicio)&&(posX<=this.xFin)&&(posY>=this.yInicio)&&(posY<=this.yFin))
                                    {
                                    	return true;
                                    }
                                    return false;
                                } 


cFigura.prototype.enLimitesAreaFigura=function(posX,posY)
                                {
                                	if(this.recalcularAreaDibujo)
	                                	this.areaDibujoTemp=this.obtenerAreaDibujo();
                                	var area=this.areaDibujoTemp;
                                    if((posX>=area.x1)&&(posX<=area.x2)&&(posY>=area.y1)&&(posY<=area.y2))
                                    {
                                    	return true;
                                    }
                                    return false;
                                } 


cFigura.prototype.enLimiteAreaFigura=function(posX1,posY1,posX2,posY2,posX3,posY3,posX4,posY4,ojReferencia)
									{
                                    	if(this.enLimitesAreaFigura(posX1,posY1))
                                        	return true;
                                        this.recalcularAreaDibujo=false;
                                        if(this.enLimitesAreaFigura(posX2,posY2))
                                        	return true;    
                                        if(this.enLimitesAreaFigura(posX3,posY3))
                                        	return true;    
                                        if(this.enLimitesAreaFigura(posX4,posY4))
                                        	return true;            
                                        this.recalcularAreaDibujo=true;    
                                        
                                        if(ojReferencia)
                                        {
                                        	if(ojReferencia.enLimitesAreaFigura(this.areaDibujoTemp.x1,this.areaDibujoTemp.y1))
                                            	return true;
                                            
                                            if(ojReferencia.enLimitesAreaFigura(this.areaDibujoTemp.x2,this.areaDibujoTemp.y1))
                                            	return true; 
                                                
                                            if(ojReferencia.enLimitesAreaFigura(this.areaDibujoTemp.x2,this.areaDibujoTemp.y2))
                                            	return true; 
                                                
                                                
                                            if(ojReferencia.enLimitesAreaFigura(this.areaDibujoTemp.x1,this.areaDibujoTemp.y2))
                                            	return true;     
                                            
                                        }
                                        
                                            
                                    	return false;
                                    }

cFigura.prototype.dibujarPuntosConexion=function()
										{
                                        	var x;
                                            
                                            
                                            
                                            for(x=0;x<this.arrPuntosConexion.length;x++)
                                            {
                                                
                                                this.arrPuntosConexion[x].dibujar();
                                            }
                                            this.puntosConexionDibujados=true;
                                        }
                                        

cFigura.prototype.dibujarPuntosOperacion=function()
										{
                                        	
                                            
                                            var pRotacion=this.arrPuntosOperacion[0];
                                            var pDestino=this.arrPuntosOperacion[2];
                                            
                                            
                                            lienzo.beginPath();
                                            lienzo.moveTo(pRotacion.getPosX(),pRotacion.getPosY()+pRotacion.anchoRadio);
                                            lienzo.lineWidth = 1;

                                            lienzo.lineTo(pDestino.getPosX(),pDestino.getPosY()-pDestino.anchoRadio);
                                            lienzo.closePath();
                                            lienzo.strokeStyle = "#BBB";
											lienzo.stroke();
                                            
                                            
                                            lienzo.beginPath();
                                            lienzo.moveTo(this.arrPuntosOperacion[1].getPosX(),this.arrPuntosOperacion[1].getPosY());
                                            lienzo.lineTo(this.arrPuntosOperacion[3].getPosX(),this.arrPuntosOperacion[3].getPosY());
                                            lienzo.lineTo(this.arrPuntosOperacion[5].getPosX(),this.arrPuntosOperacion[5].getPosY());
                                            lienzo.lineTo(this.arrPuntosOperacion[7].getPosX(),this.arrPuntosOperacion[7].getPosY());
                                            lienzo.lineTo(this.arrPuntosOperacion[1].getPosX(),this.arrPuntosOperacion[1].getPosY());
                                            
                                            lienzo.lineWidth = 1;

                                            
                                            lienzo.closePath();
                                            lienzo.strokeStyle = "#BBB";
                                            
											lienzo.stroke();
                                            var x;
                                            for(x=0;x<this.arrPuntosOperacion.length;x++)
                                            {
                                                
                                                this.arrPuntosOperacion[x].dibujar();
                                            }
                                            this.puntosOperacionDibujados=true;
                                            

                                        }
                                        
cFigura.prototype.dibujarElementosHijos=function()
										{
                                        	var x;
                                            for(x=0;x<this.elementosHijos.length;x++)
                                            {
                                                
                                                this.elementosHijos[x].dibujar();
                                            }
                                            
                                            

                                        }
                                        
cFigura.prototype.seleccionarFigura=function()                                        
									{
                                    	this.seleccionado=true;
                                    	this.dibujarPuntosOperacion();
                                        
                                    }
                                    
cFigura.prototype.desSeleccionarFigura=function()                                        
									{
                                    	this.seleccionado=false;
                                        if(this.enEdicion)
	                                        this.finalizarEdicion(true);
                                    	this.redibujar();
                                        
                                    }                                    


cFigura.prototype.obtenerPuntoCentral=function()
									{
                                    	
                                        
                                    }
                                    
                                    
                                    
                                    
                                        
/*cFigura.prototype.desDibujarPuntosConexion=function()
										{
                                        	var x;
                                            for(x=0;x<this.arrPuntosConexion.length;x++)
                                            {
                                                
                                                this.arrPuntosConexion[x].dibujar();
                                            }
                                        }                                        
*/


/*cFigura.prototype.desDibujarPuntosOperacion=function()
										{
                                        	
                                            
                                            
                                            

                                        }    */
                      
                      
                      
                                        
cFigura.prototype.obtenerAreaDibujo=function()
									{
                                    
                                    	var obj={};
                                        
                                        obj.x1=this.arrPuntosOperacion[1].getPosX()-this.arrPuntosOperacion[1].anchoRadio-1;
                                        obj.x2=this.arrPuntosOperacion[3].getPosX()+this.arrPuntosOperacion[3].anchoRadio+1;
                                        obj.y1=this.arrPuntosOperacion[0].getPosY()-this.arrPuntosOperacion[0].anchoRadio-1;
                                        obj.y2=this.arrPuntosOperacion[5].getPosY()+this.arrPuntosOperacion[5].anchoRadio+1;
                                        return obj;
                                    }                                                                            

cFigura.prototype.redibujar=function()
								{
                                	this.limpiarAreaDibujo();
                                    this.dibujar();
                                }
                                
cFigura.prototype.limpiarAreaDibujo=function()
                                    {
                                    	editor.redibujarCanvas();
/*                                            var area=this.obtenerAreaDibujo();
                                            lienzo.clearRect(area.x1,area.y1,(area.x2-area.x1),(area.y2-area.y1));
                                            this.redibujarFigurasAfectadas();
*/										                                        
                                    }                                
                                

cFigura.prototype.dibujarSiluetaFigura=function()
										{
                                        
                                        }


cFigura.prototype.redibujarFigurasAfectadas=function()
                                        {
                                            var arrFiguras=[];
                                            var x;
                                            var areaDibujo=this.obtenerAreaDibujo();
                                            
                                            for(x=0;x<arrObjetosCanvas.length;x++)
                                            {
                                            	if(arrObjetosCanvas[x].id!=this.id)
                                                {
                                                    if(arrObjetosCanvas[x].enLimiteAreaFigura(areaDibujo.x1,areaDibujo.y1,areaDibujo.x2,areaDibujo.y1,areaDibujo.x2,areaDibujo.y2,areaDibujo.x1,areaDibujo.y2,this))
                                                    {
                                                        arrObjetosCanvas[x].redibujar();
                                                    }
                                                }
                                            }
                                            
                                        
                                        }


cFigura.prototype.registrarFigura=function()
									{
                                    	this.registrado=true;
                                    }
					

cFigura.prototype.iniciarEdicion=function()
								{
                                
                                }


cFigura.prototype.finalizarEdicion=function(guardarCambios)
                                    {
                                    
                                    }


///--------------- cPuntoAccion

function cPuntoAccion(id,x,y,padre,anchoRadio)
{
	this.padre=padre;
	this.x=x;
    this.y=y;
    this.id=id;
    this.anchoRadio=anchoRadio;
    this.xInicio=x-1;
    this.xFin=x+this.anchoRadio+1;
    this.yInicio=y-1;
    this.yFin=y+this.anchoRadio+1;
    this.seleccionado=false;
    this.visible=false;
    this.color="#09FF00";
    this.tipo=0;
}

cPuntoAccion.prototype.dibujar=function()
                                {
                                    lienzo.fillStyle = this.color;
                                    lienzo.strokeStyle = "#000";
                                    lienzo.lineWidth = 1;
                                    lienzo.beginPath();
                                    lienzo.arc(this.x,this.y,this.anchoRadio,0,(Math.PI*2),true);
                                    lienzo.closePath;
                                    lienzo.fill();
                                    lienzo.stroke();
                                    this.visible=false;
                                } 

cPuntoAccion.prototype.setPosX=function(valor)
                                {
                                    this.x=valor;
                                    this.xInicio=valor-1;
								    this.xFin=valor+this.anchoRadio+1;
                                } 

cPuntoAccion.prototype.getPosX=function()
                                {
                                    return this.x;
                                } 

cPuntoAccion.prototype.setPosY=function(valor)
                                {
                                    this.y=valor;
                                    this.yInicio=valor-1;
								    this.yFin=valor+this.anchoRadio+1;
                                }  
                
cPuntoAccion.prototype.getPosY=function()
                            {
                                return this.y;
                            }

cPuntoAccion.prototype.enLimitesFigura=function(posX,posY)
                                {
                                	if(this.visible)
                                    {
                                        if((posX>=this.xInicio)&&(posX<=this.xFin)&&(posY>=this.yInicio)&&(posY<=this.yFin))
                                        {
                                            return true;
                                        }
                                    }
                                    return false;
                                } 

cPuntoAccion.prototype.dibujarSeleccion=function()
                                        {
                                           
                                            
                                            lienzo.fillStyle = "#FFFF00";
                                            lienzo.strokeStyle = "#000";
                                            lienzo.lineWidth = 1;
                                            lienzo.beginPath();
                                            lienzo.arc(this.x,this.y,this.anchoRadio,0,(Math.PI*2),true);
                                            lienzo.closePath;
                                            lienzo.fill();
                                            lienzo.stroke();
                                            this.seleccionado=true;
                                        } 

cPuntoAccion.prototype.desDibujarSeleccion=function()
                                        {
                                           
                                            if(this.seleccionado)
                                            {
                                                lienzo.fillStyle = "#09FF00";
                                                lienzo.strokeStyle = "#000";
                                                lienzo.lineWidth = 1;
                                                lienzo.beginPath();
                                                lienzo.arc(this.x,this.y,this.anchoRadio,0,(Math.PI*2),true);
                                                lienzo.closePath;
                                                lienzo.fill();
                                                lienzo.stroke();
                                                seleccionado=false;
                                            }
                                            
                                        } 

///--------------- cPuntoConexion
function cPuntoConexion(id,x,y,padre,anchoRadio)
{
	cPuntoAccion.call(this,id,x,y,padre,anchoRadio); 
    this.color="#FFFF00";  
    this.tipo=1;                
}

cPuntoConexion.prototype= new cPuntoAccion();

///--------------- cPuntoOperacion

function cPuntoOperacion(id,x,y,padre,anchoRadio)
{
	cPuntoAccion.call(this,id,x,y,padre,anchoRadio);	                       
    this.tipo=2;
}

cPuntoOperacion.prototype= new cPuntoAccion();


///--------------- cPuntoRotacion

function cPuntoRotacion(id,x,y,padre,anchoRadio)
{
	
	cPuntoAccion.call(this,id,x,y,padre,anchoRadio);	                       
    this.color="#FF0000";  
    this.tipo=3;
}

cPuntoRotacion.prototype= new cPuntoAccion();



