<?php session_start();
	include_once("latis/conexionBD.php");
	include_once("latis/funcionesActores.php");
	include_once("latis/utiles.php");
	


	
	if(isset($_POST["parametros"]))
		$parametros=$_POST["parametros"];
	if(isset($_POST["funcion"]))
		$funcion=$_POST["funcion"];
	$lenguaje=$_SESSION["leng"];
	
	switch($funcion)
	{
		case 1:
			obtenerInfoCodigoProcesoUnico();
		break;
		case 2:
			obtenerPlantillaNotificacion();
		break;
		case 3:
			obtenerDocumentosGeneradosAudiencia();
		break;
		case 4:
			registrarTipoDocumentoGeneradosAudiencia();
		break;
		case 5:
			obtenerActorTipoDocumentoGeneradosAudiencia();
		break;
		case 6:
			cancelarDocumentoAudiencia();
		break;
		case 7:
			obtenerResultadosBusquedaProcesoGeneral();
		break;
		case 8:
			registrarNotificacionMensajeSedeJudicial();
		break;
		case 9:
			obtenerReporteEstado();
		break;
		case 10:
			obtenerTutelasRecibidas();
		break;
		case 11:	
			crearPaqueteTutelas();
		break;
		case 12:
			obtenerPaqueteTutelasRecibidas();
		break;
		case 13:
			registrarTutelasPaquete();
		break;
		case 14:	
			removerTutelasPaquete();
		break;
		case 15:	
			obtenerInformacionTutela();
		break;
		case 16:	
			obtenerVotacionesMagistradosRevisionTutela();
		break;
		case 17:
			aperturarVotacionMagistrado();
		break;
		case 18:
			registrarResultadoVotacion();
		break;
	}


	function obtenerInfoCodigoProcesoUnico($cupj="")
	{
		global $con;
		
		$vReasignacion=0;
		if(isset($_POST["vReasignacion"]))
			$vReasignacion=$_POST["vReasignacion"];
		
		
		if(isset($_POST["cupj"]))
			$cupj=$_POST["cupj"];
		
		$lblNombreAuto="";
		$arrCUPS=explode(",",$cupj);
		
		
		$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$arrCUPS[0]."'";
		$fRegistroCarpeta=$con->obtenerPrimeraFilaAsoc($consulta);
		
		if($fRegistroCarpeta["tipoCarpetaAdministrativa"]==3)
		{
			
			$arrCarpetas=array();
			obtenerCarpetasPadre($arrCUPS[0],$arrCarpetas);
			
			if(sizeof($arrCarpetas)==0)
			{
				array_push($arrCarpetas,$carpetaAdministrativo);
			}
			
			$carpetaAdministrativaBase=$arrCarpetas[0];	
			
			$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativaBase."' and tipoCarpetaAdministrativa=1";
			$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
			
			
			$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fRegistro["unidadGestion"]."'";
			$despacho=$con->obtenerValor($consulta);
			
			$especialidad="";
			$claseProceso="";
			$subClaseProceso="";
			$tema="";
			$subTemaProceso="";
			$tipoProceso="";
			
			if($fRegistro["tipoProceso"]!=6)
			{
				$consulta="SELECT nombreEspecialidadDespacho FROM _637_tablaDinamica WHERE id__637_tablaDinamica=".$fRegistro["especialidad"];
				$especialidad=$con->obtenerValor($consulta);
				
				$consulta="SELECT nombreClaseProceso FROM _626_tablaDinamica WHERE id__626_tablaDinamica=".$fRegistro["claseProceso"];
				$claseProceso=$con->obtenerValor($consulta);
				
				$consulta="SELECT nombreSubclaseProceso FROM _627_tablaDinamica WHERE id__627_tablaDinamica=".$fRegistro["subclaseProceso"];
				$subClaseProceso=$con->obtenerValor($consulta);
				
				$consulta="SELECT nombreTema FROM _628_tablaDinamica WHERE id__628_tablaDinamica=".$fRegistro["tema"];
				$tema=$con->obtenerValor($consulta);
				
				$consulta="SELECT nombreSubtema FROM _629_tablaDinamica WHERE id__629_tablaDinamica=".$fRegistro["subtema"];
				$subTemaProceso=$con->obtenerValor($consulta);
				
				$consulta="SELECT nombreTipoProceso FROM _625_tablaDinamica WHERE id__625_tablaDinamica=".$fRegistro["tipoProceso"];
				$tipoProceso=$con->obtenerValor($consulta);
			}
			else
			{
				$consulta="SELECT nombreTipoProceso FROM _625_tablaDinamica WHERE id__625_tablaDinamica=".$fRegistro["tipoProceso"];
				$tipoProceso=$con->obtenerValor($consulta);
			}
			
			
			
			$demantante="";
			
			$idFiguraActor=2;
			$idFiguraDemandado=2;
			if($fRegistro["tipoProceso"]==6)
			{
				$idFiguraActor=7;
				$idFiguraDemandado=8;
			}
			
			
			$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
						FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
						AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in (SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='A') ORDER BY nombre,nombre,apellidoMaterno";
			
			$res=$con->obtenerFilas($consulta);
			while($filaImputado=mysql_fetch_row($res))
			{
				$nombre=trim($filaImputado[0]);
				if($demantante=="")
					$demantante=$nombre;
				else
					$demantante.=", ".$nombre;
			}
			
			$demandados="";
			$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
						FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
						AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in (SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='D') ORDER BY nombre,nombre,apellidoMaterno";
			
			$res=$con->obtenerFilas($consulta);
			while($filaImputado=mysql_fetch_row($res))
			{
				$nombre=trim($filaImputado[0]);
				if($demandados=="")
					$demandados=$nombre;
				else
					$demandados.=", ".$nombre;
			}
			
			
			$arrCarpetas=array();
			obtenerCarpetasPadre($fRegistro["carpetaAdministrativa"],$arrCarpetas);
			if(sizeof($arrCarpetas)==0)
			{
				array_push($arrCarpetas,$fRegistro["carpetaAdministrativa"]);
			}
			
			$carpetaBase=$arrCarpetas[0];
			
			$lblDespachoSegundaInstancia="";
			$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$arrCUPS[0]."' and tipoCarpetaAdministrativa=3 order by idCarpeta desc";
			$fRegistro2=$con->obtenerPrimeraFilaAsoc($consulta);
			
			if($fRegistro2)
			{
				$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fRegistro2["unidadGestion"]."'";
				$despacho2=$con->obtenerValor($consulta);
				if($vReasignacion==0)
					$lblDespachoSegundaInstancia="<td width='185' class='TSJDF_Etiqueta'>Despacho Segunda Instancia: </b></td><td class='TSJDF_Control' >".cv($despacho2)."</td>";

				$consulta="SELECT * FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$fRegistro2["idRegistro"];
				$fDatosApelacion=$con->obtenerPrimeraFilaAsoc($consulta);
				
				$consulta="SELECT contenido FROM 902_opcionesFormulario WHERE idGrupoElemento=13166 AND valor='".$fDatosApelacion["tipoApelacion"]."'";
				$tipoApelacion=$con->obtenerValor($consulta);
				
				if($fDatosApelacion["tipoApelacion"]==1)
				{
					$consulta="SELECT nomArchivoOriginal FROM 908_archivos WHERE idArchivo=".$fDatosApelacion["autoRecurso"];
					$nombreAuto=$con->obtenerValor($consulta);
					$tipoApelacion.=": ".$nombreAuto."";
					$lblNombreAuto="<tr height='21'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Tipo de Apelaci&oacute;n:</span></td><td class='TSJDF_Control'>".cv($tipoApelacion)."</td></tr>";

				}
				

				
			}
			
			
			$departamento="";
			$municipio="";
			$leyenda="";
			$fRegistroBase=array();
		
			$consulta="SELECT * FROM _632_tablaDinamica WHERE carpetaAdministrativa='".$arrCarpetas[0]."'";
			$fRegistroBase=$con->obtenerPrimeraFilaAsoc($consulta);
			$consulta="SELECT estado FROM 820_estados WHERE cveEstado='".$fRegistroBase["departamento"]."'";
			$departamento=$con->obtenerValor($consulta);
		
			$consulta="SELECT municipio FROM 821_municipios WHERE cveMunicipio='".$fRegistroBase["municipio"]."'";
			$municipio=$con->obtenerValor($consulta);
			$leyenda="<div style='line-height:21px'><b>T&iacute;tulo del Proceso:</b> ".cv($fRegistroBase["tituloProceso"])."<br><b>Especialidad:</b> ".cv($especialidad).", <b>Tipo de Proceso:</b> ".
				cv($tipoProceso).", <b>Lugar de Radicaci&oacute;n:</b> ".cv($municipio).", ".cv($departamento)."<br><b>Demandante: </b> ".$demantante.
				"<br><b>Demandados: </b>".$demandados."<br><b>Despacho:</b> ".cv($despacho.$lblDespachoSegundaInstancia)."</div>";
		
		
			$leyenda="<table>";
			$leyenda.="<tr height='19'><td width='140'><span class='TSJDF_Etiqueta'>T&iacute;tulo del Proceso:</span></td><td width='1100'><span class='TSJDF_Control'> ".cv($fRegistroBase["tituloProceso"])."</span></td></tr>";
			$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Especialidad:</span></td><td width='150'><span class='TSJDF_Control'> ".cv($especialidad).
						"</span></td><td width='125'><span class='TSJDF_Etiqueta'>Tipo de Proceso:</span></td><td width='150'><span class='TSJDF_Control'>".cv($tipoProceso).
						"</span></td><td width='140'><span class='TSJDF_Etiqueta'>Lugar de Radicaci&oacute;n:</span></td><td width='170'><span class='TSJDF_Control'>".cv($municipio)."</span></td></tr></table></td></tr>";
			
			
			$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Demandante:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($demantante).
						"</span></td><td width='125'><span class='TSJDF_Etiqueta'>Demandado:</span></td><td width='430'><span class='TSJDF_Control'>".cv($demandados).
						"</span></td></tr></table></td></tr>";

			$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Despacho:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($despacho).
						"</span></td>".$lblDespachoSegundaInstancia."</tr>".$lblNombreAuto;
			
			
			$leyenda.="</table>";
			
			
			
			
			
			
			$obj='{"despacho":"'.cv($despacho).'","especialidad":"'.cv($especialidad).'","claseProceso":"'.cv($claseProceso).
					'","subClaseProceso":"'.cv($subClaseProceso).'","tema":"'.cv($tema).'","subTemaProceso":"'.cv($subTemaProceso).'","tipoProceso":"'.
				cv($tipoProceso).'","tituloProceso":"'.cv(isset($fRegistroBase["tituloProceso"])?$fRegistroBase["tituloProceso"]:"").'","demandantes":"'.cv($demantante).
				'","demandado":"'.cv($demandados).'","departamento":"'.cv($departamento).'","municipio":"'.cv($municipio).
				'","leyenda":"'.$leyenda.'"}';
		
			echo "1|".$obj;
		}
		else
		{
			if($fRegistroCarpeta["tipoCarpetaAdministrativa"]==40)
			{
				$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fRegistroCarpeta["unidadGestion"]."'";
				$despacho=$con->obtenerValor($consulta);
				$especialidad="";
				$claseProceso="";
				$subClaseProceso="";
				$tema="";
				$subTemaProceso="";
				$tipoProceso=6;
				$leyenda="<table>";
				
				$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Despacho:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($despacho).
							"</span></td></tr>".$lblNombreAuto;
				
				
				$leyenda.="</table>";
				
				
				
				
				
				
				$obj='{"despacho":"'.cv($despacho).'","especialidad":"'.cv($especialidad).'","claseProceso":"'.cv($claseProceso).
						'","subClaseProceso":"'.cv($subClaseProceso).'","tema":"'.cv($tema).'","subTemaProceso":"'.cv($subTemaProceso).'","tipoProceso":"'.
					cv($tipoProceso).'","tituloProceso":"","demandantes":"","demandado":"","departamento":"","municipio":"","leyenda":"'.cv($leyenda).'"}';
			
				echo "1|".$obj;
				
			}
			else
			{
				$arrCarpetas=array();
				obtenerCarpetasPadre($arrCUPS[0],$arrCarpetas);
				
				if(sizeof($arrCarpetas)==0)
				{
					array_push($arrCarpetas,$arrCUPS[0]);
				}
	
				$carpetaBase=$arrCarpetas[0];
				
				$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaBase."' and tipoCarpetaAdministrativa in(1)";
				$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
				
				
				
				$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fRegistro["unidadGestion"]."'";
				$despacho=$con->obtenerValor($consulta);
				
				$especialidad="";
				$claseProceso="";
				$subClaseProceso="";
				$tema="";
				$subTemaProceso="";
				$tipoProceso="";
				
				if($fRegistro["tipoProceso"]!=6)
				{
					$consulta="SELECT nombreEspecialidadDespacho FROM _637_tablaDinamica WHERE id__637_tablaDinamica=".$fRegistro["especialidad"];
					$especialidad=$con->obtenerValor($consulta);
					
					$consulta="SELECT nombreClaseProceso FROM _626_tablaDinamica WHERE id__626_tablaDinamica=".$fRegistro["claseProceso"];
					$claseProceso=$con->obtenerValor($consulta);
					
					$consulta="SELECT nombreSubclaseProceso FROM _627_tablaDinamica WHERE id__627_tablaDinamica=".$fRegistro["subclaseProceso"];
					$subClaseProceso=$con->obtenerValor($consulta);
					
					$consulta="SELECT nombreTema FROM _628_tablaDinamica WHERE id__628_tablaDinamica=".$fRegistro["tema"];
					$tema=$con->obtenerValor($consulta);
					
					$consulta="SELECT nombreSubtema FROM _629_tablaDinamica WHERE id__629_tablaDinamica=".$fRegistro["subtema"];
					$subTemaProceso=$con->obtenerValor($consulta);
					
					$consulta="SELECT nombreTipoProceso FROM _625_tablaDinamica WHERE id__625_tablaDinamica=".$fRegistro["tipoProceso"];
					$tipoProceso=$con->obtenerValor($consulta);
				}
				else
				{
					$consulta="SELECT nombreTipoProceso FROM _625_tablaDinamica WHERE id__625_tablaDinamica=".$fRegistro["tipoProceso"];
					$tipoProceso=$con->obtenerValor($consulta);
				}
				
				
				
				$demantante="";
				
				
				
				
				$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
							FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
							AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in (SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='A') ORDER BY nombre,nombre,apellidoMaterno";
				
				$res=$con->obtenerFilas($consulta);
				while($filaImputado=mysql_fetch_row($res))
				{
					$nombre=trim($filaImputado[0]);
					if($demantante=="")
						$demantante=$nombre;
					else
						$demantante.=", ".$nombre;
				}
				
				$demandados="";
				$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
							FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
							AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in (SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='D') ORDER BY nombre,nombre,apellidoMaterno";
				
				$res=$con->obtenerFilas($consulta);
				while($filaImputado=mysql_fetch_row($res))
				{
					$nombre=trim($filaImputado[0]);
					if($demandados=="")
						$demandados=$nombre;
					else
						$demandados.=", ".$nombre;
				}
				
				
				
				
				$lblDespachoSegundaInstancia="";
				$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$arrCUPS[0]."' and tipoCarpetaAdministrativa=2 order by idCarpeta DESC";
				$fRegistro2=$con->obtenerPrimeraFilaAsoc($consulta);
				
				if(($fRegistro2)&&($vReasignacion==0))
				{
					$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fRegistro2["unidadGestion"]."'";
					$despacho2=$con->obtenerValor($consulta);
					$lblDespachoSegundaInstancia="<td width='185' class='TSJDF_Etiqueta'>Despacho Segunda Instancia: </b></td><td class='TSJDF_Control' >".cv($despacho2)."</td>";
					
				}
				
				
				$departamento="";
				$municipio="";
				$leyenda="";
				$fRegistroBase=array();
				if($fRegistro["tipoProceso"]!=6)
				{
					$consulta="SELECT * FROM _632_tablaDinamica WHERE carpetaAdministrativa='".$arrCarpetas[0]."'";
					$fRegistroBase=$con->obtenerPrimeraFilaAsoc($consulta);
					$consulta="SELECT estado FROM 820_estados WHERE cveEstado='".$fRegistroBase["departamento"]."'";
					$departamento=$con->obtenerValor($consulta);
				
					$consulta="SELECT municipio FROM 821_municipios WHERE cveMunicipio='".$fRegistroBase["municipio"]."'";
					$municipio=$con->obtenerValor($consulta);
					/*$leyenda="<div style='line-height:21px'><b>T&iacute;tulo del Proceso:</b> ".cv($fRegistroBase["tituloProceso"])."<br><b>Especialidad:</b> ".cv($especialidad).", <b>Tipo de Proceso:</b> ".
						cv($tipoProceso).", <b>Lugar de Radicaci&oacute;n:</b> ".cv($municipio).", ".cv($departamento)."<br><b>Demandante: </b> ".$demantante.
						"<br><b>Demandados: </b>".$demandados."<br><b>Despacho:</b> ".cv($despacho.$lblDespachoSegundaInstancia)."</div>";*/
				
				
					$leyenda="<table>";
					$leyenda.="<tr height='19'><td width='140'><span class='TSJDF_Etiqueta'>T&iacute;tulo del Proceso:</span></td><td width='1100'><span class='TSJDF_Control'> ".cv($fRegistroBase["tituloProceso"])."</span></td></tr>";
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Especialidad:</span></td><td width='150'><span class='TSJDF_Control'> ".cv($especialidad).
								"</span></td><td width='125'><span class='TSJDF_Etiqueta'>Tipo de Proceso:</span></td><td width='150'><span class='TSJDF_Control'>".cv($tipoProceso).
								"</span></td><td width='140'><span class='TSJDF_Etiqueta'>Lugar de Radicaci&oacute;n:</span></td><td width='170'><span class='TSJDF_Control'>".cv($municipio)."</span></td></tr></table></td></tr>";
					
					
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Demandante:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($demantante).
								"</span></td><td width='125'><span class='TSJDF_Etiqueta'>Demandado:</span></td><td width='430'><span class='TSJDF_Control'>".cv($demandados).
								"</span></td></tr></table></td></tr>";
	
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Despacho:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($despacho).
								"</span></td>".$lblDespachoSegundaInstancia."</tr>";
					
					
					$leyenda.="</table>";
				
				}
				else
				{
					$fRegistroBase["tituloProceso"]="";
					$consulta="SELECT * FROM _847_tablaDinamica WHERE carpetaAdministrativa='".$arrCarpetas[0]."'";
					$fRegistroBase=$con->obtenerPrimeraFilaAsoc($consulta);
					$consulta="SELECT estado FROM 820_estados WHERE cveEstado='".$fRegistroBase["DepartamentoRegistroTutela"]."'";
					$departamento=$con->obtenerValor($consulta);
				
					$consulta="SELECT municipio FROM 821_municipios WHERE cveMunicipio='".$fRegistroBase["ciudadRegistroTutela"]."'";
					$municipio=$con->obtenerValor($consulta);
					$leyenda="<div style='line-height:21px'><b>Tipo de Proceso:</b> ".
						cv($tipoProceso).",<br><b>Lugar de Radicaci&oacute;n:</b> ".cv($municipio).", ".cv($departamento)."<br><b>Accionantes: </b> ".$demantante.
						"<br><b>Accionados: </b>".$demandados."<br><b>Despacho:</b> ".cv($despacho.$lblDespachoSegundaInstancia)."</div>";
				
					$leyenda="<table id='principal'>";
					$leyenda.="<tr height='19'><td width='140'><span class='TSJDF_Etiqueta'>Tipo de Proceso:</span></td><td width='1100'><span class='TSJDF_Control'> ".cv($tipoProceso)."</span></td></tr>";
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Lugar de Radicaci&oacute;n:</span></td><td width='400'><span class='TSJDF_Control'> ".cv($municipio).
							", ".cv($departamento)."</span></td></tr></table></td></tr>";
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Accionantes:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($demantante).
								"</span></td></tr></table></td></tr>";
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Accionado:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($demandados).
								"</span></td></tr></table></td></tr>";
					$leyenda.="<tr height='19'><td colspan='2'><table><tr><td width='140'><span class='TSJDF_Etiqueta'>Despacho:</span></td><td width='430'><span class='TSJDF_Control'> ".cv($despacho).
								"</span></td>".$lblDespachoSegundaInstancia."</tr></table></td></tr>";
					
					$leyenda.="</table>";
				
				}
				
				
				
				
				
				
				$obj='{"despacho":"'.cv($despacho).'","especialidad":"'.cv($especialidad).'","claseProceso":"'.cv($claseProceso).
						'","subClaseProceso":"'.cv($subClaseProceso).'","tema":"'.cv($tema).'","subTemaProceso":"'.cv($subTemaProceso).'","tipoProceso":"'.
					cv($tipoProceso).'","tituloProceso":"'.cv(isset($fRegistroBase["tituloProceso"])?$fRegistroBase["tituloProceso"]:"").'","demandantes":"'.cv($demantante).
					'","demandado":"'.cv($demandados).'","departamento":"'.cv($departamento).'","municipio":"'.cv($municipio).
					'","leyenda":"'.$leyenda.'"}';
			
				echo "1|".$obj;
			}
		}
		
			
	}
	
	function obtenerPlantillaNotificacion()
	{
		global $con;
		$tN=$_POST["tN"];
		$cA=$_POST["cA"];
	
	
	
		$consulta="SELECT plantillaMensajeEnvio FROM _666_tablaDinamica WHERE id__666_tablaDinamica=".$tN;
		$fTipoNotificacion=$con->obtenerPrimeraFilaAsoc($consulta);
	
		$consulta="SELECT * FROM 2011_mensajesEnvio WHERE idMensajeEnvio=".$fTipoNotificacion["plantillaMensajeEnvio"];
		$fMensajeEnvio=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$consulta="SELECT cuerpoMensaje FROM 2013_cuerposMensajes WHERE idMensaje=".$fTipoNotificacion["plantillaMensajeEnvio"];
		$cuerpoMensaje=$con->obtenerValor($consulta);
		
		$consulta="SELECT o.unidad,c.idActividad,c.tipoProceso FROM 7006_carpetasAdministrativas c,817_organigrama o WHERE c.carpetaAdministrativa='".$cA.
				"' AND o.codigoUnidad=c.unidadGestion ";
		$fDatosCarpetas=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$arrParametros["nombreDespacho"]=$fDatosCarpetas["unidad"];
		$arrParametros["codigoUnicoProceso"]=$cA;
		$idActividad=$fDatosCarpetas["idActividad"];
		
		
		
		$demantante="";
		$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
					FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
					AND r.idActividad=".$idActividad." AND r.idFiguraJuridica in(2,7) ORDER BY nombre,nombre,apellidoMaterno";
		
		$res=$con->obtenerFilas($consulta);
		while($filaImputado=mysql_fetch_row($res))
		{
			$nombre=trim($filaImputado[0]);
			if($demantante=="")
				$demantante=$nombre;
			else
				$demantante.=", ".$nombre;
		}
		
		$demandados="";
		$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
					FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
					AND r.idActividad=".$idActividad." AND r.idFiguraJuridica in(4,8) ORDER BY nombre,nombre,apellidoMaterno";
		
		$res=$con->obtenerFilas($consulta);
		while($filaImputado=mysql_fetch_row($res))
		{
			$nombre=trim($filaImputado[0]);
			if($demandados=="")
				$demandados=$nombre;
			else
				$demandados.=", ".$nombre;
		}
		$arrParametros["etDemandante"]="ACTOR";
		$arrParametros["etDemandado"]="DEMANDADO";
		$arrParametros["demandante"]=$demantante;
		$arrParametros["demandado"]=$demandados;
		if($fDatosCarpetas["tipoProceso"]==6)
		{
			$arrParametros["etDemandante"]="ACCIONANTE";
			$arrParametros["etDemandado"]="ACCIONADO";
		}
		
		
		foreach($arrParametros as $campo=>$valor)
		{
			$cuerpoMensaje=str_replace("[".$campo."]",$valor,$cuerpoMensaje);
		}
		
		echo "1|".bE($cuerpoMensaje);
		
	}
	
	function obtenerDocumentosGeneradosAudiencia()
	{
		global $con;
		$idEvento=$_POST["idEvento"];
		
		$consulta=" SELECT id__696_tablaDinamica AS idRegistro,'696' AS idFormulario,fechaCreacion AS fechaRegistro,tipoDocumento AS tipoDocumento,idEstado as situacionActual
				 FROM _696_tablaDinamica WHERE idProcesoPadre=0 AND idReferencia=".$idEvento." and idEstado<>15 ORDER BY fechaCreacion";
		
		
		$arrRegistros=$con->obtenerFilasJSON($consulta);
		echo '{"numReg":"'.$con->filasAfectadas.'","registros":'.$arrRegistros.'}';
		
	}
	
	function registrarTipoDocumentoGeneradosAudiencia()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		$rol="175_0";
		$numEtapa=1;
		$idRegistro=-1;
		$consulta=" SELECT id__696_tablaDinamica AS idRegistro,'696' AS idFormulario,fechaCreacion AS fechaRegistro,tipoDocumento AS tipoDocumento,idEstado as situacionActual
				 FROM _696_tablaDinamica WHERE idProcesoPadre=0 AND idReferencia=".$obj->idEvento." and tipoDocumento=".$obj->tipoDocumento." and idEstado =1 ORDER BY fechaCreacion";	
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		if($fRegistro)
		{
			$idRegistro=$fRegistro["idRegistro"];
			$numEtapa=$fRegistro["situacionActual"];
		}
		else
		{
			
			$consulta="SELECT nombreFormato,seNotificaDocumento FROM _10_tablaDinamica WHERE id__10_tablaDinamica=".$obj->tipoDocumento;
			$fRegistroPlantilla=$con->obtenerPrimeraFilaAsoc($consulta);
			$nombreFormato=$fRegistroPlantilla["nombreFormato"];
			
			$consulta="SELECT carpetaAdministrativa FROM 7007_contenidosCarpetaAdministrativa WHERE tipoContenido=3 AND idRegistroContenidoReferencia=".$obj->idEvento;
			$carpetaAdministrativa=$con->obtenerValor($consulta);
			$arrValores=array();
			$arrValores["carpetaAdministrativa"]=$carpetaAdministrativa;
			$arrValores["tipoDocumento"]=$obj->tipoDocumento;
			$arrValores["seNotificaAuto"]=$fRegistroPlantilla["seNotificaDocumento"];
			$arrValores["tituloAuto"]=$fRegistroPlantilla["nombreFormato"];
			$arrValores["idProcesoPadre"]=0;
			$arrDocumentos=array();
			
			



			
			$idRegistro=crearInstanciaRegistroFormulario(696,$obj->idEvento,1,$arrValores,$arrDocumentos,-1,0);
		}
		
		

		$a=bE("auto");
		$idProceso=283;
		$actor=obtenerActorProcesoIdRol(283,$rol,$numEtapa);
		$act=bE($actor);
		
		


		echo "1|".$idRegistro."|".$a."|".$act;
	}
	
	
	function obtenerActorTipoDocumentoGeneradosAudiencia()
	{
		global $con;
		$idFormulario=bD($_POST["iF"]);
		$idRegistro=bD($_POST["iR"]);
		$rol="175_0";
		$numEtapa=1;
		
		$consulta=" SELECT idEstado 
				 FROM _".$idFormulario."_tablaDinamica WHERE id__".$idFormulario."_tablaDinamica=".$idRegistro." and idEstado<>15";	
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		
		
		$numEtapa=$fRegistro["idEstado"];
		
		

		$a=bE("auto");
		$idProceso=283;
		$actor=obtenerActorProcesoIdRol(283,$rol,$numEtapa);
		$act=bE($actor);
		
		


		echo "1|".$idRegistro."|".$a."|".$act;
	}
	
	function cancelarDocumentoAudiencia()
	{
		global $con;
		$idFormulario=bD($_POST["iF"]);
		$idRegistro=bD($_POST["iR"]);
		
		
		if(cambiarEtapaFormulario($idFormulario,$idRegistro,15,"",-1,"NULL","NULL",0))
			echo "1|";
	}
	
	function obtenerResultadosBusquedaProcesoGeneral()
	{
		global $con;
		
		$criterioBusqueda=$_POST["criterioBusqueda"];
		$obj=json_decode($criterioBusqueda);
		
		
		
		$condWhere=" 1=1 ";
		
		if($obj->depacho!='')
		{
			$condWhere.=" and unidadGestion='".$obj->depacho."'";
		}
		
		if($obj->especialidad!='')
		{
			$condWhere.=" and especialidad=".$obj->especialidad;
		}
		
		if($obj->tipoProceso!='')
		{
			$condWhere.=" and tipoProceso=".$obj->tipoProceso;
		}
		
		if($obj->fechaInicioRegistro!='')
		{
			
			if($obj->condFInicioFiltro=="=")
				$condWhere.=" and fechaCreacion>='".$obj->fechaInicioRegistro."' and fechaCreacion<='".$obj->fechaInicioRegistro." 23:59:59'";
			else
				$condWhere.=" and fechaCreacion".$obj->condFInicioFiltro."'".$obj->fechaInicioRegistro."'";
		}
		
		if($obj->fechaFinRegistro!='')
		{
			if($obj->condFFinFiltro=="=")
				$condWhere.=" and fechaCreacion>='".$obj->fechaFinRegistro."' and fechaCreacion<='".$obj->fechaFinRegistro." 23:59:59'";
			else
				$condWhere.=" and fechaCreacion".$obj->condFFinFiltro."'".$obj->fechaFinRegistro."'";
		}
		
		if($obj->estadoProceso!="")
		{
			$condWhere.=" and situacion=".$obj->estadoProceso;
		}
		
		if($obj->procesoJudicial!="")
		{
			$condWhere.=" and carpetaAdministrativa like '".$obj->procesoJudicial."%'";
		}
		
		if($obj->nombreParticipante!="")
		{
			
			$tipoCriterio=1;
			$arrValoresBusqueda=explode(" ",trim($obj->nombreParticipante));
			for($x=0;$x<sizeof($arrValoresBusqueda);$x++)
			{
				$arrValoresBusqueda[$x]=normalizaToken($arrValoresBusqueda[$x]);
			}
			
			$listaActividades="-1";
			$resultado=buscarCoincidenciasCriterio($tipoCriterio,urldecode($obj->nombreParticipante),60,$obj->tipoFigura);
			$arrResultados=$resultado[2];
			foreach($arrResultados as $idActividad=>$resto)
			{
				if($listaActividades=="-1")
					$listaActividades=$idActividad;
				else
					$listaActividades.=",".$idActividad;
			}
			
			$condWhere.=" and idActividad in (".$listaActividades.")";
			
		}
		
		if($obj->noDocumento!="")
		{
			$listaActividades="-1";
			$consulta="SELECT i.idActividad FROM _47_tablaDinamica i,7005_relacionFigurasJuridicasSolicitud r WHERE i.tipoIdentificacion=".$obj->tipoDocumento." AND i.folioIdentificacion='".cv($obj->noDocumento)."'
					AND  i.id__47_tablaDinamica=r.idParticipante";
			$resParticipantes=$con->obtenerFilas($consulta);
			while($fParticipante=mysql_fetch_assoc($resParticipantes))
			{
				if($listaActividades=="-1")
					$listaActividades=$fParticipante["idActividad"];
				else
					$listaActividades.=",".$fParticipante["idActividad"];
			}
			
			$condWhere.=" and idActividad in (".$listaActividades.")";
		}
		
		
		if($obj->folioRadiacion!="")
		{
			$obj->folioRadiacion=urldecode($obj->folioRadiacion);
			$condWhereAux="";
			$consulta="SELECT DISTINCT idFormulario FROM 7006_carpetasAdministrativas";
			$rFormulario=$con->obtenerFilas($consulta);
			while($fFormulario=mysql_fetch_assoc($rFormulario))
			{
				$nombreTabla=obtenerNombreTabla($fFormulario["idFormulario"]);
				
				$consulta="SELECT id_".$nombreTabla." FROM ".$nombreTabla." WHERE codigo='".$obj->folioRadiacion."'";
				$idRegistroFolio=$con->obtenerValor($consulta);
				
				if($idRegistroFolio!="")
				{
					if($condWhereAux=="")
						$condWhereAux="(idFormulario=".$fFormulario["idFormulario"]." and idRegistro=".$idRegistroFolio.")";
					else
						$condWhereAux.=" or (idFormulario=".$fFormulario["idFormulario"]." and idRegistro=".$idRegistroFolio.")";
									
				}
			}
			
			if($condWhereAux!="")
			{
				$condWhere.=" and (".$condWhereAux.")";
			}
			else
			{
				$condWhere.=" and 1=2";
			}
			
		}
		
		
		$arrRegistros="";
		$numReg=0;
		$consulta="SELECT * FROM 7006_carpetasAdministrativas where ".$condWhere;
		$consulta.=" ORDER BY fechaCreacion";
		

		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$idActividad=$fila["idActividad"];
			$cBase=obtenerCarpetaBaseOriginal($fila["carpetaAdministrativa"]);
			
			$consulta="SELECT * FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cBase."'";
			$fBase=$con->obtenerPrimeraFilaAsoc($consulta);
			
			
			$consulta="SELECT * FROM _632_tablaDinamica WHERE id__632_tablaDinamica=".$fBase["idRegistro"];
			$fRadicacionBase=$con->obtenerPrimeraFilaAsoc($consulta);
			
			
			$consulta="SELECT codigo FROM _".$fila["idFormulario"]."_tablaDinamica WHERE id__".$fila["idFormulario"]."_tablaDinamica=".$fila["idRegistro"];
			$folioRegistro=$con->obtenerValor($consulta);
			

			$consulta="SELECT group_concat(upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno)))) 
					FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
					AND r.idActividad=".$idActividad." AND r.idFiguraJuridica in(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='A') ORDER BY nombre,nombre,apellidoMaterno";
			$actor=$con->obtenerValor($consulta);
			
			$consulta="SELECT group_concat(upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno)))) 
					FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
					AND r.idActividad=".$idActividad." AND r.idFiguraJuridica in(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='D') ORDER BY nombre,nombre,apellidoMaterno";
			$demandado=$con->obtenerValor($consulta);
			
			$o='{"idFormulario":"'.$fila["idFormulario"].'","idRegistro":"'.$fila["idRegistro"].
				'","folioRegistro":"'.$folioRegistro.'","fechaRegistro":"'.$fila["fechaCreacion"].'","codigoUnicoProceso":"'.$fila["carpetaAdministrativa"].
				'","tituloProceso":"'.cv($fRadicacionBase["tituloProceso"]).'","especialidad":"'.$fila["especialidad"].'","departamento":"'.$fRadicacionBase["departamento"].
				'","despacho":"'.$fila["unidadGestion"].'","estadoProceso":"'.$fila["situacion"].
				'","tipoProceso":"'.$fila["tipoProceso"].'","idCarpeta":"'.$fila["idCarpeta"].'","tipoCarpeta":"'.$fila["tipoCarpetaAdministrativa"].
				'","actor":"'.cv($actor).'","demandado":"'.cv($demandado).'"}';
			if($arrRegistros=="")
				$arrRegistros=$o;
			else
				$arrRegistros.=",".$o;
			$numReg++;
		}
		
		echo '{"numReg":"'.$numReg.'","registros":['.$arrRegistros.']}';
	}
	
	
	function registrarNotificacionMensajeSedeJudicial()
	{
		global $con;
		$idNotificacion=$_POST["iR"];
		$consulta="SELECT COUNT(*) FROM _722_tablaDinamica WHERE idReferencia=".$idNotificacion." and medioNotificacionResultadoNotificacion=3";
		 $numReg=$con->obtenerValor($consulta);
		 if($numReg==0)
		 {
			 $consulta="INSERT INTO _722_tablaDinamica(idReferencia,fechaCreacion,responsable,fechaRealizacionDiligenciaResultadoNotificacion,
						horaRealizacionDiligenciaResultadoNotificacion,
						realizarNotificacionResultadoNotificacion,medioNotificacionResultadoNotificacion,confirmadoMedianteCorreo)
						VALUES(".$idNotificacion.",'".date("Y-m-d H:i:s")."',".$_SESSION["idUsr"].",'".date("Y-m-d")."','".date("H:i")."',1,3,0)";
			 $con->ejecutarConsulta($consulta);
			 
			 $idRegistro=$con->obtenerUltimoID();
			 asignarFolioRegistro(722,$idRegistro);
			 
		 }
		 if(cambiarEtapaFormulario(665,$idNotificacion,5.6,"",-1,"NULL","NULL",0))
		 	echo "1|";
	}
	
	
	function obtenerReporteEstado()
	{
		global $con;
		$fechaInicio=$_POST["fechaInicio"];
		$fechaFin=$_POST["fechaFin"];
		
		
		$consulta="SELECT a.idArchivo AS idDocumento,a.nomArchivoOriginal AS nombreAuto,a.categoriaDocumentos AS tipoDocumento,f.fechaBloqueo AS  fechaCreacion,
					0 AS enviadoEstado,'' AS fechaEnvioEstado,i.carpetaAdministrativa,i.idFormulario AS iFormulario,i.idReferencia AS iFormulario
					FROM 3000_formatosRegistrados f,7035_informacionDocumentos i,7006_carpetasAdministrativas c,908_archivos a,908_categoriasDocumentos cD,908_tipoDocumentos tD 
					WHERE f.fechaBloqueo>='".$fechaInicio."' 
					AND f.fechaBloqueo <='".$fechaFin." 23:59:59'
					AND f.idFormulario=-2 AND f.idRegistro=i.idRegistro AND i.carpetaAdministrativa=c.carpetaAdministrativa AND c.unidadGestion='".$_SESSION["codigoInstitucion"]."'
					AND a.idArchivo=f.idDocumento AND cD.idCategoria=a.categoriaDocumentos AND tD.idRegistro=cD.idCategoriaDocumento AND tD.seNotificaEstado=1 ORDER BY f.fechaBloqueo";
		

		$arrRegistros=utf8_encode($con->obtenerFilasJSON($consulta));
		
		echo '{"numReg":"'.$con->filasAfectadas.'","registros":'.$arrRegistros.'}';
	}
	
	function obtenerTutelasRecibidas()
	{
		global $con;
		$idReferencia="-1";
		$fechaInicio=NULL;
		$fechaFin=NULL;
		
		$ss=0;
		if(isset($_POST["ss"]))
			$ss=$_POST["ss"];
		
		
		if(isset($_POST["iRef"]))
			$idReferencia=$_POST["iRef"];
			
		if(isset($_POST["fechaInicio"]))	
			$fechaInicio=$_POST["fechaInicio"];
		if(isset($_POST["fechaFin"]))	
			$fechaFin=$_POST["fechaFin"];	
			
		$consulta="SELECT id__917_tablaDinamica as idRegistro,'917' AS idFormulario,fechaCreacion,codigo AS folioRegistro,folioCorteConstitucional,carpetaAdministrativa,
					(SELECT unidad FROM 817_organigrama WHERE codigoUnidad=d.codigoInstitucion ) AS despachoEnvio,
					(SELECT COUNT(*) FROM _989_tablaDinamica WHERE idReferencia=d.id__917_tablaDinamica) as cuentaFicha ,if(idEstado>4,5,idEstado) as idEstado,
					(SELECT candidatoSeleccion FROM _995_tablaDinamica WHERE idReferencia=d.id__917_tablaDinamica) as candidato,
					(SELECT COUNT(*) FROM _997_tablaDinamica WHERE idReferencia=d.id__917_tablaDinamica) as existeInsistencia,
					(SELECT tutelaSeleccionable FROM _996_tablaDinamica WHERE idReferencia=d.id__917_tablaDinamica) as seleccionada,
					if(idEstado>5,6,idEstado) as estado6,
					(SELECT nombreSala FROM _992_tablaDinamica WHERE id__992_tablaDinamica=if(d.salaRevision='N/E',-1,d.salaRevision) ) AS despachoSeleccion
					
					
					FROM _917_tablaDinamica d WHERE 1=1 and idReferencia=".$idReferencia;
		if($fechaInicio)
			$consulta.=" and fechaCreacion>='".$fechaInicio."' and fechaCreacion<='".$fechaFin." 23:59:59'";

		if($idReferencia==-1)
			$consulta.=" and idEstado=2";
		
		if($ss==1)
		{
			$consulta="select * from (".$consulta.") as tmp where seleccionada=1";
		}

		$arrRegistros=utf8_encode($con->obtenerFilasJSON($consulta));

		
		echo '{"numReg":"'.$con->filasAfectadas.'","registros":'.$arrRegistros.'}';
		
	}
	
	function crearPaqueteTutelas()
	{
		global $con;
		global $servidorPruebas;
		global $tipoMateria;
		$iR=$_POST["iR"];
		$a="";
		$act="";

		if($iR==-1)
		{
			$consulta="INSERT INTO _990_tablaDinamica(fechaCreacion,responsable,idEstado,codigoInstitucion,despachoAsignado) 
						VALUES('".date("Y-m-d H:i:s")."',".$_SESSION["idUsr"].",1,'".$_SESSION["idUsr"]."','N/E')";
			
			if($con->ejecutarConsulta($consulta))
			{
				$iR=$con->obtenerUltimoID();
				asignarFolioRegistro(990,$iR);
			}
			
		}
	
		
		$a=bE("auto");
		$idProceso=obtenerIdProcesoFormulario(990);
		
		$rol='234_0'; 
			
		
		
		$actor=obtenerActorProcesoIdRol($idProceso,$rol,1);
		$act=bE($actor);
		
		echo "1|".$iR."|".$a."|".$act;
		
	}
	
	function obtenerPaqueteTutelasRecibidas()
	{
		global $con;
		
		$consulta="SELECT id__990_tablaDinamica as idRegistro,'990' AS idFormulario,fechaCreacion,codigo AS folioRegistro,
					(SELECT COUNT(*) FROM _917_tablaDinamica WHERE idReferencia=d.id__990_tablaDinamica) as totalTutelas,
					(SELECT unidad FROM 817_organigrama WHERE codigoUnidad=d.despachoAsignado ) AS despachoAsignado,idEstado as situacionActual 
					FROM _990_tablaDinamica d order by fechaCreacion desc";
			
		$arrRegistros=utf8_encode($con->obtenerFilasJSON($consulta));

		
		echo '{"numReg":"'.$con->filasAfectadas.'","registros":'.$arrRegistros.'}';
		
	}
	
	
	function registrarTutelasPaquete()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		$x=0;
		$consulta[$x]="begin";
		$x++;
		
		$arrRegistros=explode(",",$obj->arrRegistros);
		foreach($arrRegistros as $r)
		{
			
			$consulta[$x]="UPDATE _917_tablaDinamica SET idReferencia=".$obj->idRegistro.",idProcesoPadre=339 WHERE id__917_tablaDinamica=".$r;
			$x++;
		}
		
		
		
		$consulta[$x]="commit";
		$x++;
		if($con->ejecutarBloque($consulta))
		{
			foreach($arrRegistros as $r)
			{
				
				cambiarEtapaFormulario(917,$r,3,"",-1,"NULL","NULL",0);
			}
			echo "1|";
		}
		
	}
	
	function removerTutelasPaquete()
	{
		global $con;
		$arrRegistros=$_POST["arrRegistros"];
		
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="UPDATE _917_tablaDinamica SET idReferencia=-1,idEstado=2,idProcesoPadre=NULL WHERE id__917_tablaDinamica IN(".$arrRegistros.")";
		$x++;
		$consulta[$x]="commit";
		$x++;

		if($con->ejecutarBloque($consulta))
		{
			
			echo "1|";
		}
		
	}
	
	function obtenerInformacionTutela()
	{
		global $con;
		global $servidorPruebas;
		global $tipoMateria;
		$iR=$_POST["iR"];
		$a="";
		$act="";
		
		$consulta="SELECT idEstado FROM _917_tablaDinamica WHERE id__917_tablaDinamica=".$iR;
		$etapa=$con->obtenerValor($consulta);
		
		$a=bE("auto");
		$idProceso=obtenerIdProcesoFormulario(917);
		
		$rol='235_0'; 
		if(isset($_POST["rolIngreso"]))	
			$rol=$_POST["rolIngreso"];
		
		$actor=obtenerActorProcesoIdRol($idProceso,$rol,$etapa);
		$act=bE($actor);
		
		echo "1|".$iR."|".$a."|".$act;
		
	}
	
	function obtenerVotacionesMagistradosRevisionTutela()
	{
		global $con;
		
		$idEstado=0;
		$numReg=0;
		$arrRegistros="";
		$iA=$_POST["iA"];
		$consulta="SELECT * FROM 7000_participantesEventoAudiencia WHERE idRegistroEvento=-".$iA." ORDER BY idRegistro";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$consulta="SELECT dictamenFinal,comentariosAdicionales FROM _1027_tablaDinamica b,_1028_tablaDinamica d
						WHERE d.idReferencia=b.id__1027_tablaDinamica AND b.idEvento=".$iA." AND b.idMagistrado=".$fila["nombrePersona"];
			$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$idEstado=$fRegistro["dictamenFinal"];
			if($idEstado=="")
			{
				$consulta="SELECT * FROM _1027_tablaDinamica WHERE idEvento=".$iA." AND idMagistrado=".$fila["nombrePersona"];
				$fRegistroPersona=$con->obtenerPrimeraFilaAsoc($consulta);
				if(!$fRegistroPersona)
					$idEstado=5;
				else
					$idEstado=0;
			}
			$o='{"idMagistrado":"'.$fila["nombrePersona"].'","nombreMagistrado":"'.cv(obtenerNombreUsuario($fila["nombrePersona"]).($arrRegistros==""?" (Ponente)":"")).
				'","votacion":"'.$idEstado.'","comentariosAdicionales":"'.cv($fRegistro["comentariosAdicionales"]).'"}';
			if($arrRegistros=="")
				$arrRegistros=$o;
			else
				$arrRegistros.=",".$o;

			$numReg++;
		}
		
		$consulta="SELECT dictamenVotacion FROM 7000_eventosAudiencia WHERE idRegistroEvento=".$iA;
		$dictamenVotacion=$con->obtenerValor($consulta);
		echo '{"numReg":"'.$numReg.'","registros":['.$arrRegistros.'],"situacionVotacion":"'.$idEstado.'","votacionAbierta":"'.($dictamenVotacion==0?1:0).'"}';
	}
	
	
	function aperturarVotacionMagistrado()
	{
		global $con;
		$idEvento=$_POST["idEvento"];
		
		$consulta="SELECT carpetaAdministrativa FROM 7007_contenidosCarpetaAdministrativa WHERE tipoContenido=3 AND idRegistroContenidoReferencia=".$idEvento;
		$carpetaAdministrativa=$con->obtenerValor($consulta);
		
		$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativa."' AND tipoCarpetaAdministrativa=30";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$arrValores=array();
			$arrDocumentos=array();
			
			$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='96_0' AND ad.Institucion in(".$fila["unidadGestion"].")";
			$idMagistrado=$con->obtenerValor($consulta);
			
			$arrValores["codigoInstitucion"]=$fila["unidadGestion"];
			$arrValores["idMagistrado"]=$idMagistrado;
			$arrValores["carpetaAdministrativa"]=$carpetaAdministrativa;
			$arrValores["responsable"]=$idMagistrado;
			$arrValores["idEvento"]=$idEvento;
			$idRegistroInstancia=crearInstanciaRegistroFormulario(1027,$idEvento,1,$arrValores,$arrDocumentos,-1,0);
			
			
		}
		
		echo "1|";
		
	}
	
	function registrarResultadoVotacion()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		
		$consulta="UPDATE 7000_eventosAudiencia SET dictamenVotacion=".$obj->dictamenFinal.",comentariosAdicionales='".cv(isset($obj->comentariosFinales)?$obj->comentariosFinales:"")."' WHERE idRegistroEvento=".$obj->idEvento;

		eC($consulta);
	}
?>