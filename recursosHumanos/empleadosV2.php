<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$tituloModulo="Empleado";
	
?>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<link rel="stylesheet" href="../Scripts/ux/grid/RowEditor.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/ux/grid/RowEditor.js"></script>


<link rel="stylesheet" href="../Scripts/jQueryUI/temas/smoothness/jquery-ui.css" />
<script src="../Scripts/jQuery/jquery-1.9.1.js"></script>
<script src="../Scripts/jQueryUI/jquery-ui.js"></script>



<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>


<link rel="stylesheet" href="../Scripts/taskBar/taskbar.css" />
<script type="text/javascript" src="../recursosHumanos/Scripts/empleadosV2.js.php"></script>
<style type="text/css">
<!--
pre{
	width:600px;
	border:1px dashed #CCCCCC;
	overflow:auto;
}
-->
</style>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
							$idEmpleado=-1;
							
							
							$arrTipoTel[0]="Fijo";
							$arrTipoTel[1]="Celular";
							if(isset($objParametros->idEmpleado))
								$idEmpleado=$objParametros->idEmpleado;
								
							$esEmpresaUsuario=0;
							$cDirObl="";
							$vDirObl="";
							
							echo formatearTituloPagina($tituloModulo,true,$idEmpleado);
										
							$consulta="SELECT * FROM 693_empleadosNominaV2 WHERE idEmpleado=".$idEmpleado;

							$fRegistro=$con->obtenerPrimeraFila($consulta);
							
							$consulta="SELECT mail FROM 694_emailContactoEmpleadoV2 WHERE idEmpleado=".$idEmpleado;
							$arrMail=$con->obtenerFilasArreglo($consulta);
							$consulta="SELECT lada,telefono,extension,tipo FROM 695_telefonoContactoEmpleadoV2 WHERE idEmpleado=".$idEmpleado;
							$arrContactos=$con->obtenerFilasArreglo($consulta);
							
							$consulta="SELECT tipoPercepcion,clave,descripcion,importeGravado,importeExento FROM 696_percepcionesEmpleadoV2 WHERE idEmpleado=".$idEmpleado;
							$arrPercepcionesEmp=$con->obtenerFilasArreglo($consulta);
							
							$consulta="SELECT tipoDeduccion,clave,descripcion,importeGravado,importeExento FROM 697_deduccionesEmpleadoV2 WHERE idEmpleado=".$idEmpleado;
							$arrDeduccionesEmp=$con->obtenerFilasArreglo($consulta);

							$consulta="SELECT idConcepto,valor FROM 714_empleadosConceptoBase WHERE idEmpleado=".$idEmpleado;
							$arrConceptosBase=$con->obtenerFilasArreglo($consulta);
							
							
							$tipoCalculoNomina="2"; //1 Captura manual; 2 Modulo de nomina
							if(existeRol("'58_0'"))
								$tipoCalculoNomina="1";
							
						?>
                        	
                        
                        	
                            <table width="700">
                            	<tr>
                                	<td align="left">
                                    	
                                    	<form method="post" action="../paginasFunciones/guardarDatos.php" id="frmEnvio">
                                        
                                    	<fieldset class="frameHijoV3"><legend>Datos generales</legend>
                                    	<table>
                                        <tr>
                                        	<td align="left">
                                        		
                                                <table>
                                                	<tr>
                                                    	<td><span class="letraAzulSimple">No. de empleado</span> <span style="color:#F00"></span></td>
                                                        <td><input type="text" size="15" name="_numEmpleadoint" id="_numEmpleadoint" value="<?php echo $fRegistro[1]?>" val="" campo="Curp" onkeypress="return soloNumero(event,false,false,this)"/></td>
                                                    </tr>
                                                    <tr height="21" style="display:<?php echo $ocultarClasificacion?>">
                                                        <td width="150" ><span class="letraAzulSimple">RFC:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450">
                                                        	<input type="text" size="10" maxlength="10" name="_rfc1vch" id="_rfc1vch" value="<?php echo $fRegistro[2]?>" val="obl" campo="RFC" /> -
                                                            <input type="text" size="10" maxlength="10" name="_rfc2vch" id="_rfc2vch" value="<?php echo $fRegistro[3]?>" val="obl" campo="RFC" /> - 
                                                            <input type="text" size="10" maxlength="10" name="_rfc3vch" id="_rfc3vch" value="<?php echo $fRegistro[4]?>" val="obl" campo="RFC" />
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fCurp">
                                                        <td ><span class="letraAzulSimple">CURP:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450"><input type="text" size="30" name="_curpvch" id="_curpvch" value="<?php echo $fRegistro[5]?>" val="obl" campo="Curp" /></td>
                                                    </tr>
                                                    <tr height="21" id="fPaterno">
                                                        <td ><span class="letraAzulSimple">Apellido Paterno:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450"><input type="text" size="30" name="_apPaternovch" id="_apPaternovch" value="<?php echo $fRegistro[6]?>" val="obl" campo="Apellido Paterno" /></td>
                                                    </tr>
                                                    <tr height="21" id="fMaterno">
                                                        <td ><span class="letraAzulSimple">Apellido Materno:</span> <span style="color:#F00"></span></td>
                                                        <td width="450"><input type="text" size="30" name="_apMaternovch" id="_apMaternovch" value="<?php echo $fRegistro[7]?>" val="" campo="Apellido Materno" /></td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td ><span class="letraAzulSimple" id="lblRazonSocial">Nombre:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450"><input type="text" size="70" name="_nombrevch" id="_nombrevch" value="<?php echo $fRegistro[8]?>" val="obl" campo="Nombre del empleado" /></td>
                                                    </tr>
                                                    
                                                    
                                                    
                                                    <tr height="21">
                                                        <td  valign="top"><span class="letraAzulSimple">Situaci&oacute;n:</span></td>
                                                        <td >
                                                        	<select id="_situacionint" name="_situacionint">
                                                        <?php
                                                        	$consulta="SELECT claveElemento,nombreElemento FROM 1018_catalogoVarios WHERE tipoElemento=1";
															echo $con->generarOpcionesSelect($consulta,$fRegistro[9]);
														?>
                                                        	</select>
                                                        </td>
                                                    </tr>
                                                    <tr height="21">
                                                    </tr>
                                                    
                                                </table>
                                                

                                                
                                                <input type="hidden" name="tabla" value="693_empleadosNominaV2" />
                                                <input type="hidden" name="id" id="idEmpleado" value="<?php echo $idEmpleado?>" />
                                                <input type="hidden" name="campoId" value="idEmpleado" />
                                                
                                                <?php
													$pagRedireccion="../recursosHumanos/tblEmpleados.php";
													$valorPost=$nConfRegresar;
													
													
												?>
                                                
                                                <input type="hidden" name="valorPost" value='<?php echo $valorPost?>' />
                                                <input type="hidden" name="paramPost" value='[{"nombreP":"configuracion","valorP":"<?php echo $valorPost?>"}]' />
                                                <input type="hidden" name="pagRedireccion" value="<?php echo $pagRedireccion?>"/>	
                                                
                                            </td>
                                       	</tr>
                                        </table>
                                        </fieldset><br /><br />
                                        <div id="tabs">
                                        <ul>
                                            <li><a href="#tabs-1">Dirección</a></li>
                                            <li><a href="#tabs-2">Datos de Contactos</a></li>
                                            <li><a href="#tabs-3">Datos de contratación</a></li>
                                       <?php
										   	if($tipoCalculoNomina==1)
									   		{
										?>
                                            <li><a href="#tabs-4">Conceptos de Nómina</a></li>
                                        <?php
											}
											else
											{
										?>
                                            <li><a href="#tabs-5">Conceptos Base de Nómina</a></li>
                                        <?php
											}
										?>
                                        </ul>
                                        <div id="tabs-1" style="background-color:rgb(240, 240, 240);">
                                        
                                    	<table width=780>
                                        <tr>
                                        	<td align="left">
                                        		
                                                <table>
                                                	
                                                    	
                                                        <tr height="21">
                                                            <td  width="120"><span class="letraAzulSimple">Estado:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <select  id="_estadovch" name="_estadovch" onchange="estadoSel(this)" val="<?php echo $vDirObl?>" campo="Estado" >
                                                                    <option value="-1">Seleccione</option>
                                                                <?php
                                                                    $consulta="sELECT cveEstado,estado FROM 820_estados ORDER BY estado";
                                                                    $con->generarOpcionesSelect($consulta,$fRegistro[10]);
                                                                ?>	
                                                                    </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Municipio:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <select  id="_municipiovch" name="_municipiovch" val="<?php echo $vDirObl?>" campo="Municipio">
                                                                    <option value="-1">Seleccione</option>
                                                                <?php
                                                                    if($fRegistro[10]!="")
                                                                    {
                                                                        $consulta="SELECT cveMunicipio,municipio FROM 821_municipios WHERE cveEstado='".$fRegistro[10]."' ORDER BY municipio";
                                                                        $con->generarOpcionesSelect($consulta,$fRegistro[11]);
                                                                    }
                                                                ?>	
                                                                    </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Localidad:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_localidadvch" id="_localidadvch" value="<?php echo $fRegistro[12]?>" size="40" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td ><span class="letraAzulSimple">Calle:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <input type="text" name="_callevch" id="_callevch" value="<?php echo $fRegistro[13]?>"  size="40" val="<?php echo $vDirObl?>" campo="Calle"/>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">No. Exterior.:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_noExteriorvch" id="_noExteriorvch" value="<?php echo $fRegistro[14]?>" size="30" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">No. Interior.:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_noInteriorvch" id="_noInteriorvch" value="<?php echo $fRegistro[15]?>" size="30" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Colonia:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_coloniavch" id="_coloniavch" value="<?php echo $fRegistro[16]?>" size="60" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">C.P.:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <input type="text" name="_cpint" id="_cpint" value="<?php echo $fRegistro[17]?>" size="10" onkeypress="return soloNumero(event,false,false,this)"  val="<?php echo $vDirObl?>" campo="C.P."/>
                                                            </td>
                                                        </tr>
                                               	</table> 
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        
                                        <br /><br /> <br /><br /> <br /><br /> <br /><br /> <br /><br /> <br /><br /><br /><br /> <br /><br /> <br /><br /><br /><br /> <br /><br />
                                        </div>
                                        <div id="tabs-2" style="background-color:rgb(240, 240, 240);">
                                        
                                    	<table width="790">
                                        <tr>
                                        	<td align="left">
                                        		<table>
                                                <tr>
                                                	<td width="330">
		                                                <span id="tblMail"></span>
                                                    </td>
                                                    <td width="330">
		                                                <span id="tblTelefono"></span>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        
                                        </div>
                                        
                                        
                                        <div id="tabs-3" style="background-color:rgb(240, 240, 240);">

                                        
                                    	<table width="790">
                                        <tr>
                                        	<td align="left">
												<table>
                                                   
                                                    <tr>
                                                        <td><span class="letraAzulSimple">Empresa:</span> <span style="color:#F00">*</span></td>
                                                        <td colspan="3">
                                                        	 <select id="_idEmpresaint" name="_idEmpresaint" val="obl" campo="Empresa" jQUI="tabs" noTab="2"  onchange="obtenerPuestosDepartamentosEmpresa(this)" />
                                                             <option value="-1">Seleccione</option>
                                                        <?php
                                                        	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT('[',cveEmpresa,'] ',razonSocial,' ',if(apPaterno is null,'',apPaterno),' ',if(apMaterno is null,'',apMaterno)),razonSocial) AS nombreEmpresa  
															FROM 6927_empresas WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1 order by rfc1,rfc2,rfc3";
															echo $con->generarOpcionesSelect($consulta,$fRegistro[20]);
														?>
                                                        	</select>
                                                        </td>
                                                    </tr>
                                                    
                                                    
                                                    
                                                     <tr height="21" id="fCurp">
                                                        
                                                        <td><span class="letraAzulSimple">No. de seguro social:</span> <span style="color:#F00"></span></td>
                                                        <td><input type="text" size="15" name="_noSeguroSocialvch" id="_noSeguroSocialvch" value="<?php echo $fRegistro[19]?>" val="" campo="Curp" onkeypress="return soloNumero(event,false,false,this)"/></td>
                                                        <td><span class="letraAzulSimple">No. de registro patronal:</span> <span style="color:#F00"></span></td>
                                                        <td>
                                                        	<select style="width:200px" id="_noRegistroPatronalvch" name="_noRegistroPatronalvch" val="" campo="Puesto" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Seleccione</option>
                                                            <?php
																if($fRegistro[20]!="")
																{
																	$consulta="SELECT idRegistro,registroPatronal FROM 6927_empresaRegistroPatronal WHERE idEmpresa=".$fRegistro[20]." ORDER BY registroPatronal";
																	$con->generarOpcionesSelect($consulta,$fRegistro[18]);
																}
															?>
                                                            
                                                            
                                                            </select>
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fCurp">
                                                        <td><span class="letraAzulSimple">Puesto:</span> <span style="color:#F00">*</span></td>
                                                        <td>
                                                        	<select style="width:210px" id="_idPuestovch" name="_idPuestovch" val="obl" campo="Puesto" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Seleccione</option>
                                                            <?php
																if($fRegistro[20]!="")
																{
																	$consulta="SELECT idPuesto,CONCAT('[',cvePuesto,'] ',puesto) FROM 692_puestosNominaV2 WHERE idEmpresa=".$fRegistro[20]." ORDER BY puesto";
																	$con->generarOpcionesSelect($consulta,$fRegistro[21]);
																}
															?>
                                                            
                                                            
                                                            </select>
                                                        </td>
                                                    
                                                        <td><span class="letraAzulSimple">Departamento:</span> <span style="color:#F00">*</span></td>
                                                        <td>
                                                        	<select style="width:210px" id="_idDepartamentovch" name="_idDepartamentovch" val="obl" campo="Departamento" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Seleccione</option>
                                                            <?php
																if($fRegistro[20]!="")
																{
																	$consulta="SELECT idDepartamento,CONCAT('[',cveDepartamento,'] ',nombreDepartamento) FROM 691_departamentosNominaV2 WHERE idEmpresa=".$fRegistro[20]." ORDER BY nombreDepartamento";
																	$con->generarOpcionesSelect($consulta,$fRegistro[22]);
																	
																}
															?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fCurp">
                                                        <td><span class="letraAzulSimple">Centro de Costo:</span> <span style="color:#F00"></span></td>
                                                      <td>
                                                        	<select style="width:210px" id="_idCentroCostoint" name="_idCentroCostoint" val="" campo="Centro de Costo" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Ninguno</option>
                                                            <?php
																if($fRegistro[20]!="")
																{
																	$consulta="SELECT idCentroCosto,centroCosto FROM 722_centrosCostos WHERE idEmpresa=".$fRegistro[20]." ORDER BY centroCosto";
																	$con->generarOpcionesSelect($consulta,$fRegistro[36]);
																}
															?>
                                                            
                                                            
                                                            </select>
                                                        </td>
                                                    
                                                        
                                                    </tr>
                                                    <tr height="21" id="fCurp">
                                                        <td><span class="letraAzulSimple">Fecha de inicio de relaci&oacute;n laboral:</span> <span style="color:#F00"></span></td>
                                                        <td>
                                                        	<span id="spFechaInicioRelLab"></span>
                                                            
                                                        <?php
															if($fRegistro[23]!="")
															{
																$fRegistro[23]=date("d/m/Y",strtotime($fRegistro[23]));
															}
														?>    
                                                        <input  type="hidden" size="15" name="_fechaIniRelLabdte" id="_fechaIniRelLabdte" value="<?php echo $fRegistro[23]?>" val=""/>
                                                        </td>
                                                    
                                                        <td><span class="letraAzulSimple">R&eacute;gimen de contrataci&oacute;n:</span> <span style="color:#F00">*</span></td>
                                                        <td>
                                                        	<select style="width:210px" id="_regimenContratacionint" name="_regimenContratacionint" val="obl" campo="R&eacute;gimen de contrataci&oacute;n" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Selecciones</option>
															<?php
																$consulta="SELECT idRegimen,concat('[',clave,'] ',descripcion) FROM 683_regimenContratacionSAT ORDER BY descripcion";
															 	$con->generarOpcionesSelect($consulta,$fRegistro[24]);
															?>
                                                            </select>
                                                        	
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fCurp">
                                                        <td><span class="letraAzulSimple">Tipo de contrato:</span> <span style="color:#F00"></span></td>
                                                        <td>
                                                        	<select style="width:210px" id="_tipoContratoint" name="_tipoContratoint" val="" campo="R&eacute;gimen de contrataci&oacute;n" />
                                                            <option value="-1">No especificado</option>
															<?php
																$consulta="SELECT idTipoContrato,tipoContrato FROM 686_tiposContrato ORDER BY tipoContrato";
															 	$con->generarOpcionesSelect($consulta,$fRegistro[25]);
															?>
                                                            </select>
                                                        </td>
                                                    
                                                        <td><span class="letraAzulSimple">Tipo de jornada laboral:</span> <span style="color:#F00"></span></td>
                                                        <td>
                                                        	<select style="width:210px" id="_tipoJornadaint" name="_tipoJornadaint" val="" campo="R&eacute;gimen de contrataci&oacute;n" />
                                                            <option value="-1">No especificado</option>
															<?php
																$consulta="SELECT idTipoJornada,tipoJornada FROM 689_tipoJornadas ORDER BY tipoJornada";
															 	$con->generarOpcionesSelect($consulta,$fRegistro[26]);
															?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr height="21" >
                                                        <td width="155"><span class="letraAzulSimple">Salario Diario Integrado:</span> <span style="color:#F00"></span></td>
                                                        <td width="250"><input type="text" size="10" name="_sdivch" id="_sdivch" value="<?php echo $fRegistro[32]?>" val="" campo="Curp" onkeypress="return soloNumero(event,true,false,this)" /></td>
                                                   
                                                        
                                                    </tr>
                                                    
                                                     <tr height="21" id="fCurp">
                                                        <td><span class="letraAzulSimple">Periodicidad de pago:</span> <span style="color:#F00">*</span></td>
                                                        <td><input type="text" size="35" name="_periodicidadPagovch" id="_periodicidadPagovch" value="<?php echo $fRegistro[27]?>" val="obl" campo="Periodicidad de pago" jQUI="tabs" noTab="2" /></td>
                                                   
                                                        <td><span class="letraAzulSimple">Forma de pago:</span> <span style="color:#F00">*</span></td>
                                                        <td>
                                                        	 <select style="width:210px" id="_formaPagoint" name="_formaPagoint" val="obl" campo="Forma de pago" onchange="selFormaPago(this)" jQUI="tabs" noTab="2" />
                                                            <option value="-1">Seleccione</option>
                                                            <?php
                                                                $consulta="SELECT idFormaPago,formaPago FROM 710_metodoPagoComprobante";
                                                                $con->generarOpcionesSelect($consulta,$fRegistro[28]);
																if($fRegistro[28]!=4)
																	$displayBanco="none";
                                                            ?>
                                                            </select>
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fBanco" style="display: <?php echo $displayBanco?> ">
                                                        <td><span class="letraAzulSimple">Banco:</span> <span style="color:#F00"></span></td>
                                                        <td>
                                                        <select style="width:210px" id="_idBancovch" name="_idBancovch" val="" campo="R&eacute;gimen de contrataci&oacute;n" />
                                                        <option value="-1">No especificado</option>
                                                        <?php
                                                            $consulta="SELECT idBanco,nombreCorto FROM 6000_bancos WHERE  claveSAT IS NOT NULL ORDER BY nombreCorto ";
                                                            $con->generarOpcionesSelect($consulta,$fRegistro[29]);
                                                        ?>
                                                        </select>
                                                        
                                                        
                                                        </td>
                                                   
                                                        <td><span class="letraAzulSimple">Cuenta:</span> <span style="color:#F00"></span></td>
                                                        <td><input type="text" size="30" name="_noCuentavch" id="_noCuentavch" value="<?php echo $fRegistro[30]?>" val="" campo="Curp" onkeypress="return soloNumero(event,false,false,this)"/></td>
                                                    </tr>
                                                    <tr height="21" id="fBanco2" style="display: <?php echo $displayBanco?> ">
                                                        <td><span class="letraAzulSimple">CLABE:</span> <span style="color:#F00"></span></td>
                                                        <td><input type="text" size="30" name="_clabevch" id="_clabevch" value="<?php echo $fRegistro[31]?>" val="" campo="Curp" onkeypress="return soloNumero(event,false,false,this)"/></td>
                                                    </tr>
                                                    <tr height="21">
                                                    </tr>
                                                    
                                                </table>
                                               
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        
                                        </div>
                                        <?php
										   	if($tipoCalculoNomina==1)
									   		{
										?>
                                        <div id="tabs-4" style="background-color:rgb(240, 240, 240);">
                                        <span class="letraAzulSimple">Especifique las percepciones predefinidas que aplicar&aacute; al empleado:</span> <br /><br />
                                        <span id="tblPercepciones"></span>
                                        <br /><br />
                                         <span class="letraAzulSimple">Especifique las deducciones predefinidas que aplicar&aacute; al empleado:</span> <br /><br />
                                        <span id="tblDeducciones"></span>
                                        
                                        </div>
                                         <?php
											}
											else
											{
										?>
                                        <div id="tabs-5" style="background-color:rgb(240, 240, 240);"><br />
                                        	
			                                        	 <span class="letraAzulSimple">Especifique los valores de los conceptos base a aplicar al empleado:</span> <br /><br />
	        			                                <span id="tblConceptosBase"></span><br /><br />
                                            
                                        </div>
                                         <?php
											}
											  if($idEmpleado=="-1")
											  {
										  ?>
											  <input name="funcPHPEjecutarNuevo" id="funcPHPEjecutarNuevo" type="hidden"  value=""  />
											  
										  <?php
											  }
											  else
											  {
										  ?>
											  <input name="funcPHPEjecutarModif" id="funcPHPEjecutarModif" type="hidden"  value=""  />
										  <?php
											  }
											  
										  ?>	
                                        </div>
                                        
                                        </form>
                                        <br /><br />
                                        <table width="100%">
                                        	<tr>
                                                <td align="center" colspan="2">	

                                                <input type="hidden" id="arrMail" value="<?php echo bE($arrMail)?>" />
                                                <input type="hidden" id="arrContactos" value="<?php echo bE($arrContactos)?>" />
                                                <input type="hidden" id="arrConceptosBase" value="<?php echo bE($arrConceptosBase)?>" />
                                                <input type="hidden" id="arrPercepcionesEmp" value="<?php echo bE($arrPercepcionesEmp)?>" />
                                                <input type="hidden" id="arrDeduccionesEmp" value="<?php echo bE($arrDeduccionesEmp)?>" />
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                	<td colspan="2" align="center">
                                     <input type="button" class="btnAceptar" value="Aceptar" onclick="validarFormulario()" />
                                  	<input type="button" class="btnCancelar" value="Cancelar" onclick="cancelarOperacion()" />
                                    </td>
                                </tr>
                                
                            </table>
                            
			                    


                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
