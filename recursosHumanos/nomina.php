<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->

<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$tituloModulo="Nomina";
?>
<link rel="stylesheet" href="../Scripts/ux/grid/RowEditor.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/ux/grid/RowEditor.js"></script>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../recursosHumanos/Scripts/nomina.js.php"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        
                        
                        
                        
                        <?php
							$idNomina=-1;
							if(isset($objParametros->idNomina))
								$idNomina=$objParametros->idNomina;
                        	echo formatearTituloPagina($tituloModulo);
							$consulta="SELECT * FROM 698_nominasV2 WHERE idNomina=".$idNomina;
							$fNomina=$con->obtenerPrimeraFila($consulta);
							
							$consulta="SELECT COUNT(*) FROM 699_empleadosEjecucionNominaV2 e,703_relacionFoliosCFDI r WHERE e.idNomina=".$idNomina." AND r.idFolio=e.idComprobante AND r.situacion=2";
							$nTimbrados=$con->obtenerValor($consulta);
							
							$consulta="SELECT IF(tipoEmpresa=1,CONCAT(razonSocial,' ',apPaterno,' ',apMaterno),razonSocial) AS nombreEmpresa  FROM 6927_empresas WHERE idEmpresa=".$fNomina[5];
							$nombreEmpresa=$con->obtenerValor($consulta);
						?>
                        	<table width="100%">
                            	<tr>
                                	
                                	<td align="left">
                                    	<table width="920">
                                        <tr>
                                        	<td>
                                                <fieldset class="frameHijoV3"><legend>Nomina</legend>
                                                    <table>
                                                    	<tr>
                                                            <td width="120" valign="top">
                                                                <span class="letraAzulSimple">
                                                                Empresa:
                                                                </span>
                                                            </td>
                                                            <td width="800" valign="top" colspan="3">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	echo $nombreEmpresa;
																?>
                                                                <input type="hidden" id="tDescripcion" value="<?php echo $fNomina[9]?>" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple">
                                                                Descripci&oacute;n:
                                                                </span>
                                                            </td>
                                                            <td  valign="top" colspan="3">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	echo $fNomina[9];
																?>
                                                                <input type="hidden" id="tDescripcion" value="<?php echo $fNomina[9]?>" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple">
                                                                Periodo:
                                                                </span>
                                                            </td>
                                                            <td  valign="top" width="200">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	echo "Del ".date("d/m/Y",strtotime($fNomina[1]))." al ".date("d/m/Y",strtotime($fNomina[2]));

																?>
                                                                <input type="hidden" id="tFInicio" value="<?php echo $fNomina[1]?>" />
                                                                <input type="hidden" id="tFFin" value="<?php echo $fNomina[2]?>" />
                                                                </span>
                                                            </td>
                                                             <td  valign="top" width="110">
                                                                <span class="letraAzulSimple">
                                                                Fecha de pago:
                                                                </span>
                                                            </td>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	echo date("d/m/Y",strtotime($fNomina[3]));

																?>
                                                                <input type="hidden" id="tFechaPago" value="<?php echo $fNomina[3]?>" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple">
                                                                Num. dias pagados:
                                                                </span>
                                                            </td>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	echo $fNomina[4];

																?>
                                                                <input type="hidden" id="tNumDiasPagados" value="<?php echo $fNomina[4]?>" />
                                                                </span>
                                                            </td>
                                                        
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple">
                                                                Timbrar con CSD:
                                                                </span>
                                                            </td>
                                                            <td  valign="top">
                                                                <span class="letraAzulSimple" style="color:#000">
                                                                <?php
																	$consulta="SELECT noCertificado FROM 687_certificadosSelloDigital WHERE idCertificado=".$fNomina[10];
																	$noCertificado=$con->obtenerValor($consulta);
																	
																	$consulta="SELECT serie FROM 688_seriesCertificados WHERE idSerieCertificado=".$fNomina[11];
																	$noSerie=$con->obtenerValor($consulta);
																	
																	echo $noCertificado." (Serie: ".$noSerie.")";
																	
																	$consulta="SELECT idCertificado,noCertificado FROM 687_certificadosSelloDigital WHERE idReferencia=".$fNomina[5]." AND (fechaFinVigencia>='".date("Y-m-d")."' or idCertificado=".$fNomina[10].")  ORDER BY noCertificado";	
																	$arrCertificados="";
																	$resCertificados=$con->obtenerFilas($consulta);
																	while($fCertificado=mysql_fetch_row($resCertificados))
																	{
																		$consulta="SELECT idSerieCertificado,serie FROM 688_seriesCertificados WHERE idCertificado=".$fCertificado[0];
																		$arrSeries=$con->obtenerFilasArreglo($consulta);
																		$o="['".$fCertificado[0]."','".$fCertificado[1]."',".$arrSeries."]";
																		if($arrCertificados=="")
																			$arrCertificados=$o;
																		else
																			$arrCertificados.=",".$o;
																	}
																	$arrCertificados="[".$arrCertificados."]";
																	
																	$consulta="SELECT idSerieCertificado,serie FROM 688_seriesCertificados WHERE idCertificado=".$fNomina[10]." ORDER BY serie";
																	$arrSeries=$con->obtenerFilasArreglo($consulta);
																?>
                                                                <input type="hidden" id="idCertificado" value="<?php echo $fNomina[10]?>" />
                                                                <input type="hidden" id="idSerie" value="<?php echo $fNomina[11]?>" />
                                                                <input type="hidden" id="arrCertificados" value="<?php echo bE($arrCertificados)?>" />
                                                                <input type="hidden" id="arrSeries" value="<?php echo bE($arrSeries)?>" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        
                                                        <?php
                                                        if($nTimbrados==0)
														{
                                                        ?>
                                                        <tr>
                                                            <td  colspan="4" align="right"><br />
                                                                <span class="letraAzulSimple" style="color:rgb(9, 58, 2);font-size:10px">
                                                                <b>Para modificar la informaci&oacute;n de la n&oacute;mina de click <a href="javascript:modificarDatosNomina()"><span style="color:#F00"><b>AQU&Iacute;</b></span></a></b>&nbsp;&nbsp;
                                                                </span>
                                                            </td>
                                                            <td  valign="top">
                                                                
                                                            </td>
                                                        </tr>
                                                        <?php
														}
														?>
                                                    </table>

                                                </fieldset>
                                                <input type="hidden" id="idNomina" value="<?php echo $idNomina?>" />
                                            </td>
                                       </tr>
                                       </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        <?php
							
							
                        
                        	$consulta="SELECT 'Empleados' as empleados,n.idEmpleadoNomina,numEmpleado,CONCAT(rfc1,'-',rfc2,'-',rfc3) AS rfc,apPaterno,apMaterno,nombre,n.totalPercepciones,n.totalDeducciones,montoIncapacidad,montoHorasExtra,sueldoNeto,
										(concat('[',d.cveDepartamento,'] ',nombreDepartamento)) as departamento,(concat('[',p.cvePuesto,'] ',puesto)) as puesto,n.sdi,f.situacion,f.idFolio FROM 
									693_empleadosNominaV2 e,699_empleadosEjecucionNominaV2 n,692_puestosNominaV2 p,691_departamentosNominaV2 d,703_relacionFoliosCFDI f WHERE e.idEmpleado=n.idEmpleado AND n.idNomina=".$idNomina." 
									and e.idPuesto=p.idPuesto and e.idDepartamento=d.idDepartamento and f.idFolio=n.idComprobante";
							

							$configuracion='{
												"inicializar":1,
												"agruparPor":"empleados",
												"confBotones":	[
																	{
																		"tipo":"C",
																		"leyenda":"Timbrar elementos seleccionados",
																		"icono":"../images/page_accept.png",
																		"cuerpoFuncion":"timbrarElementos()"
																	},
																	{
																		"tipo":"-"
																	},
																	{
																		"tipo":"C",
																		"leyenda":"Cancelar elementos seleccionados",
																		"icono":"../images/page_remove.png",
																		"cuerpoFuncion":"cancelarComprobantes()"
																	}
																	
																	
																],
												"confCampos":	[
																	{
																		
																		"campoID":"1",
																		"campo":"idEmpleadoNomina",
																		"formato":"funcionRenderer",
																		"funcionRenderer":"formatearModificarNomina",
																		"ancho":"60",
																		"titulo":""
																	},
																	{
																		"titulo":"Situaci&oacute;n",
																		"alineacion":"I",
																		"ancho":"200",
																		"formato":"funcionRenderer",
																		"funcionRenderer":"formatearSituacionComprobante",
																		
																		"campo":"situacion"	
																	},
																	{
																		
																		"campo":"idFolio",
																		"ancho":"40",
																		"oculto":"1",
																		"titulo":""
																	},
																	{
																		"titulo":"Num. Empleado",
																		"alineacion":"I",
																		"ancho":"95",
																		"campo":"numEmpleado"
																			
																	},
																	
																	{
																		"titulo":"RFC",
																		"alineacion":"I",
																		"ancho":"120",
																		"campo":"rfc",
																		"campoOrden":"1",
																		"direccionOrden":"ASC"	
																		
																	},
																	{
																		"titulo":"Ap. Paterno",
																		"alineacion":"I",
																		"ancho":"100",
																		"campo":"apPaterno"
																			
																	},
																	{
																		"titulo":"Ap. Materno",
																		"alineacion":"I",
																		"ancho":"100",
																		"campo":"apMaterno"
																			
																	},
																	{
																		"titulo":"Nombre",
																		"alineacion":"I",
																		"ancho":"150",
																		"campo":"nombre"
																	},
																	{
																		"titulo":"Puesto",
																		"alineacion":"I",
																		"ancho":"150",
																		"campo":"puesto"
																	},
																	{
																		"titulo":"Departamento",
																		"alineacion":"I",
																		"ancho":"150",
																		"campo":"departamento"
																	},
																	{
																		"titulo":"Nombre",
																		"alineacion":"I",
																		"ancho":"150",
																		"campo":"nombre"
																	},
																	{
																		"titulo":"Total percepciones",
																		"alineacion":"I",
																		"ancho":"130",
																		"formato":"moneda",
																		"campo":"totalPercepciones",
																		"sumary":"sum"	
																	},
																	{
																		"titulo":"(Horas extra)",
																		"alineacion":"I",
																		"ancho":"100",
																		"oculto":"1",
																		"formato":"moneda",
																		"campo":"montoHorasExtra",
																		"sumary":"sum"		
																	},
																	{
																		"titulo":"Total deducciones",
																		"alineacion":"I",
																		"ancho":"130",
																		"formato":"moneda",
																		"campo":"totalDeducciones",
																		"sumary":"sum"		
																	},
																	{
																		"titulo":"(Incapacidades)",
																		"alineacion":"I",
																		"oculto":"1",
																		"ancho":"100",
																		"formato":"moneda",
																		"campo":"montoIncapacidad",
																		"sumary":"sum"		
																	},
																	
																	{
																		"titulo":"Sueldo Neto",
																		"alineacion":"I",
																		"ancho":"100",
																		"formato":"moneda",
																		"campo":"sueldoNeto",
																		"sumary":"sum"		
																	},
																	{
																		"titulo":"SDI",
																		"alineacion":"I",
																		"ancho":"100",
																		"formato":"moneda",
																		"campo":"sdi"
																	},
																	
																	{
																		"titulo":"Empleados",
																		"alineacion":"I",
																		"ancho":"100",
																		"campo":"empleados"	
																	}
																	
																	
																	
																]
											}';
																		
							$funTabla=crearGridDinamico($consulta,$configuracion,"tblTabla",960,500,500);
							echo $funTabla;
							?>
							<table>
                            	<tr>
                                <td align="left">
                        		<span id='tblTabla'></span>
                                </td>
                            </table>
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
