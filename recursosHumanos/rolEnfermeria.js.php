<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__866_gridIncidenciasAdmon,incidencia,funcionJavaScript,funcionValidacion FROM _866_gridIncidenciasAdmon ORDER BY incidencia";
	$res=$con->obtenerFilas($consulta);
	$arrIncidencias="";
	$cadCase="";
	while($fila=mysql_fetch_row($res))
	{
		$obj="['".$fila[0]."','".$fila[1]."']";
		if($arrIncidencias=="")
			$arrIncidencias=$obj;
		else
			$arrIncidencias.=",".$obj;
		
		$cadCase.="case '".$fila[0]."':
						var arrCeldas='';
						var arrCheck=gEN('checkAsigna');
						var x;
						var obj;
						for(x=0;x<arrCheck.length;x++)
						{
							if(arrCheck[x].checked)
							{
								if(arrCeldas=='')
									arrCeldas=arrCheck[x].id;
								else
									arrCeldas+=','+arrCheck[x].id;
								
							}
						}
                                                   
						".$fila[2]."(".$fila[0].",arrCeldas,cmbIncidencia.getRawValue());
					break;
				";
	}
	$arrIncidencias="[".$arrIncidencias."]";
?>	

Ext.onReady(inicializar);

function inicializar()
{
	/*new Ext.Button(
    					{
                        	id:'btnGuardar',
                        	icon:'../images/guardar.PNG',
                            cls:'x-btn-text-icon',
                            text:' Marcar asignaci&oacute;n laboral',
                            renderTo:'spButton',
                            disabled:true,
                            handler:function()
                                    {
                                        function resp(btn)
                                        {
                                        	if(btn=='yes')
                                            {
                                            	var arrCheck=gEN('checkAsigna');
                                                var x;
                                                var obj;
                                                var arrObj='';
                                                var datosUsr;
                                                var accion;
                                                for(x=0;x<arrCheck.length;x++)
                                                {
                                                	if(arrCheck[x].checked)
                                                    	accion=1;
                                                    else
                                                    	accion=-1;
                                                    datosUsr=arrCheck[x].id.split("_");
                                                    obj='{"idUsuario":"'+datosUsr[1]+'","fecha":"'+datosUsr[2]+'","accion":"'+accion+'","idTurno":"'+datosUsr[3]+'"}';
                                                    if(arrObj=='')
                                                        arrObj=obj;
                                                    else
                                                        arrObj+=","+obj;
                                                }
                                                
                                                var objFinal='{"idRol":"-1","idServicio":"'+gE('idServicio').value+'","arrObj":['+arrObj+']}';
                                                function funcAjax()
                                                {
                                                    var resp=peticion_http.responseText;
                                                    arrResp=resp.split('|');
                                                    if(arrResp[0]=='1')
                                                    {
                                                    	msgBox('Las asignaciones han sido guardadas correctamente');
                                                        gEx('btnGuardar').disable();
                                                        gEx('btnGuardar2').disable();
                                                        
                                                    }
                                                    else
                                                    {
                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                    }
                                                }
                                                obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=52&obj='+objFinal,true);
                                                
                                                
                                            }
                                        }
                                        msgConfirm('Est&aacute; seguro de querer guardar los cambios realizados?',resp)
                                        return;
                                    }
                       	}
                       )*/

	new Ext.Button(
    					{
                        	id:'btnGuardar2',
                        	icon:'../images/guardar.PNG',
                            cls:'x-btn-text-icon',
                            text:' Marcar incidencia',
                            renderTo:'spButton2',
                            disabled:true,
                            handler:function()
                                    {
                                    	mostrarVentanaIncidencia();
                                    }
                       	}
                       ) 
	/*new Ext.Button(
    					{
                        	id:'btnGuardar3',
                        	icon:'../images/delete.png',
                            cls:'x-btn-text-icon',
                            text:' Remover incidencias',
                            renderTo:'spButton3',
                            disabled:true,
                            handler:function()
                                    {
                                    	
                                    }
                       	}
                       ) */                                             
}

function clickCelda(check)
{
	var checkBox=gE(check);
    if((checkBox!=undefined)&&(!checkBox.disabled))
	{
    	checkBox.checked=!checkBox.checked;
    	asignaCheck();
    }
}

function mostrarVentanaIncidencia()
{
	var arrIncidencias=<?php echo $arrIncidencias?>;
	var cmbIncidencia=crearComboExt('cmbIncidencia',arrIncidencias,140,5,250);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Incidencia a insertar:'
                                                        },
                                                        cmbIncidencia
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar incidencia',
										width: 540,
										height:130,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	if(cmbIncidencia.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbIncidencia.focus();
                                                                           	}
                                                                        	msgBox('Debe seleccionar la incidencia que desea insertar',resp);
                                                                        	return;
                                                                        }
                                                                        switch(cmbIncidencia.getValue())
                                                                        {
																			<?php echo $cadCase?>
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function asignaCheck()
{
	//gEx('btnGuardar').enable();
    gEx('btnGuardar2').enable();
//    gEx('btnGuardar3').enable();
    detenerEvento();
}

function removerInciencia(id)
{

	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	recargarPagina();
                    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=53&id='+id,true);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover las incidencias asigndas a este d&iacute;a?',resp);
    detenerEvento();
   cancelarEvento();
}