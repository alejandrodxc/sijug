<?php session_start();
	include("latis/conexionBD.php");
	$parametros="";
	$funcion="";
	if(isset($_POST["funcion"]))
	{
		$funcion=$_POST["funcion"];
	}	
	switch($funcion)
	{
		case 1:
			agregarObjetoDegasto();
		break;
		case 2:
			buscarProductos();
		break;
		case 3:
			eliminarProductosObjGasto();
		break;
		case 4:
			obtenerInformacionProducto();
		break;
		case 5:
			obtenerHistoricoProducto();
		break;
		case 6:
			buscarProveedores();
		break;
		case 7:
			cargarProductoPantalla();
		break;
		case 8:
			obtenerListadoProductos();
		break;
		case 9:
			obtenerListadoSolicitudes();
		break;
		case 10:
			obtenerRequisicionesHistoricas();
		break;
		case 11:
			clonarRequisicion();
		break;
		case 12:
			cambiarEtapaRegistros();
		break;
		case 13:
			obtenerHistorialRequisicion();
		break;
		case 14:
			bloquearDesbloquearRequisicion();
		break;
		case 15:
			guardarResultadoEvaluacion();
		break;
		case 16:
			obtenerBitacoraSolicitud();
		break;
		case 17:
			obtenerResponsablesPAT();
		break;
		case 18:
			removerAsignacion();
		break;
		case 19:
			buscarEmpleado();
		break;
		case 20:
			agregarResponsableDeptoPAT();
		break;
		case 21:
			guardarEstructura();
		break;
		case 22:
			eliminarEstructora();
		break;
		case 23:
			obtenerListadoSolicitudesPresupuesto();
		break;
		case 24:
			generarPresupuestoAutorizado();
		break;
		case 25:
			removerDefinicionEstruturaProg();
		break;
		case 26:
			crearNuevaDefinicionEstructura();
		break;
		case 27:
			obtenerCicloDisponibles();
		break;
		case 28:
			modificarSituacionEstructura();
		break;
		case 29:
			obtenerProgramasPlaneacionDisp();
		break;
		case 30:
			guardarProgramasPlaneacion();
		break;
		case 31:
			removerProgramaPlaneacion();
		break;
		case 32:
			obtenerPresupuesto1000();
		break;
		case 33:
			guardarRequerimientoMes();
		break;
		case 34:
			someterEvaluacionPartida1000();
		break;
		case 100:
			obtenerRecursosHumanosServicio();
		break;
		case 101:
			eliminarDepartamentoServicio();
		break;
		case 102:
			guardarDepartamentoServicio();
		break;
		case 103:
			obtenerPuestosDepartamento();
		break;
		case 104:
			guardarRecursoServicio();
		break;
		case 105:
			eliminarRecursoServicio();
		break;
		case 106:
			eliminarServicio();
		break;
		case  107:
			guardarCostoServicio();
		break;
		case  108:
			obtenerCandidatosSolicitud();
		break;
		case  109:
			marcarCandidatosSolicitud();
		break;
		case  110:
			obtenerCandidatosSeleccionados();
		break;
		case  111:
			modificarVacanteSeleccionados();
		break;
		case  112:
			enviarContrato();
		break;
		case  113:
			verElegidos();
		break;
		case 114:
			guardarPartidaComite();
		break;
		case 115:
			obtenerPartidasComiteDisponible();
		break;
		case 116:
			obtenerDatosContrato();
		break;
		case 117:
			obtenerAreasProrrateo();
		break;
		case 118:
			obtenerBasesProrrateo();
		break;
		case 119:
			guardarProrrateoContrato();
		break;
		case 120:
			obtenerActivoFijoDepreciado();
		break;
		case 121:
			guardarProyeccionServicio();
		break;
		case 122:
			obtenerConcentradoRopaje();
		break;
	}
	
	function agregarObjetoDegasto()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		$x=0;
		$idRegistroObjeto=$obj->idRegistroObjeto;
		$query="begin";
		$idProveedor=$obj->idProveedorSugerido;
		if($idProveedor=="")
			$idProveedor="NULL";
		if($con->ejecutarConsulta($query))
		{
			if($idRegistroObjeto!="-1")
			{
				$query="select numEtapa from 9110_objetosGastoVSCiclo where idCodigoGastociclo=".$idRegistroObjeto;
				$numEtapa=$con->obtenerValor($query);
				$incUnidad=0;
				if($numEtapa>1)
				{
				
					$consulta[$x]="INSERT INTO 9110_objetosGastoVSCicloHistorico(idCodigoGastoCiclo,idProducto,cantidad,idProveedorSugerido,responsableModif,fechaModif,costoUnitario,montoTotal,tipoPresupuesto,VERSION,comentarios)
									SELECT idCodigoGastoCiclo,idProducto,cantidad,idProveedorSugerido,'".$_SESSION["idUsr"]."' AS responsableModif,'".date('Y-m-d')."' AS fechaModif,costoUnitario,montoTotal,tipoPresupuesto,VERSION,'' AS comentarios from 9110_objetosGastoVSCiclo where 
									idCodigoGastoCiclo=".$idRegistroObjeto;
					$x++;
					$consulta[$x]="INSERT INTO 9111_cantidaObjVSMesHistorico(idCodigoGastoCicloHistorico,mes,cantidad) SELECT (SELECT LAST_INSERT_ID()) AS idCodigoGastoCicloHistorico,mes,cantidad FROM 9111_cantidaObjVSMes
									WHERE idCodigoGastoCiclo=".$idRegistroObjeto;
					$x++;	
					$incUnidad=1;
				}
						
				$consulta[$x]="DELETE FROM 9111_cantidaObjVSMes WHERE idCodigoGastociclo=".$idRegistroObjeto;
				$x++;
				
				$consulta[$x]=	"UPDATE 9110_objetosGastoVSCiclo SET version=version+".$incUnidad.",cantidad=".$obj->cantidad.",justificacion='".cv($obj->justificacion)."',idProveedorSugerido=".$idProveedor.",observaciones='".cv($obj->observaciones).
								"',responsableModif=".$_SESSION["idUsr"].",fechaUltimaModif='".date('Y-m-d')."',costoUnitario=".$obj->costoUnitario.",montoTotal=".$obj->costoTotal.",tipoPresupuesto=".$obj->tipoPresupuesto." WHERE idCodigoGastociclo=".$idRegistroObjeto;
				$x++;
			}
			else
			{
				$query="SELECT objetoGasto FROM 9101_CatalogoProducto WHERE idProducto=".$obj->idProducto;
				$clave=$con->obtenerValor($query);
				$query="	INSERT INTO 9110_objetosGastoVSCiclo(clave,idCiclo,codDepto,codInstitucion,idProducto,cantidad,justificacion,idProveedorSugerido,observaciones,idPrograma,idResponsable,fechaSolicitud,costoUnitario,montoTotal,tipoPresupuesto)
								VALUES(".$clave.",".$obj->idCiclo.",'".$obj->codDepto."','".$obj->codInstitucion."',".$obj->idProducto.",".$obj->cantidad.",'".cv($obj->justificacion)."',".
								$idProveedor.",'".cv($obj->observaciones)."',".$obj->idPrograma.",".$_SESSION["idUsr"].",'".date("Y-m-d")."',".$obj->costoUnitario.",".$obj->costoTotal.",".$obj->tipoPresupuesto.")";

				if(!$con->ejecutarConsulta($query))
				{
					echo "|";
					return;	
				}
				$idRegistroObjeto=$con->obtenerUltimoID();
				$consulta[$x]="insert into 9110_bitacoraObjetosGastoVSCiclo(idCodigoGastoVSCiclo,etapaOrigen,etapaCambio,idResponsable,fechaCambio,comentarios) values
							(".$idRegistroObjeto.",0,1,".$_SESSION["idUsr"].",'".date('Y-m-d')."','')";
				$x++;		
			}
			$arrMeses=explode(",",$obj->distribucion);
			for($ct=0;$ct<sizeof($arrMeses);$ct++)
			{
				$consulta[$x]="insert into 9111_cantidaObjVSMes(idCodigoGastoCiclo,mes,cantidad) values(".$idRegistroObjeto.",".$ct.",".$arrMeses[$ct].")";
				$x++;	
			}
			
			$consulta[$x]="commit";
			$x++;
			eB($consulta);
		}
		else
			echo "|";
	}
	
	function buscarProductos()
	{
		global $con;
		$criterio=$_POST["criterio"];
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		$ciclo=bD($_POST["ciclo"]);
		$codDepto=bD($_POST["codDepto"]);
		$programa=bD($_POST["programa"]);
		$capitulos=bD($_POST["capitulos"]);
		$arrCapitulos=explode(",",$capitulos);
		$cadCapitulos="";
		global $tipoCosto;
		global $nPromedio;
		global $tipoOG;
		foreach($arrCapitulos as $c)
		{
			$consulta="select codigoControl from 507_objetosGasto where clave=".$c;
			$cControl=$con->obtenerValor($consulta);
			$particula="(o.codigoControlPadre like '".$cControl."%')";
			if($cadCapitulos=="")
				$cadCapitulos=$particula;	
			else
				$cadCapitulos.=" or ".$particula;	
		}
		
		$consulta="SELECT partidas FROM 9130_departamentoVSPrograma WHERE idPrograma=".$programa." AND codigoUnidad='".$codDepto."' AND ciclo=".$ciclo;

		$listPartidas=$con->obtenerListaValores($consulta);
		if($listPartidas=="")
			$listPartidas="-1";
			
		if($cadCapitulos!="")
		{
			$cadCapitulos="(".$cadCapitulos.")";		
			$cadCapitulos.=" and objetoGasto in (".$listPartidas.")";	
		}
		else
			$cadCapitulos.=" objetoGasto in (".$listPartidas.")";	
		if($tipoCosto==1)
			$costo="(SELECT costoUnitario FROM 9103_PedidoDetalle WHERE idProducto=p.idProducto ORDER BY fechaPedido DESC LIMIT 0,1) as costo";	
		else
			$costo="(SELECT avg(costoUnitario) FROM 9103_PedidoDetalle WHERE idProducto=p.idProducto ORDER BY fechaPedido DESC LIMIT 0,".$nPromedio.") as costo";	
		
		
		$consulta="select idProducto from 9110_objetosGastoVSCiclo where idCiclo=".$ciclo." AND codDepto=".$codDepto." AND idPrograma=".$programa;
		$listProductos=$con->obtenerListaValores($consulta);
		if($listProductos=="")
			$listProductos="-1";
		$consulta="select idProducto,nombreProducto,descripcion,".$costo." from 9101_CatalogoProducto p,507_objetosGasto o  where o.clave=p.objetoGasto 
					and  ".$cadCapitulos."  and status_art=1 and 	nombreProducto like '".$criterio."%' and idProducto not in (".$listProductos.")  order by nombreProducto limit ".$inicio.",".$cantidad;
		$res=$con->obtenerFilas($consulta);
		$arrDatos="";
		
		while($fila=mysql_fetch_row($res))
		{
			$costo=$fila[3];
			$obj='{"idProducto":"'.$fila[0].'","nombreProducto":"'.cv($fila[1]).'","descripcion":"'.cv($fila[2]).'","costoUnitario":"'.number_format($costo,2).'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
			
		}
		$consulta="select idProducto,nombreProducto from 9101_CatalogoProducto p, 507_objetosGasto o  where 
				o.clave=p.objetoGasto and ".$cadCapitulos."  and status_art=1 and nombreProducto like '".$criterio."%' and idProducto not in (".$listProductos.")";
		$con->obtenerFilas($consulta);
		$obj='{"num":"'.$con->filasAfectadas.'","personas":['.uDJ($arrDatos).']}';
		echo $obj;
	}
	
	function eliminarProductosObjGasto()
	{
		global $con;
		$idCodigoGastoCiclo=$_POST["idCodigoGastoCiclo"];
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			$consulta="select idCodigoGastociclo,numEtapa from 9110_objetosGastoVSCiclo where idCodigoGastociclo in(".$idCodigoGastoCiclo.")";
			
			$res=$con->obtenerFilas($consulta);
			
			while($fila=mysql_fetch_row($res))
			{
				$idObjeto=$fila[0];
				$numEtapa=$fila[1];
				$incUnidad=0;
				if($numEtapa>1)
				{
				
					$query[$ct]="INSERT INTO 9110_objetosGastoVSCicloHistorico(idCodigoGastoCiclo,idProducto,cantidad,idProveedorSugerido,responsableModif,fechaModif,costoUnitario,montoTotal,tipoPresupuesto,VERSION,comentarios)
									SELECT idCodigoGastoCiclo,idProducto,cantidad,idProveedorSugerido,'".$_SESSION["idUsr"]."' AS responsableModif,'".date('Y-m-d')."' AS fechaModif,costoUnitario,montoTotal,tipoPresupuesto,VERSION,'' AS comentarios from 9110_objetosGastoVSCiclo where 
									idCodigoGastoCiclo=".$idObjeto;
					$ct++;
					$query[$ct]="INSERT INTO 9111_cantidaObjVSMesHistorico(idCodigoGastoCicloHistorico,mes,cantidad) SELECT (SELECT LAST_INSERT_ID()) AS idCodigoGastoCicloHistorico,mes,cantidad FROM 9111_cantidaObjVSMes
									WHERE idCodigoGastoCiclo=".$idObjeto;
												
					$ct++;	
					$incUnidad=1;
				}
			}
			
			$query[$ct]="DELETE FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo in(".$idCodigoGastoCiclo.")";
			
			$ct++;
			$query[$ct]="DELETE FROM 9110_objetosGastoVSCiclo WHERE idCodigoGastoCiclo in (".$idCodigoGastoCiclo.")";
			$ct++;
			$query[$ct]="commit";
			eB($query);
		}
		else
			echo "|";
	}
	
	function obtenerInformacionProducto()
	{
		global $con;
		$id=$_POST["id"];
		$consulta="SELECT * FROM 9110_objetosGastoVSCiclo WHERE idCodigoGastoCiclo=".$id;
		$fila=$con->obtenerPrimeraFila($consulta);
		$conNombre="SELECT nombreProducto FROM 9101_CatalogoProducto WHERE idProducto=".$fila[5];
		$nombre=$con->obtenerValor($conNombre);
		$nombreP="";
		if($fila[8]!="")
		{
			$conNombreProov="SELECT txtRazonSocial2 FROM _405_tablaDinamica WHERE id__405_tablaDinamica=".$fila[8];
			$nombreP=$con->obtenerValor($conNombreProov);
		}
		$conMeses="SELECT cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$id." ORDER BY mes";
		$resMeses=$con->obtenerFilas($conMeses);
		$cadenaHijo="";
		while($filaMes=mysql_fetch_row($resMeses))
		{
			if($cadenaHijo=="")
				$cadenaHijo="'".$filaMes[0]."'";
			else
				$cadenaHijo.=",'".$filaMes[0]."'";
		}
		if($cadenaHijo=="")
			$cadenaHijo="['0','0','0','0','0','0','0','0','0','0','0','0']";
		else
			$cadenaHijo="[".$cadenaHijo."]";
		echo "1|".$fila[5]."|".$nombre."|".$fila[6]."|".$fila[7]."|[".uEJ($cadenaHijo)."]|".$fila[0]."|".$fila[8]."|".$fila[9]."|".$nombreP."|".$fila[15]."|".$fila[16]."|".$fila[19];
	}
	
	function obtenerHistoricoProducto()
	{
		global $con;
		$idProducto=$_POST["idProducto"];
		$idCiclo=$_POST["idCiclo"];
		$codDepartamento=bD($_POST["codigoUnidad"]);
		$idPrograma=bD($_POST["idPrograma"]);
		if($codDepartamento=="")
			$codDepartamento="0001";
		$arreglo="";	
		
		$consulta="SELECT idCodigoGastoCiclo,idProducto,cantidad,idCiclo FROM 9110_objetosGastoVSCiclo WHERE idProducto=".$idProducto." AND idCiclo<>".$idCiclo." AND idPrograma=".$idPrograma." AND codDepto='".$codDepartamento."'";	

		$res=$con->obtenerFilas($consulta);
		$nFilas=$con->filasAfectadas;
		if($nFilas>0)
		{
			$conNombre="SELECT nombreProducto FROM 9101_CatalogoProducto WHERE idProducto=".$idProducto;
			$nombre=$con->obtenerValor($conNombre);
			
			while($filaM=mysql_fetch_row($res))
			{
				$conMeses="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$filaM[0]." ORDER BY mes";
				//echo $conMeses;
				$rMeses=$con->obtenerFilas($conMeses);
				$cadenaHijo="";
				while($fMes=mysql_fetch_row($rMeses))
				{
					$objH="'".$fMes[1]."'";
					$cadenaHijo.=",".$objH;
				
				}
				//echo $cadenaHijo;
				$obj="['".$filaM[0]."','".$filaM[1]."','".$filaM[3]."','".$filaM[2]."'".$cadenaHijo."]";
				//echo $obj;
				if($arreglo=="")
					$arreglo=$obj;
				else
					$arreglo.=",".$obj;
			}
			$arreglo="[".$arreglo."]";
			//echo $arreglo;
			echo "1|".uEJ($arreglo)."|".$nombre;
		}
		else
		{
			echo "2|";
		}
		
	}
	
	function obtenerAbreviaturaMes($num)
	{
		$abreviatura="";
		switch($num)
		{
			case 0:
				$abreviatura="Ene";
			break;
			case 1:
				$abreviatura="Feb";
			break;
			case 2:
				$abreviatura="Mar";
			break;
			case 3:
				$abreviatura="Abr";
			break;
			case 4:
				$abreviatura="May";
			break;
			case 5:
				$abreviatura="Jun";
			break;
			case 6:
				$abreviatura="Jul";
			break;
			case 7:
				$abreviatura="Ago";
			break;
			case 8:
				$abreviatura="Sep";
			break;
			case 9:
				$abreviatura="Oct";
			break;
			case 10:
				$abreviatura="Nov";
			break;
			case 11:
				$abreviatura="Dic";
			break;
		}
		return $abreviatura;
	}
	
	function buscarProveedores()
	{
		global $con;
		$criterio2=$_POST["criterio"];
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		
		$consulta="SELECT id__405_tablaDinamica,txtRazonSocial2 FROM _405_tablaDinamica WHERE txtRazonSocial2 like '".$criterio2."%'   order by txtRazonSocial2 limit ".$inicio.",".$cantidad;
		$res=$con->obtenerFilas($consulta);
		$arrDatos="";
		while($fila=mysql_fetch_row($res))
		{
			$conDatos="SELECT CONCAT(txtDireccion,' ',txtcolonia,' ','-',txtDelegacion,' ',p.nombre)AS direccion,
			txtTelefono1,txtTelefono2,txtFax,txtCorreo,txtRFC  FROM _405_tablaDinamica d, 238_paises p
			WHERE id__405_tablaDinamica=".$fila[0]." AND d.cmbPais=p.idPais";
			$filaD=$con->obtenerPrimeraFila($conDatos);
			
			
			$obj='{"idProovedor":"'.$fila[0].'","nombreProovedor":"'.cv($fila[1]).'","RFC":"'.cv($filaD[5]).'","direccion":"'.cv($filaD[0]).'","tel1":"'.cv($filaD[1]).'","tel2":"'.cv($filaD[2]).'","fax":"'.cv($filaD[3]).'","correo":"'.cv($filaD[5]).'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		$obj='{"num":"'.$con->filasAfectadas.'","proovedores":['.uDJ($arrDatos).']}';
		echo $obj;
	}
	
	function obtenerListadoProductos()
	{
		global $con;
		global $SO;
		$filtroUsuario="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$filtroUsuario.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
					case 'list':
						if($arrFiltro[$x]["field"]=='nombreObjetoGasto')
						{
							$listaClaves=$arrFiltro[$x]["data"]["value"];
							$filtroUsuario.=" and o.clave in (".$listaClaves.")";
						}
						else
						{
							$listaClaves=$arrFiltro[$x]["data"]["value"];
							$filtroUsuario.=" and o.codigoControlPadre in (".$listaClaves.")";
						}
					break;
				}
			}
		}
		$condWhere="";
		if($filtroUsuario!="")
			$condWhere=$filtroUsuario;
		
		
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		
		$ciclo=bD($_POST["ciclo"]);
		$codDepto=bD($_POST["codDepto"]);
		$programa=bD($_POST["programa"]);
		$capitulos=bD($_POST["capitulos"]);
		$arrCapitulos=explode(",",$capitulos);
		$cadCapitulos="";
		global $tipoOG;
		foreach($arrCapitulos as $c)
		{
			$consulta="select codigoControl from 507_objetosGasto where clave=".$c;
			$cControl=$con->obtenerValor($consulta);
			$particula="(o.codigoControlPadre like '".$cControl."%')";
			if($cadCapitulos=="")
				$cadCapitulos=$particula;	
			else
				$cadCapitulos.=" or ".$particula;	
		}
		
		if($cadCapitulos!="")
			$cadCapitulos="(".$cadCapitulos.")";
		$consulta="SELECT partidas FROM 9130_departamentoVSPrograma WHERE idPrograma=".$programa." AND codigoUnidad='".$codDepto."' AND ciclo=".$ciclo;
		$listPartidas=$con->obtenerListaValores($consulta);
		if($listPartidas=="")
			$listPartidas="-1";
			
		$cadCapitulos." and objetoGasto in (".$listPartidas.")";	
		$consulta="SELECT concat('[',clave,'] ',nombreObjetoGasto),codigoControl FROM 507_objetosGasto o WHERE CONCAT(clave,'') LIKE '%00'";
		$res=$con->obtenerFilas($consulta);
		$arrSubPartida=array();
		while($fila=mysql_fetch_row($res))
			$arrSubPartida[$fila[1]]["clave"]=$fila[0];
		
		$consulta="select idProducto from 9110_objetosGastoVSCiclo where idCiclo=".$ciclo." AND codDepto=".$codDepto." AND idPrograma=".$programa;
		$listProductos=$con->obtenerListaValores($consulta);
		if($listProductos=="")
			$listProductos="-1";
		
		$consulta="SELECT cp.idProducto,o.codigoControlPadre,CONCAT('[',cp.0bjetoGasto,'] ',o.nombreObjetoGasto) AS 'nombreObjetoGasto',cp.nombreProducto,cp.descripcion 
						FROM 9101_CatalogoProducto cp,507_objetosGasto o  
						WHERE o.clave=cp.0bjetoGasto AND ".$cadCapitulos." ".$condWhere." and cp.idProducto not in (".$listProductos.") and cp.status_art=1 order by nombreProducto limit ".$inicio." ,".$cantidad;		
		$res=$con->obtenerFilas($consulta);
		$arrDatos="";
		while($fila=mysql_fetch_row($res))
		{
			$obj='{"idProducto":"'.$fila[0].'","subPartida":"'.cv($arrSubPartida[$fila[1]]["clave"]).'","nombreObjetoGasto":"'.cv($fila[2]).'","nombreProducto":"'.cv($fila[3]).'","descripcion":"'.cv($fila[4]).'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		$consulta="SELECT cp.idProducto,o.codigoControlPadre,CONCAT('[',cp.0bjetoGasto,'] ',o.nombreObjetoGasto) AS 'nombreObjetoGasto',cp.nombreProducto,cp.descripcion 
						FROM 9101_CatalogoProducto cp,507_objetosGasto o  
						WHERE o.clave=cp.0bjetoGasto AND ".$cadCapitulos." and cp.status_art=1 and cp.idProducto not in (".$listProductos.") ".$condWhere; 
		$con->obtenerFilas($consulta);
		if($SO==2)
			$obj='{"numReg":"'.$con->filasAfectadas.'","registros":['.($arrDatos).']}';
		else
			$obj='{"numReg":"'.$con->filasAfectadas.'","registros":['.($arrDatos).']}';
		echo $obj;
	
	}
	
	function obtenerListadoSolicitudes()
	{
		global $con;
		global $SO;
		$inicio=0;
		if(isset($_POST["start"]))
			$inicio=$_POST["start"];
		$cantidad=5000;
		if(isset($_POST["limit"]))
			$cantidad=$_POST["limit"];
		$idCiclo=bD($_POST["ciclo"]);
		$codDepartamento=bD($_POST["codigoUnidad"]);
		$idPrograma=bD($_POST["idPrograma"]);
		$capitulos=bD($_POST["capitulos"]);
		$idProceso=$_POST["idProceso"];
		$actor=$_POST["actor"];
		$etapa=$_POST["numEtapa"];
		$cEtapa=$_POST["cEtapa"];
		$idActorProcesoEtapa=$_POST["idActorProcesoEtapa"];
		$arrCapitulos=explode(",",$capitulos);
		$cadCapitulos="";
		global $tipoOG;
		$consulta="select numEtapa from 4037_etapas where idProceso=".$idProceso." order by numEtapa";
		$resEtapas=$con->obtenerFilas($consulta);
		$arrPermisosEtapa=array();
		$arrEtapasOpciones="";
		while($filaEtapa=mysql_fetch_row($resEtapas))
		{
			$permisos="";
			if(strpos($actor,"_")!==false)
				$consulta="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE actor='".$actor."' AND idProceso=".$idProceso." AND tipoActor=1 and numEtapa=".$filaEtapa[0];
			else
				$consulta="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE actor='".$actor."' AND idProceso=".$idProceso." AND tipoActor=2 and numEtapa=".$filaEtapa[0];
			$idAProcesoEtapa=$con->obtenerValor($consulta);	   
			if($idAProcesoEtapa=="")
				$idAProcesoEtapa="-1";
			$consulta="SELECT idGrupoAccion,complementario,idAccionesProcesoEtapaVSAcciones FROM 947_actoresProcesosEtapasVSAcciones WHERE idActorProcesoEtapa=".$idAProcesoEtapa;
			$arrAcciones=$con->obtenerFilasArregloPHP($consulta);
			$permisos="";
			if(existeValorMatriz($arrAcciones,"13")!=-1)
			{
				$permisos="['A','']";
			}
			if(existeValorMatriz($arrAcciones,"2")!=-1)
			{
				if($permisos=="")
					$permisos="['M','']";
				else
					$permisos.=",['M','']";
			}
			if(existeValorMatriz($arrAcciones,"7")!=-1)
			{
				if($permisos=="")
					$permisos.="['E','']";
				else
					$permisos.=",['E','']";
			}
			if(existeValorMatriz($arrAcciones,"6")!=-1)
			{
				if($permisos=="")
					$permisos="['B','']";	
				else
					$permisos.=",['B','']";	
			}
			$pos=existeValorMatriz($arrAcciones,"11");
			if($pos!=-1)
			{
				if($permisos=="")
					$permisos="['D','']";	
				else
					$permisos.=",['D','']";	
				$idAccionProceso=$arrAcciones[$pos][2];
				
				$consulta="SELECT valor,contenido,etapa FROM 9114_opcionesEvaluacion WHERE idAccion=".$idAccionProceso." AND idIdioma=".$_SESSION["leng"];
				$arrOpciones=$con->obtenerFilasArreglo($consulta);
				
				$obj="['".$filaEtapa[0]."',".$arrOpciones."]";
				if($arrEtapasOpciones=="")
					$arrEtapasOpciones=$obj;
				else
					$arrEtapasOpciones.=",".$obj;
			}
			$pos=existeValorMatriz($arrAcciones,"1");
			if($pos!=-1)
			{
				$etapaSomete=$arrAcciones[$pos][1];
				if($permisos=="")
					$permisos="['S','".$arrAcciones[$pos][1]."']";
				else
					$permisos.=",['S','".$arrAcciones[$pos][1]."']";
			}	   
			
			if(existeValorMatriz($arrAcciones,"14")!=-1)
			{
				if($permisos=="")
					$permisos="['G','']";	
				else
					$permisos.=",['G','']";	
			}
			$arrPermisosEtapa[removerCerosDerecha($filaEtapa[0])]="[".$permisos."]";
		}
		
		if(strpos($actor,"_")!==false)
		{
			foreach($arrCapitulos as $c)
			{
				$consulta="select codigoControl from 507_objetosGasto where clave=".$c;
				$cControl=$con->obtenerValor($consulta);
				
				$particula="(o.codigoControlPadre like '".$cControl."%')";
				if($cadCapitulos=="")
					$cadCapitulos=$particula;	
				else
					$cadCapitulos.=" or ".$particula;	
			}
		}
		else
		{
			$consulta="SELECT idComite FROM 235_proyectosVSComites WHERE idProyectoVSComite=".$actor;
			$idComite=$con->obtenerValor($consulta);
			$consulta="SELECT idProyectoVSComiteVSEtapa FROM 234_proyectosVSComitesVSEtapas WHERE idProyecto=".$idProceso." AND idComite=".$idComite." AND numEtapa=".$etapa;
			$idProyectoCE=$con->obtenerValor($consulta);
			$consulta="SELECT p.partida,o.codigoControl FROM 9044_proyectoComitePartida p,507_objetosGasto o WHERE o.clave=p.partida AND idProyectoComite=".$idProyectoCE;
			$resPartidas=$con->obtenerFilas($consulta);
			while($filaPartidas=mysql_fetch_row($resPartidas))
			{
				$particula="(o.codigoControlPadre like '".$filaPartidas[1]."%')";
				if($cadCapitulos=="")
					$cadCapitulos=$particula;	
				else
					$cadCapitulos.=" or ".$particula;
			}
		}
		
		if($cadCapitulos!="")
			$cadCapitulos="(".$cadCapitulos.")";
		$filtroUsuario="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$filtroUsuario.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
					case 'list':
						if($arrFiltro[$x]["field"]=='nombreObj')
						{
							$listaClaves=$arrFiltro[$x]["data"]["value"];
							$filtroUsuario.=" and o.clave in (".$listaClaves.")";
						}
						
					break;
				}
			}
		}
		$condWhere="";
		if($filtroUsuario!="")
			$condWhere=$filtroUsuario;
		$condFiltro="";
		$consulta="SELECT idGrupoAccion,complementario FROM 947_actoresProcesosEtapasVSAcciones WHERE idGrupoAccion=11 and  idActorProcesoEtapa=".$idActorProcesoEtapa;
		$fila=$con->obtenerPrimeraFila($consulta);
		if($fila)
		{
			$complementario=$fila[1];
			switch($complementario)
			{
				case 1:
					$condFiltro="";
				break;	
				case 2:
					$condFiltro=" and codInstitucion='".$_SESSION["codigoInstitucion"]."'";
				break;	
				case 3:
					$condFiltro=" and idResponsable=".$_SESSION["idUsr"];
				break;	
				case 4:
					$condFiltro=" AND codDepto='".$codDepartamento."'";
				break;	
				case 5:
					$condFiltro=" AND codDepto like '".$codDepartamento."%'";
				break;	
				case 6:
					$condFiltro=" and idPrograma=".$idPrograma;
				break;	
				case 7:
					$condFiltro=" AND codDepto='".$codDepartamento."' and idPrograma=".$idPrograma;
				break;
			}
		}
		else
		{
			$consulta="SELECT idAccion,complementario FROM 949_actoresVSAccionesProceso WHERE idAccion=9 and  actor='".$actor."' and idProceso=".$idProceso;
			$fila=$con->obtenerPrimeraFila($consulta);
			if($fila)
			{
				$complementario=$fila[1];
				switch($complementario)
				{
					case 1:
						$condFiltro="";
					break;	
					case 2:
						$condFiltro=" and codInstitucion='".$_SESSION["codigoInstitucion"]."'";
					break;	
					case 3:
						$condFiltro=" and idResponsable=".$_SESSION["idUsr"];
					break;	
					case 4:
						$condFiltro=" AND codDepto='".$codDepartamento."'";
					break;	
					case 5:
						$condFiltro=" AND codDepto = '".$codDepartamento."' and codDepto like '".$codDepartamento."%'";
					break;	
					case 6:
						$condFiltro=" and idPrograma=".$idPrograma;
					break;	
					case 7:
						$condFiltro=" AND codDepto='".$codDepartamento."' and idPrograma=".$idPrograma;
					break;
				}
			}
		}
		
		if(strpos($actor,"_")!==false)
			$consulta="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE actor='".$actor."' AND tipoActor=1 AND idProceso=".$idProceso." AND numEtapa=1";
		else
			$consulta="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE actor='".$actor."' AND tipoActor=2 AND idProceso=".$idProceso." AND numEtapa=1";
		$filaActor=$con->obtenerPrimeraFila($consulta);
		if(!$filaActor)
			$condFiltro.=" and numEtapa=".$etapa;
		$condAuxiliar="";
		if($cEtapa==1)
		{
			$condAuxiliar=" and c.numEtapa=".$etapa;
		}
		$consulta="SELECT idCodigoGastoCiclo,c.clave,CONCAT('[',c.clave,']',nombreObjetoGasto) AS nombreObj,idCiclo,p.nombreProducto,
					justificacion,c.cantidad,c.observaciones,costoUnitario as costo,p.descripcion, org.unidad,montoTotal,c.modificable,
					(SELECT nombreEtapa FROM 4037_etapas WHERE idProceso=".$idProceso." AND numEtapa=c.numEtapa) as etapa,
					(SELECT tituloPrograma FROM 517_programas WHERE idPrograma=c.idPrograma) as programa,
					(select Nombre from 800_usuarios where idUsuario=c.idResponsable) as responsableSolicitud,
					(select tituloTipoP from 508_tiposPresupuesto where idTipoPresupuesto=c.tipoPresupuesto) as tipoPresupuesto,version,c.numEtapa
			   FROM 9110_objetosGastoVSCiclo c,507_objetosGasto o,9101_CatalogoProducto p,817_organigrama org 
			   WHERE idCiclo=".$idCiclo.$condFiltro." AND org.codigoUnidad=c.codDepto  AND c.clave=o.clave AND 
			   ".$cadCapitulos." and p.idProducto=c.idProducto ".$condWhere.$condAuxiliar." order by nombreProducto limit ".$inicio.",".$cantidad;
		//echo $consulta;
		$sqlQuery=bE($consulta);
		$res=$con->obtenerFilas($consulta);
		$consulta="SELECT count(idCodigoGastoCiclo)
			   FROM 9110_objetosGastoVSCiclo c,507_objetosGasto o,9101_CatalogoProducto p  where p.idProducto=c.idProducto and o.clave=c.clave and
			   idCiclo=".$idCiclo.$condFiltro."  AND 
			   ".$cadCapitulos." ".$condWhere;
		   
		$numRegistros=$con->obtenerValor($consulta);
		$arrDatos='';
		while($fila=mysql_fetch_row($res))
		{
			$conCantidades="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$fila[0]." order by mes";
			$filas=$con->obtenerFilas($conCantidades);
			$nFilas=$con->filasAfectadas;
			$cadenaMeses="";
			if($nFilas>0)
			{
				$cadenaAux="<table>";
				$fCabecera="<tr>";
				$fDatos="<tr>";
				
				while($fMeses=mysql_fetch_row($filas))
				{
					
					$fCabecera.='<td width="60" align="center"><span class="corpo8_bold">'.obtenerAbreviaturaMes($fMeses[0]).'</span></td>';
					$fDatos.='<td align="center">'.$fMeses[1].'</td>';
				}
			}
			$fCabecera.="</tr>";
			$fDatos.="</tr>";
			$cadenaAux.=$fCabecera.'<tr height="2"><td colspan="12" style="background:#600"></td></tr>'.$fDatos;
			$cadenaAux.="</table>";
			
			$obj='{"idCodigoGastoCiclo":"'.$fila[0].'","clave":"'.cv($fila[1]).'","nombreObj":"'.cv($fila[2]).'","idCiclo":"'.cv($fila[3]).'","nombreProducto":"'.cv($fila[4]).'","justificacion":"'.
					cv(str_replace("#R","",$fila[5])).'","cantidad":"'.cv($fila[6]).'","cadenaMeses":"'.cv($cadenaAux).'","observaciones":"'.cv(str_replace("#R","",$fila[7])).'","costoUnitario":"'.cv($fila[8]).
					'","costoTotal":"'.($fila[11]).'","descripcion":"'.cv($fila[9]).'","depto":"'.cv($fila[10]).'","modificable":"'.$fila[12].'","etapa":"'.$fila[13].
					'","programa":"'.$fila[14].'","responsableSolicitud":"'.$fila[15].'","tipoPresupuesto":"'.$fila[16].'","version":"'.$fila[17].'","permisos":"'.$arrPermisosEtapa[removerCerosDerecha($fila[18])].'","numEtapa":"'.$fila[18].'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		//$sqlQuery="";
		if($SO==2)
			$obj='{"arrEtapasOpciones":"['.$arrEtapasOpciones.']","sqlQuery":"'.$sqlQuery.'","numReg":"'.$numRegistros.'","registros":['.($arrDatos).']}';
		else
			$obj='{"arrEtapasOpciones":"['.$arrEtapasOpciones.']","sqlQuery":"'.$sqlQuery.'","numReg":"'.$numRegistros.'","registros":['.($arrDatos).']}';
		echo $obj;
	}
	
	function cargarProductoPantalla()
	{
		global $con;
		$idCodigoGastoCiclo=$_POST["idCodigoG"];
		$consulta="SELECT * FROM 9110_objetosGastoVSCiclo WHERE idCodigoGastoCiclo=".$idCodigoGastoCiclo;
		
		$fila=$con->obtenerPrimeraFila($consulta);
		if($fila)
		{
			$conNombre="SELECT nombreProducto FROM 9101_CatalogoProducto WHERE idProducto=".$fila[5];
			$nombre=$con->obtenerValor($conNombre);
			$nombreP="";
			if($fila[8]!="")
			{
				$conNombreProov="SELECT txtRazonSocial2 FROM _405_tablaDinamica WHERE id__405_tablaDinamica=".$fila[8];
				$nombreP=$con->obtenerValor($conNombreProov);
			}
			$conMeses="SELECT cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$idCodigoGastoCiclo." ORDER BY mes";
			$resMeses=$con->obtenerFilas($conMeses);
			$cadenaHijo="";
			while($filaMes=mysql_fetch_row($resMeses))
			{
				if($cadenaHijo=="")
					$cadenaHijo="'".$filaMes[0]."'";
				else
					$cadenaHijo.=",'".$filaMes[0]."'";
			}
			if($cadenaHijo=="")
				$cadenaHijo="['0','0','0','0','0','0','0','0','0','0','0','0']";
			else
				$cadenaHijo="[".$cadenaHijo."]";
			echo "1|".$fila[5]."|".$fila[6]."|".$fila[8]."|[".$cadenaHijo."]|".uEJ($nombre)."|".uEJ($nombreP);
		}
	}
	
	function obtenerRequisicionesHistoricas()
	{
		global $con;
		$cicloActual=bD($_POST["ciclo"]);
		$codDepartamento=bD($_POST["codigoUnidad"]);
		$idPrograma=bD($_POST["idPrograma"]);
		$consulta="select distinct idCiclo from 9110_objetosGastoVSCiclo where idCiclo<>".$cicloActual." and codDepto='".$codDepartamento."' and idPrograma=".$idPrograma." order by idCiclo desc";
		$resCiclos=$con->obtenerFilas($consulta);	
		$arrCiclos="";
		while($filaCiclo=mysql_fetch_row($resCiclos))
		{
			$consulta="select sum(montoTotal) from 9110_objetosGastoVSCiclo where codDepto='".$codDepartamento."' and idPrograma=".$idPrograma." and idCiclo=".$filaCiclo[0];
			$montoTotal=$con->obtenerValor($consulta);
			$obj="['".$filaCiclo[0]."','".$montoTotal."']";
			if($arrCiclos=="")
				$arrCiclos=$obj;
			else
				$arrCiclos.=",".$obj;
		}
		echo "1|[".$arrCiclos."]";
	}
	
	function clonarRequisicion()
	{
		$cicloDestino=$_POST["cicloDestino"];
		$cicloOrigen=$_POST["cicloOrigen"];
		$codigoDepto=$_POST["codigoDepto"];
		$idPrograma=$_POST["idPrograma"];
		$capitulos=bD($_POST["capitulos"]);
		$arrCapitulos=explode(",",$capitulos);
		$cadCapitulos="";
		global $con;
		global $tipoCosto;
		global $nPromedio;
		global $tipoOG;
		
		foreach($arrCapitulos as $c)
		{
			$consulta="select codigoControl from 507_objetosGasto where clave=".$c;
			$cControl=$con->obtenerValor($consulta);
			$particula="(o.codigoControlPadre like '".$cControl."%')";
			if($cadCapitulos=="")
				$cadCapitulos=$particula;	
			else
				$cadCapitulos.=" or ".$particula;	
		}
		if($cadCapitulos!="")
			$cadCapitulos="(".$cadCapitulos.")";
		$consulta="SELECT partidas FROM 9130_departamentoVSPrograma WHERE idPrograma=".$idPrograma." AND codigoUnidad='".$codigoDepto."' AND ciclo=".$cicloDestino;
		$listPartidas=$con->obtenerListaValores($consulta);
		if($listPartidas=="")
			$listPartidas="-1";
			
		$cadCapitulos." and objetoGasto in (".$listPartidas.")";	
		$query="delete FROM 9110_objetosGastoVSCiclo  WHERE idCiclo=".$cicloDestino." AND codDepto='".$codigoDepto."' and idPrograma=".$idPrograma;
		
		if(!$con->ejecutarConsulta($query))
		{
			echo "|";
			return ;	
		}
		
		$query="SELECT * FROM 9110_objetosGastoVSCiclo c,507_objetosGasto o WHERE c.clave=o.clave and 
				idCiclo=".$cicloOrigen." AND codDepto='".$codigoDepto."' and idPrograma=".$idPrograma." and ".$cadCapitulos;
		$resProductos=$con->obtenerFilas($query);
		$x=0;
		$query="begin";
		if($con->ejecutarConsulta($query))
		{
			
			while($fila=mysql_fetch_row($resProductos))
			{
				$idProveedor=$fila[8];
				if($idProveedor=="")
					$idProveedor="NULL";
				$query="SELECT objetoGasto,status_art FROM 9101_CatalogoProducto WHERE idProducto=".$fila[5];
				$filaArt=$con->obtenerPrimeraFila($query);
				$clave=$filaArt[0];
				$status=$filaArt[1];
				if($status==1)
				{
					if($tipoCosto==1)
						$costo="SELECT costoUnitario FROM 9103_PedidoDetalle WHERE idProducto=".$fila[5]." ORDER BY fechaPedido DESC LIMIT 0,1";	
					else
						$costo="SELECT avg(costoUnitario) FROM 9103_PedidoDetalle WHERE idProducto=".$fila[5]." ORDER BY fechaPedido DESC LIMIT 0,".$nPromedio;	
					
					$costo=$con->obtenerValor($costo);
					$query="	INSERT INTO 9110_objetosGastoVSCiclo(clave,idCiclo,codDepto,codInstitucion,idProducto,cantidad,justificacion,
								idProveedorSugerido,observaciones,idPrograma,idResponsable,fechaSolicitud,costoUnitario,montoTotal,numEtapa,modificable,tipoPresupuesto,situacion)
										VALUES(".$clave.",".$cicloDestino.",'".$codigoDepto."','".$fila[4]."',".$fila[5].",".$fila[6].",'',".
										$idProveedor.",'',".$idPrograma.",".$_SESSION["idUsr"].",'".date("Y-m-d")."',".$costo.",".($costo*$fila[6]).",1,1,".$fila[19].",1)";
					
					if(!$con->ejecutarConsulta($query))
					{
						$query="rollback";
						$con->ejecutarConsulta($query);
						echo "|";
						return;
					}
					$idRegistro=$con->obtenerUltimoID();
					
					$query="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$fila[0]." order by mes";
					
					$resMes=$con->obtenerFilas($query);
					while($filaMes=mysql_fetch_row($resMes))
					{
						$query="insert into 9111_cantidaObjVSMes(idCodigoGastoCiclo,mes,cantidad) values(".$idRegistro.",".$filaMes[0].",".$filaMes[1].")";
						if(!$con->ejecutarConsulta($query))
						{
							$query="rollback";
							$con->ejecutarConsulta($query);
							echo "|";
							return;
						}	
					}
				}
			}
			$query="commit";
			eC($query);
		}
		else
		{
			echo "|";	
		}
	}
	
	function cambiarEtapaRegistros()
	{
		global $con;
		$cadObj=$_POST["obj"];
		$obj=json_decode($cadObj);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$etapaAnt=0;
		foreach($obj->arrRegistros as $registro)
		{
			$consulta[$x]="update 9110_objetosGastoVSCiclo SET numEtapa=".$registro->etapaSomete." WHERE idCodigoGastoCiclo=".$registro->idRegistro;
			$x++;
			$consulta[$x]="insert into 9110_bitacoraObjetosGastoVSCiclo(idCodigoGastoVSCiclo,etapaOrigen,etapaCambio,idResponsable,fechaCambio,comentarios) values
							(".$registro->idRegistro.",".$etapaAnt.",".$registro->etapaSomete.",".$_SESSION["idUsr"].",'".date('Y-m-d')."','')";
			$x++;		
		}
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
	function obtenerHistorialRequisicion()
	{
		global $con;
		$idRegistroObjeto=$_POST["idRegistroObjeto"];
		$consulta="SELECT idCodigoGastoCicloHistorico,VERSION,cantidad,costoUnitario,montoTotal,
					(SELECT tituloTipoP FROM 508_tiposPresupuesto WHERE idTipoPresupuesto=h.tipoPresupuesto) AS tipoPresupuesto,
					comentarios,(SELECT txtRazonSocial2 FROM _405_tablaDinamica WHERE id__405_tablaDinamica=h.idProveedorSugerido) AS idProveedorSugerido,
					u.Nombre,DATE_FORMAT(fechaModif,'%d/%m/%Y') AS fechaModif FROM
					9110_objetosGastoVSCicloHistorico h,800_usuarios u WHERE u.idUsuario=h.responsableModif AND idCodigoGastoCiclo=".$idRegistroObjeto.
					" ORDER BY VERSION desc";	
		$res=$con->obtenerFilas($consulta);
		$arrDatos="";
		while($fila=mysql_fetch_row($res))
		{
			$conCantidades="SELECT mes,cantidad FROM 9111_cantidaObjVSMesHistorico WHERE idCodigoGastoCicloHistorico=".$fila[0]." order by mes";
			$filas=$con->obtenerFilas($conCantidades);
			$nFilas=$con->filasAfectadas;
			$cadenaMeses="";
			if($nFilas>0)
			{
				$cadenaAux="<table>";
				$fCabecera="<tr>";
				$fDatos="<tr>";
				
				while($fMeses=mysql_fetch_row($filas))
				{
					
					$fCabecera.='<td width="60" align="center"><span class="corpo8_bold">'.obtenerAbreviaturaMes($fMeses[0]).'</span></td>';
					$fDatos.='<td align="center">'.$fMeses[1].'</td>';
				}
			}
			$fCabecera.="</tr>";
			$fDatos.="</tr>";
			$cadenaAux.=$fCabecera.'<tr height="2"><td colspan="12" style="background:#600"></td></tr>'.$fDatos;
			$cadenaAux.="</table>";
			$obj='{"idCodigo":"'.$fila[0].'","cantidad":"'.cv($fila[2]).'","cadenaMeses":"'.cv($cadenaAux).'","costoUnitario":"'.cv($fila[3]).
					'","costoTotal":"'.($fila[4]).'","comentarios":"'.cv($fila[6]).'","responsableCambio":"'.$fila[8].'","fechaCambio":"'.$fila[9].
					'","proveedorSugerido":"'.$fila[7].'","tipoPresupuesto":"'.$fila[5].'","version":"'.$fila[1].'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		echo '{"registros":['.$arrDatos.']}';
	}
	
	
	function bloquearDesbloquearRequisicion()
	{
		global $con;	
		$arrCambios=$_POST["arrCambios"];
		$arrObj=json_decode($arrCambios);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		foreach($arrObj->arrCambios as $obj)
		{
			$consulta[$x]="update 9110_objetosGastoVSCiclo set modificable=".$obj->valor." where idCodigoGastoCiclo=".$obj->idRegistro;
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
	function guardarResultadoEvaluacion()
	{
		global $con;
		$cadObj=$_POST["obj"];
		$obj=json_decode($cadObj);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		foreach($obj->arrRegistros as $registro)
		{
			$query="select numEtapa from 9110_objetosGastoVSCiclo where idCodigoGastoCiclo=".$registro->idRegistro;
			$etapaAnt=$con->obtenerValor($query);
			if($etapaAnt=="")
				$etapaAnt=0;
			$consulta[$x]="update 9110_objetosGastoVSCiclo SET numEtapa=".$registro->etapaSomete." WHERE idCodigoGastoCiclo=".$registro->idRegistro;
			$x++;		
			$consulta[$x]="insert into 9110_bitacoraObjetosGastoVSCiclo(idCodigoGastoVSCiclo,etapaOrigen,etapaCambio,idResponsable,fechaCambio,comentarios) values
							(".$registro->idRegistro.",".$etapaAnt.",".$registro->etapaSomete.",".$_SESSION["idUsr"].",'".date('Y-m-d')."','".cv($registro->comentarios)."')";
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
	function obtenerBitacoraSolicitud()
	{
		global $con;
		$idProceso=$_POST["idProceso"];
		$idRegistro=$_POST["idRegistro"];
		$consulta="select (if(etapaOrigen=0,'Sin antecedentes',(select nombreEtapa from 4037_etapas where numEtapa=b.etapaOrigen and idProceso=".$idProceso."))) as eOrigen,etapaOrigen,
					(select nombreEtapa from 4037_etapas where numEtapa=b.etapaCambio and idProceso=".$idProceso.") as eCambio,etapaCambio,
					(select Nombre from 800_usuarios where idUsuario=b.idResponsable) as responsable,date_format(fechaCambio,'%d/%m/%Y') as fechaCambio,
					comentarios from 9110_bitacoraObjetosGastoVSCiclo b where idCodigoGastoVSCiclo=".$idRegistro." order by idBitacoraVSCiclo desc";
		$res=$con->obtenerFilas($consulta);
		$arrElem="";
		$obj="";
		while($fila=mysql_fetch_row($res))
		{
			$obj="['".removerCerosDerecha($fila[1]).".- ".$fila[0]."','".removerCerosDerecha($fila[3]).".- ".$fila[2]."','".$fila[4]."','".$fila[5]."','".$fila[6]."']";
			if($arrElem=="")
				$arrElem=$obj;
			else
				$arrElem.=",".$obj;
		}
		echo "1|[".$arrElem."]";
	}
	
	function obtenerResponsablesPAT()
	{
		global $con;
		$idProceso=$_POST["idProceso"];
		$tipoAsignacion=$_POST["tipoAsignacion"];
		
		$comp="";
		switch($tipoAsignacion)
		{
			case 0:
			break;	
			case 1:
				$comp=" and codigoUnidad='".$_SESSION["codigoUnidad"]."'";
			break;
			case 2:
				$comp=" and (codigoUnidad='".$_SESSION["codigoUnidad"]."%' or codigoUnidad='".$_SESSION["codigoUnidad"]."')";
			break;
		}
		
		$consulta="select idResponsablePAT as idAsignacion,o.unidad as departamento,r.codigoDepto as codDepto,
					idPrograma,(SELECT tituloPrograma FROM 517_programas WHERE idPrograma=r.idPrograma) as programa,
					idResponsable,(select Nombre from 800_usuarios where idUsuario=r.idResponsable) as responsable from 9116_responsablesPAT r, 817_organigrama o
					where o.codigoUnidad=r.codigoDepto and idProceso=".$idProceso.$comp." order by departamento";	
		$arrRegistros=utf8_encode($con->obtenerFilasJson($consulta));
		echo '{"registros":'.$arrRegistros.'}';
	}
	
	function removerAsignacion()
	{
		global $con;
		$idAsignacion=$_POST["idAsignacion"];
		$consulta="delete from 9116_responsablesPAT where idResponsablePAT=".$idAsignacion;
		eC($consulta);
	}
	
	function buscarEmpleado()
	{
		global $con;
		$criterio=$_POST["criterio"];
		$campoB=$_POST["campoBusqueda"];
		$ambitoAsignacion=$_POST["ambitoAsignacion"];
		
		$comp="";
		$depto=$_SESSION["codigoUnidad"];
		switch($ambitoAsignacion)
		{
			case 0:
				
			break;
			case 1:
				$comp=" and a.codigoUnidad='".$depto."'";
			break;
			case 2:
				$comp=" and (a.codigoUnidad = '".$depto."' or a.codigoUnidad like '".$depto."%')";
			break;
		}
		switch($campoB)
		{
			case "1": //Paterno
				$consulta="(select i.idUsuario,concat('<b>',Paterno,'</b>') as Paterno,Materno,Nom,Nombre,'' as Status,a.codigoUnidad,o.unidad 
				from 802_identifica i,801_adscripcion a ,817_organigrama o 
				where o.codigoUnidad=a.codigoUnidad and a.idUsuario=i.idUsuario ".$comp."  and  Paterno like '".$criterio."%'   order by Paterno,Materno,Nom asc)";
			break;
			case "2": //Materno
				$consulta=" (select i.idUsuario,Paterno,concat('<b>',Materno,'</b>') as Materno,Nom,Nombre,'' as Status,a.codigoUnidad,o.unidad
				from 802_identifica i,801_adscripcion a ,817_organigrama o 
				where o.codigoUnidad=a.codigoUnidad and a.idUsuario=i.idUsuario ".$comp." and  Materno like '".$criterio."%' order by Materno,Paterno,Nom asc)";
			break;
			case "3": //Nombre
				$consulta=" (select i.idUsuario,Paterno, Materno,concat('<b>',Nom,'</b>') as Nom,Nombre,'' as Status,a.codigoUnidad,o.unidad 
				from 802_identifica i,801_adscripcion a ,817_organigrama o 
				where o.codigoUnidad=a.codigoUnidad and a.idUsuario=i.idUsuario ".$comp." and Nom like '".$criterio."%' order by Nom,Paterno,Materno asc)";
			break;
		}
		$res=$con->obtenerFilas($consulta);
		$arrDatos="";
		while($fila=mysql_fetch_row($res))
		{
			$situaciones="";
			$departamento=$fila[7];
			$codigoDepto=$fila[6];

			
			$obj='{"idUsuario":"'.$fila[0].'","Paterno":"'.$fila[1].'","Materno":"'.$fila[2].'","Nom":"'.$fila[3].'","Nombre":"'.$fila[4].'",
			"Status":"'.$situaciones.'","departamento":"'.$departamento.'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		$obj='{"num":"'.$con->filasAfectadas.'","personas":['.uDJ($arrDatos).']}';
		echo $obj;
	}


	function agregarResponsableDeptoPAT()
	{
		global $con;
		$cadObj=$_POST["obj"];
		$obj=json_decode($cadObj);
		$consulta="INSERT INTO 9116_responsablesPAT(codigoDepto,idPrograma,idProceso,idResponsable,rolActor)
					VALUES('".$obj->depto."','".$obj->programa."',".$obj->idProceso.",".$obj->idUsuario.",'".str_replace("|","_",$obj->rolAsigna)."')";
		eC($consulta);						
	}
	
	function guardarEstructura()
	{
		global $con;
		$cadObj=$_POST["obj"];
		$obj=json_decode($cadObj);
		if($obj->idRegistro=="-1")
		{
			$consulta="INSERT INTO 9117_estructuraPAT(grupoFuncional,funcion,subFuncion,programaGasto,actividadInstitucional,partidaPresupuestal,ciclo,codigoInstitucion,ruta)
						VALUES('".$obj->grupoFuncional."','".$obj->funcion."','".$obj->subfuncion."','".$obj->programaGasto."','".$obj->actividadInstitucional."','".$obj->partidaPresupuestal."'
						,".$obj->ciclo.",'".$obj->institucion."','".$obj->grupoFuncional."_".$obj->funcion."_".$obj->subfuncion."_".$obj->programaGasto."_".$obj->actividadInstitucional."_".$obj->partidaPresupuestal."')";
		}
		else
		{
			$consulta="update 9117_estructuraPAT set grupoFuncional='".$obj->grupoFuncional."',funcion='".$obj->funcion."',subFuncion='".$obj->subfuncion."',programaGasto='".$obj->programaGasto."',
					actividadInstitucional='".$obj->actividadInstitucional."',partidaPresupuestal='".$obj->partidaPresupuestal."',
					ruta='".$obj->grupoFuncional."_".$obj->funcion."_".$obj->subfuncion."_".$obj->programaGasto."_".$obj->actividadInstitucional."_".$obj->partidaPresupuestal."' where idEstructura=".$obj->idRegistro;

		}
		
		if($con->ejecutarConsulta($consulta))
		{
			$obj->idRegistro=$con->obtenerUltimoID();
			$consulta="SELECT idEstructura,grupoFuncional,funcion,subFuncion,programaGasto,actividadInstitucional,partidaPresupuestal FROM 9117_estructuraPAT where idEstructura=".$obj->idRegistro;
			$arrRegistros=$con->obtenerFilasArreglo($consulta);
			echo "1|".$arrRegistros;
		}
		
	}
	
	function eliminarEstructora()
	{
		global $con;
		$listRegistros=$_POST["listRegistros"];
		$consulta="delete from 9117_estructuraPAT where idEstructura in (".$listRegistros.")";
		eC($consulta);
	}
	
	function obtenerRecursosHumanosServicio()
	{
		global $con;
		$idServicio=$_POST["idServicio"];
		global $SO;
		
		$consulta="SELECT *FROM 9134_recursosVSservicio WHERE idServicio=".$idServicio;
		$res=$con->obtenerFilas($consulta);
		$nfilas=$con->filasAfectadas;
		
		$conNser="SELECT nombreServicio FROM 9132_servicios WHERE idServicio=".$idServicio;
		$nSer=$con->obtenerValor($conNser);
		
		$arrDatos="";
		while($filaM=mysql_fetch_row($res))
		{
			$conPuesto="SELECT puesto FROM 819_puestosOrganigrama WHERE idPuesto=".$filaM[2];
			$puesto=$con->obtenerValor($conPuesto);
			
			$conUnidad="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$filaM[3]."'";
			$nombreU=$con->obtenerValor($conUnidad);
			
			$obj='{"idRecursoVSServicio":"'.$filaM[0].'","nombreP":"['.$nombreU.']'.$puesto.'","costoHora":"'.$filaM[4].'","minutos":"'.$filaM[5].'","unidades":"'.$filaM[6].'","costoTotal":"'.$filaM[7].'","nServicio":"'.$nSer.'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		
		if($SO==2)
			$obj='{"numReg":"'.$nfilas.'","registros":['.($arrDatos).']}';
		else
			$obj='{"numReg":"'.$nfilas.'","registros":['.($arrDatos).']}';
		echo $obj;
	
	}
	
	function eliminarDepartamentoServicio()
	{
		global $con;
		$idServicio=$_POST["idServicio"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			
			for($x=0;$x<$tamano;$x++)
			{
				$query[$ct]="DELETE FROM 9134_recursosVSservicio WHERE codigoUnidad='".$arreglo[$x]."' AND idServicio=".$idServicio;
				$ct++;
				$query[$ct]="DELETE FROM 9133_departamentoVSServicio WHERE codigoUnidad='".$arreglo[$x]."' AND idServicio=".$idServicio;
				$ct++;
			}
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "1";
		}
	}
	
	function guardarDepartamentoServicio()
	{
		global $con;
		$idServicio=$_POST["idServicio"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
		  	{
			  $query[$ct]="INSERT INTO 9133_departamentoVSServicio(codigoUnidad,idServicio) VALUES(".$arreglo[$x].",'".$idServicio."')";
			  $ct++;
		  	}
		
		  	$query[$ct]="commit";
		  	if($con->ejecutarBloque($query))
			  	echo "1|";
			else
				echo "|";
		}
	}
	
	function obtenerPuestosDepartamento()
	{
		global $con;
		$cadena=$_POST["cadena"];
		
		$consulta="SELECT DISTINCT(u.idPuesto),puesto,codUnidad,unidad FROM  653_unidadesOrgVSPuestos u, 819_puestosOrganigrama p, 817_organigrama o 
					WHERE codUnidad IN (".$cadena.") AND u.idPuesto=p.idPuesto AND codUnidad=o.codigoUnidad ORDER BY puesto";
		$res=$con->obtenerFilasArreglo($consulta);
		
		echo "1|".uEJ($res);
		
	}
	
	function guardarRecursoServicio()
	{
		global $con;
		$codUnidad=$_POST["codigoUnidad"];
		$idPuesto=$_POST["idPuesto"];
		$unidades=$_POST["unidades"];
		$minutos=$_POST["minutos"];
		$idServicio=$_POST["idServicio"];
		
		$conNser="SELECT nombreServicio FROM 9132_servicios WHERE idServicio=".$idServicio;
		$nSer=$con->obtenerValor($conNser);
				
		
		$consulta="SELECT sueldoMinimo,sueldoMaximo,horasPuesto FROM 819_puestosOrganigrama WHERE idPuesto=".$idPuesto;
		$fila=$con->obtenerPrimeraFila($consulta);
		
		if($fila[0]!=0 && $fila[1]!=0)
		{
			
			$promedio=($fila[0]+$fila[1])/2;
			
			$promSemana=$promedio/4;
			$costoHora=$promSemana/$fila[2];
			//$costoHora=number_format($costoHora,2,'.',',');
			
			$horas=$minutos/60;
			$totalUnit=$costoHora*$horas;
			
			$costoTotal=$totalUnit*$unidades;
			
			$costoTotalF=number_format($costoTotal,2,'.',',');
			
			//echo $costoTotalF ;
			
			$query="INSERT INTO 9134_recursosVSservicio (idServicio,idPuesto,codigoUnidad,costoHora,minutos,unidades,costoTotal)
					VALUES('".$idServicio."','".$idPuesto."','".$codUnidad."','".$totalUnit."','".$minutos."','".$unidades."','".$costoTotalF."')";
			
			if($con->ejecutarconsulta($query))
			{
				$conPuesto="SELECT puesto FROM 819_puestosOrganigrama WHERE idPuesto=".$idPuesto;
				$puesto=$con->obtenerValor($conPuesto);
				
				$conUnidad="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$codUnidad."'";
				$nombreU=$con->obtenerValor($conUnidad);
				
				$nombrePuesto="[".$nombreU."] ".$puesto;
				
				$idRecursoVSServicio=$con->obtenerUltimoID();
				echo "1|".$idRecursoVSServicio."|".$nombrePuesto."|".$totalUnit."|".$minutos."|".$unidades."|".$costoTotalF."|".$nSer;
			}
			else
			{
				echo "|";
			}
		}
	
	}
	
	function eliminarRecursoServicio()
	{
		global $con;
		
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
		  	{
			  $query[$ct]="DELETE FROM 9134_recursosVSservicio WHERE idRecursoVSServicio=".$arreglo[$x];
			  $ct++;
		  	}
		
		  	$query[$ct]="commit";
		  	if($con->ejecutarBloque($query))
			  	echo "1|";
			else
				echo "|";
		}
	}
	
	function eliminarServicio()
	{
		global $con;
		$idServicio=base64_decode($_POST["idServicio"]);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			$query[$ct]="DELETE FROM 9134_recursosVSservicio WHERE idServicio=".$idServicio;
			$ct++;
			$query[$ct]="DELETE FROM 9132_servicios WHERE idServicio=".$idServicio;
			$ct++;
			$query[$ct]="DELETE FROM 9133_departamentoVSServicio WHERE idServicio=".$idServicio;
			$ct++;
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	
	function obtenerListadoSolicitudesPresupuesto() //Pendiente objeto gasto
	{
		global $con;
		global $con;
		global $SO;
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		$idCiclo=bD($_POST["ciclo"]);
		$codDepartamento=bD($_POST["codigoUnidad"]);
		$idPrograma=bD($_POST["idPrograma"]);
		$capitulos=bD($_POST["capitulos"]);
		$idProceso=$_POST["idProceso"];
		$actor=$_POST["actor"];
		$etapa=$_POST["numEtapa"];
		$idActorProcesoEtapa=$_POST["idActorProcesoEtapa"];
		$arrCapitulos=explode(",",$capitulos);
		$cadCapitulos="";
		global $tipoOG;
		$consulta="select numEtapa from 4037_etapas where idProceso=".$idProceso." order by numEtapa";
		$resEtapas=$con->obtenerFilas($consulta);
		$arrPermisosEtapa=array();
		$arrEtapasOpciones="";
		$permisos="MEGB";
		/*while($filaEtapa=mysql_fetch_row($resEtapas))
		{
			$permisos="ME";
			/*$consulta="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE actor='".$actor."' AND idProceso=".$idProceso." AND tipoActor=1 and numEtapa=".$filaEtapa[0];
			$idAProcesoEtapa=$con->obtenerValor($consulta);	   
			if($idAProcesoEtapa=="")
				$idAProcesoEtapa="-1";
			$consulta="SELECT idGrupoAccion,complementario,idAccionesProcesoEtapaVSAcciones FROM 947_actoresProcesosEtapasVSAcciones WHERE idActorProcesoEtapa=".$idAProcesoEtapa;
			$arrAcciones=$con->obtenerFilasArregloPHP($consulta);
			$permisos="";
			if(existeValorMatriz($arrAcciones,"13")!=-1)
			{
				$permisos="['A','']";
			}
			if(existeValorMatriz($arrAcciones,"2")!=-1)
			{
				if($permisos=="")
					$permisos="['M','']";
				else
					$permisos.=",['M','']";
			}
			if(existeValorMatriz($arrAcciones,"7")!=-1)
			{
				if($permisos=="")
					$permisos.="['E','']";
				else
					$permisos.=",['E','']";
			}
			if(existeValorMatriz($arrAcciones,"6")!=-1)
			{
				if($permisos=="")
					$permisos="['B','']";	
				else
					$permisos.=",['B','']";	
			}
			$pos=existeValorMatriz($arrAcciones,"11");
			if($pos!=-1)
			{
				if($permisos=="")
					$permisos="['D','']";	
				else
					$permisos.=",['D','']";	
				$idAccionProceso=$arrAcciones[$pos][2];
				
				$consulta="SELECT valor,contenido,etapa FROM 9114_opcionesEvaluacion WHERE idAccion=".$idAccionProceso." AND idIdioma=".$_SESSION["leng"];
				$arrOpciones=$con->obtenerFilasArreglo($consulta);
				
				$obj="['".$filaEtapa[0]."',".$arrOpciones."]";
				if($arrEtapasOpciones=="")
					$arrEtapasOpciones=$obj;
				else
					$arrEtapasOpciones.=",".$obj;
			}*/
			/*$pos=existeValorMatriz($arrAcciones,"1");
			if($pos!=-1)
			{
				$etapaSomete=$arrAcciones[$pos][1];
				if($permisos=="")
					$permisos="['S','".$arrAcciones[$pos][1]."']";
				else
					$permisos.=",['S','".$arrAcciones[$pos][1]."']";
			}	   
			
			if(existeValorMatriz($arrAcciones,"14")!=-1)
			{
				if($permisos=="")
					$permisos="['G','']";	
				else
					$permisos.=",['G','']";	
			}
			$arrPermisosEtapa[removerCerosDerecha($filaEtapa[0])]="[".$permisos."]";
		}*/
		
		foreach($arrCapitulos as $c)
		{
			$cFin=substr($c,0,1);
			if($tipoOG==1)
				$cFin=str_pad($cFin,strlen($c),"9",STR_PAD_RIGHT);
			else
			{
				$cFin=str_pad($cFin,strlen($c)+1,"9",STR_PAD_RIGHT);
				$c.="0";
			}
			$particula="(c.clave>=".$c." and c.clave<=".$cFin.")";	
			if($cadCapitulos=="")
				$cadCapitulos=$particula;	
			else
				$cadCapitulos.=" or ".$particula;	
		}
		if($cadCapitulos!="")
			$cadCapitulos="(".$cadCapitulos.")";
		$filtroUsuario="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$filtroUsuario.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
					case 'list':
						if($arrFiltro[$x]["field"]=='nombreObj')
						{
							$listaClaves=$arrFiltro[$x]["data"]["value"];
							$filtroUsuario.=" and o.clave in (".$listaClaves.")";
						}
						
					break;
				}
			}
		}
		$condWhere="";
		if($filtroUsuario!="")
			$condWhere=$filtroUsuario;
		$condFiltro="";
		$consulta="SELECT idGrupoAccion,complementario FROM 947_actoresProcesosEtapasVSAcciones WHERE idGrupoAccion=11 and  idActorProcesoEtapa=".$idActorProcesoEtapa;
		/*$fila=$con->obtenerPrimeraFila($consulta);
		if($fila)
		{
			$complementario=$fila[1];
			switch($complementario)
			{
				case 1:
					$condFiltro="";
				break;	
				case 2:
					$condFiltro=" and codInstitucion='".$_SESSION["codigoInstitucion"]."'";
				break;	
				case 3:
					$condFiltro=" and idResponsable=".$_SESSION["idUsr"];
				break;	
				case 4:
					$condFiltro=" AND codDepto='".$codDepartamento."'";
				break;	
				case 5:
					$condFiltro=" AND codDepto like '".$codDepartamento."%'";
				break;	
				case 6:
					$condFiltro=" and idPrograma=".$idPrograma;
				break;	
				case 7:
					$condFiltro=" AND codDepto='".$codDepartamento."' and idPrograma=".$idPrograma;
				break;
			}
		}
		else
		{
			$consulta="SELECT idAccion,complementario FROM 949_actoresVSAccionesProceso WHERE idAccion=9 and  actor='".$actor."' and idProceso=".$idProceso;
			$fila=$con->obtenerPrimeraFila($consulta);
			if($fila)
			{
				$complementario=$fila[1];
				switch($complementario)
				{
					case 1:
						$condFiltro="";
					break;	
					case 2:
						$condFiltro=" and codInstitucion='".$_SESSION["codigoInstitucion"]."'";
					break;	
					case 3:
						$condFiltro=" and idResponsable=".$_SESSION["idUsr"];
					break;	
					case 4:
						$condFiltro=" AND codDepto='".$codDepartamento."'";
					break;	
					case 5:
						$condFiltro=" AND codDepto like '".$codDepartamento."%'";
					break;	
					case 6:
						$condFiltro=" and idPrograma=".$idPrograma;
					break;	
					case 7:
						$condFiltro=" AND codDepto='".$codDepartamento."' and idPrograma=".$idPrograma;
					break;
				}
			}
		}*/
		
		$consulta="SELECT idCodigoGastoCiclo,c.clave,CONCAT('[',c.clave,']',nombreObjetoGasto) AS nombreObj,idCiclo,p.nombreProducto,
					justificacion,c.cantidad,c.observaciones,costoUnitario as costo,p.descripcion, org.unidad,montoTotal,c.modificable,
					(SELECT nombreEtapa FROM 4037_etapas WHERE idProceso=".$idProceso." AND numEtapa=c.numEtapa) as etapa,
					(SELECT tituloPrograma FROM 517_programas WHERE idPrograma=c.idPrograma) as programa,
					(select Nombre from 800_usuarios where idUsuario=c.idResponsable) as responsableSolicitud,
					(select tituloTipoP from 508_tiposPresupuesto where idTipoPresupuesto=c.tipoPresupuesto) as tipoPresupuesto,version,c.numEtapa
			   FROM 9110_objetosGastoVSCiclo c,507_objetosGasto o,9101_CatalogoProducto p,817_organigrama org 
			   WHERE idCiclo=".$idCiclo.$condFiltro." AND org.codigoUnidad=c.codDepto  AND c.clave=o.clave AND p.idProducto=c.idProducto ".$condWhere." order by nombreProducto limit ".$inicio.",".$cantidad;
		$res=$con->obtenerFilas($consulta);
		$consulta="SELECT count(idCodigoGastoCiclo)
			   FROM 9110_objetosGastoVSCiclo c
			   WHERE idCiclo=".$idCiclo.$condFiltro."  AND 
			   ".$cadCapitulos." ".$condWhere;
		   
		$numRegistros=$con->obtenerValor($consulta);
		$arrDatos='';
		while($fila=mysql_fetch_row($res))
		{
			$conCantidades="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$fila[0]." order by mes";
			$filas=$con->obtenerFilas($conCantidades);
			$nFilas=$con->filasAfectadas;
			$cadenaMeses="";
			if($nFilas>0)
			{
				$cadenaAux="<table>";
				$fCabecera="<tr>";
				$fDatos="<tr>";
				
				while($fMeses=mysql_fetch_row($filas))
				{
					
					$fCabecera.='<td width="60" align="center"><span class="corpo8_bold">'.obtenerAbreviaturaMes($fMeses[0]).'</span></td>';
					$fDatos.='<td align="center">'.$fMeses[1].'</td>';
				}
			}
			$fCabecera.="</tr>";
			$fDatos.="</tr>";
			$cadenaAux.=$fCabecera.'<tr height="2"><td colspan="12" style="background:#600"></td></tr>'.$fDatos;
			$cadenaAux.="</table>";
			
			$obj='{"idCodigoGastoCiclo":"'.$fila[0].'","clave":"'.cv($fila[1]).'","nombreObj":"'.cv($fila[2]).'","idCiclo":"'.cv($fila[3]).'","nombreProducto":"'.cv($fila[4]).'","justificacion":"'.
					cv(str_replace("#R","",$fila[5])).'","cantidad":"'.cv($fila[6]).'","cadenaMeses":"'.cv($cadenaAux).'","observaciones":"'.cv(str_replace("#R","",$fila[7])).'","costoUnitario":"'.cv($fila[8]).
					'","costoTotal":"'.($fila[11]).'","descripcion":"'.cv($fila[9]).'","depto":"'.cv($fila[10]).'","modificable":"'.$fila[12].'","etapa":"'.$fila[13].
					'","programa":"'.$fila[14].'","responsableSolicitud":"'.$fila[15].'","tipoPresupuesto":"'.$fila[16].'","version":"'.$fila[17].'","permisos":"'.$permisos.'","numEtapa":"'.$fila[18].'"}';
			if($arrDatos=="")
				$arrDatos=$obj;
			else
				$arrDatos.=",".$obj;
		}
		$sqlQuery=bE("SELECT idCodigoGastoCiclo  FROM 9110_objetosGastoVSCiclo c  WHERE idCiclo=".$idCiclo.$condFiltro."  AND ".$cadCapitulos." ".$condWhere);
		if($SO==2)
			$obj='{"arrEtapasOpciones":"['.$arrEtapasOpciones.']","sqlQuery":"'.$sqlQuery.'","numReg":"'.$numRegistros.'","registros":['.($arrDatos).']}';
		else
			$obj='{"arrEtapasOpciones":"['.$arrEtapasOpciones.']","sqlQuery":"'.$sqlQuery.'","numReg":"'.$numRegistros.'","registros":['.($arrDatos).']}';
		echo $obj;
	}
	
	function guardarCostoServicio()
	{
		global $con;
		$idServicio=$_POST["idServicio"];
		$costoTotal=$_POST["costoT"];
		
		$query="UPDATE 9132_servicios SET  costo='".$costoTotal."' WHERE idServicio=".$idServicio;
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
	}
	
	
	function obtenerCandidatosSolicitud()
	{
		global $con;
		$idSolicitud=$_POST["idSolicitud"];
		
		$consulta="SELECT c.idUsuario,Nombre FROM 802_identifica i, 4231_candidatosSolicitud  c WHERE i.idUsuario=c.idUsuario AND idSolicitud=".$idSolicitud." AND estado=0";
		$arreglo=$con->obtenerFilasArreglo($consulta);
		
		echo "1|".uEJ($arreglo);
	}
	
	function marcarCandidatosSolicitud()
	{
		global $con;
		$cadena=$_POST["cadena"];
		$idSolicitud=$_POST["idSolicitud"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
		  	{
			  $query[$ct]="UPDATE 4231_candidatosSolicitud SET estado=1 WHERE idSolicitud=".$idSolicitud." AND idUsuario=".$arreglo[$x];
			  $ct++;
		  	}
		
		  	$query[$ct]="commit";
		  	if($con->ejecutarBloque($query))
			  	echo "1|";
			else
				echo "|";
		}
	}
	
	function obtenerCandidatosSeleccionados()
	{
		global $con;
		$idFormulario=$_POST["idFormulario"];
		$idSolicitud=$_POST["idSolicitud"];
		
		$consulta="SELECT c.idUsuario,Nombre FROM 802_identifica i, 4231_candidatosSolicitud  c WHERE i.idUsuario=c.idUsuario AND idFormulario=".$idFormulario." and  idSolicitud=".$idSolicitud." AND estado=1";
		$arreglo=$con->obtenerFilasArreglo($consulta);
		
		echo "1|".uEJ($arreglo);
	}

	
	function obtenerPartidasComiteDisponible()
	{
		global $con;
		global $tipoOG;
		global $SO;
		$idProceso=$_POST["idProceso"];
		$arrComite=explode("_",$_POST["idComite"]);
		$idComite=$arrComite[1];
		
		$consulta="SELECT capitulo FROM 9044_capitulosProcesoPresupuesto WHERE idProcesoPresupuesto=".$idProceso;
		$resCap=$con->obtenerFilas($consulta);	
		$cadCapitulos="";
		while($filaCap=mysql_fetch_row($resCap))
		{
			$c=$filaCap[0];
			$consulta="select codigoControl from 507_objetosGasto where clave=".$c;
			$cControl=$con->obtenerValor($consulta);
			$particula="(o.codigoControlPadre like '".$cControl."%')";
			if($cadCapitulos=="")
				$cadCapitulos=$particula;	
			else
				$cadCapitulos.=" or ".$particula;	
		}	
		if($cadCapitulos!="")
			$cadCapitulos=" and (".$cadCapitulos.")";
		$consulta="select partida from 9044_proyectoComitePartida where idProyectoComite=".$idComite;
		$listPartidas=$con->obtenerListaValores($consulta);
		if($listPartidas=="")
			$listPartidas="-1";
		$consulta="SELECT clave as partida,nombreObjetoGasto as descripcion FROM 507_objetosGasto o WHERE concat(clave) like '%00' and  concat(clave) not like '%000' ".$cadCapitulos." and clave not in (".$listPartidas.")";
		$arrPartidas=$con->obtenerFilasJson($consulta);
		if($SO==1)
			echo '{"registros":'.utf8_encode($arrPartidas).'}';
		else
			echo '{"registros":'.$arrPartidas.'}';
		
	}
	
	
	function guardarPartidaComite()
	{
		global $con;
		$arrComite=explode("_",$_POST["iC"]);	
		$listPartida=$_POST["listPartida"];
		$idComite=$arrComite[1];
		$arrPartidas=explode(",",$listPartida);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		foreach($arrPartidas as $p)
		{
			$consulta[$x]="insert into 9044_proyectoComitePartida(idProyectoComite,partida) values(".$idComite.",".$p.")";
			$x++;	
		}
		
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
	function modificarVacanteSeleccionados()
	{
		global $con;
		$idSolicitud=$_POST["idSolicitud"];
		$idUsuario=$_POST["idUsuario"];
		$fecha=date('Y-m-d');
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			$query[$ct]="UPDATE 667_puestosVacantes SET STATUS=0,fechaCambio='".$fecha."' WHERE idRegistroPerfil=".$idSolicitud;
			$ct++;
			$query[$ct]="UPDATE 4231_candidatosSolicitud SET estado=3 WHERE idSolicitud=".$idSolicitud." AND idUsuario=".$idUsuario;
			$ct++;
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function enviarContrato()
	{
		global $con;
		
		$cadena=$_POST["cadena"];
		$idSolicitud=$_POST["idSolicitud"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
		  	{
			  $query[$ct]="UPDATE 4231_candidatosSolicitud SET estado=2 WHERE idSolicitud=".$idSolicitud." AND idUsuario=".$arreglo[$x];
			  $ct++;
		  	}
		
		  	$query[$ct]="commit";
		  	if($con->ejecutarBloque($query))
			  	echo "1|";
			else
				echo "|";
		}
	
	}
	
	function verElegidos()
	{
		global $con;
		$idSolicitud=$_POST["idSolicitud"];
		
		$consulta="SELECT c.idUsuario,Nombre FROM 802_identifica i, 4231_candidatosSolicitud  c WHERE i.idUsuario=c.idUsuario AND idSolicitud=".$idSolicitud." AND estado=2";
		$arreglo=$con->obtenerFilasArreglo($consulta);
		
		echo "1|".uEJ($arreglo);
	
	}


	function generarPresupuestoAutorizado()
	{
		global $con;	
		$ciclo=$_POST["ciclo"];
		$idProceso=$_POST["idProceso"];
		$listadoConceptos=$_POST["listadoConceptos"];
		$arrListadoConceptos=explode(",",$listadoConceptos);
		if($listadoConceptos=="")
			$listadoConceptos="-1";
		$tPresupuestal=$_POST["tP"];
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="insert into 527_aprobacionesProcesoPresupuesto(idProceso,ciclo,fechaAprobacion,responsableAprobacion) values(".$idProceso.",".$ciclo.",'".date("Y-m-d")."',".$_SESSION["idUsr"].")";
		$x++;
		$consulta[$x]="update 9110_objetosGastoVSCiclo set numEtapa=3 where idCodigoGastoCiclo in (".$listadoConceptos.")";
		
		$x++;

		$arrConceptos=array();
		$arrProductosAutorizados=array();
		if(sizeof($arrListadoConceptos)>0)
		{
			foreach($arrListadoConceptos as $c)
			{
				
				$query="SELECT idProducto,cantidad,costoUnitario FROM 9110_objetosGastoVSCiclo WHERE idCodigoGastoCiclo=".$c;
	
				$fProducto=$con->obtenerPrimeraFila($query);
				$idProducto=$fProducto[0];
				$cantidad=$fProducto[1];
				$costoUnitario=$fProducto[2];
				
				if(!isset($arrProductosAutorizados[$idProducto]))
				{
					$arrProductosAutorizados[$idProducto]["total"]=$cantidad;
					$arrProductosAutorizados[$idProducto]["costoUnitario"]=$costoUnitario;
					$arrProductosAutorizados[$idProducto]["desgloce"]=array();
					for($nCt=0;$nCt<12;$nCt++)
					{
						$arrProductosAutorizados[$idProducto]["desgloce"][$nCt]=0;
					}
				}
				else
					$arrProductosAutorizados[$idProducto]["total"]+=$cantidad;
				
				$consulta[$x]="insert into 525_productosAutorizados select * from 9110_objetosGastoVSCiclo where idCodigoGastoCiclo=".$c;	
				$x++;
				$consulta[$x]="insert into 526_distribucionAutorizada select * from 9111_cantidaObjVSMes where idCodigoGastoCiclo=".$c;	
				$x++;
				$query="select * from 9110_objetosGastoVSCiclo where idCodigoGastoCiclo=".$c;	
				$fila=$con->obtenerPrimeraFila($query);
				$coordenada=$fila[10]."_".$fila[3]."_".$fila[1]."_".$fila[19];  
				if(!isset($arrConceptos[$coordenada]))
				{
					$arrConceptos[$coordenada]["monto"]=$fila[16];	
					$arrConceptos[$coordenada]["institucion"]=$fila[4];
					$query="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$c;
					$resDist=$con->obtenerFilas($query);
					$arrDist=array();
					while($fDist=mysql_fetch_row($resDist))
					{
						$arrDist[$fDist[0]]=$fDist[1]*$fila[15];
						$arrProductosAutorizados[$idProducto]["desgloce"][$fDist[0]]+=$fDist[1];
					}
					$arrConceptos[$coordenada]["distibucion"]=$arrDist;
				}
				else
				{
					$arrConceptos[$coordenada]["monto"]+=$fila[16];	
					$query="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$c;
					$resDist=$con->obtenerFilas($query);
					$arrDist=array();
					while($fDist=mysql_fetch_row($resDist))
					{
						$arrConceptos[$coordenada]["distibucion"][$fDist[0]]+=$fDist[1]*$fila[15];
						$arrProductosAutorizados[$idProducto]["desgloce"][$fDist[0]]+=$fDist[1];
					}
				}
			}
		}
		if(sizeof($arrConceptos)>0)
		{
			foreach($arrConceptos as $concepto=>$resto)
			{
				$datosConcepto=explode("_",$concepto);
				$query="select codigoControl from 507_objetosGasto where clave=".$datosConcepto[2];
				$cControl=$con->obtenerValor($query);
				$codCapitulo=substr($cControl,0,3);
				$query="select clave from 507_objetosGasto where codigoControl='".$codCapitulo."'";
				$capitulo=$con->obtenerValor($query);
				$consulta[$x]="INSERT INTO 523_presupuestoAutorizado(ciclo,programa,depto,capitulo,partida,montoTotal,montoAjustado,tipoPresupuesto,institucion)
								VALUES(".$ciclo.",".$datosConcepto[0].",'".$datosConcepto[1]."',".$capitulo.",".$datosConcepto[2].",".$resto["monto"].",".$resto["monto"].",".$datosConcepto[3].",'".$resto["institucion"]."')";
				$x++;
				$consulta[$x]="set @idRegistro=(select LAST_INSERT_ID())";
				$x++;
				foreach($resto["distibucion"] as $nDist=>$d)
				{
					$consulta[$x]="INSERT INTO 523_distribucionPresupuestoAutorizado(idPresupuestoAutorizado,nDistribucion,monto,montoAjustado)
									VALUES(@idRegistro,".$nDist.",".$d.",".$d.")";
					$x++;
					$consulta[$x]="INSERT INTO 528_asientosCuentasPresupuestales(ciclo,programa,departamento,capitulo,partida,mes,monto,fechaOperacion,responsableOperacion,tiempoPresupuestal,operacion,idReferencia,idProceso,complementario)
								VALUES(".$ciclo.",".$datosConcepto[0].",'".$datosConcepto[1]."',".$capitulo.",".$datosConcepto[2].",".$nDist.",".$d.",'".date("Y-m-d")."',".$_SESSION["idUsr"].",".$tPresupuestal.",1,0,0,'')";
					$x++;
				}
			}
		}
		if(sizeof($arrProductosAutorizados)>0)
		{
			foreach($arrProductosAutorizados as $idProducto=>$resto)
			{
				$consulta[$x]="insert into 527_concentradoProducto(idProducto,cantidad,costoUnitario,total,estado,ciclo)
								VALUES(".$idProducto.",".$resto["total"].",".$resto["costoUnitario"].",".($resto["costoUnitario"]*$resto["total"]).",0,".$ciclo.")";	
				$x++;
				$consulta[$x]="set @idRegistro=(select LAST_INSERT_ID())";
				$x++;
				foreach($resto["desgloce"] as $d=>$cantidad)
				{
					$consulta[$x]="INSERT INTO 528_distribucionConcentrado(idCompraVSProducto,mes,cantidad) values(@idRegistro,".$d.",".$cantidad.")";	
					$x++;
				}
			}
		}
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
	function removerDefinicionEstruturaProg()
	{
		global $con;
		$ciclo=$_POST["ciclo"];
		$institucion=$_POST["institucion"];
		$x=0;
		$consulta[$x]="begin";		
		$x++;
		$consulta[$x]="delete from 9117_definicionEstructuraProgramatica where ciclo='".$ciclo."' and codigoInstitucion='".$institucion."'";		
		$x++;
		$consulta[$x]="delete from 9117_estructuraPAT where ciclo='".$ciclo."' and codigoInstitucion='".$institucion."'";	
		$x++;
		$consulta[$x]="commit";		
		$x++;
		eB($consulta);
		
	}
	
	function crearNuevaDefinicionEstructura()
	{
		global $con;
		$ciclo=$_POST["ciclo"];
		$institucion=$_POST["institucion"];
		$consulta="insert into 9117_definicionEstructuraProgramatica(ciclo,codigoInstitucion) values(".$ciclo.",'".$institucion."')";
		if($con->ejecutarConsulta($consulta))
		{
			$consulta="SELECT ciclo,situacion FROM 9117_definicionEstructuraProgramatica WHERE codigoInstitucion='".$_SESSION["codigoInstitucion"]."' ORDER BY ciclo";
			$arrDefinicion=$con->obtenerFilasArreglo($consulta);	
			echo "1|".$arrDefinicion;
		}
	
	}
	
	function obtenerCicloDisponibles()
	{
		global $con;
		$institucion=$_POST["institucion"];
		$consulta="SELECT ciclo FROM 9117_definicionEstructuraProgramatica WHERE codigoInstitucion='".$_SESSION["codigoInstitucion"]."' ORDER BY ciclo";
		$listCiclo=$con->obtenerListaValores($consulta);
		if($listCiclo=="")
			$listCiclo="-1";
		$consulta="select ciclo,ciclo FROM 550_cicloFiscal WHERE codigoInstitucion='".$institucion."' AND ciclo NOT IN (".$listCiclo.")";	
		$arrCiclos=$con->obtenerFilasArreglo($consulta);
		echo "1|".$arrCiclos;
		
	}
	
	function modificarSituacionEstructura()
	{
		global $con;
		$ciclo=$_POST["ciclo"];
		$institucion=$_POST["institucion"];
		$valor=$_POST["valor"];
		$consulta="update 9117_definicionEstructuraProgramatica set situacion=".$valor." where ciclo=".$ciclo." and codigoInstitucion='".$institucion."'";
		eC($consulta);
			
	}
	
	function obtenerProgramasPlaneacionDisp()
	{
		
		global $con;
		$ciclo=$_POST["ciclo"];
		$institucion=$_POST["institucion"];
		$partida=$_POST["partida"];
		$consulta="SELECT idProgramaInstitucional FROM 9117_estructurasVSPrograma WHERE ciclo=".$ciclo." AND institucion='".$institucion."'";
		$listProg=$con->obtenerListaValores($consulta);
		if($listProg=="")
			$listProg="-1";
		$consulta="SELECT idPrograma ,tituloPrograma FROM 517_programas WHERE codigoInstitucion='".$_SESSION["codigoInstitucion"]."' AND idPrograma NOT IN(".$listProg.") order by tituloPrograma";
		$arrProgramas=$con->obtenerFilasArreglo($consulta);
		echo "1|".$arrProgramas;

	}	
	
	function guardarProgramasPlaneacion()
	{
		global $con;	
		$listaProgramas=$_POST["listaProgramas"];
		$ciclo=$_POST["ciclo"];
		$institucion=$_POST["institucion"];
		$partida=$_POST["partida"];
		$arrProgramas=explode(",",$listaProgramas);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		foreach($arrProgramas as $programa)
		{
			$consulta[$x]="insert into 9117_estructurasVSPrograma(idPartidaPresupuestal,idProgramaInstitucional,ciclo,institucion) values('".$partida."',".$programa.",".$ciclo.",'".$institucion."')";
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		if($con->ejecutarBloque($consulta))
		{
			
			$query="SELECT idEstructura,grupoFuncional,funcion,subFuncion,programaGasto,actividadInstitucional,partidaPresupuestal FROM 9117_estructuraPAT where ciclo=".$ciclo." and codigoInstitucion='".$institucion."'";
			$arrRegistros="";
			$resRegistros=$con->obtenerFilas($query);
			while($fila=mysql_fetch_row($resRegistros))
			{
				$query="SELECT idProgramaInstitucional FROM 9117_estructurasVSPrograma WHERE idPartidaPresupuestal='".$fila[6]."' AND ciclo=".$ciclo." AND institucion='".$institucion."'";
				$listProg=$con->obtenerListaValores($query);
				if($listProg=="")
					$listProg="-1";
				$query="SELECT idPrograma,tituloPrograma FROM 517_programas WHERE idPrograma IN(".$listProg.")";
				$tblProg='<table><tr><td colspan="3"><span class="letraRojaSubrayada8">Programas asociados:</span></td></tr>';
				
				$resProg=$con->obtenerFilas($query);
				while($filaProg=mysql_fetch_row($resProg))
				{
					$tblProg.='<tr height="21"><td width="55"><a href="javascript:removerPrograma(\\\''.bE($filaProg[0]).'\\\',\\\''.bE($fila[6]).'\\\')"><img src="../images/delete.png" alt="Remover programa" title="Remover programa"></a>&nbsp;<img src="../images/bullet_green.png">&nbsp;&nbsp;</td><td width="600">'.$filaProg[1].'</td><td>&nbsp;&nbsp;</td></tr>'	;
				}
				$tblProg.='</table>';
				$obj="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$fila[3]."','".$fila[4]."','".$fila[5]."','".$fila[6]."','".$tblProg."']";
				if($arrRegistros=="")
					$arrRegistros=$obj;
				else
					$arrRegistros.=",".$obj;
				
			}
			$arrRegistros="[".$arrRegistros."]";	
			echo "1|".$arrRegistros;	
		}
		
	}
	
	function removerProgramaPlaneacion()
	{
		global $con;
		$ciclo=$_POST["ciclo"]	;
		$institucion=$_POST["institucion"];
		$idPrograma=$_POST["idPrograma"];
		$idPartida=$_POST["idPartida"];
		$consulta="delete from 9117_estructurasVSPrograma where idPartidaPresupuestal='".$idPartida."' and idProgramaInstitucional=".$idPrograma." and ciclo=".$ciclo." and institucion='".$institucion."'";

		if($con->ejecutarConsulta($consulta))
		{
			$consulta="SELECT idEstructura,grupoFuncional,funcion,subFuncion,programaGasto,actividadInstitucional,partidaPresupuestal FROM 9117_estructuraPAT where ciclo=".$ciclo." and codigoInstitucion='".$institucion."'";
			$arrRegistros="";
			$resRegistros=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($resRegistros))
			{
				$consulta="SELECT idProgramaInstitucional FROM 9117_estructurasVSPrograma WHERE idPartidaPresupuestal='".$fila[6]."' AND ciclo=".$ciclo." AND institucion='".$institucion."'";
				$listProg=$con->obtenerListaValores($consulta);
				if($listProg=="")
					$listProg="-1";
				$consulta="SELECT idPrograma,tituloPrograma FROM 517_programas WHERE idPrograma IN(".$listProg.")";
				$tblProg='<table><tr><td colspan="3"><span class="letraRojaSubrayada8">Programas asociados:</span></td></tr>';
				
				$resProg=$con->obtenerFilas($consulta);
				while($filaProg=mysql_fetch_row($resProg))
				{
					$tblProg.='<tr height="21"><td width="55"><a href="javascript:removerPrograma(\\\''.bE($filaProg[0]).'\\\',\\\''.bE($fila[6]).'\\\')"><img src="../images/delete.png" alt="Remover programa" title="Remover programa"></a>&nbsp;<img src="../images/bullet_green.png">&nbsp;&nbsp;</td><td width="600">'.$filaProg[1].'</td><td>&nbsp;&nbsp;</td></tr>'	;
				}
				$tblProg.='</table>';
				$obj="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$fila[3]."','".$fila[4]."','".$fila[5]."','".$fila[6]."','".$tblProg."']";
				if($arrRegistros=="")
					$arrRegistros=$obj;
				else
					$arrRegistros.=",".$obj;
				
			}
			$arrRegistros="[".$arrRegistros."]";	
			echo "1|".$arrRegistros;
		}
	}
	
	function obtenerPresupuesto1000()
	{
		global $con;
		$ciclo=	bD($_POST["ciclo"]);
		$programas=bD($_POST["programas"]);
		$idProceso=$_POST["idProceso"];
		$query="SELECT partidas FROM 9044_capitulosProcesoPresupuesto WHERE idProcesoPresupuesto=".$idProceso." AND (capitulo=1000 or capitulo=3000)";
		$partidas=$con->obtenerValor($query);
		$query="select clave from 507_objetosGasto where codigoControl in (".$partidas.")";
		$listClaves=$con->obtenerListaValores($query);
		$arrProgramas=explode(",",$programas);
		$arrPartidas=explode(",",$partidas);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		if($programas!="0")
		{
			foreach($arrProgramas as $p)
			{
				if(sizeof($arrPartidas)>0)
				{
					foreach($arrPartidas as $pa)
					{
						$query="select clave from 507_objetosGasto where codigoControl=".$pa."";
						$clave=$con->obtenerValor($query);
						$query="SELECT * FROM 9110_objetosGastoVSCiclo WHERE idProducto=-1 and idCiclo=".$ciclo." AND idPrograma=".$p." AND clave=".$clave;	
						$fila=$con->obtenerPrimeraFila($query);
						if(!$fila)
						{
							$query="insert into 9110_objetosGastoVSCiclo(idProducto,idCiclo,clave,codInstitucion,codDepto,idPrograma,idResponsable,fechaSolicitud,montoTotal,numEtapa,modificable,tipoPresupuesto,version,costoUnitario,cantidad)
										values(-1,".$ciclo.",".$clave.",'".$_SESSION["codigoInstitucion"]."','".$_SESSION["codigoInstitucion"]."',".$p.",".$_SESSION["idUsr"].",'".date("Y-m-d")."',0,1,1,1,1,1,0)";
							
							if(!$con->ejecutarConsulta($query))
								return;
							$idObjetoGasto=$con->obtenerUltimoID();
							for($mes=0;$mes<12;$mes++)
							{
								$consulta[$x]="INSERT INTO 9111_cantidaObjVSMes(idCodigoGastoCiclo,mes,cantidad) VALUES(".$idObjetoGasto.",".$mes.",0)";
								$x++;
							}
						}
					}
				}
			}
			
			$consulta[$x]="commit";
			$x++;
			$con->ejecutarBloque($consulta);
			$query="select idCodigoGastoCiclo,clave,idCiclo,(SELECT nombreObjetoGasto FROM 507_objetosGasto WHERE clave=o.clave) as nombreProducto,montoTotal,(SELECT tituloPrograma FROM 517_programas WHERE idPrograma=o.idPrograma),
			(select Nombre from 800_usuarios where idusuario=o.idResponsable),(SELECT tituloTipoP FROM 508_tiposPresupuesto WHERE idTipoPresupuesto=o.tipoPresupuesto),numEtapa from 9110_objetosGastoVSCiclo o where idPrograma in (".$programas.")
			and idCiclo=".$ciclo." and idProducto=-1 and clave in (".$listClaves.")";
			
		}
		else
		{
			$query="select idCodigoGastoCiclo,clave,idCiclo,(SELECT nombreObjetoGasto FROM 507_objetosGasto WHERE clave=o.clave) as nombreProducto,montoTotal,(SELECT tituloPrograma FROM 517_programas WHERE idPrograma=o.idPrograma),
			(select Nombre from 800_usuarios where idusuario=o.idResponsable),(SELECT tituloTipoP FROM 508_tiposPresupuesto WHERE idTipoPresupuesto=o.tipoPresupuesto),numEtapa from 9110_objetosGastoVSCiclo o where
			idCiclo=".$ciclo." and idProducto=-1 and numEtapa in(2,3) and clave in (".$listClaves.")";
			
		}
		$sql=bE($query);
		$res=$con->obtenerFilas($query);
		$arrObjetos="";
		while($fila=mysql_fetch_row($res))
		{
			$obj='{"idCodigoGastoCiclo":"'.$fila[0].'","clave":"'.$fila[1].'","idCiclo":"'.$fila[2].'","nombreProducto":"'.$fila[3].'","montoTotal":"'.$fila[4].'","programa":"'.$fila[5].'","responsableSolicitud":"'.$fila[6].
			'","tipoPresupuesto":"'.$fila[7].'","numEtapa":"'.$fila[8].'"';
			$query="SELECT mes,cantidad FROM 9111_cantidaObjVSMes WHERE idCodigoGastoCiclo=".$fila[0]." order by mes asc";
			$resMes=$con->obtenerFilas($query);
			while($filaMes=mysql_fetch_row($resMes))
			{
				$obj.=',"mes'.$filaMes[0].'":"'.$filaMes[1].'"';
			}
			$obj.='}';

			if($arrObjetos=="")
				$arrObjetos=$obj;
			else
				$arrObjetos.=",".$obj;
				
		}
		
		echo '{"sql":"'.$sql.'","registros":['.$arrObjetos.']}';
		
	}
	
	
	function guardarRequerimientoMes()
	{
		global $con;
		$idRegistro=$_POST["idRegistro"];
		$mes=$_POST["mes"];
		$valor=$_POST["valor"];
		$x=0;
		$query="begin";
		if(!$con->ejecutarConsulta($query))
			return;
		$query="UPDATE 9111_cantidaObjVSMes SET cantidad=".$valor." WHERE mes=".$mes." AND idCodigoGastoCiclo=".$idRegistro;
		if(!$con->ejecutarConsulta($query))
			return;
		$query="select sum(cantidad) from 9111_cantidaObjVSMes where idCodigoGastoCiclo=".$idRegistro;
		$mTotal=$con->obtenerValor($query);
		$consulta[$x]="UPDATE 9110_objetosGastoVSCiclo set montoTotal=".$mTotal." , cantidad=1 where idCodigoGastoCiclo=".$idRegistro;
		$x++;
		$consulta[$x]="commit";
		$x++;
		if($con->ejecutarBloque($consulta))
		{
			echo "1|".$mTotal;
		}
	}
	
	function someterEvaluacionPartida1000()
	{
		global $con;
		$registros=$_POST["registros"];
		$consulta="UPDATE 9110_objetosGastoVSCiclo SET numEtapa =2 WHERE idCodigoGastoCiclo IN (".$registros.")";
		eC($consulta);
	}
	
	function obtenerDatosContrato()
	{
		global $con;
		$criterio=$_POST["criterio"];
		$consulta="SELECT DISTINCT(idContrato) FROM 562_prorrateoContratos";
		$listContratos=$con->obtenerListaValores($consulta);
		if($listContratos=="")
			$listContratos="-1";
		$consulta="SELECT UPPER(t.noContrato),t.montoContrato,txtRazonSocial2,t.id__751_tablaDinamica FROM _751_tablaDinamica t,_405_tablaDinamica p WHERE p.id__405_tablaDinamica=t.cmbProveedor AND  t.noContrato LIKE '%".$criterio."%'
				and id__751_tablaDinamica not in (".$listContratos.")"	;

		$resContratos=$con->obtenerFilas($consulta);
		$arrObjetos="";
		while($filaContrato=mysql_fetch_row($resContratos))
		{
			$obj='{"idContrato":"'.$filaContrato[3].'","contrato":"'.str_replace(strtoupper($criterio),"<b>".strtoupper($criterio)."</b>",$filaContrato[0]).'","monto":"$ '.number_format($filaContrato[1]).'","proveedor":"'.cv($filaContrato[2]).'"}';
			if($arrObjetos=="")
				$arrObjetos=$obj;
			else
				$arrObjetos.=",".$obj;
		}
		$arrObjetos="[".$arrObjetos."]";
		echo '{"num":"","contratos":'.$arrObjetos.'}';
	}
	
	function obtenerAreasProrrateo()
	{
		global $con;
		$cadDeptos=$_POST["cadDeptos"];
		if($cadDeptos=="")
			$cadDeptos="-1";
		$consulta="SELECT codigoUnidad,unidad,codigoDepto FROM 817_organigrama WHERE codigoUnidad not in (".$cadDeptos.") and instColaboradora=0 AND institucion=0 AND (unidadPadre='0001' OR unidadPadre LIKE '0001%') ORDER BY TRIM(codigodepto)";

		$arrAreas=$con->obtenerFilasArreglo($consulta);
		echo "1|".$arrAreas;
	}

	function obtenerBasesProrrateo()
	{
		global $con;
		$bases=$_POST["base"];
		$cadDeptos=$_POST["cadDeptos"];
		$arrBases=explode(",",$bases);
		$arrDeptos=explode(",",$cadDeptos);

		$objRes='';
		$objDepto="";
		$aDeptos="";
		foreach($arrDeptos as $depto)
		{
			$objDepto=$depto;
			foreach($arrBases as $base)
			{
				$cadObj='{"codigoUnidad":"'.str_replace("'","",$depto).'"}';
				$obj=json_decode($cadObj);
				$Nul=NULL;
				$resBase=resolverExpresionCalculoPHP($base,$obj,$Nul);

				$objDepto.=",'".$resBase."'";
			}	
			if($aDeptos=="")
				$aDeptos="[".$objDepto."]";
			else
				$aDeptos.=",[".$objDepto."]";
			
		}
		$aDeptos="[".$aDeptos."]";
		echo "1|".$aDeptos;
		
	}	
	
	function guardarProrrateoContrato()
	{
		global $con;
		$idContrato=$_POST["idContrato"];
		$cadObj=$_POST["cadObj"];
		$cadDeptos=$_POST["cadDeptos"];
		$query="select idContrato from 562_prorrateoContratos where idContrato=".$idContrato;
		$idContratoRes=$con->obtenerValor($query);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="delete from 563_deptosVSProrrateo WHERE idProrrateo=".$idContrato;
		$x++;
		if($idContratoRes=="")
			$consulta[$x]="INSERT INTO 562_prorrateoContratos(idContrato,estructura) VALUES(".$idContrato.",'".cv($cadObj)."')";
		else
			$consulta[$x]="update 562_prorrateoContratos set estructura='".cv($cadObj)."' where idContrato=".$idContrato;
		$x++;
		$arrDeptos=explode(",",$cadDeptos);
		$nDeptos=sizeof($arrDeptos);
		for($y=0;$y<$nDeptos;$y++)
		{
			$consulta[$x]="insert into 563_deptosVSProrrateo (idProrrateo,codigoUnidad) values(".$idContrato.",'".$arrDeptos[$y]."')";
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
		
	}
	
	function obtenerActivoFijoDepreciado()
	{
		global $con;
		$inicio=$_POST["start"];
		$limite=$_POST["limit"];
		$arrArticulos="";
		$filtroUsuario1="";
		$filtroUsuario2="";
		/*if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						switch($arrFiltro[$x]["field"])
						{
							case "numInventario":
								$filtroUsuario1.=" and i.numInventario like '".$arrFiltro[$x]["data"]["value"]."%'";
								$filtroUsuario2.=" and codigo, like '".$arrFiltro[$x]["data"]["value"]."%'";
							break;
							case "descripcion":
								$filtroUsuario1.=" and descripcion like '".$arrFiltro[$x]["data"]["value"]."%'";
								$filtroUsuario2.=" and cp.nombreProducto like '".$arrFiltro[$x]["data"]["value"]."%'";
							break;
							case "anioAdquisicion":
								$filtroUsuario1.=" and DATE_FORMAT(fechaAdquisicion,'Y') like '".$arrFiltro[$x]["data"]["value"]."%'";
								$filtroUsuario2.=" and DATE_FORMAT(fechaCompra,'Y')  like '".$arrFiltro[$x]["data"]["value"]."%'";
							break;
								
						}
					
						
				
						
					break;
				}
			}
		}*/
	$filtroUsuario="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$filtroUsuario.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
					
				}
			}
		}
		$consulta="select count(*) from 9307_inventario";
		
		$nArticulo=$con->obtenerValor($consulta);
		$consulta="select numInventario,descripcion,anioAdquisicion,valorCompra,claveCABMS from 9000_inventarioActivoFijo where 1=1 and descripcion<>'' ".$filtroUsuario." limit ".$inicio.",".$limite;
		
		/*$consulta="(SELECT i.numInventario,i.descCorta AS descripcion,DATE_FORMAT(fechaAdquisicion,'%Y') AS anioAdquisicion,IF(claveProyecto='',valorInversion,valorProyecto) AS valorCompra,i.claveCABMS,estadoDepreciacion,'0' AS origen FROM 9120_inventario i,9307_inventario i2
					WHERE i2.idProducto=-1 AND i.numInventario=i2.codigo ".$filtroUsuario1.")
					UNION
					(
						SELECT codigo,cp.nombreProducto AS descripcion,DATE_FORMAT(fechaCompra,'%Y') AS anioAdquisicion,precioCompra,claveCABMS,estadoDepreciacion,'1' AS origen FROM 9307_inventario i,9101_CatalogoProducto cp WHERE 
						cp.idProducto=i.idProducto AND i.idProducto<>-1 ".$filtroUsuario2."
					)	order by descripcion
	";
*/
		$resArticulos=$con->obtenerFilas($consulta);
		$idFormula=555;
		$factor=0.5;
		$nActual=date("Y");
		while($fila=mysql_fetch_row($resArticulos))
		{
			$clave=str_pad(substr($fila[4],0,3),10,'0',STR_PAD_RIGHT);
			$consulta="select txtDescripcion,txtVida from _802_tablaDinamica where txtClave='".$clave."'";
			$fCabms=$con->obtenerPrimeraFila($consulta);
			$tVida=$fCabms[1];
			if($tVida=="")
				$tVida=5;
			$nInventario=$fila[0];
			
			//$cadObj='{"noInventario":"'.$nInventario.'","param2":"'.$tVida.'","param3":"'.$nActual.'"}';
			
			/*$obj=json_decode($cadObj);
			
			$nNull=null;
			$factor=resolverExpresionCalculoPHP($idFormula,$obj,$nNull);*/
			$factor=($nActual-$fila[2])/$tVida;
			if($factor>1)
				$factor=1;
			if(($factor=="''")||($factor==""))
				$factor=0;
			$depreciacionAcum=$fila[3]*$factor;
			$obj='{"CABMS":"'.$fCabms[0].'","numInventario":"'.$fila[0].'","descripcion":"'.cv($fila[1]).'","anioAdquisicion":"'.$fila[2].'","valorCompra":"'.$fila[3].'","factorDepreciacion":"'.$factor.'","valorDepreciacion":"'.$depreciacionAcum.'","valorLibro":"'.($fila[3]-$depreciacionAcum).'"}'	;
			if($arrArticulos=="")
				$arrArticulos=$obj;
			else
				$arrArticulos.=",".$obj;
		}
		
		echo '{"num":"'.$nArticulo.'","registros":['.$arrArticulos.']}';	
	}
	
	function guardarProyeccionServicio()
	{
		global $con;
		$cadObj=$_POST["obj"];	
		$obj=json_decode($cadObj);
		
		$desgloce=explode(",",$obj->desgloce);
		
		$obj->desgloce=$desgloce;
		if($obj->idRegistro==-1)
		{
			$consulta="INSERT INTO 564_proyeccionCostoServicio(ciclo,idServicio,idNivel,totalServicios,costoServicio,porcentajeNivel,
					montoTotal,enero,febrero,marzo,abril,mayo,junio,julio,agosto,septiembre,octubre,noviembre,diciembre)
					VALUES(".$obj->ciclo.",".$obj->idServicio.",".$obj->idNivel.",".$obj->totalServicios.",".$obj->costoServicio.",".$obj->porcentajeNivel.",
					".$obj->montoTotal.",".$obj->desgloce[0].",".$obj->desgloce[1].",".$obj->desgloce[2].",".$obj->desgloce[3].",".$obj->desgloce[4].",
					".$obj->desgloce[5].",".$obj->desgloce[6].",".$obj->desgloce[7].",".$obj->desgloce[8].",".$obj->desgloce[9].",".$obj->desgloce[10].",".$obj->desgloce[11].")";	
		}
		else
		{
			$consulta="update 564_proyeccionCostoServicio set ciclo=".$obj->ciclo.",idServicio=".$obj->idServicio.",idNivel=".$obj->idNivel.",totalServicios=".$obj->totalServicios.",
					costoServicio=".$obj->costoServicio.",porcentajeNivel=".$obj->porcentajeNivel.",montoTotal=".$obj->montoTotal.",enero=".$obj->desgloce[0].",febrero=".$obj->desgloce[1].",
					marzo=".$obj->desgloce[2].",abril=".$obj->desgloce[3].",mayo=".$obj->desgloce[4].",junio=".$obj->desgloce[5].",julio=".$obj->desgloce[6].",
					agosto=".$obj->desgloce[7].",septiembre=".$obj->desgloce[8].",octubre=".$obj->desgloce[9].",noviembre=".$obj->desgloce[10].",diciembre=".$obj->desgloce[11]." where idServicioProy=".$obj->idRegistro;
		}

		if($con->ejecutarConsulta($consulta))		
		{
			if($obj->idRegistro==-1)
			{
				$obj->idRegistro=$con->obtenerUltimoID();
				
			}	
			echo "1|".$obj->idRegistro;
		}
	}
	
	function obtenerConcentradoRopaje()
	{
		global $con;
		
		$idCiclo=$_POST["idCiclo"];
		$arrPrendas=Array();
		
		$conDeptos="SELECT 	codigoUnidad FROM 817_organigrama WHERE codigoInstitucion=".$_SESSION["codigoInstitucion"]." AND institucion=0";
		$lista=$con->obtenerListaValores($conDeptos);
		if($lista=="")
			$lista="-1";
		
		$consulta="SELECT idPrenda,(SELECT txtTipoPrenda FROM _757_tablaDinamica WHERE id__757_tablaDinamica=td.idPrenda) AS prenda,tipoTalla,
					color,(SELECT txtColor FROM _758_tablaDinamica WHERE id__758_tablaDinamica=td.color) AS nColor,
					cantidad ,tamano,genero,(SELECT costoEstimado FROM _757_tablaDinamica WHERE id__757_tablaDinamica=td.idPrenda)
					FROM 533_ropajeUsuario td  WHERE ciclo=".$idCiclo." AND  codigoUnidad IN (".$lista.")  AND codigoUnidad IN 
					(SELECT codigoDepartamento FROM 534_departamentosCierreRopaje WHERE ciclo=".$idCiclo.")";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			if($fila[2]==1)
				$conEtiquetaTam="select talla FROM _767_dtgZapato where id__767_dtgZapato=".$fila[6];
			else
				$conEtiquetaTam="select DescTalla FROM _766_dtgTallas WHERE id__766_dtgTallas=".$fila[6];
			
			$etiqueta=$con->obtenerValor($conEtiquetaTam);
			
			if($fila[7]==1)
			{
				$sexo="Dama";
			}
			else
			{
				if($fila[7]==0)
					$sexo="Caballero";
				else
					$sexo="Unisex";
			}
			
			if(!isset($arrPrendas[$fila[0]][$fila[3]][$etiqueta]))
				$arrPrendas[$fila[0]][$fila[3]][$etiqueta]=$fila[5];
			else
				$arrPrendas[$fila[0]][$fila[3]][$etiqueta]+=$fila[5];
		}
		
		//echo var_dump($arrPrendas);
		$conta=1;
		$arrFinal="";
		foreach($arrPrendas as $prenda=>$resto)
		{
			foreach($resto as $obj=>$a)
			{
				foreach($a as $tamano)
				{
					$cantidad=$arrPrendas[$prenda][$obj][$a];
					echo "es aqui".$arrPrendas[$prenda]=>[$resto]=>[$a];
					$nombrePrenda="SELECT txtTipoPrenda FROM _757_tablaDinamica WHERE  id__757_tablaDinamica=".$prenda;
					$nPren=$con->obtenerValor($nombrePrenda);
					
					$nombreColor="SELECT txtColor FROM _758_tablaDinamica WHERE id__758_tablaDinamica=".$obj;
					$nColor=$con->obtenerValor($nombreColor);
					
					$conPrecio="SELECT costoEstimado FROM _757_tablaDinamica WHERE id__757_tablaDinamica=".$prenda;
					$costo=$con->obtenerValor($conPrecio);
					if($costo=="")
						$costo=0;
					
					$aux='{"idPrenda":"'.$prenda.'","nomPren":"'.$nPren.'","idColor":"'.$obj.'","nColor":"'.$nColor.'","talla":"'.$tamano.'","cantidad":"'.$cantidad.'","costo":"'.$costo.'"}';
					if($arrFinal=="")
						$arrFinal=$aux;
					else
						$arrFinal.=",".$aux;
					
					$conta++;	
				}
			}
		}
		
		echo '{"numReg":"'.$conta.'","registros":['.$arrFinal.']}';	
	}
?>
