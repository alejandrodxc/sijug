<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");

function obtenerLetraDia($num)
{
	switch($num)
		{
			case 1:
				$letra="L";
			break;
			case 2:
				$letra="M";
			break;
			case 3:
				$letra="M";
			break;
			case 4:
				$letra="J";
			break;
			case 5:
				$letra="V";
			break;
			case 6:
				$letra="S";
			break;
			case 7:
				$letra="D";
			break;
		}
		return $letra;
}
//idUsuario,idServicio,idTurno,idCategoria,$fechaInicio,$fechaFin
function generarGridPeriodo($fechaInicio,$fechaFin,$idTurno,$diasAntes,$diasDespues,$colorGrid)
{
	$ancho=275;
	$anchoCelda=40;
	$altoCelda=35;
	$color="AAAAAA";
	global $con;
	global $arrMesLetra;
	global $idConfiguracion;
	global $idCategoria;
	global $idServicio;
	global $arrAcumuladores;
	foreach($arrAcumuladores as $pos=>$obj)
	{
		$arrAcumuladores[$pos]["nElementos"]=array();

	}
	$colorGrid=str_replace("#","",$colorGrid);
	$estiloTbl="";
	if($colorGrid!="")
	{
		$estiloTbl=" style='border-style:solid; border-color:#".$colorGrid."; border-width:2px; padding: 3px 3px 3px 3px; '";
	}
	
	$cadTabla="	
				<table>
				<tr>
					<td ".$estiloTbl.">
				<table  width='@tTabla'>
				<tr height='21'>
					<td width='80'></td>
					<td width='275'>
					</td><td colspan='".$diasAntes."'></td>@tituloMeses<td colspan='".$diasDespues."'>
				</tr>
				<tr>
					
					<td colspan='2'  style='border:solid; border-width:1px; border-color:#000'>&nbsp;
					</td>
					";

				  $fechaDia=$fechaInicio;
				  $ultimoDia="";
				  $mesActual="";
				  $tituloMes="";
				  while($fechaDia<=$fechaFin)
				  {
						if(date("d",$fechaDia)==1)
						{
							$ultimoDia=date("t",$fechaDia);
							$mesActual=	$arrMesLetra[date("m",$fechaDia)-1];
							
						}
						if(date("d",$fechaDia)==$ultimoDia)
						{
							$tituloMes.="<td colspan='".$ultimoDia."' style='background-color: #".$color."' align='center'><b>".$mesActual."</b></td>";
							if($color=="888888")
								$color="AAAAAA";
							else
								$color="888888";
								
						}
						$estilo="style='border:solid; border-width:1px; border-color:#000'";
						$cadTabla.="<td width='".$anchoCelda."' align='center' ".$estilo.">".obtenerLetraDia(date('N',$fechaDia))."</td>";	
						$fechaDia=strtotime("+ 1 days ",$fechaDia);
						$ancho+=$anchoCelda;
				  }
				
							   
	$cadTabla.="</tr>
				<tr>
				<td  style='border:solid; border-width:1px; border-color:#000' align='center'>
				<span class='letraExt'>
				Cve. Emp.
				</span>
				</td>
				<td  style='border:solid; border-width:1px; border-color:#000' align='center'><span class='letraExt'>Nombre del Empleado</span>
					</td>
				";

				$fechaDia=$fechaInicio;
				
				while($fechaDia<=$fechaFin)
				{
					$estilo="style='border:solid; border-width:1px; border-color:#000'";
					$cadTabla.="<td  align='center' ".$estilo.">";
				    $fechaF=date('d',$fechaDia);
					$cadTabla.=$fechaF."</td>";
					$fechaDia=strtotime("+ 1 days ",$fechaDia);
				}
                                             
	$cadTabla.="</tr>";
	$baseUsuario='{"idUsuario":"@idUsuario","idServicio":"'.$idServicio.'","idTurno":"'.$idTurno.'","idCategoria":"'.$idCategoria.'","fechaInicio":"'.date("Y-m-d",$fechaInicio).'","fechaFin":"'.date("Y-m-d",$fechaFin).'"}';
	$arrCalculosUsuario=array();
	$consulta="SELECT cmbCaegoria FROM _799_tablaDinamica f WHERE cmbCaegoria<>-1 and cmbActivo=1 and cmbTipoEvaluacion=1";
	$res=$con->obtenerFilas($consulta);
	while($filaCat=mysql_fetch_row($res))
	{
		$arrCalculosUsuario[$filaCat[0]]=array();	
	}
	
	
	
	$consulta="SELECT cmbCaegoria,radTipo,txtSigla,Descripcion,txtCodigo,cmbPermiteAsignar,(select txtCodigo from _800_tablaDinamica where id__800_tablaDinamica=cmbcolorletra) as colorLetra,cmbTipoEvaluacion,id__799_tablaDinamica 
	FROM _799_tablaDinamica f,_800_tablaDinamica c WHERE f.cmbColores=c.id__800_tablaDinamica AND cmbCaegoria<>-1 and cmbActivo=1";
	$resFunciones=$con->obtenerFilas($consulta);
	for($tmp=0;$tmp<1;$tmp++)
	{
		$consulta="select i.idUsuario,concat(Paterno,' ',Materno,' ',Nom) as empleado FROM 9315_empleadoRolEnfermeria e,802_identifica i WHERE i.idUsuario=e.idUsuario and 
					idTurno=".$idTurno." AND idCategoria=".$idCategoria." AND idConfiguracion=".$idConfiguracion." order by Paterno,Materno,Nom ";
		
		$resEmpleados=$con->obtenerFilas($consulta);
		$ref=NULL;
		while($fila=mysql_fetch_row($resEmpleados))
		{
			
			$border="000000";
			$fondo="FFFFFF";
			$circulo="";
			$letra="000000";
			$cadDescripcion="";
			$cadObjSituacion=str_replace("@idUsuario",$fila[0],$baseUsuario);
			$oUsr=json_decode($cadObjSituacion);
			foreach($arrCalculosUsuario as $idCalculo=>$resto)
			{
				$cadResultado=resolverExpresionCalculoPHP($idCalculo,$oUsr,$ref);
				
				if($cadResultado!="-1")
					$arrCalculosUsuario[$idCalculo]=explode(",",str_replace("'","",$cadResultado));
				else	
					$arrCalculosUsuario[$idCalculo]=array();

				
			}
			
			
			$consulta="SELECT funcionIdentificacion,(select txtCodigo from _800_tablaDinamica where id__800_tablaDinamica=colorIdentificador) as color,tipoPintado,descripcion,colorIdentificador FROM _866_gridSituacionesEmpleado WHERE activo=1";
			$resSit=$con->obtenerFilas($consulta);
			while($filaSit=mysql_fetch_row($resSit))
			{
				$resultado=resolverExpresionCalculoPHP($filaSit[0],$oUsr,$ref);
				if($resultado==1)
				{
					if($cadDescripcion=="")
						$cadDescripcion=$filaSit[3];
					else
						$cadDescripcion.="<br>".$filaSit[3];
					switch($filaSit[2])
					{
						case 1: //Contorno
							$border=$filaSit[1];
						break;	
						case 2: //Fondo
							$fondo=$filaSit[1];
						break;	
						case 3: //Cìrculo
							if($circulo=="")
								$circulo="<br>";
							$circulo.='&nbsp;<span class="x-combo-list-item ext-color-'.$filaSit[4].'"><span class="ext-cal-picker-icon">&nbsp;</span></span>';
						break;	
						case 4: //Letra
							$letra=$filaSit[1];
						break;	
					}
				}
			}
			
			$cadTabla.="<tr height='".$altoCelda."' id='".$fila[0]."_".$idTurno."'>
							<td style='border:solid; border-width:1px; border-color:#".$border."; background-color:#".$fondo."; color:#".$letra."' align='center' alt='".$cadDescripcion."' title='".$cadDescripcion."'><span class='letraExt'>".$fila[0]."</span></td>
							<td style='border:solid; border-width:1px; border-color:#".$border."; background-color:#".$fondo."; ' align='left' alt='".$cadDescripcion."' title='".$cadDescripcion."'>&nbsp;&nbsp;<span class='letraExt' > <a href='javascript:verUsrNuevaPagina(\"".bE($fila[0])."\")'><span style='color:#".$letra."'>".$fila[1]."</span></a></span>".$circulo."</td>";
			$fechaDia=$fechaInicio;
			while($fechaDia<=$fechaFin)
			{
				$check="";
				$nEstilo="";
				$etiqueta="";
				$alt="";
				$evClick="";
				$btnEliminar="";
				if(mysql_num_rows($resFunciones)>0)
					mysql_data_seek($resFunciones,0);
				while($filaFunc=mysql_fetch_row($resFunciones))
				{
					$colorLetra=str_replace("#","",$filaFunc[6]);
					if($colorLetra=="")
						$colorLetra="000";
					$resultado=="";
					$fechaEval=date('Y-m-d',$fechaDia);
					if($filaFunc[7]==2)
					{
						$cadObj='{"idUsuario":"'.$fila[0].'","fechaVerificar":"'.$fechaEval.'"}';
						$obj=json_decode($cadObj);
						$resultado=resolverExpresionCalculoPHP($filaFunc[0],$obj,$ref);
					}
					else
					{
						
						if(existeValor($arrCalculosUsuario[$filaFunc[0]],$fechaEval))
							$resultado=1;
						else
							$resultado=0;
					}
					if($resultado==1)
					{
						if($btnEliminar=="")
						{
							$btnEliminar="<a href='javascript:removerInciencia(\"".bE($fila[0]."_".date("Y-m-d",$fechaDia).'_'.$idTurno)."\")'><img height='12' width='12' src='../images/delete.png' title='Remover incidencia' alt='Remover incidencia'></a>";
							
						}
						
						foreach($arrAcumuladores as $pos=>$resto)
						{
							$r=existeValor($resto["elementosConsiderar"],$filaFunc[8]);
							if($r)
							{
								if(!isset($resto["nElementos"][$fechaEval]))
									$arrAcumuladores[$pos]["nElementos"][$fechaEval]=1;
								else
									$arrAcumuladores[$pos]["nElementos"][$fechaEval]++;	
								
							}
						}
						
						$etiqueta=$filaFunc[2];
						$alt=$filaFunc[3];
						$filaFunc[4]=str_replace("#","",$filaFunc[4]);
						switch($filaFunc[1])
						{
							case "1":
								$nEstilo="style='color:#".$colorLetra.";background-color:#".$filaFunc[4].";border:solid; border-width:1px; border-color:#000;padding:0px 0px 0px 0px'";
							break;
							case "2";
								$nEstilo="style='color:#".$colorLetra.";border:solid; border-width:1px; border-color:#".$filaFunc[4].";padding:0px 0px 0px 0px'";
							break;
							case "3";
								$nEstilo="style='color:#".$colorLetra.";border:solid; border-width:1px; border-color:#000;padding:0px 0px 0px 0px'";
							break;
						}
						if($filaFunc[5]!=0)
						{
							$check='<input type="checkbox" @name id="chk_'.$fila[0]."_".date("Y-m-d",$fechaDia).'_'.$idTurno.'" @reemplazo @checado onclick="asignaCheck(this)">';
						}
						break;	
									
					}
				}
							
				if($nEstilo=="")
				{
					$estilo="style=\"border:solid; border-width:1px; border-color:#000;padding:0px 0px 0px 0px\"";
					$check='<input type="checkbox" @name  id="chk_'.$fila[0]."_".date("Y-m-d",$fechaDia).'_'.$idTurno.'" @reemplazo @checado onclick="asignaCheck(this)">';
				}
				else
					$estilo=$nEstilo;
						
				$cadTabla.="<td  width='40' align='center' ".$estilo." id='".$fila[0]."_".date("Y-m-d",$fechaDia)."_".$idTurno."'  onclick='clickCelda(\"chk_".$fila[0]."_".date("Y-m-d",$fechaDia).'_'.$idTurno."\")' >
							<span alt=".$alt.">".$etiqueta;
				
				if($check!="")
				{
					$query="select idUsuario from 9315_asignacionesFechas where idTurno=".$idTurno." and idServicio=".$idServicio." and fechaAsignacion='".date("Y-m-d",$fechaDia)."' and idUsuario=".$fila[0];
					$filaAsigna=$con->obtenerPrimeraFila($query);
					if($filaAsigna)
						$check=str_replace("@checado","checked='checked'",$check);
					else
						$check=str_replace("@checado",'',$check);
						
					if($fechaDia<=strtotime(date("Y-m-d")))
					{
						$check=str_replace("@name","",$check);
						$check=str_replace("@reemplazo","disabled='disabled'",$check);
						$btnEliminar="";
					}
					else
					{
						$check=str_replace("@name","name='checkAsigna'",$check);
						$check=str_replace("@reemplazo","",$check);
					}
					$cadTabla.="<br>".$check;
				}			
				
				$cadTabla.="
							</span>&nbsp;".$btnEliminar."
						</td>";
	
				$fechaDia=strtotime("+ 1 days ",$fechaDia);
			}
			$cadTabla.="</tr>
	
						";
		}
	}
	
	foreach($arrAcumuladores as $resto)
	{
		$fechaDia=$fechaInicio;
		$cadTabla.="<tr height='".$altoCelda."'>
							<td   colspan='2' style='border:solid; border-width:1px; border-color:#0000; background-color:#FFFFFF; color:#000000' align='left' ><span class='letraExt'>".$resto["leyenda"]."</span></td>
						";
		while($fechaDia<=$fechaFin)
		{
			$fechaEval=date('Y-m-d',$fechaDia);
			if(isset($resto["nElementos"][$fechaEval]))
				$cadTabla.="<td align='center'  style='border:solid; border-width:1px; border-color:#0000; background-color:#FFFFFF; color:#000000'><span class='letraExt'>".$resto["nElementos"][$fechaEval]."</span></td>";
			else
				$cadTabla.="<td align='center'  style='border:solid; border-width:1px; border-color:#0000; background-color:#FFFFFF; color:#000000'><span class='letraExt'>0</span></td>";
			$fechaDia=strtotime("+ 1 days ",$fechaDia);
		}
		$cadTabla.="</tr>";
	}
	
	$cadTabla.="</table>
				</td>
			</tr>
		</table>	
	";	
	
	$cadTabla=str_replace("@tTabla",$ancho,$cadTabla);
	$cadTabla=str_replace("@tituloMeses",$tituloMes,$cadTabla);
	echo $cadTabla;
	flush();
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<link rel="stylesheet" type="text/css" href="../Scripts/ext/agenda/calendar.css" />
<style>
.ext-cal-picker-icon
{
	width:8px !important;
	height:8px !important;
}
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$consulta="select id__800_tablaDinamica,txtCodigo from _800_tablaDinamica";
	$resColor=$con->obtenerFilas($consulta);
	while($filaColor=mysql_fetch_row($resColor))
	{
		echo "
				.ext-color-".$filaColor[0].", .ext-ie .ext-color-".$filaColor[0]."-ad, .ext-opera .ext-color-".$filaColor[0]."-ad 
				{
					color: #".$filaColor[1].";
				}
				.ext-cal-day-col .ext-color-".$filaColor[0].", .ext-dd-drag-proxy .ext-color-".$filaColor[0].",	.ext-color-".$filaColor[0]."-ad,
				.ext-color-".$filaColor[0]."-ad .ext-cal-evm, .ext-color-".$filaColor[0]." .ext-cal-picker-icon, .ext-color-".$filaColor[0]."-x dl,
				.ext-color-".$filaColor[0]."-x .ext-cal-evb 
				{
					background: #".$filaColor[1].";
				}
				.ext-color-".$filaColor[0]."-x .ext-cal-evb, .ext-color-".$filaColor[0]."-x dl 
				{
					border-color: #8C500B;
				}
	
			";
	}
?>
</style>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<link rel="stylesheet" type="text/css" href="../Scripts/Carousel/thickbox.css"/>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<script type="text/javascript" src="../Scripts/Carousel/thickbox.js"></script>
<script type="text/javascript" src="Scripts/ventanasIncidenciasRolEnfermeria.js.php"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<?php

?>
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td >
						<?php
						
						$idPeriodo="-1";
						
						if(isset($objParametros->idPeriodo))
						{
							$idPeriodo=$objParametros->idPeriodo;
						}
						$ciclo="-1";
						if(isset($objParametros->ciclo))
						{
							$ciclo=$objParametros->ciclo;
						}
						$idCategoria="-1";
						if(isset($objParametros->idCategoria))
						{
							$idCategoria=$objParametros->idCategoria;
						}
						$idServicio="-1";
						if(isset($objParametros->idServicio))
						{
							$idServicio=$objParametros->idServicio;
						}
						$consulta="SELECT * FROM 9315_periodosRoles WHERE noPeriodo=".$idPeriodo;
						$filaPeriodo=$con->obtenerPrimeraFila($consulta);
						$periodo=$filaPeriodo[1];
						$listMeses=$filaPeriodo[2];
						$arrMeses=explode(",",$listMeses);
						$consulta="SELECT servicioEnfermeria FROM _465_tablaDinamica where id__465_tablaDinamica=".$idServicio;
						$servicio=$con->obtenerValor($consulta);
						$consulta="SELECT txtCategoria FROM _865_tablaDinamica WHERE id__865_tablaDinamica=".$idCategoria;
						$puesto=$con->obtenerValor($consulta);
						$consulta="select idConfiguracion from 9315_configuracionRolEnfermeria where ciclo=".$ciclo." and idServicio=".$idServicio." and idPeriodo=".$idPeriodo;
						$idConfiguracion=$con->obtenerValor($consulta);
						if($idConfiguracion=="")
							$idConfiguracion="-1";
						?>
                        <script type="text/javascript" src="../recursosHumanos/Scripts/rolEnfermeria.js.php"></script>
                        <table >
                        	<tr>
                            	<td align="left">
                                	<table >
                                    	<tr>
                                        	<td>
                                            <BR />
                                            <BR />
                                            <table>
                                            	<tr>
                                                	<td>
														<?php
                                                            echo $banner;
                                                        ?>
                                                    </td>
                                                    <td align="center" valign="top"> 
                                                     <span class="tituloPaginas" style="line-height:25px">
                                                    Subdirección de Enfermería "María Dolores Rodríguez Ramírez"<br />
                                                    Departamento de Enfemería<br />
                                                   Rol de turnos y descansos</span>
                                                    </td>
                                                </tr>
                                            <BR />
                                            <BR />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                        	<td align="left"><br /><br />
                                            	<table >
                                                	<tr height="21">
                                                    	<td width="80">
                                                        <span class="corpo8_bold">Periodo:</span>
                                                        </td>
                                                        <td>
                                                        <span class="copyrigthSinPadding">
                                                        <?php
															echo $periodo;
														?>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr height="21">
                                                    	<td width="80">
                                                        <span class="corpo8_bold">Servicio:</span>
                                                        </td>
                                                        <td>
                                                        <span class="copyrigthSinPadding">
                                                        <?php
															echo $servicio;
														?>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr height="21">
                                                    	<td width="80">
                                                        <span class="corpo8_bold">Categoría:</span>
                                                        </td>
                                                        <td >
                                                        <span class="copyrigthSinPadding">
                                                        <?php
															echo $puesto;
														?>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                                <br /><br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                	<table>
                                    <tr>
                                    	<td>
											<span id='spButton'></span>
                                        </td>
                                        <td width="10">
                                        </td>
                                        <td>
	                                        <span id='spButton2'></span>
                                        </td>
                                         <td width="10">
                                        </td>
                                        <td>
	                                        <span id='spButton3'></span>
                                        </td>
                                    </tr>    
                                    </table><br /><br />
									<?php
										$mesInicio=$arrMeses[0];
										if($mesInicio<10)
											$mesInicio="0".$mesInicio;
										$mesFinal=$arrMeses[sizeof($arrMeses)-1];
										if($mesFinal<10)
											$mesFinal="0".$mesFinal;
										$fInicio=$ciclo."-".$mesInicio."-01";
										$mes = mktime( 0, 0, 0, $mesFinal, 1, $ciclo ); 
										$fFin=$ciclo."-".$mesFinal."-".date("t",$mes);
										
										$fI=strtotime($fInicio);
										$fF=strtotime($fFin);
										$dI=date("w",$fI);
										$dF=date("w",$fF);
										if($dI==0)
											$dI=7;
										$nDias=$dI-1;
										if($dF==0)
											$dF=7;
										$nDiasFin=7-$dF;
										$fInicio=strtotime("-".$nDias." days",$fI);
										$fFinal=strtotime("+".$nDiasFin." days",$fF);
										$arrAcumuladores=array();
										$consulta="SELECT id__879_tablaDinamica,txtDescripcion FROM _879_tablaDinamica";
										$resAcum=$con->obtenerFilas($consulta);
										while($filaAcum=mysql_fetch_row($resAcum))
										{
											$arrAcumuladores[$filaAcum[0]]["leyenda"]=$filaAcum[1];
											$arrAcumuladores[$filaAcum[0]]["nElementos"]=array();
											$arrAcumuladores[$filaAcum[0]]["elementosConsiderar"]=array();
											$consulta="SELECT incidenciaConsiderar FROM _879_gridLeyendasConsiderar WHERE idReferencia=".$filaAcum[0];
											$resIncidencia=$con->obtenerFilas($consulta);
											while($filaIncidencia=mysql_fetch_row($resIncidencia))
											{
												array_push($arrAcumuladores[$filaAcum[0]]["elementosConsiderar"],$filaIncidencia[0]);
											}
										}
										$consulta="SELECT c.idTurno FROM _465_tablaDinamica t,_465_gridTurno c WHERE c.idReferencia=t.id__465_tablaDinamica
													AND t.id__465_tablaDinamica=".$idServicio." order by c.idTurno";
										$res=$con->obtenerFilas($consulta);
										while($fTurno=mysql_fetch_row($res))
										{
											$consulta="SELECT txtTurno,(select txtCodigo from _800_tablaDinamica where id__800_tablaDinamica=cmbColores) as color FROM _738_tablaDinamica WHERE id__738_tablaDinamica=".$fTurno[0];
											
											$fTurnoAux=$con->obtenerPrimeraFila($consulta);
											$turno=$fTurnoAux[0];
											$color=$fTurnoAux[1];
											echo '<span class="corpo8_bold" style="font-size:14px" >Turno:</span><span class="letraRojaSubrayada8" style="font-size:14px"> '.$turno.'</span><br><br>';
                                        	generarGridPeriodo($fInicio,$fFinal,$fTurno[0],$nDias,$nDiasFin,$color);
											echo "<br><br><br>";

										}
                                        
                                    ?>
                                    
                                    
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="idPeriodo" name="idPeriodo" value="<?php echo $idPeriodo ?>" />
                        <input type="hidden" id="idServicio" name="ciclo" value="<?php echo $idServicio ?>" />
                        <input type="hidden" id="idCategoria" name="idServicio" value="<?php echo $idCategoria ?>" />
                        <input type="hidden" id="ciclo" name="idCategoria" value="<?php echo $ciclo ?>" />
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
