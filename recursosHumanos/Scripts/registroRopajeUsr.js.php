<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	//$consulta="SELECT talla,talla,concat(medida, 'cm.') as medida FROM _767_dtgZapato ORDER BY CAST(talla AS DECIMAL)";
//	$arrTallaZap=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__767_dtgZapato,talla FROM _767_dtgZapato ORDER BY CAST(talla AS DECIMAL)";
	$arrTallaZap=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT DISTINCT(id__766_dtgTallas),DescTalla FROM _766_dtgTallas WHERE idReferencia=1 GROUP BY DescTalla ORDER BY DescTalla";
	$arrTallaH=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT DISTINCT(id__766_dtgTallas),DescTalla FROM _766_dtgTallas WHERE idReferencia=2 GROUP BY DescTalla ORDER BY DescTalla";
	$arrTallaM=$con->obtenerFilasArreglo($consulta);
?>
Ext.onReady(inicializar);
var arrTallaZap=<?php echo $arrTallaZap?>;
var arrTallaH=<?php echo $arrTallaH?>;
var arrTallaM=<?php echo $arrTallaM?>;

function inicializar()
{
	crearGridRopaje();

}

function crearGridRopaje()
{
	var contador=gE('contador').value;
    var x=0;
    for(x=0;x<contador;x++)
    {
        var cmbTamano=crearComboExt('cmbTamano'+x,[]);
        var dsDatos=eval(bD(gE('arrPrendas'+x).value));
        var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'prenda'},
                                                                    {name: 'tipoTalla'},
                                                                    {name: 'color'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'tamano'},
                                                                    {name: 'idColor'},
                                                                    {name: 'idPrenda'},
                                                                    {name: 'situacion'}
                                                                ]
                                                    }
                                                );
    
        alDatos.loadData(dsDatos);
        var chkRow=new Ext.grid.CheckboxSelectionModel();
        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Prenda',
                                                                width:140,
                                                                sortable:true,
                                                                dataIndex:'prenda'
                                                            },
                                                            {
                                                                header:'Color',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'color'
                                                            },
                                                            {
                                                                header:'Cantidad anual',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
                                                                renderer:function(val)
                                                                            {
                                                                                return Ext.util.Format.number(val,'0.00');
                                                                            }
                                                            },
                                                            {
                                                                header:'Talla/Tama&ntilde;o *',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'tamano',
                                                                editor:cmbTamano,
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	
                                                                        	switch(registro.get('tipoTalla'))
                                                                            {
                                                                            
                                                                        		case '1':
                                                                                	return formatearValorRenderer(arrTallaZap,val);
                                                                                break;
                                                                                default:
                                                                                	var sexo=gE('sexo').value;
                                                                                    
                                                                                    if((sexo==='0')||(sexo=='2'))
                                                                                        return formatearValorRenderer(arrTallaH,val);
                                                                                    else
                                                                                    	return formatearValorRenderer(arrTallaM,val);
                                                                                break;        
                                                                            }
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridRopaje'+x,
                                                                renderTo:'tblRopaje_'+x,
                                                                store:alDatos,
                                                                frame:true,
                                                                cm: cModelo,
                                                                height:260,
                                                                width:530,
                                                                sm:chkRow
                                                            }
                                                        );
                                                        
		tblGrid.x=x;                                                        
        tblGrid.on('beforeedit',funcAntesEdit);                                                    
    }
}

function cancelar()
{
	function resp(btn)
    {
    	if(btn=='yes')
			window.parent.cerrarVentana();
    }
    msgConfirm('Est&aacute; seguro de querer cancelar el registro de uniforme de empleado?',resp);
}

function funcAntesEdit(e)
{
	var x=e.grid.x;
	if(e.record.get('situacion')=='1')
    	e.cancel=true;
	if(e.record.get('tipoTalla')=='1')
    {
    	gEx('cmbTamano'+x).getStore().loadData(arrTallaZap);
    }
    else
    {
    	var sexo=gE('sexo').value;
        if((sexo==='0')||(sexo=='2'))
	        gEx('cmbTamano'+x).getStore().loadData(arrTallaH);
        else
        	gEx('cmbTamano'+x).getStore().loadData(arrTallaM);
        
    }
}

function guardar()
{
    var contador=gE('contador').value;
    var y;
    var cadenaFinal='';
    var sexo=gE('sexo').value;
    for(y=0;y<contador;y++)
    {
        var idUniforme=bD(gE('idUniforme'+y).value);
        var gridRopaje=gEx('gridRopaje'+y).getStore();
        var x;
        var cadObj='';
        var obj;
        var fila;
        for(x=0;x<gridRopaje.getCount();x++)
        {
            var genero=bD(gE('genero'+y).value);
            fila=gridRopaje.getAt(x);
            if(fila.get('tamano')=='')
            {
            	var nomU=bD(gE('nombreU'+y).value);
                msgBox('El campo Talla/Tama&ntilde;o de la prenda: <b>'+fila.get('prenda')+'</b>  del uniforme <b>'+nomU+'</b> no puede quedar vacio');
                return;
            }
            obj='{"prenda":"'+fila.get('idPrenda')+'","color":"'+fila.get('idColor')+'","cantidad":"'+fila.get('cantidad')+'","tamano":"'+fila.get('tamano')+'","idUniforme":"'+idUniforme+'","tipoTalla":"'+fila.get('tipoTalla')+'","genero":"'+genero+'"}';
            if(cadObj=='')
                cadObj=obj;
            else
                cadObj+=','+obj;
        }
        
        var cadO='{"ciclo":"'+gE('ciclo').value+'","idUsuario":"'+gE('idUsuario').value+'","arrPrendas":['+cadObj+'],"idU":"'+idUniforme+'"}';
        if(cadenaFinal=='')
        	cadenaFinal=cadO;
        else
        	cadenaFinal+=','+cadO;    
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            function resp2()
            {
            	var nFila=gE('nFila').value;
                var fila=window.parent.gEx('gridRopaje').getStore().getAt(parseInt(nFila));
                fila.set('situacion','1');
            	window.parent.cerrarVentana();
            }
            msgBox('Los datos han sido guardados correctamente',resp2);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=37&cadena=['+cadenaFinal+']',true);
}

