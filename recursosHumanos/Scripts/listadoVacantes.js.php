<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
			
?>

Ext.onReady(inicializar);

var arrEtapas=[];
var arrActores=[];
function inicializar()
{
	arrEtapas=eval(bD(gE('arrEtapas').value));
    arrActores=eval(bD(gE('arrActores').value));
    var dsDatos=eval(bD(gE('arrVacantes').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idUnidadPuesto'},
                                                                {name: 'codUnidad'},
                                                                {name: 'idPuesto'},
                                                                {name: 'fechaVacante'},
                                                                {name: 'puesto'},
                                                                {name: 'departamento'},
                                                                {name: 'idFormularioPerfil'},
                                                                {name: 'etapa'},
                                                                {name: 'idRegistroPerfil'},
                                                                {name: 'idVacante'}
                                                               
                                                                
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'departamento'
														},
														{
															header:'Puesto',
															width:300,
															sortable:true,
															dataIndex:'puesto'
														},
                                                        {
															header:'Vacante desde:',
															width:100,
															sortable:true,
															dataIndex:'fechaVacante'
														},
                                                        {
															header:'Candidato',
															width:100,
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var idRegistro='-1';
                                                                        if((registro.get('idRegistroPerfil')!='')&&(registro.get('idRegistroPerfil')!='-1'))
                                                                        	idRegistro=registro.get('idRegistroPerfil');
                                                                            
                                                                        var idRegistroPerfil='-1';
                                                                        if((registro.get('idRegistroPerfil')!='')&&(registro.get('idRegistroPerfil')!='-1'))
                                                                        	idRegistro=registro.get('idRegistroPerfil');
                                                                    	if(idRegistro!='-1')
                                                                        {
                                                                        	var pos=existeValorMatriz(arrActores,registro.get('etapa'));
                                                                        	var idActor=arrActores[pos][1];
                                                                        	return '<a href="javascript:enviarRegistroPerfil(\''+bE(registro.get('idUnidadPuesto'))+'\',\''+bE(idActor)+'\',\''+bE(idRegistro)+'\',\''+bE('auto')+'\',\''+bE(registro.get('idFormularioPerfil'))+'\',\''+bE(registro.get('idVacante'))+'\')"><img height="13" width="13" src="../images/magnifier.png" title="Ver perfil de candidato a puesto" alt="Ver perfil de candidato a puesto"></a>';
                                                                        }
                                                                        else
                                                                        {
                                                                        	return '<a href="javascript:enviarRegistroPerfil(\''+bE(registro.get('idUnidadPuesto'))+'\',\''+bE('114')+'\',\''+bE(idRegistro)+'\',\''+bE('agregar')+'\',\''+bE(registro.get('idFormularioPerfil'))+'\',\''+bE(registro.get('idVacante'))+'\')"><img height="13" width="13" src="../images/page_edit2.png" title="Crear perfil de candidato a puesto" alt="Crear perfil de candidato a puesto"></a>';
                                                                        }
                                                                    }
														},
                                                        {
															header:'Situaci&oacute;n',
															width:300,
															sortable:true,
															dataIndex:'etapa',
                                                            renderer:function (val)
                                                            		{
                                                                    	if(val!='')
                                                                        {
                                                                        	return formatearValorRenderer(arrEtapas,val);
                                                                        }
                                                                        return val;
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridVacantes',
                                                            store:alDatos,
                                                            frame:true,
															renderTo:'tblVacantes',
                                                            cm: cModelo,
                                                            height:400,
                                                            width:870
                                                            
                                                        }
                                                    );
    
}

function enviarRegistroPerfil(iU,a,iR,acc,iF,iV)
{
	var arrParam=[['idRegistro',bD(iR)],['idFormulario',bD(iF)],['dComp',acc],['actor',a],['idReferencia',bD(iU)],['funcPHPEjecutarNuevo',bE('vincularVacantePerfil('+bD(iV)+',idRegPadre,"'+gE('qy').value+'");')]];
    window.open('',"vAuxiliar", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
    enviarFormularioDatos('../modeloPerfiles/vistaDTD.php',arrParam,'POST','vAuxiliar');

}

function recargarGridVacantes(arrDatos)
{
	gEx('gridVacantes').getStore().loadData(arrDatos);
}

function funcionAntesCerrar()
{
	recargarPagina();
}

function regresar1PaginaContenedor()
{}