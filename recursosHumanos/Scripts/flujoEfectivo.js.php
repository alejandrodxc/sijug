<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$conCalculos="SELECT idConsulta,nombreConsulta FROM 991_consultasSql WHERE idTipoConcepto<=2 ORDER BY nombreConsulta";
	$arrCalc=$con->obtenerFilasArreglo($conCalculos);
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridConceptos();
}

function recargarGrid()
{
	var cmb=gE('idCiclo');
    var opcion=cmb.options[cmb.selectedIndex].value;
    if(opcion=='-1')
    	return;
    gE('hIdCiclo').value=opcion;
    //var almacen=Ext.getCmp('gridFlujo').getStore();
    //almacen.reload();
    var arrP=[['idCiclo',opcion]];
	enviarFormularioDatos('../recursosHumanos/flujoEfectivo.php',arrP);
}

var arregloNombreMeses=new Array();
arregloNombreMeses[0]='Ene';
arregloNombreMeses[1]='Feb';
arregloNombreMeses[2]='Mar';
arregloNombreMeses[3]='Abr';
arregloNombreMeses[4]='May';
arregloNombreMeses[5]='Jun';
arregloNombreMeses[6]='Jul';
arregloNombreMeses[7]='Ago';
arregloNombreMeses[8]='Sep';
arregloNombreMeses[9]='Oct';
arregloNombreMeses[10]='Nov';
arregloNombreMeses[11]='Dic';

var arregloMeses=new Array();

	var chkRow=new Ext.grid.CheckboxSelectionModel();
    arregloMeses.push(chkRow);
    arregloMeses.push(new  Ext.grid.RowNumberer());
    arregloMeses.push(  {
                            header:'Partida',
                            width:80,
                            sortable:true,
                            dataIndex:'partida',
                            align:'left'
                        }
                        );
     arregloMeses.push( {
                          header:'Concepto',
                          width:300,
                          sortable:true,
                          dataIndex:'nombre',
                          align:'left'
                        }
                      );

var z;
for(z=0;z< 12;z++)
{
	var obj={
                header:arregloNombreMeses[z],
                width:80,
                sortable:true,
                dataIndex:'mes_'+z,
                align:'right',
                renderer:'usMoney',
                editor:{xtype:'numberfield'}
            };
    arregloMeses.push(obj);
}

	arregloMeses.push( {
                          header:'Total',
                          width:100,
                          sortable:true,
                          dataIndex:'total',
                          align:'right',
                          renderer:'usMoney'
                        }
                      );
    arregloMeses.push( {
                          header:'Tipo de concepto',
                          width:100,
                          sortable:true,
                          dataIndex:'tipoConcepto',
                          align:'right',
                          renderer:function(val,meta,registro)
                          			{	
                                        if(val==1)
                                        	return 'Ingreso';
                                        else
                                        	return 'Egreso'    ;
                                    }
                        }
                      );                  

function crearGridConceptos()
{
    var guardado=gE('registrado').value;
    var arregloBotones=new Array();
    if(guardado==0)
    {
    	arregloBotones.push(
                              {
                                text:'Registrar',
                                icon:'../images/icon_big_tick.gif',
                                cls:'x-btn-text-icon',
                                handler:function()
                                        {
                                            registrar();
                                        }
                              }
                           );
        arregloBotones.push(
                              {
                                text:'Agregar Concepto',
                                icon:'../images/add.png',
                                cls:'x-btn-text-icon',
                                handler:function()
                                        {
                                            agregarConcepto();
                                        }
                              }
                           );
                           
        arregloBotones.push(
                              {
                                text:'Modificar Concepto',
                                icon:'../images/pencil.png',
                                cls:'x-btn-text-icon',
                                handler:function()
                                        {
                                            modificarConcepto();
                                        }
                              }
                           );         
        arregloBotones.push(
                              {
                                text:'Remover Concepto',
                                icon:'../images/cancel_round.png',
                                cls:'x-btn-text-icon',
                                handler:function()
                                        {
                                            removerConcepto();
                                        }
                              }
                           );                   
    }
    
    
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                               			{name: 'idCatalogoConcepto'},
                                                                  {name: 'nombre'},
                                                                  {name: 'partida'},
                                                                  {name: 'nombrePart'},
                                                                  {name: 'tipoEntrada'},
                                                                  {name: 'puedeModificar'},
                                                                  {name: 'tipoConcepto'},
                                                                  {name: 'idCalculo'},
                                                                  {name: 'nomCalc'},
                                                                  {name: 'mes_0'},
                                                                  {name: 'mes_1'},
                                                                  {name: 'mes_2'},
                                                                  {name: 'mes_3'},
                                                                  {name: 'mes_4'},
                                                                  {name: 'mes_5'},
                                                                  {name: 'mes_6'},
                                                                  {name: 'mes_7'},
                                                                  {name: 'mes_8'},
                                                                  {name: 'mes_9'},
                                                                  {name: 'mes_10'},
                                                                  {name: 'mes_11'},
                                                                  {name: 'total'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsConceptos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombre', direction: 'ASC'},
                                                            groupField: 'tipoConcepto'
                                                        })                                      
     
	dsConceptos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=1;
                                        proxy.baseParams.idCiclo=gE('hIdCiclo').value;
                                    }
                  );                  
                                                                      
   // var dsConceptos=new Ext.data.JsonStore({
//                                                        root: 'registros',
//                                                        totalProperty: 'numReg',
//                                                        fields: [
//                                                                  {name: 'idCatalogoConcepto'},
//                                                                  {name: 'nombre'},
//                                                                  {name: 'partida'},
//                                                                  {name: 'nombrePart'},
//                                                                  {name: 'tipoEntrada'},
//                                                                  {name: 'puedeModificar'},
//                                                                  {name: 'tipoConcepto'},
//                                                                  {name: 'idCalculo'},
//                                                                  {name: 'nomCalc'},
//                                                                  {name: 'mes_0'},
//                                                                  {name: 'mes_1'},
//                                                                  {name: 'mes_2'},
//                                                                  {name: 'mes_3'},
//                                                                  {name: 'mes_4'},
//                                                                  {name: 'mes_5'},
//                                                                  {name: 'mes_6'},
//                                                                  {name: 'mes_7'},
//                                                                  {name: 'mes_8'},
//                                                                  {name: 'mes_9'},
//                                                                  {name: 'mes_10'},
//                                                                  {name: 'mes_11'},
//                                                                  {name: 'total'}
//                                                              ],         
//                                                        proxy : new Ext.data.HttpProxy	(
//
//                                                                                          {
//
//                                                                                              url: '../paginasFunciones/funcionesAuxiliares.php'
//                                                                                          }
//                                                                                      )                             
//                                                    })
//	dsConceptos.on('beforeload',function(proxy)
//    								{
//                                    	proxy.baseParams.funcion=1;
//                                        proxy.baseParams.idCiclo=gE('idCiclo').value;
//                                    }
//                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombre' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'partida' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
                                                        arregloMeses
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridFlujo',
                                                            store:dsConceptos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            renderTo:'gridCon',
                                                            height:750,
                                                            width:1500,
                                                            tbar:arregloBotones,
                                                            plugins: [filters],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsConceptos.load()  ;
    tblGridP.on('beforeedit',funcAbeforeEdit);
    tblGridP.on('afteredit',funcAfterEdit);
    return tblGridP;     
}

var arregloTipoE=[['1','Calculo'],['2','Abierto']];
var arregloMod=[['1','Si'],['0','No']];
var arregloTipoC=[['1','Ingreso'],['2','Egreso']];

function agregarConcepto()
{
	 var cmb=gE('idCiclo');
     var idCiclo=cmb.options[cmb.selectedIndex].value;
     if(idCiclo==-1)
     {
     	msgBox('Debe seleccionar un ciclo');
        return;
     }
     
     //var idCiclo=gE('idCiclo').value;
     var cmbTipoC=crearComboExt('cmbTipoC',arregloTipoC,115,5,200);
     var comboTipoE=crearComboExt('comboTipoE',arregloTipoE,115,95,200);
     comboTipoE.on('select',mostrarCombo);
     var cmbMod=crearComboExt('cmbMod',arregloMod,115,155,200);
     cmbMod.hide();
     var cmbCalculo=crearComboExt('cmbCalculo',<?php echo $arrCalc?>,115,125,200);
     cmbCalculo.hide();
     var form1=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 1',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           {
                                                             x:10,
                                                             y:10,
                                                             xtype:'label',
                                                             html:'Tipo de Concepto:'
                                                           },
                                                            cmbTipoC
                                                           ,
                                                           {
                                                             x:65,
                                                             y:40,
                                                             xtype:'label',
                                                             html:'Nombre:'
                                                           },
                                                           {
                                                             x:115,
                                                             y:35,
                                                             xtype:'textfield',
                                                             id:'txtNombre',
                                                             width:200
                                                           },
                                                           {
                                                             x:69,
                                                             y:70,
                                                             xtype:'label',
                                                             html:'Partida:'
                                                           },
                                                           {
                                                             x:115,
                                                             y:65,
                                                             xtype:'textfield',
                                                             id:'txtPartida',
                                                             width:100,
                                                             disabled:true
                                                           },
                                                           {
                                                             x:220,
                                                             y:70,
                                                             xtype:'label',
                                                             html:'<a href="javascript:verPartidas(1)"><img height="13" width="13" src="../images/add.png" />Agregar Partida</a>'
                                                           },
                                                           {
                                                             x:37,
                                                             y:100,
                                                             xtype:'label',
                                                             html:'Tipo entrada:'
                                                           },
                                                           comboTipoE,
                                                           {
                                                             x:68,
                                                             y:130,
                                                             xtype:'label',
                                                             id:'txtCalculo',
                                                             html:'Calculo:',
                                                             hidden:true
                                                           },
                                                           cmbCalculo,
                                                           {
                                                             x:20,
                                                             y:160,
                                                             xtype:'label',
                                                             id:'txtModificar',
                                                             html:'Puede Modificar:',
                                                             hidden:true
                                                           },
                                                           cmbMod
                                                        ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Agregar Concepto',
										width:370,
										height:250,
										layout:'fit',
										buttonAlign:'center',
										items:[form1],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var tipoC=Ext.getCmp('cmbTipoC').getValue();
                                                                    if(tipoC=='')
                                                                    {
                                                                    	msgBox('Debe indicar el tipo de concepto');
                                                                        Ext.getCmp('cmbTipoC').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var nombre=Ext.getCmp('txtNombre').getValue();
                                                                    if(nombre=='')
                                                                    {
                                                                    	msgBox('Debe indicar el nombre del concepto');
                                                                        Ext.getCmp('txtNombre').focus();
                                                                        return;
                                                                    }
                                                                    var partida=Ext.getCmp('txtPartida').getValue();
                                                                    
                                                                    var tipoE=Ext.getCmp('comboTipoE').getValue();
                                                                    if(tipoE=='')
                                                                    {
                                                                    	msgBox('Debe indicar el tipo de entrada del concepto');
                                                                        Ext.getCmp('comboTipoE').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var modif='-1';
                                                                    var idCal='-1';
                                                                    if(tipoE==1)
                                                                    {
                                                                    	idCal=Ext.getCmp('cmbCalculo').getValue();
                                                                        if(idCal=='')
                                                                        {
                                                                            msgBox('Debe seleccionar un calculo');
                                                                            Ext.getCmp('cmbCalculo').focus();
                                                                            return;
                                                                        }
                                                                        
                                                                        modif=Ext.getCmp('cmbMod').getValue();
                                                                        if(modif=='')
                                                                        {
                                                                            msgBox('Debe indicar si puede modificar');
                                                                            Ext.getCmp('cmbMod').focus();
                                                                            return;
                                                                        }
                                                                    }
                                                                    
                                                                    function funcAjax()
                                                                    {
                                                                        var resp=peticion_http.responseText;
                                                                        arrResp=resp.split('|');
                                                                        if(arrResp[0]=='1')
                                                                        {
                                                                            var almacen=Ext.getCmp('gridFlujo').getStore();
    																		almacen.reload();
                                                                        }
                                                                        else
                                                                        {
                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                        }
                                                                    }
                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=2&idCiclo='+idCiclo+'&tipoC='+tipoC+'&nombre='+nombre+'&partida='+partida+'&tipoE='+tipoE+'&modif='+modif+'&idCal='+idCal,true);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function verPartidas(bandera)
{
	var gridP=crearGridPartidas();
    var form2=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 2',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           gridP
                                                        ]
                                              }
                                          )
    
    var ventana=new Ext.Window(
							   		{
										title:'Agregar partida',
										width:550,
										height:500,
										layout:'fit',
										buttonAlign:'center',
										items:[form2],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var fila=Ext.getCmp('gridPartidas').getSelectionModel().getSelected();
                                                                    if(fila==undefined)
                                                                    {
                                                                    	msgBox('Debe seleccionar una partida');
                                                                        return;
                                                                    }
                                                                    
                                                                    var clave=fila.get('clave');
                                                                    if(bandera==1)
                                                                    {
                                                                       Ext.getCmp('txtPartida').setValue(clave);
                                                                    }
                                                                    else
                                                                    {
                                                                    	Ext.getCmp('txtPartidaM').setValue(clave);
                                                                    }
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function mostrarCombo(combo,fila,pos)
{
	var valor=combo.getValue();
    if(valor==1)
    {
    	Ext.getCmp('cmbMod').show();
        Ext.getCmp('txtModificar').show();
        Ext.getCmp('cmbCalculo').show();
        Ext.getCmp('txtCalculo').show();
    }
    else
    {
    	Ext.getCmp('cmbMod').hide();
        Ext.getCmp('cmbMod').setValue('');
        Ext.getCmp('txtModificar').hide();
        Ext.getCmp('cmbCalculo').hide();
        Ext.getCmp('cmbCalculo').setValue('');
        Ext.getCmp('txtCalculo').hide();
    }
}

function crearGridPartidas()
{
    var dsPartidas=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'clave'},
                                                                  {name: 'nombreObjetoGasto'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(
                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsPartidas.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=3;
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'clave' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreObjetoGasto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
                                                                 
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:35}),
                                                        chkRow,
														{
															header:'Clave',
															width:50,
															sortable:true,
															dataIndex:'clave',
                                                            align:'left'
														},
                                                        {
															header:'Partida',
															width:400,
															sortable:true,
															dataIndex:'nombreObjetoGasto'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPartidas',
                                                            x:0,
                                                            y:0,
                                                            store:dsPartidas,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:430,
                                                            sm:chkRow,
                                                            width:530,
                                                            plugins: [filters]
                                                        }
                                                    );
		
    dsPartidas.load()  ;
    return tblGridP;     
}

function removerConcepto()
{
	var filas=Ext.getCmp('gridFlujo').getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox('Debe seleccionar al menos un concepto');
        return;
    }
    
    var x;
    var cadena='';
    for(x=0;x<filas.length;x++)
    {
    	var id=filas[x].get('idCatalogoConcepto');
        if(cadena=='')
        	cadena=id;
        else
        	cadena+=','+id;    
    }
    
    function respPregunta(btn)
	{
		if(btn=='yes')
		{
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    var almacen=Ext.getCmp('gridFlujo').getStore();
                    almacen.reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=5&cadena='+cadena,true);
        }
        else
        {
        	return;
        }
    }
    Ext.MessageBox.confirm(lblAplicacion,'Est&aacute; seguro de eliminar los elementos seleccionados',respPregunta);   
}

function funcAbeforeEdit(e)
{
	var registrado=gE('registrado').value;
    if(registrado==0)
    {
        var editar=e.record.get('tipoEntrada');
        if((editar==1) && (e.record.get('puedeModificar')!=1))
        {
            e.cancel=true;
        }
    }
    else
    {
    	e.cancel=true;
    }    
}

function funcAfterEdit(e)
{
	var cadena=e.field;
    var arrC=cadena.split('_');
    var mes=arrC[1];
    if(e.value==='')
    {
    	msgBox('El valor ingresado no es valido');
        e.record.set('mes_'+mes,e.originalValue);
        return;
    }
    var idTabla=e.record.get('idCatalogoConcepto');
    var valor=e.value;
    var valorInicio=parseFloat(e.originalValue);
    var total=parseFloat(e.record.get('total'));
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
           var nuevoTotal=0;//total-valorInicio;
           //nuvoTotal+valor;
           var x;
           for(x=0;x<12;x++)
           {
           	   var valorM=parseFloat(e.record.get('mes_'+x));
               nuevoTotal=nuevoTotal+valorM;
           }
           e.record.set('total',nuevoTotal);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=6&mes='+mes+'&idTabla='+idTabla+'&valor='+valor,true);
}

function registrar()
{
    var cmb=gE('idCiclo');
    var idCiclo=cmb.options[cmb.selectedIndex].value;
    if(idCiclo==-1)
    {
      msgBox('Debe seleccionar un ciclo');
      return;
    }
    //var idCiclo=gE('idCiclo').value;
    function respPregunta(btn)
	{
		if(btn=='yes')
		{
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    //var almacen=Ext.getCmp('gridFlujo').getStore();
                    //almacen.reload();
                    recargarPagina()
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=4&idCiclo='+idCiclo,true);
        }
        else
        {
        	return;
        }
    }
    Ext.MessageBox.confirm(lblAplicacion,'Una vez registrado este concentrado no podra modificarlo<br />Est&aacute; seguro de registrarlo ',respPregunta);         
}


function modificarConcepto()
{
	var cmb=gE('idCiclo');
     var idCiclo=cmb.options[cmb.selectedIndex].value;
     if(idCiclo==-1)
     {
     	msgBox('Debe seleccionar un ciclo');
        return;
     }
    //var idCiclo=gE('idCiclo').value;
    var filas=Ext.getCmp('gridFlujo').getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox('Debe seleccionar al menos un concepto');
        return;
    }
    
    if(filas.length>1)
    {
    	msgBox('Debe seleccionar solo un concepto');
        return;
    }
    
    var x;
    var cadena='';
    for(x=0;x<filas.length;x++)
    {
    	var id=filas[x].get('idCatalogoConcepto');
        if(cadena=='')
        	cadena=id;
        else
        	cadena+=','+id;    
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            ventanaModificar(arrResp[1],arrResp[2],arrResp[3],arrResp[4],arrResp[5],arrResp[6],arrResp[7]);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=9&id='+id,true);
}

function ventanaModificar(id,nombre,partida,tipoEntrada,puedeMod,tipoConcepto,idCalculo)
{
     var cmbTipoCM=crearComboExt('cmbTipoCM',arregloTipoC,115,5,200);
     cmbTipoCM.setValue(tipoConcepto);
     
     var comboTipoEM=crearComboExt('comboTipoEM',arregloTipoE,115,95,200);
     comboTipoEM.setValue(tipoEntrada);
     comboTipoEM.on('select',mostrarCombo2);
     
     var cmbModM=crearComboExt('cmbModM',arregloMod,115,155,200);
     cmbModM.setValue(puedeMod);
     cmbModM.hide();
     
     var cmbCalculoM=crearComboExt('cmbCalculoM',<?php echo $arrCalc?>,115,125,200);
     cmbCalculoM.setValue(idCalculo.trim());
     cmbCalculoM.hide();
     
     var form3=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 3',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           {
                                                             x:10,
                                                             y:10,
                                                             xtype:'label',
                                                             html:'Tipo de Concepto:'
                                                           },
                                                            cmbTipoCM
                                                           ,
                                                           {
                                                             x:65,
                                                             y:40,
                                                             xtype:'label',
                                                             html:'Nombre:'
                                                           },
                                                           {
                                                             x:115,
                                                             y:35,
                                                             xtype:'textfield',
                                                             id:'txtNombreM',
                                                             value:nombre,
                                                             width:200
                                                           },
                                                           {
                                                             x:69,
                                                             y:70,
                                                             xtype:'label',
                                                             html:'Partida:'
                                                           },
                                                           {
                                                             x:115,
                                                             y:65,
                                                             xtype:'textfield',
                                                             id:'txtPartidaM',
                                                             width:100,
                                                             disabled:true,
                                                             value:partida
                                                           },
                                                           {
                                                             x:220,
                                                             y:70,
                                                             xtype:'label',
                                                             html:'<a href="javascript:verPartidas(2)"><img height="13" width="13" src="../images/add.png" />Agregar Partida</a>'
                                                           },
                                                           {
                                                             x:37,
                                                             y:100,
                                                             xtype:'label',
                                                             html:'Tipo entrada:'
                                                           },
                                                           comboTipoEM,
                                                           {
                                                             x:68,
                                                             y:130,
                                                             xtype:'label',
                                                             id:'txtCalculoM',
                                                             html:'Calculo:',
                                                             hidden:true
                                                           },
                                                           cmbCalculoM,
                                                           {
                                                             x:20,
                                                             y:160,
                                                             xtype:'label',
                                                             id:'txtModificarM',
                                                             html:'Puede Modificar:',
                                                             hidden:true
                                                           },
                                                           cmbModM
                                                        ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Agregar Concepto',
										width:370,
										height:250,
										layout:'fit',
										buttonAlign:'center',
										items:[form3],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var tipoC=Ext.getCmp('cmbTipoCM').getValue();
                                                                    if(tipoC=='')
                                                                    {
                                                                    	msgBox('Debe indicar el tipo de concepto');
                                                                        Ext.getCmp('cmbTipoCM').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var nombre=Ext.getCmp('txtNombreM').getValue();
                                                                    if(nombre=='')
                                                                    {
                                                                    	msgBox('Debe indicar el nombre del concepto');
                                                                        Ext.getCmp('txtNombreM').focus();
                                                                        return;
                                                                    }
                                                                    var partida=Ext.getCmp('txtPartidaM').getValue();
                                                                    
                                                                    var tipoE=Ext.getCmp('comboTipoEM').getValue();
                                                                    if(tipoE=='')
                                                                    {
                                                                    	msgBox('Debe indicar el tipo de entrada del concepto');
                                                                        Ext.getCmp('comboTipoEM').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var modif='-1';
                                                                    var idCal='-1';
                                                                    if(tipoE==1)
                                                                    {
                                                                    	idCal=Ext.getCmp('cmbCalculoM').getValue();
                                                                        if(idCal=='')
                                                                        {
                                                                            msgBox('Debe seleccionar un calculo');
                                                                            Ext.getCmp('cmbCalculoM').focus();
                                                                            return;
                                                                        }
                                                                        
                                                                        modif=Ext.getCmp('cmbModM').getValue();
                                                                        if(modif=='')
                                                                        {
                                                                            msgBox('Debe indicar si puede modificar');
                                                                            Ext.getCmp('cmbModM').focus();
                                                                            return;
                                                                        }
                                                                    }
                                                                    
                                                                    function funcAjax()
                                                                    {
                                                                        var resp=peticion_http.responseText;
                                                                        arrResp=resp.split('|');
                                                                        if(arrResp[0]=='1')
                                                                        {
                                                                            var almacen=Ext.getCmp('gridFlujo').getStore();
    																		almacen.reload();
                                                                        }
                                                                        else
                                                                        {
                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                        }
                                                                    }
                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=10&idCiclo='+idCiclo+'&tipoC='+tipoC+'&nombre='+nombre+'&partida='+partida+'&tipoE='+tipoE+'&modif='+modif+'&idCal='+idCal+'&id='+id,true);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
   if(tipoEntrada==1)
   {
      cmbModM.show();
      Ext.getCmp('txtCalculoM').show();
      cmbCalculoM.show();
      Ext.getCmp('txtModificarM').show();
   }
   ventana.show();
}

function mostrarCombo2(combo,fila,pos)
{
	var valor=combo.getValue();
    if(valor==1)
    {
    	Ext.getCmp('cmbModM').show();
        Ext.getCmp('txtModificarM').show();
        Ext.getCmp('cmbCalculoM').show();
        Ext.getCmp('txtCalculoM').show();
    }
    else
    {
    	Ext.getCmp('cmbModM').hide();
        Ext.getCmp('cmbModM').setValue('');
        Ext.getCmp('txtModificarM').hide();
        Ext.getCmp('cmbCalculoM').hide();
        Ext.getCmp('cmbCalculoM').setValue('');
        Ext.getCmp('txtCalculoM').hide();
    }
}