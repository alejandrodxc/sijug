<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
?>

Ext.onReady(inicializar);
var ocultarBtn=false;

function inicializar()
{
	
    if(gE('idRopaje').value!='-1')
    	ocultarBtn=true;
    crearGridConcentrado();
}

function crearGridConcentrado()
{
	var dsDatos=eval(bD(gE('arrRegistros').value));
    
 var lector=	new Ext.data.ArrayReader	(
                                                    {
                                                        fields:	[
                                                                    {name: 'departamento'},
                                                                    {name: 'prenda'},
                                                                     {name: 'tamano'},
                                                                     {name: 'costo'},
                                                                     {name: 'cantidad'},
                                                                     {name: 'total'}
                                                                ]
                                                    }
                                                );

    
    var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            groupField: 'departamento',
                                                            autoLoad:true,
                                                            data:dsDatos
                                                        }) 
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.HybridSummary();
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Departamento',
															width:250,
															sortable:true,
															dataIndex:'departamento'
														},
														{
															header:'Prenda',
															width:150,
															sortable:true,
															dataIndex:'prenda'
														},
                                                        {
															header:'Tama&ntilde;o',
															width:80,
															sortable:true,
															dataIndex:'tamano'
														},
                                                        
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'Costo',
															width:100,
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney',
                                                            summaryRenderer:function()
                                                            				{
                                                                            	return "<b>Costo total:</b>";
                                                                            }
														},
                                                        {
                                                        	header:'Total',
															width:150,
															sortable:true,
															dataIndex:'total',
                                                            summaryType:'sum',
                                                            renderer:'usMoney'
                                                        }
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                           	renderTo:'tblRegistros',
                                                            cm: cModelo,
                                                            height:460,
                                                            width:850,
                                                            sm:chkRow,
                                                            plugins:[summary],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Enviar pedido a compra',
                                                                            hidden:ocultarBtn,
                                                                            handler:function()
                                                                            		{
                                                                                    	function respAux(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        recargarPagina();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=39&ciclo='+gE('ciclo').value,true);
		
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer realizar el pedido para compra?',respAux)
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	return 	tblGrid;	

}