<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	var registrado=gE('registrado').value;
    if(registrado=='-1')
    {
    	mE('envioC');
        oE('leyenda')
    }
    else
    {
    	mE('leyenda');
        oE('envioC');
    }
    crearGrid();
}	

function crearGrid()
{
    var dsPrendas=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPrenda'},
                                                                  {name: 'nomPren'},
                                                                  {name: 'idColor'},
                                                                  {name: 'nColor'},
                                                                  {name: 'talla'},
                                                                  {name: 'cantidad'},
                                                                  {name: 'costo'},
                                                                  {name: 'idProducto'},
                                                                  {name: 'total'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesPlaneacion.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsPrendas.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=122;
                                        proxy.baseParams.idCiclo=gE('idCiclo').value;
                                    }
                        );
    var summary = new Ext.ux.grid.GridSummary();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Prenda',
															width:280,
															sortable:true,
															dataIndex:'nomPren',
                                                            align:'left'
														},
                                                        {
															header:'Color',
															width:100,
                                                            align:'left',
															sortable:true,
															dataIndex:'nColor'
                                                        },
                                                        {
															header:'Talla',
															width:100,
                                                            align:'left',
															sortable:true,
															dataIndex:'talla'
														},
                                                        {
															header:'No. Prendas',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidad'
                                                        },
                                                        {
															header:'Costo Unit.',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney'
                                                        },
                                                        {
															header:'Costo Total',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney',
                                                            summaryType:'sum'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPrendas',
                                                            title:'Concentrado de prendas',
                                                            store:dsPrendas,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'gridC',
                                                            height:650,
                                                            width:830,
                                                            plugins:[summary]
                                                        }
                                                    );
		
    dsPrendas.load()  ;
    return grid;     
}


function verDeptos(bandera)//1 registrado, -1 falta registro;
{
	var inst=gE('codInst').value;
    if(bandera==1)
    	var mensaje='Departamentos/&Aacute;reas con deteccion de necesidades registrada';
    else
    	var mensaje='Departamentos/&Aacute;reas sin deteccion de necesidades registrada';    
        
    var gridDeptos=crearGridDeptos(bandera);
    var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
                                                            {
                                                            	xtype:'panel',
                                                                x:0,
                                                                y:0,
                                                                items:
                                                                [
                                                                	gridDeptos
                                                                ]
                                                            }
														]
											}
										);
	
	ventana = new Ext.Window	(
									{
										title:mensaje,
										width: 650,
										height:550,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										listeners : {
														
													},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);
    ventana.show();
}


function crearGridDeptos(bandera)
{
    var dsDeptos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'codigoDepto'},
                                                                  {name: 'unidad'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesPlaneacion.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsDeptos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=123;
                                        proxy.baseParams.bandera=bandera;
                                    }
                        );
   
    
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'codigoDepto' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'unidad' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:35}),
														{
															header:'Codigo Depto',
															width:100,
															sortable:true,
															dataIndex:'codigoDepto',
                                                            align:'left'
														},
                                                        {
															header:'Departamento',
															width:450,
                                                            align:'left',
															sortable:true,
															dataIndex:'unidad'
                                                        }
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridDepartamentos',
                                                            store:dsDeptos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:470,
                                                            width:620,
                                                            plugins: [filters]
                                                        }
                                                    );
		
    dsDeptos.load()  ;
    return tblGridP;     
}

function registrarConcentrado()
{
	var idCiclo=gE('idCiclo').value;
    var almacen=Ext.getCmp('gridPrendas').getStore() ;
    var tamano=almacen.getCount();
    var x;
    var cadena='';
   
    for(x=0;x< tamano;x++)
    {
    	var reg=almacen.getAt(x);
        var obj=reg.get('idProducto')+'_'+reg.get('cantidad')+'_'+reg.get('costo')+'_'+reg.get('total')+'_'+reg.get('nColor')+'_'+reg.get('talla');
        
        if(cadena=='')
        	cadena=obj;
        else
        	cadena+='|'+obj; 
    }
    
    function funcAjax2()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
            oE('envioC');
            mE('leyenda');
        }
        else
        {
             Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesPlaneacion.php',funcAjax2, 'POST','funcion=125&idCiclo='+idCiclo+'&cadena='+cadena,true)
}
