<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="select distinct ciclo,ciclo FROM 672_nominasEjecutadas n,671_asientosCalculosNomina a,703_relacionFoliosCFDI r WHERE  n.idNomina=a.idNomina AND idUsuario=".$_SESSION["idUsr"]." AND idComprobante IS NOT NULL
				and r.idFolio=a.idComprobante and r.situacion=2 order by ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
?>

var arrCiclo=<?php echo $arrCiclo?>;
Ext.onReady(inicializar);

function inicializar()
{
	var cmbCicloFiscal=crearComboExt('cmbCicloFiscal',arrCiclo,0,0,110);
    if(arrCiclo.length>0)
    {
    	cmbCicloFiscal.setValue(arrCiclo[arrCiclo.length-1][0]);
    }
    
    cmbCicloFiscal.on('select',function(cmb,registro)
    							{
                                	gEx('gridRecibos').getStore().load(
                                    										{
                                                                            	url: '../paginasFunciones/funcionesRecursosHumanos.php',
                                                                                parms:	{
                                                                                			funcion:66,
                                                                                            ciclo:gEx('cmbCicloFiscal').getValue()	
                                                                                		}	
                                                                            }
                                    									)
                                }
    				)
    
     var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAsiento'},
		                                                {name: 'idNomina'},
		                                                {name:'totalPercepciones'},
                                                        {name: 'totalDeducciones'},
                                                        {name: 'sueldoNeto'},
                                                        {name: 'idComprobante'},
                                                        {name: 'descripcion'},
                                                        {name: 'folioNomina'},
                                                        {name: 'plantel'},
                                                        {name: 'periodo'},
                                                        {name: 'ordinal'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
	var chkRow=new Ext.grid.CheckboxSelectionModel();     
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesRecursosHumanos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'ordinal', direction: 'ASC'},
                                                            groupField: 'ordinal',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='66';
                                        proxy.baseParams.ciclo=gEx('cmbCicloFiscal').getValue();
                                    }
                        )   
       
       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'idComprobante',
                                                                renderer:formatearRecibo
                                                            },
                                                            {
                                                                header:'Periodo',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'ordinal',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return registro.data.periodo;
                                                                        }
                                                            },
                                                             {
                                                                header:'Plantel',
                                                                width:160,
                                                                sortable:true,
                                                                dataIndex:'plantel',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Folio de n&oacute;mina',
                                                                width:95,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'folioNomina'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:160,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Total percepciones',
                                                                width:110,
                                                                sortable:true,
                                                                renderer:'usMoney',
                                                                css:'text-align:right;',
                                                                dataIndex:'totalPercepciones'
                                                            },
                                                            {
                                                                header:'Total deducciones',
                                                                width:110,
                                                                sortable:true,
                                                                renderer:'usMoney',
                                                                css:'text-align:right;',
                                                                dataIndex:'totalDeducciones'
                                                            },
                                                            {
                                                                header:'Sueldo Neto',
                                                                width:90,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                renderer:'usMoney',
                                                                dataIndex:'sueldoNeto'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridRecibos',
                                                                store:alDatos,
                                                                renderTo:'tblRecibos',
                                                                frame:true,
                                                                cm: cModelo,
                                                                width:880,
                                                                sm:chkRow,
                                                                height:450,
                                                                frame:false,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true, 
                                                                tbar:
                                                                	[
                                                                    	{
                                                                        
                                                                        	xtype:'label',
                                                                            html:'<b><span style="color:#000"><b>Ciclo:</b></span></b>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbCicloFiscal,'-',
                                                                        {
                                                                        	icon:'../images/download.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDescargarXML',
                                                                            hidden:false,
                                                                            text:'Descargar XML/PDF seleccionados',
                                                                            handler:function()
                                                                            		{
																						var arrFilas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(arrFilas.length==0)
																						{
                                                                                        	msgBox('Debe seleccionar los elementos cuyos XML/PDF desea obtener');
                                                                                        	return;
                                                                                        }
                                                                                        var listaComprobante='';
                                                                                        var x;
                                                                                        var fila;
                                                                                        for(x=0;x<arrFilas.length;x++)
                                                                                        {
                                                                                        	fila=arrFilas[x];
                                                                                            if(listaComprobante=='')
                                                                                            	listaComprobante=fila.data.idComprobante;
                                                                                            else
                                                                                            	listaComprobante+=','+fila.data.idComprobante;
                                                                                            
                                                                                        }
                                                                                        var arrParam=[['c',bE(listaComprobante)]];
                                                                                    	enviarFormularioDatos('../nomina/descargarComprobantesUsuario.php',arrParam);
                                                                                    }
                                                                            
                                                                        }
                                                                	],                                                               
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function formatearRecibo(val,meta,registro)
{
	var comp='';
    var comp2='';
    
    if((registro.data.idComprobante!='')&&(registro.data.situacionComprobante!='')&&(registro.data.situacionComprobante!='3')&&(registro.data.situacionComprobante!='5'))
    {
    	comp='<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(val)+'"><img src="../images/Icono_xml.gif" title="Obtener XML del CFDI" alt="Obtener XML del CFDI" width="16" height="16"></a> ';
    }
	if((registro.data.idComprobante!='')&&(registro.data.situacionComprobante!='3')&&(registro.data.situacionComprobante!='')&&(registro.data.situacionComprobante!='5'))
    {
		comp2='<a href="javascript:mostrarReciboNomina(\''+bE(val)+'\')"><img src="../images/vcard.png" width="16" height="16" title="Ver recibo de n&oacute;mina" alt="Ver recibo de n&oacute;mina"></a>';
    }
    return comp+comp2;
}

function mostrarReciboNomina(iE)
{
	var obj={};
    obj.titulo='Recibo de N&oacute;mina';
    obj.ancho=900;
    obj.alto=450;
    obj.params=[['iC',iE],['cPagina','sFrm=true']];
    obj.url='../formatosFacturasElectronicas/vistaPreviaReciboNomina.php';
    obj.openEffect='fade';
    if(window.parent)
    	window.parent.abrirVentanaFancy(obj);
    else
    	abrirVentanaFancy(obj);
}	