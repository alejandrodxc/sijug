<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	include_once("latis/diccionarioTerminos.php");
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares where situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);
	$consulta="SELECT id__464_gridPeriodos,nombrePeriodo FROM _464_gridPeriodos ";
	$arrPeriodos=$con->obtenerFilasArreglo($consulta);
?>

Ext.onReady(inicializar);

function inicializar()
{
	var arrCiclo=<?php echo $arrCiclo?>;
    var arrPeriodo=<?php echo $arrPeriodos?>;//eval(gE('arrPeriodos').value);
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,90);
    cmbCiclo.on('select',function(cmb,registro)
    					{
                        	gEx('gridContratos').getStore().reload();
                            
                        }
    			)
    cmbCiclo.setValue('<?php echo $cicloActivo?>')
	var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodo,0,0,190);   
    cmbPeriodo.on('select',function(cmb,registro)
    					{
                        	gEx('gridContratos').getStore().reload();
                        }
    			) 
    cmbPeriodo.setValue(arrPeriodo[0][0]);
    
    var alDatos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                       
                                                        fields:	[
                                                                    {name: 'idUsuario'},
                                                                    {name: 'Nombre'},
                                                                    {name: 'noMateriasEscolarizado'},
                                                                    {name: 'noMateriasAbierto'},
                                                                    {name: 'datosMateria'}
                                                                ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
																							
                                                                                              url: '../paginasFunciones/funcionesRecursosHumanos.php'
                                                                                          }
                                                                                      )                             
                                                    })
	alDatos.on('beforeload',function(proxy)
    								{
                                    	function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                if(arrResp[1]=='3')
                                                {
                                                	gEx('btnContrato').enable();
                                                    gEx('lblNoContrato').hide();
                                                    
                                                }
                                                else
                                                {
                                                	gEx('btnContrato').disable();
                                                    gEx('lblNoContrato').show();
                                                }
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=57&idCiclo='+cmbCiclo.getValue()+'&idPeriodo='+cmbPeriodo.getValue(),true);

                                    	proxy.baseParams.funcion=18;
                                        proxy.baseParams.idCiclo=cmbCiclo.getValue();
                                        proxy.baseParams.idPeriodo=cmbPeriodo.getValue();

                                    }
                        );
    
    var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'Nombre'},
                                                        				{type: 'numeric', dataIndex: 'noMaterias'}
                                                                    ]

                                                    }

                                                ); 
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<table ><tr><td  height="10">{datosMateria}</td></tr></table>'
                                                )
                                            });
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                        expander,
														{
															header:'Docente',
															width:400,
															sortable:true,
															dataIndex:'Nombre'
														},
														{
															header:'# <?php echo $dic["materia"]["p"]["et"]?> escolarizado',
															width:180,
															sortable:true,
                                                            align:'center',
															dataIndex:'noMateriasEscolarizado'
														}
                                                        ,
														{
															header:'# <?php echo $dic["materia"]["p"]["et"]?> NO escolarizado',
															width:180,
															sortable:true,
                                                            align:'center',
															dataIndex:'noMateriasAbierto'
														}
													]
												);
                                            
                                                                        
                                                                        
                                                                        
                                                                            
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridContratos',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            plugins:[expander,filters],
                                                            tbar:	[
                                                            			
                                                            			{
                                                                        	icon:'../images/page_white_stack.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            id:'btnContrato',
                                                                            text:'Generar contrato',
                                                                            handler:function()
                                                                            		{
																						var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos un docente para generar el contrato')
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                        var listadoDocentes=obtenerListadoArregloFilas(filas,'idUsuario');
                                                                                        tblGrid.getStore().remove(filas);
                                                                                        var arrParam=[['listContratos',listadoDocentes],['idCiclo',gEx('cmbCiclo').getValue()],['idPeriodo',gEx('cmbPeriodo').getValue()]];
                                                                                        enviarFormularioDatos('../reportes/CDA.php',arrParam);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            id:'lblNoContrato',
                                                                            html:'<span style="color:#F00"><b>No puede generar alg&uacute;n contrato hasta que todos sus <?php echo strtolower($dic["planEstudio"]["p"]["et"])?> hayan sido autorizados por rector&iacute;a</b></span>'
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load();     
    
    new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:[
                                                    {
                                                          xtype:'label',
                                                          html:'<span class="letraRojaSubrayada8" style="font-size:14px"><b>Docentes en espera de contrato</b></span>'
                                                      }
                                                  ],
                                            items:	[	
                                            			{
                                                            xtype:'panel',
                                                            region:'center',
                                                            layout:'border',
                                                            tbar:[
                                                                   
                                                                      {
                                                                            xtype:'label',
                                                                            html:'<span class="letraRojaSubrayada8"><b>Ciclo:</b></span>&nbsp;&nbsp;'
                                                                        },cmbCiclo,'-',
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8"><b>Periodo:</b></span>&nbsp;&nbsp;'
                                                                        },'-',
                                                                        cmbPeriodo
                                                                  ],
                                                            items:	[
                                                                        tblGrid
                                                                    ]
                                                        }
                                            		]
                                        }
                            			
                                     ]
						}
                    )   
    
   
}
