<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

function mostrarVentanaIncidencias(tI,arrCeldas,i)
{
	tIncidencia='';

    switch(tI)
    {
    	case '1':
        case 1:
        	tIncidencia='1';
            
        break;
        case '2':
        case 2:
        	tIncidencia='17';
            
        break;
        case '22':
        case 22:
        	tIncidencia='16';
            
        break;
        case '9':
        case 9:
        	tIncidencia='2';
            
        break;
        case 10:
        case '10':
        	tIncidencia='7';
            
        break;
        case '11':
        case 11:
        	tIncidencia='8';
            
        break;
        case '12':
        case 12:
        	tIncidencia='9';
            
        break;
        case '13':
        case 13:
        	tIncidencia='4';
            
        break;
        case '14':
        case 14:
        	tIncidencia='5';
            
        break;
        case '15':
        case 15:
        	tIncidencia='6';
            
        break;
        case '16':
        case 16:
        	tIncidencia='15';
            
        break;
        case '17':
        case 17:
        	tIncidencia='14';
            
        break;
    }
	var cadObj='{"idPeriodo":"'+gE('idPeriodo').value+'","ciclo":"'+gE('ciclo').value+'","categoria":"'+gE('idCategoria').value+'","servicio":"'+gE('idServicio').value+
                '","arrCeldas":"'+arrCeldas+'","tipoIncidencia":"'+tIncidencia+'"}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            //reemplazarFila(arrCelda[0]+'_'+arrCelda[2],arrResp[1]);
            recargarPagina();
            ventanaAM.close();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRolEnfermeria.php',funcAjax, 'POST','funcion=1&obj='+cadObj,true);
}
/*{
	arrCelda=celda.split('_');
    desHabilitarfechaFin=false;
    tIncidencia='';

    switch(tI)
    {
    	case '1':
        case 1:
        	tIncidencia='1';
            
        break;
        case '2':
        case 2:
        	tIncidencia='17';
            
        break;
        case '22':
        case 22:
        	tIncidencia='16';
            
        break;
        case '9':
        case 9:
        	tIncidencia='2';
            
        break;
        case 10:
        case '10':
        	tIncidencia='7';
            
        break;
        case '11':
        case 11:
        	tIncidencia='8';
            
        break;
        case '12':
        case 12:
        	tIncidencia='9';
            
        break;
        case '13':
        case 13:
        	tIncidencia='4';
            
        break;
        case '14':
        case 14:
        	tIncidencia='5';
            
        break;
        case '15':
        case 15:
        	tIncidencia='6';
            
        break;
        case '16':
        case 16:
        	tIncidencia='15';
            
        break;
        case '17':
        case 17:
        	tIncidencia='14';
            
        break;
    }

    switch(tIncidencia)
    {
    	case '1':
        case '9':
        case '22':
        	desHabilitarfechaFin=true;
        break;
    }
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha de inicio:'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaIni',
                                                            value:arrCelda[1],
                                                            disabled:true
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha de Fin:'
                                                            
                                                        },
                                                        {
                                                        	x:100,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaFin',
                                                            value:arrCelda[1],
                                                            disabled:desHabilitarfechaFin
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Comentarios:'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:65,
                                                            width:400,
                                                            height:80,
                                                            id:'txtComentarios',
                                                            xtype:'textarea'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar incidencia ['+i+']',
										width: 550,
										height:240,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var arrCelda=celda.split('_');
                                                                    	var cadObj='{"idPeriodo":"'+gE('idPeriodo').value+'","ciclo":"'+gE('ciclo').value+'","categoria":"'+gE('idCategoria').value+'","servicio":"'+gE('idServicio').value+
                                                                        			'","fechaInicio":"'+gEx('dteFechaIni').getValue().format('Y-m-d')+'","fechaFin":"'+gEx('dteFechaFin').getValue().format('Y-m-d')+
                                                                                    '","tipoIncidencia":"'+tIncidencia+'","idUsuario":"'+arrCelda[0]+'","turno":"'+arrCelda[2]+'","comentarios":"'+cv(gEx('txtComentarios').getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                reemplazarFila(arrCelda[0]+'_'+arrCelda[2],arrResp[1]);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRolEnfermeria.php',funcAjax, 'POST','funcion=1&obj='+cadObj,true);

																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}	*/

function mostrarVentanaSuplencias(tI,celda)
{
	
}

function reemplazarFila(idFila,nFila)
{
	var fila=gE(idFila);
    fila.innerHTML=nFila;
}