<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	include_once("latis/diccionarioTerminos.php");
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares where situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);
	$consulta="SELECT id__464_gridPeriodos,nombrePeriodo FROM _464_gridPeriodos ";
	$arrPeriodos=$con->obtenerFilasArreglo($consulta);
?>

var idContratoRedireccion=-1;
var idUsuarioContrato=-1;
var versionContrato=-1;

Ext.onReady(inicializar);

function inicializar()
{
	var arrCiclo=<?php echo $arrCiclo?>;
    var arrPeriodo=<?php echo $arrPeriodos?>;//eval(gE('arrPeriodos').value);
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,90);
    cmbCiclo.on('select',function(cmb,registro)
    					{
                        	idContratoRedireccion=-1;
                        	gEx('gridContratos').getStore().reload();
                            
                        }
    			)
    cmbCiclo.setValue('<?php echo $cicloActivo?>')
	var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodo,0,0,190);   
    cmbPeriodo.on('select',function(cmb,registro)
    					{
                        	idContratoRedireccion=-1;
                        	gEx('gridContratos').getStore().reload();
                        }
    			) 
    cmbPeriodo.setValue(arrPeriodo[0][0]);
    
    
    var oConf=	{
    					idCombo:'cmbCliente',
                        anchoCombo:430,
                        campoDesplegar:'Nombre',
                        campoID:'idUsuario',
                        funcionBusqueda:6,
                        raiz:'personas',
                        nRegistros:'num',
                        paginaProcesamiento:'../Usuarios/procesarbUsuario.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br></td><td width="50"><img height="40" width="33" src="../Usuarios/verFoto.php?Id={idUsuario}"/></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idUsuario'},
                                    {name:'Paterno'},
                                    {name:'Materno'},
                                    {name:'Nombre'},
                                    {name:'Nom'},
                                    {name: 'Status'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	var cond='i.idUsuario in (SELECT DISTINCT idUsuario FROM 4519_asignacionProfesorGrupo a,4520_grupos g WHERE g.idGrupos=a.idGrupo AND g.Plantel=\'<?php echo $_SESSION["codigoInstitucion"]?>\' AND g.idCiclo='+cmbCiclo.getValue()+')';
                                        dSet.baseParams.criterio=combo.getRawValue();
                                        dSet.baseParams.campoBusqueda=5;
                                        dSet.baseParams.listRoles="'-1000_0'";
                                        dSet.baseParams.oDepto=1;
                                        dSet.baseParams.oInstitucion=1;
                                        dSet.baseParams.cW=bE(cond);
										idUsuarioContrato=-1;
                                        gEx('gridContratos').getStore().removeAll();

                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idUsuarioContrato=registro.get('idUsuario');
                                        idContratoRedireccion=-1;
                                       	gEx('gridContratos').getStore().reload(); 
                                    }  
    				};

    
	var cmbPersonal=crearComboExtAutocompletar(oConf);
    
	var arrStatusContrato=[['2','Vigentes'],['3','Cancelados'],['4','Finalizados'],['5','Cancelados por baja de docente']];
    
    var alDatos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields:	[
                                                                    {name: 'idContratoProfesor'},
                                                                    {name: 'folio'},
                                                                    {name: 'fInicio',type:'date',dateFormat:'Y-m-d'},
                                                                    {name: 'fTermino',type:'date',dateFormat:'Y-m-d'},
                                                                    {name: 'versiones'},
                                                                    {name: 'documento'},
                                                                    {name: 'datosMateria'},
                                                                    {name: 'cambiaContrato'},
                                                                    {name: 'cambiosDetectados'}
                                                                    
                                                                ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
																							
                                                                                              url: '../paginasFunciones/funcionesRecursosHumanos.php'
                                                                                          }
                                                                                      )                             
                                                    })
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=20;
                                        proxy.baseParams.idCiclo=cmbCiclo.getValue();
                                        proxy.baseParams.idPeriodo=cmbPeriodo.getValue();
                                        proxy.baseParams.plantel='<?php echo $_SESSION["codigoInstitucion"]?>';
                                        proxy.baseParams.idUsuario=idUsuarioContrato;
                                        

                                    }
                        );
    
    alDatos.on('load',function()
    					{
                        	if(idContratoRedireccion!=-1)
                            {

                                var arrParam=[['idContrato',idContratoRedireccion],['nVersion',versionContrato]];
                                idContratoRedireccion=-1;
                                enviarFormularioDatos('../modulosEspeciales_UGM/imprimirContrato.php',arrParam);
                            }
                        }
    			)
   
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<table ><tr><td  height="10">{datosMateria}</td></tr></table>'
                                                )
                                            });
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        expander,
														{
															header:'Folio',
															width:180,
                                                            align:'left',
															sortable:true,
															dataIndex:'folio',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                    	if(registro.get('cambiaContrato')=='1')
                                                                        {
                                                                        	comp='<img src="../images/exclamation.png" title="Se ha detectado que el contrato actual presenta cambios en la informaci&oacute;n asociada, expanda el registro para m&aacute;s detalles" alt="Se ha detectado que el contrato actual presenta cambios en la informaci&oacute;n asociada, expanda el registro para m&aacute;s detalles" />&nbsp;';
                                                                        }
                                                                        return comp+' '+val;
                                                                    }
														},
                                                       	{
															header:'Fecha de inicio',
															width:120,
															sortable:true,
                                                            align:'right',
															dataIndex:'fInicio',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y');
                                                                    }
														}
                                                        ,
														{
															header:'Fecha de t&eacute;rmino',
															width:120,
															sortable:true,
                                                            align:'right',
															dataIndex:'fTermino',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y');
                                                                    }
														},
														{
															header:'Versi&oacute;n',
															width:100,
															sortable:true,
                                                            align:'center',
															dataIndex:'versiones',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(val!='1')
                                                                        	return '<a href="javascript:mostrarBitacoraVersiones(\''+bE(registro.get('idContratoProfesor'))+'\')">'+val+'</a>';
                                                                        else
                                                                        	return val;	
                                                                    }
														},
														{
															header:'Documento',
															width:600,
															sortable:true,
                                                            align:'left',
															dataIndex:'documento',
                                                            renderer:mostrarValorDescripcion
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridContratos',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            region:'center',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            tbar:	[
                                                            			 {
                                                                         	xtype:'label',
                                                                          	html:'<span class="letraRojaSubrayada8"><b>Docente:</b></span>&nbsp;&nbsp;'
                                                                     	 },
                                                            			cmbPersonal,
                                                                        '-',
                                                                        
                                                            		],
                                                            plugins:[expander]
                                                        }
                                                    );
	alDatos.load();                                                    
	
    new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:[
                                                    {
                                                          xtype:'label',
                                                          html:'<span class="letraRojaSubrayada8" style="font-size:14px"><b>Historial de contratos de docentes</b></span>'
                                                      }
                                                  ],
                                            items:	[	
                                            			{
                                                            xtype:'panel',
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            layout:'border',
                                                            tbar:[
                                                                   
                                                                      {
                                                                          xtype:'label',
                                                                          html:'<span class="letraRojaSubrayada8"><b>Ciclo:</b></span>&nbsp;&nbsp;'
                                                                      },cmbCiclo,'-',
                                                                      {
                                                                          xtype:'label',
                                                                          html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8"><b>Periodo:</b></span>&nbsp;&nbsp;'
                                                                      },'-',
                                                                      cmbPeriodo,
                                                                      '-',
                                                                      {
                                                                          id:'btnImprimir',
                                                                          icon:'../images/printer.png',
                                                                          cls:'x-btn-text-icon',
                                                                          disabled:true,
                                                                          text:'Imprimir contrato vigente',
                                                                          handler:function()
                                                                                  {
		                                                                              idContratoRedireccion=-1;
                                                                                      var fila=tblGrid.getSelectionModel().getSelected();
                                                                                      imprimirContrato(bE(fila.get('idContratoProfesor')),bE(fila.get('versiones')));

                                                                                  }
                                                                          
                                                                      },'-',
                                                                      {
                                                                          id:'btnReconstruir',
                                                                          icon:'../images/arrow_refresh.PNG',
                                                                          cls:'x-btn-text-icon',
                                                                          disabled:true,
                                                                          text:'Rehacer contrato',
                                                                          handler:function()
                                                                                  {
                                                                                  		var fila=tblGrid.getSelectionModel().getSelected();
                                                                                  		mostrarVentanaReconstruirContratos(fila);   
                                                                                  }
                                                                          
                                                                      }
                                                                  ],
                                                            items:	[
                                                                        tblGrid
                                                                    ]
                                                        }
                                            		]
                                        }
                            			
                                     ]
						}
                    )  
	chkRow.on('rowselect',function(sm,nFinla,fila)
                          {
                              gEx('btnImprimir').enable();
                              gEx('btnReconstruir').disable();
                              if(fila.data.cambiaContrato=='1')
                                  gEx('btnReconstruir').enable();
                              
                          }
              )                    
}


function mostrarVentanaReconstruirContratos(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cambios detectados:'
                                                        },
                                                        {
                                                        	x:155,
                                                            xtype:'textarea',
                                                            y:5,
                                                            width:500,
                                                            height:80,
                                                            readOnly:true,
                                                            id:'txtCambiosDetectados',
                                                            value:fila.get('cambiosDetectados').replace(/<br>/gi,'\n\r')
                                                        },
                                                        {
                                                        	x:10,
                                                            y:120,
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:155,
                                                            xtype:'textarea',
                                                            y:115,
                                                            width:500,
                                                            height:90,
                                                            readOnly:false,
                                                            id:'txtComentarios'
                                                        }	
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Rehacer contrato',
										width: 710,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtComentarios').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var idContrato=fila.get('idContratoProfesor');
                                                                        var cadObj='{"cambiosDetectados":"'+cv(gEx('txtCambiosDetectados').getValue())+'","txtComentarios":"'+cv(gEx('txtComentarios').getValue())+'"}';
                                                                        function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	idContratoRedireccion=idContrato;
                                                                                        versionContrato=arrResp[1];
                                                                                        ventanaAM.close();
                                                                                        gEx('gridContratos').getStore().reload();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=56&idContrato='+idContrato+'&cadObj='+cadObj,true);
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer rehacer el contrato seleccionado?',resp);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function mostrarBitacoraVersiones(iC)
{
	var gridVersiones=crearGridVersiones(bD(iC));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridVersiones

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Versiones del contrato',
										width: 700,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridVersiones(iC)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
		                                                {name: 'noVersion', type:'int'},
		                                                {name:'fechaReemplazo', type:'date', dateFormat:'Y-m-d H:i:s'},
		                                                {name:'motivo'},
                                                        {name:'responsable'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesRecursosHumanos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'noVersion', direction: 'DESC'},
                                                            groupField: 'noVersion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='57';
                                        proxy.baseParams.idContrato=iC;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            {
                                                                header:'No. Versi&oacute;n',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'noVersion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return '<a href="javascript:imprimirContrato(\''+bE(iC)+'\',\''+bE(val)+'\')"><b>'+val+'</b>&nbsp;<img height="13" width="13" src="../images/printer.png" alt="Imprimir versi&oacute;n del contrato" title="Imprimir versi&oacute;n del contrato"/></a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de reemplazo',
                                                                width:130,
                                                                sortable:true,
                                                                dataIndex:'fechaReemplazo',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        }
                                                            },
                                                            {
                                                                header:'Responsable de reemplazo',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'responsable'
                                                                
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridComentarios',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:370,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableRowBody:true,
						                                                                            getRowClass : formatearFila
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function formatearFila(record, rowIndex, p, ds) 
{
	p.body = '<br /><p style="margin-left: 10em;margin-right: 3em;text-align:left"><span class="copyrigthSinPaddingNegro"><table>'+
    		'<tr height="21"><td width="20"></td><td width="140"><span class="letraRojaSubrayada8">Motivo del reemplazo: </span></td><td align="justify"  ><span class="copyrigthSinPaddingNegro">'+record.data.motivo.replace(/<br><br>/gi,'<br />')+'</span></td></tr></table></span></p><br />';
    return 'x-grid3-row-expanded';

	
}

function imprimirContrato(iC,v)
{
    var arrParam=[['idContrato',bD(iC)],['nVersion',bD(v)]];
    enviarFormularioDatos('../modulosEspeciales_UGM/imprimirContrato.php',arrParam);
}