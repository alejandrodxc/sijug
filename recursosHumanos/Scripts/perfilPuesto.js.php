<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$conCalculos="SELECT idConsulta,nombreConsulta FROM 991_consultasSql WHERE idTipoConcepto>2";
	$arregloCalc=$con->obtenerFilasArreglo($conCalculos);
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridCriterios();
}

function crearGridCriterios()
{
    
    var idPuesto=gE('idPuesto').value;
    var dsRegistrosC=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPerfilVSPuesto'},
                                                                  {name: 'idCriterio'},
                                                                  {name: 'nombreConsulta'},
                                                                  {name: 'tieneParam'},
                                                                  {name: 'arregloParam'},
                                                                  {name: 'valor'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesRecursosHumanos.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosC.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=1;
                                        proxy.baseParams.idPuesto=idPuesto;
                                    }
                        );
    var columnaCheck=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        columnaCheck,
														{
															header:'Criterio',
															width:350,
															sortable:true,
															dataIndex:'nombreConsulta',
                                                            align:'left'
														},
                                                        {
															header:'Puntos',
															width:100,
															sortable:true,
															dataIndex:'valor',
                                                            align:'right',
                                                            renderer:function(val,meta,registro){
                                                            						return Ext.util.Format.number(val,'0,0.00')+'&nbsp;<a href="javascript:modificarPuntos('+registro.get('idPerfilVSPuesto')+','+val+')"><img height="13" width="13" src="../images/pencil.png" alt="Modificar Puntos" title="Modificar Puntos" /></a>';
                                                            				   }
														},
                                                        {
															header:'Parametros',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'tieneParam',
                                                            renderer:function(val,meta,registro)
                                                            				  {
                                                                                if(val==1)
                                                                                	return '<a href="javascript:verParametros('+registro.get('idPerfilVSPuesto')+','+registro.get('idCriterio')+')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver Parametros" title="Ver Parametros" /></a>';
                                                                                else
                                                                                	return '';
                                                                              }
														}
													]
												);
	var tblGridC=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPerfil',
                                                            title:'Pefil de puesto',
                                                            store:dsRegistrosC,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:columnaCheck,
                                                            renderTo:'gCiterios',
                                                            height:750,
                                                            width:600,
                                                            tbar:[
                                                            		{
                                                                    	text:'Agregar Criterio',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                        		{
                                                                                	agregarCriterio(idPuesto);
                                                                                }
                                                                    },
                                                                    {
                                                                    	text:'Eliminar Criterio',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                        		{
                                                                                	eliminarCriterio();
                                                                                }
                                                                    }
                                                            	 ]
                                                            
                                                        }
                                                    );
		
    dsRegistrosC.load()  ;
    return tblGridC;     
}


function verParametros(idTabla)
{
	tb_show(lblAplicacion,'../recursosHumanos/modificarParametros.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idTabla='+idTabla+'&TB_iframe=true&height=420&width=650',"","scrolling=yes");
}

var regCriterio=crearRegistro([
                                                                    
                                  {name:'idCalculo'},
                                  {name:'nombreCalculo'},
                                  {name:'variables'}
                              ]);

function agregarCriterio(idPuesto)
{
	 if(idPuesto=='-1')
    {
    	var cmb=gE('idPuestoC');
        idPuesto=cmb.options[cmb.selectedIndex].value;
    }
    if(idPuesto==-1)
    {
    	msgBox('Debe Indicar el puesto');
        return;
    }
    var arregloC=<?php echo $arregloCalc ?>;
    var comboCalculos=crearComboExt('comboCalculos',arregloC,70,10);
    var form2 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario2',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:15,
                                                            html:'<span><b>C&aacute;lculo:</b></span>'
                                                        },
                                                        comboCalculos,
                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:45,
                                                            html:'<span><b>Puntos:</b></span>'
                                                        },
                                                        {
                                                            xtype:'numberfield',
                                                            x:70,
                                                            y:45,
                                                            allowDecimal:true,
                                                            allowNegative:false,
                                                            width:60,
                                                            id:'puntosF'
                                                        }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Agregar C&aacute;lculo',
										width: 300,
										height:150,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form2,
                                        id:'ventana2',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var idCalculo=Ext.getCmp('comboCalculos').getValue();
                                                                                    if(idCalculo=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el c&aacute;lculo ');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var puntos=Ext.getCmp('puntosF').getValue();
                                                                                    if(puntos=='')
                                                                                    	puntos=0;
																					
                                                                                    var idC=idCalculo;
                                                                                    
                                                                                    var almacenP=Ext.getCmp('gridPerfil').getStore();
                                                                                    var pos=obtenerPosFila(almacenP,'idCriterio',idC);
                                                                                    
                                                                                    if(pos==-1)
                                                                                    {
                                                                                        function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                var arrDatos=eval(arrResp[1]);
                                                                                                if(arrDatos.length>0)
                                                                                                {
                                                                                                    ventana.close();
                                                                                                    //gEx('ventanaD').hide();
                                                                                                    tb_show(lblAplicacion,'../recursosHumanos/parametrosCalculo.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idCalculo='+idCalculo+'&idPuesto='+idPuesto+'&valor='+puntos+'&TB_iframe=true&height=420&width=650',"","scrolling=yes");
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    var cadena='';
                                                                                                    function funcAjax2()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                                        if(resp[0]==1)
                                                                                                        {
                                                                                                            
                                                                                                            
                                                                                                            ventana.close();
                                                                                                            
                                                                                                            entraPagina(idPuesto);
                                                                                                            //recargarPagina();
                                                                                                            //Ext.getCmp('gridPerfil').getStore().reload();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax2, 'POST','funcion=2&idCalculo='+bE(idCalculo)+'&idPuesto='+bE(idPuesto)+'&valor='+puntos+'&cadena='+cadena,true)
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax, 'POST','funcion=135&idCalculo='+idCalculo,true);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	ventana.close();
                                                                                    }
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

function recargarAlmacen()
{
    Ext.getCmp('gridPerfil').getStore().reload();
}

function eliminarCriterio()
{
	var filas=Ext.getCmp('gridPerfil').getSelectionModel().getSelections();
    var tamano=filas.length;
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un criterio');
        return;
    }
    
    var cadena='';
    var x;
    
    for(x=0;x<tamano;x++)
    {
    	var id=filas[x].get('idPerfilVSPuesto');
        
        if(cadena=='')
        	cadena=id;
        else     
        	cadena+=','+id;
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
           Ext.getCmp('gridPerfil').getStore().reload();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=3&cadena='+cadena,true);
}

function modificarPuntos(idP,valor)
{
    var valorM=Ext.util.Format.number(valor,'0,0.00');
    var form1 = new Ext.form.FormPanel(	
                                    {
                                        id:'formulario1',
                                        baseCls: 'x-plain',
                                        layout:'absolute',
                                        defaultType: 'textfield',
                                        items: 	[ 
                                                    {
                                                        xtype:'label',
                                                        x:15,
                                                        y:15,
                                                        html:'<span><b>Puntos:</b></span>'
                                                    },
                                                    {
                                                        xtype:'numberfield',
                                                        x:70,
                                                        y:10,
                                                        allowDecimal:true,
                                                        allowNegative:false,
                                                        width:60,
                                                        id:'puntosM',
                                                        value:valorM
                                                    }
                                                ]
                                    }
                                );

    
    var ventana = new Ext.Window(
									{
										title: 'Modificar Puntos',
										width: 200,
										height:150,
										minWidth: 100,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form1,
                                        id:'ventana1',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
                                                                                    var puntos=Ext.getCmp('puntosM').getValue();
                                                                                    if(puntos=='')
                                                                                    	puntos=0;
																					function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            recargarAlmacen();
                                                                                            ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=5&idTabla='+idP+'&puntos='+puntos,true);
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

function entraPagina(idPuesto)
{
	var conf=gE('configuracion').value;
    var arrP=[['idPuesto',idPuesto],['configuracion',conf]];
    enviarFormularioDatos('../recursosHumanos/guardarCalculo.php',arrP);
}