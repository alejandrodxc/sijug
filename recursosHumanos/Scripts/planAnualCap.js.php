<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	crearGrid();
    
}	

function crearGrid()
{
    
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                               			{name: 'idCurso'},
                                                        {name: 'anio'},
                                                        {name: 'codigoU'},
                                                        {name: 'nombreU'},
                                                        {name: 'nombreCurso'},
                                                        {name: 'numPer'},
                                                        {name: 'tipoFin'},
                                                        {name: 'costo'},
                                                        {name: 'cadUsr'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesPlaneacion.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombreCurso', direction: 'ASC'},
                                                            groupField: 'nombreU'
                                                        })                                      
     
	dsCursos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=124;
                                    }
                  ); 
    var summary = new Ext.ux.grid.HybridSummary();                                      
 
   
   var suma=0;
    var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreCurso' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'anio' 
                                                                      }
                                                                  ]
                                                  }
                                              );                                
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Ciclo',
															width:60,
															sortable:true,
															dataIndex:'anio',
                                                            align:'left'
														},
                                                        {
															header:'Departamento',
															width:100,
															sortable:true,
															dataIndex:'nombreU',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:300,
															sortable:true,
															dataIndex:'nombreCurso',
                                                            align:'left'
														},
                                                        {
															header:'Tipo de financiamiento',
															width:110,
                                                            align:'left',
															sortable:true,
															dataIndex:'tipoFin',
                                                            renderer:function(val,meta,registro)
                                                            		 {
                                                                     	switch(val)
                                                                        {
                                                                            case '1':
                                                                        		return'Interno'
                                                                            break;
                                                                            case '2':
                                                                        		return'Instituci&oacute;n beneficiaria'    
                                                                            break;
                                                                            case '3':
                                                                        		return'3 Proveedores'    
                                                                            break;
                                                                            default:
                                                                            	return''
                                                                            break;    
                                                                        }    
                                                                     }    
                                                        },
                                                        {
															header:'No. personas',
															width:80,
                                                            align:'center',
															sortable:true,
															dataIndex:'numPer',
                                                            summaryRenderer:function()
                                                                          {
                                                                              return "<b>Costo total:</b>";
                                                                          }
														},
                                                        {
															header:'Costo',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney',
                                                            summaryType:'sum'
                                                        },
                                                        {
															header:'Participantes',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'cadUsr',
                                                            renderer:function(val,meta,registro)
                                                                     {
                                                                        return'<a href="javascript:verParticipantes(\''+bE(val)+'\')"><img height="13" width="13" src="../images/users.png" /></a>'
                                                                     }
                                                        }
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPrendas',
                                                            title:'Concentrado de prendas',
                                                            store:dsCursos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'gridCap',
                                                            height:650,
                                                            width:850,
                                                            plugins: [filters,summary],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursos.load()  ;
    return grid;     
}

function verParticipantes(cadena)
{
	var gridP=crearGridParticipantes(bD(cadena));
    var form2=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 2',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           gridP
                                                        ]
                                              }
                                          )
    
    var ventana=new Ext.Window(
							   		{
										title:'Participantes',
										width:620,
										height:500,
										layout:'fit',
										buttonAlign:'center',
										items:[form2],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
													{
														text:'Cerrar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function crearGridParticipantes(cadena)
{
    var dsPar=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idUsuario'},
                                                                  {name: 'Nombre'},
                                                                  {name: 'codigoUnidad'},
                                                                  {name: 'unidad'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(
                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesPlaneacion.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsPar.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=126;
                                        proxy.baseParams.cadena=cadena;
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'Nombre' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'unidad' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
                                                                 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:35}),
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'unidad',
                                                            align:'left'
														},
                                                        {
															header:'Participante',
															width:250,
															sortable:true,
															dataIndex:'Nombre'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridParticipantes',
                                                            x:0,
                                                            y:0,
                                                            store:dsPar,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:430,
                                                            width:600,
                                                            plugins: [filters]
                                                        }
                                                    );
    dsPar.load()  ;
    return tblGridP;     
}
