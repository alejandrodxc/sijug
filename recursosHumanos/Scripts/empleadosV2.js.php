<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idTipoPercepcion,CONCAT('[',clave,'] ',descripcion) FROM 681_tiposPercepcionSAT ORDER BY clave";
	$arrTiposPercepciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoDeduccion,CONCAT('[',clave,'] ',descripcion) FROM 682_tiposDeduccionSAT ORDER BY clave";
	$arrTiposDeducciones=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idConcepto,CONCAT('[',idConcepto,'] ',nombreConcepto),tipoValor FROM 713_conceptosBaseNomina WHERE referencia='".$referenciaFiltros."' and situacion=1";
	$arrConceptosBaseActivos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idConcepto,CONCAT('[',idConcepto,'] ',nombreConcepto),tipoValor FROM 713_conceptosBaseNomina WHERE referencia='".$referenciaFiltros."'";
	$arrConceptosBase=$con->obtenerFilasArreglo($consulta);

?>

var arrConceptosBaseActivos=<?php echo $arrConceptosBaseActivos?>;
var arrConceptosBase=<?php echo $arrConceptosBase?>;
var arrTiposPercepciones=<?php echo $arrTiposPercepciones?>;
var arrTiposDeducciones=<?php echo $arrTiposDeducciones?>;

var arrTipoTelefono=[['0','Fijo'],['1','Celular']];

Ext.onReady(inicializar);

function inicializar()
{
	
    gE('_rfc1vch').focus();

    crearGridEmail();
    crearGridTelefono();
    var tabs = $( "#tabs" ).tabs	(
    									{
                                        	activate: function( event, ui ) 
                                        		{

                                                	if(ui.newTab.context.innerHTML=='Conceptos de Nómina')
                                                    {
                                                    	gEx('gDeducciones').getView().refresh();
                                                        gEx('gPercepciones').getView().refresh();
                                                    }
                                                    if(ui.newTab.context.innerHTML=='Conceptos Base de Nómina')
                                                    {
                                                    	gEx('gConceptosBase').getView().refresh();
                                                       
                                                    }
                                                    
                                                }	
                                        }
    								);
    new Ext.form.DateField	(
    							{
    								renderTo:'spFechaInicioRelLab',
                                    value:gE('_fechaIniRelLabdte').value,
                                    listeners:	{
                                    				select:function(dte,val)
                                                    		{
                                                            	gE('_fechaIniRelLabdte').value=val.format('d/m/Y');
                                                            }
                                    			}
                                }
    						)
	
    if(gE('tblPercepciones'))
    {
        crearGridPercepciones();
        crearGridDeducciones();
    }
    else
	    crearGridConceptosBase();                        
    
}

function validarFormulario()
{
	var rfc1=gE('_rfc1vch');
    var rfc2=gE('_rfc2vch');
    var rfc3=gE('_rfc3vch');
	if(validarFormularios('frmEnvio'))
    {
    
    	
    
    	var tipoEmpresa='1';
        if(tipoEmpresa=='1')//Persona fisica
        {
        	if(rfc1.value.length!=4)
            {
            	function resp1()
                {
                	rfc1.focus();
                }
                msgBox('La logitud del RFC debe ser de 4 caracteres para personas f&iacute;sicas',resp1);
                return;
            }
			

        }
        else
        {
        	if(rfc1.value.length!=3)
            {
            	function resp2()
                {
                	rfc1.focus();
                }
                msgBox('La logitud del RFC debe ser de 3 caracteres para personas morales',resp2);
                return;
            }
        }
        
        if(rfc2.value.length!=6)
        {
            function resp3()
            {
                rfc2.focus();
            }
            msgBox('La logitud del RFC debe ser de 6 caracteres',resp3);
            return;
        }

        if(rfc3.value.length!=3)
        {
            function resp4()
            {
                rfc3.focus();
            }
            msgBox('La logitud del RFC debe ser de 3 caracteres',resp4);
            return;
            
        }
        
       
        var o;
        var x;
        var fila;
        
        var id=gEN('id')[0].value;
        
        var gMail=gEx('gMail');
        var arrMail='';
        for(x=0;x<gMail.getStore().getCount();x++)
        {
        	fila=gMail.getStore().getAt(x);
            
            if(!validarCorreo(fila.data.email))
            {
            	$( "#tabs" ).tabs( "option", "active", 1 );    
                function respMail()
                {
                	
                    gMail.startEditing(x,1);
                }
                msgBox('La direcci&oacute;n de E-mail no es v&aacute;lida',respMail);
                return;
            }
            
            o='{"mail":"'+fila.data.email+'"}';
            if(arrMail=='')
            	arrMail=o;
            else
            	arrMail+=','+o;
        }
        
        
        
        var arrTelefonos='';
        
        var gTelefono=gEx('gTelefono');
       
        for(x=0;x<gTelefono.getStore().getCount();x++)
        {
        	fila=gTelefono.getStore().getAt(x);
            if(fila.data.telefono=='')
            {
            	$( "#tabs" ).tabs( "option", "active", 1 );   
                function respTel()
                {
                	     
                    gTelefono.startEditing(x,3);
                }
                msgBox('Debe ingresar el n&uacute;mero telefonico',respTel);
                return;
            }                                                                            
            o='{"tipo":"'+fila.data.tipo+'","lada":"'+fila.data.lada+'","telefono":"'+fila.data.telefono+'","extension":"'+fila.data.extension+'"}';
            if(arrTelefonos=='')
            	arrTelefonos=o;
            else
            	arrTelefonos+=','+o;
            
        }
        
        
        var arrPercepciones='';
        var arrDeducciones='';
        var arrConceptosBase='';
        if(gE('tblPercepciones'))
        {
            var gPercepciones=gEx('gPercepciones');
           
            for(x=0;x<gPercepciones.getStore().getCount();x++)
            {
                fila=gPercepciones.getStore().getAt(x);
                
                if(fila.data.importeGravado=='')
                    fila.data.importeGravado=0;
                
                if(fila.data.importeExento=='')
                    fila.data.importeExento=0;
                                                                                         
                o='{"tipoPercepcion":"'+fila.data.tipoPercepcion+'","clave":"'+fila.data.clave+'","descripcion":"'+(fila.data.descripcion.replace(/"/gi,"'"))+'","importeGravado":"'+fila.data.importeGravado+'","importeExento":"'+fila.data.importeExento+'"}';
                if(arrPercepciones=='')
                    arrPercepciones=o;
                else
                    arrPercepciones+=','+o;
                
            }
        
        
            var gDeducciones=gEx('gDeducciones');
           
            for(x=0;x<gDeducciones.getStore().getCount();x++)
            {
                fila=gDeducciones.getStore().getAt(x);
                      
                if(fila.data.importeGravado=='')
                    fila.data.importeGravado=0;
                
                if(fila.data.importeExento=='')
                    fila.data.importeExento=0;
                                                                                     
                o='{"tipoDeduccion":"'+fila.data.tipoDeduccion+'","clave":"'+fila.data.clave+'","descripcion":"'+(fila.data.descripcion.replace(/"/gi,"'"))+'","importeGravado":"'+fila.data.importeGravado+'","importeExento":"'+fila.data.importeExento+'"}';
                if(arrDeducciones=='')
                    arrDeducciones=o;
                else
                    arrDeducciones+=','+o;
                
            }
        
        }
        else
        {
        	 
            var gConceptosBase=gEx('gConceptosBase');
            for(x=0;x<gConceptosBase.getStore().getCount();x++)
            {
                fila=gConceptosBase.getStore().getAt(x);
                if(fila.data.idConcepto=='')
                {
                    function resp1000()
                    {
                        $( "#tabs" ).tabs( "option", "active", 3 );   
                        gConceptosBase.startEditing(x,2);
                    }
                    msgBox('Debe especificar el concepto base a aplicar',resp1000);
                    return;
                    
                }
                if(fila.data.valor=='')
                {
                    fila.data.valor=0;
                }
                o='{"idConcepto":"'+fila.data.idConcepto+'","valor":"'+fila.data.valor+'"}';
                
                if(arrConceptosBase=='')
                    arrConceptosBase=o;
                else
                    arrConceptosBase+=','+o;
            }
        
        }
        var _clabevch=gE('_clabevch');
        if((_clabevch.value.length>0)&&(_clabevch.value.length!=18))
        {
        	function resp100()
            {
            	$( "#tabs" ).tabs( "option", "active", 2 );   
                _clabevch.focus();
            }
            msgBox('La clabe bancaria ingresada no es v&aacute;lida',resp100);
            return;
        }
        
       
        
        var objArr='{"arrConceptosBase":['+arrConceptosBase+'],"arrMail":['+arrMail+'],"arrTelefonos":['+arrTelefonos+'],"arrPercepciones":['+arrPercepciones+'],"arrDeducciones":['+arrDeducciones+']}';
        if(id=='-1')
        {
        	gE('funcPHPEjecutarNuevo').value=bE('asociarDatosEmpleado(@idRegPadre,\''+bE(objArr)+'\')');
        }
        else
        {
        	gE('funcPHPEjecutarModif').value=bE('asociarDatosEmpleado('+id+',\''+bE(objArr)+'\')');
        }
        
        
    	gE('frmEnvio').submit();
    }
}

function estadoSel(cmb)
{
	var valor=cmb.options[cmb.selectedIndex].value;
	obtenerMunicipio(valor,'_municipiovch','1')
}

function crearGridEmail()
{
	var dsDatos=eval(bD(gE('arrMail').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'email'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,
														{
															header:'E-mail <span style="color:#F00">*<span>',
															width:220,
															sortable:true,
															dataIndex:'email',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            renderTo:'tblMail',
                                                            width:275,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:135,
															id:'gMail',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span style="color:#900"><b>E-mail de contacto</b></span>&nbsp;&nbsp;&nbsp;'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro([{name: 'email'}]);
                                                                                        var r=new reg	(
                                                                                        					{
                                                                                                            	email:''
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la direcci&oacute;n de E-mail que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la direcci&oacute;n de E-mail seleccionada?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	
    
	return 	tblGrid;
}

function crearGridTelefono()
{
	var cmbTipo=crearComboExt('cmbTipo',arrTipoTelefono);
	var dsDatos=eval(bD(gE('arrContactos').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'lada'},
                                                                    {name: 'telefono'},
                                                                    {name: 'extension'},
                                                                    {name: 'tipo'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,

														{
															header:'Tipo <span style="color:#F00">*<span>',
															width:70,
															sortable:true,
															dataIndex:'tipo',
                                                            editor:cmbTipo,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoTelefono,val);
                                                                    }
														},
														{
															header:'Lada',
															width:40,
															sortable:true,
															dataIndex:'lada',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Tel&eacute;fono <span style="color:#F00">*<span>',
															width:90,
															sortable:true,
															dataIndex:'telefono',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Extensi&oacute;n',
															width:60,
															sortable:true,
															dataIndex:'extension',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
                                                            
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            renderTo:'tblTelefono',
                                                            clicksToEdit:1,
                                                            width:315,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:135,
															id:'gTelefono',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span style="color:#900"><b>Tel&eacute;fonos de contacto</b></span>&nbsp;&nbsp;&nbsp;'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                    	{name: 'lada'},
                                                                                                                        {name: 'telefono'},
                                                                                                                        {name: 'extension'},
                                                                                                                        {name: 'tipo'}
                                                                                                                    ]
                                                                                        						);
                                                                                        var r=new reg	(
                                                                                        					{
                                                                                                            	lada:'',
                                                                                                                telefono:'',
                                                                                                                extension:'',
                                                                                                                tipo:'0'
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el n&uacute;mero de tel&eacute;fono que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el n&uacute;mero de tel&eacute;fono seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function obtenerPuestosDepartamentosEmpresa(cmb)
{
	var idEmpresa=cmb.options[cmb.selectedIndex].value;
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var arrPuestos=eval(arrResp[1]);
            var arrDepartamentos=eval(arrResp[2]);
            var arrRegPatronal=eval(arrResp[3]);
            var arrCentroCosto=eval(arrResp[4]);
            llenarCombo(gE('_idPuestovch'),arrPuestos,true);
            llenarCombo(gE('_idDepartamentovch'),arrDepartamentos,true);
            llenarCombo(gE('_noRegistroPatronalvch'),arrRegPatronal,true);
            llenarCombo(gE('_idCentroCostoint'),arrCentroCosto,true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=58&idEmpresa='+idEmpresa,true);

}

function selFormaPago(cmb)
{
	var iFormaPago=cmb.options[cmb.selectedIndex].value;
    if(iFormaPago=='4')
    {
    	mE('fBanco');
        mE('fBanco2');
    }
    else
    {
    	oE('fBanco');
        oE('fBanco2');
    }
}


function crearGridPercepciones()
{

	var crearCmbPercepcion=crearComboExt('crearCmbPercepcion',arrTiposPercepciones);
	var dsDatos=eval(bD(gE('arrPercepcionesEmp').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'tipoPercepcion'},
                                                                    {name: 'clave'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:2
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Tipo percepci&oacute;n <span style="color:#F00">*</span>',
															width:250,
															sortable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrTiposPercepciones,val));
                                                                    },
															dataIndex:'tipoPercepcion',
                                                            editor:crearCmbPercepcion
														},
														{
															header:'Cve. percepci&oacute;n <span style="color:#F00">*</span>',
															width:100,
															sortable:true,
															dataIndex:'clave',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Descripci&oacute;n <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
                                                            renderer:mostrarValorDescripcion,
															dataIndex:'descripcion',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Imp. gravado <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Imp. exento <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            id:'gPercepciones',
                                                            renderTo:'tblPercepciones',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddPercepcion',
                                                                            text:'Agregar percepci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'tipoPercepcion'},
                                                                                                                        {name: 'clave'},
                                                                                                                        {name: 'descripcion'},
                                                                                                                        {name: 'importeGravado'},
                                                                                                                        {name: 'importeExento'}
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	tipoPercepcion:'',
                                                                                                                clave:'',
                                                                                                                descripcion:'',
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddPercepcion').disable();
                                                                                        Ext.getCmp('btnDelPercepcion').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelPercepcion',
                                                                            text:'Remover percepci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la percepci&oacute;n que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la percepci&oacute;n seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion)
    editorFila.on('validateedit',funcEditorValidaEdicion);
    editorFila.on('canceledit',funcEditorCancelEdicion);                                                    
                                                    
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion(rowEdit,obj,registro,nFila)
{
	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   Ext.getCmp('btnDelPercepcion').enable();
   Ext.getCmp('btnAddPercepcion').enable();
   return true;
}

function funcEditorCancelEdicion(rowEdit,cancelado)
{

	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnDelPercepcion').enable();
    Ext.getCmp('btnAddPercepcion').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}



function crearGridDeducciones()
{

	var cmbDeduccion=crearComboExt('cmbDeduccion',arrTiposDeducciones);
	var dsDatos=eval(bD(gE('arrDeduccionesEmp').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'tipoDeduccion'},
                                                                    {name: 'clave'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:2
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Tipo deducci&oacute;n <span style="color:#F00">*</span>',
															width:250,
															sortable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrTiposPercepciones,val));
                                                                    },
															dataIndex:'tipoDeduccion',
                                                            editor:cmbDeduccion
														},
														{
															header:'Cve. deducci&oacute;n <span style="color:#F00">*</span>',
															width:100,
															sortable:true,
															dataIndex:'clave',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Descripci&oacute;n <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
                                                            renderer:mostrarValorDescripcion,
															dataIndex:'descripcion',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Imp. gravado <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Imp. exento <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            id:'gDeducciones',
                                                            renderTo:'tblDeducciones',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddDeduccion',
                                                                            text:'Agregar deducci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'tipoDeduccion'},
                                                                                                                        {name: 'clave'},
                                                                                                                        {name: 'descripcion'},
                                                                                                                        {name: 'importeGravado'},
                                                                                                                        {name: 'importeExento'}
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	tipoDeduccion:'',
                                                                                                                clave:'',
                                                                                                                descripcion:'',
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddDeduccion').disable();
                                                                                        Ext.getCmp('btnDelDeduccion').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelDeduccion',
                                                                            text:'Remover deducci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la deducci&oacute;n que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la deducci&oacute;n seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion2)
    editorFila.on('validateedit',funcEditorValidaEdicion2);
    editorFila.on('canceledit',funcEditorCancelEdicion2);                                                    
                                                    
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion2(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion2(rowEdit,obj,registro,nFila)
{
	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   Ext.getCmp('btnAddDeduccion').enable();
   Ext.getCmp('btnDelDeduccion').enable();
   return true;
}

function funcEditorCancelEdicion2(rowEdit,cancelado)
{

	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnAddDeduccion').enable();
   Ext.getCmp('btnDelDeduccion').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}

function crearGridConceptosBase()
{
	var cmbConceptoBase=crearComboExt('cmbConceptoBase',arrConceptosBaseActivos);
    
	var dsDatos=eval(bD(gE('arrConceptosBase').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idConcepto'},
                                                                    {name: 'valor'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Concepto',
															width:300,
															sortable:true,
															dataIndex:'idConcepto',
                                                            editor:cmbConceptoBase,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrConceptosBase,val);
                                                                    }
														},
														{
															header:'Valor',
															width:150,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'valor',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(registro.data.idConcepto!='')
                                                                        {
                                                                        	var pos=existeValorMatriz(arrConceptosBase,registro.data.idConcepto);
                                                                            switch(arrConceptosBase[pos][2])
                                                                            {
                                                                            	case '1':
                                                                                	if(val=='')
                                                                                    	val=0;
                                                                                	return Ext.util.Format.number(val,'0.00')+' %';
                                                                                break;
                                                                                case '2':
                                                                                	return Ext.util.Format.usMoney(val);
                                                                                break;
                                                                                case '3':
                                                                                	return Ext.util.Format.number(val,'0.00');
                                                                                break;
                                                                            }
                                                                        }
                                                                        else
	                                                                        return val;
                                                                    },
                                                            editor:	{
                                                            			allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            id:'gConceptosBase',
                                                            renderTo:'tblConceptosBase',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:250,
                                                            width:740,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar Concepto Base',
                                                                            handler:function()
                                                                            		{
                                                                                    	var regConcepto=crearRegistro(
                                                                                        								[
                                                                                                                            {name: 'idConcepto'},
                                                                                                                            {name: 'valor'}
                                                                                                                        ]
                                                                                        							);
                                                                                    	var r=new regConcepto	(		
                                                                                        							{
                                                                                                                    	idConcepto:'',
                                                                                                                      	valor:'' 
                                                                                                                    	
                                                                                                                    }
                                                                                        						)
                                                                                    
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Concepto Base',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el concepto base que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el concepto base seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}
