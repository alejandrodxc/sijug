<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridResultados();
}

function crearGridResultados()
{
    var dsRegistros=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idUsuario'},
                                                                  {name: 'nombre'},
                                                                  {name: 'resPsicometrico'},
                                                                  {name: 'resConocimientos'},
                                                                  {name: 'comentarios'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesRecursosHumanos.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistros.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=14;
                                    }
                        );
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Nombre del Candidato',
															width:240,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left'
														},
                                                        {
															header:'Eval. Psicometrica',
															width:115,
                                                            align:'left',
															sortable:true,
															dataIndex:'resPsicometrico'
                                                        },
                                                        {
															header:'Eval. Conocimientos',
															width:115,
                                                            align:'center',
															sortable:true,
															dataIndex:'resConocimientos'
														},
                                                        {
															header:'Observaciones',
															width:240,
                                                            align:'center',
															sortable:true,
															dataIndex:'comentarios'
														}
													]
												);
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridResultados',
                                                            title:'Resultados',
                                                            store:dsRegistros,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'reporte',
                                                            height:750,
                                                            width:750
                                                        }
                                                    );
    dsRegistros.load()  ;
    return tblGrid;     
}