<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion=1 AND codigoUnidad<>'0001' ORDER BY unidad";
	$arrPlanteles=$con->obtenerFilasArreglo($consulta);
	
?>
var arrPlanteles=<?php echo $arrPlanteles?>;
var mRegresar=false;
Ext.onReady(inicializar);

function inicializar()
{
	Ext.QuickTips.init();

	if(gE('mRegresar').value=='1')
    	mRegresar=true;
	var grid1=crearGridAsistencia();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:	[
                                            			{
                                                            xtype:'label',
                                                            html:'<span class="letraRojaSubrayada8" style="font-size:14px !important"><b>Control de asistencia</b></span></span>'
                                                        }
                                            		],
                                            items:	[
                                           				grid1
	                                           		]
                                        }
                                     ]
						}
                    )   

}

function crearGridAsistencia()
{


    var oConf=	{
                    idCombo:'cmbProfesor',
                    posX:0,
                    posY:0,
                    raiz:'personas',
                    nRegistros:'num',
                    anchoCombo:300,
                    campoDesplegar:'Nombre',
                    campoID:'idUsuario',
                    campoHDestino:'idUsuario',
                    funcionBusqueda:13,
                    paginaProcesamiento:'../paginasFunciones/funcionesAuxiliares.php',
                    confVista:'<tpl for="."><div class="search-item">[{idUsuario}] {Paterno} {Materno} {Nom}</div></tpl>',
                    campos:	[
                                {name:'idUsuario'},
                                {name:'Nombre'},
                                {name: 'Paterno'},
                                {name: 'Materno'},
                                {name: 'Nom'}
                            ],
                    funcAntesCarga:function(dSet,combo)
                                {
                                    var tipoBusqueda=1;
                                    gE('idUsuario').value='-1';
                                    var aValor=combo.getRawValue();
                                    dSet.baseParams.criterio=aValor;
                                    dSet.baseParams.campoBusqueda=tipoBusqueda;
                                    dSet.baseParams.rol=bE("'5_0'");
                                    dSet.baseParams.cond=bE(' idUsuario in (SELECT DISTINCT idUsuario FROM 4520_grupos g,4519_asignacionProfesorGrupo a WHERE a.idGrupo=g.idGrupos AND '+
															' a.situacion=1 AND participacionPrincipal=1 AND g.Plantel=\''+gEx('cmbPlantel').getValue()+'\')');
                                    
                                },
                    funcElementoSel:function(combo,registro)
                                {
                                    gEx('gridEventosAsistencia').getStore().reload();
                                    gE('idUsuario').value=registro.get('idUsuario');
                                   
                                }  
                };
	var cmbEmpleado=crearComboExtAutocompletar(oConf);  


	var cmbPlantel=crearComboExt('cmbPlantel',arrPlanteles,0,0,250);
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'fecha', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'grupo'},
                                                        {name:'horario'},
		                                                {name: 'horaEntrada'},
                                                        {name: 'tRetardo'},
		                                                {name:'horaSalida'},
                                                        {name: 'tAnticipado'},
                                                        {name:'tratamientoEvento'},
                                                        {name: 'plantel'},
                                                        {name: 'tEvento'},
                                                        {name: 'hInicioBloque',type:'date',dateFormat:'H:i:s'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesProfesores.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'hInicioBloque', direction: 'ASC'},
                                                            groupField: 'fecha',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='61';
                                        proxy.baseParams.idUsuario=gE('idUsuario').value;
                                        proxy.baseParams.plantel=cmbPlantel.getValue();
                                        proxy.baseParams.fInicio=gEx('dteFechaInicio').getValue();
                                        proxy.baseParams.fFin=gEx('dteFechaFin').getValue();
                                        proxy.baseParams.ignoraTipo='0,1';
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                           
                                                            {
                                                                header:'Fecha',
                                                                width:100,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'fecha',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Hora entrada',
                                                                width:90,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'horaEntrada'
                                                            },
                                                            {
                                                                header:'Hora salida',
                                                                width:90,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'horaSalida'
                                                            },
                                                            {
                                                                header:'Grupo',
                                                                width:200,
                                                                align:'center',
                                                                css:'text-align:left !important;',
                                                                sortable:true,
                                                                dataIndex:'grupo'
                                                            },
                                                            {
                                                                header:'Plantel',
                                                                width:200,
                                                                align:'center',
                                                                css:'text-align:left !important;',
                                                                sortable:true,
                                                                dataIndex:'plantel'
                                                            },
                                                            {
                                                                header:'Horario',
                                                                width:110,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'horario'
                                                            },
                                                            {
                                                                header:'Tiempo Retardo',
                                                                width:120,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'tRetardo'
                                                            },
                                                            {
                                                                header:'Tiempo anticipado',
                                                                width:120,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'tAnticipado'
                                                            },
                                                            {
                                                                header:'Tipo evento',
                                                                width:380,
                                                                align:'center',
                                                                css:'text-align:left !important;',
                                                                sortable:true,
                                                                dataIndex:'tratamientoEvento'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridEventosAsistencia',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                tbar:	[

                                                                			
                                                                			
                                                                            {
                                                                            	xtype:'label',
                                                                                html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8" style="font-size:12px !important"><b>Plantel:</b></span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbPlantel,
                                                                            '-',
                                                                            {
                                                                            	xtype:'label',
                                                                                html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8" style="font-size:12px !important"><b>Empleado:</b></span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbEmpleado,
                                                                            '-',
                                                                            {
                                                                            	xtype:'label',
                                                                                html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8" style="font-size:12px !important"><b>Asistencia del:</b></span>&nbsp;&nbsp;'
                                                                            },
                                                                            {
                                                                            	xtype:'datefield',
                                                                                id:'dteFechaInicio',
                                                                                value:'<?php echo date("Y-m-d")?>'
                                                                            },
                                                                            {
                                                                            	xtype:'label',
                                                                                html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8" style="font-size:12px !important"><b>al:</b></span>&nbsp;&nbsp;'
                                                                            },
                                                                             {
                                                                            	xtype:'datefield',
                                                                                id:'dteFechaFin',
                                                                                value:'<?php echo date("Y-m-d")?>'
                                                                            },
                                                                            '-',
                                                                            {
                                                                                icon:'../images/arrow_refresh.PNG',
                                                                                cls:'x-btn-text-icon',
                                                                                tooltip:'Reprocesar eventos del periodo',
                                                                                handler:function()
                                                                                        {
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                		],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    hideGroupedColumn :true,
                                                                                                    enableGrouping :true,
                                                                                                    getRowClass:formatearFila
                                                                                                    
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function formatearFila(record, rowIndex, p, ds)
{
	var comp='';
	switch(record.get('tEvento'))
    {
    	case '1':
        	comp='asistencia';
        break;
        case '2':
        break;
        case '3':
        	comp='falta';
        break;
    }
    
	return 'x-grid3-row-expanded '+comp;
}