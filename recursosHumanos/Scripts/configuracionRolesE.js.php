<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$conCiclos="SELECT ciclo,ciclo FROM 550_cicloFiscal";
	$arregloCiclos=$con->obtenerFilasArreglo($conCiclos);
	
	$conServicios="SELECT id__465_tablaDinamica,servicioEnfermeria FROM _465_tablaDinamica ORDER BY servicioEnfermeria";
	$arregloServicios=$con->obtenerFilasArreglo($conServicios);
	
	$conPeriodos="SELECT noPeriodo,etiqueta FROM 9315_periodosRoles";
	$arregloPer=$con->obtenerFilasArreglo($conPeriodos);
	
	$conActivo="SELECT ciclo FROM 550_cicloFiscal WHERE STATUS=1";
	$activo=$con->obtenerValor($conActivo);
	
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridConfiguracion();
}

function crearGridConfiguracion()
{
    
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                                        	{name: 'idConfiguracion'},
                                                            {name: 'ciclo'},
                                                            {name: 'idServicio'},
                                                            {name: 'nomServicio'},
                                                            {name: 'idPeriodo'},
                                                            {name: 'nombrePeriodo'}
                                            		],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsConf=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nomServicio', direction: 'ASC'},
                                                            groupField: 'ciclo'
                                                        })                                      
     
	dsConf.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=14;
                                    }
                  );                  

    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
    											  [
                                                     new  Ext.grid.RowNumberer(),
                                                     {
                                                        header:'Servicio',
                                                        width:200,
                                                        sortable:true,
                                                        dataIndex:'nomServicio',
                                                        align:'left'
                                                     },
                                                     {
                                                          header:'Per&iacute;odo',
                                                          width:150,
                                                          sortable:true,
                                                          dataIndex:'nombrePeriodo',
                                                          align:'left'
                                                      },
                                                      {
                                                          header:'Ciclo',
                                                          width:80,
                                                          sortable:true,
                                                          dataIndex:'ciclo',
                                                          align:'left'
                                                      },
                                                      {
                                                          header:'',
                                                          width:80,
                                                          sortable:true,
                                                          dataIndex:'idConfiguracion',
                                                          align:'center',
                                                          renderer:function(val,meta,registro)
                                                          	       {
                                                                   		return'<a href="javascript:agregarPersonal('+val+')"><img height="13" width="13" src="../images/form_add.png" /></a>';
                                                                   }
                                                          
                                                      }
                                                  ]   
												);
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridConfiguraciones',
                                                            store:dsConf,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            renderTo:'gridC',
                                                            height:550,
                                                            width:650,
                                                            title:'Distribuciones de empleados por rol de enfermer&iacute;a configuradas',
                                                            tbar:
                                                            	[
                                                            		{
                                                                      text:'Agregar distribuci&oacute;n de empleados',
                                                                      icon:'../images/add.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                              {
                                                                                  agregarC();
                                                                              }
                                                                    }
                                                                    //,
//                                                                    {
//                                                                      text:'Modificar',
//                                                                      icon:'../images/pencil.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                              {
//                                                                                  modificarC();
//                                                                              }
//                                                                    },
//                                                                    {
//                                                                      text:'Quitar',
//                                                                      icon:'../images/cancel_round.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                              {
//                                                                                  eliminarC();
//                                                                              }
//                                                                    }
                                                                    
                                                                ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsConf.load()  ;
    //tblGridP.on('beforeedit',funcAbeforeEdit);
    //tblGridP.on('afteredit',funcAfterEdit);
    return tblGrid;   
    
}    

function agregarC()
{
     var cmbCiclo=crearComboExt('cmbCiclo',<?php echo $arregloCiclos?>,70,5,200);
     cmbCiclo.setValue(<?php echo $activo?>);
     var cmbServicio=crearComboExt('cmbServicio',<?php echo $arregloServicios?>,70,35,200);
    // cmbServicio.on('select',obtenerTurnos);
//     var comboTurno=crearComboExt('comboTurno',[],70,65,200);
     var cmbPeriodo=crearComboExt('cmbPeriodo',<?php echo $arregloPer?>,70,65,200);
     var form1=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 1',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           {
                                                             x:35,
                                                             y:10,
                                                             xtype:'label',
                                                             html:'Ciclo:'
                                                           },
                                                            cmbCiclo,
                                                           {
                                                             x:18,
                                                             y:40,
                                                             xtype:'label',
                                                             html:'Servicio:'
                                                           },
                                                           cmbServicio,
                                                           {
                                                             x:17,
                                                             y:70,
                                                             xtype:'label',
                                                             html:'Per&iacute;odo:'
                                                           },
                                                           cmbPeriodo
                                                        ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Agregar Configuraci&oacute;n',
										width:300,
										height:220,
										layout:'fit',
										buttonAlign:'center',
										items:[form1],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var ciclo=Ext.getCmp('cmbCiclo').getValue();
                                                                    if(ciclo=='')
                                                                    {
                                                                    	msgBox('Debe indicar el Ciclo');
                                                                        Ext.getCmp('cmbCiclo').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var servicio=Ext.getCmp('cmbServicio').getValue();
                                                                    if(servicio=='')
                                                                    {
                                                                    	msgBox('Debe indicar el Servicio');
                                                                        Ext.getCmp('cmbServicio').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var periodo=Ext.getCmp('cmbPeriodo').getValue();
                                                                    if(periodo=='')
                                                                    {
                                                                        msgBox('Debe indicar el Per&iacute;odo');
                                                                        Ext.getCmp('cmbPeriodo').focus();
                                                                        return;
                                                                    }
                                                                        
                                                                        
                                                                    
                                                                    function funcAjax()
                                                                    {
                                                                        var resp=peticion_http.responseText;
                                                                        arrResp=resp.split('|');
                                                                        if(arrResp[0]=='1')
                                                                        {
                                                                            //var almacen=Ext.getCmp('gridConfiguraciones').getStore();
    																		//almacen.reload();
                                                                            var arrP=[['idConfiguracion',arrResp[1]]];
																			enviarFormularioDatos('../recursosHumanos/distribucionPersonalRolE.php',arrP);
                                                                        }
                                                                        else
                                                                        {
                                                                            if(arrResp[0]=='2')
                                                                        	{
                                                                            	msgBox('Ya existe un registro con las caracteristicas seleccionadas');
                                                                                return;
                                                                            }   
                                                                            else
                                                                            {
                                                                            	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                    }
                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=16&idCiclo='+ciclo+'&idServicio='+servicio+'&idPeriodo='+periodo,true);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function obtenerTurnos(combo,fila,pos)
{
	var idServicio=combo.getValue();
    if(idServicio!=-1)
    {
        function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                Ext.getCmp('comboTurno').getStore().loadData(eval(arrResp[1]));
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=15&idServicio='+idServicio,true);
    }
}

function agregarPersonal(id)
{
	var arrP=[['idConfiguracion',id]];
	enviarFormularioDatos('../recursosHumanos/distribucionPersonalRolE.php',arrP);
}