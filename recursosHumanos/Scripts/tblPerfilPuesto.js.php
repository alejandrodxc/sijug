<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridPerfiles();
}

function crearGridPerfiles()
{
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPuesto'},
                                                                  {name: 'puesto'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesRecursosHumanos.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=8;
                                    }
                        );
   
    
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'puesto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Puesto',
															width:400,
															sortable:true,
															dataIndex:'puesto',
                                                            align:'left'
														},
                                                        {
															header:'',
															width:100,
															sortable:true,
                                                            align:'center',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:modificar('+registro.get('idPuesto')+')"><img height="13" width="13" src="../images/pencil.png" alt="Modificar" title="Modificar" />'
                                                                            	   +'&nbsp;&nbsp;<a href="javascript:borrar('+registro.get('idPuesto')+')"><img height="13" width="13" src="../images/cancel_round.png" alt="Eliminar" title="Eliminar" /></a>';
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPerfiles',
                                                            title:'Perfiles de puesto',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'gPerfiles',
                                                            height:750,
                                                            width:600,
                                                            plugins: [filters],
                                                            tbar:[
                                                            	   {
                                                        				 text:'Agregar Perfil',
                                                                          icon:'../images/add.png',
                                                                          cls:'x-btn-text-icon',
                                                                          handler:function()
                                                                              {
                                                                                  modificar(-1);
                                                                              }           
                                                                   }	
                                                                 ]
                                                        }
                                                    );
		
    dsRegistrosP.load()  ;
    return tblGridP;     
}


function modificar(idPuesto)
{
	var arrP=[['idPuesto',idPuesto]];
	enviarFormularioDatos('../recursosHumanos/perfilPuesto.php',arrP);
}

function borrar(idPuesto)
{
	function respMod(btnM)
    {
        if(btnM=='yes')
        {
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    Ext.getCmp('gridPerfiles').getStore().reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=9&idPuesto='+idPuesto,true);
		}
    }
    msgConfirm('Est&aacute; seguro de eliminar este registro?',respMod);
}