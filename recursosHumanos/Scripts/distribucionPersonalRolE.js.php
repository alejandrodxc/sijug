<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idServicio=$_GET["idServicio"];
	
	$conTurnos="SELECT id__738_tablaDinamica,txtTurno FROM _738_tablaDinamica t, _465_gridTurno g WHERE idTurno=id__738_tablaDinamica AND g.idReferencia=".$idServicio;
	$arregloTurnos=$con->obtenerFilasArreglo($conTurnos);
	
	$conCategorias="SELECT id__865_tablaDinamica FROM _865_tablaDinamica c,_465_gridServicioCategoria s
						WHERE  s.idReferencia=".$idServicio." AND id__865_tablaDinamica=idCategoria";
	//echo $conCategorias;
	$cadenaCat=$con->obtenerListaValores($conCategorias);	
	if($cadenaCat=="")
		$cadenaCat="-1";
	
	$conIdPusetos="SELECT DISTINCT(g.idPuesto),puesto FROM  _865_gridPuestosCategoria g ,819_puestosOrganigrama o 
					WHERE o.idPuesto=g.idPuesto AND idReferencia IN (".$cadenaCat.") ORDER BY puesto";
	//echo $conIdPusetos;
	$arregloPuestos=$con->obtenerFilasArreglo($conIdPusetos);
	
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridConfiguracion();
}

function crearGridConfiguracion()
{
    
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idEmpleadoRolEnfermeria',
                                            fields: [
                                                        	{name: 'idEmpleadoRolEnfermeria'},
                                                            {name: 'idUsuario'},
                                                            {name: 'idTurno'},
                                                            {name: 'nombre'},
                                                            {name: 'cod_Puesto'},
                                                            {name: 'puesto'},
                                                            {name: 'codigoUnidad'},
                                                            {name: 'unidad'},
                                                            {name: 'nombreTurno'}
                                            		],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsConf=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombre', direction: 'ASC'},
                                                            groupField: 'nombreTurno'
                                                        })                                      
     
	dsConf.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=15;
                                        proxy.baseParams.idServicio=gE('idConfiguracion').value;
                                    }
                  );                  

    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
    											  [
                                                     new  Ext.grid.RowNumberer(),
                                                     chkRow,
                                                     {
                                                        header:'Empleado',
                                                        width:180,
                                                        sortable:true,
                                                        dataIndex:'nombre',
                                                        align:'left'
                                                     },
                                                     {
                                                          header:'Departamento',
                                                          width:270,
                                                          sortable:true,
                                                          dataIndex:'unidad',
                                                          align:'left'
                                                      },
                                                      {
                                                          header:'Puesto',
                                                          width:120,
                                                          sortable:true,
                                                          dataIndex:'puesto',
                                                          align:'left'
                                                      },
                                                      {
                                                          header:'Codigo Puesto',
                                                          width:80,
                                                          sortable:true,
                                                          dataIndex:'cod_Puesto',
                                                          align:'center'
                                                      },
                                                      {
                                                          header:'Turno',
                                                          width:80,
                                                          sortable:true,
                                                          dataIndex:'nombreTurno',
                                                          align:'center'
                                                      }
                                                  ]   
												);
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPersonal',
                                                            store:dsConf,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            renderTo:'personalG',
                                                            height:750,
                                                            width:800,
                                                            tbar:
                                                            	[
                                                            		{
                                                                      text:'Agregar Empleado',
                                                                      icon:'../images/add.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                              {
                                                                                  agregarE();
                                                                              }
                                                                    },
                                                                    {
                                                                      text:'Remover empleado',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                              {
                                                                                  eliminarE();
                                                                              }
                                                                    }
                                                                    
                                                                ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsConf.load()  ;
    //tblGridP.on('beforeedit',funcAbeforeEdit);
    //tblGridP.on('afteredit',funcAfterEdit);
    return tblGrid;   
    
}  

function agregarE()
{
	var cmbPuesto=crearComboExt('cmbPuesto',<?php echo $arregloPuestos?>,80,5,300);
    cmbPuesto.on('select',cambiarPuesto);
    var gridE=crearGridEmpleados();
    var comboTurno=crearComboExt('comboTurno',<?php echo $arregloTurnos?>,350,415,200);
    
     var form1=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 1',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                           {
                                                             x:35,
                                                             y:10,
                                                             xtype:'label',
                                                             html:'Puesto:'
                                                           },
                                                            cmbPuesto,
                                                           {
                                                             x:0,
                                                             y:40,
                                                             xtype:'panel',
                                                             items:[
                                                             			gridE
                                                                   ]
                                                           },
                                                           {
                                                             x:35,
                                                             y:420,
                                                             xtype:'label',
                                                             html:'Considerar los empleados seleccioandos para el turno:'                                                           },
                                                            comboTurno                                                           
                                                        ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Agregar Empleados',
										width:640,
										height:520,
										layout:'fit',
										buttonAlign:'center',
										items:[form1],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var filas=Ext.getCmp('gridPersonalServicio').getSelectionModel().getSelections();
                                                                    var tamano=filas.length;
                                                                    if(tamano==0)
                                                                    {
                                                                    	msgBox('Debe seleccionar al menos un empleado');
                                                                        return;
                                                                    }
                                                                    
                                                                    var idPuesto=Ext.getCmp('cmbPuesto').getValue();
                                                                    if(idPuesto=='')
                                                                    {
                                                                    	msgBox('Debe indicar el Puesto');
                                                                        Ext.getCmp('cmbPuesto').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var idTurno=Ext.getCmp('comboTurno').getValue();
                                                                    if(idTurno=='')
                                                                    {
                                                                    	msgBox('Debe indicar el Turno');
                                                                        Ext.getCmp('comboTurno').focus();
                                                                        return;
                                                                    }
                                                                    
                                                                    var x;
                                                                    
                                                                    var cadena='';
                                                                    for(x=0;x<tamano;x++)
                                                                    {
                                                                    	var idUsuario=filas[x].get('idUsuario');
                                                                        var idCategoria=filas[x].get('idCategoria');
                                                                        var unidad=filas[x].get('unidad');
                                                                        
                                                                        if(cadena=='')
                                                                        	cadena=idUsuario+'_'+idCategoria;
                                                                        else
                                                                        	cadena+=','+idUsuario+'_'+idCategoria;    
                                                                    }
                                                                    
                                                                    function funcAjax()
                                                                    {
                                                                        var resp=peticion_http.responseText;
                                                                        arrResp=resp.split('|');
                                                                        if(arrResp[0]=='1')
                                                                        {
                                                                            gE('idPuesto').value=-1;
                                                                            var almacen=Ext.getCmp('gridPersonal').getStore();
    																		almacen.reload();
                                                                        }
                                                                        else
                                                                        {
                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                        }
                                                                    }
                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=18&idConfiguracion='+gE('idConfiguracion').value+'&idTurno='+idTurno+'&cadena='+cadena,true);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																gE('idPuesto').value=-1;
                                                                ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function crearGridEmpleados()
{
	 var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                                        	{name: 'idUsuario'},
                                                            {name: 'nombre'},
                                                            {name: 'idCategoria'},
                                                            {name: 'nombreCat'},
                                                            {name: 'unidad'}
                                            		],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsEmp=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombre', direction: 'ASC'},
                                                            groupField: 'unidad'
                                                        })                                      
     
	dsEmp.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=17;
                                        //proxy.baseParams.idServicio=gE('idServicio').value;
                                        proxy.baseParams.idPuesto=gE('idPuesto').value;
                                    }
                  );                  

    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
    											  [
                                                     new  Ext.grid.RowNumberer(),
                                                     chkRow,
                                                     {
                                                        header:'Empleado',
                                                        width:200,
                                                        sortable:true,
                                                        dataIndex:'nombre',
                                                        align:'left'
                                                     },
                                                      {
                                                          header:'Categoria',
                                                          width:120,
                                                          sortable:true,
                                                          dataIndex:'nombreCat',
                                                          align:'center'
                                                      },
                                                       {
                                                          header:'Departamento',
                                                          width:250,
                                                          sortable:true,
                                                          dataIndex:'unidad',
                                                          align:'left'
                                                      }
                                                      
                                                  ]   
												);
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPersonalServicio',
                                                            store:dsEmp,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            renderTo:'personalG',
                                                            height:350,
                                                            width:620,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsEmp.load()  ;
    return tblGrid;   
}

function cambiarPuesto(combo,fila,pos)
{
	gE('idPuesto').value=combo.getValue();
    Ext.getCmp('gridPersonalServicio').getStore().reload();
}

function eliminarE()
{
	 var filas=Ext.getCmp('gridPersonal').getSelectionModel().getSelections();
      var tamano=filas.length;
      if(tamano==0)
      {
          msgBox('Debe seleccionar al menos un empleado');
          return;
      }
      
      function resp(btn)
      {
    	if(btn=='yes')
        {
            var x;
            var cadena='';
            for(x=0;x<tamano;x++)
            {
                var id=filas[x].get('idEmpleadoRolEnfermeria');
                          
                if(cadena=='')
                    cadena=id;
                else
                    cadena+=','+id;    
            }
            
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    var almacen=Ext.getCmp('gridPersonal').getStore();
                    almacen.reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=19&cadena='+cadena,true);
        }
      }
      msgConfirm('Est&aacute; seguro de eliminar los registros seleccionados',resp);  
}