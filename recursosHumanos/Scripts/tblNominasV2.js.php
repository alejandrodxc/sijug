<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT(razonSocial,' ',apPaterno,' ',apMaterno),razonSocial) AS nombreEmpresa  FROM 6927_empresas WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1";
	$arrEmpresas=$con->obtenerFilasArreglo($consulta);
	
?>

var arrEmpresas=<?php echo $arrEmpresas?>;

function crearNuevaNomina()
{
	var cmbEmpresa=crearComboExt('cmbEmpresa',arrEmpresas,120,5,400);
    cmbEmpresa.on('select',function(cmb,registro)
    						{
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        cmbCertificado.reset();
                                        var arrDatos=eval(arrResp[1]);
                                        cmbCertificado.getStore().loadData(arrDatos);
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=62&idEmpresa='+registro.data.id,true);
                            }
    			)
    var cmbCertificado=crearComboExt('cmbCertificado',[],200,5,200);
    cmbCertificado.on('select',function(cmb,registro)
    						{
                            	
                              cmbSerie.reset();
                             
                              cmbSerie.getStore().loadData(registro.data.valorComp);
                                
                            }
    			)
    var cmbSerie=crearComboExt('cmbSerie',[],200,35,200);
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Empresa: <span style="color:#F00">*</span>'
                                                        },
                                                        cmbEmpresa,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:400,
                                                            id:'txtDescripcion'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Periodo del: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaInicio',
                                                            listeners:	{
                                                            				select:function()
                                                                            		{
                                                                                    	calcularDiferenciaDias();
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:240,
                                                            y:70,
                                                            html:'al'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaFin',
                                                            listeners:	{
                                                            				select:function()
                                                                            		{
                                                                                    	calcularDiferenciaDias();
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'N&uacute;m. d&iacute;s pagados: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:95,
                                                            xtype:'numberfield',
                                                            width:80,
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'txtNumDiaspagados'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Fecha de pago: <span style="color:#F00">*</span>'
                                                        },
                                                         {
                                                        	x:120,
                                                            y:125,
                                                            xtype:'datefield',
                                                            id:'dteFechaPago'
                                                        },
                                                        {
                                                        	x:5,
                                                            y:155,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Esta n&oacute;mina ser&aacute; timbrada utilizando:',
                                                            height:100,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Certificado de Sello Digital (CSD):'
                                                                        },
                                                                        cmbCertificado,
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Serie a aplicar:'
                                                                        },
                                                                        cmbSerie
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Nueva n&oacute;mina',
										width: 570,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtDescripcion=gEx('txtDescripcion');
                                                                        var dteFechaInicio=gEx('dteFechaInicio');
                                                                        var dteFechaFin=gEx('dteFechaFin');
                                                                        var txtNumDiaspagados=gEx('txtNumDiaspagados');
                                                                        var dteFechaPago=gEx('dteFechaPago');
                                                                        
                                                                        
                                                                        if(cmbEmpresa.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cmbEmpresa.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la empresa cuya n&oacute;mina desea generar',resp1);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(txtDescripcion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtDescripcion.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la descripci&oacute;n de la n&oacute;mina que desea generar',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaInicio.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaInicio.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de inicio del periodo de la n&oacute;mina que desea generar',resp3);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaFin.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteFechaFin.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de t&eacute;rmino del periodo de la n&oacute;mina que desea generar',resp4);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(txtNumDiaspagados.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	txtNumDiaspagados.focus();
                                                                            }
                                                                        	msgBox('Debe especificar el n&uacute;mero de dias pagados de la n&oacute;mina que desea generar',resp5);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaPago.getValue()=='')
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	dteFechaPago.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de pago de la n&oacute;mina que desea generar',resp6);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbCertificado.getValue()=='')
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	cmbCertificado.focus();
                                                                            }
                                                                        	msgBox('Debe especificar el certificado de sello digital con el cual la n&oacute;mina ser&aacute; timbrada',resp7);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(cmbSerie.getValue()=='')
                                                                        {
                                                                        	function resp8()
                                                                            {
                                                                            	cmbSerie.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la serie del sello digital con el cual la n&oacute;mina ser&aacute; timbrada',resp8);
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idCertificado":"'+cmbCertificado.getValue()+'","idSerie":"'+cmbSerie.getValue()+'","idEmpresa":"'+cmbEmpresa.getValue()+'","descripcion":"'+cv(txtDescripcion.getValue())+'","fechaInicio":"'+dteFechaInicio.getValue().format('Y-m-d')+
                                                                        			'","fechaFin":"'+dteFechaFin.getValue().format('Y-m-d')+'","fechaPago":"'+dteFechaPago.getValue().format('Y-m-d')+'","diasPagados":"'+txtNumDiaspagados.getValue()+'"}';
                                                                                    
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                var arrParam=[['idNomina',arrResp[1]]];
                                                                                enviarFormularioDatos('../recursosHumanos/nomina.php',arrParam);
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=59&cadObj='+cadObj,true);
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function calcularDiferenciaDias()
{
	var fechaInicioNominadte=gEx('dteFechaInicio');	
    var fechaFinNominadte=gEx('dteFechaFin');
    if((fechaInicioNominadte.getValue()!='') && (fechaFinNominadte.getValue()!=''))
    {
        gEx('txtNumDiaspagados').setValue(obtenerDiasDiferencia(fechaInicioNominadte.getValue().format('Y-m-d'),fechaFinNominadte.getValue().format('Y-m-d')));
       	
   	}
}