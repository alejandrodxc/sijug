<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
Ext.onReady(inicializar);
var ocultarBtnCerrar=false;
function inicializar()
{
	
    if(gE('idDepto').value!='-1')
    	ocultarBtnCerrar=true;

    crearGridEmpleados();   
}

function crearGridEmpleados()
{
	var arrUsuarios=eval(bD(gE('arrUsuarios').value));
    var lector=	new Ext.data.ArrayReader	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idUsuario'},
                                                                    {name: 'nombre'},
                                                                    {name: 'puesto'},
                                                                    {name: 'departamento'},
                                                                    {name: 'situacion'}
                                                                ]
                                                    }
                                                );

    
    var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            groupField: 'departamento',
                                                            autoLoad:true,
                                                            data:arrUsuarios
                                                        }) 
    
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var ciclo=gE('ciclo').value;
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Cve. Empleado',
															width:80,
															sortable:true,
															dataIndex:'idUsuario'
														},
														{
															header:'Nombre',
															width:200,
															sortable:true,
															dataIndex:'nombre'
														},
                                                        {
															header:'Puesto',
															width:230,
															sortable:true,
															dataIndex:'puesto'
														},
                                                        {
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'departamento'
														},
                                                         {
															header:'Situacion',
															width:60,
															sortable:true,
															dataIndex:'situacion',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	if((val=='')||(val=='0'))
                                                                        {
                                                                        	return '<a href="javascript:registrarRopaje(\''+bE(registro.get('idUsuario'))+'\',\''+ciclo+'\',\''+bE(nFila)+'\')"><img height="13" width="13" src="../images/cross.png" title="A&uacute;n no ha registrados informaci&oacute;n" alt="A&uacute;n no ha registrados informaci&oacute;n"></a>';
                                                                        }
                                                                        else
                                                                        {
                                                                        	return '<a href="javascript:registrarRopaje(\''+bE(registro.get('idUsuario'))+'\',\''+ciclo+'\',\''+bE(nFila)+'\')"><img height="13" width="13" src="../images/icon_big_tick.gif" title="Ya ha registrados informaci&oacute;n" alt="Ya ha registrados informaci&oacute;n"></a>';
                                                                        }
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'gridRopaje',
                                                            renderTo:'tblRopaje',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:460,
                                                            width:800,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnCerrar',
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Cerrar registro de ropaje',
                                                                            hidden:ocultarBtnCerrar,
                                                                            handler:function()
                                                                            		{
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        recargarPagina();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=38&ciclo='+gE('ciclo').value+'&departamento='+gE('depto').value,true);
                                                                                           	}
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer cerrar el registro de ropaje?<br /><b>Nota: </b>Una vez realizada esta acci&oacute;n no podr&aacute; modificar dicho registro',resp);
                                                                                        
                                                                                        
                                                                                    }
                                                                        }
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            ) 
                                                        }
                                                    );
	return 	tblGrid;		
}

function registrarRopaje(iU,c,nF)
{
	$.fancybox({
    			'href'				: '../recursosHumanos/registroRopajeUsr.php?iU='+iU+'&c='+c+'&nF='+nF+'&cPagina=sFrm=true',
				'title'    			: 'Registro de uniforme de empleado',			
				'width'				: 800,
				'height'			: 500,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
                'modal':true
			});	
}

function cerrarVentana()
{
	$.fancybox.close();
}