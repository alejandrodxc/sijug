<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idTipoPercepcion,CONCAT('[',clave,'] ',descripcion) FROM 681_tiposPercepcionSAT where idTipoPercepcion<>16  ORDER BY clave";
	$arrTiposPercepciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoDeduccion,CONCAT('[',clave,'] ',descripcion) FROM 682_tiposDeduccionSAT where idTipoDeduccion<>6 ORDER BY clave";
	$arrTiposDeducciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoIncapacidad,descripcion FROM 685_tipoIncapacidadSAT ORDER BY descripcion";
	$arrTiposIncapacidad=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idSituacion,descripcion,imagen FROM 712_situacionComprobantesFiscales";
	$arrSituacionComprobante=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idTipoPercepcion,CONCAT('[',clave,'] ',descripcion) FROM 681_tiposPercepcionSAT   ORDER BY clave";
	$arrTiposPercepcionesCompleta=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoDeduccion,CONCAT('[',clave,'] ',descripcion) FROM 682_tiposDeduccionSAT ORDER BY clave";
	$arrTiposDeduccionesCompleta=$con->obtenerFilasArreglo($consulta);
	
?>
var arrTiposPercepcionesCompleta=<?php echo $arrTiposPercepcionesCompleta?>;
var arrTiposDeduccionesCompleta=<?php echo $arrTiposDeduccionesCompleta?>;
var arrSituacionComprobante=<?php echo $arrSituacionComprobante?>;
var arrTiposPagoHorasExtra=[['2','Dobles'],['3','Triples']];
var arrTiposIncapacidad=<?php echo $arrTiposIncapacidad?>;
var arrTiposPercepciones=<?php echo $arrTiposPercepciones?>;
var arrTiposDeducciones=<?php echo $arrTiposDeducciones?>;

function modificarDatosNomina()
{
	
    var arrCertificados=eval(bD(gE('arrCertificados').value));
    var cmbCertificado=crearComboExt('cmbCertificado',arrCertificados,200,5,200);
    cmbCertificado.setValue(gE('idCertificado').value);
    cmbCertificado.on('select',function(cmb,registro)
    						{
                                  cmbSerie.setValue('');
                                  cmbSerie.getStore().loadData(registro.data.valorComp);
                            }
    			)
	var arrSeries= eval(bD(gE('arrSeries').value));               
    var cmbSerie=crearComboExt('cmbSerie',arrSeries,200,35,200);
    
    
    
    cmbSerie.setValue(gE('idSerie').value);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            html:'Descripci&oacute;n: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:400,
                                                            id:'txtDescripcion',
                                                            value:gE('tDescripcion').value
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Periodo del: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaInicio',
                                                            value:gE('tFInicio').value,
                                                            listeners:	{
                                                            				select:function()
                                                                            		{
                                                                                    	calcularDiferenciaDias();
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:240,
                                                            y:40,
                                                            html:'al'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaFin',
                                                            value:gE('tFFin').value,
                                                            listeners:	{
                                                            				select:function()
                                                                            		{
                                                                                    	calcularDiferenciaDias();
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'N&uacute;m. d&iacute;s pagados: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:65,
                                                            xtype:'numberfield',
                                                            width:80,
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'txtNumDiaspagados',
                                                            value:gE('tNumDiasPagados').value
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Fecha de pago: <span style="color:#F00">*</span>'
                                                        },
                                                         {
                                                        	x:120,
                                                            y:95,
                                                            xtype:'datefield',
                                                            id:'dteFechaPago',
                                                            value:gE('tFechaPago').value
                                                        },

                                                        {
                                                        	x:5,
                                                            y:125,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Esta n&oacute;mina ser&aacute; timbrada utilizando:',
                                                            height:100,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Certificado de Sello Digital (CSD):'
                                                                        },
                                                                        cmbCertificado,
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Serie a aplicar:'
                                                                        },
                                                                        cmbSerie
                                                            		]
                                                        }


													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Datos de la n&oacute;mina',
										width: 570,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtDescripcion=gEx('txtDescripcion');
                                                                        var dteFechaInicio=gEx('dteFechaInicio');
                                                                        var dteFechaFin=gEx('dteFechaFin');
                                                                        var txtNumDiaspagados=gEx('txtNumDiaspagados');
                                                                        var dteFechaPago=gEx('dteFechaPago');
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if(txtDescripcion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtDescripcion.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la descripci&oacute;n de la n&oacute;mina que desea generar',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaInicio.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaInicio.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de inicio del periodo de la n&oacute;mina que desea generar',resp3);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaFin.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteFechaFin.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de t&eacute;rmino del periodo de la n&oacute;mina que desea generar',resp4);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(txtNumDiaspagados.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	txtNumDiaspagados.focus();
                                                                            }
                                                                        	msgBox('Debe especificar el n&uacute;mero de dias pagados de la n&oacute;mina que desea generar',resp5);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaPago.getValue()=='')
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	dteFechaPago.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la fecha de pago de la n&oacute;mina que desea generar',resp6);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbCertificado.getValue()=='')
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	cmbCertificado.focus();
                                                                            }
                                                                        	msgBox('Debe especificar el certificado de sello digital con el cual la n&oacute;mina ser&aacute; timbrada',resp7);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(cmbSerie.getValue()=='')
                                                                        {
                                                                        	function resp8()
                                                                            {
                                                                            	cmbSerie.focus();
                                                                            }
                                                                        	msgBox('Debe especificar la serie del sello digital con el cual la n&oacute;mina ser&aacute; timbrada',resp8);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        var cadObj='{"idCertificado":"'+cmbCertificado.getValue()+'","idSerie":"'+cmbSerie.getValue()+'","idNomina":"'+gE('idNomina').value+'","descripcion":"'+cv(txtDescripcion.getValue())+'","fechaInicio":"'+dteFechaInicio.getValue().format('Y-m-d')+
                                                                        			'","fechaFin":"'+dteFechaFin.getValue().format('Y-m-d')+'","fechaPago":"'+dteFechaPago.getValue().format('Y-m-d')+'","diasPagados":"'+txtNumDiaspagados.getValue()+'"}';
                                                                                    
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	ventanaAM.close();
                                                                                recargarPagina();
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=59&cadObj='+cadObj,true);
                                                                                    
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function calcularDiferenciaDias()
{
	var fechaInicioNominadte=gEx('dteFechaInicio');	
    var fechaFinNominadte=gEx('dteFechaFin');
    if((fechaInicioNominadte.getValue()!='') && (fechaFinNominadte.getValue()!=''))
    {
    	
        gEx('txtNumDiaspagados').setValue(obtenerDiasDiferencia(fechaInicioNominadte.getValue().format('Y-m-d'),fechaFinNominadte.getValue().format('Y-m-d')));
       	
   	}
}

function formatearModificarNomina(val,meta,registro)
{
	if((registro.data.situacion=='1')||(registro.data.situacion=='5'))
		return '<a href="javascript:modificarDatosNominaEmpleado(\''+bE(val)+'\')"><img  src="../images/vcard_edit.png" title="Modificar nomina de empleado" alt="Modificar nomina de empleado"></a>'	;
    else
    {
    	return formatearRecibo(val,meta,registro);
    }
}

function modificarDatosNominaEmpleado(iN)
{
	var pos=obtenerPosFila(gEx('grid_tblTabla').getStore(),'idEmpleadoNomina',bD(iN));
    var filaEmpleado=gEx('grid_tblTabla').getStore().getAt(pos);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:0,
                                                            layout:'absolute',
                                                            xtype:'fieldset',
                                                            title:'Datos del empleado',
                                                            width:780,
                                                            height:105,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Empleado:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:120,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblNombreEmpleado" style="color:#000">['+filaEmpleado.data.rfc+'] '+filaEmpleado.data.apPaterno+' '+filaEmpleado.data.apMaterno+' '+filaEmpleado.data.nombre+'</span></b>'
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Total percepciones:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:120,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblTotalPercepciones" style="color:#900">$ 0.00</span></b>'
                                                                        },
                                                                        {
                                                                        	x:200,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000">(<b>Horas Extra:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:310,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblTotalHorasExtra" style="color:#900">$ 0.00</span>)</b>'
                                                                        }	,
                                                                        {
                                                                        	x:390,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Total deducciones:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:500,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblTotalDeducciones" style="color:#900">$ 0.00</span></b>'
                                                                        },
                                                                        {
                                                                        	x:580,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>(Incapacidades:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:690,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblTotalIncapacidades" style="color:#900">$ 0.00</span>)</b>'
                                                                        },

                                                                        {
                                                                        	x:580,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Sueldo Neto:</b></span>'
                                                                        },
                                                                        {
                                                                        	x:690,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<b><span id="lblSueldoNeto" style="color:#030">$ 0.00</span></b>'
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:50,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Salario Diario Integrado:</b></span>'
                                                                        }	,
                                                                        {
                                                                        	x:145,
                                                                            y:45,
                                                                            width:50,
                                                                            xtype:'numberfield',
                                                                            allowDecimals:true,
                                                                            allowNegative:false,
                                                                            id:'txtSDI',
                                                                            value:filaEmpleado.data.sdi
                                                                        }	
                                                            		]
                                                        },
                                                        {
                                                        	x:10,
                                                            y:110,
                                                            xtype:'tabpanel',
                                                            id:'tblTab',
                                                            height:340,
                                                            width:780,
                                                            activeTab:1,
                                                            items:	[
                                                            			{
                                                                        	xtype:'panel',
                                                                            layout:'absolute',
                                                                            baseCls: 'x-plain',
                                                                            title:'Percepciones/Deducci&oacute;nes',
                                                                            items:	[
                                                                            			crearGridPercepciones(),
                                                                                        crearGridDeducciones()
                                                                            		]
                                                                        },
                                                                        {
                                                                        	xtype:'panel',
                                                                            layout:'absolute',
                                                                            title:'Incapacidades/Horas Extra',
                                                                            items:	[
                                                                            			crearGridIncapacidades(),
                                                                                        crearGridHorasExtra()
                                                                            		]
                                                                        }
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar N&oacute;mina de empleado',
										width: 820,
										height:540,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                        var gPercepciones=gEx('gPercepciones');
                                                                        var gDeducciones=gEx('gDeducciones');
                                                                        var gIncapacidades=gEx('gIncapacidades');
                                                                        var gHorasExtra=gEx('gHorasExtra');
                                                                        
                                                                        var arrPercepciones='';
                                                                        var x;
                                                                        var fila;
                                                                        var o;
                                                                        for(x=0;x<gPercepciones.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gPercepciones.getStore().getAt(x);
                                                                            o='{"tipoPercepcion":"'+fila.data.tipoPercepcion+'","clave":"'+fila.data.clave+'","descripcion":"'+fila.data.descripcion+'","importeGravado":"'+fila.data.importeGravado+
                                                                            		'","importeExento":"'+fila.data.importeExento+'"}';
                                                                            if(arrPercepciones=='') 
                                                                            	arrPercepciones=o;
                                                                            else
                                                                            	arrPercepciones+=','+o;
                                                                        }
                                                                        arrPercepciones='['+arrPercepciones+']';
                                                                        
                                                                        var arrDeducciones='';
                                                                        for(x=0;x<gDeducciones.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gDeducciones.getStore().getAt(x);
                                                                            o='{"tipoDeduccion":"'+fila.data.tipoDeduccion+'","clave":"'+fila.data.clave+'","descripcion":"'+fila.data.descripcion+'","importeGravado":"'+fila.data.importeGravado+
                                                                            		'","importeExento":"'+fila.data.importeExento+'"}';
                                                                            if(arrDeducciones=='') 
                                                                            	arrDeducciones=o;
                                                                            else
                                                                            	arrDeducciones+=','+o;
                                                                        }
                                                                        arrDeducciones='['+arrDeducciones+']';
                                                                       
                                                                        var arrIncapacidades='';
                                                                        
                                                                        
                                                                        
                                                                        for(x=0;x<gIncapacidades.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gIncapacidades.getStore().getAt(x);
                                                                            o='{"importeGravado":"'+fila.data.importeGravado+'","importeExento":"'+fila.data.importeExento+'","diasIncapacidad":"'+fila.data.diasIncapacidad+'","tipoIncapacidad":"'+fila.data.tipoIncapacidad+'","descuentoIncapacidad":"'+fila.data.descuentoIncapacidad+'"}';
                                                                            if(arrIncapacidades=='') 
                                                                            	arrIncapacidades=o;
                                                                            else
                                                                            	arrIncapacidades+=','+o;
                                                                        }
                                                                        arrIncapacidades='['+arrIncapacidades+']';
                                                                        
                                                                        
                                                                        var arrHorasExtra='';
                                                                        for(x=0;x<gHorasExtra.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gHorasExtra.getStore().getAt(x);
                                                                            o='{"importeGravado":"'+fila.data.importeGravado+'","importeExento":"'+fila.data.importeExento+'","diasHorasExtra":"'+fila.data.diasHorasExtra+'","tipoPagoHoras":"'+fila.data.tipoPagoHoras+'","totalHorasExtra":"'+fila.data.totalHorasExtra+'","importePagado":"'+fila.data.importePagado+'"}';
                                                                            if(arrHorasExtra=='') 
                                                                            	arrHorasExtra=o;
                                                                            else
                                                                            	arrHorasExtra+=','+o;
                                                                        }
                                                                        arrHorasExtra='['+arrHorasExtra+']';
                                                                        
                                                                        var txtSDI=gEx('txtSDI');
                                                                        if(txtSDI.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtSDI.focus();
                                                                            }
                                                                            msgBox('Debe indicar el valor del Salario Diario Integrado (SDI)',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        var cadObj='{"sdi":"'+txtSDI.getValue()+'","idEmpleadoNomina":"'+bD(iN)+'","arrPercepciones":'+arrPercepciones+',"arrDeducciones":'+arrDeducciones+',"arrIncapacidades":'+arrIncapacidades+',"arrHorasExtra":'+arrHorasExtra+'}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	filaEmpleado.set('sdi',txtSDI.getValue());
                                                                                
                                                                                filaEmpleado.set('totalDeducciones',normalizarValor(gE('lblTotalDeducciones').innerHTML));
                                                                                filaEmpleado.set('montoIncapacidad',normalizarValor(gE('lblTotalIncapacidades').innerHTML));
                                                                                filaEmpleado.set('montoHorasExtra',normalizarValor(gE('lblTotalHorasExtra').innerHTML));
                                                                                filaEmpleado.set('totalPercepciones',normalizarValor(gE('lblTotalPercepciones').innerHTML));
                                                                                
                                                                                filaEmpleado.set('sueldoNeto',parseFloat(filaEmpleado.data.totalPercepciones)-parseFloat(filaEmpleado.data.totalDeducciones));
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=61&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    gEx('tblTab').setActiveTab(0);
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
         	var arrPercepciones=eval(arrResp[1]);   
            var arrDeducciones=eval(arrResp[2]);   
            var arrIncapacidades=eval(arrResp[3]);   
            var arrHorasExtra=eval(arrResp[4]);   
            
            gEx('gPercepciones').getStore().loadData(arrPercepciones);
            gEx('gDeducciones').getStore().loadData(arrDeducciones);
            gEx('gIncapacidades').getStore().loadData(arrIncapacidades);
            gEx('gHorasExtra').getStore().loadData(arrHorasExtra);
            calcularValoresNomina();
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=60&idEmpleadoNomina='+bD(iN),true);
    
}

function crearGridPercepciones()
{

	var crearCmbPercepcion=crearComboExt('crearCmbPercepcion',arrTiposPercepciones);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'tipoPercepcion'},
                                                                    {name: 'clave'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:1
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Tipo percepci&oacute;n <span style="color:#F00">*</span>',
															width:250,
															sortable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrTiposPercepcionesCompleta,val));
                                                                    },
															dataIndex:'tipoPercepcion',
                                                            editor:crearCmbPercepcion
														},
														{
															header:'Cve. percepci&oacute;n <span style="color:#F00">*</span>',
															width:100,
															sortable:true,
															dataIndex:'clave',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Descripci&oacute;n <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
                                                            renderer:mostrarValorDescripcion,
															dataIndex:'descripcion',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Imp. gravado <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Imp. exento <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            x:0,
                                                            y:0,
                                                            frame:false,
                                                            border:true,
                                                            id:'gPercepciones',
                                                            
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#900"><b>Percepciones aplicadas al empleado</b></span>'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddPercepcion',
                                                                            text:'Agregar percepci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'tipoPercepcion'},
                                                                                                                        {name: 'clave'},
                                                                                                                        {name: 'descripcion'},
                                                                                                                        {name: 'importeGravado'},
                                                                                                                        {name: 'importeExento'}
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	tipoPercepcion:'',
                                                                                                                clave:'',
                                                                                                                descripcion:'',
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddPercepcion').disable();
                                                                                        Ext.getCmp('btnDelPercepcion').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelPercepcion',
                                                                            text:'Remover percepci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la percepci&oacute;n que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                                calcularValoresNomina();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la percepci&oacute;n seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion)
    editorFila.on('validateedit',funcEditorValidaEdicion);
    editorFila.on('canceledit',funcEditorCancelEdicion);                                                    
    editorFila.on('afteredit',function()
    						{
                            	calcularValoresNomina();
                            }
    			);                                                    
                                                    
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion(rowEdit,obj,registro,nFila)
{
	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   Ext.getCmp('btnDelPercepcion').enable();
   Ext.getCmp('btnAddPercepcion').enable();

   return true;
}

function funcEditorCancelEdicion(rowEdit,cancelado)
{

	var idGrid='gPercepciones';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnDelPercepcion').enable();
    Ext.getCmp('btnAddPercepcion').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}

function crearGridDeducciones()
{

	var cmbDeduccion=crearComboExt('cmbDeduccion',arrTiposDeducciones);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'tipoDeduccion'},
                                                                    {name: 'clave'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:1
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Tipo deducci&oacute;n <span style="color:#F00">*</span>',
															width:250,
															sortable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrTiposDeduccionesCompleta,val));
                                                                    },
															dataIndex:'tipoDeduccion',
                                                            editor:cmbDeduccion
														},
														{
															header:'Cve. deducci&oacute;n <span style="color:#F00">*</span>',
															width:100,
															sortable:true,
															dataIndex:'clave',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Descripci&oacute;n <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
                                                            renderer:mostrarValorDescripcion,
															dataIndex:'descripcion',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Imp. gravado <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Imp. exento <span style="color:#F00"></span>',
															width:80,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            x:0,
                                                            y:155,
                                                            frame:false,
                                                            border:true,
                                                            id:'gDeducciones',
                                                            
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#900"><b>Deducciones aplicadas al empleado</b></span>'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddDeduccion',
                                                                            text:'Agregar deducci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'tipoDeduccion'},
                                                                                                                        {name: 'clave'},
                                                                                                                        {name: 'descripcion'},
                                                                                                                        {name: 'importeGravado'},
                                                                                                                        {name: 'importeExento'}
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	tipoDeduccion:'',
                                                                                                                clave:'',
                                                                                                                descripcion:'',
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddDeduccion').disable();
                                                                                        Ext.getCmp('btnDelDeduccion').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelDeduccion',
                                                                            text:'Remover deducci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la deducci&oacute;n que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                                calcularValoresNomina();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la deducci&oacute;n seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion2)
    editorFila.on('validateedit',funcEditorValidaEdicion2);
    editorFila.on('canceledit',funcEditorCancelEdicion2); 
    editorFila.on('afteredit',function()
    						{
                            	calcularValoresNomina();
                            }
    			);                                                       
                                                    
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion2(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion2(rowEdit,obj,registro,nFila)
{
	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   Ext.getCmp('btnAddDeduccion').enable();
   Ext.getCmp('btnDelDeduccion').enable();
   return true;
}

function funcEditorCancelEdicion2(rowEdit,cancelado)
{

	var idGrid='gDeducciones';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnAddDeduccion').enable();
   Ext.getCmp('btnDelDeduccion').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}


function crearGridIncapacidades()
{

	var cmbTipoIncapacidad=crearComboExt('cmbTipoIncapacidad',arrTiposIncapacidad);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'diasIncapacidad'},
                                                                    {name: 'tipoIncapacidad'},
                                                                    {name: 'descuentoIncapacidad'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:1
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'D&iacute;as de incapacidad <span style="color:#F00">*</span>',
															width:130,
															sortable:true,
                                                            dataIndex:'diasIncapacidad',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Tipo de incapacidad <span style="color:#F00">*</span>',
															width:150,
															sortable:true,
															dataIndex:'tipoIncapacidad',
                                                            editor:	cmbTipoIncapacidad,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTiposIncapacidad,val);
                                                                    }
														},
                                                        {
															header:'Importe descuento gravado',
															width:150,
															sortable:true,
                                                            dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
                                                       {
															header:'Importe descuento exento',
															width:150,
															sortable:true,
                                                            dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Total descuento<span style="color:#F00">*</span>',
															width:170,
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'descuentoIncapacidad'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            x:0,
                                                            y:0,
                                                            frame:false,
                                                            border:true,
                                                            id:'gIncapacidades',
                                                            
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#900"><b>Incapacidades aplicadas al empleado</b></span>'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddIncapacidad',
                                                                            text:'Agregar Incapacidad',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'diasIncapacidad'},
                                                                                                                        {name: 'tipoIncapacidad'},
                                                                                                                        {name: 'descuentoIncapacidad'},
                                                                                                                        {name: 'importeGravado'},
                                                                    													{name: 'importeExento'}	
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	diasIncapacidad:'',
                                                                                                                tipoIncapacidad:'',
                                                                                                                descuentoIncapacidad:0,
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddIncapacidad').disable();
                                                                                        Ext.getCmp('btnDelIncapacidad').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelIncapacidad',
                                                                            text:'Remover Incapacidad',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la incapcidad que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                                calcularValoresNomina();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la incapcidad seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion3)
    editorFila.on('validateedit',funcEditorValidaEdicion3);
    editorFila.on('canceledit',funcEditorCancelEdicion3);                                                    
	editorFila.on('afteredit',function()
    						{
                            	calcularValoresNomina();
                            }
    			);                                                        
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion3(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gIncapacidades';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion3(rowEdit,obj,registro,nFila)
{
	var idGrid='gIncapacidades';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   
   Ext.getCmp('btnAddIncapacidad').enable();
  	Ext.getCmp('btnDelIncapacidad').enable();     
   
   if(obj.importeGravado=='')
   		obj.importeGravado=0; 
   
   if(obj.importeExento=='')
   		obj.importeExento=0; 
   
   obj.descuentoIncapacidad=parseFloat(obj.importeGravado)+parseFloat(obj.importeExento);
   return true;
}

function funcEditorCancelEdicion3(rowEdit,cancelado)
{

	var idGrid='gIncapacidades';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnAddIncapacidad').enable();
  	Ext.getCmp('btnDelIncapacidad').enable(); 
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}

function crearGridHorasExtra()
{

	var cmbTipoPagoHorasExtra=crearComboExt('cmbTipoPagoHorasExtra',arrTiposPagoHorasExtra);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'diasHorasExtra'},
                                                                    {name: 'tipoPagoHoras'},
                                                                    {name: 'totalHorasExtra'},
                                                                    {name: 'importePagado'},
                                                                    {name: 'importeGravado'},
                                                                    {name: 'importeExento'}
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:1
                                                        }
                                                    );
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'D&iacute;as horas extra <span style="color:#F00">*</span>',
															width:105,
															sortable:true,
                                                            dataIndex:'diasHorasExtra',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
														{
															header:'Tipo de pago por hora extra <span style="color:#F00">*</span>',
															width:150,
															sortable:true,
															dataIndex:'tipoPagoHoras',
                                                            editor:	cmbTipoPagoHorasExtra,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTiposPagoHorasExtra,val);
                                                                    }
														},
                                                        {
															header:'Total horas extra <span style="color:#F00">*</span>',
															width:100,
															sortable:true,
                                                            dataIndex:'totalHorasExtra',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
                                                        {
															header:'Importe pagado gravado',
															width:130,
															sortable:true,
                                                            dataIndex:'importeGravado',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
                                                       {
															header:'Importe pagado exento',
															width:130,
															sortable:true,
                                                            dataIndex:'importeExento',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														},
                                                        
														{
															header:'Total pagado<span style="color:#F00"></span>',
															width:120,
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'importePagado'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            x:0,
                                                            y:155,
                                                            frame:false,
                                                            border:true,
                                                            id:'gHorasExtra',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:150,
                                                            width:780,
                                                            plugins:[editorFila],
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#900"><b>Horas extra aplicadas al empleado</b></span>'
                                                                        },'-',
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnAddHorasExt',
                                                                            text:'Agregar Hora Extra',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                        {name: 'diasHorasExtra'},
                                                                                                                        {name: 'tipoPagoHoras'},
                                                                                                                        {name: 'totalHorasExtra'},
                                                                                                                        {name: 'importePagado'},
                                                                                                                        {name: 'importeGravado'},
                                                                    													{name: 'importeExento'}
                                                                                                                    ]
                                                                                        						)
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	diasHorasExtra:'',
                                                                                                                tipoPagoHoras:'',
                                                                                                                totalHorasExtra:'',
                                                                                                                importePagado:'',
                                                                                                                importeGravado:0,
                                                                                                                importeExento:0
                                                                                                            }
                                                                                        				)    
                                                                                                        
                                                                                                        
                                                                                     	editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAddHorasExt').disable();
                                                                                        Ext.getCmp('btnDelHorasExt').disable();                                           
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnDelHorasExt',
                                                                            text:'Remover Hora Extra',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar las horas extra que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        		tblGrid.getStore().remove(fila);
                                                                                                calcularValoresNomina();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover las horas extra seleccionadas?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	editorFila.on('beforeedit',funcEditorFilaBeforeEdicion4)
    editorFila.on('validateedit',funcEditorValidaEdicion4);
    editorFila.on('canceledit',funcEditorCancelEdicion4);                                                    
	editorFila.on('afteredit',function()
    						{
                            	calcularValoresNomina();
                            }
    			);                                                        
	return 	tblGrid;		
}

function funcEditorFilaBeforeEdicion4(rowEdit,fila)
{
	/*if(gE('sL').value=='1')
    	return false;*/
	var idGrid='gHorasExtra';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion4(rowEdit,obj,registro,nFila)
{
	var idGrid='gHorasExtra';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
    
    
    
	var nColumnas=cm.getColumnCount(false);
    /*if(capturado)
    {
    	return;
    }
    capturado=true;*/
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   
   Ext.getCmp('btnAddHorasExt').enable();
  	Ext.getCmp('btnDelHorasExt').enable();     
    
	if(obj.importeGravado=='')
   		obj.importeGravado=0; 
   
   if(obj.importeExento=='')
   		obj.importeExento=0; 
   
   obj.importePagado=parseFloat(obj.importeGravado)+parseFloat(obj.importeExento);    
   return true;
}

function funcEditorCancelEdicion4(rowEdit,cancelado)
{

	var idGrid='gHorasExtra';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnAddHorasExt').enable();
  	Ext.getCmp('btnDelHorasExt').enable();  
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}


function calcularValoresNomina()
{
	calcularTotalIncapacidades();
    calcularTotalHorasExtra();
	calcularTotalPercepciones();
    calcularTotalDeducciones();
    
    var sueldoNeto=parseFloat(normalizarValor(gE('lblTotalPercepciones').innerHTML))-parseFloat(normalizarValor(gE('lblTotalDeducciones').innerHTML));
    gE('lblSueldoNeto').innerHTML=Ext.util.Format.usMoney(sueldoNeto);
}

function calcularTotalPercepciones()
{
	var totalPercepciones=0;
    
    var totalHorasExtra=parseFloat(normalizarValor(gE('lblTotalHorasExtra').innerHTML));
    
    var gPercepciones=gEx('gPercepciones');
    var fila;
    var x;
    for(x=0;x<gPercepciones.getStore().getCount();x++)
    {
    	fila=gPercepciones.getStore().getAt(x);
        totalPercepciones+=(parseFloat(fila.data.importeGravado)+parseFloat(fila.data.importeExento));
        
        
    }
    gE('lblTotalPercepciones').innerHTML=Ext.util.Format.usMoney(totalPercepciones+totalHorasExtra);
    
}

function calcularTotalDeducciones()
{
	var totalDeducciones=0;
    var totalIncapacidades=parseFloat(normalizarValor(gE('lblTotalIncapacidades').innerHTML))
    var gDeducciones=gEx('gDeducciones');
    var fila;
    var x;
    for(x=0;x<gDeducciones.getStore().getCount();x++)
    {
    	fila=gDeducciones.getStore().getAt(x);
        totalDeducciones+=(parseFloat(fila.data.importeGravado)+parseFloat(fila.data.importeExento));
        
        
    }
    gE('lblTotalDeducciones').innerHTML=Ext.util.Format.usMoney(totalDeducciones+totalIncapacidades);
    
}

function calcularTotalIncapacidades()
{
	var totalIncapacidad=0;
    var gIncapacidades=gEx('gIncapacidades');
    var fila;
    var x;
    for(x=0;x<gIncapacidades.getStore().getCount();x++)
    {
    	fila=gIncapacidades.getStore().getAt(x);
        totalIncapacidad+=parseFloat(fila.data.descuentoIncapacidad);
        
        
    }
    var gDeducciones=gEx('gDeducciones');
   
    
    
    gE('lblTotalIncapacidades').innerHTML=Ext.util.Format.usMoney(totalIncapacidad);
    
}

function calcularTotalHorasExtra()
{
	var totalHorasExtra=0;
    var gHorasExtra=gEx('gHorasExtra');
    var fila;
    var x;
    for(x=0;x<gHorasExtra.getStore().getCount();x++)
    {
    	fila=gHorasExtra.getStore().getAt(x);
        totalHorasExtra+=parseFloat(fila.data.importePagado);
        
        
    }
    gE('lblTotalHorasExtra').innerHTML=Ext.util.Format.usMoney(totalHorasExtra);
    
}


function formatearSituacionComprobante(val,meta,registro)
{
	var pos=existeValorMatriz(arrSituacionComprobante,val);
    var reg=arrSituacionComprobante[pos];
    return '<img src="'+arrSituacionComprobante[pos][2]+'" width="14" height="14"> '+arrSituacionComprobante[pos][1];
}

function formatearRecibo(val,meta,registro)
{
	if(registro.data.situacion!='3')
    {
		return '<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(registro.data.idFolio)+'"><img src="../images/Icono_xml.gif" title="Obtener XML del CFDI" alt="Obtener XML del CFDI" width="16" height="16"></a>&nbsp;&nbsp;<a href="javascript:mostrarReciboNomina(\''+bE(val)+'\')"><img src="../images/vcard.png" width="14" height="14" title="Ver recibo de n&oacute;mina" alt="Ver recibo de n&oacute;mina"> </a>';
    }
    
   
    
}

function mostrarReciboNomina(iE)
{
	var obj={};
    obj.titulo='Recibo de N&oacute;mina';
    obj.ancho=900;
    obj.alto=450;
    obj.params=[['iE',iE],['cPagina','sFrm=true']];
    obj.url='../formatosFacturasElectronicas/vistaPreviaReciboNomina.php';
    if(window.parent)
    	window.parent.abrirVentanaFancy(obj);
    else
    	abrirVentanaFancy(obj);
}	

function timbrarElementos()
{
	var grid_tblTabla=gEx('grid_tblTabla');
	var filas=grid_tblTabla.getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox('Al menos debe seleccionar un elemento a timbrar');
    	return;
   	}
    
    
    var listaEmpleados='';
    var x;
    var f;
    for(x=0;x<filas.length;x++)
    {
    	f=filas[x];
        if((f.data.situacion=='1')||(f.data.situacion=='5'))
        {
        	if(listaEmpleados=='')
            	listaEmpleados=f.data.idEmpleadoNomina;
            else
            	listaEmpleados+=','+f.data.idEmpleadoNomina;
        }
   	}
    
    if(listaEmpleados=='')
    {
    	msgBox('No existen elmenetos por timbrar');
    	return;
    }
    
    
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    grid_tblTabla.getStore().reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=63&listaEmpleados='+listaEmpleados,true);
        }
   	}
    msgConfirm('Est&aacute; seguro de querer timbrar los elemntos seleccionados?',resp)
    
}


function cancelarComprobantes()
{
	var grid_tblTabla=gEx('grid_tblTabla');
	var filas=grid_tblTabla.getSelectionModel().getSelections();
    
    var listaEmpleados='';
    var x;
    var f;
    for(x=0;x<filas.length;x++)
    {
    	f=filas[x];
        if(f.data.situacion=='2')
        {
        	if(listaEmpleados=='')
            	listaEmpleados=f.data.idFolio;
            else
            	listaEmpleados+=','+f.data.idFolio;
        }
   	}
    
    if(listaEmpleados=='')
    {
    	msgBox('Al menos debe seleccionar un elemento ya timbrado para cancelar');
    	return;
   	}
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo de la cancelaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:480,
                                                            height:100,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar CFDI',
										width: 530,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus('false,500');
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	if(gEx('txtMotivo').getValue()=='')
                                                                        {
                                                                        
                                                                        	function resp1()
                                                                            {
                                                                            	gEx('txtMotivo').focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo de la cancelaci&oacute;n',resp1);
                                                                            return;
                                                                        }
																		function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	grid_tblTabla.getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=48&motivo='+cv(gEx('txtMotivo').getValue())+'&iC='+listaEmpleados,true);
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar los elementos seleccioandos?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
}