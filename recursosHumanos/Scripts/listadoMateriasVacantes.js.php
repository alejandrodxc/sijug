<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select numEtapa,nombreEtapa FROM 4037_etapas WHERE idProceso=40";
	$arrEtapas=$con->obtenerFilasArreglo($consulta);
			
?>

Ext.onReady(inicializar);

var arrEtapas=<?php echo $arrEtapas?>;
var arrActores=[];
function inicializar()
{

    var dsDatos=eval(bD(gE('arrVacantes').value));
    
    var lector=new Ext.data.ArrayReader({fields:	[
                                                                    {name: 'idSolicitud'},
                                                                    {name: 'materia'},
                                                                    {name: 'idFormulario' },
                                                                    {name: 'idRegistro'},
                                                                    {name: 'etapa'},
                                                                    {name: 'programa'},
                                                                    {name: 'grupo'}
                                                                ]});
    
    
	var alDatos= new Ext.data.GroupingStore(
                                                          {
                                                              reader: lector,
                                                              remoteSort:false,
                                                              groupField:'programa',
                                                              data:dsDatos
                                                         }
                                                     ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Asignatura',
															width:350,
															sortable:true,
															dataIndex:'materia'
														},
                                                        {
															header:'Grupo',
															width:150,
															sortable:true,
															dataIndex:'grupo'
														},
                                                        {
                                                        	header:'Etapa',
															width:200,
															sortable:true,
															dataIndex:'etapa',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrEtapas,val);
                                                                    }
                                                            
                                                            
                                                        },
                                                        {
                                                        	header:'Ingresar proceso',
															width:120,
															sortable:true,
                                                            dataIndex:'idRegistro',
															renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	var img='<img width="13" height="13" src="../images/magnifier.png" />';
                                                                    	if(val!='')
                                                                        {
                                                                        	return '<a href="javascript:enviarRegistroPerfil(\''+bE(val)+'\',\''+bE(registro.get('etapa'))+'\')">'+img+'</a>';
                                                                        }
                                                                        else
                                                                        {
                                                                        
                                                                        	return '<a href="javascript:registrarPerfil(\''+bE(registro.get('idSolicitud'))+'\',\''+bE(nFila)+'\')">'+img+'</a>';
                                                                        
                                                                       	}
                                                                    }
                                                        },
                                                        {
															header:'Programa',
															width:350,
															sortable:true,
															dataIndex:'programa'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridVacantes',
                                                            store:alDatos,
                                                            frame:true,
															renderTo:'tblVacantes',
                                                            cm: cModelo,
                                                            height:600,
                                                            width:870,
                                                            view: new Ext.grid.GroupingView({
                                                                                            forceFit:false,
                                                                                            groupTextTpl: '<span class="letraFicha">{text}</span> (<font color="#000066">Total: </font> <font color="#FF0000">{values.rs.length}</font>)',
                                                                                            startCollapsed :false,
                                                                                            hideGroupedColumn:true
                                                                                        })
                                                            
                                                        }
                                                    );
    
}

function enviarRegistroPerfil(iR,et)
{
	var arrEt=new Array();
    var arrEtapas=eval(bD(gE('arrEtapas').value));
    var pos=existeValorMatriz(arrEtapas,parseFloat(bD(et)));

	if(pos!=-1)
    {
    	var fila=arrEtapas[pos];

        if(fila[2].length>1)
        {
        	mostrarVentanaSelActorProc(fila,iR);
        }
        else
        {
        	var arrParam;
        	if(fila[2].length==0)
	        	arrParam=[['dComp',bE('auto')],['actor',bE(0)],['idFormulario',290],['idRegistro',bD(iR)]];
            else
            	arrParam=[['dComp',bE('auto')],['actor',bE(fila[2][0][0])],['idFormulario',290],['idRegistro',bD(iR)]];
            window.open('',"vAuxiliar", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
            enviarFormularioDatos('../modeloPerfiles/vistaDTD.php',arrParam,'POST','vAuxiliar');
       	}
       
        
	}
}


function mostrarVentanaSelActorProc(fila,iR)
{
	var gridSelRol=crearRolSelRol(fila[2]);
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Se ha detectado que usted puede ingresar al proceso bajos los siguientes actores, por favor elija uno para continuar:'
                                                        },
														gridSelRol

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Seleccion de actor',
										width: 460,
										height:420,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var fila=gridSelRol.getSelectionModel().getSelected();
                                                                        if(fila==null)
                                                                        {
                                                                        	msgBox('Debe seleccionar el actor bajo el cual desea entrar al proceso');
                                                                        	return;
                                                                        }
																		var arrParam=[['dComp',bE('auto')],['actor',bE(fila.get('idActor'))],['idFormulario',290],['idRegistro',bD(iR)]];
                                                                        window.open('',"vAuxiliar", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
                                                                        enviarFormularioDatos('../modeloPerfiles/vistaDTD.php',arrParam,'POST','vAuxiliar');
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function crearRolSelRol(almacen)
{

    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idActor'},
                                                                {name: 'actor'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(almacen);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Actor',
															width:300,
															sortable:true,
															dataIndex:'actor'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            y:40,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:400,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;
}

function recargarGridVacantes(arrDatos)
{
	gEx('gridVacantes').getStore().loadData(arrDatos);
}

function funcionAntesCerrar()
{
	recargarPagina();
}

function regresar1PaginaContenedor()
{}


function registrarPerfil(iS,nFila)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var gridVacantes=gEx('gridVacantes');
            var fila=gridVacantes.getStore().getAt(bD(nFila));
            fila.set('idRegistro',arrResp[1]);
            fila.set('etapa','2.00');
            enviarRegistroPerfil(bE(arrResp[1]),bE('2'));
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=50&iS='+bD(iS),true);
    
}

