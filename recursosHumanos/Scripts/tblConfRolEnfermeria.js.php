<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridRolesP();
}

function crearGridRolesP()
{
    var lector=new Ext.data.JsonReader({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idRolE', type: 'int'},
                                                                  {name: 'periodo'},
                                                                 
                                                                  {name: 'idCategoria'},
                                                                  {name: 'categoria'}
                                                              ],         
                                                                                   
                                                    })
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesRecursosHumanos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idRolE', direction: 'ASC'},
                                                            groupField: 'periodo',
                                                            autoLoad:true
                                                        })                                                     
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	var cmb=gE('idCiclo');
                                        var ciclo=cmb.options[cmb.selectedIndex].value;
                                        cmb=gE('idServicio');
                                        var iS=cmb.options[cmb.selectedIndex].value;
                                    	proxy.baseParams.funcion=11;
                                        proxy.baseParams.idCiclo=ciclo;
                                        proxy.baseParams.idServicio=iS;
                                        
                                    }
                        );
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:40}),
														
                                                        {
															header:'Periodo:',
															width:200,
                                                            align:'left',
															sortable:true,
															dataIndex:'periodo'
                                                        },
                                                        
                                                        {
															header:'Categor&iacute;a:',
															width:280,
                                                            align:'left',
															sortable:true,
															dataIndex:'categoria'
														},
                                                        
                                                        {
															header:'',
															width:70,
															sortable:true,
                                                            align:'center',
                                                            dataIndex:'idRolVSConfigSemana',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var cmb=gE('idCiclo');
                                                                        var ciclo=cmb.options[cmb.selectedIndex].value;
                                                                        cmb=gE('idServicio');
                                                                        var iS=cmb.options[cmb.selectedIndex].value;
                                                                        return '<a href="javascript:verRol(\''+bE(registro.get('idRolE'))+'\',\''+bE(registro.get('idCategoria'))+'\',\''+bE(ciclo)+'\',\''+bE(iS)+'\')"><img src="../images/user_go.png" title="Ingresar a rol" alt="Ingresar a rol" width="13" /></a>';
                                                                        
	                                                                    	
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridRoles',
                                                            title:'Rol de Turnos y Descansos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'rolesAno',
                                                            height:500,
                                                            width:700,
                                                            loadMask :true,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                            	}   
                                                                                            ) 
                                                            
                                                        }
                                                    );
		
   
    return tblGridP;     
}

function recargaStore(combo)
{
	var cmb=gE('idCiclo');
    var ciclo=cmb.options[cmb.selectedIndex].value;
    cmb=gE('idServicio');
    var iS=cmb.options[cmb.selectedIndex].value;
    
    var almacen=Ext.getCmp('gridRoles').getStore().load({params:{funcion:11,idCiclo:ciclo,idServicio:iS}});
}

function modificar(id)
{
	var cmb=gE('idCiclo');
    var idCiclo=cmb.options[cmb.selectedIndex].value;
    var noSemanas=gE('nSemanas').value;
    
    var arrP=[['idRolConfE',id],['noSemanas',noSemanas],['idCiclo',idCiclo]];
    enviarFormularioDatos('../recursosHumanos/rolEnfermeria.php',arrP);
}

function verRol(iP,iC,c,iS)
{
	var arrParam=[['idPeriodo',bD(iP)],['idCategoria',bD(iC)],['ciclo',bD(c)],['idServicio',bD(iS)]];
    enviarFormularioDatos('../recursosHumanos/rolEnfermeria.php',arrParam);
}

function crearRol(o)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            verRol(bE(arrResp[1]));
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=15&obj='+bD(o),true);
}