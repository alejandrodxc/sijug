<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridIncidensias();
}
function crearGridIncidensias()
{
    var dsR=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'nomU'},
                                                                  {name: 'nombreU'},
                                                                  {name: 'incidensia'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsR.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=12;
                                        proxy.baseParams.fechaIni=gE('fechaI').value;
                                        proxy.baseParams.fechaFin=gE('fechaF').value;
                                        proxy.baseParams.idUsr=gE('idUsuario').value;
                                    }
                        );
   
    
  
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'nomU',
                                                            align:'left'
														},
                                                        {
															header:'Usuario',
															width:200,
                                                            align:'left',
															sortable:true,
															dataIndex:'nombreU'
                                                        },
                                                        {
															header:'Incidensia',
															width:250,
                                                            align:'left',
															sortable:true,
															dataIndex:'incidensia'
                                                           
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridUsuarios',
                                                            store:dsR,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'gridIns',
                                                            height:650,
                                                            width:800,
                                                            tbar:
                                                            	[
                                                                	{
                                                                      text:'Buscar Empleados',
                                                                      icon:'../images/magnifier.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                              {
                                                                                  buscarU();
                                                                              }
                                                                    },
                                                                    {
                                                                      text:'Limpiar',
                                                                      icon:'../images/arrow_refresh.PNG',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                              {
                                                                                  limpiar();
                                                                              }
                                                                    }
                                                                ]
                                                        }
                                                    );
		
    dsR.load()  ;
    return tblGridP;     
}


function buscarU()
{
	gE('idUsuario').value='-1';
    gE('cBusquedaP').value='1';
    var parametros2=	{
							funcion:'13',
							criterio:''
						};
    var comboPapa=inicializarCmbPadre(parametros2);
    var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
														 	new Ext.form.Label	(
																				 	{
																						x:55,
																						y:40,
																						text:'Persona: '
																					}
																				)
															,
															comboPapa,
															new Ext.form.Radio	(
																					{
																						x:5,
																						y:5,
																						id:'rdoPaterno',
																						boxLabel:'Ap. Paterno',
																						checked:true,
																						value:1
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:135,
																						y:5,
																						id:'rdoMaterno',
																						boxLabel:'Ap. Materno',
																						value:2
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:265,
																						y:5,
																						id:'rdoNombre',
																						boxLabel:'Nombre',
																						value:3
																					}
																				),
                                                                                {
                                                                                    xtype:'label',
                                                                                    html:'Fecha de inicio:',
                                                                                    x:18,
                                                                                    y:75
                                                                                },
                                                                                {
                                                                                    id:'dteFechaInicio',
                                                                                    xtype:'datefield',
                                                                                    format:'d/m/Y',
                                                                                    x:110,
                                                                                    y:70
                                                                                },
                                                                                {
                                                                                    xtype:'label',
                                                                                    html:'Fecha de fin:',
                                                                                    x:32,
                                                                                    y:110,
                                                                                    id:'lblFechaFin'
                                                                                },
                                                                                {
                                                                                    id:'dteFechaFin',
                                                                                    xtype:'datefield',
                                                                                    format:'d/m/Y',
                                                                                    x:110,
                                                                                    y:105
                                                                                }
                                                      ]
                                          }
                                       )
    ventana = new Ext.Window	(
									{
										title: lblAplicacion,
										width:470,
										height:220,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										listeners : {
														show : {
																	buffer : 500,
																	fn : function() 
																	{
																		comboPapa.focus();
																	}
																}
													},
										buttons:	[
														{
															text: 'Aceptar',
															handler:function()
																	{
                                                                        
                                                                        var dteFechaInicio=Ext.getCmp('dteFechaInicio');
                                                                        var fFin='';
                                                                        if(dteFechaInicio.getValue()=='')
                                                                        {
                                                                        	function respFI()
                                                                            {
                                                                            	dteFechaInicio.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio es obligatoria',respFI);
                                                                            return;
                                                                        }
                                                                        
                                                                         var dteFechaFin=Ext.getCmp('dteFechaFin');
                                                                            if(dteFechaFin.getValue()=='')
                                                                            {
                                                                                function respFF()
                                                                                {
                                                                                    dteFechaFin.focus();
                                                                                }
                                                                                msgBox('La fecha de t&eacute;rmino es obligatoria',respFF);
                                                                                return;
    
                                                                            }
                                                                            
                                                                            if(dteFechaInicio.getValue()>dteFechaFin.getValue())
                                                                            {
                                                                                function respFF2()
                                                                                {
                                                                                    dteFechaFin.focus();
                                                                                }
                                                                                msgBox('La fecha de t&eacute;rmino no puede ser menor a la fecha de inicio',respFF2);
                                                                                return;
                                                                            }
                                                                            
                                                                            
                                                                            var fIniComparar=dteFechaInicio.getValue().format('Y-m-d');
                                                                            gE('fechaI').value=fIniComparar;
                                                                            var fFinComparar=dteFechaFin.getValue().format('Y-m-d') ;
                                                                            gE('fechaF').value=fFinComparar;
                                                                            //alert(fIniComparar);
                                                                            //alert(fFinComparar);
                                                                            Ext.getCmp('gridUsuarios').getStore().reload();
                                                                           // if(fIniComparar===fFinComparar)
//                                                                            {
//                                                                            	msgBox('La fecha de inicio no puede ser igual a la fecha de fin');
//                                                                                return;
//                                                                            }
                                                                        ventana.close();
                                                                    }
                                                                    
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);                                                                                
                                                                                
                                                                                
	var rdoPaterno=Ext.getCmp('rdoPaterno');
	var rdoMaterno=Ext.getCmp('rdoMaterno');
	var rdoNombre=Ext.getCmp('rdoNombre');
	rdoPaterno.on('check',cambiarRadioSel);									
	rdoMaterno.on('check',cambiarRadioSel);									
	rdoNombre.on('check',cambiarRadioSel);							
    ventana.show();                                                                                
}

function cambiarRadioSel(chk, valor)
{
	if(valor==true)
	{
		var rdoPaterno=Ext.getCmp('rdoPaterno');
		var rdoMaterno=Ext.getCmp('rdoMaterno');
		var rdoNom=Ext.getCmp('rdoNombre');
		if(rdoPaterno.id!=chk.id)
			rdoPaterno.setValue(false);
		if(rdoMaterno.id!=chk.id)
			rdoMaterno.setValue(false);
		if(rdoNom.id!=chk.id)
			rdoNom.setValue(false);
		gE('cBusquedaP').value=chk.value;
	}
}

function inicializarCmbPadre(parametros2)
{

	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesAuxiliares.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'idUsuario',mapping:'idUsuario'},
                                                {name:'codigoUnidad',mapping:'codigoUnidad'}
											]
										);

	var ds=new Ext.data.Store	(
								 	{
										proxy:pPagina,
										reader:lector,
										baseParams:parametros2
									}
								 );
	
	function cargarDatos(dSet)
	{
		gE('idUsuario').value='-1';
		var aNombre=Ext.getCmp('cmbNombrePadre').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=gE('cBusquedaP').value;
       
	}
	
	ds.on('beforeload',cargarDatos);

	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br>',
										'</div></tpl>'
									 );
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:110,
														y:35,
														id:'cmbNombrePadre',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:320,
                                                        listWidth :320,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item'
														
													}
												 );
	
    function funcElemSeleccionado(combo,registro)
	{	
		var idUsuario=registro.get('idUsuario');
		gE('idUsuario').value=idUsuario;
    }
	comboNombre.on('select',funcElemSeleccionado);	
	return comboNombre;
}

function limpiar()
{
	 Ext.getCmp('gridUsuarios').getStore().removeAll();
}
