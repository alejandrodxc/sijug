<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	panel();
    crearGrid();
    crearAgrupados();
    crearRechazados();
}	


function panel() 
{
    var tabs = new Ext.TabPanel	(
									{
                                    	id:'idTab',
										renderTo: 'panel',
										activeTab: 0,
										width:950,
										height:650,
										items:	[	
                                                     {
                                                        contentEl:'tabla1', 
                                                        title:'Concentrado'
                                                     }
                                                     ,
                                                     {
                                                        contentEl:'tabla2', 
                                                        title:'Cursos Agrupados'
                                                     },
                                                     {
                                                        contentEl:'tabla3', 
                                                        title:'Cursos Rechazados'
                                                     }
												]
									}
								);
}

function crearGrid()
{
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idCurso',
                                            fields: [
                                               			{name: 'idCurso'},
                                                        {name: 'nombre'},
                                                        {name: 'noHoras'},
                                                        {name: 'costo'},
                                                        {name: 'idRubro'},
                                                        {name: 'nomRubro'},
                                                        {name: 'noPersonas'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomCategoria'},
                                                        {name: 'codUnidad'},
                                                        {name: 'nomUnidad'},
                                                        {name: 'participantes'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nomUnidad', direction: 'ASC'},
                                                            groupField: 'nomCategoria'
                                                        })                                      
     
	dsCursos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=21;
                                        proxy.baseParams.idCiclo=gE('idCicloH').value;
                                    }
                  ); 
    var summary = new Ext.ux.grid.HybridSummary();                                      
 
   
   var suma=0;
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'nomUnidad',
                                                            align:'left'
														},
                                                        {
															header:'Rubro',
															width:100,
															sortable:true,
															dataIndex:'nomRubro',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:300,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left',
                                                            summaryRenderer:function()
                                                                          {
                                                                              return "<b>Costo total:</b>";
                                                                          }
														},
                                                        {
															header:'Costo',
															width:110,
                                                            align:'left',
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'UsMoney',
                                                            summaryType:'sum'
                                                        },
                                                        {
															header:'No. personas',
															width:80,
                                                            align:'center',
															sortable:true,
															dataIndex:'noPersonas',
                                                            
														},
                                                        {
															header:'Participantes',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'participantes',
                                                            renderer:function(val,meta,registro)
                                                                     {
                                                                        var noPer=registro.get('noPersonas');
                                                                        if((noPer>0) && (noPer!=''))
                                                                        {
                                                                        	return'<a href="javascript:verParticipantes(\''+bE(val)+'\',\''+bE(registro.get('nomUnidad'))+'\',\''+bE(registro.get('nomCategoria'))+'\',\''+bE(registro.get('nomRubro'))+'\',\''+bE(registro.get('nombre'))+'\')"><img height="13" width="13" src="../images/users.png" /></a>'
                                                                        }   
                                                                        else
                                                                        {
                                                                        	return'0';
                                                                        }
                                                                     }
                                                        },
                                                        {
															header:'Categoria',
															width:100,
															sortable:true,
															dataIndex:'nomCategoria',
                                                            align:'left'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCursos',
                                                            title:'Concentrado de cursos',
                                                            store:dsCursos,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            renderTo:'gridC',
                                                            height:600,
                                                            width:940,
                                                            plugins: [summary],
                                                            tbar:[
                                                                  {
                                                                      text:'Agrupar Cursos',
                                                                      icon:'../images/bricks.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              agruparCursos();
                                                                          }
                                                                  },
                                                                  {
                                                                      text:'Agregar a grupo',
                                                                      icon:'../images/brick_add.jpg',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              agruparCursoGrupo();
                                                                          }
                                                                  },
                                                                  {
                                                                      text:'Quitar Cursos',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              quitarCursos();
                                                                          }
                                                                  }
                                                                  ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursos.load()  ;
    return grid;     
}

function verParticipantes(cadena,unidad,categoria,rubro,curso)
{
    var gridP=crearGridParticipantes(cadena);
    var form2=new Ext.form.FormPanel (
                                              {
                                                  baseCls: 'x-plain',
                                                  id:'formulario 2',
                                                  layout:'absolute',
                                                  disabled:false,
                                                  items:[		
                                                          {
                                                            xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<b>'+bD(unidad)+'</b>'
                                                          },
                                                          {
                                                            xtype:'label',
                                                            x:10,
                                                            y:30,
                                                            html:'<b>Curso:</b>&nbsp;'+bD(curso)
                                                          },
                                                          {
                                                            xtype:'label',
                                                            x:10,
                                                            y:50,
                                                            html:'<b>Categor&iacute;a:</b>&nbsp;'+bD(categoria)
                                                          },
                                                          {
                                                            xtype:'label',
                                                            x:10,
                                                            y:70,
                                                            html:'<b>Rubro:</b>&nbsp;'+bD(rubro)
                                                          },
                                                          {
                                                           	xtype:'panel',
                                                            x:0,
                                                            y:70,
                                                            items:[
                                                            		gridP
                                                                  ] 
                                                          } 
                                                        ]
                                              }
                                          )
    
    var ventana=new Ext.Window(
							   		{
										title:'Participantes',
										width:500,
										height:500,
										layout:'fit',
										buttonAlign:'center',
										items:[form2],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
													{
														text:'Cerrar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function crearGridParticipantes(cadena)
{
    var dsPar=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idUsuario'},
                                                                  {name: 'Nombre'},
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(
                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsPar.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=23;
                                        proxy.baseParams.cadena=bD(cadena);
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'Nombre' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
                                                                 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:35}),
                                                        {
															header:'Participante',
															width:400,
															sortable:true,
															dataIndex:'Nombre'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridParticipantes',
                                                            x:0,
                                                            y:0,
                                                            store:dsPar,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:430,
                                                            width:480,
                                                            plugins: [filters]
                                                        }
                                                    );
    dsPar.load()  ;
    return tblGridP;     
}

function recargarGrid()
{
	var cmb=gE('cmbCiclo');
    var idCiclo=cmb.options[cmb.selectedIndex].value;
    
    gE('idCicloH').value=idCiclo;
    
    Ext.getCmp('gridCursos').getStore().reload();
    Ext.getCmp('gridAgrupados').getStore().reload();
    Ext.getCmp('gridRechazados').getStore().reload();
}


function quitarCursos()
{
	var filas=Ext.getCmp('gridCursos').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    function resp(btn)
    {
    	if(btn=='yes')
        {
              var x;
              var cadena='';
              for(x=0;x< tamano;x++)
              {
                  var id=filas[x].get('idCurso');
                  
                  if(cadena=='')
                      cadena=id;
                  else
                      cadena+=','+id;
              }
              
              function funcAjax1()
              {
                  var resp=peticion_http.responseText.split('|');
                  if(resp[0]==1)
                  {
                       Ext.getCmp('gridCursos').getStore().reload();
                       Ext.getCmp('gridRechazados').getStore().reload();
                  }
                  else
                  {
                        Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                  }
              }
              obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=22&cadena='+cadena,true);
		}
    } 
    msgConfirm('Est&aacute; seguro de quitar los cursos seleccionados',resp);                 
}

function agruparCursos()
{
	var filas=Ext.getCmp('gridCursos').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    var x;
    var cadena='';
    var rubroA='';
    for(x=0;x< tamano;x++)
    {
    	if(rubroA=='')
        	rubroA=filas[x].get('idRubro');
        
        if(filas[x].get('idRubro')==rubroA)
        {
            var id=filas[x].get('idCurso');
            
            if(cadena=='')
                cadena=id;
            else
                cadena+=','+id;
        }        
        else
        {
        	msgBox('Los cursos que intenta agrupar no pertenecen al mismo rubro por lo tanto no puede realizar esta acci&oacute;n');
            return;
        }
    }
    
    agregarEtiqueta(cadena,rubroA);
}

function agregarEtiqueta(cadena,rubroA)
{
	 var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                      {
                                                         x:10,
                                                         y:10,
                                                         xtype:'label',
                                                         html:'Nombre:'
                                                      },
                                                      {
                                                         x:60,
                                                         y:5,
                                                         xtype:'textfield',
                                                         id:'nombreC',
                                                         width:350
                                                      }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Nombre de curso para este grupo',
										width: 440,
										height:130,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var nom=Ext.getCmp('nombreC').getValue();
                                                                                    if(nom==0)
                                                                                    {
                                                                                     	msgBox('Debe indicar el nombre');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var idCiclo=gE('idCicloH').value;
                                                                                    function funcAjax1()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             Ext.getCmp('gridCursos').getStore().reload();
                                                                                             Ext.getCmp('gridAgrupados').getStore().reload();
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=24&cadena='+cadena+'&nombre='+nom+'&idCiclo='+idCiclo+'&idRubro='+rubroA,true);
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
   ventana.show();                    
}


function crearAgrupados()
{
	 var lector= new Ext.data.JsonReader({
                                            idProperty:'noGrupo',
                                            fields: [
                                               			{name: 'noGrupo'},
                                                        {name: 'idRubro'},
                                                        {name: 'nombreRubro'},
                                                        {name: 'nombreGrupo'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomCategoria'},
                                                        {name: 'noPersonas'},
                                                        {name: 'costo'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursosA=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombreRubro', direction: 'ASC'},
                                                            groupField: 'nomCategoria'
                                                        })                                      
     
	dsCursosA.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=25;
                                        proxy.baseParams.idCiclo=gE('idCicloH').value;
                                    }
                  ); 
    var summary = new Ext.ux.grid.HybridSummary();                                      
 
   
   var suma=0;
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
															header:'Rubro',
															width:200,
															sortable:true,
															dataIndex:'nombreRubro',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:300,
															sortable:true,
															dataIndex:'nombreGrupo',
                                                            align:'left'
														},
                                                        {
															header:'No. personas',
															width:80,
                                                            align:'right',
															sortable:true,
															dataIndex:'noPersonas',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0');
                                                                    },	
                                                            
                                                            summaryRenderer:function()
                                                                          {
                                                                              return "<b>Costo total:</b>";
                                                                          }
														},
                                                        {
															header:'Costo',
															width:110,
                                                            align:'right',
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney',
                                                            editor:{xtype:'numberfield'},
                                                            summaryType:'sum'
                                                        },
                                                        {
															header:'',
															width:60,
                                                            align:'center',
															sortable:true,
															dataIndex:'noGrupo',
                                                            renderer:function(val,meta,registro) 
                                                            		 {
                                                                     	return'<a href="javascript:verAgrupados(\''+bE(val)+'\',\''+bE(registro.get('nomCategoria'))+'\',\''+bE(registro.get('nombreGrupo'))+'\')"><img height="13" width="13" src="../images/icon_code.gif" /></a>'
                                                                     }
                                                        },
                                                        {
															header:'Categoria',
															width:100,
															sortable:true,
															dataIndex:'nomCategoria',
                                                            align:'left'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridAgrupados',
                                                            title:'Concentrado de cursos',
                                                            store:dsCursosA,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            renderTo:'gridA',
                                                            height:600,
                                                            width:940,
                                                            plugins: [summary],
                                                            tbar:[
                                                                  {
                                                                      text:'Deshacer grupo',
                                                                      icon:'../images/cancel.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              deshacerGrupo();
                                                                          }
                                                                  }
                                                                  //,
//                                                                  {
//                                                                      text:'Quitar Cursos',
//                                                                      icon:'../images/cancel_round.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                          {
//                                                                              quitarCursos();
//                                                                          }
//                                                                  }
                                                                  ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursosA.load()  ;
    //tblGridP.on('beforeedit',funcAbeforeEdit);
    grid.on('afteredit',funcAfterEdit);
    return grid;     
}

function funcAfterEdit(e)
{
    if(e.value=='')
    {
    	msgBox('El valor ingresado no es valido');
        e.record.set('costo',e.originalValue);
        return;
    }
    var valor=e.value;
    var noGrupo=e.record.get('noGrupo');
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]==1)
        {
           
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax, 'POST','funcion=26&noGrupo='+noGrupo+'&valor='+valor,true);
}


function verAgrupados(noGrupo,categoria,nomGrupo)
{
    var gridAg=crearGridAgrupados(noGrupo);
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                      {
                                                         x:10,
                                                         y:10,
                                                         xtype:'label',
                                                         html:'<b>Nombre del Grupo:</b>'+bD(nomGrupo)
                                                      },
                                                      {
                                                         x:10,
                                                         y:30,
                                                         xtype:'label',
                                                         html:'<b>Categor&iacute;a:</b>'+bD(categoria)
                                                      },
                                                      {
                                                         x:0,
                                                         y:50,
                                                         xtype:'panel',
                                                         items:[
                                                            		gridAg
                                                                ] 
                                                      }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Cursos Agrupados',
										width: 600,
										height:530,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
   ventana.show();              
}

function crearGridAgrupados(noGrupo)
{
	//alert('ok');
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idCurso',
                                            fields: [
                                               			{name: 'idCurso'},
                                                        {name: 'unidad'},
                                                        {name: 'codigoUnidad'},
                                                        {name: 'txtCurso'},
                                                        {name: 'numpersonal'},
                                                        {name: 'nombreRubro'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursosAg=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'unidad', direction: 'ASC'},
                                                            groupField: 'nombreRubro'
                                                        })                                      
     
	dsCursosAg.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=27;
                                        proxy.baseParams.noGrupo=bD(noGrupo);
                                    }
                  ); 
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Departamento',
															width:280,
															sortable:true,
															dataIndex:'unidad',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:200,
															sortable:true,
															dataIndex:'txtCurso',
                                                            align:'left'
														},
                                                        {
															header:'No. Personas',
															width:80,
															sortable:true,
															dataIndex:'numpersonal',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		 {
                                                                     	return Ext.util.Format.number(val,'0');
                                                                     }
														},
                                                        {
															header:'Rubro',
															width:10,
															sortable:true,
															dataIndex:'nombreRubro',
                                                            align:'left'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridGrupo',
                                                            store:dsCursosAg,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:350,
                                                            width:580,
                                                            tbar:[
                                                                  {
                                                                      text:'Quitar Cursos',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              quitarCursoGrupo(noGrupo);
                                                                          }
                                                                  }
                                                                  ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursosAg.load()  ;
    return grid;     
}

function agruparCursoGrupo()
{
    var filas=Ext.getCmp('gridCursos').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    var gridGruposF=crearGruposFormados();
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                      {
                                                         x:0,
                                                         y:0,
                                                         xtype:'panel',
                                                         items:[
                                                            		gridGruposF
                                                                ] 
                                                      }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Grupos Formados',
										width: 500,
										height:500,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														
													    {
                                                            text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var fila=Ext.getCmp('gridAgrupadosAgregar').getSelectionModel().getSelected();
                                                                                    if(fila==undefined)
                                                                                    {
                                                                                     	msgBox('Debe seleccionar un grupo');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var idCiclo=gE('idCicloH').value;
                                                                                    var rubroA=fila.get('idRubro');
                                                                                    var nombre=fila.get('nombreGrupo');
                                                                                    var noGrupo=fila.get('noGrupo');
                                                                                    var x;
                                                                                    var cadena='';
                                                                                    
                                                                                    for(x=0;x< tamano;x++)
                                                                                    {
                                                                                        
                                                                                        if(filas[x].get('idRubro')==rubroA)
                                                                                        {
                                                                                            var id=filas[x].get('idCurso');
                                                                                            
                                                                                            if(cadena=='')
                                                                                                cadena=id;
                                                                                            else
                                                                                                cadena+=','+id;
                                                                                        }        
                                                                                        else
                                                                                        {
                                                                                            msgBox('Los cursos que intenta agrupar no pertenecen al mismo rubro por lo tanto no puede realizar esta acci&oacute;n');
                                                                                            return;
                                                                                        }
                                                                                    }
                                                                                    
                                                                                    function funcAjax1()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             Ext.getCmp('gridCursos').getStore().reload();
                                                                                             Ext.getCmp('gridAgrupados').getStore().reload();
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=28&cadena='+cadena+'&nombre='+nombre+'&idCiclo='+idCiclo+'&idRubro='+rubroA+'&noGrupo='+noGrupo,true);
																				}
																		}
														},
                                                        {
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
   ventana.show();      
}

function crearGruposFormados()
{
	var idCiclo=gE('idCicloH').value;
    var lector= new Ext.data.JsonReader({
                                            idProperty:'noGrupo',
                                            fields: [
                                               			{name: 'noGrupo'},
                                                        {name: 'idRubro'},
                                                        {name: 'nombreRubro'},
                                                        {name: 'nombreGrupo'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomCategoria'},
                                                        {name: 'noPersonas'},
                                                        {name: 'costo'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursosF=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombreRubro', direction: 'ASC'},
                                                            groupField: 'nomCategoria'
                                                        })                                      
     
	dsCursosF.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=25;
                                        proxy.baseParams.idCiclo=gE('idCicloH').value;
                                    }
                  ); 
 
   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
															header:'Rubro',
															width:200,
															sortable:true,
															dataIndex:'nombreRubro',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:300,
															sortable:true,
															dataIndex:'nombreGrupo',
                                                            align:'left'
														},
                                                        {
															header:'Categoria',
															width:100,
															sortable:true,
															dataIndex:'nomCategoria',
                                                            align:'left'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridAgrupadosAgregar',
                                                            store:dsCursosF,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:450,
                                                            width:500,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursosF.load()  ;
    return grid;     
}

function quitarCursoGrupo(noGrupo)
{
	var filas=Ext.getCmp('gridGrupo').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    function resp(btn)
    {
    	if(btn=='yes')
        {
            var x;
            var cadena='';
            for(x=0;x< tamano;x++)
            {
                var id=filas[x].get('idCurso');
                
                if(cadena=='')
                    cadena=id;
                else
                    cadena+=','+id;
            }
            
            function funcAjax1()
            {
                var resp=peticion_http.responseText.split('|');
                if(resp[0]==1)
                {
                     Ext.getCmp('gridGrupo').getStore().reload();
                     Ext.getCmp('gridCursos').getStore().reload();
                     Ext.getCmp('gridAgrupados').getStore().reload();
                     //ventana.close();
                }
                else
                {
                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=29&cadena='+cadena+'&noGrupo='+bD(noGrupo),true);
        }
    } 
    msgConfirm('Est&aacute; seguro de quitar del grupo los cursos seleccionados',resp);   
}

function deshacerGrupo()
{
	var filas=Ext.getCmp('gridAgrupados').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    function resp(btn)
    {
    	if(btn=='yes')
        {
            var x;
            var cadena='';
            for(x=0;x< tamano;x++)
            {
                var id=filas[x].get('noGrupo');
                
                if(cadena=='')
                    cadena=id;
                else
                    cadena+=','+id;
            }
            
            function funcAjax1()
            {
                var resp=peticion_http.responseText.split('|');
                if(resp[0]==1)
                {
                     Ext.getCmp('gridCursos').getStore().reload();
                     Ext.getCmp('gridAgrupados').getStore().reload();
                     //ventana.close();
                }
                else
                {
                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=30&cadena='+cadena,true);
        }
    } 
    msgConfirm('Est&aacute; seguro de deshacer los grupos seleccionados',resp);   
}

function crearRechazados()
{
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCurso',
                                            fields: [
                                               			{name: 'idCurso'},
                                                        {name: 'nombre'},
                                                        {name: 'noHoras'},
                                                        {name: 'costo'},
                                                        {name: 'idRubro'},
                                                        {name: 'nomRubro'},
                                                        {name: 'noPersonas'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomCategoria'},
                                                        {name: 'codUnidad'},
                                                        {name: 'nomUnidad'},
                                                        {name: 'participantes'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
    var dsCursosR=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAuxiliares.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nomUnidad', direction: 'ASC'},
                                                            groupField: 'nomCategoria'
                                                        })                                      
     
	dsCursosR.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=31;
                                        proxy.baseParams.idCiclo=gE('idCicloH').value;
                                    }
                  ); 
    var summary = new Ext.ux.grid.HybridSummary();                                      
 
   
   var suma=0;
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'nomUnidad',
                                                            align:'left'
														},
                                                        {
															header:'Rubro',
															width:100,
															sortable:true,
															dataIndex:'nomRubro',
                                                            align:'left'
														},
                                                        {
															header:'Curso',
															width:300,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left',
                                                            summaryRenderer:function()
                                                                          {
                                                                              return "<b>Costo total:</b>";
                                                                          }
														},
                                                        {
															header:'Costo',
															width:110,
                                                            align:'left',
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'UsMoney',
                                                            summaryType:'sum'
                                                        },
                                                        {
															header:'No. personas',
															width:80,
                                                            align:'center',
															sortable:true,
															dataIndex:'noPersonas',
                                                            
														},
                                                        {
															header:'Participantes',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'participantes',
                                                            renderer:function(val,meta,registro)
                                                                     {
                                                                        var noPer=registro.get('noPersonas');
                                                                        if((noPer>0) && (noPer!=''))
                                                                        {
                                                                        	return'<a href="javascript:verParticipantes(\''+bE(val)+'\',\''+bE(registro.get('nomUnidad'))+'\',\''+bE(registro.get('nomCategoria'))+'\',\''+bE(registro.get('nomRubro'))+'\',\''+bE(registro.get('nombre'))+'\')"><img height="13" width="13" src="../images/users.png" /></a>'
                                                                        }   
                                                                        else
                                                                        {
                                                                        	return'0';
                                                                        }
                                                                     }
                                                        },
                                                        {
															header:'Categoria',
															width:100,
															sortable:true,
															dataIndex:'nomCategoria',
                                                            align:'left'
														}
													]
												);
	var grid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridRechazados',
                                                            title:'Concentrado de cursos',
                                                            store:dsCursosR,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            renderTo:'gridR',
                                                            height:600,
                                                            width:940,
                                                            plugins: [summary],
                                                            tbar:[
                                                                  {
                                                                      text:'Regresar a concentrado',
                                                                      icon:'../images/table_row_insert.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              regresarCursos();
                                                                          }
                                                                  }
                                                                  ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
		
    dsCursosR.load()  ;
    return grid;     
}

function regresarCursos()
{
	var filas=Ext.getCmp('gridRechazados').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    function resp(btn)
    {
    	if(btn=='yes')
        {
            var x;
            var cadena='';
            for(x=0;x< tamano;x++)
            {
                var id=filas[x].get('idCurso');
                
                if(cadena=='')
                    cadena=id;
                else
                    cadena+=','+id;
            }
            
            function funcAjax1()
            {
                var resp=peticion_http.responseText.split('|');
                if(resp[0]==1)
                {
                     Ext.getCmp('gridCursos').getStore().reload();
                     Ext.getCmp('gridRechazados').getStore().reload();
                }
                else
                {
                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAuxiliares.php',funcAjax1, 'POST','funcion=32&cadena='+cadena,true);
        }
    } 
    msgConfirm('Est&aacute; seguro de regresar al concetrado los grupos seleccionados',resp);  
}