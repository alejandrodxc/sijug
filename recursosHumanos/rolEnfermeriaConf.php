<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");
function obtenerLetraDia($num)
{
	switch($num)
		{
			case 1:
				$letra="L";
			break;
			case 2:
				$letra="M";
			break;
			case 3:
				$letra="M";
			break;
			case 4:
				$letra="J";
			break;
			case 5:
				$letra="V";
			break;
			case 6:
				$letra="S";
			break;
			case 7:
				$letra="D";
			break;
		}
		return $letra;
}

function obtenerDisponibilidad($idUsuario,$fecha)
{
	global $con;
	
	$consulta="SELECT idJustificacion,fecha_Inicial,fecha_Final FROM 9106_Justificaciones WHERE idUsuario=".$idUsuario." AND fecha_Inicial>='".$fecha."'";
	$fila=$con->obtenerPrimeraFila($consulta);
	$valorRegreso="";
	if($fila)
	{
		if($fila[1]==$fecha)
		{
			$valorRegreso=1;
			return $valorRegreso;
		}
		else
		{
			$fechaEntrada=date('d/m/Y',strtotime($fecha));
			$fechaDia=strtotime($fila[1]);
			$fechaFin=strtotime($fila[2]);
			while($fechaDia<=$fechaFin)
			{
				if($fechaDia==$fechaEntrada)
				{
					$valorRegreso=1;
					break;
				}
				$fechaDia=strtotime("+ 1 days ",$fechaDia);
			}
			if($valorRegreso=="")
			{
				$valorRegreso=0;
			}
			
			return $valorRegreso;
		}
	}
	else
	{
		$valorRegreso=0;
		return $valorRegreso;
	}
	
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
?>
<link rel="stylesheet" type="text/css" href="../Scripts/Carousel/thickbox.css"/>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<script type="text/javascript" src="../Scripts/Carousel/thickbox.js"></script>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td>
                        <?php
						$idCiclo="2011";
						if(isset($objParametros->idCiclo))
						$idCiclo=$objParametros->idCiclo;
						
						$idRolConfE="30";
						if(isset($objParametros->idRolConfE))
						$idRolConfE=$objParametros->idRolConfE;
						
						$noSemanas="";
						if(isset($objParametros->noSemanas))
						$noSemanas=$objParametros->noSemanas;
						
						$conReferencia="SELECT id__464_tablaDinamica FROM _464_tablaDinamica WHERE ciclo=".$idCiclo;
						$referencia=$con->obtenerValor($conReferencia);
						if($referencia=="")
							$referencia="-1";
							
						$conFechas="SELECT fecha,fecha FROM _464_diasNoHabiles WHERE idReferencia=".$referencia;	
						$resF=$con->obtenerFilas($conFechas);
						$nFechas=$con->filasAfectadas;
						if($nFechas>0)
						{
							$arregloD=$con->obtenerFilasArreglo1D($conFechas);
						}
						else
						{
							$arregloD=null;
						}
						
						//var_dump($arregloD);
						
						$consulta="SELECT semanaInicio,fechaInicio,semanaFin,fechaFin,idRolE FROM 9314_instanciasRolEnfermeria WHERE idRolVSConfigSemana=".$idRolConfE;
						//echo $consulta;
						$fila=$con->obtenerPrimeraFila($consulta);
						$fIni=$fila[1];
						$fFin=$fila[3];
						
						$arregloSemanas=obtenerSemanasTrabajo($fila[1],$fila[3]);
						$tamano=sizeof($arregloSemanas);
						
						$conUsuarios="select idUsuario,Nombre from 800_usuarios where idUsuario>1028 and idUsuario<1689 order by nombre";
						$arregloEmpleados=$con->obtenerFilasArregloPHP($conUsuarios);
						$tamanoUsu=sizeof($arregloEmpleados);
						?>
                        <script type="text/javascript" src="../recursosHumanos/Scripts/rolEnfermeriaConf.js.php"></script>
                        <table width="100%">
                        	<tr>
                            	<td align="center">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	Hora:&nbsp;&nbsp;<?php echo date('H:i')?>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td>
                                             <span class="tituloPaginas">Rol de turnos y descansos</span>
                                             <br />
                                             <br />	
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td>
                                            	
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="20000">
                                    <table width="100%">
                                        <tr>
                                        	<td width="250" style=" border:solid; border-width:1px; border-color:#000">&nbsp;
                                            </td>
                                            <?php
											for($x=0;$x<$tamano;$x++)
											{
												$fechaInicioSemana=date('d/m/Y',$arregloSemanas[$x]['fechaI']);
												$fechaFinSemana=date('d/m/Y',$arregloSemanas[$x]['fechaF']);
												$noSem=$arregloSemanas[$x]['nSemana'];
											?>
                                            <td>
                                                <table width="100%" height="23">
                                                	<!--<tr>
                                                    	<td colspan="8" style=" border:solid; border-width:1px; border-color:#000">
                                                        <span class="letraExt">Semana:&nbsp;<?php //echo $noSem?>&nbsp;&nbsp;Del&nbsp;&nbsp;<?php //echo $fechaInicioSemana?>&nbsp;&nbsp;AL&nbsp;&nbsp;<?php //echo $fechaFinSemana?></span>
                                                        </td>
                                                    </tr>-->
                                                    <tr>
                                                    	<?php
														$fechaDia=$arregloSemanas[$x]['fechaI'];
														while($fechaDia<=$arregloSemanas[$x]['fechaF'])
														{
															  if(existeValor($arregloD,date('Y-m-d',$fechaDia)))
															  {
															  	  $estilo="style='background: #D700D7;border:solid; border-width:1px; border-color:#000'";
															  }
															  else
															  {
															  	  $estilo="style='border:solid; border-width:1px; border-color:#000'";
															  }
															  ?>
																  <td width="20" align="center" <?php echo $estilo?>>
																	  <?php echo obtenerLetraDia(date('N',$fechaDia))?>
																  </td>	
															  <?php
															  $fechaDia=strtotime("+ 1 days ",$fechaDia);
														}
														?>
                                                       <!-- <td style="background:#309;border:solid; border-width:1px; border-color:#000"" width="5">
                                                        &nbsp;&nbsp;
                                                        </td>-->
                                                	</tr>
                                                    <tr>
                                                    	<?php
														$fechaDia=$arregloSemanas[$x]['fechaI'];
														while($fechaDia<=$arregloSemanas[$x]['fechaF'])
														{
															 if(existeValor($arregloD,date('Y-m-d',$fechaDia)))
															  {
															  	  $estilo="style='background: #D700D7;border:solid; border-width:1px; border-color:#000'";
															  }
															  else
															  {
															  	  $estilo="style='border:solid; border-width:1px; border-color:#000'";
															  }
														?>
                                                              <td width="20" align="center" <?php echo $estilo ?>>
                                                                  <?php 
                                                                      $fechaF=date('d/m/Y',$fechaDia);
                                                                      $cadena=explode('/',$fechaF);
																	  echo $cadena[0];
															  	  ?>
                                                              </td>
															 <?php     
															$fechaDia=strtotime("+ 1 days ",$fechaDia);
														}
														?>
                                                       <!-- <td style="background:#309;border:solid; border-width:1px; border-color:#000"" width="5">
                                                        &nbsp;&nbsp;
                                                        </td>-->
                                                	</tr>
                                                </table>
                                            </td>
											<?php
											}
                                            ?>
                                        </tr>
										<?php 
										//$ct=0;
										for($c=0;$c<$tamanoUsu;$c++)
										{
										?>
                                        <tr height="23">
											<td width="250" class="letraExt" style=" border:solid; border-width:1px; border-color:#000">
                                            &nbsp;&nbsp;<a href="javascript:verFicha(<?php echo $arregloEmpleados[$c][0]?>)"><?php echo $arregloEmpleados[$c][1]?></a>
                                            </td>
											<?php
											for($x=0;$x<$tamano;$x++)
											{
												$fechaInicioSemana=date('d/m/Y',$arregloSemanas[$x]['fechaI']);
												$fechaFinSemana=date('d/m/Y',$arregloSemanas[$x]['fechaF']);
												$noSem=$arregloSemanas[$x]['nSemana'];
											?>
                                            <td valign="top" style="padding:0 px 0 px 0 px 0 px">
                                                <table width="100%" height="23" style=" border:solid; border-width:1px; border-color:#000;padding:0px">
                                                    <tr>
                                                    	<?php
														$fechaDia=$arregloSemanas[$x]['fechaI'];
														while($fechaDia<=$arregloSemanas[$x]['fechaF'])
														{
															if(existeValor($arregloD,date('Y-m-d',$fechaDia)))
															{
																$estilo="style='background: #D700D7;border:solid; border-width:1px; border-color:#000';padding:0 px 0 px 0 px 0 px'";
															}
															else
															{
																$estilo="style='border:solid; border-width:1px; border-color:#000';padding:0 px 0 px 0 px 0 px'";
															}
														?>
                                                            <td width="20" align="center" <?php echo $estilo?>>
                                                                <?php 
                                                                    $fechaF=date('d/m/Y',$fechaDia);
                                                                    
                                                                   // $muestra=obtenerDisponibilidad($arregloEmpleados[$c][0],cambiaraFechaMysql($fechaF));
//                                                                    $cadena=explode('/',$fechaF);
                                                                    //echo $cadena[0];
                                                                    $conExiste="SELECT idUsuarioVSRolE FROM 9315_registroRolEnfermeria WHERE idUsuario=".$arregloEmpleados[$c][0]." AND idRolVSConfigSemana=".$idRolConfE." AND fecha='".cambiaraFechaMysql($fechaF)."'";
                                                                    $existe=$con->obtenerValor($conExiste);
                                                                    if($existe=="")
                                                                    {
                                                                        $estado="";
                                                                    }
                                                                    else
                                                                    {
                                                                        $estado="checked=checked";
                                                                    }
                                                               // if($muestra==0)
//                                                                {
																	$conLabora="SELECT idUsuarioVSRolE FROM 9315_registroRolEnfermeria WHERE  idRolVSConfigSemana=30 AND fecha='".cambiaraFechaMysql($fechaF)."'";
																	//echo $conLabora;
																	$labora=$con->obtenerValor($conLabora);
																	if($labora!="")
																	{
																		$conRetardo="SELECT esRetardo FROM 9105_controlAsistencia WHERE idUsuario=".$arregloEmpleados[$c][0]." AND fecha='".cambiaraFechaMysql($fechaF)."' AND tipo=0";
																		$retardo=$con->obtenerValor($conRetardo);
																		if()
																	}
                                                                ?>
                                                                
                                                  <!--              <input type="checkbox" name="radioDia_<?php //echo $arregloEmpleados[$c][0]?>[]" id="<?php //echo $arregloEmpleados[$c][0]?>_<?php //echo $idRolConfE?>_<?php //echo cambiaraFechaMysql($fechaF)?>_<?php //echo $noSem ?>"  <?php //echo $estado?> />--> <!--//onchange="validarCondiciones(this)"/>-->
                                                                <?php
                                                                //}
                                                                ?>
                                                            </td>
                                                        <?php
															$fechaDia=strtotime("+ 1 days ",$fechaDia);
														}
														?>
                                                        <!--<td style="background:#309;border:solid; border-width:1px; border-color:#000" width="5">
                                                        &nbsp;&nbsp;
                                                        </td>-->
                                                	</tr>
                                                </table>
                                            </td>
											<?php
											}
                                            ?>
                                        </tr>
                                        <?php
										}
										?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="idRolTabla" name="idRolTabla" value="<?php echo $idRolConfE ?>" />
                        <input type="hidden" id="fIni" name="fIni" value="<?php echo $fIni ?>" />
                        <input type="hidden" id="fFin" name="fFin" value="<?php echo $fFin ?>" />
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
