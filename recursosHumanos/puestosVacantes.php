<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");

$fecha=date("d-m-Y");
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Puestos Vacantes</title>
<style type="text/css">
.fondoLinea {
	background-color: #3A7676;
}
.TITULO2 {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 24px;
	font-weight: bolder;
	color: #000;
}
.fecha {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 16px;
	font-style: italic;
	font-weight: bolder;
	color: #FF8000;
}
.depto {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-style: oblique;
	font-weight: bold;
	color: #000;
}
.puesto {
	font-family: "Times New Roman", Times, serif;
	font-size: 18px;
	font-style: oblique;
	font-weight: bold;
	color: #009;
}
</style>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td align="center"><img src="../images/logoINCMNSZ.jpg" />;</td>
  </tr>
  <tr>
    <td class="fondoLinea">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0">
  <tr>
    <td width="76%" align="center" class="TITULO2">PUESTOS VACANTES</td>
    <td width="24%" class="fecha" align="center"><?php echo $fecha ?></td>
  </tr>
  <tr>
  		<td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0">
<?php
	global $con;
	$consulta="SELECT o.unidad,p.cvePuesto,p.puesto FROM 817_organigrama AS o,819_puestosOrganigrama AS p, 653_unidadesOrgVSPuestos AS u 
				WHERE o.codigoUnidad=u.codUnidad AND  p.idPuesto=u.idPuesto AND u.situacion=0";
	$Registros=$con->obtenerFilas($consulta);
	  while ($row= mysql_fetch_row($Registros))
	  {
		  $unidad=$row[0];
		  $cvePuesto=$row[1];
		  $puesto=$row[2];
	?>
    <tr>
	<td width="14%" class="depto">Departamento :</td>
    <td width="38%" align="center" class="puesto"><?php echo $unidad?></td>
    <td width="13%" align="center" class="puesto"><?php echo $cvePuesto?></td>
    <td width="28%" align="center" class="puesto"><?php echo $puesto?></td>
    <td width="7%">&nbsp;</td>
    </tr>
    <?php

	  }
?>
</table>
</body>
</html>