<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");

function obtenerExistenciaProgramaDepto($idProducto,$idPrograma,$idDepartamento,$idAlmacen)
{
	global $con;
	$consulta="SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idPrograma=".$idPrograma." AND codigoUnidad='".$idDepartamento."' AND idAlmacen=".$idAlmacen;
	$existencia=$con->obtenerValor($consulta);
	if($existencia=="")
	{
		$existencia=0;
	} 
	return $existencia;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$pagRegresar="../principal/inicio.php";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	
?>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/EditableItem.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/RangeMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/ListMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GridFilters.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/Filter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/StringFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/DateFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/ListFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/NumericFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/BooleanFilter.js"></script>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
                                  ?>
                                  
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                    <?php
				   $idAlmacen="-1";
				   if(isset($objParametros->idAlmacen))
				   	  $idAlmacen=$objParametros->idAlmacen;
				   //$idAlmacen="20";
				   
				   $conCiclo="SELECT ciclo,ciclo FROM 550_cicloFiscal where status=1";  
				   $idCiclo=$con->obtenerValor($conCiclo);
				   if($idCiclo=="")
				   	$idCiclo="-1";
				   
				   $consulta="SELECT nombreAlmacen FROM 9030_almacenes WHERE idAlmacen=".$idAlmacen;
				   $nombreA=$con->obtenerValor($consulta);
				   
				   $consulta="SELECT idPedido,folioPedido,noFactura,txtRazonSocial2,date_format(horaInicio,'%h:%i %p') as horaI,idProveedor_ult FROM 9102_PedidoCabecera p,_405_tablaDinamica pr
							  WHERE fechaAgenda='".date('Y-m-d')."'  AND idAlmacen=".$idAlmacen." AND status_pedido=1 AND idProveedor_ult=id__405_tablaDinamica ORDER BY horaInicio";
				   
				   $res=$con->obtenerFilas($consulta);
				   $filasP=$con->filasAfectadas;
				   $arreglo="";
				   while($fila=mysql_fetch_row($res))
				   {
				   		$consulta2="SELECT  d.idProducto,nombreProducto,clave_Art,descripcion,cantidad,idPedidoDetalle,idMarca,modelo,costoUnitario,iva,idPedidoDetalle FROM 9103_PedidoDetalle d,9101_CatalogoProducto p WHERE idPedido=".$fila[0]." AND p.idProducto=d.idProducto";
						$res2=$con->obtenerFilas($consulta2);
						$nreg=$con->filasAfectadas;
						
						while($filaD=mysql_fetch_row($res2))
						{
							
							$conMarca="SELECT descripcion FROM _406_tablaDinamica WHERE id__406_tablaDinamica=".$filaD[6];
							$marca=$con->obtenerValor($conMarca);
							
							$total=0;
							$totalPieza=0;
							if(($filaD[8]!="") || ($filaD[8]!="")) //costoU 
							{
								$importeIva=0;
								if(($filaD[9]!="") || ($filaD[9]!=0)) //iva
								{
									$importeIva=$filaD[8]*".".$filaD[9];
								}
								$totalPieza=$filaD[8]+$importeIva;
								if(($filaD[4]!="") || ($filaD[4]!=0))
								{
									$total=$totalPieza*$fila[4];
								}
							}
							
							$arregloDist="";
							$cabeceraT="<table>";
							$cuerpo="";
							$existeCabecera=0;
							$cuerpoT="";
							
							$distribucion="SELECT unidad,tituloPrograma,cantidad,p.idPrograma,o.codigoUnidad FROM 817_organigrama o,517_programas p,9301_distribucionProducto d 
											WHERE  p.idPrograma=d.idPrograma AND o.codigoUnidad=d.codigoUnidad AND idPedidoDetalle=".$filaD[5];
							//echo $distribucion;
							$resD=$con->obtenerFilas($distribucion);				
							$numDist=$con->filasAfectadas;
							if($numDist>0)
							{
								$cuerpoT="<tr><td width=\'300\' class=\'letraRojaSubrayada8\'>Departamento</td><td width=\'450\' class=\'letraRojaSubrayada8\'>Programa</td><td width=\'80\' class=\'letraRojaSubrayada8\'>Cantidad</td><td width=\'80\' class=\'letraRojaSubrayada8\'>Existencia</td></tr>";
							}
							
							while($filaDis=mysql_fetch_row($resD))
							{
									$existenciaD=obtenerExistenciaProgramaDepto($filaDis[0],$filaDis[3],$filaDis[4],$idAlmacen);
									$cuerpo.="<tr><td width=\'300\'>".$filaDis[0]."</td><td width=\'450\'>".$filaDis[1]."</td><td width=\'80\'>".$filaDis[2]."</td><td width=\'80\'>".$existenciaD."</td></tr>";
							}
							
							$finalT='</table>';
							
							$arregloDist=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
							//echo $arregloDist;
						
							$obj="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$fila[3]."','".$fila[4]."','".$fila[4]."','".$arregloDist."','".cv($filaD[3])."']";
							
							if($arreglo=="")
								$arreglo=$obj;
							else
								$arreglo.=",".$obj;
						}
				   }
				   $arreglo="[".$arreglo."]";
				   
				   $arregloE="";
				   $consultaE="SELECT e.codigoUnidad,unidad,e.idPrograma,tituloPrograma 
							   FROM 517_programas pd,817_organigrama o,9305_fechasEntregasAlmacen e
							   WHERE fecha='".date('Y-m-d')."' AND idAlmacen=".$idAlmacen." AND e.codigoUnidad=o.codigoUnidad AND e.idPrograma=pd.idPrograma order by unidad";
				   $resE=$con->obtenerFilas($consultaE);		   
				   $nEntregas=$con->filasAfectadas;
				   while($fila=mysql_fetch_row($resE))
				   {
						$conProducto="SELECT e.idProducto,nombreProducto,cantidad FROM 9101_CatalogoProducto p,9305_fechasEntregasAlmacen e
									  WHERE p.idProducto=e.idProducto AND codigounidad='".$fila[0]."' AND idPrograma=".$fila[2]." AND fecha='".date('Y-m-d')."' AND e.idAlmacen=".$idAlmacen." ORDER BY nombreProducto";
						$resP=$con->obtenerFilas($conProducto);			  
						$numP=$con->filasAfectadas;
						
						$arregloP="";
						$cabeceraT="<table>";
						$cuerpo="";
						$existeCabecera=0;
						$cuerpoT="";
						
						
						if($numP>0)
						{
							$cuerpoT="<tr><td width=\'400\' class=\'letraRojaSubrayada8\'>Producto</td><td width=\'80\' class=\'letraRojaSubrayada8\'>Cantidad</td></tr>";
						}
						
						$clase="filaBlanca10";
						while($filaP=mysql_fetch_row($resP))
						{
							$cuerpo.="<tr><td width=\'500\' class=\'".$clase."\'>".$filaP[1]."</td><td width=\'80\' class=\'".$clase."\'>".$filaP[2]."</td></tr>";
							if($clase=="filaBlanca10")	
								$clase="filaRosa10";
							else
								$clase="filaBlanca10";
						}
						
						$finalT='</table>';
						
						$arregloP=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
					
						$obj="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$fila[3]."','".$arregloP."']";
						
						if($arregloE=="")
							$arregloE=$obj;
						else
							$arregloE.=",".$obj;
				   }
				   $arregloE="[".$arregloE."]";
				   
				    $arregloS="";
					$consulta="SELECT idCategoria FROM 9303_almacenVSCategoria WHERE idAlmacen=".$idAlmacen;
					$cadena=$con->obtenerListaValores($consulta);
					
					if($cadena=="")
					{
						$cadena="-1";
					}
					
					$conConceptos="SELECT claveConcepto,codigoControl FROM 9351_categoriaVSConcepto c,507_objetosGasto o WHERE claveConcepto=clave AND  idCategoria IN (".$cadena.") ORDER BY claveConcepto";
					$res3=$con->obtenerFilas($conConceptos);
					$consultaUnion="";
					$filasC=$con->filasAfectadas;
					$filasS=0;
					if($filasC>0)
					{
						$ct=0;
						if($filasS==1)
						{
							$filaC=$con->obtenerPrimeraFila($conConceptos);
							$conConcentrado="SELECT clave,p.idProducto,clave_Art,nombreProducto,descripcion,DATE_FORMAT(fechaCreacion,'%d/%m/%Y') AS fecha,codigoUnidad,txtcantidad,idEstado FROM 9101_CatalogoProducto p,507_objetosGasto o ,_604_tablaDinamica d WHERE  codigoControlPadre='".$filaC[1]."' AND clave=objetoGasto AND d.cmbProducto=p.idProducto AND ciclo=".$idCiclo." and (idEstado=1 or idEstado=2) ORDER BY clave,nombreProducto ";
						}
						else
						{
						  while($filaU=mysql_fetch_row($res3))
						  {
							  $union="(SELECT clave,p.idProducto,clave_Art,nombreProducto,descripcion,DATE_FORMAT(fechaCreacion,'%d/%m/%Y') AS fecha,codigoUnidad,txtcantidad,idEstado FROM 9101_CatalogoProducto p,507_objetosGasto o ,_604_tablaDinamica d WHERE  codigoControlPadre='".$filaU[1]."' AND clave=objetoGasto AND d.cmbProducto=p.idProducto AND ciclo=".$idCiclo." and (idEstado=1 or idEstado=2))"; 
							  if($consultaUnion=="")
								$consultaUnion=$union;
							  else
								$consultaUnion.="UNION ".$union;
						  }
						  $conConcentrado=$consultaUnion." ORDER BY fecha ASC ";
						}
						$respuesta=$con->obtenerFilas($conConcentrado);
						$filasS=$con->filasAfectadas;
						
							while($filaP=mysql_fetch_row($respuesta))
							{
								
								$conUnidad="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$filaP[6]."'";
								$unidad=$con->obtenerValor($conUnidad);
								
								$obj="['".$filaP[0]."','".$filaP[1]."','".$filaP[2]."','".$filaP[3]."','".$filaP[4]."','".$filaP[5]."','".$unidad."','".$filaP[6]."','".$filaP[7]."','".$filaP[8]."','".$idCiclo."']";
								//$obj='{"clave":"'.cv($filaP[0]).'","idProducto":"'.$filaP[1].'","clave_Art":"'.cv($filaP[2]).'","nombreProducto":"'.cv($filaP[3]).'","descripcion":"'.cv($filaP[4]).'","fechaE":"'.$filaP[5].'","unidad":"'.$unidad.'","codigoUnidad":"'.$filaP[6].'","cantidad":"'.$filaP[7].'","estado":"'.$filaP[8].'","idCiclo":"'.$idCiclo.'"}';	
								if($arregloS=="")
									$arregloS=$obj;
								else
									$arregloS.=",".$obj;
							}
					}
					$arregloS="[".$arregloS."]";
					
					$arregloSinA="";
					$consulta="SELECT idPedido,folioPedido,noFactura,txtRazonSocial2,date_format(horaInicio,'%h:%i %p') as horaI,idProveedor_ult FROM 9102_PedidoCabecera p,_405_tablaDinamica pr
							  WHERE fechaAgenda is null  AND idAlmacen=".$idAlmacen." AND status_pedido=1 AND idProveedor_ult=id__405_tablaDinamica ORDER BY horaInicio";
				   
				   	$res4=$con->obtenerFilas($consulta);
				    $filasSinA=$con->filasAfectadas;
					 while($fila=mysql_fetch_row($res4))
					 {
						  $consulta2="SELECT  d.idProducto,nombreProducto,clave_Art,descripcion,cantidad,idPedidoDetalle,idMarca,modelo,costoUnitario,iva,idPedidoDetalle FROM 9103_PedidoDetalle d,9101_CatalogoProducto p WHERE idPedido=".$fila[0]." AND p.idProducto=d.idProducto";
						  //echo $consulta2;
						  $res2=$con->obtenerFilas($consulta2);
						  $nreg=$con->filasAfectadas;
						  
						  while($filaD=mysql_fetch_row($res2))
						  {
							  
							  $conMarca="SELECT descripcion FROM _406_tablaDinamica WHERE id__406_tablaDinamica=".$filaD[6];
							  $marca=$con->obtenerValor($conMarca);
							  
							  $total=0;
							  $totalPieza=0;
							  if(($filaD[8]!="") || ($filaD[8]!="")) //costoU 
							  {
								  $importeIva=0;
								  if(($filaD[9]!="") || ($filaD[9]!=0)) //iva
								  {
									  $importeIva=$filaD[8]*$filaD[9];
								  }
								  $totalPieza=$filaD[8]+$importeIva;
								  if(($filaD[4]!="") || ($filaD[4]!=0))
								  {
									  $total=$totalPieza*$fila[4];
								  }
							  }
							  
							  $arregloDist="";
							  $cabeceraT="<table>";
							  $cuerpo="";
							  $existeCabecera=0;
							  $cuerpoT="";
							  
							  $distribucion="SELECT unidad,tituloPrograma,cantidad,p.idPrograma,o.codigoUnidad FROM 817_organigrama o,517_programas p,9301_distribucionProducto d 
											  WHERE  p.idPrograma=d.idPrograma AND o.codigoUnidad=d.codigoUnidad AND idPedidoDetalle=".$filaD[5];
							  //echo $distribucion;
							  $resD=$con->obtenerFilas($distribucion);				
							  $numDist=$con->filasAfectadas;
							  if($numDist>0)
							  {
								  $cuerpoT="<tr><td width=\'300\' class=\'letraRojaSubrayada8\'>Departamento</td><td width=\'450\' class=\'letraRojaSubrayada8\'>Programa</td><td width=\'80\' class=\'letraRojaSubrayada8\'>Cantidad</td><td width=\'80\' class=\'letraRojaSubrayada8\'>Existencia</td></tr>";
							  }
							  
							  while($filaDis=mysql_fetch_row($resD))
							  {
									  $existenciaD=obtenerExistenciaProgramaDepto($filaDis[0],$filaDis[3],$filaDis[4],$idAlmacen);
									  $cuerpo.="<tr><td width=\'300\'>".$filaDis[0]."</td><td width=\'450\'>".$filaDis[1]."</td><td width=\'80\'>".$filaDis[2]."</td><td width=\'80\'>".$existenciaD."</td></tr>";
							  }
							  
							  $finalT='</table>';
							  
							  $arregloDist=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
							  //echo $arregloDist;
						  
							  $obj="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$fila[3]."','".$fila[4]."','".$fila[4]."','".$arregloDist."','".cv($filaD[3])."']";
							  
							  if($arregloSinA=="")
								  $arregloSinA=$obj;
							  else
								  $arregloSinA.=",".$obj;
						  }
					 }
					$arregloSinA="[".$arregloSinA."]";
					
					$arregloExist="";
					$conExistenciasP="SELECT nombreProducto,c.idProducto,descripcion,resultado,clave_Art FROM (SELECT SUM(cantidad*operacion) AS resultado,idProducto FROM 9302_existenciaAlmacen WHERE idAlmacen=".$idAlmacen.") AS temp,9101_CatalogoProducto c
									  WHERE resultado<50 AND temp.idProducto=c.idProducto ORDER BY nombreProducto";
					$res5=$con->obtenerFilas($conExistenciasP);				  
					$nExist=$con->filasAfectadas;
					while($filaE=mysql_fetch_row($res5))
					{
						$obj="['".$filaE[0]."','".$filaE[1]."','".$filaE[2]."','".$filaE[3]."','".$filaE[4]."']";
							  
							  if($arregloExist=="")
								  $arregloExist=$obj;
							  else
								  $arregloExist.=",".$obj;
					}
									  
					$arregloExist="[".$arregloExist."]";
				   ?>
                   <tr>
                        <td align="center">
                        <br />
						<script type="text/javascript" src="../almacen/Scripts/reporteCentral.js.php"></script>
                        	<span class="tituloPaginas"><?php echo $nombreA?></span>
                            <br />
                            <br />
                            <br />
                            <table>
                            	<tr>
                                	<td align="left">
                                    	<table>
                                            <tr>
                                            	<td>
                                                	<span id="cReportes"></span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                        	<tr height="23">
                                            	<td align="left" width="300" class="letraRojaSubrayada8">
                                                	<span class="corpo8_bold"><img src="../images/bullet_green.png" />Pedidos por recibir</span>
                                                </td>
                                                <td align="right" width="100" class="letraRojaSubrayada8">
                                                <a href="javascript:verPedidos('<?php echo bE('Pedidos por recibir')?>')"><?php echo $filasP?></a>
                                                </td>
                                            </tr>
                                            <tr height="23">
                                            	<td align="left" width="300" class="letraRojaSubrayada8">
                                                	<span class="corpo8_bold"><img src="../images/bullet_green.png" />Entregas programadas</span>
                                                </td>
                                                <td align="right" width="100" class="letraRojaSubrayada8">
                                                <a href="javascript:verEntregas('<?php echo bE("Entregas programadas")?>')"><?php echo $nEntregas?></a>
                                                </td>
                                            </tr>
                                            <tr height="23">
                                            	<td align="left" width="300" class="letraRojaSubrayada8">
                                                	<span class="corpo8_bold"><img src="../images/bullet_green.png" />Solicitudes sin atender</span>
                                                </td>
                                                <td align="right" width="100" class="letraRojaSubrayada8">
                                                <a href="javascript:verSolicitudes('<?php echo bE('Solicitudes sin atender')?>')"><?php echo $filasS?></a>
                                                </td>
                                            </tr>
                                            <tr height="23">
                                            	<td align="left" width="300" class="letraRojaSubrayada8">
                                                	<span class="corpo8_bold"><img src="../images/bullet_green.png" />Pedidos por recibir sin atender</span>
                                                </td>
                                                <td align="right" width="100" class="letraRojaSubrayada8">
                                                <a href="javascript:verSinAgendar('<?php echo bE('Pedidos por recibir sin agendar')?>')"><?php echo $filasSinA?></a>
                                                </td>
                                            </tr>
                                            <tr height="23">
                                            	<td align="left" width="300" class="letraRojaSubrayada8">
                                                	<span class="corpo8_bold"><img src="../images/bullet_green.png" />Productos con baja existencia</span>
                                                </td>
                                                <td align="right" width="100" class="letraRojaSubrayada8">
                                                <a href="javascript:verExistencias('<?php echo bE('Productos con baja existencia')?>')"><?php echo $nExist?></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" id="idAlmacen" name="idAlmacen" value="<?php echo $idAlmacen ?>" />
                            <input type="hidden" id="fechaActual" name="fechaActual" value="<?php echo date("d/m/Y")?>" />
                            <input type="hidden" id="nombreA" name="nombreA" value="<?php echo $nombreA?>" />
                            <input type="hidden" id="arregloP" name="arregloP" value="<?php echo bE($arreglo)?>" />
                            <input type="hidden" id="arregloE" name="arregloE" value="<?php echo bE($arregloE)?>" />
                            <input type="hidden" id="arregloS" name="arregloS" value="<?php echo bE($arregloS)?>" />
                            <input type="hidden" id="arregloSinA" name="arregloSinA" value="<?php echo bE($arregloSinA)?>" />
                            <input type="hidden" id="arregloExist" name="arregloExist" value="<?php echo bE($arregloExist)?>" />
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                <?php
									if($soloContenido)
									{
										echo '<input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />';
										
									}
								?>
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
