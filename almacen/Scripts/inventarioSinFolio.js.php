<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridNoFoliado();
}

function crearGridNoFoliado()
{
    var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idTablaFormulario'},
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'ubicacion'},
                                                                  {name: 'idInventario'},
                                                                  {name: 'idFormulario'},
                                                                  {name: 'idDetallePedido'},
                                                                  {name: 'codigo'},
                                                                  {name: 'noFactura'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=51;
                                        proxy.baseParams.tipoI=2;
                                    }
                        );
   
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table>'
                                                                             +'<tr>'
                                                                                +'<td width="600" height="10"></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>No. Factura:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="copyrigthSinPadding"><b>{noFactura}</b></span><br /><br /></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Ubicaci&oacute;n:</b><br /></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{ubicacion}</span></td>'
                                                                             +'</tr>'
                                                                         +'</table>'
                                                                        
                                                					  )
                                            }); 
                                            
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 50,
                                                      store: dsRegiNoFol,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )                                                    
                                            
                                                                            
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
														{
															header:'Nombre Producto',
															width:560,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Fecha Recepci&oacute;n',
															width:100,
															sortable:true,
															dataIndex:'fechaRecepcion',
                                                            align:'center'
														},
                                                        {
															header:'',
															width:100,
															sortable:true,
                                                            align:'center',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:verDetalle('+registro.get('idTablaFormulario')+','+registro.get('idFormulario')+','+registro.get('idDetallePedido')+')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver detalles de producto" title="Ver detalles de producto" /></a>'
                                                                            +'&nbsp;&nbsp;<a href="javascript:generarCodigo('+registro.get('idInventario')+',\''+bE(registro.get('nombreProducto'))+'\')"><img height="13" width="13" src="../images/Settings1.jpg" alt="Registrar producto" title="Registrar producto" /></a>';
                                                                            //&nbsp;&nbsp;<a href="javascript:Cancelar('+registro.get('idPedido')+','+registro.get('folio')+')"><img height="13" width="13" src="../images/cancel_round.png" alt="Cancelar" title="Cancelar" /></a>';
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridNoFolio',
                                                            title:'',
                                                            store:dsRegiNoFol,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'cInventarioS',
                                                            height:400,
                                                            width:800,
                                                            bbar:[paginador],
                                                            plugins: [filters,expander]
                                                        }
                                                    );
		
    dsRegiNoFol.load()  ;
}


function verDetalle(idRegistro,idFormulario,idDetallePedido)
{
	var cPagina='sFrm=true';
	var arrParam=[['idFormulario',idFormulario],['idReferencia',idDetallePedido],['idRegistro',idRegistro],['cPagina',cPagina],['accionCancelar','window.close();'],['eJs',bE('window.opener();window.close();return;')]];
    
    window.open('',"vAuxiliar3", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
 	enviarFormularioDatosV("../modeloPerfiles/verFichaFormulario.php",arrParam,'POST','vAuxiliar3'); 
}

function generarCodigo(idInventario,nombreProd)
{
	
    function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
            var codigo=resp[1];
            mostrarVentanaInv(idInventario,nombreProd,resp[1]);
           // function funcAjax2()
//            {
//                var resp=peticion_http.responseText.split('|');
//                if(resp[0]==1)
//                {
//                     mostrarVentanaInv(idInventario,nombreProd,codigo);
//                }
//                else
//                {
//                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
//                }
//            }
//            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=52&idInventario='+idInventario+'&codigo='+codigo,true);
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=55&campoId='+idInventario,true)
}

function mostrarVentanaInv(idInventario,nombreProd,codigo)
{
	var panel=crearPanel(idInventario,nombreProd,codigo);
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'panel 1',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:10,
                                                            html:'<b>C&oacute;digo de Inventario: </b>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:140,
                                                            y:10,
                                                            width:120,
                                                            height:23,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+codigo+'</b></span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:80,
                                                            y:30,
                                                            html:'<b>Producto: </b>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:140,
                                                            y:30,
                                                            width:500,
                                                            height:23,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+bD(nombreProd)+'</b></span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:60,
                                                            html:'<span class="letraRojaSubrayada8">Nota:</span><span>Una vez registrado el c&oacute;digo este registro sera enviado al tablero de inventario foliado.</span>'
                                                        }
                                                        ,
                                                        {
                                                            xtype:'panel',
                                                            x:0,
                                                            y:85,
                                                            items:[
                                                            	  	panel
                                                                  ]
                                                        }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Registro de inventario',
										width: 650,
										height:430,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaDetalle',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
                                                                                       var almacen=Ext.getCmp('gridNoFolio').getStore();
                                                                                       almacen.reload(); 
                                                                                       ventana.close();
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}


function crearPanel(idInventario,nombreProd,codigo)
{
	var gridA=gridAsociados(idInventario,nombreProd,codigo);
    var gridCont=gridContabilidad(idInventario,nombreProd,codigo);
    var tabs = new Ext.TabPanel	(
									{
										activeTab: 0,
										width:620,
										height:300,
										items:	[	
												 	gridA,
                                                    gridCont
												]
									}
								);
     return  tabs;                         
}

function gridAsociados(idInventario,nombreProd,codigo)
{
    var dsRegiAsociados=new Ext.data.JsonStore({
                                              root: 'registros',
                                              totalProperty: 'numReg',
                                              fields: [
                                                        {name: 'idInventario'},
                                                        {name: 'idProducto'},
                                                        {name: 'nombreProducto'},
                                                        {name: 'descripcion'},
                                                        {name: 'codigo'},
                                                        {name: 'tipoAsociacion'}
                                                        
                                                    ],         
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {
                                                                                    url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                }
                                                                            )                             
                                          })
	dsRegiAsociados.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=53;
                                        proxy.baseParams.idInventario=idInventario;
                                    }
                        );
   
   var chkRow=new Ext.grid.CheckboxSelectionModel();
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table>'
                                                                             +'<tr>'
                                                                                +'<td width="600" height="10"></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                         +'</table>'
                                                                        
                                                					  )
                                            });                              
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														expander,
                                                        chkRow,
                                                        {
															header:'C&oacute;digo',
															width:80,
															sortable:true,
															dataIndex:'codigo',
                                                            align:'left'
														},
                                                        {
															header:'Nombre Producto',
															width:460,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														}
													]
												);
	var tblGridA=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridAsociados',
                                                            title:'Elementos asociados',
                                                            store:dsRegiAsociados,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            height:260,
                                                            width:600,
                                                            plugins: [expander],
                                                            tbar:[
                                                                    {
                                                                    text:'Asociar elemento Inventariado',
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    handler:function()
                                                                        {
                                                                            asociarElemento(idInventario);
                                                                        }
                                                                    }
                                                                    ,
                                                                    
                                                                    {
                                                                    text:'Asociar elemento Sin Inventariar',
                                                                    icon:'../images/area.png',
                                                                    cls:'x-btn-text-icon',
                                                                    handler:function()
                                                                        {
                                                                            asociarElementoSinInventariar(idInventario);
                                                                        }
                                                                    }
                                                                    ,
                                                                    {
                                                                        text:'Remover Elemento',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                removerElemento(idInventario);
                                                                            }
                                                                    }
                                                                 ]
                                                        }
                                                    );
		
    dsRegiAsociados.load()  ;
    return tblGridA;     
}

function asociarElemento(idInventario)
{
    var form1 = new Ext.form.FormPanel(	
                                    {
                                        id:'formulario1',
                                        baseCls: 'x-plain',
                                        layout:'absolute',
                                        defaultType: 'textfield',
                                        items: 	[ 
                                                    {
                                                        xtype:'label',
                                                        x:10,
                                                        y:5,
                                                        html:'<span class="letraRojaSubrayada8"><b>Ingrese el c&oacute;digo de inventario del elemento que desea asociar.</b></span>'
                                                    },
                                                    {
                                                        xtype:'label',
                                                        x:60,
                                                        y:55,
                                                        html:'<span><b>C&oacute;digo de Inventario:</b></span>'
                                                    },
                                                    {
                                                        xtype:'textfield',
                                                        x:190,
                                                        y:50,
                                                        width:150,
                                                        height:23,
                                                        id:'codigoInventario'
                                                    }
                                                ]
                                    }
                                );

	var ventana = new Ext.Window(
                                {
                                    title: 'Elemento Inventariado',
                                    width: 470,
                                    height:170,
                                    minWidth: 300,
                                    minHeight: 100,
                                    layout: 'fit',
                                    plain:true,
                                    modal:true,
                                    bodyStyle:'padding:5px;',
                                    buttonAlign:'center',
                                    items: form1,
                                    id:'ventana1',
                                    listeners : {
                                                show : {
                                                            buffer : 10,
                                                            fn : function() 
                                                            {
                                                                
                                                            }
                                                        }
                                            },
                                    buttons:	[
                                                    {
                                                        text: 'Aceptar',
                                                        listeners:	{
                                                                        click:function()
                                                                            {
                                                                                var codigo=Ext.getCmp('codigoInventario').getValue();
                                                                                if(codigo=='')
                                                                                {
                                                                                    msgBox('Debe indicar el c&oacute;digo del elemento que desea asociar');
                                                                                    return;
                                                                                }
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText.split('|');
                                                                                    if(resp[0]==1)
                                                                                    {
                                                                                          var idAsociado=resp[1];
                                                                                          function funcAjax2()
                                                                                          {
                                                                                              var resp=peticion_http.responseText.split('|');
                                                                                              if(resp[0]==1)
                                                                                              {
                                                                                                  var almacen=Ext.getCmp('gridAsociados').getStore();
                                                                                                  almacen.reload();
                                                                                                  ventana.close();
                                                                                              }
                                                                                              else
                                                                                              {
                                                                                                    Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                              }
                                                                                          }
                                                                                          obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=57&idAsociado='+idAsociado+'&idInventario='+idInventario,true);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                          if(resp[0]==2)
                                                                                          {
                                                                                               msgBox('El codigo ingresado no es valido');
                                                                                          }
                                                                                          else
                                                                                          {
                                                                                          	   Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                          }  
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=56&codigo='+codigo,true);
                                                                            }      
                                                                    }
                                                    },
                                                    {
                                                        text: 'Cancelar',
                                                        handler:function()
                                                                {
                                                                    ventana.close();
                                                                }
                                                    }
                                                ]        
                                }
                            );
    ventana.show();
}

function asociarElementoSinInventariar(idInventario)
{
    var form2 = new Ext.form.FormPanel(	
                                    {
                                        id:'formulario2',
                                        baseCls: 'x-plain',
                                        layout:'absolute',
                                        defaultType: 'textfield',
                                        items: 	[ 
                                                    {
                                                        xtype:'label',
                                                        x:10,
                                                        y:15,
                                                        html:'<span><b>Nombre:&nbsp;<font color="#FF0000">*</font></b></span>'
                                                    },
                                                    {
                                                        xtype:'textfield',
                                                        x:70,
                                                        y:10,
                                                        width:245,
                                                        height:23,
                                                        id:'nombreE'
                                                    },
                                                    {
                                                        xtype:'label',
                                                        x:10,
                                                        y:50,
                                                        html:'<span><b>Descripci&oacute;n:</b></span>'
                                                    },
                                                    {
                                                        xtype:'textarea',
                                                        x:10,
                                                        y:70,
                                                        width:305,
                                                        height:80, 
                                                        id:'descripcionE'
                                                    }
                                                ]
                                    }
                                );

	var ventana = new Ext.Window(
                                {
                                    title: 'Elemento asociado',
                                    width: 350,
                                    height:230,
                                    minWidth: 300,
                                    minHeight: 100,
                                    layout: 'fit',
                                    plain:true,
                                    modal:true,
                                    bodyStyle:'padding:5px;',
                                    buttonAlign:'center',
                                    items: form2,
                                    id:'ventana2',
                                    listeners : {
                                                show : {
                                                            buffer : 10,
                                                            fn : function() 
                                                            {
                                                                
                                                            }
                                                        }
                                            },
                                    buttons:	[
                                                    {
                                                        text: 'Aceptar',
                                                        listeners:	{
                                                                        click:function()
                                                                            {
                                                                                var nombreE=Ext.getCmp('nombreE').getValue();
                                                                                if(nombreE=='')
                                                                                {
                                                                                    msgBox('Debe indicar el nombre del nuevo elemento');
                                                                                    return;
                                                                                }
                                                                                var descripcionE=Ext.getCmp('descripcionE').getValue();
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText.split('|');
                                                                                    if(resp[0]==1)
                                                                                    {
                                                                                         var almacen=Ext.getCmp('gridAsociados').getStore();
                                                                                         almacen.reload(); 
                                                                                         ventana.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                          Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=54&nombreE='+nombreE+'&descripcionE='+descripcionE+'&idInventario='+idInventario,true);
                                                                            }      
                                                                    }
                                                    },
                                                    {
                                                        text: 'Cancelar',
                                                        handler:function()
                                                                {
                                                                    ventana.close();
                                                                }
                                                    }
                                                ]        
                                }
                            );
    ventana.show();
}


function removerElemento(idInventario)
{
    var filas=Ext.getCmp('gridAsociados').getSelectionModel().getSelections();
    var tamano=filas.length;
    
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    var cadena='';
    var x;
    for(x=0;x<tamano;x++)
    {
    	var idInv=filas[x].get('idInventario');
        if(cadena=='')
        	cadena=idInv;
        else
        	cadena+=','+idInv;    
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
             var almacen=Ext.getCmp('gridAsociados').getStore();
             almacen.reload(); 
             ventana.close();
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=58&nombreE='+nombreE+'&descripcionE='+descripcionE+'&idInventario='+idInventario,true);
}

function gridContabilidad(idInventario,nombreProd,codigo)
{
	var dsRegContab=new Ext.data.JsonStore({
                                              root: 'registros',
                                              totalProperty: 'numReg',
                                              fields: [
                                                        {name: 'idInventarioVSCuentas'},
                                                        {name: 'cuenta'},
                                                        {name: 'tituloCta'},
                                                        {name: 'porcentaje'},
                                                        {name: 'tipoAfectacion'}
                                                    ],         
                                              proxy : new Ext.data.HttpProxy(
                                                                                {
                                                                                    url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                }
                                                                            )                             
                                          })
	dsRegContab.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=59;
                                        proxy.baseParams.idInventario=idInventario;
                                        proxy.baseParams.codigo=codigo;
                                    }
                        );
   var chkRow=new Ext.grid.CheckboxSelectionModel();
   var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
															header:'C&oacute;digo',
															width:100,
															sortable:true,
															dataIndex:'cuenta',
                                                            align:'left'
														},
                                                        {
															header:'Nombre Cuenta',
															width:300,
															sortable:true,
															dataIndex:'tituloCta',
                                                            align:'left'
														},
                                                        {
															header:'Porcentaje',
															width:80,
															sortable:true,
															dataIndex:'porcentaje',
                                                            align:'left'
														},
                                                        {
															header:'Afectaci&oacute;n',
															width:80,
															sortable:true,
															dataIndex:'tipoAfectacion',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            				 {
                                                                             	if(val=='')
                                                                                {
                                                                                	return ''
                                                                                }
                                                                                else
                                                                                {
                                                                                	if(val==1)
                                                                                    	return 'Debe'
                                                                                    else    
                                                                                		return 'Haber'
                                                                                }
                                                                             }
														}
													]
												);
	var tblGridCtas=  new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCuentas',
                                                            title:'Cuentas asociadas',
                                                            store:dsRegContab,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            height:260,
                                                            width:600,
                                                            tbar:[
                                                                    {
                                                                    text:'Agregar cuenta',
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    handler:function()
                                                                        {
                                                                            agregarCuenta(idInventario,codigo);
                                                                        }
                                                                    },
                                                                    {
                                                                        text:'Remover cuenta',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                removerCuenta();
                                                                            }
                                                                    }
                                                                 ]
                                                        }
                                                    );
		
    dsRegContab.load()  ;
    return tblGridCtas;     
}


function agregarCuenta(idInventario,codigo)
{
	var arrAf=[['1','Debe'],['2','Haber']];
    var cmbCuentas=crearComboExt('cmbCuentas',[],100,10,400);
    var cmbAfectacion=crearComboExt('cmbAfectacion',arrAf,100,70);
    var form3 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario3',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:52,
                                                            y:15,
                                                            html:'<b>Cuenta:</b>'
                                                        },
                                                        cmbCuentas,
                                                        {
                                                            xtype:'label',
                                                            x:30,
                                                            y:45,
                                                            html:'<b>Porcentaje:</b>'
                                                        },
                                                        {
                                                            xtype:'numberfield',
                                                            x:100,
                                                            y:40,
                                                            width:80,
                                                            height:23,
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'porcentajeC'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:30,
                                                            y:75,
                                                            width:500,
                                                            height:23,
                                                            html:'<b>Afectaci&oacute;n:</b>'
                                                        },
                                                        cmbAfectacion
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Agregar cuenta',
										width: 550,
										height:200,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form3,
                                        id:'ventana3',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
                                                                                       var cuenta=Ext.getCmp('cmbCuentas').getValue();
                                                                                       if(cuenta=='')
                                                                                       {
                                                                                       		msgBox('Debe seleccionar una cuenta');
                                                                                            return;
                                                                                       }
                                                                                       
                                                                                       var porcentaje=Ext.getCmp('porcentajeC').getValue();
                                                                                       if(porcentaje=='')
                                                                                       {
                                                                                       		msgBox('Debe indicar el porcentaje');
                                                                                            return;
                                                                                       }
                                                                                       
                                                                                       var afectacion=Ext.getCmp('cmbAfectacion').getValue();
                                                                                       if(afectacion=='')
                                                                                       {
                                                                                       		msgBox('Debe indicar la afectaci&oacute;n');
                                                                                            return;
                                                                                       }
                                                                                       
                                                                                       function funcAjax()
                                                                                       {
                                                                                            var resp=peticion_http.responseText.split('|');
                                                                                            if(resp[0]==1)
                                                                                            {
                                                                                                 var almacen=Ext.getCmp('gridCuentas').getStore();
                                                                                                 almacen.reload(); 
                                                                                                 ventana.close();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                            }
                                                                                       }
                                                                                       obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=60&cuenta='+cuenta+'&porcentaje='+porcentaje+'&idInventario='+idInventario+'&afectacion='+afectacion+'&codigo='+codigo,true);
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        llenarCombo(ventana,idInventario,codigo)
        //ventana.show();
}

function llenarCombo(ventana,idInventario,codigo)
{
   function funcAjax()
   {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
             var arreglo=eval(resp[1]);
             var combo=Ext.getCmp('cmbCuentas');
             combo.getStore().loadData(arreglo);
             ventana.show();
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
   }
   obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=61&idInventario='+idInventario+'&codigo='+codigo,true);
}

function removerCuenta()
{
	var filas=Ext.getCmp('gridCuentas').getSelectionModel().getSelections();
	var tamano=filas.length;
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    var cadena='';
    var x;
    for(x=0;x<tamano;x++)
    {
    	var id=filas[x].get('idInventarioVSCuentas');
        if(cadena=='')
        	cadena=id;
        else
        	cadena+=','+id;    
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
             Ext.getCmp('gridCuentas').getStore().reload();
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=62&cadena='+cadena,true);
}


