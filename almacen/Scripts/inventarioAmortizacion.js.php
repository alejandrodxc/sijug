<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridFoliado();
}

function crearGridFoliado()
{
    var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'ubicacion'},
                                                                  {name: 'idInventario'},
                                                                  {name: 'codigo'},
                                                                  {name: 'noFactura'},
                                                                  {name: 'valor'},
                                                                  {name: 'valorDepre'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=64;
                                    }
                        );
   
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table>'
                                                                             +'<tr>'
                                                                                +'<td width="600" height="10"></td>'
                                                                             +'</tr>'
                                                                            // +'<tr>'
//                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>No. Factura:</b></span></td>'
//                                                                             +'</tr>'
//                                                                             +'<tr>'
//                                                                                +'<td width:"600" align="left"><span class="copyrigthSinPadding"><b>{noFactura}</b></span><br /><br /></td>'
//                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Ubicaci&oacute;n:</b><br /></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{ubicacion}</span></td>'
                                                                             +'</tr>'
                                                                         +'</table>'
                                                                        
                                                					  )
                                            });  
                                            
   var tamPagina=100;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegiNoFol,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                     
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														expander,
                                                        {
															header:'Nombre Producto',
															width:250,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'C&oacute;digo',
															width:100,
															sortable:true,
															dataIndex:'codigo',
                                                            align:'center'
														},
                                                        {
															header:'Fecha Recepci&oacute;n',
															width:100,
															sortable:true,
															dataIndex:'fechaRecepcion',
                                                            align:'center'
														},
                                                        {
															header:'No.Factura',
															width:100,
															sortable:true,
															dataIndex:'noFactura',
                                                            align:'right'
														},
                                                        {
															header:'Costo',
															width:70,
															sortable:true,
															dataIndex:'valor',
                                                            align:'right'
														},
                                                        {
															header:'Costo Depreciado',
															width:110,
															sortable:true,
															dataIndex:'valorDepre',
                                                            align:'right'
														}
                                                      //  ,
//                                                        {
//															header:'',
//															width:100,
//															sortable:true,
//                                                            align:'center',
//                                                            renderer:function(val,meta,registro)
//                                                            		{
//	                                                                    	return '<a href="javascript:verDetalle('+registro.get('idTablaFormulario')+','+registro.get('idFormulario')+','+registro.get('idDetallePedido')+')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver detalles de producto" title="Ver detalles de producto" /></a>'
//                                                                            +'&nbsp;&nbsp;<a href="javascript:mostrarVentanaInv('+registro.get('idInventario')+',\''+bE(registro.get('nombreProducto'))+'\',\''+bE(registro.get('codigo'))+'\')"><img height="13" width="13" src="../images/code.png" alt="Asociar elementos" title="Asociar elementos" /></a>';
//                                                                            //&nbsp;&nbsp;<a href="javascript:Cancelar('+registro.get('idPedido')+','+registro.get('folio')+')"><img height="13" width="13" src="../images/cancel_round.png" alt="Cancelar" title="Cancelar" /></a>';
//                                                                    }
//														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridNoFolio',
                                                            store:dsRegiNoFol,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'aInventario',
                                                            height:750,
                                                            width:800,
                                                            bbar: paginador,
                                                            plugins: [filters,expander]
                                                        }
                                                    );
		
    //dsRegiNoFol.load()  ;
    dsRegiNoFol.load({params:{start:0,limit:tamPagina,funcion:64}})  ;     
}
