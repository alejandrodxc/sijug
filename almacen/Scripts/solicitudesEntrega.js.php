<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
var arrSituacionesPedido=[['0','En espera de atenci&oacute;n'],['1','Parcialmente entregado'],['2','Entregado/Atendido'],['3','Entregada cancelada'],['4','Entregada pendiente']];
var tamPagina=100;
Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
	var gridDetallesProducto=crearGridDetallesProducto();
    
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos,
                                                        gridDetallesProducto
                                                        
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idSolicitudEntrega'},
		                                                {name: 'unidad'},
                                                        {name: 'fechaSolicitud', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'fechaLimitePrefenteEntrega',  type:'date', dateFormat:'Y-m-d'},
                                                        {name :'Nombre'},
                                                        {name: 'situacion'},
                                                        {name: 'codigoDepto'},
                                                        {name: 'subUnidadSolicitante'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'unidad'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'string', dataIndex: 'codigoDepto'},
                                                                        {type: 'date', dataIndex: 'fechaSolicitud'},
                                                                        {type: 'numeric', dataIndex:'idSolicitudEntrega'},
                                                                        {type: 'date', dataIndex: 'fechaLimitePrefenteEntrega'}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaSolicitud', direction: 'ASC'},
                                                            groupField: 'fechaSolicitud',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='89';
                                        proxy.baseParams.situacion=0;
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                        var gridDetallesProductos=gEx('gridDetallesProductos');
                                        if(gridDetallesProductos!=undefined)
	                                        gridDetallesProductos.getStore().removeAll();
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                         {
															header:'Folio solicitud',
															width:100,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'idSolicitudEntrega',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Fecha solicitud',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaSolicitud',
                                                            css:'text-align:right !important;',
                                                            hideable:true,
                                                             renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
														},
													 	
                                                        {
															header:'Fecha m&aacute;x. solicitada <br />de entrega',
															width:120,
                                                            align :'center',
															sortable:true,
                                                            hidden:true,
                                                            dataIndex:'fechaLimitePrefenteEntrega',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Situaci&oacute;n',
															width:150,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'situacion',
                                                            css:'text-align:left !important;',
                                                            hideable:true,
                                                             renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrSituacionesPedido,val);
                                                                    }
														},
                                                        {
															header:'',
															width:50,
															sortable:true,
															dataIndex:'codigoDepto',
                                                            hideable:true
														},
                                                        {
															header:'Unidad solicitante',
															width:350,
															sortable:true,
															dataIndex:'unidad',
                                                            hideable:true
														},
                                                        {
															header:'SubUnidad solicitante',
															width:350,
															sortable:true,
															dataIndex:'subUnidadSolicitante',
                                                            hideable:true
														},
														{
															header:'Responsable solicitud',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'Nombre',
                                                            hideable:true
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Solicitudes de entrega</b></span>',
                                                            columnLines:true,
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agendar entrega',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la solicitud cuya entrega desea agendar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaAgendarSolicitud(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Rechazar solicitud',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la solicitud que desea rechazar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaRechazarSolicitud(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar entrega',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la solicitud que desea registrar como entregada');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaRegistrarEntrega(fila);
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    chkRow.on('rowselect',function(sm,nFila,registro)
    						{
                            	var gridDetallesProductos=gEx('gridDetallesProductos');
                                gridDetallesProductos.getStore().load({url:'../paginasFunciones/funcionesAlmacen.php',params:{funcion:134,idSolicitudEntrega:registro.get('idSolicitudEntrega')}});
                            }
    		)							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:89,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}

function mostrarVentanaRechazarSolicitud(fila)
{	

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Motivo del rechazo: <font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Rechazar solicitud',
										width: 420,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                                														
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=102&idSolicitud='+fila.get('idSolicitudEntrega')+'&situacion=3&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             }
                                                                    	
                                                                       	}
                                                                        msgConfirm('Est&aacute; seguro de querer rechazar la solicitud de entrega seleccionada?',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	

}

function mostrarVentanaAgendarSolicitud(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha solicitada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:10,
                                                            xtype:'label',
                                                            html:fila.get('fechaLimitePrefenteEntrega').format('d/m/Y')
                                                        },
                                            			{
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha programada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            minValue:'<?php echo date("Y-m-d") ?>'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agendar entrega',
										width: 420,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de entrega del producto',resp1);
                                                                        }
                                                                    	
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();    
                                                                                gEx('gridDetallesProductos').getStore().removeAll();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=103&fechaEntrega='+dteFechaEntrega.getValue().format('Y-m-d')+'&idSolicitud='+fila.get('idSolicitudEntrega'),true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDetallesProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDetalleEntrega'},
		                                                {name: 'idProducto'},
                                                        {name: 'nombreProducto'},
                                                        {name :'cantidad'},
                                                        {name :'existenciaAlmacen'},
                                                        {name: 'comentarios'},
                                                        {name: 'situacion'},
                                                        {name: 'bloqueado'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                         {
															header:'Cve. Producto',
															width:100,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'idProducto',
                                                            hideable:true
														},
                                                        {
															header:'Producto',
															width:260,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'nombreProducto',
                                                            hideable:true
														},
													 	
                                                        {
															header:'Cantidad solicitada',
															width:120,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Existencia en almac&eacute;n',
															width:140,
															sortable:true,
                                                            align :'right',
															dataIndex:'existenciaAlmacen',
                                                            hideable:true
														},
                                                         {
															header:'Situaci&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'situacion',
                                                             align :'left',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	var imagen='s.gif';
                                                                        switch(val)
                                                                        {
                                                                        	case '2':
                                                                            	imagen='icon_big_tick.gif';
                                                                            break;
                                                                            case '3':
                                                                            	imagen='cross.png';
                                                                            break;
                                                                            case '4':
                                                                            	imagen='tag_blue_edit.png';
                                                                            break;
                                                                        }
                                                                    	return '<img width="13" height="13"  src="../images/'+imagen+'"> '+formatearValorRenderer(arrSituacionesPedido,val);
                                                                    }
														},
														{
															header:'Comentarios',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'cmentarios',
                                                            hideable:true
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallesProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            height:250,
                                                            collapsible:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Detalle de entrega</b></span>',
                                                            columnLines:true,
                                                            region: 'south',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	
	return tblGrid;       	
}

function mostrarVentanaRegistrarEntrega(fila)
{
	var gridProducto=crearGridProductoEntrega();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span style="color:#000"><b>Folio solicitud:</b></span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('idSolicitudEntrega')+'</b></span>'
                                                        },
                                                        {
                                                        	x:400,
                                                            y:10,
                                                            html:'<span style="color:#000"><b>Fecha solicitud:</b></span>'
                                                        },
                                                        {
                                                        	x:510,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('fechaSolicitud').format("d/m/Y")+'</b></span>'
                                                        },

                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'<span style="color:#000"><b>Unidad Solicitante:</b></span>'
                                                        },
                                                         {
                                                        	x:120,
                                                            y:40,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('unidad')+'</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'<span style="color:#000"><b>Responsable solicitud:</b></span>'
                                                        },
                                                         {
                                                        	x:140,
                                                            y:70,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('Nombre')+'</b></span>'
                                                        },
                                                        gridProducto


													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar entrega de producto',
										width: 700,
                                        id:'vEntregaProducto',
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filaReg;
                                                                        var x;
                                                                        var cadEntrega='';
                                                                        var obj='';
                                                                        var agendarEntrega=false;
                                                                        for(x=0;x<gridProducto.getStore().getCount();x++)
                                                                        {
                                                                        	filaReg=gridProducto.getStore().getAt(x);
                                                                            if(filaReg.get('situacion')=='0')
                                                                            {
                                                                            	msgBox('Para registrar la entrega de una solicitud, no deben existir productos marcados como <b>En espera de atenci&oacute;n</b>')
                                                                            	return;
                                                                            }
                                                                            if(filaReg.get('situacion')=='4')
                                                                            {
                                                                            	agendarEntrega=true;
                                                                            }
                                                                            if(filaReg.get('bloqueado')=='0')
                                                                            {
                                                                            	obj='{"idDetalleEntrega":"'+filaReg.get('idDetalleEntrega')+'","situacion":"'+filaReg.get('situacion')+'"}';
                                                                            	if(cadEntrega=='')
                                                                                	cadEntrega=obj;
                                                                                else
                                                                                	cadEntrega+=','+obj;
                                                                            }
                                                                            
                                                                        }
                                                                        var fechaAgenda='';
                                                                        
                                                                        var cadObj='{"fechaAgenda":"@fechaEntrega","idSolicitudEntrega":"'+fila.get('idSolicitudEntrega')+'","arrDetalle":['+cadEntrega+']}';
                                                                        function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	if(!agendarEntrega)
                                                                                {
                                                                                	cadObj=cadObj.replace('@fechaEntrega','');
                                                                                	registrarEntrega(cadObj);
                                                                                }
                                                                                else
                                                                                {
                                                                                	mostrarVentanaSelfechaEntrega(cadObj);
                                                                                }
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la entrega de la solicitud seleccionada?',resp);
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridProductoEntrega()
{	
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDetalleEntrega'},
		                                                {name: 'idProducto'},
                                                        {name: 'nombreProducto'},
                                                        {name :'cantidad'},
                                                        {name :'existenciaAlmacen'},
                                                        {name: 'comentarios'},
                                                        {name: 'situacion'},
                                                        {name: 'bloqueado'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:false});              
	
    var gridDetallesProductos=gEx('gridDetallesProductos');
    var arrFilas=new Array();
    var x;
    for(x=0;x<gridDetallesProductos.getStore().getCount();x++)
    {
    	filaCopia=gridDetallesProductos.getStore().getAt(x).copy();
        if(filaCopia.get('situacion')=='4')
        	filaCopia.set('bloqueado',0);
    	arrFilas.push(filaCopia);
    	
    }
	dsTablaRegistros2.add(arrFilas); 
    
	
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                         {
															header:'Cve. Producto',
															width:95,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'idProducto',
                                                            hideable:true
														},
                                                        {
															header:'Producto',
															width:230,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'nombreProducto',
                                                            hideable:true
														},
													 	
                                                        {
															header:'Cantidad sol.',
															width:100,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Existencia en alm.',
															width:125,
															sortable:true,
                                                            align :'right',
															dataIndex:'existenciaAlmacen',
                                                            hideable:true
														},
                                                         {
															header:'Situaci&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'situacion',
                                                             align :'left',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	var imagen='s.gif';
                                                                        switch(val)
                                                                        {
                                                                        	case '2':
                                                                            	imagen='icon_big_tick.gif';
                                                                            break;
                                                                            case '3':
                                                                            	imagen='cross.png';
                                                                            break;
                                                                            case '4':
                                                                            	imagen='tag_blue_edit.png';
                                                                            break;
                                                                        }
                                                                    	return '<img width="13" height="13" src="../images/'+imagen+'"> '+formatearValorRenderer(arrSituacionesPedido,val);
                                                                    }
														},
														{
															header:'Comentarios',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'cmentarios',
                                                            hideable:true,
                                                            editor:	{
                                                            			xtype:'textfield'
                                                                        
                                                            		}
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridDetallesProductosEntrega',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            height:250,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Detalle de entrega</b></span>',
                                                            columnLines:true,
                                                           	x:5,
                                                            y:100,
                                                            height:300,
                                                            with:700,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            clicksToEdit:1,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnEntregado',
                                                                            text:'Marcar como entregado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var x;
                                                                                        var arrSel=tblGrid.getSelectionModel().getSelections();
                                                                                        if(arrSel.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar los productos que desea marcar como entregados');
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                        
                                                                                        var filasOk=true;
                                                                                        for(x=0;x<arrSel.length;x++)
                                                                                        {
                                                                                        	//if(parseFloat(arrSel[x].get('cantidad'))<(parseFloat(arrSel[x].get('existenciaAlmacen'))))
                                                                                        		arrSel[x].set('situacion','2');
                                                                                            //else
                                                                                            	//filasOk=false;
                                                                                        }
                                                                                        if(!filasOk)
                                                                                        {
                                                                                        	msgBox('Algunos productos no pudieron marcarse como entregados debido a que la cantidad solicitada es mayor que la existente');
                                                                                            return;
                                                                                        }
                                                                                        else
                                                                                        	tblGrid.getSelectionModel().deselectRange(0,tblGrid.getStore().getCount()-1);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnCancelado',
                                                                            text:'Marcar como entrega cancelada',
                                                                            handler:function()
                                                                            		{
                                                                                    	var x;
                                                                                        var arrSel=tblGrid.getSelectionModel().getSelections();
                                                                                        if(arrSel.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar los productos que desea marcar como cancelados');
                                                                                            return;
                                                                                        }
                                                                                        for(x=0;x<arrSel.length;x++)
                                                                                        {
                                                                                        	arrSel[x].set('situacion','3');
                                                                                        }
                                                                                        tblGrid.getSelectionModel().deselectRange(0,tblGrid.getStore().getCount()-1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/tag_blue_edit.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnPendiente',
                                                                            text:'Marcar como entrega pendiente',
                                                                            handler:function()
                                                                            		{
                                                                                    	var x;
                                                                                        var arrSel=tblGrid.getSelectionModel().getSelections();
                                                                                        if(arrSel.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar los productos que desea marcar como pendientes');
                                                                                            return;
                                                                                        }
                                                                                        for(x=0;x<arrSel.length;x++)
                                                                                        {
                                                                                        	arrSel[x].set('situacion','4');
                                                                                        }
                                                                                        tblGrid.getSelectionModel().deselectRange(0,tblGrid.getStore().getCount()-1);
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            loadMask:true,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	chkRow.on('rowselect',function()
    						{
								var x;
                                var arrSel=tblGrid.getSelectionModel().getSelections();
                                for(x=0;x<arrSel.length;x++)
                                {
                                	if((arrSel[x].get('bloqueado')=='1')&&(arrSel[x].get('situacion')!='4'))
                                    {
                                    	gEx('btnEntregado').disable();
                                        gEx('btnCancelado').disable();
                                        gEx('btnPendiente').disable();
                                    	return;
                                    }
                                }
                                gEx('btnEntregado').enable();
                                gEx('btnCancelado').enable();
                                gEx('btnPendiente').enable();
                            }
    		);
	return tblGrid;       		
}

function registrarEntrega(cadObj)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('gridProductos').getStore().reload();
         	gEx('gridDetallesProductos').getStore().removeAll();
            gEx('vEntregaProducto').close();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=135&cadObj='+cadObj,true);

}

function mostrarVentanaSelfechaEntrega(cadObj)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span style="color:#000">Se ha detectado que al menos un producto ha sido maracado como <b>Entrega pendiente</b>, <br>por lo cual es necesario indicar la fecha estimada de atenci&oacute;n a dichos productos</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:60,
                                                            html:'Fecha estimada de atenci&oacute;n:'
                                                        },
                                                        {
                                                        	x:170,
                                                            y:55,
                                                            xtype:'datefield',
                                                            id:'dtrFechaAtencion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Asignar fecha de entrega de productos pendientes',
										width: 500,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dtrFechaAtencion=gEx('dtrFechaAtencion');
																		if(dtrFechaAtencion.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar la fecha estimada de atenci&oacute;n de los productos marcados como pendientes');
                                                                        	return;
                                                                        }
                                                                        var fecha=dtrFechaAtencion.getValue().format("Y-m-d");
                                                                        cadObj=cadObj.replace('@fechaEntrega',fecha);
                                                                        registrarEntrega(cadObj);
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}
	