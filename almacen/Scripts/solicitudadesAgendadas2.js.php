<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();

	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos
                                                        
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var tamPagina=100;
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idSolicitudEntrega'},
		                                                {name: 'unidad'},
		                                                {name:'tituloPrograma'},
		                                                {name:'ruta'},
                                                        {name:'cvePrograma'},
                                                        {name: 'nombreProducto'},
                                                        {name: 'cantidad', type:'int'},
                                                        {name: 'fechaSolicitud', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'fechaLimitePrefenteEntrega',  type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'comentarioSolicitud'},
                                                        {name: 'fechaAgendaEntrega', type:'date', dateFormat:'Y-m-d'},
                                                        {name :'Nombre'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'unidad'},
                                                                        {type: 'string', dataIndex: 'nombreProducto'},
                                                                        {type: 'string', dataIndex: 'tituloPrograma'},
                                                                        {type: 'string', dataIndex: 'cvePrograma'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'date', dataIndex: 'fechaSolicitud'},
                                                                        {type: 'date', dataIndex: 'fechaLimitePrefenteEntrega'}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaSolicitud', direction: 'ASC'},
                                                            groupField: 'fechaSolicitud',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='104';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Fecha solicitud',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaSolicitud',
                                                            css:'text-align:right !important;',
                                                            hideable:true,
                                                             renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
														},
													 	{
															header:'Departamento',
															width:350,
															sortable:true,
															dataIndex:'unidad',
                                                            hideable:true
														},
														{
															header:'Programa',
															width:130,
															sortable:true,
															dataIndex:'ruta',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'',
															width:50,
															sortable:true,
															dataIndex:'cvePrograma',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
                                                        {
															header:'Cantidad',
															width:110,
															sortable:true,
															dataIndex:'cantidad',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Fecha solicitada <br />de entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaLimitePrefenteEntrega',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'Fecha programada <br />de entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaAgendaEntrega',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return '<font color="green">'+val.format('d/m/Y')+'</font>';
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
														{
															header:'Comentarios',
															width:300,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'comentarioSolicitud',
                                                            hideable:true
														},
                                                        {
															header:'Responsable solicitud',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'Nombre',
                                                            hideable:true
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Solicitudes con agenda de entrega</b></span>',
                                                            columnLines:true,
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar entrega del producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuya entrega desea registrar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaRegistrarEntrega(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Cancelar entrega',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la entrega que desea cancelar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaCancelarEntrega(fila);
                                                                                    }
                                                                            
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	icon:'../images/arrow_refresh.PNG',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Reagendar entrega',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la entrega que desea reagendar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaReAgendarSolicitud(fila);
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:104,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}

function mostrarVentanaCancelarEntrega(fila)
{	

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Motivo de la operaci&oacute;n: <font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar entrega',
										width: 420,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo por el cual desea cancelar la entrega',resp2);
                                                                        }
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().reload();    
                                														
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=102&idSolicitud='+fila.get('idSolicitudEntrega')+'&situacion=5&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             }
                                                                    	
                                                                       	}
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar la entrega seleccionada?',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	

}

var campoBusqueda;

function mostrarVentanaRegistrarEntrega(fila)
{
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha de entrega: '
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            value:fila.get('fechaAgendaEntrega').format('d/m/Y'),
                                                            minValue:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Producto recibido por: '
                                                        },
                                                       cmbComboBusqueda,
                                                        
														{
                                                        	x:10,
                                                            y:100,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:120,
                                                        	xtype:'textarea',
                                                            width:420,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar entrega de producto',
										width: 560,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de entrega del producto',resp1);
                                                                        }
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                        
                                                                        if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbComboBusqueda.focus();
                                                                            }
                                                                            msgBox('Debe indicar la persona responsable que recibi&oacute; la entrega del producto');
                                                                            return;
                                                                        }
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=106&responsable='+cmbComboBusqueda.getValue()+'&fechaEntrega='+dteFechaEntrega.getValue().format('Y-m-d')+'&idSolicitud='+fila.get('idSolicitudEntrega')+'&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaReAgendarSolicitud(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha programada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:10,
                                                            xtype:'label',
                                                            html:fila.get('fechaAgendaEntrega').format('d/m/Y')
                                                        },
                                            			{
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha de reprogramaci&oacute;n: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            value:fila.get('fechaAgendaEntrega').format('d/m/Y'),
                                                            minValue:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        
														{
                                                        	x:10,
                                                            y:70,
                                                            html:'Motivo de reprogramaci&oacute;n: <span class="letraRoja">*</span> '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:100,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agendar entrega',
										width: 420,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha a la cual ser&aacute; reagendada la entrega del producto',resp1);
                                                                        }
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del cambio de fecha',resp2);
                                                                        }
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=105&fechaEntrega='+dteFechaEntrega.getValue().format('Y-m-d')+'&idSolicitud='+fila.get('idSolicitudEntrega')+'&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesPlaneacion.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'departamento'}
											]
										);
	var parametros=	{
						funcion:'19',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;
        dSet.baseParams.ambitoAsignacion=0
	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br><span class="letraRojaSubrayada8">Departamento:</span> {departamento}',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:135,
                                                        y:65
													}
												 );
	
   	return comboNombre;
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}