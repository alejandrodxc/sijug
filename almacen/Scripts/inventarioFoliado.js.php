<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$conDeptos="SELECT codigoUnidad,CONCAT('[',codigoDepto,']',' ',unidad) as unidad FROM 817_organigrama WHERE codigoInstitucion='".$_SESSION["codigoInstitucion"]."' AND institucion<>1";
	$arregloDeptos=$con->obtenerFilasArreglo($conDeptos);
	$consulta="SELECT codigoControl,nombreArea FROM 9309_ubicacionesFisicas ORDER BY  nombreArea";
	$arrUbicaciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__406_tablaDinamica,descripcion FROM  _406_tablaDinamica ORDER BY descripcion";
	$arrMarcas=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);	
?>	
var arrSiNo=<?php echo $arrSiNo?>;
var arrMarcas=<?php echo $arrMarcas?>;
var arrConciliado=[['0','No'],['1','Sí']];
var arrTipoValor=[['1','Plan de inversi\xF3n'],['2','Por proyecto'],['4','No especificado']];
var arrSituacion=[['0','Baja'],['1','Activo']];

Ext.onReady(inicializar);
var arrUbicaciones=<?php echo $arrUbicaciones?>;
function inicializar()
{
	var gridInventarios=crearGridFoliado();
    var gridHistorialProducto=crearGridHistorial();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            border:false,
                                            frame:false,
                                            tbar:[{
                                                                        	xtype:'label',
                                                                        	html:'<span style="font-size:13px; font-weight:bold; color:#900;">Inventario General</span>'
                                                                        }],
                                            items:	[
                                            			gridInventarios,
                                                        gridHistorialProducto
                                                        
                                            		]
                                        }
                                     ]
						}
                    ) 
    
}

function crearGridFoliado()
{
    var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idInventario'},
                                                                  {name: 'noInv', type:'int'},
                                                                  {name: 'claveCABMS'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'noFactura'},
                                                                  {name: 'precioCompra'},
                                                                  {name: 'txtRazonSocial2'},
                                                                  {name: 'idProveedor'},
                                                                  {name: 'fechaCompra'},
                                                                  {name: 'anioInversion'},
                                                                  {name: 'unidad'},
                                                                  {name: 'codigoU'},
                                                                  {name: 'idResponsable'},
                                                                  {name: 'Nombre'},
                                                                  {name: 'idDetallePedido'},
                                                                  {name: 'idFormulario'},
                                                                  {name: 'idTablaDinamica'},
                                                                  {name: 'idAlmacen'},
                                                                  {name: 'cuenta'},
                                                                  {name: 'subcuenta'},
                                                                  {name: 'marca'},
                                                                  {name: 'modelo'},
                                                                  {name: 'numSerie'},
                                                                  {name: 'centroCosto'},
                                                                  {name: 'idPedido'},
                                                                  {name: 'conciliado'},
                                                                  {name: 'tipoValor'},
                                                                  {name: 'idProducto'},
                                                                  {name: 'codigoCentroCosto'},
                                                                  {name: 'valorActualBien'},
                                                                  {name: 'situacion'},
                                                                  {name: 'idFormularioBaja'},
                                                                  {name: 'idRegistroBaja'}
                                                                  
                                                                  
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
                                                    
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 50,
                                                      store: dsRegiNoFol,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )                                                     
                                                    
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=63;
                                       
                                    }
                        );
   
                                           
   var tamPagina=50;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegiNoFol,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                     
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'claveCABMS' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'noFactura' 
                                                                      },
                                                                     
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'txtRazonSocial2' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'noInv' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'anioInversion' 
                                                                      },
                                                                     {
                                                                          type:'int',
                                                                          dataIndex:'cuenta' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'subcuenta' 
                                                                      },
                                                                      /*{
                                                                          type:'string',
                                                                          dataIndex:'marca' 
                                                                      },
                                                                      */
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'modelo' 
                                                                      },
                                                                      
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'numSerie' 
                                                                      },                                                                     
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'idPedido' 
                                                                      },                                                                     
                                                                      {
                                                                          type:'list',
                                                                          dataIndex:'conciliado' ,
                                                                          options:arrConciliado,
                                                                          phpMode:true
                                                                      },                                                                     
                                                                      {
                                                                          type:'list',
                                                                          dataIndex:'tipoValor' ,
                                                                          options:arrTipoValor,
                                                                          phpMode:true
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true}); 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'No.Inventario',
															width:100,
															sortable:true,
															dataIndex:'noInv',
                                                            renderer:function(val)
                                                            		{
                                                                    	return str_pad(val,6,'0','STR_PAD_LEFT');
                                                                    },
                                                            align:'left'
														},
                                                        {
															header:'CABMS',
															width:80,
															sortable:true,
															dataIndex:'claveCABMS',
                                                            align:'left'
														},
                                                        {
															header:'Cuenta',
															width:80,
															sortable:true,
															dataIndex:'cuenta',
                                                            align:'left'
														},
                                                        {
															header:'Subcuenta',
															width:80,
															sortable:true,
															dataIndex:'subcuenta',
                                                            align:'left'
														},
                                                        {
															header:'Nombre Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Precio de compra',
															width:130,
															sortable:true,
															dataIndex:'precioCompra',
                                                            align:'right',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Valor bien',
															width:130,
															sortable:true,
															dataIndex:'valorActualBien',
                                                            align:'right',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Fecha Adquisici&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'fechaCompra',
                                                            align:'right'
														},
                                                        {
															header:'A&ntilde;o de Inversi&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'anioInversion',
                                                            align:'right'
														},
                                                        {
															header:'Marca',
															width:130,
															sortable:true,
															dataIndex:'marca',
                                                            align:'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrMarcas,val);
                                                                        if(pos==-1)
                                                                        	return val;
                                                                         return arrMarcas[pos][1];
                                                                    }
														},
                                                        {
															header:'Modelo',
															width:130,
															sortable:true,
															dataIndex:'modelo',
                                                            align:'right'
														},
                                                        {
															header:'No. Serie',
															width:130,
															sortable:true,
															dataIndex:'numSerie',
                                                            align:'right'
														},
                                                        {
															header:'Factura',
															width:100,
															sortable:true,
															dataIndex:'noFactura',
                                                            align:'right'
														},
                                                         {
															header:'Pedido',
															width:100,
															sortable:true,
															dataIndex:'idPedido',
                                                            align:'right'
														},
                                                        {
															header:'Proveedor',
															width:200,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
																		
                                                                        	return ''+val+'';
                                                                    }
                                                            
														},
                                                        {
															header:'Centro de costo',
															width:200,
															sortable:true,
															dataIndex:'centroCosto',
                                                            align:'left'
														},
                                                         {
															header:'Conciliado',
															width:100,
															sortable:true,
															dataIndex:'conciliado',
                                                            align:'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrConciliado,val);
                                                                    }
														},
                                                         {
															header:'Valor por',
															width:100,
															sortable:true,
															dataIndex:'tipoValor',
                                                            align:'left',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoValor,val);
                                                                    }
														},
                                                        {
															header:'Situaci&oacute;n',
															width:100,
															sortable:true,
															dataIndex:'situacion',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(val=='0')
                                                                        {
                                                                        	if((registro.get('idFormularioBaja')!='')&&(registro.get('idRegistroBaja')!=''))
                                                                            {
                                                                            	comp='<a href="javascript:verInformacionBaja(\''+bE(registro.get('idFormularioBaja'))+'\',\''+bE(registro.get('idRegistroBaja'))+'\')">&nbsp;<img width="13" height="13" src="../images/magnifier.png" title="Ver información de baja" alt="Ver información de baja"></a>';
                                                                            }
                                                                        }
                                                                    	return formatearValorRenderer(arrSituacion,val)+comp;
                                                                    }
														}
                                                        
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridNoFolio',
                                                            store:dsRegiNoFol,
                                                            frame:false,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            region:'center',
                                                            loadMask:true,
                                                            border:false,
                                                            columnLines:true,
                                                            stripeRows :true,
                                                            bbar:[paginador],
                                                            plugins: [filters],
                                                            viewConfig: {
                                                                            getRowClass: function(record, rowIndex, rp, ds)
                                                                            {
                                                                            	if(record.get('situacion')=='0')
	                                                                            	return 'letraRojaFila';
                                                                            }
                                                                        },  
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Asignar nuevo responsable',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea asignarle un responsable');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaAsignacionResponsable(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/page_white_magnify.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:true,
                                                                            text:'Ver datos de compra del bien',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuyos datos de compra desea observar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaDatosCompra(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar nuevo producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaProducto();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar producto inventariado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuya informaci&oacute;n desea modificar');
                                                                                            return;
                                                                                        }
                                                                                    	mostrarVentanaProducto(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto inventariado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover');
                                                                                            return;
                                                                                        }
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGridP.getStore().remove(fila);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=146&idInventario='+fila.get('idInventario'),true);
                                                                                                
                                                                                             }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                        
                                                            		]
                                                        }
                                                    );
		
    dsRegiNoFol.load({params:{start:0 ,limit:50}})  ;
    tblGridP.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGridP.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridHistorial').getStore().load({params:{funcion:107,idInventario:registro.get('idInventario')}});
                                                }
    							)  
    return tblGridP;     
}

function crearGridHistorial()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idMovimiento'},
		                                                {name: 'fechaAsignacion', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'idResponsable'},
		                                                {name:'titularAsignacion'},
                                                        {name:'codigoUbicacion'},
		                                                {name:'ubicacion'},
                                                        {name:'comentarios'},
                                                        {name:'codigoUnidad'},
                                                        {name:'departamento'},
                                                        {name: 'idFormularioRef'},
                                                        {name: 'idRegistroRef'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
                                      

                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            
                                                            groupField: 'fechaAsignacion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=107;
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
													 	{
															header:'Fecha de asignaci&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'fechaAsignacion',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	if((val!='')&&(val!=null))
	                                                                                	return val.format('d/m/Y');
                                                                                	
                                                                                }
														},
														
                                                        {
															header:'Asignado a',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'titularAsignacion',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes asignados a '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(1,\''+bE(titulo)+'\',\''+bE(registro.get('idResponsable'))+'\')">'+val+'</a>';
                                                                    }
														},
                                                        {
															header:'Departamento / &aacute;rea',
															width:250,
                                                            align :'left',
															sortable:true,
															dataIndex:'departamento',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes asignados concentrados en el departamento/&aacute;rea '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(3,\''+bE(titulo)+'\',\''+bE(registro.get('codigoUnidad'))+'\')">'+val+'</a>';
                                                                    }
														},
														{
															header:'Ubicaci&oacute;n f&iacute;sica',
															width:250,
                                                            align :'left',
															sortable:true,
															dataIndex:'ubicacion',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes ubicados en '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(2,\''+bE(titulo)+'\',\''+bE(registro.get('codigoUbicacion'))+'\')">'+val+'</a>';
                                                                    }
														},
                                                        {
															header:'Comentario',
															width:350,
                                                            sortable:true,
                                                            dataIndex:'comentarios',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(registro.get('idFormularioRef')!='')
                                                                        {
	                                                                       	comp='<a href="javascript:verInformacionBaja(\''+bE(registro.get('idFormularioRef'))+'\',\''+bE(registro.get('idRegistroRef'))+'\')">&nbsp;<img width="13" height="13" src="../images/magnifier.png" title="Ver información" alt="Ver información"></a>';
                                                                        }
                                                                        return comp+' '+val;
                                                                    }
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridHistorial',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            region:'south',
                                                            height:250,
                                                            cm: cModelo,
                                                            collapsible:true,
                                                            columnLines:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  	
	return tblGrid;                                                
}

function mostrarVentanaAsignacionResponsable(fila)
{
	var cmbUbicacion=crearComboExt('cmbUbicacion',arrUbicaciones,135,95,350);
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha de asignaci&oacute;n: '
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaAsignacion',
                                                            value:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Asignar a: '
                                                        },
                                                       cmbComboBusqueda,
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Ubicaci&oacute;n f&iacute;sica:'
                                                        },
                                                        cmbUbicacion,
														{
                                                        	x:10,
                                                            y:140,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:165,
                                                        	xtype:'textarea',
                                                            width:460,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar entrega de producto',
										width: 560,
										height:360,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaAsignacion');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de asginaci&oacute;n del bien',resp1);
                                                                        }
                                                                    	
                                                                        
                                                                        if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbComboBusqueda.focus();
                                                                            }
                                                                            msgBox('Debe indicar la persona a la cual se le asigna el bien',resp3);
                                                                            return;
                                                                        }
                                                                
                                                                		if(cmbUbicacion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbUbicacion.focus();
                                                                            }
                                                                            msgBox('Debe indicar la ubicaci&oacute;n f&iacute;sica del bien',resp2);
                                                                            return;
                                                                        }
                                                                
                                                                		var obj='{"fechaAsignacion":"'+dteFechaEntrega.getValue().format('Y-m-d')+'","responsable":"'+cmbComboBusqueda.getValue()+'","ubicacion":"'+cmbUbicacion.getValue()+
                                                                        		'","comentarios":"'+cv(gEx('txtMotivo').getValue())+'","idInventario":"'+fila.get('idInventario')+'"}';
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridHistorial').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=111&cadObj='+obj,true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesPlaneacion.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'departamento'}
											]
										);
	var parametros=	{
						funcion:'19',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;
        dSet.baseParams.ambitoAsignacion=0
	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br><span class="letraRojaSubrayada8">Departamento:</span> {departamento}',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:135,
                                                        y:65
													}
												 );
	
   	return comboNombre;
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}

var accionVigente;
var parametroVigente;


function motrarVentanaBienesAsociados(accion,titulo,valor)
{
	accionVigente=accion;
	parametroVigente=bD(valor);

	var gridBienes=crearGridBienesAsociados();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridBienes		

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: bD(titulo),
										width: 850,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridBienesAsociados()
{
	var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idInventario'},
                                                                  {name: 'noInv', type:'int'},
                                                                  {name: 'claveCABMS'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'noFactura'},
                                                                  {name: 'precioCompra'},
                                                                  {name: 'txtRazonSocial2'},
                                                                  {name: 'idProveedor'},
                                                                  {name: 'fechaCompra'},
                                                                  {name: 'anioInversion'},
                                                                  {name: 'unidad'},
                                                                  {name: 'codigoU'},
                                                                  {name: 'idResponsable'},
                                                                  {name: 'Nombre'},
                                                                  {name: 'idDetallePedido'},
                                                                  {name: 'idFormulario'},
                                                                  {name: 'idTablaDinamica'},
                                                                  {name: 'idAlmacen'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
                                                    
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 50,
                                                      store: dsRegiNoFol,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )                                                     
                                                    
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=112;
                                        proxy.baseParams.accion=accionVigente;
                                        proxy.baseParams.valor=parametroVigente;
                                    }
                        );
   
                                           
   var tamPagina=50;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegiNoFol,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                     
  
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true}); 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'No.Inventario',
															width:100,
															sortable:true,
															dataIndex:'noInv',
                                                            align:'left',
                                                            renderer:function(val)
                                                            			{
                                                                        	return str_pad(val,6,'0','STR_PAD_LEFT');
                                                                        }
														},
                                                        {
															header:'CABMS',
															width:80,
															sortable:true,
															dataIndex:'claveCABMS',
                                                            align:'left'
														},
                                                        {
															header:'Nombre Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Factura',
															width:100,
															sortable:true,
															dataIndex:'noFactura',
                                                            align:'right'
														},
                                                        {
															header:'Costo de Inversi&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'precioCompra',
                                                            align:'right',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Proveedor',
															width:200,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
																		
                                                                        	return ''+val+'';
                                                                    }
                                                            
														},
                                                        {
															header:'Fecha Adquisici&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'fechaCompra',
                                                            align:'right'
														},
                                                        {
															header:'A&ntilde;o de Inversi&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'anioInversion',
                                                            align:'right'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridBienesAsociados',
                                                            store:dsRegiNoFol,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            x:10,
                                                            y:10,
                                                            width:800,
                                                            height:350,
                                                            loadMask:true,
                                                            border:true,
                                                            columnLines:true,
                                                            stripeRows :true,
                                                            bbar:[paginador],
                                                            tbar:[{
                                                                        	icon:'../images/page_white_magnify.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Ver datos de compra del bien',
                                                                            hidden:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuyos datos de compra desea observar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaDatosCompra(fila);
                                                                                    }
                                                                            
                                                                        }]
                                                        }
                                                    );
		
    dsRegiNoFol.load({params:{start:0 ,limit:50}})  ;
    return tblGridP;   
}

function mostrarVentanaDatosCompra(fila)
{
	msgBox('No existen datos de pedido asociados al bien');
}

function mostrarVentanaProducto(fila)
{
	var cmbMarca=crearComboExt('cmbMarca',arrMarcas,80,125,250);
    var cmbSiNo=crearComboExt('cmbSiNo',arrSiNo,80,335,130);
    cmbSiNo.setValue('1');
    var cmbTipoValor=crearComboExt('cmbTipoValor',arrTipoValor,300,335,150);
    cmbTipoValor.setValue('4');
    var oConf=	{
    					idCombo:'cmbCambs',
                        posX:80,
                        posY:5,
                        anchoCombo:250,
                        campoDesplegar:'txtClave',
                        campoID:'id__802_tablaDinamica',
                        campoHDestino:'hCambs',
                        funcionBusqueda:141,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:'<tpl for="."><div class="search-item"><b>{txtClave}</b> <br><span class="letraRojaSubrayada8">Descripci&oacute;n:</span> {txtDescripcion}</div></tpl>',
                        campos:	[
                                    {name:'id__802_tablaDinamica'},
                                    {name:'txtClave'},
                                    {name:'txtDescripcion'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	
                                    	gE('hCambs').value='-1';
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('hCambs').value=registro.get('txtClave');
                                    	
                                    }  
    				};
    var cmbCambs=crearComboExtAutocompletar(oConf);
    oConf=	{
    					idCombo:'cmbProducto',
                        posX:80,
                        posY:65,
                        anchoCombo:350,
                        campoDesplegar:'nombreProducto',
                        campoID:'idProducto',
                        campoHDestino:'hIdProducto',
                        funcionBusqueda:142,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:'<tpl for="."><div class="search-item">{nombreProducto}</div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'nombreProducto'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	
                                    	gE('hIdProducto').value='-1';
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('hIdProducto').value=registro.get('idProducto');
                                    	
                                    }  
    				};
    
    var cmbProducto=crearComboExtAutocompletar(oConf);
    oConf=	{
    					idCombo:'cmbProveedor',
                        posX:80,
                        posY:275,
                        anchoCombo:350,
                        campoDesplegar:'txtRazonSocial2',
                        campoID:'id__405_tablaDinamica',
                        campoHDestino:'hIdProveedor',
                        funcionBusqueda:143,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:'<tpl for="."><div class="search-item">{txtRazonSocial2} <br><span class="letraRojaSubrayada8">RFC:</span> {txtRFC}</div></tpl>',
                        campos:	[
                                    {name:'id__405_tablaDinamica'},
                                    {name:'txtRazonSocial2'},
                                    {name: 'txtRFC'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	
                                    	gE('hIdProveedor').value='-1';
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('hIdProveedor').value=registro.get('id__405_tablaDinamica');
                                    	
                                    }  
    				};
	var cmbProveedor=crearComboExtAutocompletar(oConf);      
    
    oConf=	{
    					idCombo:'cmbCentroCosto',
                        posX:110,
                        posY:305,
                        anchoCombo:320,
                        campoDesplegar:'unidad',
                        campoID:'codigoUnidad',
                        campoHDestino:'hCentroCosto',
                        funcionBusqueda:144,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:'<tpl for="."><div class="search-item">{unidad} </div></tpl>',
                        campos:	[
                                    {name:'codigoUnidad'},
                                    {name:'unidad'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	
                                    	gE('hCentroCosto').value='-1';
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('hCentroCosto').value=registro.get('codigoUnidad');
                                    	
                                    }  
    				};
	var cmbCentroCosto=crearComboExtAutocompletar(oConf);               
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'CAMBS:<font color="red">*</font>'
                                                        },
                                                        cmbCambs,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Cuenta:<font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:80,
                                                            y:35,
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'txtCuenta',
                                                            width:80
                                                        },
                                                        {
                                                        	x:180,
                                                            y:40,
                                                            html:'Subcuenta:<font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:260,
                                                            y:35,
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'txtSubCuenta',
                                                            width:80
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Producto:<font color="red">*</font>'
                                                        },cmbProducto,
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Fecha de adquisici&oacute;n:<font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:125,
                                                            y:95,
                                                            xtype:'datefield',
                                                            id:'dteFechaAdquisicion'
                                                            
                                                        },
                                                         {
                                                        	x:240,
                                                            y:100,
                                                            html:'Precio de compra:<font color="red">*</font>'
                                                        },
                                                         {
                                                        	x:340,
                                                            y:95,
                                                            width:80,
                                                            xtype:'numberfield',
                                                            id:'txtPrecioCompra',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Marca:<font color="red">*</font>'
                                                        },
                                                        cmbMarca,
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Modelo:'
                                                        },
                                                        {
                                                        	x:80,
                                                            y:155,
                                                            xtype:'textfield',
                                                            width:300,
                                                            id:'txtModelo'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            html:'No. serie:'
                                                        },
                                                         {
                                                        	x:80,
                                                            y:185,
                                                            xtype:'textfield',
                                                            width:300,
                                                            id:'txtNoSerie'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:220,
                                                            html:'Factura:<font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:80,
                                                            y:215,
                                                            width:80,
                                                            xtype:'numberfield',
                                                            id:'txtFactura',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:250,
                                                            html:'Pedido:<font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:80,
                                                            y:245,
                                                            width:80,
                                                            xtype:'numberfield',
                                                            id:'txtPedido',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:280,
                                                            html:'Proveedor:'
                                                        },
                                                        cmbProveedor,
                                                        {
                                                        	x:10,
                                                            y:310,
                                                            html:'Centro de costo:<font color="red">*</font>'
                                                        },
                                                        cmbCentroCosto,
                                                        {
                                                        	x:10,
                                                            y:340,
                                                            html:'Conciliado:<font color="red">*</font>'
                                                        },
                                                        cmbSiNo,
                                                        {
                                                        	x:220,
                                                            y:340,
                                                            html:'Valor por:<font color="red">*</font>'
                                                        },
                                                        cmbTipoValor

													]
										}
									);
	var lblTitulo='Agregar producto a inventario';
    if(fila==undefined)
    	lblTitulo='Modificar producto de inventario';
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 500,
                                        id:'vInventario',
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbCambs').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var idInventario="-1";
                                                                        if(fila!=null)
                                                                        	idInventario=fila.get('idInventario');
                                                                        var hCambs=gE('hCambs'); 
                                                                        if(hCambs.value=='-1')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbCambs.focus();
                                                                            }
                                                                        	msgBox('Debe indicar la clave CAMBS del producto',resp);
                                                                        	return;
                                                                        }    
                                                                        var txtCuenta=gEx('txtCuenta');
                                                                        if(txtCuenta.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtCuenta.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la cuenta del producto',resp1);
                                                                            return;
                                                                        }
                                                                        var txtSubCuenta=gEx('txtSubCuenta');
                                                                        if(txtSubCuenta.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtSubCuenta.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la subcuenta del producto',resp2);
                                                                            return;
                                                                        }
                                                                        var hIdProducto=gE('hIdProducto');
                                                                        if(hIdProducto.value==-1)
                                                                        {
                                                                            function resp3()
                                                                            {
                                                                            	cmbProducto.focus();
                                                                            }
                                                                            msgBox('Debe indicar el producto a inventariar',resp3);
                                                                            return;
                                                                        }
                                                                        var dteFechaAdquisicion=gEx('dteFechaAdquisicion');
                                                                        if(dteFechaAdquisicion.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteFechaAdquisicion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de adquisici&oacute;n del producto',resp4);
                                                                            return;
                                                                        }
                                                                        var txtPrecioCompra=gEx('txtPrecioCompra');
                                                                        if(txtPrecioCompra.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	txtPrecioCompra.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el precio de compra del producto',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbMarca.getValue()=='')
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	cmbMarca.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la marca del producto',resp6);
                                                                            return;
                                                                        }
                                                                        
                                                                        var txtFactura=gEx('txtFactura');
                                                                        if(txtFactura.getValue()=='')
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	txtFactura.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de factura del producto',resp7);
                                                                            return;
                                                                        }
                                                                        
                                                                         var txtPedido=gEx('txtPedido');
                                                                        if(txtPedido.getValue()=='')
                                                                        {
                                                                        	function resp8()
                                                                            {
                                                                            	txtPedido.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de pedido del producto',resp8);
                                                                            return;
                                                                        }
                                                                        var hCentroCosto=gE('hCentroCosto');
                                                                        if(hCentroCosto.value=='-1')
                                                                        {
                                                                        	function resp9()
                                                                            {
                                                                            	cmbCentroCosto.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el centro de costo del producto',resp9);
                                                                            return;
                                                                        }
                                                                        var idProveedor=0;
                                                                        if(gE('hIdProveedor').value!=-1)
                                                                        	idProveedor=gE('hIdProveedor').value;
                                                                        var cadObj='{"idInventario":"'+idInventario+'","cambs":"'+hCambs.value+'","cuenta":"'+txtCuenta.getValue()+'","subcuenta":"'+txtSubCuenta.getValue()+
                                                                        			'","idProducto":"'+hIdProducto.value+'","fechaAdquisicion":"'+dteFechaAdquisicion.getValue().format("Y-m-d")+'","precioCompra":"'+txtPrecioCompra.getValue()+
                                                                                    '","marca":"'+cmbMarca.getValue()+'","modelo":"'+cv(gEx('txtModelo').getValue())+'","noSerie":"'+cv(gEx('txtNoSerie').getValue())+
                                                                                    '","factura":"'+txtFactura.getValue()+'","idPedido":"'+txtPedido.getValue()+'","idProveedor":"'+idProveedor+'","centroCosto":"'+gE('hCentroCosto').value+
                                                                                    '","conciliado":"'+cmbSiNo.getValue()+'","valorPor":"'+cmbTipoValor.getValue()+'"}';
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridNoFolio').getStore().reload();
                                                                                gEx('gridHistorial').getStore().removeAll();
                                                                                if(idInventario=='-1')
                                                                                {
                                                                                	function resp10()
                                                                                    {
                                                                                    	gEx('vInventario').close();
                                                                                    }
                                                                                	msgBox('El producto ha sido ingresado al sistema de manera exitosa bajo el n&uacute;mero de inventario: <b>'+str_pad(arrResp[1],6,'0','STR_PAD_LEFT')+'</b>',resp10);
                                                                                    return;
                                                                                }
                                                                                else
                                                                                {
                                                                                	function resp11()
                                                                                    {
                                                                                    	gEx('vInventario').close();
                                                                                    }
                                                                                	msgBox('El la informaci&oacute;n del producto ha sido modificado exitosamente',resp11);
                                                                                    return;
                                                                                }
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=145&cadObj='+cadObj,true);           
																		
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	if(fila!=undefined)
    {
    	gE('hCambs').value=fila.get('claveCABMS');

        cmbCambs.setValue(fila.get('claveCABMS'));
        gEx('txtCuenta').setValue(fila.get('cuenta'));
        gEx('txtSubCuenta').setValue(fila.get('subcuenta'));
        cmbProducto.setValue(fila.get('nombreProducto'));
        gE('hIdProducto').value=fila.get('idProducto');
        gEx('dteFechaAdquisicion').setValue(fila.get('fechaCompra'));
        gEx('txtPrecioCompra').setValue(fila.get('precioCompra'));
        gEx('cmbMarca').setValue(fila.get('marca'));-
 		gEx('txtModelo').setValue(fila.get('modelo'));
        gEx('txtNoSerie').setValue(fila.get('numSerie'));
        gEx('txtFactura').setValue(fila.get('noFactura'));
        gEx('txtPedido').setValue(fila.get('idPedido'));
        gEx('cmbProveedor').setValue(fila.get('txtRazonSocial2'));
        gE('hIdProveedor').value=fila.get('idProveedor');
        cmbSiNo.setValue(fila.get('conciliado'));
        cmbTipoValor.setValue(fila.get('tipoValor'));
        cmbCentroCosto.setValue('centroCosto');
        hCentroCosto.value=fila.get('codigoCentroCosto');
        
        
    }                                
	ventanaAM.show();
}

function verInformacionBaja(iF,iR)
{
	/*var objConf={};
    objConf.ancho=720;
    objConf.alto=460;
    objConf.titulo='Datos de baja';
    objConf.url='../modeloPerfiles/registroFormulario.php';
    objConf.params=[['idFormulario',bD(iF)],['idRegistro',bD(iR)],['cPagina','Sfrm=true']];
	abrirVentanaFancy(objConf);*/
    verRegistroProyecto(iR,0,iF);
}