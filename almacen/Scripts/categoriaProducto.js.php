<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select idProceso,nombre from 4001_procesos order by nombre";
	$arrProcesos=($con->obtenerFilasArreglo($consulta));
?>

var regSeccion=null;
var arrProcesos=<?php echo $arrProcesos?>;
Ext.onReady(inicializar);

function inicializar()
{
	regSeccion=crearRegistro	([ {name: 'idFormulario'},{name: 'nombreFormulario'},{name: 'proceso'},{name: 'orden'}]);
	var _categoriavch=gE('_nombreCategoriavch');
    if(_categoriavch!=null)
    	_categoriavch.focus();
	crearGridSecciones();        
   
}

function validarFrm()
{
	var cadSecciones='';
    var tSecciones=gEx('tSecciones');
    var x;
    var fila;
    var o='';
    var id=gE('idCategoria').value;
    for(x=0;x<tSecciones.getStore().getCount();x++)
    {
    	fila=tSecciones.getStore().getAt(x)
        o='{"idFormulario":"'+fila.get('idFormulario')+'","orden":"'+fila.get('orden')+'"}';
        if(cadSecciones=='')
        	cadSecciones=o;
        else
        	cadSecciones+=','+o;
    }
    cadSecciones='{"arrSecciones":['+cadSecciones+']}';
    if(id=='-1')
    {
        gE('funcPHPEjecutarNuevo').value=bE('asociarSeccionesProducto(@idRegPadre,\''+bE(cadSecciones)+'\')');
    }
    else
    {
        gE('funcPHPEjecutarModif').value=bE('asociarSeccionesProducto('+id+',\''+bE(cadSecciones)+'\')');
    }
    
	if(validarFormularios('frmEnvio'))
    	gE('frmEnvio').submit();
}

function crearGridSecciones()
{
	var dsDatos=eval((gE('arrSecciones').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormulario'},
                                                                    {name: 'nombreFormulario'},
                                                                    {name: 'proceso'},
                                                                    {name: 'orden',type:'int'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Nombre del formulario',
															width:220,
															sortable:true,
															dataIndex:'nombreFormulario',
                                                            renderer:function(val)
                                                                    {
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Proceso',
															width:200,
															sortable:true,
															dataIndex:'proceso',
                                                            renderer:function(val)
                                                                    {
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Orden',
															width:110,
															sortable:true,
															dataIndex:'orden'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            id:'tSecciones',
                                                            renderTo:'tblSecciones',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:635,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar secci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaFormulariosDinamicos();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover secci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos una secci&oacute;n a remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                                var x;
                                                                                                for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                                {
                                                                                                	tblGrid.getStore().getAt(x).set('orden',(x+1));
                                                                                                }
                                                                                            }
                                                                                            
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover las secciones seleccionadas?',resp);
                                                                                    }
                                                                            
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;<b>Modificar orden de secci&oacute;n:&nbsp;&nbsp;</b>'
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignUp.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la secci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.orden>1)
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.orden-2);
                                                                                            fila.data.orden-=1;
                                                                                            filaAux.data.orden+=1;
                                                                                            tblGrid.getStore().sort('orden','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignDown.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la secci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.orden<tblGrid.getStore().getCount())
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.orden);
                                                                                            fila.data.orden+=1;
                                                                                            filaAux.data.orden-=1;
                                                                                            tblGrid.getStore().sort('orden','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function mostrarVentanaFormulariosDinamicos()
{
	
	
    var alOpciones=		new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                 	{name:'idFormulario'},
                                                                    {name:'nombre'}, 
                                                                    {name:'titulo'},
                                                                    {name: 'descripcion'},
                                                                    {name:'idProceso'},
                                                                    {name:'formularioBase'}
                                                                      
                                                                ]
                                                    }
                                                );
    
    
    var dsOpciones= [];
    
    alOpciones.loadData(dsOpciones);
    
    var cmFrmDTD= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'Nombre del formulario',
                                                            width:200,
                                                            dataIndex:'nombre'
                                                        },
                                                        {
                                                        	header:'T\xEDtulo',
                                                            width:150,
                                                            dataIndex:'titulo'
                                                        },
                                                        {
                                                        	header:'Descripci\xF3n',
                                                            width:330,
                                                            dataIndex:'descripcion'
                                                        }
                                                        
                                                       
                                                    ]
                                                );
    
    
    var tblOpciones=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridFormularios',
                                                            store:alOpciones,
                                                            frame:true,
                                                            cm: cmFrmDTD,
                                                            height:300,
                                                            width:750
                                                            
                                                        }
                                                    );
    
    panelGrid=new Ext.Panel	(
                                {
                                    y:50,
                                    items:	[
                                                tblOpciones
                                            ]
                                }
                            );
                            
    
    var cmbProcesos=crearComboExt('cmbProcesos',arrProcesos,480,5,260);
    cmbProcesos.on('select',cargarFormularios);
    
    var form = new Ext.form.FormPanel(	
                                        {
                                            baseCls: 'x-plain',
                                            layout:'absolute',
                                            defaultType: 'textfield',
                                            items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Elija el proceso al cual pertenece el formulario que desea utilizar como secci&oacute;n complementaria:'
                                                        },
                                                        cmbProcesos,
                                                        panelGrid
                                                    ]
                                        }
                                    );
    
    btnSiguiente=new Ext.Button	(
                                    {
                                        text: 'Aceptar',
                                        minWidth:80,
                                        id:'btnFinalizar',
                                        listeners:	{
                                                        click:
                                                                {
                                                                    fn:function()
                                                                    {
                                                                    	var tSecciones=gEx('tSecciones');
                                                                        var filaSel= gEx('gridFormularios').getSelectionModel().getSelected();
                                                                        if(filaSel==null)
                                                                        {
                                                                        	msgBox('Debe seleccionar el formulario que desa utilizar como secci&oacute;n');
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                       	if(obtenerPosFila(tSecciones.getStore(),'idFormulario',filaSel.get('idFormulario'))==-1)
                                                                        {
                                                                            var r=new regSeccion(	{
                                                                                                    idFormulario:filaSel.get('idFormulario'),
                                                                                                    nombreFormulario:filaSel.get('nombre'), 
                                                                                                    proceso:cmbProcesos.getRawValue(),
                                                                                                    orden:(tSecciones.getStore().getCount()+1)
                                                                                                }
                                                                                            );
                                                                            tSecciones.getStore().add(r);
                                                                        }
                                                                        ventanaSelForm.close();
                                                                        
                                                                    }
                                                                }
                                                    }
                                    }
                                )
    
    ventanaSelForm = new Ext.Window(
                                            {
                                                title: 'Selecci\xF3n de formulario',
                                                width: 780 ,
                                                height:450,
                                                minWidth: 300,
                                                minHeight: 100,
                                                layout: 'fit',
                                                plain:true,
                                                modal:true,
                                                bodyStyle:'padding:5px;',
                                                buttonAlign:'center',
                                                items: 	[
                                                            form
                                                        ],
                                                listeners : {
                                                            show : {
                                                                        buffer : 10,
                                                                        fn : function() 
                                                                        {
                                                          			                  
                                                                        }
                                                                    }
                                                        },
                                                buttons:	[
                                                                btnSiguiente,
                                                                {
                                                                    text: 'Cancelar',
                                                                    handler:function()
                                                                    {
                                                                    	
                                                                        ventanaSelForm.close();
                                                                        
                                                                    }
                                                                }
                                                            ]
                                            }
                                        );
	
    ventanaSelForm.show();
}

function cargarFormularios(combo,registro,indice)
{

	function funcResp()
    {
    	var arrResp=peticion_http.responseText.split('|');
        if(arrResp[0]=='1')
		{
            	var arrTablas=eval(arrResp[1]);
                var almacen=Ext.getCmp('gridFormularios').getStore();
                almacen.loadData(arrTablas);
               	
		}
		else
		{
			msgBox('No se ha podido realizar la operación debido al siguiente problema:'+' <br />'+arrResp[0]);
		}
    }
    obtenerDatosWeb('../paginasFunciones/funcionesFormulario.php',funcResp, 'POST','funcion=14&idProceso='+registro.get('id'),true);
}
