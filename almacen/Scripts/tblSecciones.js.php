<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
Ext.onReady(inicializar);

function inicializar()
{

	nuevoDTD();
}

var panelArbol;
function nuevoDTD()
{
  	 var idAlmacen=gE('idAlmacen').value;
     var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
      
      var cargadorArbol=new Ext.tree.TreeLoader(
												   {
												     baseParams:{
																
																funcion:'31',
																idAlmacen:idAlmacen
															},
														dataUrl:'../paginasFunciones/funcionesAlmacen.php'
													}	
		                                         )	
		//function preparar(cargador)
//		{
//			var idMateria=gE('idMateria').value;
//			cargador.baseParams.funcion=3;
//			cargador.baseParams.idMateria=idMateria;
//		
//		}                            
                                                 
		//cargadorArbol.on('beforeload')//,preparar)                                                 
		panelArbol=new Ext.tree.TreePanel	(
                                              {
												  id:'tblArbol',
                                                  useArrows:true,
                                                  autoScroll:true,
                                                  animate:false,
                                                  enableDD:true,
                                                  containerScroll:true,
                                                  root:'Secciones',
                                                  loader:cargadorArbol,
                                                  height:500,
												  width:600,
												  collapsible: true,
                                                  draggable:false,
												  rootVisible:true,
												  el:'tblTabla',
                                                  tbar:[
                                                  			{
                                                                text:'Agregar Secci&oacute;n',
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                    {
                                                                        agregarSeccion(1);
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                text:'Agregar Sub-Secci&oacute;n',
                                                                icon:'../images/area.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                    {
                                                                       agregarSeccion(2);
                                                                    }
                                                            },
                                                            {
                                                                text:'Modificar',
                                                                icon:'../images/update_nw.gif',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                    {
                                                                       agregarSeccion(3);
                                                                    }
                                                            },
                                                            {
                                                                text:'Borrar Secci&oacute;n',
                                                                icon:'../images/cancel_round.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                    {
                                                                       eliminar();
                                                                    }
                                                            }
                                                  		]
											  }
                                          );                                                 	  
      panelArbol.render();
      panelArbol.expandAll();
}

//function guardar(form)
//{
//	gE(form).action="../Administracion/fichaMateria.php";
//    gE(form).submit();			
//}
//
//function regresar(form)
//{
//	var arrP=[];
//	enviarFormularioDatos('../Administracion/fichaMateria.php',arrP);				
//}
//
//

function agregarSeccion(bandera)
{
	 var idAlmacen=gE('idAlmacen').value;
     var cadena='';
     var selNodes = panelArbol.getChecked();
     var tamano=selNodes.length;
     var x;
     var nPagina=gE('nPagina').value;
     
     switch(bandera)
     {
         case 1:
            var arrP=[['idAlmacen',idAlmacen],['cadena',cadena],['idSeccion',-1],['bandera',bandera],['cPagina','sFrm=true|mR1=true'],['nPagina',nPagina]];
	 		enviarFormularioDatos('../almacen/secciones.php',arrP);	
            
         break;
         case 2:
         	if(tamano==0)
            {
            	Ext.MessageBox.alert(lblAplicacion,'Debe Seleccionar al menos un elemento');
                return;
            }
             
            for(x=0;x<tamano;x++)
            {
                var id=selNodes[x].id;
                if(cadena=='')
                    cadena=id;
                else
                    cadena+=','+id;    
            }
             
            var arrP=[['idAlmacen',idAlmacen],['cadena',cadena],['bandera',bandera],['cPagina','sFrm=true|mR1=true'],['nPagina',nPagina]];
	 		enviarFormularioDatos('../almacen/secciones.php',arrP);			
         break;
         case 3:
         	if(tamano==0)
            {
            	Ext.MessageBox.alert(lblAplicacion,'Debe Seleccionar al menos un elemento');
                return;
            }
            
            if(tamano>1)
            {
            	Ext.MessageBox.alert(lblAplicacion,'Debe Seleccionar solo un elemento');
                return;
            }
            
            var id=selNodes[0].id;
            var arrP=[['idAlmacen',idAlmacen],['cadena',cadena],['bandera',bandera],['idSeccion',id],['cPagina','sFrm=true|mR1=true'],['nPagina',nPagina]];
	 		enviarFormularioDatos('../almacen/secciones.php',arrP);		
         break;
     }
}


function agregarSubtemas(idMateria,idCiclo)
{
	//var selNodes = panelArbol.getChecked();
//    var idTema=selNodes[0].id;
//    var idMateria=gE('idMateria').value;
//    var idCiclo=gE('idCiclo').value;
//    
//            var arrP=[['idPadre',idTema],['idMateria',idMateria],['idCiclo',idCiclo]];
//            enviarFormularioDatos('../Administracion/temas.php',arrP);										


}

//
//
//function modificarTema(idMateria,idCiclo)
//{
//	var selNodes = panelArbol.getChecked();
//    var idTema=selNodes[0].id;
//    var idMateria=gE('idMateria').value;
//    var idCiclo=gE('idCiclo').value;
//   
//    var arrP=[['idTema',idTema],['idMateria',idMateria],['idCiclo',idCiclo]];
//	enviarFormularioDatos('../Administracion/temas.php',arrP);										
//   
//}
//
function eliminar()
{
  var selNodes = panelArbol.getChecked();
  if((selNodes.length==0) || (selNodes==''))
  {
  	 Ext.MessageBox.alert(lblAplicacion,'Debe Seleccionar al menos un elemento');
     return;
  }
      var cadena='';
        
        for(x=0;x<selNodes.length;x++)
        {	
            if(cadena=='')
                cadena=selNodes[x].id;
            else
                cadena+=","+selNodes[x].id;
        }
            
        function respPregunta(btn)
        {
            if(btn=='yes')
            {
                    function funcAjax()
                    {
                        var resp=peticion_http.responseText;
                        arrResp=resp.split('|');
                        if(arrResp[0]=='1')
                        {
                            var raiz=panelArbol.getRootNode() ;
                            raiz.reload();
                            panelArbol.expandAll();
                        }
                        else
                        {
                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                        }
                    }
                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=32&idAlmacen='+idAlmacen+'&cadena='+cadena,true);
            }
        }
        Ext.MessageBox.confirm(lblAplicacion,'Si elimina los elementos seleccionados borraran los registros asociados a estos.<br />Est&aacute; seguro de ejecutar esta acci&oacute;n',respPregunta);   
}

//
//function asigna(radio)
//{
//	var valor=radio.value;
//    gE('idTema').value=valor;
//}
//
//function eliminarTabla()
//{
//	 var valor=gE('idTema').value;
//     var tr=gE('tr_'+valor);
//     var padre=tr.parentNode;
//     padre.removeChild(tr);
//     
//}
//
//function agregarSubtema()
//{
//
//      var selNodes = panelArbol.getChecked();
//      if (selNodes==0)
//          Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
//      if(selNodes.length>1)
//          Ext.MessageBox.alert(lblAplicacion,'Seleccione solo un tema')
//      if(selNodes.length==1)
//      {
//          
//         agregarSubtemas();
//         selNodes=null;
//      }
//}
//
//function modificar()
//{
//    var selNodes = panelArbol.getChecked();
//    
//    if (selNodes==0)
//    
//        Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
//    if(selNodes.length>1)
//        Ext.MessageBox.alert(lblAplicacion,'Seleccione solo un tema para modificar')
//    if(selNodes.length==1)
//    {
//        
//       modificarTema();
//       selNodes=null;
//        
//    }
//}
//
//function eliminar()
//{
//	var selNodes = panelArbol.getChecked();
//     if (selNodes==0)
//        
//            Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
//     else
//     
//            eliminarTema();
//}