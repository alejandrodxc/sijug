<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	
     var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:900,
                                        height:450,
                                        items: [
                                                    {
                                                       title: 'General',
                                                       contentEl:'tblGral'
                                                    }
                                                    
                                                     
                                                ]
                                    }
                               );
                               
	}

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	gE('frmEnvio').submit();
        
    }
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}


function cerrarVentana()
{
	TB_remove();
}
