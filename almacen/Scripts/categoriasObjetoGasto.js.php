<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT codigoControl,CONCAT('[',clave,'] ',nombreObjetoGasto) AS objetoGasto  FROM 507_objetosGasto 
				WHERE STATUS=1 AND nivel=1 ORDER BY clave";
	$arrCapitulos=$con->obtenerFilasArreglo($consulta);


?>

var arrCapitulos=<?php echo $arrCapitulos?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridObjetoGasto=crearGrid();
}

function crearGrid()
{
	var cmbCapitulo=crearComboExt('cmbCapitulo',arrCapitulos,0,0,325);
    var cmbObjetoGasto=crearComboExt('cmbObjetoGasto',[],0,0,375);
    cmbObjetoGasto.on('select',function (cmb,registro)
    							{
                                	habilitarBotones();
                                    
                                	gEx('gridObjetoGasto').getStore().reload();
                                }
                     )
    			
    
    cmbCapitulo.on('select',function(cmb,registro)
    						{
                            	gEx('gridObjetoGasto').getStore().removeAll();
                                desHabilitarBotones();
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        cmbObjetoGasto.getStore().loadData(eval(arrResp[1]));
                                        cmbObjetoGasto.reset();
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=97&capitulo='+registro.get('id'),true);


                            }
    			)
   	var alDatos= new Ext.data.JsonStore({
                                            
                                            totalProperty :'numReg',
                                            fields: [
                                                        {name: 'idCategoria'},
                                                        {name: 'cveCategoria'},
                                                        {name: 'nombreCategoria'},
                                                        {name: 'descripcion'}
                                                    ],
                                             proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                      
                                                                                  }

                                                                              ),
                                            autoLoad:false,
                                            root:'registros',
                                            remoteSort: false
                                        }
                                      );

    alDatos.on('beforeload',function(proxy)
                                    {
                                        proxy.baseParams.funcion=115;
                                        proxy.baseParams.objetoGasto=cmbObjetoGasto.getValue();
                                    }
                        )
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                         {
                                                            header:'Cve. categor&iacute;a',
                                                            width:100,
                                                            sortable:true,
                                                            dataIndex:'cveCategoria'
                                                        },
                                                        {
                                                            header:'Nombre de la categor&iacute;a',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'nombreCategoria'
                                                        },
                                                        {
                                                            header:'Descripci&oacute;n',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'descripcion'
                                                        }
                                                    ]
                                                );
	
    
    var panel=new Ext.Panel   (	
    								{
                                    	layout:'anchor',
                                        frame:true,
                                        items:[
                                                    {
                                                        id:'gridObjetoGasto',
                                                        store:alDatos,
                                                        xtype:'grid',
                                                        frame:false,
                                                        cm: cModelo,
                                                        stripeRows :true,
                                                        loadMask:true,
                                                        border:false,
                                                        columnLines : true,
                                                        anchor:'100% 100%',
                                                        tbar:	[
                                                                   {
                                                                   		id:'btnAgregar',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        disabled:true,
                                                                        text:'Agregar categor&iacute;a',
                                                                        handler:function()
                                                                                {
                                                                                    mostrarVentanaAgregarCategoria();
                                                                                }
                                                                        
                                                                    },
                                                                    {
                                                                    	id:'btnModificar',
                                                                        icon:'../images/pencil.png',
                                                                        cls:'x-btn-text-icon',
                                                                        disabled:true,
                                                                        text:'Modificar categor&iacute;a',
                                                                        handler:function()
                                                                                {
                                                                                	var fila=gEx('gridObjetoGasto').getSelectionModel().getSelected();
                                                                                    if(fila==null)
                                                                                    {
                                                                                    	msgBox('Debe seleccionar la categor&iacute;a que desea modificar');
                                                                                        return;
                                                                                    }
                                                                                    mostrarVentanaAgregarCategoria(fila);
                                                                                }
                                                                        
                                                                    },
                                                                    {
                                                                    	id:'btnRemover',
                                                                        icon:'../images/delete.png',
                                                                        cls:'x-btn-text-icon',
                                                                        disabled:true,
                                                                        text:'Remover categor&iacute;a',
                                                                        handler:function()
                                                                                {
                                                                                    var fila=gEx('gridObjetoGasto').getSelectionModel().getSelected();
                                                                                    if(fila==null)
                                                                                    {
                                                                                    	msgBox('Debe seleccionar la categor&iacute;a que desea remover');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    gEx('gridObjetoGasto').getStore().remove(fila);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=117&idCategoria='+fila.get('idCategoria'),true);

                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer remover la categor&iacute;a seleccionada?',resp);
                                                                                }
                                                                        
                                                                    }
                                                                ]
                                                    }
                                                ],
                                        renderTo:'tblCategorias',
                                        width:900,
                                        height:350,
                                        tbar:	[
                                                    {
                                                        xtype:'label',
                                                        html:'&nbsp;<span class="letraRojaSubrayada8"><b>Cap&iacute;tulo:</b></span>&nbsp;'
                                                    },
                                                    cmbCapitulo,
                                                    
                                                    '-',
                                                    
                                                    {
                                                        xtype:'label',
                                                        html:'&nbsp;<span class="letraRojaSubrayada8"><b>Objeto de gasto:</b></span>&nbsp;'
                                                    },
                                                    cmbObjetoGasto
                                                ]
                                                                                 
									}
                                                        
		                      );
    return 	panel;	
}

function habilitarBotones()
{
	gEx('btnAgregar').enable();
    gEx('btnModificar').enable();
    gEx('btnRemover').enable();
}

function desHabilitarBotones()
{
	gEx('btnAgregar').disable();
    gEx('btnModificar').disable();
    gEx('btnRemover').disable();
}

function mostrarVentanaAgregarCategoria(fila)
{
	var lblCategoria='Agregar categor&iacute;a';
    if(fila!=undefined)
    	lblCategoria='Modificar categor&iacute;a'
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cve. categor&iacute;a: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            id:'txtCveCategoria',
                                                            width:110,
                                                            x:125,
                                                            y:5
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre categor&iacute;a: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            id:'txtNombre',
                                                            width:350,
                                                            x:125,
                                                            y:35
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n:'
                                                        },
                                                        {
                                                        	xtype:'textarea',
                                                            id:'txtDescripcion',
                                                            width:350,
                                                            heigth:80,
                                                            x:125,
                                                            y:65
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblCategoria,
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCveCategoria').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCveCategoria=gEx('txtCveCategoria');
                                                                        if(txtCveCategoria.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtCveCategoria.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la clave de la categor&iacute;a',resp);
                                                                        }
                                                                        
                                                                        var txtNombre=gEx('txtNombre');
                                                                        if(txtNombre.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtNombre.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre de la categor&iacute;a',resp2);
                                                                        }
                                                                        var idRegistro=-1;
                                                                        if(fila!=undefined)
                                                                        	idRegistro=fila.get('idCategoria');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        var obj='{"cveCategoria":"'+cv(txtCveCategoria.getValue())+
                                                                        		'","nombreCategoria":"'+cv(txtNombre.getValue())+
                                                                                '","descripcion":"'+gEx('txtDescripcion').getValue()+
                                                                                '","idCategoria":"'+idRegistro+'","objetoGasto":"'+gEx('cmbObjetoGasto').getValue()+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridObjetoGasto').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=116&cadObj='+obj,true);

																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	if(fila!=undefined)
    {
    	var txtCveCategoria=gEx('txtCveCategoria');
        txtCveCategoria.setValue(fila.get('cveCategoria'))
        var txtNombre=gEx('txtNombre');
        txtNombre.setValue(fila.get('nombreCategoria'));
        var txtDescripcion=gEx('txtDescripcion');
        txtDescripcion.setValue(fila.get('descripcion'));
    }
	                                
	ventanaAM.show();
}