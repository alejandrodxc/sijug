<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridCategorias();
}

function crearGridCategorias()
{
	 var dsCategorias=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad: true,
                                                        fields: [
                                                                  {name: 'idCategoria'},
                                                                  {name: 'nombre'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'conceptos'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	                                                    
	dsCategorias.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=14;
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombre' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
   var expander = new Ext.ux.grid.RowExpander({
                                              column:2,
                                              tpl : new Ext.Template(
                                                                      '<table >'+
                                                                      '<tr>'+
                                                                      	'<td colspan="2" height="10"></td>'+
                                                                        '</tr>'+
                                                                        '<tr>'+
                                                                          '<td colspan="2"><span class="letraAzul">Descripci&oacute;n:</span><br /></td>'+
                                                                        '</tr>'+
                                                                        '<tr>'+
                                                                          '<td colspan="2" width="200"><span class="copyrigthSinPadding">{descripcion}</span><br /></td>'+
                                                                       '</tr>'+
                                                                       '<tr>'+
                                                                          '<td width:"230" colspan="2"><span class="letraAzul">Conceptos:<br /></span></td>'+
                                                                       '</tr>'+
                                                                       '<tr>'+
                                                                          '<td colspan="2"><span class="copyrigthSinPadding">{conceptos}</span><br /><br /></td>'+
                                                                      '</tr>'+
                                                                      '</table>'
                                                                      
                                                                    )
                                          });                                                       
                                       
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        expander,
														{
															header:'Categoria',
															width:600,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left'
														}
													]
												);
	var tblGridCat=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gCategorias',
                                                            title:'Categorias Alm&aacute;cen',
                                                            store:dsCategorias,
                                                            renderTo:'tblCategorias',
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:700,
                                                            width:800,
                                                            tbar:[
                                                            	  {
                                                                      text:'Agregar',
                                                                      icon:'../images/add.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              agregar(-1);
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'Modificar',
                                                                      icon:'../images/pencil.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              var almacen=tblGridCat.getSelectionModel();
                                                                              var fila=almacen.getSelected();
                                                                              if(fila==null)
                                                                              {
                                                                                  Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un elemento');
                                                                                  return;
                                                                              }
                                                                              agregar(fila.get('idCategoria'));
                                                                          }
                                                                  },
                                                                  {
                                                                      text:'Eliminar',
                                                                      id:'removerB',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                                var almacen=tblGridCat.getSelectionModel();
                                                                                var fila=almacen.getSelected();
                                                                                if(fila==null)
                                                                                {
                                                                                    Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un elemento');
                                                                                    return;
                                                                                }
																				                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                    if(btn=='yes')
                                                                                    {
                                                                                    	var id=fila.get('idCategoria');
                                                                                        function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText.split('|');
                                                                                            if(resp[0]==1)
                                                                                            {
                                                                                                 tblGridCat.getStore().reload();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                 Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=22&id='+id,true);
                                                                                    }
                                                                                }
                                                                                 msgConfirm('Esta seguro de eliminar este elemento',resp);     
                                                                          }
                                                                  }
                                                                ]
                                                            ,
                                                            plugins: [filters,expander]
                                                        }
                                                    );
}

function agregar(id)
{
    var arrP=[['idCategoria',id]];
	enviarFormularioDatos('../almacen/categoriasVSArea.php',arrP);	
}    
    
   // var grid=crearGridConceptos(id);
//    var form = new Ext.form.FormPanel(	
//										{
//                                        	id:'ventanaHistorial',
//											baseCls: 'x-plain',
//											layout:'absolute',
//											defaultType: 'textfield',
//											items: 	[ 
//                                                        {
//                                                            xtype:'label',
//                                                            x:150,
//                                                            y:15,
//                                                            html:'Nombre:'
//                                                        },
//                                                        {
//                                                            x:200,
//                                                            y:10,
//                                                            xtype:'textfield',
//                                                            width:300,
//                                                            id:'nombreC'
//                                                        },
//                                                        {
//                                                            xtype:'label',
//                                                            x:130,
//                                                            y:40,
//                                                            html:'Descripci&oacute;n:'
//                                                        },
//                                                        {
//                                                            xtype:'textarea',
//                                                            x:200,
//                                                            y:40,
//                                                            width:300,
//                                                            height:80,
//                                                            id:'descripcionC'
//                                                        }
//                                                        ,
//                                                            grid
//													]
//										}
//									);
//    var ventana = new Ext.Window(
//									{
//										title: 'Categor&iacute;a',
//										width: 700,
//										height:530,
//										minWidth: 300,
//										minHeight: 100,
//										layout: 'fit',
//										plain:true,
//										modal:true,
//										bodyStyle:'padding:5px;',
//										buttonAlign:'center',
//										items: form,
//                                        id:'ventanaCategoria',
//										listeners : {
//													show : {
//																buffer : 10,
//																fn : function() 
//																{
//																	
//																}
//															}
//												},
//                                        buttons:	[
//														{
//															id:'btnAceptar',
//															text: 'Aceptar',
//															listeners:	{
//																			click:function()
//																				{
//																					var nomb=Ext.getCmp('nombreC').getValue();
//                                                                                    if(nomb=='')
//                                                                                    {
//                                                                                     	msgBox('Debe escribir el nombre');
//                                                                                        return;
//                                                                                    }
//                                                                                    
//                                                                                    var conceptos=Ext.getCmp('gridCatVSConcepto').getSelectionModel().getSelections();
//                                                                                    var tamano=conceptos.length;
//                                                                                    if(tamano==0)
//                                                                                    {
//                                                                                    	msgBox('Debe seleccionnar al menos un concepto');
//                                                                                        return;
//                                                                                    }
//                                                                                    var descripcion=Ext.getCmp('descripcionC').getValue();
//                                                                                    var x;
//                                                                                    var cadena='';
//                                                                                    for(x=0;x<tamano;x++)
//                                                                                    {
//                                                                                    	var clave=conceptos[x].get('clave');
//                                                                                        
//                                                                                        if(cadena=='')
//                                                                                        	cadena=clave;
//                                                                                        else
//                                                                                        	cadena+=','+clave;
//                                                                                    }
//                                                                                    
//                                                                                    function funcAjax()
//                                                                                    {
//                                                                                        var resp=peticion_http.responseText.split('|');
//                                                                                        if(resp[0]==1)
//                                                                                        {
//                                                                                             var suma=0;
//                                                                                             var almacen=Ext.getCmp('articulos').getStore();
//    		  																				 almacen.load({callback:function()
//                                                                                                                            {
//                                                                                                                                 var tamanoS=almacen.getCount();
//                                                                                                                                 var y;
//                                                                                                                                 for(y=0;y<tamanoS;y++)
//                                                                                                                                 {
//                                                                                                                                    var elemento=almacen.getAt(y);
//                                                                                                                                    if(elemento.get('estado')==2)
//                                                                                                                                    {
//                                                                                                                                        suma=suma+1;
//                                                                                                                                    }
//                                                                                                                                 }
//                                                                                                                                 
//                                                                                                                                 var boton=Ext.getCmp('removerB');
//                                                                                                                                 if(suma>0)
//                                                                                                                                 {
//                                                                                                                                      boton.show();
//                                                                                                                                 }
//                                                                                                                                 else
//                                                                                                                                 {
//                                                                                                                                      boton.hide();
//                                                                                                                                 }
//                                                                                                                            }
//                                                                                                                  }); 
//                                                                                             ventana.close();
//                                                                                        }
//                                                                                        else
//                                                                                        {
//                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
//                                                                                        }
//                                                                                    }
//                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=8&idPedido='+idPedido+'&idProducto='+idProducto+'&observaciones='+observaciones,true)
//																				}
//																		}
//														},
//														{
//															text: 'Cancelar',
//															handler:function()
//																	{
//                                                                        ventana.close();
//																	}
//														}
//													]        
//									}
//								);
//        
//        llenarDatos(ventana,id);
//}
//
//function llenarDatos(ventana,id)
//{
//	if(id==-1)
//    {
//    	ventana.show();
//    }
//    else
//    {
//        function funcAjax()
//        {
//            var resp=peticion_http.responseText.split('|');
//            if(resp[0]==1)
//            {
//                 Ext.getCmp('nombreC').setValue(resp[1]);
//                 Ext.getCmp('descripcionC').setValue(resp[2]);
//                 var almacen=Ext.getCmp('gridCatVSConcepto').getStore();
//                 var arregloR=resp[3].split(',');
//                 var tamano=arregloR.length;
//                 var x;
//                 for(x=0;x<tamano;x++)
//                 {
//                 	
//                    var existe=obtenerPosFila(almacen,'clave',arregloR[x])
//                    if(existe!=-1)
//                    {
//                    	alert('encontro');
//                    }
//                 
//                 }
//                 
//                 ventana.show();
//            }
//            else
//            {
//                Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
//            }
//        }
//        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=22&id='+id,true);
//	}
//}
//
//function crearGridConceptos(id)
//{
//    var dsConceptos=new Ext.data.JsonStore({
//                                                        root: 'registros',
//                                                        totalProperty: 'numReg',
//                                                        autoLoad: true,
//                                                        fields: [
//                                                                  {name: 'idObjetoGasto'},
//																  {name: 'clave'},
//                                                                  {name: 'nombreObjetoGasto'}
//                                                              ],         
//                                                        proxy : new Ext.data.HttpProxy	(
//
//                                                                                          {
//                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
//                                                                                          }
//                                                                                      )                             
//                                                    })
//	                                                    
//	dsConceptos.on('beforeload',function(proxy)
//    								{
//                                    	proxy.baseParams.funcion=21;
//                                        proxy.baseParams.id=id;
//                                    }
//                        );
//   
//   var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'nombreObjetoGasto' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
//	var chkRow=new Ext.grid.CheckboxSelectionModel();
//	var cModelo= new Ext.grid.ColumnModel   	(
//												 	[
//													 	new  Ext.grid.RowNumberer(),
//														chkRow
//                                                        ,
//														{
//															header:'Clave',
//															width:100,
//															sortable:true,
//															dataIndex:'clave'
//														},
//														{
//															header:'Nombre Objeto de Gasto',
//															width:470,
//															sortable:true,
//															dataIndex:'nombreObjetoGasto'
//														}
//													]
//												);
//                                                
//	var tblGrid=	new Ext.grid.EditorGridPanel	(
//                                                        {
//                                                            id:'gridCatVSConcepto',
//                                                            x:10,
//                                                            y:130,
//                                                            store:dsConceptos,
//                                                            //frame:true,
//                                                            cm: cModelo,
//                                                            height:300,
//                                                            width:650,
//                                                            sm:chkRow
//                                                            ,
//                                                            plugins: [filters]
//														}
//													);
//   return tblGrid;
//}
//
//
//function modificar()
//{
//	alert('modificar');
//}

