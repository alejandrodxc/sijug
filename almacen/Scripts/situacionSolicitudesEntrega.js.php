<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idSituacion,situacionSolicitudEntrega FROM 1000_situacionSolicitudesEntrega";
	$arrSituacionSolicitud=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT ciclo,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="select ciclo FROM 550_cicloFiscal where status=1";
	$cicloVigente=$con->obtenerValor($consulta);
?>
var arrSituacionSolicitud=<?php echo $arrSituacionSolicitud?>;
var arrCiclo=<?php echo $arrCiclo?>;
Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();

	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos
                                                        
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var tamPagina=100;
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,120);
    cmbCiclo.setValue(<?php echo $cicloVigente ?>);
    cmbCiclo.on('select',function(combo,registro)
    						{
                            	gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                                gEx('btnCancelar').disable();
                            }
    			)
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idSolicitudEntrega'},
		                                                {name:'tituloPrograma'},
		                                                {name:'ruta'},
                                                        {name:'cvePrograma'},
                                                        {name: 'nombreProducto'},
                                                        {name: 'cantidad', type:'int'},
                                                        {name: 'fechaSolicitud', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'fechaLimitePrefenteEntrega',  type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'comentarioSolicitud'},
                                                        {name: 'idEstado'},
                                                        {name: 'Nombre'},
                                                        {name: 'fechaAgendaEntrega',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'respRecepcion'},
                                                        {name: 'fechaUltimaOperacion',type:'date', dateFormat:'Y-m-d'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'unidad'},
                                                                        {type: 'string', dataIndex: 'nombreProducto'},
                                                                        {type: 'string', dataIndex: 'tituloPrograma'},
                                                                        {type: 'string', dataIndex: 'cvePrograma'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'date', dataIndex: 'fechaSolicitud'},
                                                                        {type: 'date', dataIndex: 'fechaLimitePrefenteEntrega'},
                                                                        {type: 'date', dataIndex: 'situacionSolicitud'},
                                                                        {type: 'date', dataIndex: 'fechaAgendaEntrega'},
                                                                        {type: 'list', dataIndex: 'idEstado', options:arrSituacionSolicitud, phpMode: true}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaSolicitud', direction: 'ASC'},
                                                            groupField: 'fechaSolicitud',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='101';
                                        proxy.baseParams.ciclo=gEx('cmbCiclo').getValue();
                                        gEx('btnCancelar').disable();
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Fecha solicitud',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaSolicitud',
                                                            css:'text-align:right !important;',
                                                            hideable:true,
                                                             renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
														},
													 	
														{
															header:'Programa',
															width:130,
															sortable:true,
															dataIndex:'ruta',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'',
															width:50,
															sortable:true,
															dataIndex:'cvePrograma',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
                                                        {
															header:'Cantidad solicitada',
															width:100,
															sortable:true,
															dataIndex:'cantidad',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Fecha solicitada <br />de entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaLimitePrefenteEntrega',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Fecha programada <br />de entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaAgendaEntrega',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return '<font color="green">'+val.format('d/m/Y')+'</font>';
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         
                                                         {
															header:'Recibido por',
															width:220,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'respRecepcion',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Situaci&oacute;n',
															width:100,
															sortable:true,
															dataIndex:'idEstado',
                                                            css:'text-align:right !important;',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrSituacionSolicitud,val);
                                                                    }
														},
                                                        {
															header:'Fecha &uacute;ltima<br /> operaci&oacute;n',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaUltimaOperacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return '<font color="green">'+val.format('d/m/Y')+'</font>';
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
														{
															header:'&Uacute;ltimo Comentario',
															width:300,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'comentarioSolicitud',
                                                            hideable:true
														},
                                                        {
															header:'Responsable solicitud',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'Nombre',
                                                            hideable:true
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Solicitudes de entrega</b></span>',
                                                            columnLines:true,
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000"><b>Ciclo: </b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbCiclo,'-',
                                                                        {
                                                                        	id:'btnCancelar',
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Cancelar solicitud',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                    	mostrarVentanaCancelacionSolicitud(fila);
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if((registro.get('idEstado')=='1')||(registro.get('idEstado')=='2'))
                                                    {
                                                    	gEx('btnCancelar').enable();
                                                    }
                                                    else
                                                    	gEx('btnCancelar').disable();
                                                }
                                  )							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:101,ciclo:gEx('cmbCiclo').getValue()}});                                                  
	return tblGrid;                                                                                                     
}

function mostrarVentanaCancelacionSolicitud(fila)
{	

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Motivo  de la cancelaci&oacute;n: <font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar solicitud',
										width: 420,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo de la cancelaci&oacute;n',resp2);
                                                                        }
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                                														gEx('btnCancelar').disable();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=102&idSolicitud='+fila.get('idSolicitudEntrega')+'&situacion=5&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             }
                                                                    	
                                                                       	}
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar la solicitud de entrega seleccionada?',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	

}