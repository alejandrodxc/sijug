<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idCondicionPago,nombreCondicionPago FROM 6911_condicionesPago ORDER BY nombreCondicionPago";
	$arrCondiciones=$con->obtenerFilasArreglo($consulta);
?>


var arrCondiciones=<?php echo $arrCondiciones?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
    var gridDetalle=crearGridDetalle();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos,
                                                        gridDetalle
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
                                                        {name: 'rfc'},
		                                                {name: 'nombreProveedor'},
		                                                {name:'folioPedido'},
                                                        {name:'numEntrega'},
                                                        {name:'condicionPago'},
		                                                {name:'fechaRecepcion', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'diferencia', type:'int'},
                                                        {name: 'comentarios'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'nombreProveedor'},
                                                                        {type: 'string', dataIndex: 'folioPedido'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'},
                                                                        {type: 'int', dataIndex:'num_entrega'},
                                                                        //{type: 'string', dataIndex:'txtRFC'},
                                                                        {type: 'string', dataIndex: 'num_Factura'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'date', dataIndex: 'fecha_entrada'}
                                                                    ]
                                                    }
                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='137';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var tamPagina=100;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Folio pedido',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'No. Entregable',
															width:100,
															sortable:true,
															dataIndex:'numEntrega',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'RFC',
															width:150,
															sortable:true,
															dataIndex:'rfc',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
													 	{
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'nombreProveedor',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	return val;
                                                                                	
                                                                                }
														},
														
                                                        {
															header:'Fecha entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaRecepcion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
														
                                                        {
															header:'D&iacute;as restantes',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'diferencia',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='green';
                                                                    	if(val<0)
                                                                        	color="red";                                                                        
                                                                         return '<font color="'+color+'">'+val+'</font>';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Condici&oacute;n de pago',
															width:130,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'condicionPago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCondiciones,val);
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Comentarios',
															width:350,
															sortable:true,
															dataIndex:'comentarios',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Pedidos por recibir</b></span>',
                                                            columnLines:true,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/printer.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:true,
                                                                            text:'Pedidos por recibir',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrDatos=[['idAlmacen',gE('idAlmacen').value]];
                                                                                        enviarFormularioDatos('../reportes/entradaProgramadas.php',arrDatos);
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/printer.png',
                                                                            hidden:true,
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Pedidos atrasados',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrDatos=[['idAlmacen',gE('idAlmacen').value]];
                                                                                        enviarFormularioDatos('../reportes/reporteEntradasNoCumplidas.php',arrDatos);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/printer.png',
                                                                            hidden:true,
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Formato de pedido',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	nsgBox('Debe seleccionar el pedido cuyo formato desea imprimir');
                                                                                        	return;
                                                                                        }
                                                                                        var arrParam=[['folioPedido',fila.get('folioPedido')]];
                                                                                        enviarFormularioDatos('../reportes/formatoPedido2.php',arrParam);
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridDetallePedido').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:registro.get('idPedido'),funcion:138}})
                                                }
    							)                                                 
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:137,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}


function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'},
                                                        {name: 'total'},
                                                        {name:'subtotal'},
                                                        {name: 'cveProducto'},
                                                        {name: 'cve_grupo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='86';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Cve. Producto',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'cveProducto',
                                                           
                                                            hideable:true
														},
                                                       
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'subtotal',
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'total',
                                                            hideable:true
														},
                                                        {
															header:'Contenedor',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            
                                                            dataIndex:'contenedor',
                                                            hideable:true
														},
                                                        {
															header:'Unidad de medida',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'unidadMedida',
                                                            hideable:true
														},
                                                        {
															header:'Presentacion',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'presentacion',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:230,
                                                            
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:[
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registra entrada de producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el pedido cuyo ingreso desea registrar ');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaRegistroEntrada(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Rechazar entrada de producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el pedido que desea rechazar');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                          		mostrarVentanaMotivo(fila,3,'Rechazar pedido');
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer rechazar este pedido? ',resp)
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/page_accept.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Solicitar autorizaci&oacute;n de entrada',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea enviar a autorizacion');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	mostrarVentanaMotivo(fila,4,'Enviar pedido a validaci&oacute;n');
                                                                                            	
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer enviar este pedido a validaci&oacute;n? ',resp)
                                                                                        	
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                           
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}

function mostrarVentanaMotivo(fila,etapa,titulo)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el motivo por el cual desea llevar  a cabo esta acci&oacute;n'
                                                        },
                                                        {
                                                        	x:25,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:400,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: titulo,
										width: 500,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                    	if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp(btn)
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe indicar el motivo de la operaci&oacute;n ',resp);
                                                                            return;
                                                                        }
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().remove(fila);
                                                                                gEx('gridDetallePedido').getStore().removeAll();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=90&idPedido='+fila.get('idPedido')+'&motivo='+cv(txtMotivo.getValue())+'&status='+etapa,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaRegistroEntrada(fila)
{
	var gridDesglocePedido=crearGridDesglocePedido();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span style="color: #000"><b>Fecha de recepci&oacute;n:</b>'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                        	xtype:'datefield',
                                                            disabled:true,
                                                            value:'<?php echo date("Y-m-d")?>',
                                                            id:'dteFechaRecepcion'
                                                        },
														
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'<span style="color: #000"><b>Folio de pedido:</b></span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:40,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('folioPedido')+'</b></span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:40,
                                                            html:'<span style="color: #000"><b>No. Entregable:</b></span>'
                                                        },
                                                        {
                                                        	x:330,
                                                            y:40,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('numEntrega')+'</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'<span style="color: #000"><b>Proveedor:</b></span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:70,
                                                            html:'<span class="letraRojaSubrayada8"><b>['+fila.get('rfc')+'] '+fila.get('nombreProveedor')+'</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:90,
                                                            xtype:'fieldset',
                                                            height:95,
                                                            title:'Datos de factura',
                                                            layout:'absolute',
                                                            items:	[
                                                            			{
                                                                            x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'<span style="color: #000"><b>N&uacute;mero  de factura  del pedido:</b>'
                                                                        },
                                                                        {
                                                                            x:200,
                                                                            y:5,
                                                                            xtype:'textfield',
                                                                            width:130,
                                                                            id:'txtNumFactura'
                                                                        },
                                                                        {
                                                                            x:360,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'<span style="color: #000"><b>Fecha de la factura:</b>'
                                                                        },
                                                                        {
                                                                            x:490,
                                                                            y:5,
                                                                            xtype:'datefield',
                                                                            id:'dteFechaFactura'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'<span style="color: #000"><b>Tipo de  factura:</b>'
                                                                        },
                                                                        {
                                                                            x:120,
                                                                            y:35,
                                                                            xtype:'radio',
                                                                          	boxLabel:'Electr&oacute;nica'  ,
                                                                            id:'txtFactElec',
                                                                            name:'tipoFactura',
                                                                            checked:true
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:35,
                                                                            xtype:'radio',
                                                                          	boxLabel:'F&iacute;sica'  ,
                                                                            id:'txtFactFisica',
                                                                            name:'tipoFactura'
                                                                        }
                                                            		]
                                                        }
                                                        ,
                                                        gridDesglocePedido
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Datos de ingreso de pedido',
										width: 820,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNumFactura').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                var txtNumFactura=gEx('txtNumFactura');
                                                                                if(txtNumFactura.getValue()=='')
                                                                                {
                                                                                    function resp()
                                                                                    {
                                                                                        txtNumFactura.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar el n&uacute;mero de factura del pedido',resp)
                                                                                    return;
                                                                                }
                                                                                
                                                                                var dteFechaFactura=gEx('dteFechaFactura');
                                                                                 if(dteFechaFactura.getValue()=='')
                                                                                {
                                                                                    function resp3()
                                                                                    {
                                                                                        dteFechaFactura.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar la fecha de la factura',resp3)
                                                                                    return;
                                                                                }
                                                                                
                                                                                var dteFechaRecepcion=gEx('dteFechaRecepcion');
                                                                                if(dteFechaRecepcion.getValue()=='')
                                                                                {
                                                                                    function resp2()
                                                                                    {
                                                                                        dteFechaRecepcion.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar la fecha de recepci&oacute;n del pedido',resp2)
                                                                                    return;
                                                                                }
                                                                                var tFactura=1; //Electronica
                                                                                if(gEx('txtFactFisica').getValue())
                                                                                	tFactura=2;
                                                                                var obj='{"tipoFactura":"'+tFactura+'","fechaFactura":"'+dteFechaFactura.getValue().format("Y-m-d")+'","fechaRecepcion":"'+dteFechaRecepcion.getValue().format('Y-m-d')+'","numFactura":"'+cv(txtNumFactura.getValue())+'"}';
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().remove(fila);
                                                                                        gEx('gridDetallePedido').getStore().removeAll();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=141&cadObj='+obj+'&idPedido='+fila.get('idPedido'),true);
                                                                        	}
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar el ingreso de este pedido? ',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
	
}

function crearGridDesglocePedido()
{
	
  	almacen= gEx('gridDetallePedido').getStore();
    var x;
    
   
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
                                                        {name: 'cveProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'},
                                                        {name: 'subtotal'},
                                                        {name: 'total'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
     for(x=0;x<almacen.getCount();x++)
    {
    	dsTablaRegistros2.add(almacen.getAt(x).copy());
    }

    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var summary = new Ext.ux.grid.GridSummary();
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                        {
															header:'Cve. Producto',
															width:95,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cveProducto',
                                                            hideable:true
														},
													 	{
															header:'Producto',
															width:180,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														
														{
															header:'Cantidad',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:95,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Subtotal',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'subtotal',
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:100,
                                                            align :'right',
															sortable:true,
                                                            summaryType:'sum',
                                                            dataIndex:'total',
                                                            renderer:'usMoney',
                                                           
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedidoFactura',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            x:5,
                                                            y:200,
                                                            width:790,
                                                            height:270,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            plugins:[summary],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}