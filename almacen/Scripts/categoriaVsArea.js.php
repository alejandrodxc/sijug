<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idCategoria=$_GET["idCategoria"];
	
	$consulta="SELECT clave,nombreObjetoGasto FROM 507_objetosGasto o, 9351_categoriaVSConcepto c  WHERE clave=claveConcepto AND idCategoria=".$idCategoria;
	$arrUsuarios=$con->obtenerFilasArreglo($consulta);	
		
?>
Ext.onReady(inicializar);

function inicializar()
{
	crearGrid();
}

function crearGrid()
{
	var dsDatos=<?php echo $arrUsuarios?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
																{name: 'clave'},
                                                                {name: 'nombreObjetoGasto'}
                                                            ]
                                                }
												);

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Clave',
															width:100,
															sortable:true,
															dataIndex:'clave'
														},
														{
															header:'Nombre Objeto de Gasto',
															width:470,
															sortable:true,
															dataIndex:'nombreObjetoGasto'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridP',
                                                            store:alDatos,
                                                            frame:true,
                                                            renderTo:'grid',
                                                            cm: cModelo,
                                                            height:300,
                                                            width:650,
                                                            sm:chkRow,
                                                            tbar:
                                                            	 [
                                                                 	{
                                                                        text:'Agregar Concepto',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                agregar();
                                                                                
                                                                            }
                                                                    }
                                                                    ,
                                                                    {
                                                                        text:'Quitar Concepto',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                var almacen=tblGrid.getSelectionModel();
                                                                                var fila=almacen.getSelected();
                                                                                if(fila==null)
                                                                                {
                                                                                    Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un elemento');
                                                                                    return;
                                                                                }
                                                                                tblGrid.getStore().remove(fila);
                                                                            }
                                                                    }
                                                                 ]
														}
													);
	return 	tblGrid;
}

function muestraDatos(){
	var grid=Ext.getCmp('gridP');
	var fila=grid.getSelectionModel().getSelected();
	
	if (fila == null)
    {
		alert('Debe de seleccionar al menos un elemento');
		return;
    }
    else
    {
		clave = fila.get('clave');
		gE('claveHidden').value=clave;
	}
}

function validarFrmE(formulario)
{
	var nombre=gE('_nombreCategoria').value;
    if(nombre=='')
    {
    	Ext.MessageBox.alert(lblAplicacion,'El Campo <b>Nombre</b> es obligatorio');
        return;
    }
    else
    {
        var grid=Ext.getCmp('gridP');
        var fila=grid.getStore().getCount();
        var cadena='';
        var x;
        if (fila == 0)
        {
            Ext.MessageBox.alert(lblAplicacion,'Debe agregar al menos un concepto');
            return;
        }
        else
        {
            var almacen=grid.getStore();
            for(x=0;x<fila;x++)
            {
            	var clave = almacen.getAt(x).get('clave');
                if(cadena=='')
                	cadena=clave;
                else    
            		cadena+=','+clave;
            }
            
            var hidden=gE('claveHidden');
            hidden.value=cadena;
        }
        
        gE(formulario).submit();
    }
}

var registroConcepto=Ext.data.Record.create	(
                                                [
                                                    {name: 'clave'},
                                                    {name: 'nombreObjetoGasto'}
                                                ]
                                            )

function agregar()
{
	var grid=crearGridConceptos();
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaHistorial',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                         grid
													]
										}
									);
    var ventana = new Ext.Window(
									{
										title: 'Conceptos',
										width: 700,
										height:550,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaCategoria',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
                                                                                    var filas=Ext.getCmp('gridCatVSConcepto').getSelectionModel().getSelections();
                                                                                    var tamano=filas.length;
                                                                                    if(tamano==0)
                                                                                    {
                                                                                     	msgBox('Debe seleccionar al menos un concepto');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var x;
                                                                                    var almacen=Ext.getCmp('gridP').getStore();
                                                                                    for(x=0;x<tamano;x++)
                                                                                    {
                                                                                    	var cve=filas[x].get('clave');
                                                                                        var nom=filas[x].get('nombreObjetoGasto');
                                                                                        
                                                                                        var pos=obtenerPosFila(almacen,'clave',cve);
                                                                                        if(pos==-1)
                                                                                        {
                                                                                            var nRegistro=new registroConcepto	(
                                                                                                                                    {
                                                                                                                                         clave:cve,
                                                                                                                                         nombreObjetoGasto:nom,
                                                                                                                                    }
                                                                                                                                )
                                                                                            almacen.add(nRegistro);
                                                                                        }    
                                                                                    }
                                                                                    
                                                                                    ventana.close();
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
    ventana.show();                            
}

function crearGridConceptos(id)
{
    var dsConceptos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad: true,
                                                        fields: [
																  {name: 'clave'},
                                                                  {name: 'nombreObjetoGasto'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	                                                    
	dsConceptos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=21;
                                        proxy.baseParams.id=id;
                                    }
                        );
    var chkRow=new Ext.grid.CheckboxSelectionModel();
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow
                                                        ,
														{
															header:'Clave',
															width:100,
															sortable:true,
															dataIndex:'clave'
														},
														{
															header:'Nombre Objeto de Gasto',
															width:470,
															sortable:true,
															dataIndex:'nombreObjetoGasto'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCatVSConcepto',
                                                            x:0,
                                                            y:0,
                                                            store:dsConceptos,
                                                            //frame:true,
                                                            cm: cModelo,
                                                            height:480,
                                                            width:650,
                                                            sm:chkRow
														}
													);
   return tblGrid;
}
