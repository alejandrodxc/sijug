<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT codigoControl,nombreArea FROM 9309_ubicacionesFisicas ORDER BY  nombreArea";
	$arrUbicaciones=$con->obtenerFilasArreglo($consulta);	
	
	
	
?>

Ext.onReady(inicializar);
var arrUbicaciones=<?php echo $arrUbicaciones?>;
function inicializar()
{
	
    var gridDetalle=crearGridDetalle();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			
                                                        gridDetalle
                                            		]
                                        }
                                     ]
						}
                    )   

}




function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedidoDetalle'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'noFactura'},
                                                        {name: 'iva'},
                                                        {name: 'noFactura'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='113';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Num factura',
															width:80,
															sortable:true,
															dataIndex:'noFactura',
                                                            hideable:true
                                                            
														},
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*1;
                                                                        var total=subTotal+parseFloat(registro.get('iva'));
                                                                    	return Ext.util.Format.usMoney(total)
                                                                    },
                                                           
                                                            hideable:true
														}
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'center',
                                                            height:230,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                           tbar:[
                                                           			{
                                                                        	xtype:'label',
                                                                        	html:'<span style="font-size:13px; font-weight:bold; color:#900;">Productos en espera de ser inventariadas</span>&nbsp;&nbsp;'
                                                                        },'-',
                                                           			{
                                                                        	icon:'../images/page_white_magnify.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Inventariar este bien',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desee inventariar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaInventariarProducto(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        }],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}


function mostrarVentanaInventariarProducto(fila)
{
	var cmbUbicacion=crearComboExt('cmbUbicacion',arrUbicaciones,135,95,350);
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. inventario: '
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'txtNoInventario',
                                                            width:120
                                                        },
                                                        {
                                                        	x:10,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Asignar a: '
                                                        },
                                                       cmbComboBusqueda,
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Ubicaci&oacute;n f&iacute;sica:'
                                                        },
                                                        cmbUbicacion,
														{
                                                        	x:10,
                                                            y:140,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:165,
                                                        	xtype:'textarea',
                                                            width:460,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar entrega de producto',
										width: 560,
										height:360,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNoInventario').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('txtNoInventario');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de inventario del bien',resp1);
                                                                        }
                                                                    	
                                                                        
                                                                        /*if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbComboBusqueda.focus();
                                                                            }
                                                                            msgBox('Debe indicar la persona a la cual se le asigna el bien',resp3);
                                                                            return;
                                                                        }*/
                                                                
                                                                		if(cmbUbicacion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbUbicacion.focus();
                                                                            }
                                                                            msgBox('Debe indicar la ubicaci&oacute;n f&iacute;sica del bien',resp2);
                                                                            return;
                                                                        }
                                                                
                                                                		var obj='{"idPedidoDetalle":"'+fila.get('idPedidoDetalle')+'","noInventario":"'+gEx('txtNoInventario').getValue()+'","responsable":"'+cmbComboBusqueda.getValue()+'","ubicacion":"'+cmbUbicacion.getValue()+
                                                                        		'","comentarios":"'+cv(gEx('txtMotivo').getValue())+'","idInventario":"'+fila.get('idInventario')+'"}';
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridDetallePedido').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=114&cadObj='+obj,true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesPlaneacion.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'departamento'}
											]
										);
	var parametros=	{
						funcion:'19',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;
        dSet.baseParams.ambitoAsignacion=0
	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br><span class="letraRojaSubrayada8">Departamento:</span> {departamento}',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:135,
                                                        y:65
													}
												 );
	
   	return comboNombre;
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}