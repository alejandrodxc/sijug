Ext.onReady(inicializar);

function inicializar()
{
	var iAlmacen=gE('idAlmacen').value;
	var contenido=new Ext.Panel	(
                                        {
                                            renderTo:'tblView',
                                            height:870,
                                            border:false,
                                            items:	[
                                            			{
                                                        	id:'frameContenido',
                                                        	xtype:'iframepanel',
                                                            height:860,
                                                            border:false,
                                                            autoLoad:	{	
                                                                            url:'../almacen/almacenes.php',
                                                                            scripts:true,
                                                                            params:	{
	                                                                            		cPagina:'sFrm=true',
                                                                                        idAlmacen:iAlmacen
                                                                                    }
                                                                        },
                                                        	 loadMask:	{
                                                                            msg:'Cargando'
                                                                        }
                                                        }	
                                            		]		
                                        }
                                    )
}