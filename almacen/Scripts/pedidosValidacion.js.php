<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    gridPedidos();
}

function gridPedidos()
{
    var dsPedidos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad: true,
                                                        fields: [
                                                                  {name: 'idPedido'},
                                                                  {name: 'folioPedido'},
                                                                  {name: 'txtRazonSocial2'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'idProveedor_ult'},
                                                                  {name: 'fechaRecibido'},
                                                                  {name: 'idAlmacen'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	                                                    
	dsPedidos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=11;
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'txtRazonSocial2' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
                                       
   
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Folio',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            align:'left'
														}
                                                        ,
                                                        {
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'txtRazonSocial2'
														},
                                                        {
															header:'Fecha de recepci&oacute;n',
															width:150,
															sortable:true,
                                                            align:'left',
                                                            dataIndex:'fechaRecepcion',
                                                            renderer:function(val,meta,registro)
                                                            				  {
                                                                                return Ext.util.Format.date(val,'d/m/Y')
                                                                              }
														},
                                                        {
															header:'Acciones',
															width:80,
															sortable:true,
                                                            align:'center',
                                                            renderer:function(val,meta,registro)
                                                            		 {
                                                                        return'<a href="javascript:verDetalleRecibido('+registro.get('idPedido')+','+registro.get('folioPedido')+',\''+registro.get('txtRazonSocial2')+'\',\''+registro.get('fechaRecibido')+'\','+registro.get('idProveedor_ult')+','+registro.get('idAlmacen')+')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver Detalle" title="Ver Detalle" /></a>';
                                                                     }
														}
													]
												);
	var tblGridProd=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gPedidosV',
                                                            title:'Pedidos en validaci&oacute;n',
                                                            store:dsPedidos,
                                                            renderTo:'pedidosValidacion',
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:700,
                                                            width:800,
                                                            plugins: [filters]
                                                        }
                                                    );
}

function verDetalleRecibido(idPedido,folio,nombreP,fechaRecibido,idProveedor,idAlmacen)
{
    fechaRecibido=Ext.util.Format.date(fechaRecibido,'d/m/Y');
    var gPrecib=gPedidoRecibido(idPedido,folio,nombreP,idProveedor,idAlmacen);
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaHistorial',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:105,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">No Folio: </span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:170,
                                                            y:10,
                                                            html:'<span class="letraExt">'+folio+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:90,
                                                            y:30,
                                                            html:'<span class="letraRojaSubrayada8">Proveedor:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:170,
                                                            y:30,
                                                            html:'<span class="letraExt">'+nombreP+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:40,
                                                            y:50,
                                                            html:'<span class="letraRojaSubrayada8">Fecha de Reporte:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:170,
                                                            y:50,
                                                            html:'<span class="letraExt">'+fechaRecibido+'</span>'
                                                        },
                                                        {
                                                        	xtype:'panel',
                                                            x:10,
                                                            y:85,
                                                            items:[
                                                                        gPrecib
                                                                   ]
                                                         }
													]
										}
									);
    var ventana = new Ext.Window(
									{
										title: 'Detalle de Pedido',
										width: 840,
										height:580,
										minWidth: 300,
                                        //closable:false,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaRecicbido',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
        //ventana.on('beforeClose',function()
//        				 {
//                         	Ext.MessageBox.alert('');
//                         }	
//        				);
        ventana.show();
}


function gPedidoRecibido(idPedido,folio,nombreP,idProveedor,idAlmacen)
{
    var dsPedidoR=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad: true,
                                                        fields: [
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'clave_Art'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'cantidad'},
                                                                  {name: 'estado'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	                                                    
	dsPedidoR.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=6;
                                        proxy.baseParams.idPedido=idPedido;
                                    }
                        );
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr></table>'
                                                                        
                                                					  )
                                            });                                           
   
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
                                                        chkRow,
														{
															header:'Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														}
                                                        ,
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'Situaci&oacute;n',
															width:250,
															sortable:true,
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	var estado=registro.get('estado');
                                                                            if(estado=='')
                                                                            {
                                                                            	estado=0;
                                                                            }
                                                                            var leyenda='';
                                                                            switch(estado)
                                                                            {
                                                                            	case '0':
                                                                                	leyenda='<img height="13" width="13" src="../images/exclamation.png" alt="Sin Revisar" title="Sin Revisar" />&nbsp;&nbsp;Sin Revisar';
                                                                                break;
                                                                                case '1':
                                                                                	leyenda='<img height="13" width="13" src="../images/001_06.gif" alt="Aceptado" title="Aceptado" />&nbsp;&nbsp;Aceptado';
                                                                                break;
                                                                                case '2':
                                                                                    leyenda='<a href="javascript:verObservacionRecibido('+idPedido+','+registro.get('idProducto')+')"><img height="13" width="13" src="../images/001_05.gif" alt="Ver Detalle" title="Ver Detalle" />&nbsp;&nbsp;Ver Detalle</a>';
                                                                                break;
                                                                            
                                                                            }
                                                                            return leyenda;
                                                                    }
														}
													]
												);
	var tblPedRec=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'articulos',
                                                            x:0,
                                                            y:0,
                                                            title:'Articulos Recibidos',
                                                            store:dsPedidoR,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:700,
                                                            width:800,
                                                            plugins: [filters,expander],
                                                            tbar:[
                                                            	  {
                                                                      text:'&nbsp;Rechazar Pedido&nbsp;',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              rechazarPedidos(idPedido);
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'&nbsp;Generar nuevo pedido',
                                                                      icon:'../images/mas.gif',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              dividirPedido(idPedido,folio,nombreP,idProveedor,idAlmacen);
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'&nbsp;Sanci&oacute;n',
                                                                      icon:'../images/subscriptions.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              sancion(idPedido);
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'&nbsp;Finalizar',
                                                                      icon:'../images/salir.gif',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              finalizar(idPedido);
                                                                          }
                                                                  }
                                                                 ]
                                                        }
                                                    );
    return tblPedRec;   
}

function verObservacionRecibido(idPedido,idProducto)
{
   var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[{
													 x:15,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Observaciones:</b>'
													 },
                                                     {
                                                     x:15,
													 y:30,
                                                     height:100,
													 width:350,
                                                     xtype:'textarea',
													 id:'observaciones'
                                                     }
											
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
										width: 400,
										height:200,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
		
        llenarObservacionesProductoRecibido(ventana,idPedido,idProducto);
}

function llenarObservacionesProductoRecibido(ventana,idPedido,idProducto)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]=2)
        {
             Ext.getCmp('observaciones').setValue(resp[1]);
             ventana.show();
        }
        else
        {
              if(resp[0]==1)
              {
              	  ventana.show();
              }
              else
              {	
                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
              } 
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=9&idPedido='+idPedido+'&idProducto='+idProducto,true);
}

function rechazarPedidos(idPedido)
{
	 function resp(btn)
     {
        if(btn=='yes')
        {
            ventanaMotivoC(idPedido);
        }
     }   
     msgConfirm('Si cancela el pedido no podra  realizar cambios sobre este. <br /><br />Esta seguro de realizar esta Acci&oacute;n',resp); 
}

function ventanaMotivoC(idPedido)
{
    var form1 = new Ext.form.FormPanel(	
										{
                                        	id:'panel1',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">Indique el motivo de cancelaci&oacute;n: </span>'
                                                        },
                                                        {
                                                            x:10,
                                                            y:40,
                                                            height:100,
													 		width:350,
                                                            id:'motivoC',
                                                            xtype:'textarea',
                                                        }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: '',
										width: 400,
										height:250,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form1,
                                        id:'ventanaMotivo',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var motivoC=Ext.getCmp('motivoC').getValue();
                                                                                    if(motivoC=='')
                                                                                    {
                                                                                     	msgBox('Debe escribir motivo de cancelaci&oacute;n');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var fecha=gE('fechaActual').value;
                                                                                    function funcAjax1()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var almacen=Ext.getCmp('gPedidosV').getStore();
    		  																				 almacen.reload(); 
                                                                                             ventana.close();
                                                                                             var ventana1=Ext.getCmp('ventanaRecicbido');
                                                                                             ventana1.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax1, 'POST','funcion=12&idPedido='+idPedido+'&fecha='+fecha+'&motivo='+motivoC,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

function dividirPedido(idPedido,folio,nombreP,idProveedor,idAlmacen)
{
    function funcAjax1()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
            var nuevoFolio=resp[1];
            
            var mS=Ext.getCmp('articulos').getSelectionModel();
            var arreglo=mS.getSelections();
            var tamano=arreglo.length;
            if(tamano==0)
            {
                Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un producto');
                return;
            }
            
            var x;
            var cadena='';
            var arregloG=new Array();
            for(x=0;x<tamano;x++)
            {
                var idProducto=arreglo[x].get('idProducto');
                var cantidad=arreglo[x].get('cantidad');
                var nProducto=arreglo[x].get('nombreProducto');
                
                var obj=[idProducto,nProducto,cantidad];
                
                arregloG.push(obj);
                
                if(cadena=='')
                	cadena=idProducto;
                else
                	cadena+=','+idProducto;    
            
            }
            
            var grid=gridNuevoPedido(arregloG);
            var form = new Ext.form.FormPanel(	
                                                {
                                                    id:'ventanaHistorial',
                                                    baseCls: 'x-plain',
                                                    layout:'absolute',
                                                    defaultType: 'textfield',
                                                    items: 	[ 
                                                                {
                                                                    xtype:'label',
                                                                    x:105,
                                                                    y:10,
                                                                    html:'<span class="letraRojaSubrayada8">No Folio: </span>'
                                                                },
                                                                {
                                                                    xtype:'label',
                                                                    x:170,
                                                                    y:10,
                                                                    html:'<span class="letraExt">'+nuevoFolio+'</span>'
                                                                },
                                                                {
                                                                    xtype:'label',
                                                                    x:90,
                                                                    y:30,
                                                                    html:'<span class="letraRojaSubrayada8">Proveedor:</span> '
                                                                },
                                                                {
                                                                    xtype:'label',
                                                                    x:170,
                                                                    y:30,
                                                                    html:'<span class="letraExt">nombreP</span>'
                                                                },
                                                                {
                                                                    xtype:'label',
                                                                    x:40,
                                                                    y:60,
                                                                    html:'<span class="letraRojaSubrayada8">Fecha de Entrega:</span> '
                                                                },
                                                                {
                                                                    x:170,
                                                                    y:55,
                                                                    id:'fechaEntrega',
                                                                    xtype:'datefield',
                                                                    format:'d/m/Y'
                                                                },
                                                                grid
                                                            ]
                                                }
                                            );
            var ventana = new Ext.Window(
                                            {
                                                title: 'Generar pedido',
                                                width: 600,
                                                height:380,
                                                minWidth: 300,
                                                minHeight: 100,
                                                layout: 'fit',
                                                plain:true,
                                                modal:true,
                                                bodyStyle:'padding:5px;',
                                                buttonAlign:'center',
                                                items: form,
                                                id:'ventanaNuevoP',
                                                listeners : {
                                                            show : {
                                                                        buffer : 10,
                                                                        fn : function() 
                                                                        {
                                                                            
                                                                        }
                                                                    }
                                                        },
                                               buttons:	[
                                                                {
                                                                    id:'btnAceptar',
                                                                    text: 'Aceptar',
                                                                    listeners:	{
                                                                                    click:function()
                                                                                        {
                                                                                            var fecha=Ext.getCmp('fechaEntrega').getValue().format('d/m/Y');
                                                                                            if(fecha=='')
                                                                                            {
                                                                                                msgBox('Debe indicar la fecha de entrega');
                                                                                                return;
                                                                                            }
                                                                                            
                                                                                            function funcAjax1()
                                                                                            {
                                                                                                var resp=peticion_http.responseText.split('|');
                                                                                                if(resp[0]==1)
                                                                                                {
                                                                                                     var gridDetalle=Ext.getCmp('articulos');
                                                                                                     var sM=gridDetalle.getSelectionModel();
                                                                                                     var arreglo=sM.getSelections();
                                                                                                     var almacenD=gridDetalle.getStore();
                                                                                                     var tamano=almacenD.remove(arreglo);
                                                                                                    
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax1, 'POST','funcion=16&idPedido='+idPedido+'&idProveedor='+idProveedor+'&nuevoFolio='+nuevoFolio+'&fecha='+fecha+'&idAlmacen='+idAlmacen+'&cadena='+cadena,true)
                                                                                        }
                                                                                }
                                                                },
                                                                {
                                                                    text: 'Cancelar',
                                                                    handler:function()
                                                                            {
                                                                                ventana.close();
                                                                            }
                                                                }
                                                            ]                 
                                            }
                                        );
                ventana.show();
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax1, 'POST','funcion=15',true)        
}


function gridNuevoPedido(arreglo)
{
	var arrDatos=arreglo;
    
    var dSetNuevosP= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idProducto'},
                                                                    {name:'nombreProducto'},
                                                                    {name:'cantidad'}
                                                                ]
                                                    }
                                                 )
    
	dSetNuevosP.loadData(arrDatos);	
	//var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
	var cmP= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            //columnaCheck,
                                                            {
                                                                header:'Producto',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto'
                                                            }
                                                            ,
                                                            {
                                                                header:'cantidad',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'cantidad'
                                                            }   
                                                        ]
                                                    );
											
												
	var gridNP=	new Ext.grid.GridPanel	(
                                                    {
                                                    	x:0,
                                                        y:90,
														id:'nProd',
                                                        store:dSetNuevosP,
                                                        frame:true,
                                                        cm: cmP,
                                                        //sm:columnaCheck,
                                                        height:400,
                                                        width:570
													}
					
    											);
	return gridNP;
}


function finalizar(idPedido)
{
    var grid=Ext.getCmp('articulos');
    var almacen=grid.getStore();
    var tamano=almacen.getCount();
    
    if(tamano==0)
    {
    	rechazarPedidos(idPedido);
    }
    else
    {
        function respS(btnS)
        {
            if(btnS=='yes')
            {
                //ventanaMotivoC(idPedido);
                function funcAjax1()
                {
                    var resp=peticion_http.responseText.split('|');
                    if(resp[0]==1)
                    {
                         
                    }
                    else
                    {
                          Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax1, 'POST','funcion=15',true)
            }
        }   
        msgConfirm('Al finalizar se cancelara el pedido original y los productos restantes generaran un nuevo pedido. <br /><br />Esta seguro de realizar esta Acci&oacute;n',respS); 
    }    
}


function sancion(idPedido)
{
	
    var arrTipoSancion=[[0,'Nota de credito'],[1,'sancion 2']];
    var comboSancion=crearComboExt('comboSancion',arrTipoSancion,130,5);
    comboSancion.on('select',funcChange);
    var panelS= new Ext.form.FormPanel(	
										{
                                        	id:'panelSancion',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">Tipo de Sanci&oacute;n:</span>'
                                                        }
                                                        ,
                                                        comboSancion
                                                        ,
                                                        {
                                                            xtype:'label',
                                                            x:75,
                                                            y:35,
                                                            id:'txtNota',
                                                            hidden:true,
                                                            html:'<span class="letraRojaSubrayada8">Monto:</span>'
                                                        }
                                                        ,
                                                        {
                                                            xtype:'numberfield',
                                                            x:130,
                                                            y:35,
                                                            id:'montoNota',
                                                            hidden:true
                                                        }
													]
										}
									);
    var ventana = new Ext.Window(
									{
										title: 'Tipo de Sanci&oacute;n',
										width: 540,
										height:380,
										minWidth: 300,
                                        //closable:false,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: panelS,
                                        id:'ventanaSancion',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
                                
	ventana.show();
}

function funcChange(combo,registro,indice)
{
  
  if(indice==0)
  {
  	 Ext.getCmp('txtNota').show();
     Ext.getCmp('montoNota').show();
  }
  //var tipoRecurso=registro.get('id');
//  var horaI=gE('horaIni').value;
//  var horaF=gE('horaFin').value;
//  var fecha=gE('fechaMysql').value;
//  function funcAjax()
//  {
//      var resp=peticion_http.responseText;
//      arrResp=resp.split('|');
//      if(arrResp[0]=='1')
//      {
//           var cadArregloRecursos=arrResp[1];
//           var arrRecursos=eval(cadArregloRecursos);
//           Ext.getCmp('tblRecursos').getStore().loadData(arrRecursos);
//      }
//      else
//      {
//          msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
//      }
//  }
//  obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax, 'POST','funcion=34&idCategoria='+tipoRecurso+'&fecha='+fecha+'&hI='+horaI+'&hF='+horaF,true);
}