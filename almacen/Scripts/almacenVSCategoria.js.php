<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridC();  
}

function crearGridC()
{
	var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosR=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idAlmacenVSCategoria'},
                                                                  {name: 'nombre'},
                                                                  {name: 'conceptos'},
                                                                  {name: 'descripcion'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosR.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=25;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                    }
                        );
   
   
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table ><tr><td colspan="2" align="left" height="10"></td></tr><tr><td width:"230" align="left"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td></tr><tr><td><span class="copyrigthSinPadding">{descripcion}</span><br /></td></tr><tr><td><span class="copyrigthSinPadding">{conceptos}</span><br /><br /></td></tr></table>'
                                                                        
                                                					  )
                                            });            
  // var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'txtRazonSocial2' 
//                                                                      },
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'folioPedido' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
                                                        chkRow,
														{
															header:'Categor&iacute;a',
															width:400,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left'
														}
													]
												);
	var tblGridR=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCatAlm',
                                                            title:'Categor&iacute;as del almac&eacute;n',
                                                            renderTo:'almacenC',
                                                            store:dsRegistrosR,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            height:550,
                                                            width:550,
                                                            plugins:[expander],
                                                            tbar:
                                                            	 [
                                                                 	{
                                                                        text:'Agregar Categor&iacute;a',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                agregarC();
                                                                                
                                                                            }
                                                                    }
                                                                    ,
                                                                    {
                                                                        text:'Quitar Categor&iacute;a',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                            {
                                                                                var almacen=tblGridR.getSelectionModel();
                                                                                var fila=almacen.getSelected();
                                                                                if(fila==null)
                                                                                {
                                                                                    Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un elemento');
                                                                                    return;
                                                                                }
                                                                                var idTabla=fila.get('idAlmacenVSCategoria');
                                                                                
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText.split('|');
                                                                                    if(resp[0]==1)
                                                                                    {
                                                                                         tblGridR.getStore().remove(fila);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                          Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=80&idTabla='+idTabla,true)
                                                                            }
                                                                    }
                                                                 ]
                                                           
                                                        }
                                                    );
		
    dsRegistrosR.load()  ;
    return tblGridR;     
}

function agregarC()
{
    var idAlmacen=gE('idAlmacen').value;
    var gC=gridCategorias(idAlmacen);
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaCat',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">Seleccione las categorias: </span>'
                                                        },
                                                        {
                                                        	xtype:'panel',
                                                            x:10,
                                                            y:35,
                                                            items:[
                                                                        gC
                                                                   ]
                                                         }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Categor&iacute;as',
										width: 550,
										height:400,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaD',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var filas=Ext.getCmp('griCatNoAlm').getSelectionModel().getSelections();
                                                                                    var tamano=filas.length;
                                                                                    var cadena='';
                                                                                    var x;
                                                                                    if(tamano==0)
                                                                                    {
                                                                                     	msgBox('Debe seleccionar al menos un elemento');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    for(x=0;x<tamano;x++)
                                                                                    {
                                                                                    	var idCategoria=filas[x].get('idCategoria');
                                                                                        
                                                                                        if(cadena=='')
                                                                                        	cadena=idCategoria;
                                                                                        else
                                                                                        	cadena+=','+idCategoria;
                                                                                    }
                                                                                    
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var almacen=Ext.getCmp('gridCatAlm').getStore().reload();
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=27&idAlmacen='+idAlmacen+'&cadena='+cadena,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		var observaciones=Ext.getCmp('observaciones').getValue();
                                                                        if(observaciones=='')
                                                                        {
                                                                            var radioAceptado=gE('aceptado_'+idProducto);
                                                                            var radioDetalle=gE('detalle_'+idProducto);
                                                                            
                                                                            radioDetalle.checked=false;
                                                                            radioAceptado.checked=true;
                                                                        
                                                                        }
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();

}

function gridCategorias()
{
	var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosR=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idCategoria'},
                                                                  {name: 'nombre'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosR.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=26;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                    }
                        );
   
    
  // var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'txtRazonSocial2' 
//                                                                      },
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'folioPedido' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
    
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Categor&iacute;a',
															width:400,
															sortable:true,
															dataIndex:'nombre',
                                                            align:'left'
														}
													]
												);
	var tblGridR=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'griCatNoAlm',
                                                            //title:'Categor&iacute;as del almac&eacute;n',
                                                            renderTo:'almacenC',
                                                            store:dsRegistrosR,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:550,
                                                            width:550,
                                                            // ,
//                                                            plugins:[filters]
                                                            //tbar:
//                                                            	 [
//                                                                 	{
//                                                                        text:'Agregar Categor&iacute;a',
//                                                                        icon:'../images/add.png',
//                                                                        cls:'x-btn-text-icon',
//                                                                        handler:function()
//                                                                            {
//                                                                                agregarC();
//                                                                                
//                                                                            }
//                                                                    }
//                                                                 ]
                                                        }
                                                    );
		
    dsRegistrosR.load()  ;
    return tblGridR;     
}