<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT ciclo,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoPresupuesto,tituloTipoP FROM 508_tiposPresupuesto";
	$arrTiposPresupuesto=$con->obtenerFilasArreglo($consulta);
	$consulta="select codigoControl,concat('[',clave,'] ',nombreObjetoGasto) from 507_objetosGasto where nivel=1 order by clave";
	$arrCapitulos=$con->obtenerFilasArreglo($consulta);
	$consulta="select ciclo FROM 550_cicloFiscal where status=1";
	$cicloVigente=$con->obtenerValor($consulta);
	$consulta="SELECT idSituacionCompra,situacionCompra FROM 1000_situacionesCompra";
	$arrSituacionesCompra=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idSituacionPedido,situacion FROM 9100_situacionPedido";
	$arrSituacionesPedido=$con->obtenerFilasArreglo($consulta);
?>

var arrSituacionesPedido=<?php echo $arrSituacionesPedido?>;
Ext.onReady(inicializar);



function inicializar()
{

	var gridProveedor=crearGridProveedor();
    var gridProductosDetalle=crearGridProductosDetalle();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            tbar:[{
                                                                        	xtype:'label',
                                                                        	html:'<span style="font-size:13px; font-weight:bold; color:#900;">Proveedores inactivos</span>'
                                                                        }],
                                            items:	[
                                            			gridProveedor,
                                                        gridProductosDetalle
                                            			
                                            		]
                                        }
                                     ]
						}
					)                        
}

function crearGridProveedor()
{
	var tamPagina=100;  
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProveedor'},
                                                        {name: 'txtRFC'},
                                                        {name:'txtRazonSocial2'},
                                                        {name:'txtTelefono1'},
                                                        {name: 'txtTelefono2'},
                                                        {name:'txtFax'},
                                                        {name:'txtCorreo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'txtRFC'},
                                                        				{type: 'string', dataIndex: 'txtRazonSocial2'}
                                                                    ]
                                                    }
                                                ); 
	                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'txtRazonSocial2', direction: 'ASC'},
                                                            groupField: 'txtRazonSocial2',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=95;
                                        proxy.baseParams.tipoProveedor='0';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                       
                                        
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel();   
	            
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  	         
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer({width:30}),
                                                    	chkRow,
													 	{
															header:'RFC',
															width:150,
															sortable:true,
															dataIndex:'txtRFC',
                                                            hideable:true
														},
														{
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            hideable:true
														},
                                                        {
															header:'Tel&eacute;fono 1',
															width:140,
															sortable:true,
															dataIndex:'txtTelefono1',
                                                            hideable:true
														},
														 {
															header:'Tel&eacute;fono 2',
															width:140,
															sortable:true,
															dataIndex:'txtTelefono2',
                                                            hideable:true
														},
                                                         {
															header:'Fax',
															width:140,
															sortable:true,
															dataIndex:'txtFax',
                                                            hideable:true
														},
                                                         {
															header:'E-mail',
															width:140,
															sortable:true,
															dataIndex:'txtCorreo',
                                                            hideable:true
														}
                                                        
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProveeeores',
                                                            store:dsTablaRegistros2,
                                                            frame:false,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            bbar:[paginador],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            plugins:[filters],
                                                            tbar:	[
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar proveedor',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrDatos=[['paginaRedireccion','../almacen/proveedoresActivos.php'],['cPagina','sFrm=true'],['idRegistro',-1],['idFormulario',405]];
                                                                                        enviarFormularioDatos('../modeloPerfiles/registroFormulario.php',arrDatos);
                                                                                    }
                                                                            
                                                                        }
                                                                        ,
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar proveedor',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();	
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el proveedor que desea modificar');
                                                                                        	reutrn;
                                                                                        }
                                                                                        var arrDatos=[['paginaRedireccion','../almacen/proveedoresActivos.php'],['cPagina','sFrm=true'],['idRegistro',fila.get('idProveedor')],['idFormulario',405]];
                                                                                        enviarFormularioDatos('../modeloPerfiles/registroFormulario.php',arrDatos);
                                                                                    }
                                                                            
                                                                        }
                                                                        ,
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover proveedor',
                                                                            hidden:false,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();	
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el proveedor que desea remover');
                                                                                        	reutrn;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGrid.getStore().remove(fila);
                                                                                                        gEx('gridPedido').getStore().removeAll();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=97&idProveedor='+fila.get('idProveedor'),true);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer eliminar el proveedor seleccionado?',resp)
                                                                                        	
                                                                                    }
                                                                            
                                                                        }
                                                                    ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridPedido').getStore().load({params:{funcion:96,idAlmacen:gE('idAlmacen').value,idProveedor:registro.get('idProveedor'),situacion:'0'}});
                                                }
    							)  
	dsTablaRegistros2.load({params:{start:0,limit:tamPagina}});                                                                            
	return tblGrid;  
}

function crearGridProductosDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedidoDetalle'},
                                                        {name: 'folioPedido'},
                                                        {name:'nombreProducto'},
                                                        {name: 'fechaRecepcion', type:'date',  dateFormat:'Y-m-d'},
                                                        {name: 'marca'},
                                                        {name: 'modelo'},
                                                        {name: 'situacion'},
                                                        {name: 'cantidad', type:'int'},
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

	
    var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'folioPedido'},
                                                        				{type: 'string', dataIndex: 'nombreProducto'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'},
                                                                        {type: 'string', dataIndex: 'modelo'}
                                                                    ]
                                                    }
                                                ); 
	                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'folioPedido',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
   var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer({width:30}),
													 	{
															header:'Folio del pedido',
															width:120,
															sortable:true,
															dataIndex:'folioPedido',
                                                            hideable:true
														},
														{
															header:'Producto',
															width:400,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
														},
                                                        {
															header:'Fecha prog. entrega',
															width:130,
															sortable:true,
															dataIndex:'fechaRecepcion',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!='')&&(val!=null)&&(val!='0000-00-00'))
                                                                        {
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                                    }
                                                            
														},
                                                        {
															header:'Situaci&oacute;n',
															width:100,
															sortable:true,
															dataIndex:'situacion',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrSituacionesPedido,val);
                                                                    }
                                                            
														},
                                                        {
															header:'Marca',
															width:160,
															sortable:true,
															dataIndex:'marca',
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:160,
															sortable:true,
															dataIndex:'modelo',
                                                            hideable:true
														},
                                                        {
															header:'Cantidad',
															width:160,
															sortable:true,
															dataIndex:'cantidad',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:'Historial de pedidos',
                                                        	collapsible:true,
                                                            id:'gridPedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:250,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            plugins:[filters],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:true,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	                                                                           
	return tblGrid;  
}