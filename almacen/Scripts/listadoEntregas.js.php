<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridEntregas();
}

function crearGridEntregas()
{
    var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idEntrega'},
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreP'},
                                                                  {name: 'codUnidad'},
                                                                  {name: 'nomD'},
                                                                  {name: 'idPrograma'},
                                                                  {name: 'nombrePrg'},
                                                                  {name: 'fecha'},
                                                                  {name: 'horaIni'},
                                                                  {name: 'horaFin'},
                                                                  {name: 'estado'},
                                                                  {name: 'cantidad'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=70;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                    }
                        );
     var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table>'
                                                                             +'<tr>'
                                                                                +'<td width="600" height="10"></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Producto:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{nombreP}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Programa:</b><br /></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{nombrePrg}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                         +'</table>'
                                                                        
                                                					  )
                                            });                                                     
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'nomD',
                                                            align:'left'
														},
                                                        {
															header:'Fecha Entrega',
															width:80,
                                                            align:'center',
															sortable:true,
															dataIndex:'fecha'
														},
                                                        {
															header:'Hora Inicio',
															width:65,
                                                            align:'center',
															sortable:true,
															dataIndex:'horaIni'
														},
                                                        {
															header:'Hora Fin',
															width:60,
                                                            align:'center',
															sortable:true,
															dataIndex:'horaFin'
														},
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidad'
														}
                                                        ,
                                                        {
															header:'Estado',
															width:130,
                                                            align:'left',
															sortable:true,
															dataIndex:'estado',
                                                            renderer:function(val,meta,registro)
                                                            				 {
                                                                                if(val=='0')
                                                                                {
                                                                                	return'Agendado';
                                                                                }
                                                                                else
                                                                                {
                                                                                	return 'En proceso de entrega'
                                                                                }
                                                                             }
														},
                                                        {
															header:'',
															width:20,
															sortable:true,
                                                            align:'center',
                                                            dataIndex:'idEntrega',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    return '<a href="javascript:cambiarEstado('+val+','+registro.get('estado')+','+registro.get('cantidad')+',\''+bE(registro.get('nombreP'))+'\')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver Detalle" title="Ver Detalle" /></a>';
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridEntregas',
                                                            title:'Entregas pendientes',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'entregasL',
                                                            height:750,
                                                            width:800,
                                                            plugins:[expander]
                                                        }
                                                    );
		
    dsRegistrosP.load()  ;
    return tblGridP;     
}


function cambiarEstado(idEntrega,estado,cantidad,nombreP)
{
	var arregloE=[['0','Agendado'],['1','Entregado'],['2','En proceso de entrega']];
    var comboEstado=crearComboExt('comboEstado',arregloE,70,10);
    var form1 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario1',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:15,
                                                            html:'<span><b>Estado:</b></span>'
                                                        },
                                                        comboEstado
													]
										}
									);
    
    var ventana = new Ext.Window(
									{
										title: 'Estado de entrega',
										width: 300,
										height:150,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form1,
                                        id:'ventana1',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var idEstado=Ext.getCmp('comboEstado').getValue();
                                                                                    if(idEstado=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el estado ');
                                                                                        return;
                                                                                    }
																					if(idEstado==estado)
                                                                                    {
                                                                                    	msgBox('Este registro ya se encuentra en el estado se�alado');
                                                                                        return;
                                                                                        //ventana.close();
                                                                                    }
                                                                                    
                                                                                    if(idEstado==1)
                                                                                    {
                                                                                          function funcAjax()
                                                                                          {
                                                                                              var resp=peticion_http.responseText;
                                                                                              arrResp=resp.split('|');
                                                                                              if(arrResp[0]=='1')
                                                                                              {
                                                                                                  if(arrResp[1]==1)
                                                                                                  {
                                                                                                      ventana.close();
                                                                                                      ventanaEntregados(arrResp[2],arrResp[3],cantidad,nombreP,idEntrega);
                                                                                                      //alert('registro completo');
                                                                                                      //gEx('ventanaD').hide();
                                                                                                      //tb_show(lblAplicacion,'../programaAcademico/parametrosCalculo.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idCalculo='+idCalculo+'&TB_iframe=true&height=420&width=650',"","scrolling=yes");
                                                                                                  }
                                                                                                  else
                                                                                                  {
                                                                                                      function funcAjax2()
                                                                                                      {
                                                                                                          var resp=peticion_http.responseText;
                                                                                                          arrResp=resp.split('|');
                                                                                                          if(arrResp[0]=='1')
                                                                                                          {
                                                                                                              var almacen=Ext.getCmp('gridEntregas');
                                                                                                              almacen.getStore().reload();
                                                                                                              ventana.close();
                                                                                                          }
                                                                                                          else
                                                                                                          {
                                                                                                              msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                          }
                                                                                                      }
                                                                                                      obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=72&idEntrega='+idEntrega+'&estado='+idEstado,true);
                                                                                                  }
                                                                                              }
                                                                                              else
                                                                                              {
                                                                                                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                              }
                                                                                          }
                                                                                          obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=71&idEntrega='+idEntrega,true);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	function funcAjax2()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                var almacen=Ext.getCmp('gridEntregas');
                                                                                                almacen.getStore().reload();
                                                                                                ventana.close();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=72&idEntrega='+idEntrega+'&estado='+idEstado,true);
                                                                                    }     
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

function ventanaEntregados(idFormulario,tipoR,cantidad,nombreP,idEntrega)
{
    var x;
    var arreglo=new Array();
    for(x=0;x< cantidad;x++)
    {
    	var obj=[bD(nombreP),'-1','-1','-1','-1','-1','-1','-1'];
        arreglo.push(obj);
    }
    
    var gridE=crearGridProductosE(arreglo);
   
    var form2 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario2',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                       gridE
													]
										}
									);
    
    var ventana = new Ext.Window(
									{
										title: 'Productos entregados',
										width: 800,
										height:450,
										minWidth: 300,
										minHeight: 400,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form2,
                                        id:'ventana2',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var almacen=Ext.getCmp('prodEntregados').getStore();
                                                                                    var tamano=almacen.getCount();
                                                                                    var x;
                                                                                    var cadena='';
                                                                                    //var cadenaValidacion='';
                                                                                    
                                                                                    for(x=0;x< tamano;x++)
                                                                                    {
                                                                                    	var registro=almacen.getAt(x);
                                                                                        if(registro.get('idResponsable')==-1)
                                                                                        {
                                                                                        	msgBox('Debe indicar el responsable para todos los art&iacute;culos');
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                        if(registro.get('codigoInventario')==-1)
                                                                                        {
                                                                                        	msgBox('Debe indicar el No.Inventario para todos los art&iacute;culos');
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                        if(registro.get('nombreU')==-1)
                                                                                        {
                                                                                        	msgBox('Debe indicar el departamento para todos los art&iacute;culos');
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                      //  if(cadenaValidacion=='')
//                                                                                        {
//                                                                                        	cadenaValidacion=registro.get('codigoInventario');
//                                                                                        }
//                                                                                        else
//                                                                                        {
//                                                                                        	var arregloVal=cadenaValidacion.split(',');
//                                                                                            var tamanoVal=arregloVal.length;
//                                                                                            var y;
//                                                                                            
//                                                                                            for(y=0;y< tamanoVal;y++)
//                                                                                            {
//                                                                                            	var eCad=arregloVal[y];
//                                                                                                
//                                                                                                if(eCad==registro.get('codigoInventario'))
//                                                                                                {
//                                                                                                	msgBox('No puede aosciar el mismo numero de inventario a dos art&iacute;culos');
//                                                                                                    return;
//                                                                                                }
//                                                                                            }
//                                                                                            
//                                                                                            cadenaValidacion+=','+registro.get('codigoInventario');
//                                                                                        }
                                                                                        
                                                                                        
                                                                                        if(cadena=='')
                                                                                        	cadena=registro.get('idInventario')+'_'+registro.get('codigoInventario')+'_'+registro.get('idUsuario')+'_'+registro.get('codigoUnidad')+'_'+registro.get('noOrden');
                                                                                        else
                                                                                        	cadena+=','+registro.get('idInventario')+'_'+registro.get('codigoInventario')+'_'+registro.get('idUsuario')+'_'+registro.get('codigoUnidad')+'_'+registro.get('noOrden');    
                                                                                    }
                                                                                    
                                                                                    function funcAjax3()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            alert('guardo entrega');
                                                                                            Ext.getCmp('gridEntregas').getStore().reload();
                                                                                            ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax3, 'POST','funcion=75&idEntrega='+idEntrega+'&cadena='+cadena+'&idAlmacen='+gE('idAlmacen'),true);
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

function crearGridProductosE(arreglo)
{
    var dSetEntregas= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name:'nombreP'},
                                                                    {name:'idInventario'},
                                                                    {name:'codigoInventario'},
                                                                    {name: 'idUsuario'},
                                                                    {name: 'nombreUsuario'},
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'nombreU'},
                                                                    {name: 'noOrden'}
                                                                ]
                                                    }
                                                 )
    
	dSetEntregas.loadData(arreglo);	
	var cmP= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreP',
                                                                renderer:function(val,meta,registro,nPos) 
                                                                				 {
                                                                                 	return'<a href="javascript:guardarRegistro('+registro.get('idInventario')+','+nPos+')"><img height="13" width="13" src="../images/building_edit.png" alt="Registrar Entrega" title="Registrar Entrega" /></a>&nbsp;&nbsp;'+val;
                                                                                 }
                                                            },
                                                            {
                                                                header:'Responsable',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'nombreUsuario',
                                                                renderer:function(val,meta,registro)
                                                                				  {
                                                                                  	  if(val==-1)
                                                                                      	return ''
                                                                                      else
                                                                                      	return val;  
                                                                                  }
                                                            },
                                                            {
                                                                header:'No.Inventario',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'codigoInventario',
                                                                renderer:function(val,meta,registro)
                                                                				  {
                                                                                  	  if(val==-1)
                                                                                      	return ''
                                                                                      else
                                                                                      	return val;  
                                                                                  }
                                                            },
                                                            {
                                                                header:'Departamento',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'nombreU',
                                                                renderer:function(val,meta,registro)
                                                                				  {
                                                                                  	  if(val==-1)
                                                                                      	return ''
                                                                                      else
                                                                                      	return val;  
                                                                                  }
                                                            },
                                                            {
                                                                header:'No orden',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'noOrden',
                                                                renderer:function(val,meta,registro)
                                                                				  {
                                                                                  	  if(val==-1)
                                                                                      	return ''
                                                                                      else
                                                                                      	return val;  
                                                                                  }
                                                            }
                                                        ]
                                                    );
											
	var gProdE=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'prodEntregados',
                                                        store:dSetEntregas,
                                                        frame:true,
                                                        cm: cmP,
                                                        height:345,
                                                        width:770
													}
					
    											);
	return gProdE;
}

function guardarRegistro(idInventario,pos)
{
    gE('cBusquedaP').value='1';
    var parametros2=	{
							funcion:'73',
							criterio:''
						};
	
	var comboPapa=inicializarCmbPadre(parametros2);
    
    var form3 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario3',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:60,
                                                            y:20,
                                                            html:'<span><b>No. Inventario:</b></span>'
                                                        },
                                                        {
                                                            xtype:'textfield',
                                                            x:150,
                                                            y:15,
                                                            id:'noInvR'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:45,
                                                            html:'<span><b>No.Orden de translado:</b></span>'
                                                        },
                                                        {
                                                            xtype:'textfield',
                                                            x:150,
                                                            y:40,
                                                            id:'noOrden'
                                                        },
                                                        new Ext.form.Radio	(
																					{
																						x:15,
																						y:75,
																						id:'rdoPaterno',
																						boxLabel:'Ap. Paterno',
																						checked:true,
																						value:1
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:135,
																						y:75,
																						id:'rdoMaterno',
																						boxLabel:'Ap. Materno',
																						value:2
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:265,
																						y:75,
																						id:'rdoNombre',
																						boxLabel:'Nombre',
																						value:3
																					}
																				),
                                                           {
                                                            xtype:'label',
                                                            x:25,
                                                            y:105,
                                                            html:'<span><b>Responsable:</b></span>'
                                                           },                     
                                                           comboPapa                     
													]
										}
									);
    
    var ventana = new Ext.Window(
									{
										title: 'Registro Producto',
										width: 480,
										height:220,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form3,
                                        id:'ventana3',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var noInv=Ext.getCmp('noInvR').getValue();
                                                                                    if(noInv=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el No. de inventario ');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var idUsr=gE('idUsuario').value;
                                                                                    if(idUsr==-1)
                                                                                    {
                                                                                     	msgBox('Debe indicar el responsable ');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var noOrd=Ext.getCmp('noOrden').getValue();
                                                                                    
                                                                                    var almacenV=Ext.getCmp('prodEntregados').getStore();
                                                                                    var tamanoVal=almacenV.getCount();;
                                                                                    var y;
                                                                                    
                                                                                    for(y=0;y< tamanoVal;y++)
                                                                                    {
                                                                                        var elemento=almacenV.getAt(y).get('codigoInventario');
                                                                                        var elemSel=almacenV.getAt(pos).get('codigoInventario');
                                                                                        if(pos!=y)
                                                                                        {
                                                                                            if(elemento==noInv)
                                                                                            {
                                                                                                msgBox('No puede aosciar el mismo numero de inventario a dos art&iacute;culos');
                                                                                                return;
                                                                                            }
                                                                                        }    
                                                                                    }
                                                                                    
                                                                                    function funcAjax2()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            if(arrResp[1]==0)
                                                                                            {
                                                                                            	msgBox('El numero de inventario ingresado no es valido ');
                                                                                        		return;
                                                                                            }
                                                                                            
                                                                                            if(arrResp[2]==-1)
                                                                                            {
                                                                                            	msgBox('El usuario seleccionado no cuenta con un departamento asociado ');
                                                                                        		return;
                                                                                            }
                                                                                           
                                                                                            if(arrResp[5]==1)
                                                                                            {
                                                                                            	function resp1(btn)
                                                                                                {
                                                                                                    if(btn=='yes')
                                                                                                    {
                                                                                                        var almacen=Ext.getCmp('prodEntregados').getStore();
                                                                                                        var reg=almacen.getAt(pos);
                                                                                                        reg.set('idInventario',arrResp[1]);
                                                                                                        reg.set('codigoInventario',noInv);
                                                                                                        reg.set('idUsuario',idUsr);
                                                                                                        reg.set('nombreUsuario',arrResp[4]);
                                                                                                        reg.set('codigoUnidad',arrResp[2]);
                                                                                                        reg.set('nombreU',arrResp[3]);
                                                                                                        reg.set('noOrden',noOrd);
                                                                                                        ventana.close();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                    	var almacen=Ext.getCmp('prodEntregados').getStore();
                                                                                                        var reg=almacen.getAt(pos);
                                                                                                        reg.set('idInventario',arrResp[1]);
                                                                                                        reg.set('codigoInventario',noInv);
                                                                                                        reg.set('idUsuario',arrResp[7]);
                                                                                                        reg.set('nombreUsuario',arrResp[4]);
                                                                                                        reg.set('codigoUnidad',arrResp[2]);
                                                                                                        reg.set('nombreU',arrResp[3]);
                                                                                                        reg.set('noOrden',noOrd);
                                                                                                        ventana.close();
                                                                                                    }
                                                                                                }   
                                                                                                 msgConfirm('Este articulo ya cuenta con el sig usuario asignado:<b>'+arrResp[6]+'</b> desea remplazarlo con el usuasio seleccionado',resp1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                            	var almacen=Ext.getCmp('prodEntregados').getStore();
                                                                                                var reg=almacen.getAt(pos);
                                                                                                reg.set('idInventario',arrResp[1]);
                                                                                                reg.set('codigoInventario',noInv);
                                                                                                reg.set('idUsuario',idUsr);
                                                                                                reg.set('nombreUsuario',arrResp[4]);
                                                                                                reg.set('codigoUnidad',arrResp[2]);
                                                                                                reg.set('nombreU',arrResp[3]);
                                                                                                reg.set('noOrden',noOrd);
                                                                                                ventana.close();
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=74&noInventario='+noInv+'&idUsuario='+idUsr,true);
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        var rdoPaterno=Ext.getCmp('rdoPaterno');
		var rdoMaterno=Ext.getCmp('rdoMaterno');
		var rdoNombre=Ext.getCmp('rdoNombre');
		rdoPaterno.on('check',cambiarRadioSel);									
		rdoMaterno.on('check',cambiarRadioSel);									
		rdoNombre.on('check',cambiarRadioSel);		
        ventana.show();
}

function inicializarCmbPadre(parametros2)
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesAlmacen.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'idUsuario',mapping:'idUsuario'}
											]
										);

	var ds=new Ext.data.Store	(
								 	{
										proxy:pPagina,
										reader:lector,
										baseParams:parametros2
									}
								 );
	
	function cargarDatos(dSet)
	{
		gE('idUsuario').value='-1';
		var aNombre=Ext.getCmp('cmbNombrePadre').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=gE('cBusquedaP').value;
       
	}
	
	ds.on('beforeload',cargarDatos);

	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br>',
										'</div></tpl>'
									 );
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:110,
														y:100,
														id:'cmbNombrePadre',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:320,
                                                        listWidth :320,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item'
													}
												 );
	
    function funcElemSeleccionado(combo,registro)
	{	
		var idUsuario=registro.get('idUsuario');
		gE('idUsuario').value=idUsuario;
    }
	comboNombre.on('select',funcElemSeleccionado);	
	return comboNombre;
}

function cambiarRadioSel(chk, valor)
{
	if(valor==true)
	{
		var rdoPaterno=Ext.getCmp('rdoPaterno');
		var rdoMaterno=Ext.getCmp('rdoMaterno');
		var rdoNom=Ext.getCmp('rdoNombre');
		if(rdoPaterno.id!=chk.id)
			rdoPaterno.setValue(false);
		if(rdoMaterno.id!=chk.id)
			rdoMaterno.setValue(false);
		if(rdoNom.id!=chk.id)
			rdoNom.setValue(false);
		gE('cBusquedaP').value=chk.value;
	}
}