<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$conCiclo="SELECT ciclo,ciclo FROM 550_cicloFiscal";
	$arregloC=$con->obtenerFilasArreglo($conCiclo);
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearPanel ();
    cargarDatos();
}

var arregloMeses=[['0','Enero'],['1','Febrero'],['2','Marzo'],['3','Abril'],['4','Mayo'],['5','Junio'],['6','Julio'],['7','Agosto'],['8','Septiembre'],['9','Octubre'],['10','Noviembre'],['11','Diciembre']];
function crearPanel()
{
    var arregloC=<?php echo $arregloC ?>;
    var gP=crearGrid();
    //var cmbCiclo=crearComboExt('cmbCiclo',arregloC,120,20);
 	//cmbCiclo.on('select',funcCargarPanel);
    
    var cmbMeses=crearComboExt('cmbMeses',arregloMeses,260,20);
 	cmbMeses.on('select',funcCargarPanel);
    
    var panel = new Ext.Panel(	
										{
                                        	title:'Entregas por ciclo',
                                            collapsible:true,
                                            id:'ventanaHistorial',
                                            renderTo:'entregasA',
                                            width:850,
                                            height:700,
                                            layout:'absolute',
                                            //,
											//defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:80,
                                                            y:25,
                                                            html:'<span class="corpo8_bold">Ciclo: </span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:120,
                                                            y:25,
                                                            html:'<span class="corpo8_bold">'+gE('idCiclo').value+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:220,
                                                            y:25,
                                                            html:'<span class="corpo8_bold">Mes:</span> '
                                                        },
                                                        cmbMeses
                                                        ,
                                                        {
                                                        	xtype:'panel',
                                                            x:45,
                                                            y:60,
                                                            width:750,
                                                            items:[
                                                                        gP
                                                                   ]
                                                         }
													]
										}
									);
       return panel;                             
}

function crearGrid()
{
    var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'cantidad'},
                                                                  {name: 'existencia'},
                                                                  {name: 'situacion'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=34;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                        proxy.baseParams.idCiclo=gE('idCiclo').value;
                                        proxy.baseParams.mes=gE('mes').value;
                                    }
                        );
   
    
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr></table>'
                                                                        
                                                					  )
                                            });                                                                          
    
     var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: 100,
                                                  store: dsRegistrosP,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );      
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
														{
															header:'Producto',
															width:380,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Comprometido',
															width:80,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'Existencia',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'existencia'
                                                           
														}
                                                        ,
                                                        {
															header:'Estado',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'situacion',
                                                            renderer:function(val,meta,registro)
                                                            				  {
                                                                              	return'<a href="javascript:verAgendadas('+registro.get('idProducto')+')">'+val+'</a>';
                                                                              }
														},
                                                        {
															header:'',
															width:50,
															sortable:true,
                                                            align:'center',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:verDistribucion('+registro.get('idProducto')+',\''+bE(registro.get('nombreProducto'))+'\','+registro.get('cantidad')+','+registro.get('existencia')+')"><img height="13" width="13" src="../images/Icono_Ppt.gif" alt="Ver Comprometido" title="Ver Comprometido" /></a>';
                                                                            //&nbsp;&nbsp;'+
                                                                            	  // '<a href="javascript:otraFuncion('+registro.get('idProducto')+')"><img height="13" width="13" src="../images/calendar.png" alt="Agendar Entrega" title="Agendar Entrega" /></a>';
                                                                            //&nbsp;&nbsp;<a href="javascript:Cancelar('+registro.get('idPedido')+','+registro.get('folio')+')"><img height="13" width="13" src="../images/cancel_round.png" alt="Cancelar" title="Cancelar" /></a>';
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridProductosE',
                                                            title:'Productos por Entregar',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            //renderTo:'tabPedidos',
                                                            height:450,
                                                            width:750,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            plugins: [filters,expander]
                                                        }
                                                    );
		
    dsRegistrosP.load({url:'../paginasFunciones/funcionesAlmacen.php',params:{start:0,limit:100}})  ;
    return tblGridP;     
}

function funcCargarPanel(combo,fila,pos)
{
	//var idCiclo=Ext.getCmp('cmbCiclo').getValue();
    var mes=Ext.getCmp('cmbMeses').getValue();
    //gE('idCiclo').value=idCiclo;
    gE('mes').value=mes;
    
    Ext.getCmp('gridProductosE').getStore().reload();
}

function cargarDatos()
{
	//var idCiclo=Ext.getCmp('cmbCiclo').setValue(gE('idCiclo').value);
    var mes=Ext.getCmp('cmbMeses').setValue(gE('mes').value);
}

function verDistribucion(idProducto,nombreP,cantidad,existencia)
{
    var gridD=crearGridDeptos(idProducto,existencia,nombreP);
    var formA = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:50,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Producto:</b>'
													 },
                                                     {
													 x:120,
													 y:10,
                                                     width:450,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+bD(nombreP)+'</span>'
													 },
                                                     {
													 x:40,
													 y:70,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Existencia:</b>'
													 },
                                                     {
                                                     x:110,
													 y:70,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+existencia+'</b>'
                                                     },
                                                     {
													 x:180,
													 y:70,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Cantidad Comprometida:</b>'
													 },
                                                     {
                                                     x:330,
													 y:70,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+cantidad+'</b>'
                                                     },
                                                     {
                                                        xtype:'panel',
                                                        x:0,
                                                        y:100,
                                                        items:[
                                                                    gridD
                                                               ]
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
                                        id:'ventana1',
										width: 800,
										height:600,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formA,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();                           

}

function crearGridDeptos(idProducto,existencia,nombreProducto)
{
	var dsRegistrosDep=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPrograma'},
                                                                  {name: 'tituloPrograma'},
                                                                  {name: 'codigoUnidad'},
                                                                  {name: 'cantidadSolicitada'},
                                                                  {name: 'entregados'},
                                                                  {name: 'diferencia'},
                                                                  {name: 'unidad'},
                                                                  {name: 'totalE'},
                                                                  {name: 'totalP'},
                                                                  {name: 'totalA'},
                                                                  {name: 'existencia'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosDep.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=35;
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                        proxy.baseParams.idCiclo=gE('idCiclo').value;
                                        proxy.baseParams.mes=gE('mes').value;
                                        proxy.baseParams.idProducto=idProducto;
                                    }
                        );
    
  // var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'unidad' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
    //var expander = new Ext.ux.grid.RowExpander({
//                                                column:1,
//                                                tpl : new Ext.Template(
//                                                                        '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr></table>'
//                                                                        
//                                                					  )
//                                            });                                                                          
    
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    chkRow.on('rowselect',function(chk,numeroFila,registro)
             			 {
                         	if(registro.get('existencia')==0)
                            {
                            	msgBox('La cantidad disponible para este registro es <b>0</b><br />Por lo tanto no puede agendar entrega');
                                gEx('gDeptos').getSelectionModel().deselectRow(numeroFila);
                                return;
                               
                            }
                         }	
             );
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        //expander,
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'unidad',
                                                            align:'left'
														},
                                                        {
															header:'Programa',
															width:300,
															sortable:true,
															dataIndex:'tituloPrograma'
														},
                                                        {
															header:'Comprometidos',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'cantidadSolicitada'
                                                           
														}
                                                        ,
                                                        {
															header:'Entregados',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'entregados',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:verEntregas('+registro.get('totalE')+','+(registro.get('totalP'))+',\''+bE(registro.get('unidad'))+'\',\''+bE(registro.get('tituloPrograma'))+'\')" title="Ver Detalle">'+val+'</a>';
                                                                    }
														},
                                                        {
															header:'Diferencia',
															width:70,
                                                            align:'center',
															sortable:true,
															dataIndex:'diferencia',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:verAgendados('+registro.get('totalA')+',\''+bE(registro.get('unidad'))+'\',\''+bE(registro.get('tituloPrograma'))+'\')" title="Ver Detalle">'+val+'</a>';
                                                                    }
														}
                                                        ,
                                                        {
															header:'Disponible',
															width:70,
                                                            align:'center',
															sortable:true,
                                                            dataIndex:'existencia'
														}
													]
												);
	var tblGridD=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gDeptos',
                                                            title:'Productos por Entregar',
                                                            store:dsRegistrosDep,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            //renderTo:'tabPedidos',
                                                            height:420,
                                                            width:770,
                                                            //,
                                                            //plugins: [filters,expander]
                                                            tbar:[                                                        
                                                                	{
                                                                    	text:'Agendar Entrega',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                          {
                                                                              agendarEntrega(idProducto,existencia,nombreProducto);
                                                                          }
                                                                    }
                                                                ]
                                                        }        
                                                    );
		
    dsRegistrosDep.load()  ;
    return tblGridD;     
}

function verEntregas(totalE,totalP,unidad,nombrePrograma)
{
	 var formB = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:20,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Departamento:</b>'
													 },
                                                     {
													 x:115,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+bD(unidad)+'</span>'
													 },
                                                     {
													 x:47,
													 y:50,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Programa:</b>'
													 },
                                                     {
                                                     x:115,
													 y:50,
                                                     xtype:'label',
                                                     html:'<span class="letraRojaSubrayada8">'+bD(nombrePrograma)+'</span>'
                                                     },
                                                     {
													 x:17,
													 y:90,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>No. Entregados:</b>'
													 },
                                                     {
                                                     x:115,
													 y:90,
                                                     xtype:'label',
                                                     html:'<span class="letraRojaSubrayada8">'+totalE+'</span>'
                                                     },
                                                     {
													 x:210,
													 y:90,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>En proceso de entrega:</b>'
													 },
                                                     {
                                                     x:350,
													 y:90,
                                                     xtype:'label',
                                                     html:'<span class="letraRojaSubrayada8">'+totalP+'</span>'
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Detalle de Entregas',
										width: 550,
                                        id:'ventana2',
                                        closable:true,
										height:180,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formB,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
     ventana.show();           
}

function verAgendados(totalA,unidad,nombrePrograma)
{
	var formAg = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:20,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Departamento:</b>'
													 },
                                                     {
													 x:115,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+bD(unidad)+'</span>'
													 },
                                                     {
													 x:47,
													 y:50,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Programa:</b>'
													 },
                                                     {
                                                     x:115,
													 y:50,
                                                     xtype:'label',
                                                     html:'<span class="letraRojaSubrayada8">'+bD(nombrePrograma)+'</span>'
                                                     },
                                                     {
													 x:17,
													 y:90,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>No. Agendados:</b>'
													 },
                                                     {
                                                     x:115,
													 y:90,
                                                     xtype:'label',
                                                     html:'<span class="letraRojaSubrayada8">'+totalA+'</span>'
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Detalle de Agendados',
										width: 550,
                                        id:'ventana3',
                                        closable:true,
										height:180,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formAg,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
     ventana.show();           
}


function agendarEntrega(idProducto,existencia,nombreProducto)
{
    if(existencia==0)
    {
    	Ext.MessageBox.alert(lblAplicacion,'No puede agendar entregas con <b>0</b> exitencias del producto ');
        return;
    }    
    
    var fila=Ext.getCmp('gDeptos').getSelectionModel().getSelections();
    var tamano=fila.length;
    if(fila==0)
    {
    	Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar a menos un departamento');
        return;
    }
    
    var fechaActual=gE('fechaActual').value;
    var fechaFinal=gE('fechaFinal').value;
    var x;
    var cadena='';
    var arregloG=new Array();
    for(x=0;x<tamano;x++)
    {
        var idPrograma=fila[x].get('idPrograma');
        var nombreP=fila[x].get('tituloPrograma');
        var codigoUnidad=fila[x].get('codigoUnidad');
        var unidad=fila[x].get('unidad');
        var diferencia=fila[x].get('existencia');
        
        var obj=[idPrograma,nombreP,codigoUnidad,unidad,diferencia,'',idProducto,nombreProducto];
        arregloG.push(obj);
    }
    
    var gridE=crearGridEntregas(arregloG);
    var formC = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													  x:10,
													  y:15,
													  xtype:'label',
                                                      html:'<b>Fecha Entrega: <font color="#FF0000">*</font></b>'
													 },
                                                     {
													  x:110,
													  y:10,
													  xtype:'datefield',
                                                      id:'fechaEntrega',
                                                      minValue:fechaActual,
                                                      maxValue:fechaFinal
													 },
                                                     {
													  x:260,
													  y:15,
													  xtype:'label',
                                                      html:'<b>Hora Inicio:</b>'
													 },
                                                     {
													  x:330,
													  y:10,
													  xtype:'timefield',
                                                      width:100,
                                                      id:'horaInicio'
													 },
                                                     {
													  x:500,
													  y:15,
													  xtype:'label',
                                                      html:'<b>Tiempo estimado:</b>'
													 },
                                                     {
													  x:605,
													  y:10,
													  xtype:'numberfield',
                                                      id:'tiempoE',
                                                      width:60
													 },
                                                     {
													  x:670,
													  y:15,
													  xtype:'label',
                                                      html:'<b>Min.</b>'
													 },
                                                     {
													  x:10,
													  y:40,
													  xtype:'label',
                                                      html:'<span class="letraRojaSubrayada8">Nota:</span><span> <b>Si elige una hora de inicio es obligatorio indicar el tiempo estimado</b></span>'
													 },
                                                     {
                                                      x:0,
                                                      y:70,
                                                      xtype:'panel',
                                                      items:[
                                                                gridE
                                                           ]
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Agendar Entregas',
										width: 750,
                                        id:'ventana4',
										height:500,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formC,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
                                                                                    var idAlmacen=gE('idAlmacen').value;
                                                                                    var mes=gE('mes').value;
                                                                                    var idCiclo=gE('idCiclo').value;
                                                                                    var filas=Ext.getCmp('nEntregas').getStore();
                                                                                    var tamano=filas.getCount();
                                                                                    if(tamano==0)
                                                                                    {
                                                                                    	msgBox('No puede agendar si elementos seleccionados');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var fechaEntrega=Ext.getCmp('fechaEntrega').getValue();
                                                                                    if(fechaEntrega=='')
                                                                                    {
                                                                                    	msgBox('Debe indicar la fecha de entrega');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var horaInicio=Ext.getCmp('horaInicio').getValue();
                                                                                    if(horaInicio=='')
                                                                                    {
                                                                                     	horaInicio=0;
                                                                                    }
                                                                                    
                                                                                    var tiempoE=Ext.getCmp('tiempoE').getValue();
                                                                                    if(tiempoE=='')
                                                                                    {
                                                                                     	tiempoE=0;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	if(tiempoE==0)
                                                                                        {
                                                                                            msgBox('El tiempo estimado no puede ser igual a <b>0</b>');
                                                                                            return;
                                                                                        }
                                                                                    }
                                                                                    
                                                                                    fechaEntrega=Ext.util.Format.date(fechaEntrega,'Y-m-d');
                                                                                    var x;
                                                                                    
                                                                                    var cadenaGuardar='';
                                                                                    var arregloE=new Array();
                                                                                    
                                                                                    for(x=0;x<tamano;x++)
                                                                                    {
                                                                                    	var cantidadE=filas.getAt(x).get('cantidadE');
                                                                                        if(cantidadE!='')
                                                                                        {
                                                                                        	cantidadE=parseInt(cantidadE);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                        	cantidadE=0;
                                                                                        }
                                                                                        var existencia=parseInt(filas.getAt(x).get('existencia'));
                                                                                        var codigoUnidad=filas.getAt(x).get('codigoUnidad');
                                                                                        var idPrograma=filas.getAt(x).get('idPrograma');
                                                                                        
                                                                                        if((cantidadE>existencia) || (cantidadE==0))
                                                                                        {
                                                                                            if(cantidadE!=0)
                                                                                            {
                                                                                            	var exceso=cantidadE-existencia; 
                                                                                            }
                                                                                            
                                                                                            var depto=filas.getAt(x).get('unidad');
                                                                                            var programa=filas.getAt(x).get('nombrePrograma');
                                                                                            
                                                                                            var obj=[depto,programa,exceso];
        																					arregloE.push(obj);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                                if(cadenaGuardar=='')
                                                                                                    cadenaGuardar=codigoUnidad+'_'+idPrograma+'_'+cantidadE+'_'+fechaEntrega+'_'+horaInicio+'_'+tiempoE;
                                                                                                else
                                                                                                    cadenaGuardar+=','+codigoUnidad+'_'+idPrograma+'_'+cantidadE+'_'+fechaEntrega+'_'+horaInicio+'_'+tiempoE; 
                                                                                        }
                                                                                        
                                                                                        //if(cadenaGuardar=='')
//                                                                                                    cadenaGuardar=codigoUnidad+'_'+idPrograma+'_'+cantidadE+'_'+fechaEntrega+'_'+horaInicio+'_'+tiempoE;
//                                                                                                else
//                                                                                                    cadenaGuardar+=','+codigoUnidad+'_'+idPrograma+'_'+cantidadE+'_'+fechaEntrega+'_'+horaInicio+'_'+tiempoE;
                                                                                    }
                                                                                   
                                                                                    if((horaInicio!=0) && (tiempoE!=0))
                                                                                    {
                                                                                    	function funcAjax1()
                                                                                        {
                                                                                            var resp=peticion_http.responseText.split('|');
                                                                                            if(resp[0]==1)
                                                                                            {
                                                                                                  if(arregloE.length>0)
                                                                                                  {
                                                                                                      crearVentanaError(arregloE);
                                                                                                      return;
                                                                                                  }
                                                                                                   
                                                                                                  var cadAuxG=gE('cadenaAux').value;
                                                                                                  if(cadAuxG!='')
                                                                                                  {
                                                                                                      cadenaGuardar+=','+cadAuxG;
                                                                                                  }
                                                                                                  function funcAjax()
                                                                                                  {
                                                                                                      var resp=peticion_http.responseText.split('|');
                                                                                                      if(resp[0]==1)
                                                                                                      {
                                                                                                           var almacen=Ext.getCmp('gDeptos').getStore();
                                                                                                           almacen.reload(); 
                                                                                                           ventana.close();
                                                                                                      }
                                                                                                      else
                                                                                                      {
                                                                                                            Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                                      }
                                                                                                  }
                                                                                                  obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=36&idProducto='+idProducto+'&fechaEntrega='+fechaEntrega+'&horaInicio='+horaInicio+'&tiempoE='+tiempoE+'&cadena='+cadenaGuardar+'&idAlmacen='+idAlmacen+'&mes='+mes+'&idCiclo='+idCiclo,true);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                   if(resp[0]==2)
                                                                                            	   {
                                                                                                   		 msgBox('El tiempo estimado no puede ser igual a <b>'+tiempoE+'</b>, ya que este numero genera una cambio de fecha');
                                                                                        				 return;
                                                                                                   }
                                                                                                   else
                                                                                                   {	
                                                                                                        Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                                   }     
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax1, 'POST','funcion=37&fecha='+fechaEntrega+'&horaInicio='+horaInicio+'&tiempo='+tiempoE,true)
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if(arregloE.length>0)
                                                                                        {
                                                                                            crearVentanaError(arregloE);
                                                                                            return;
                                                                                        }
                                                                                         
                                                                                        var cadAuxG=gE('cadenaAux').value;
                                                                                        if(cadAuxG!='')
                                                                                        {
                                                                                            cadenaGuardar+=','+cadAuxG;
                                                                                        }
                                                                                        
                                                                                        function funcAjax2()
                                                                                        {
                                                                                            var resp=peticion_http.responseText.split('|');
                                                                                            if(resp[0]==1)
                                                                                            {
                                                                                                 var almacen=Ext.getCmp('gDeptos').getStore();
                                                                                                 almacen.reload(); 
                                                                                                 ventana.close();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=36&idProducto='+idProducto+'&fechaEntrega='+fechaEntrega+'&horaInicio='+horaInicio+'&tiempoE='+tiempoE+'&cadena='+cadenaGuardar+'&idAlmacen='+idAlmacen+'&mes='+mes+'&idCiclo='+idCiclo,true);
                                                                                    }    
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();  
     ventana.on('close',function()
     					{
                        	gE('cadenaAux').value='';
                        }
     		   );                         
}

function crearGridEntregas(arreglo)
{
    var arrDatos=arreglo;
    
    var dSetEntregas= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idPrograma'},
                                                                    {name:'nombrePrograma'},
                                                                    {name:'codigoUnidad'},
                                                                    {name: 'unidad'},
                                                                    {name: 'existencia'},
                                                                    {name: 'cantidadE'},
                                                                    {name: 'idProducto'},
                                                                    {name: 'nombreProducto'}
                                                                ]
                                                    }
                                                 )
    
	dSetEntregas.loadData(arrDatos);	
	var cmP= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Departamento',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'unidad'
                                                            },
                                                            {
                                                                header:'Programa',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombrePrograma'
                                                            },
                                                            {
                                                                header:'Disponible',
                                                                width:70,
                                                                sortable:true,
                                                                dataIndex:'existencia'
                                                            },
                                                            {
                                                                header:'cantidad',
                                                                width:70,
                                                                sortable:true,
                                                                editor:{xtype:'numberfield'},
                                                                dataIndex:'cantidadE'
                                                            }   
                                                        ]
                                                    );
											
	var gEntregas=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'nEntregas',
                                                        store:dSetEntregas,
                                                        frame:true,
                                                        cm: cmP,
                                                        height:345,
                                                        width:715
													}
					
    											);
    gEntregas.on('afteredit',funcEditorFilaBeforeEdit)                                            
	return gEntregas;
}

function crearVentanaError(arreglo)
{
	var arrDatos=arreglo;
    
    var dSetErrores= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'depto'},
                                                                    {name:'programa'},
                                                                    {name:'exceso'}
                                                                ]
                                                    }
                                                 )
    
	dSetErrores.loadData(arrDatos);	
	var cmP= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Departamento',
                                                                width:330,
                                                                sortable:true,
                                                                dataIndex:'depto'
                                                            },
                                                            {
                                                                header:'Programa',
                                                                width:330,
                                                                sortable:true,
                                                                dataIndex:'programa'
                                                            },
                                                            {
                                                                header:'Excedente',
                                                                width:70,
                                                                sortable:true,
                                                                dataIndex:'exceso'
                                                            }
                                                        ]
                                                    );
											
	var gError=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'nErrores',
                                                        store:dSetErrores,
                                                        frame:true,
                                                        cm: cmP,
                                                        height:270,
                                                        width:770
													}
					
    											);
                                                
    var formE = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													  x:50,
													  y:10,
													  xtype:'label',
                                                      html:'<img src="../images/exclamation.png" width="20" height="20" />'
													 },
                                                     {
													  x:90,
													  y:15,
													  xtype:'label',
                                                      html:'<b>No puede agendar esta entrega por alguno de los siguientes motivos:</b>'
													 },
                                                     {
													  x:90,
													  y:32,
													  xtype:'label',
                                                      html:'<b>-</b>La solicitud excede la cantidad disponible'
													 },
                                                     {
													  x:90,
													  y:47,
													  xtype:'label',
                                                      html:'<b>-</b>La cantidad solicitada es 0'
													 },
                                                     {
													  x:90,
													  y:62,
													  xtype:'label',
                                                      html:'<b>-</b>La existencia para el registro seleccionado es 0'
													 },
                                                     {
                                                      x:0,
                                                      y:100,
                                                      xtype:'panel',
                                                      items:[
                                                                gError
                                                           ]
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
                                        id:'ventana5',
										width: 800,
										height:480,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formE,
                                        listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
                                        				{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
                                        			]
									}
								);
     ventana.show();                                                                       
}

function funcEditorFilaBeforeEdit(e)
{
	var valor=parseInt(e.record.get('cantidadE'));
    var existencia=parseInt(e.record.get('existencia'));
    var idProducto=e.record.get('idProducto');
    var idPrograma=e.record.get('idPrograma');
    var codigoUnidad=e.record.get('codigoUnidad');
    var unidad=e.record.get('unidad')
    var nombrePrograma=e.record.get('nombrePrograma');
    var nombreProducto=e.record.get('nombreProducto');
    var posicion=e.row;
    
    if(valor<existencia)
    {
          function respE(btnE)
          {
              if(btnE=='yes')
              {
                  var nuevoValor=existencia-valor;
                  mostrarVentanaAgendar(idProducto,idPrograma,nombrePrograma,unidad,codigoUnidad,nuevoValor,nombreProducto,posicion,e.originalValue);
              }
              else
              {
                    e.record.set('cantidadE',e.originalValue);
              }        
          }
          msgConfirm('El valor ingresado es menor a la cantidad solicitada.<br/> &iquest;Desea agendar el resto a una fecha diferente?',respE);
    }
}


function mostrarVentanaAgendar(idProducto,idPrograma,nombrePrograma,unidad,codigoUnidad,nuevoValor,nombreProducto,posicion,valorE)
{
    gE('banderaCerrar').value=0;
    gE('numeroR').value=posicion;
    var fechaActual=gE('fechaActual').value;
    var fechaFinal=gE('fechaFinal').value;
    var formNe = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:35,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Producto:</b>'
													 },
                                                     {
													 x:100,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+bD(nombreProducto)+'</span>'
													 },
                                                     {
													 x:2,
													 y:60,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Departamento:</b>'
													 },
                                                     {
													 x:100,
													 y:60,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+unidad+'</span>'
													 },
                                                     {
													 x:28,
													 y:100,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Programa:</b>'
													 },
                                                     {
													 x:100,
													 y:100,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+nombrePrograma+'</span>'
													 },
                                                     {
                                                     x:52,
													 y:140,
                                                     xtype:'label',
                                                     html:'<b>Fecha:</b>'
                                                     },
                                                     {
                                                     x:100,
													 y:140,
                                                     xtype:'datefield',
													 id:'fechaReagenda',
                                                     minValue:fechaActual,
                                                     maxValue:fechaFinal
                                                     }
//                                                     ,
//                                                     {
//													 x:50,
//													 y:85,
//													 xtype:'label',
//                                                     align:'center',
//													 html:'<b>Hora Inicio:</b>'
//													 },
//                                                     {
//                                                     x:120,
//													 y:80,
//                                                     xtype:'timefield',
//													 id:'horaInicio'
//                                                     },
//                                                     {
//													 x:15,
//													 y:110,
//													 xtype:'label',
//                                                     align:'center',
//													 html:'<b>Tiempo estimado:</b>'
//													 },
//                                                     {
//                                                     x:120,
//													 y:105,
//                                                     xtype:'numberfield',
//                                                     width:50,
//													 id:'tiempoE2'
//                                                     },
//                                                     {
//													 x:180,
//													 y:110,
//													 xtype:'label',
//                                                     align:'center',
//													 html:'<b>(Min.)</b>'
//													 }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Reagendar Entrega',
										width: 500,
                                        id:'ventana6',
										height:270,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formNe,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptar2',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var fecha=Ext.getCmp('fechaReagenda').getValue();
                                                                                    if(fecha=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar la fecha para agendar la entrega');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    fecha=Ext.util.Format.date(fecha,'Y-m-d');
                                                                                    //var numero=gE('numeroR').value;
                                                                                    
                                                                                    var cadenaAux=gE('cadenaAux').value;
                                                                                    if(cadenaAux=='')
                                                                                    {
                                                                                    	cadenaAux=codigoUnidad+'_'+idPrograma+'_'+nuevoValor+'_'+fecha+'_0_0';
                                                                                        gE('cadenaAux').value=cadenaAux;
                                                                                    }   
                                                                                    else
                                                                                    {
                                                                                    	var obj=codigoUnidad+'_'+idPrograma+'_'+nuevoValor+'_'+fecha+'_0_0';
                                                                                        var nuevaCadAux='';
                                                                                        var arreglo=cadenaAux.split(',');
                                                                                        var tamano=arreglo.length;
                                                                                        var x;
                                                                                        var encontrado=0;
                                                                                        
                                                                                        for(x=0;x<tamano;x++)
                                                                                        {
                                                                                        	var datos=arreglo[x].split('_');
                                                                                            var codU=datos[0];
                                                                                            var idP=datos[1];
                                                                                            var valor=datos[2];
                                                                                            var fch=datos[3];
                                                                                            if((codU==codigoUnidad) && (idP==idPrograma))
                                                                                            {
                                                                                                valor=nuevoValor;
                                                                                                encontrado=1;
                                                                                            }
                                                                                            
                                                                                            if(nuevaCadAux=='')
                                                                                            	nuevaCadAux=codU+'_'+idP+'_'+valor+'_'+fch+'_0_0';
                                                                                            else
                                                                                            	nuevaCadAux+=','+codU+'_'+idP+'_'+valor+'_'+fch+'_0_0';  
                                                                                        }
                                                                                        
                                                                                        if(encontrado==0)
                                                                                        {
                                                                                        	nuevaCadAux+=','+obj;  
                                                                                        }
                                                                                        
                                                                                        gE('cadenaAux').value=nuevaCadAux;
                                                                                    }
                                                                                    gE('banderaCerrar').value=1;
                                                                                    ventana.close();      
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show(); 
     ventana.on('close',function()
     					{
                        	var almacen=Ext.getCmp('nEntregas').getStore();
                            var numero=gE('numeroR').value;
                            if(gE('banderaCerrar').value==0)
                            {
                            	var fila=almacen.getAt(numero).set('cantidadE',valorE);
                            }    
                        }
     		   );
}

function verAgendadas(idProducto)
{
	var idAlmacen=gE('idAlmacen').value;
    var mes=gE('mes').value;
    var idCiclo=gE('idCiclo').value;
    var gEntP=gridEntregasProg(idProducto,idCiclo,mes,idAlmacen);
    var form4 = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
                                            id:'formulario4',
											defaultType: 'textfield',
											items: 	[
                                            		{
                                                        	xtype:'panel',
                                                            x:0,
                                                            y:0,
                                                            width:750,
                                                            items:[
                                                                        gEntP
                                                                   ]
                                                         }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Detalle de Entregas Agendadas',
										width: 700,
                                        id:'ventana7',
                                        closable:true,
										height:400,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form4,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
     ventana.show();         
}

function gridEntregasProg(idProducto,idCiclo,mes,idAlmacen)
{
	var dsRegComp=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idEntrega'},
                                                                  {name: 'idProducto'},
                                                                  {name: 'nombreP'},
                                                                  {name: 'codUnidad'},
                                                                  {name: 'nomD'},
                                                                  {name: 'idPrograma'},
                                                                  {name: 'nombrePrg'},
                                                                  {name: 'fecha'},
                                                                  {name: 'horaIni'},
                                                                  {name: 'horaFin'},
                                                                  {name: 'estado'},
                                                                  {name: 'cantidad'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegComp.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=78;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                        proxy.baseParams.idCiclo=idCiclo;
                                        proxy.baseParams.mes=mes;
                                        proxy.baseParams.idProducto=idProducto;
                                    }
                        );
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Departamento',
															width:300,
															sortable:true,
															dataIndex:'nomD',
                                                            align:'left'
														},
                                                        {
															header:'Fecha Entrega',
															width:80,
                                                            align:'center',
															sortable:true,
															dataIndex:'fecha'
														},
                                                        {
															header:'Hora Inicio',
															width:65,
                                                            align:'center',
															sortable:true,
															dataIndex:'horaIni'
														},
                                                        {
															header:'Hora Fin',
															width:60,
                                                            align:'center',
															sortable:true,
															dataIndex:'horaFin'
														},
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'Estado',
															width:130,
                                                            align:'left',
															sortable:true,
															dataIndex:'estado',
                                                            renderer:function(val,meta,registro)
                                                            				 {
                                                                                if(val=='0')
                                                                                {
                                                                                	return'Agendado';
                                                                                }
                                                                                else
                                                                                {
                                                                                	if(val==1)
                                                                                    {
                                                                                    	return 'Entregado'
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	return 'En proceso de entrega'
                                                                                    }
                                                                                }
                                                                             }
														}
													]
												);
	var tblGridC=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridComprometidos',
                                                            store:dsRegComp,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:350,
                                                            width:670
                                                        }
                                                    );
    dsRegComp.load()  ;
    return tblGridC;      
}