<?php session_start();
	header("charset=utf-8");
	include("latis/funcionesFormularios.php"); 
	include("latis/configurarIdioma.php");
	if(isset($_SESSION["leng"]))
	{
		$z=0;
		$consultaS;
		if(isset($_POST["parametros"]))
			$parametros=$_POST["parametros"];
		if(isset($_POST["funcion"]))
			$funcion=$_POST["funcion"];
		$lenguaje=$_SESSION["leng"];
		
		switch($funcion)
		{
			case 1:
				guardarNombreSeccionInfoProducto();
			break;
			case 2:
				eliminarSeccionInfoProducto();
			break;
			case 3:
				obtenerPedidosRecibidosAlmacen();
			break;
			case 4:
				obtenerPedidosPendientesAlmacen();
			break;
			case 5:
				obtenerPedidosCanceladosAlmacen();
			break;
			case 6:
				obtenerProductosPedidos();
			break;
			case 7:
				guardarTemporalPedido();
			break;
			case 8:
				guardarObservacionesProducto();
			break;
			case 9:
				obtenerObservacionesProducto();
			break;
			case 10:
				registrarPedido();
			break;
			case 11:
				obtenerPedidosValidacion();
			break;
			case 12:
				cancelarPedido();
			break;
			case 13:
				borrarRegistroPedido();
			break;
			case 14:
				obtenerCategoriasAlmacen();
			break;
			case 15:
				obtenerFolioPedido();
			break;
			case 16:
				guardarNuevoPedido();
			break;
			case 17:
				obtenerSanciones();
			break;
			case 18:
				obtenerDatosSancion();
			break;
			case 19:
				operacionesSancion();
			break;
			case 20:
				eliminarSancion();
			break;
			case 21:
				obtenerConceptosLibres();
			break;
			case 22:
				eliminarCategoria();
			break;
			case 23:
				guardarCategoria();
			break;
			case 24:
				guardarAlmacen();
			break;
			case 25:
				obtenerCatAlmacen();
			break;
			case 26:
				obtenerCatNoAlmacen();
			break;
			case 27:
				ligarCategoriaAlmacen();
			break;
			case 28:
				obtenerInventarioAlmacen();
			break;
			case 29:
				agendarFechaEntrega();
			break;
			case 30:
				obtenerExistenciaAlmacenProducto();
			break;
			case 31:
				obtenerSeccionesAlmacen();
			break;
			case 32:
				eliminarSeccionesAlmacen();
			break;
			case 33:
				obtenerPartidas();
			break;
			case 34:
				obtenerComprometidoEntregas();
			break;
			case 35:
				obtenerDeptosEntregaProducto();
			break;
			case 36:
				guardarEntregaAlmacen();
			break;
			case 37:
				validarFechaTiempoEstimado();
			break;
			case 38:
				guardarConfPartidas();
			break;
			case 39:
				obtenerConfiguracionProducto();
			break;
			case 40:
				obtenerRegistrosForDin();
			break;
			case 41:
				obtenerRegistroForDinVolumen();
			break;
			case 42:
				obtenerSolicitudesAlmacen();
			break;
			case 43:
				detallesPedidoDetalle();
			break;
			case 44:
				obtenerProgramaDeptoCiclo();
			break;
			case 45:
				obtenerExistenciaProductoAlmacen();
			break;
			case 46:
				obtenerDatosPedido();
			break;
			case 47:
				obtenerHistorialProv();
			break;
			case 48:
				obtenerPedidosPendiestesPorFecha();
			break;
			case 49:
				obtenerFacturaPedido();
			break;
			case 50:
				obtenerInformacionEntregaProgramada();
			break;
		}
	}
	
	
	function guardarNombreSeccionInfoProducto()
	{
		global $con;
		global $SO;
		$idCategoria=$_POST["idCategoria"];
		$idSeccion=$_POST["idSeccion"];
		$nSeccion=$_POST["nSeccion"];
		$descripcion=$_POST["descripcion"];
		$x=0;
		$query="begin";
		if($con->ejecutarConsulta($query))
		{
			if($idSeccion=="-1")
			{
				$query="insert into 9032_seccionesCategoriaProducto(nombreSeccion,descripcion,idCategoriaProducto) values('".cv($nSeccion)."','".cv($descripcion)."',".$idCategoria.")";
				if(!$con->ejecutarConsulta($query))			
				{
					echo "|";
					return;
				}
				$idSeccion=$con->obtenerUltimoID();	
				$descripcion="";
				if($SO==1)
				{
					$nFormulario=utf8_decode("Ficha de categoría: ".$nSeccion);
					$descripcion=utf8_decode("Formulario de registro de datos complementarios de producto, para aquellos productos que pertenezcan a la categoría de: ".cv($nSeccion));
				}
				else
				{
					$nFormulario=("Ficha de categoría: ").$nSeccion;
					$descripcion="Formulario de registro de datos complementarios de producto, para aquellos productos que pertenezcan a la categoría de: ".cv($nSeccion);
				}
					
				$objFormulario='{
									"nombreFormulario":"'.$nFormulario.'",
									"descripcion":"'.$descripcion.'",
									"titulo":"'.$nFormulario.'",
									"idProceso":"0",
									"idEtapa":"1",
									"idFrmEntidad":"-1",
									"frmRepetible":"0",
									"formularioBase":"1",
									"estadoInicial":"1",
									"eliminable":"0",
									"tipoFormulario":"30",
									"mostrarTableroControl":"0",
									"complemetario":"'.$idSeccion.'"
								}';
				
				$idFormulario=crearFormulario($objFormulario);
				if($idFormulario=="-1")
				{
					echo "|";
					return;
				}
				$consulta[$x]="update 9032_seccionesCategoriaProducto set idFormulario=".$idFormulario." where idSeccionInfoCategoria=".$idSeccion;
			}
			else
				$consulta[$x]="update 9032_seccionesCategoriaProducto set nombreSeccion='".cv($nSeccion)."',descripcion='".cv($descripcion)."' where idSeccionInfoCategoria=".$idSeccion;
			$x++;
			$consulta[$x]="commit";
			$x++;
			
			if($con->ejecutarBloque($consulta))
			{
				$query="SELECT * FROM 9032_seccionesCategoriaProducto WHERE idCategoriaProducto=".$idCategoria;
				$arrDatos=$con->obtenerFilasArreglo($query);
				echo "1|".uEJ($arrDatos);
			}
		}
		else
			echo "|";
	}
	
	function eliminarSeccionInfoProducto()
	{
		global $con;
		$idSeccion=$_POST["idSeccion"];
		$consulta="select idFormulario from 9032_seccionesCategoriaProducto where idSeccionInfoCategoria=".$idSeccion;
		$idFormulario=$con->obtenerValor($consulta);	
		$x=0;
		$query[$x]="begin";
		$x++;
		$query[$x]="delete from 9032_seccionesCategoriaProducto where idSeccionInfoCategoria=".$idSeccion;
		$x++;
		$query[$x]="delete from 900_formularios where idFormulario=".$idFormulario;
		$x++;
		$query[$x]="commit";
		eB($query);
	}
	
	function obtenerPedidosRecibidosAlmacen()
	{
		global $con;
		$idAlmacen=$_POST["idAlmacen"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idPedido,folioPedido,idProveedor_ult,txtRazonSocial2,date_format(fechaRecepcion,'%d/%m/%Y') as fechaRecepcion,date_format(fechaRecibido,'%d/%m/%Y') as fechaRecibido
				   FROM 9102_PedidoCabecera p, _405_tablaDinamica pr
				   WHERE idProveedor_ult=id__405_tablaDinamica AND idAlmacen=".$idAlmacen." AND status_pedido=0 ".$condWhere." ORDER BY  fechaRecepcion ASC";
		$res=$con->obtenerFilas($consulta);	
		$nreg=$con->filasAfectadas;
		
		$arrPedidos="";
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='{"idPedido":"'.$fila[0].'","folioPedido":"'.$fila[1].'","idProv":"'.$fila[2].'","txtRazonSocial2":"'.$fila[3].'","fechaRecepcion":"'.$fila[4].'","fechaRecibido":"'.$fila[5].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	}
	
	function obtenerPedidosPendientesAlmacen()
	{
		global $con;
		$idAlmacen=$_POST["idAlmacen"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idPedido,folioPedido,idProveedor_ult,txtRazonSocial2,date_format(fechaRecepcion,'%d/%m/%Y') as fechaRecepcion,date_format(fechaAgenda,'%d/%m/%Y' )as fechaAgenda
				   FROM 9102_PedidoCabecera p, _405_tablaDinamica pr
				   WHERE idProveedor_ult=id__405_tablaDinamica AND idAlmacen=".$idAlmacen." AND status_pedido=1 ".$condWhere." ORDER BY  fechaRecepcion ASC";
		$res=$con->obtenerFilas($consulta);	
		$nreg=$con->filasAfectadas;
		
		$arrPedidos="";
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='{"idPedido":"'.$fila[0].'","folioPedido":"'.$fila[1].'","idProv":"'.$fila[2].'","txtRazonSocial2":"'.$fila[3].'","fechaRecepcion":"'.$fila[4].'","fechaAgenda":"'.$fila[5].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	}
	
	function obtenerPedidosCanceladosAlmacen()
	{
				global $con;
		$idAlmacen=$_POST["idAlmacen"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idPedido,folioPedido,idProveedor_ult,txtRazonSocial2,fechaRecepcion 
				   FROM 9102_PedidoCabecera p, _405_tablaDinamica pr
				   WHERE idProveedor_ult=id__405_tablaDinamica AND idAlmacen=".$idAlmacen." AND status_pedido=2 ".$condWhere." ORDER BY  fechaRecepcion ASC";
		$res=$con->obtenerFilas($consulta);	
		$nreg=$con->filasAfectadas;
		
		$arrPedidos="";
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='{"idPedido":"'.$fila[0].'","folioPedido":"'.$fila[1].'","idProv":"'.$fila[2].'","txtRazonSocial2":"'.$fila[3].'","fechaRecepcion":"'.$fila[4].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	}
	
	function obtenerProductosPedidos()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$idAlmacen=$_POST["idAlmacen"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT  d.idProducto,nombreProducto,clave_Art,descripcion,cantidad,idPedidoDetalle,idMarca,modelo,costoUnitario,iva,idPedidoDetalle FROM 9103_PedidoDetalle d,9101_CatalogoProducto p WHERE idPedido=".$idPedido." AND p.idProducto=d.idProducto ".$condWhere;
		$res=$con->obtenerFilas($consulta);
		$nreg=$con->filasAfectadas;
		
		$arrPedidos="";
		$ct=1;
		while($fila=mysql_fetch_row($res))
		{
			$consulta="SELECT objetoGasto FROM 9101_CatalogoProducto WHERE idProducto=".$fila[0];
			$objP=$con->obtenerValor($consulta);
			if($objP=="")
				$objP="-1";

			$consulta="SELECT *FROM 9306_confPartidaAlmacen WHERE clave='".$objP."'";
			$filaConf=$con->obtenerPrimeraFila($consulta);
			if(!$filaConf)
			{
				$filaConf[2]="-1";
				$filaConf[3]="-1";
				$filaConf[4]="-1";
			}
			
			$conMarca="SELECT descripcion FROM _406_tablaDinamica WHERE id__406_tablaDinamica=".$fila[6];
			$marca=$con->obtenerValor($conMarca);
			
			$total=0;
			$totalPieza=0;
			if(($fila[8]!="") || ($fila[8]!="")) //costoU 
			{
				//echo "costoUnitario".$fila[8];
				$importeIva=0;
				if(($fila[9]!="") || ($fila[9]!=0)) //iva
				{
					//echo $fila[9];
					$importeIva=$fila[8]*(".".$fila[9]);
					//echo $importeIva=$fila[8]*(".".$fila[9]);
				}
				//echo "importeIva".$importeIva;
				$totalPieza=$fila[8]+$importeIva;
				if(($fila[4]!="") || ($fila[4]!=0))
				{
					$total=$totalPieza*$fila[4];
				}
			}
			
			$arregloDist='';
			$cabeceraT='<table>';
			$cuerpo='';
			$existeCabecera=0;
			$cuerpoT='';
			
			$distribucion="SELECT unidad,tituloPrograma,cantidad,p.idPrograma,o.codigoUnidad FROM 817_organigrama o,517_programas p,9301_distribucionProducto d 
							WHERE  p.idPrograma=d.idPrograma AND o.codigoUnidad=d.codigoUnidad AND idPedidoDetalle=".$fila[5];
			$resD=$con->obtenerFilas($distribucion);				
			$numDist=$con->filasAfectadas;
			if($numDist>0)
			{
				$cuerpoT='<tr><td width=\"300\" class=\"letraRojaSubrayada8\">Departamento</td><td width=\"450\" class=\"letraRojaSubrayada8\">Programa</td><td width=\"80\" class=\"letraRojaSubrayada8\">Cantidad</td><td width=\"80\" class=\"letraRojaSubrayada8\">Existencia</td></tr>';
			}
			
			while($filaD=mysql_fetch_row($resD))
			{
					$existenciaD=obtenerExistenciaProgramaDepto($fila[0],$filaD[3],$filaD[4],$idAlmacen);
					$cuerpo.='<tr><td width=\"300\">'.$filaD[0].'</td><td width=\"450\">'.$filaD[1].'</td><td width=\"80\">'.$filaD[2].'</td><td width=\"80\">'.$existenciaD.'</td></tr>';
			}
			
			$finalT='</table>';
			
			$arregloDist=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
			
			
			$conEstado="SELECT estado FROM 9300_temporalPedido WHERE idPedido=".$idPedido." AND idProducto=".$fila[0];
			$estado=$con->obtenerValor($conEstado);
			if($estado=="")
				$estado=0;
			
			$obj='{"idProducto":"'.$fila[0].'","nombreProducto":"'.$fila[1].'","clave_Art":"'.$fila[2].'","descripcion":"'.$fila[3].'","cantidad":"'.$fila[4].'","estado":"'.$estado.'","distribucion":"'.$arregloDist.'","marca":"'.cv($marca).'","modelo":"'.cv($fila[7]).'","costoU":"'.$fila[8].'","iva":"'.$fila[9].'","costoNetoUnidad":"'.$totalPieza.'","total":"'.$total.'","idPedidoDetalle":"'.$fila[10].'","idFormulario":"'.$filaConf[2].'","idTipoR":"'.$filaConf[3].'","inventariable":"'.$filaConf[4].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	}
	
	function guardarTemporalPedido()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$idProducto=$_POST["idProducto"];
		$estado=$_POST["estado"];
		
		$consulta="begin";
		
		if($con->ejecutarconsulta($consulta))
		{
			$ct=0;
			$existe="SELECT idPedidoVSRegistro FROM 9300_temporalPedido WHERE idPedido=".$idPedido." AND idProducto=".$idProducto;
			$id=$con->obtenerValor($existe);
			
			if($id=="")
			{
				$query[$ct]="INSERT INTO 9300_temporalPedido (idPedido,idProducto,estado) VALUES (".$idPedido.",".$idProducto.",".$estado.")";
				$ct++;
			}
			else
			{
				$query[$ct]="UPDATE 9300_temporalPedido SET estado=".$estado." WHERE idPedidoVSRegistro=".$id;
				$ct++;
				$query[$ct]="UPDATE 9300_temporalPedido SET observaciones='' WHERE idPedidoVSRegistro=".$id;
				$ct++;
			}
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
		else
		{
			echo "|";
		}
	}
	
	function guardarObservacionesProducto()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$idProducto=$_POST["idProducto"];
		$observaciones=$_POST["observaciones"];
		
		$existe="SELECT idPedidoVSRegistro FROM 9300_temporalPedido WHERE idPedido=".$idPedido." AND idProducto=".$idProducto;
		$id=$con->obtenerValor($existe);
		
		if($id=="")
		{
			$query="INSERT INTO 9300_temporalPedido (idPedido,idProducto,estado) VALUES (".$idPedido.",".$idProducto.",2)";
		}
		else
		{
			$query="UPDATE 9300_temporalPedido SET observaciones='".cv($observaciones)."',estado=2 WHERE idPedidoVSRegistro=".$id;
		}
		
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
	
	}
	
	function obtenerObservacionesProducto ()
	{
		global $con;
		$idProducto=$_POST["idProducto"];
		$idPedido=$_POST["idPedido"];
		
		$consulta="SELECT observaciones FROM 9300_temporalPedido WHERE idPedido=".$idPedido." AND idProducto=".$idProducto;
		$observaciones=$con->obtenerValor($consulta);
		
		if($observaciones=="")
			echo "1|";
		else
			echo "2|".$observaciones;
	}
	
	function registrarPedido()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$fecha=cambiaraFechaMysql($_POST["fecha"]);
		$tipo=$_POST["tipoR"];
		$idAlmacen=$_POST["idAlmacen"];
		
		if($tipo==0)
		{
			$consulta="begin";
			
			if($con->ejecutarConsulta($consulta))
			{
				$ct=0;
				$observaciones=$_POST["observacionesF"];
				$noFactura=$_POST["noFactura"];
				$conNumeroMov="SELECT noMovimiento FROM 903_variablesSistema for update";
				$ultimoNumero=$con->obtenerValor($conNumeroMov);
				$nuevoNumero=$ultimoNumero+1;
				
				$query[$ct]="UPDATE 903_variablesSistema SET noMovimiento=".$nuevoNumero;
				$ct++;
				$query[$ct]="commit";
				if($con->ejecutarBloque($query))
				{
					$consulta2="begin";
					if($con->ejecutarConsulta($consulta2))
					{
						$x=0;
						
						$query[$x]="UPDATE 9102_PedidoCabecera SET fechaRecibido='".$fecha."',status_pedido=".$tipo.",noMovimiento=".$nuevoNumero.",observaciones='".cv($observaciones)."',noFactura='".cv($noFactura)."' WHERE idPedido=".$idPedido;
						$x++;
						
						$consultaD="SELECT idPedidoDetalle,idProducto FROM 9103_PedidoDetalle WHERE idPedido=".$idPedido;
						$res=$con->obtenerFilas($consultaD);
						while($fila=mysql_fetch_row($res))
						{
							$conExistencia="SELECT codigoUnidad,idPrograma,partida,cantidad FROM 9301_distribucionProducto WHERE idPedidoDetalle=".$fila[0];
							$resE=$con->obtenerFilas($conExistencia);
							while($filaE=mysql_fetch_row($resE))
							{
								$query[$x]="INSERT INTO 9302_existenciaAlmacen (idAlmacen,idPedido,idProducto,codigoUnidad,idPrograma,partida,cantidad,fechaMovimiento,operacion,responsable,tipoMovimiento,noMovimiento,idPedidoDetalle)
											VALUES(".$idAlmacen.",".$idPedido.",".$fila[1].",'".$filaE[0]."',".$filaE[1].",".$filaE[2].",".$filaE[3].",'".$fecha."',1,".$_SESSION["idUsr"].",1,".$nuevoNumero.",".$fila[0].")";
								$x++;	
							}
						}
						$query[$x]="commit";
						
						if($con->ejecutarBloque($query))
							echo "1|";
						else
							echo "|";
					}
				}
			}
		}
		else
		{
			//$conNoMov="SELECT noMovimiento FROM 9102_PedidoCabecera WHERE idPedido=".$idPedido;
//			$noMov=$con->obtenerValor($conNoMov);
//			if($noMov=="")
//			{
//				echo "1|";
//			}
//			else
//			{
//				$conNumeroN="begin";
//				if($con->ejecutarConsulta($conNumeroN))
//				{
//					$ct=0;
//					$conNumeroMov="SELECT noMovimiento FROM 903_variablesSistema for update";
//					$ultimoNumero=$con->obtenerValor($conNumeroMov);
//					$nuevoNumero=$ultimoNumero+1;
//					
//					$query1[$ct]="UPDATE 903_variablesSistema SET noMovimiento=".$nuevoNumero;
//					$ct++;
//					$query1[$ct]="commit";
//					
//					if($con->ejecutarBloque($query1))
//					{
//						$consulta="begin";
//						if($con->ejecutarConsulta($consulta))
//						{
//							  $z=0;
//							  $conExistencias="SELECT *FROM 9302_existenciaAlmacen WHERE noMovimiento=".$noMov;
//							  $res=$con->obtenerFilas($conExistencias);
//							  
//							  while($filaC=mysql_fetch_row($res))
//							  {
//								  $query[$z]="INSERT INTO 9302_existenciaAlmacen (idAlmacen,idPedido,idProducto,codigoUnidad,idPrograma,partida,cantidad,fechaMovimiento,operacion,responsable,tipoMovimiento,noMovimiento,idPedidoDetalle)
//											  VALUES(".$filaC[1].",".$filaC[2].",".$filaC[3].",'".$filaC[4]."',".$filaC[5].",".$filaC[6].",".$filaC[7].",'".$fecha."',-1,".$_SESSION["idUsr"].",1,".$nuevoNumero.",".$filaC[15].")";
//								  //echo $query[$x];
//								  $z++;	
//							  }
//							  $query[$z]="UPDATE 9102_PedidoCabecera SET fechaRecibido='".$fecha."',status_pedido=".$tipo." WHERE idPedido=".$idPedido;
//							  $z++;
//							  $query[$z]="commit";
//							  
//							  if($con->ejecutarBloque($query))
//								  echo "1|";
//							  else
//								  echo "|";
//						}
//						else
//						{
//							echo "|";
//						}
//					}
//					else
//					{
//						echo "|";
//					}
//				}
//			}
			
			$query="UPDATE 9102_PedidoCabecera SET fechaRecibido='".$fecha."',status_pedido=".$tipo." WHERE idPedido=".$idPedido;
			if($con->ejecutarConsulta($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function obtenerPedidosValidacion()
	{
		global $con;
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idPedido,folioPedido,txtRazonSocial2,fechaRecepcion,idProveedor_ult,fechaRecibido,idAlmacen 
				   FROM 9102_PedidoCabecera p, _405_tablaDinamica pr
				   WHERE idProveedor_ult=id__405_tablaDinamica AND status_pedido=3 ".$condWhere." ORDER BY  fechaRecepcion ASC";
		$res=$con->obtenerFilas($consulta);	
		$nreg=$con->filasAfectadas;
	
		$arrPedidos="";
		$ct=1;
		while($fila=mysql_fetch_row($res))
		{
			$obj='{"idPedido":"'.$fila[0].'","folioPedido":"'.$fila[1].'","txtRazonSocial2":"'.$fila[2].'","fechaRecepcion":"'.$fila[3].'","idProveedor_ult":"'.$fila[4].'","fechaRecibido":"'.$fila[5].'","idAlmacen":"'.$fila[6].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	}
	
	function cancelarPedido()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$fecha=cambiaraFechaMysql($_POST["fecha"]);
		$motivo=$_POST["motivo"];
		
		$query="UPDATE 9102_PedidoCabecera SET fechaRecibido='".$fecha."',status_pedido=2,observaciones='".$motivo."' WHERE idPedido=".$idPedido;
		
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
			
	}
	
	function borrarRegistroPedido()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$fecha=date("Y-m-d");
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			$conNumeroMov="SELECT noMovimiento FROM 903_variablesSistema for update";
			$ultimoNumero=$con->obtenerValor($conNumeroMov);
			$nuevoNumero=$ultimoNumero+1;
			
			$query[$ct]="UPDATE 903_variablesSistema SET noMovimiento=".$nuevoNumero;
			$ct++;
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
			{
				$conNumero="SELECT noMovimiento FROM 9102_PedidoCabecera WHERE idPedido=".$idPedido;
				$numero=$con->obtenerValor($conNumero);
				if($numero=="")
					$numero=0;
					
				$consulta2="begin";
				if($con->ejecutarConsulta($consulta2))
				{
					$ct=0;
					
					$query[$ct]="UPDATE 9102_PedidoCabecera SET noMovimiento=".$nuevoNumero.",status_pedido=1 WHERE idPedido=".$idPedido;
					$ct++;
					
					$conElementos="SELECT *FROM 9302_existenciaAlmacen WHERE noMovimiento=".$numero;
					$res=$con->obtenerFilas($conElementos);
					
					while($fila=mysql_fetch_row($res))
					{
						$query[$ct]="INSERT INTO 9302_existenciaAlmacen (idAlmacen,idPedido,idProducto,codigoUnidad,idPrograma,partida,cantidad,fechaMovimiento,operacion,responsable,tipoMovimiento,noMovimiento,idPedidoDetalle)
													VALUES(".$fila[1].",".$fila[2].",'".$fila[3]."',".$fila[4].",".$fila[5].",".$fila[6].",".$fila[7].",'".$fecha."',-1,".$_SESSION["idUsr"].",1,".$nuevoNumero.",".$fila[15].")";
						$ct++;	
					}
			
					$query[$ct]="DELETE FROM 9300_temporalPedido WHERE idPedido=".$idPedido;
					$ct++;
					$query[$ct]="commit";
					if($con->ejecutarBloque($query))
						echo "1|";
					else
						echo "|";
				}
			}
		}
	}
	
	function obtenerCategoriasAlmacen()
	{
		global $con;
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idCategoria,nombre,descripcion FROM 9030_categoriasAlmacen ".$condWhere." order by nombre";
		$res=$con->obtenerFilas($consulta);
		$nreg=$con->filasAfectadas;
		
		$arrCategorias="";
		while($fila=mysql_fetch_row($res))
		{
			$arregloConc='';
			$cabeceraT='<table>';
			$cuerpo='';
			$existeCabecera=0;
			$cuerpoT='';
			$conConceptos="SELECT claveConcepto FROM 9351_categoriaVSConcepto WHERE idCategoria=".$fila[0];
			$resC=$con->obtenerFilas($conConceptos);
			$numDist=$con->filasAfectadas;
			if($numDist>0)
			{
				$cuerpoT='<tr><td width=\"50\" class=\"letraRojaSubrayada8\">Clave</td><td width=\"450\" class=\"letraRojaSubrayada8\">Concepto</td></tr>';
			}
			
			while($filaC=mysql_fetch_row($resC))
			{
				$conN="SELECT clave,nombreObjetoGasto FROM 507_objetosGasto WHERE clave='".$filaC[0]."'";
				$datos=$con->obtenerPrimeraFila($conN);
				$cuerpo.='<tr><td width=\"50\">'.$datos[0].'</td><td width=\"450\">'.$datos[1].'</td></tr>';
			
			}
			
			$finalT='</table>';
			
			$arregloConc=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
			
			$trDes='<table><tr><td width=\"400\">'.cv($fila[2]).'</td></tr></table>';
			
			
			$obj='{"idCategoria":"'.$fila[0].'","nombre":"'.$fila[1].'","descripcion":"'.$trDes.'","conceptos":"'.$arregloConc.'"}';	
			if($arrCategorias=="")
				$arrCategorias=$obj;
			else
				$arrCategorias.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrCategorias.']}';
		echo $obj;
	
	}
	
	function obtenerFolioPedido()
	{
		global $con;
		
		$consulta="obtener ultimo folio";
		
		echo "1|2000001";
	}
	
	function guardarNuevoPedido()
	{
		global $con;
		
		$idPedidoAnt=$_POST["idPedido"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		$idProveedor=$_POST["idProveedor"];
		$nuevoFolio=$_POST["nuevoFolio"];
		$fecha=$_POST["fecha"];
		$fecha=cambiaraFechaMysql($fecha);
		$idAlmacen=$_POST["idAlmacen"];
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			
			$consulta2="INSERT INTO 9102_PedidoCabecera(idProveedor_ult,fechaPedido,status_pedido,folioPedido,fechaRecepcion,idAlmacen,idPadre)
						 VALUES(".$idProveedor.",'".date("Y-m-d")."',1,".$nuevoFolio.",'".$fecha."',".$idAlmacen.",".$idPedidoAnt.")";
			if($con->ejecutarConsulta($consulta2))			 
			{
				$idNuevoPedido=$con->obtenerUltimoID();
				
				for($x=0;$x<$tamano;$x++)
				{
					$consulta3="SELECT idMarca,cantidad,costoUnitario,iva,partida,idPedidoDetalle FROM 9103_PedidoDetalle WHERE idPedido=".$idPedidoAnt." AND idProducto=".$arreglo[$x];
					$fila=$con->obtenerPrimeraFila($consulta3);
					if($fila)
					{
						$query[$ct]="INSERT INTO 9103_PedidoDetalle(idPedido,idProducto,idMarca,cantidad,costoUnitario,iva,partida) 
									 VALUES(".$idNuevoPedido.",".$arreglo[$x].",".$fila[0].",".$fila[1].",".$fila[2].",".$fila[3].",".$fila[4].")";
						$ct++;
						if($con->ejecutarConsulta($query[$ct]))
						{
							$ultimoId=$con->obtenerUltimoID();
							
							$conDistrib="SELECT codigoUnidad,idPrograma,partida,cantidad FROM 9301_distribucionProducto WHERE idPedidoDetalle=".$fila[5];
							$filaD=$con->obtenerPrimeraFila($conDistrib);
							if($filaD)
							{
								$query[$ct]="INSERT INTO 9301_distribucionProducto (codigoUnidad,idPrograma,partida,cantidad) 
											 VALUES(".$filaD[0].",".$filaD[1].",".$fila[2].",".$filaD[3].")";
								$ct++;			 
							}
						}
					}
				}
				$query[$ct]="commit";
				if($con->ejecutarBloque($query))
					echo "1|";
				else
					echo "|";
			}
						 
		}
	}
	
	function obtenerSanciones()
	{
		global $con;
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT idSancion,nombre,s.descripcion,nombreConsulta,idConsulta FROM 991_consultasSql c,9311_sanciones s WHERE idConsulta=idCalculo ".$condWhere." order by nombre";
		$res=$con->obtenerFilas($consulta);
		$nreg=$con->filasAfectadas;
		
		$arrSanciones="";
		while($fila=mysql_fetch_row($res))
		{
			$obj='{"idSancion":"'.$fila[0].'","nombre":"'.$fila[1].'","descripcion":"'.$fila[2].'","nombreConsulta":"'.$fila[3].'","idConsulta":"'.$fila[3].'"}';	
			if($arrSanciones=="")
				$arrSanciones=$obj;
			else
				$arrSanciones.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrSanciones.']}';
		echo $obj;
	}
	
	function obtenerDatosSancion()
	{
		global $con;
		
		$id=$_POST["id"];
		
		$consulta="SELECT idSancion,nombre,s.descripcion,nombreConsulta,idConsulta FROM 991_consultasSql c,9311_sanciones s WHERE idCalculo=idConsulta AND idSancion=".$id;
		$fila=$con->obtenerPrimeraFila($consulta);
		if($fila)
		{
			echo "2|".$fila[1]."|".$fila[2]."|".$fila[4];
		}
		else
		{
			echo "1|";
		}
		
	}
	
	function operacionesSancion()
	{
		global $con;
		
		$id=$_POST["id"];
		$nombre=$_POST["nombre"];
		$descripcion=$_POST["descripcion"];
		$idCalculo=$_POST["idCalculo"];
		if($id=="-1")
		{
			$query="INSERT INTO 9311_sanciones(nombre,descripcion,idCalculo) VALUES ('".$nombre."','".$descripcion."',".$idCalculo.")";
		}
		else
		{
			$query="UPDATE 9311_sanciones SET nombre='".$nombre."',descripcion='".$descripcion."',idCalculo=".$idCalculo." WHERE idSancion=".$id;
		}
		
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
	}
	
	function eliminarSancion()
	{
		global $con;
		
		$query="DELETE FROM 9311_sanciones  WHERE idSancion=".$id;
		
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
	}
	
	function obtenerConceptosLibres()
	{
		global $con;
		
		$conExisten="SELECT claveConcepto FROM 9351_categoriaVSConcepto";
		$cadena=$con->obtenerListaValores($conExisten);
		
		if($cadena=="")
		{
			$cadena="-1";
		}
		
		$consulta="SELECT clave,nombreObjetoGasto FROM 507_objetosGasto WHERE clave LIKE '%00' and clave not like '%000'  and clave not in(".$cadena.")";
		$res=$con->obtenerFilas($consulta);
		$nreg=$con->filasAfectadas;
		
		$arreConceptos="";
		while($fila=mysql_fetch_row($res))
		{
			$obj='{"clave":"'.$fila[0].'","nombreObjetoGasto":"'.$fila[1].'"}';	
			if($arreConceptos=="")
				$arreConceptos=$obj;
			else
				$arreConceptos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arreConceptos.']}';
		echo $obj;
	
	}
	
	function eliminarCategoria()
	{
		global $con;
		
		$id=$_POST["id"];
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			$query[$ct]="DELETE FROM 9351_categoriaVSConcepto WHERE idCategoria=".$id;
			$ct++;
			$query[$ct]="DELETE FROM 9030_categoriasAlmacen WHERE idCategoria=".$id;
			$ct++;
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function guardarCategoria()
	{
		global $con;
		
		$id=$_POST["id"];
		$nombre=$_POST["nombre"];
		$descripcion=$_POST["descripcion"];
		$responsablealmacen=$_POST["responsablealmacen"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		$ct=0;
		$consulta="begin";
		if($con->ejecutarconsulta($consulta))
		{
			if($id=="-1")
			{
				
				$query[$ct]="INSERT INTO 9030_categoriasAlmacen (nombre, descripcion, idResp, fechaCreacion) VALUES ('".$nombre."', '".descripcion."', '".$_SESSION["idUsr"]."','".date('Y-m-d')."')";
				if($con->ejecutarConsulta($query[$ct]))
				{
					$ct++;
					$idCat=$con->obtenerUltimoID();
					for($x=0;$x<$tamano;$x++)
					{
						$query[$ct]="INSERT INTO 9351_categoriaVSConcepto(idCategoria,claveConcepto) VALUES(".$idCat.",".$arreglo[$x].")";
						$ct++;
					}
				}
			}
			else
			{
				$query[$ct]="UPDATE 9030_categoriasAlmacen SET nombre='".$nombre."',descripcion='".$descripcion."',idRespModif=".$_SESSION["idUsr"].",fechaModificacion='".date('Y-m-d')."' WHERE idCategoria=".$id;
				$ct++;
				$query[$ct]="DELETE FROM 9351_categoriaVSConcepto WHERE idCategoria=".$id;
				$ct++;
				
				for($x=0;$x<$tamano;$x++)
				{
					$query[$ct]="INSERT INTO 9351_categoriaVSConcepto(idCategoria,claveConcepto) VALUES(".$id.",".$arreglo[$x].")";
					$ct++;
				}
			}
			
			if(!$con->ejecutarBloque($query))			
				echo "1|";
			else
				echo "|";
		}
	}
	
	function guardarAlmacen()
	{
		global $con;
		
		$id=$_POST["id"];
		$nombre=$_POST["nombre"];
		$descripcion=$_POST["descripcion"];
		if($id=="-1")
		{
			$query="INSERT INTO 9030_almacenes (nombreAlmacen,descripcion,idResp,fechaCreacion) VALUES('".$nombre."','".$descripcion."',".$_SESSION["idUsr"].",'".date('Y-m-d')."')";
		}
		else
		{
			$query="UPDATE 9030_almacenes SET nombreAlmacen='".$nombre."',descripcion='".$descripcion."',idRespModif=".$_SESSION["idUsr"].",fechaModificacion='".date('Y-m-d')."' WHERE idAlmacen=".$id;
		}
		
		if($con->ejecutarConsulta($query))
		{
			if($id=="-1")
				$id=$con->obtenerUltimoID();
			echo "1|".$id;
		}
		else
			echo "|";
	}
	
	function obtenerCatAlmacen()
	{
		global $con;
		
		$idAlmacen=$_POST["idAlmacen"];
		
		$consulta="SELECT idAlmacenVSCategoria,nombre,descripcion FROM 9303_almacenVSCategoria a,9030_categoriasAlmacen c WHERE a.idCategoria=c.idCategoria AND idAlmacen=".$idAlmacen." order by nombre";
		$res=$con->obtenerFilas($consulta);
		$numReg=$con->filasAfectadas;
		$arrCatAlm="";
		
		while($fila=mysql_fetch_row($res))
		{
			$arregloConc='';
			$cabeceraT='<table>';
			$cuerpo='';
			$existeCabecera=0;
			$cuerpoT='';
			$conConceptos="SELECT claveConcepto FROM 9351_categoriaVSConcepto WHERE idCategoria=".$fila[0];
			$resC=$con->obtenerFilas($conConceptos);
			$numDist=$con->filasAfectadas;
			if($numDist>0)
			{
				$cuerpoT='<tr><td width=\"50\" class=\"letraRojaSubrayada8\">Clave</td><td width=\"450\" class=\"letraRojaSubrayada8\">Concepto</td></tr>';
			}
			while($filaC=mysql_fetch_row($resC))
			{
				$conN="SELECT clave,nombreObjetoGasto FROM 507_objetosGasto WHERE clave='".$filaC[0]."'";
				$datos=$con->obtenerPrimeraFila($conN);
				$cuerpo.='<tr><td width=\"50\">'.$datos[0].'</td><td width=\"450\">'.$datos[1].'</td></tr>';
			
			}
			$finalT='</table>';
			
			$arregloConc=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
			
			$trDes='<table><tr><td width=\"400\">'.cv($fila[2]).'</td></tr></table>';
			
			$obj='{"idAlmacenVSCategoria":"'.$fila[0].'","nombre":"'.$fila[1].'","conceptos":"'.$arregloConc.'","descripcion":"'.$trDes.'"}';	
			if($arrCatAlm=="")
				$arrCatAlm=$obj;
			else
				$arrCatAlm.=",".$obj;
		}
			
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrCatAlm.']}';
		echo $obj;
	}
	
	function obtenerCatNoAlmacen()
	{
		global $con;
		$idAlmacen=$_POST["idAlmacen"];
		
		$consulta="SELECT idCategoria,nombre FROM 9030_categoriasAlmacen WHERE idCategoria NOT IN (SELECT idCategoria FROM 9303_almacenVSCategoria WHERE idAlmacen=".$idAlmacen.") order by nombre";
		$res=$con->obtenerFilas($consulta);
		$numReg=$con->filasAfectadas;
		$arrCatAlm="";
		
		while($fila=mysql_fetch_row($res)){
		$obj='{"idCategoria":"'.$fila[0].'","nombre":"'.$fila[1].'"}';	
			if($arrCatAlm=="")
				$arrCatAlm=$obj;
			else
				$arrCatAlm.=",".$obj;
			}
			
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrCatAlm.']}';
		echo $obj;
	}
	
	function ligarCategoriaAlmacen()
	{
		global $con;
		
		$idAlmacen=$_POST["idAlmacen"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
			{
				$query[$ct]="INSERT INTO 9303_almacenVSCategoria (idAlmacen,idCategoria) VALUES(".$idAlmacen.",".$arreglo[$x].")";
				$ct++;
			}
			
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function obtenerInventarioAlmacen()
	{
		global $con;
		$arrProductos="";
		$consultaUnion="";
		$idAlmacen=$_POST["idAlmacen"];
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		$consulta="SELECT idCategoria FROM 9303_almacenVSCategoria WHERE idAlmacen=".$idAlmacen;
		$cadena=$con->obtenerListaValores($consulta);
		if($cadena=="")
		{
			$numReg=0;
			$arrProductos="";
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			echo $obj;
		}
		else
		{
			$conConceptos="SELECT claveConcepto,codigoControl FROM 9351_categoriaVSConcepto c,507_objetosGasto o WHERE claveConcepto=clave AND  idCategoria IN (".$cadena.") ORDER BY claveConcepto";
			$res=$con->obtenerFilas($conConceptos);
			$nCat=$con->filasAfectadas;
			if($nCat>0)
			{
				$ct=0;
				if($nCat==1)
				{
					$filaC=$con->obtenerPrimeraFila($conConceptos);
					$conConcentrado="SELECT clave,idProducto,clave_Art,nombreProducto,descripcion FROM 9101_CatalogoProducto p,507_objetosGasto o WHERE  codigoControlPadre='".$filaC[1]."' AND clave=objetoGasto ".$condWhere." ORDER BY clave,nombreProducto limit ".$inicio.",".$cantidad;
					
					$conSinLimit="SELECT clave,idProducto,clave_Art,nombreProducto,descripcion FROM 9101_CatalogoProducto p,507_objetosGasto o WHERE  codigoControlPadre='".$filaC[1]."' AND clave=objetoGasto ".$condWhere." ORDER BY clave,nombreProducto";
				}
				else
				{
				  while($filaU=mysql_fetch_row($res))
				  {
					  $union="(SELECT clave,idProducto,clave_Art,nombreProducto,descripcion FROM 9101_CatalogoProducto p,507_objetosGasto o WHERE  codigoControlPadre='".$filaU[1]."' AND clave=objetoGasto ".$condWhere.")"; 
					  if($consultaUnion=="")
					  	$consultaUnion=$union;
					  else
					  	$consultaUnion.="UNION ".$union;
				  }
				  $conConcentrado=$consultaUnion." ORDER BY clave,nombreProducto limit ".$inicio.",".$cantidad;
				  $conSinLimit=$consultaUnion;
				}
				$respuesta=$con->obtenerFilas($conConcentrado);
				$resNFilas=$con->obtenerFilas($conSinLimit);
				$nreg=$con->filasAfectadas;
				
					while($filaP=mysql_fetch_row($respuesta))
					{
						$existencia=obtenerExistenciaAlmacenProductoE($filaP[1],$idAlmacen);
						
						$obj='{"clave":"'.cv($filaP[0]).'","idProducto":"'.$filaP[1].'","clave_Art":"'.cv($filaP[2]).'","nombreProducto":"'.cv($filaP[3]).'","existencia":"'.$existencia.'","descripcion":"'.cv($filaP[4]).'"}';	
						if($arrProductos=="")
							$arrProductos=$obj;
						else
							$arrProductos.=",".$obj;
						$ct++;
					}
				$obj='{"numReg":"'.$nreg.'","registros":['.$arrProductos.']}';
				
				echo $obj;
			}
		}
	}
	
	function obtenerExistenciaAlmacenProductoE($idProducto,$idAlmacen)
	{
		global  $con;
		
		$consulta="SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idProducto=".$idProducto." AND idAlmacen=".$idAlmacen;
		$existencia=$con->obtenerValor($consulta);
		if($existencia=="")
			$existencia=0;
			
		return $existencia;	
	}
	
	function obtenerTablaReparto($idProducto,$idAlmacen)
	{
		global $con;
		
		$arregloConc='';
		$cabeceraT='<table>';
		$cuerpoT='';
		$cuerpo='';
		$consulta="SELECT idPrograma,codigoUnidad,idPrograma,(SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idProducto=e.idProducto AND idAlmacen=".$idAlmacen." AND 
				   codigoUnidad=e.codigoUnidad AND idPrograma=e.idPrograma )AS cantidad FROM 9302_existenciaAlmacen e WHERE idProducto=".$idProducto;
		$res=$con->obtenerFilas($consulta);
		$nFilas=$con->filasAfectadas;
		if($nFilas>0)
		{
			$existeCabecera=0;
			$cuerpoT='<tr><td align="center" width="300" class="letraRojaSubrayada8">Departamento</td><td align="center"  width="300" class="letraRojaSubrayada8">Programa</td><td width="50" align="center">Cantidad</td></tr>';
		}
		$cadenaTabla="";
		
		while($fila=mysql_fetch_row($res))
		{
			if($fila[0]>0)
			{
				$consulta="SELECT codigoDepto,unidad FROM 817_organigrama WHERE codigoUnidad='".$fila[1]."'";
				$depto=$con->obtenerPrimeraFila($consulta);
		
				$conP="SELECT tituloPrograma FROM 517_programas WHERE idPrograma=".$fila[0];
				$prog=$con->obtenerValor($conP);
				
				$cuerpo.='<tr><td width="300">['.$depto[0].']&nbsp;&nbsp;'.$depto[1].'</td><td width="300">'.$prog.'</td><td width="50">&nbsp;&nbsp;'.$fila[3].'</td></tr>';
			}
		}
		$finalT='</table>';
		$arregloConc=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
		return $arregloConc;
	}
	
	function agendarFechaEntrega()
	{
	
		global $con;
		
		$idPedido=$_POST["idPedido"];
		$fechaAgendada=$_POST["fechaAgendada"];
		$horaInicio=$_POST["horaInicio"];
		$horaI=date('H:i:s',strtotime($horaInicio));
		$tiempoE=$_POST["tiempoE"];
		$horaFin=strtotime('+ '.$tiempoE.' minutes',strtotime($horaI));
		$horaF=date('H:i:s',$horaFin);
		$query="UPDATE 9102_PedidoCabecera SET fechaAgenda='".$fechaAgendada."',horaInicio='".$horaI."',horaFin='".$horaF."' WHERE idPedido=".$idPedido;
		if($con->ejecutarConsulta($query))
			echo "1|";
		else
			echo "|";
	}
	
	
	function obtenerSeccionesAlmacen()
	{
		global $con;
		global $mostrarXML;
		$mostrarXML=false;
		
		if(isset($_POST["idAlmacen"]))
			$idAlmacen=$_POST["idAlmacen"];
		
		$cadTemas=generarArbolSecciones($idAlmacen,"",-1);
		echo $cadTemas;
	}
	
	function generarArbolSecciones($id,$numeracion,$idPadre)
	{
	   global $con;
	   $cadena="";
	   $consulta="select idSeccion,nombreSeccion,descripcion from 9031_seccionesAlmacen where idAlmacen=".$id." and idPadre=".$idPadre;
	   $res2=$con->obtenerFilas($consulta);
		 
		 $clase="filaBlanca10";
		 $ct=1;
		 while($fila=mysql_fetch_row($res2))
		 {
			
			  $hijos=generarArbolSecciones($id,$numeracion.$ct.".",$fila[0]);
			  
			  if ($hijos=="[]")
			  {
				  $obj="{
								  id:".$fila[0].",
								  text: '".$numeracion.$ct.".- ".$fila[1]."&nbsp;&nbsp;<img src=\"../images/icon_code.gif\" title=\"Descripci&oacute;n:&nbsp;".$fila[2]."\" alt=\"Descripci&oacute;n:&nbsp;".$fila[2]."\" />',
								  checked:false,
								  icon:'images/s.gif',
								  leaf: true,
								  draggable:false,
								  listeners:	{
														  
											  }
								  
						}
							   ";
			  }
			  else
			  {
				  $obj="{
								  id:".$fila[0].",
								  text: '".$numeracion.$ct.".- ".$fila[1]."&nbsp;&nbsp;<img src=\"../images/icon_code.gif\" title=\"Descripci&oacute;n:&nbsp;".$fila[2]."\" alt=\"Descripci&oacute;n:&nbsp;".$fila[2]."\" />',
								  checked:false,
								  icon:'images/s.gif',
								  draggable:false,
								  listeners:	{
														  
											  },
								  children:
											  ".$hijos."
								 }
							   ";
			  }
			  if($cadena!="")
				  $cadena.=",".$obj;
			  else
				  $cadena=$obj;
			  $ct++;				
		 }
		return "[".$cadena."]";
	}
	
	function eliminarSeccionesAlmacen()
	{
		global $con;
		global $z;
		global $consultaS;
		
		$idAlmacen=$_POST["idAlmacen"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			
			for($x=0;$x<$tamano;$x++)
			{
				seccion($arreglo[$x]);
				borrar($arreglo[$x]);
				$consultaS[$z]="DELETE FROM 9031_seccionesAlmacen WHERE idSeccion=".$arreglo[$x];
				$z++;
			}
			$consultaS[$z]="commit";
			if($con->ejecutarBloque($consultaS))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function borrar($id)
	{	
		global $con;
		global $z;
		global $consultaS;
		$consultaS[$z]="delete from 9031_seccionesAlmacen where idPadre=".$id;
		$z++;
	
	}
	
	function seccion($id)
	{
		global $con;
		$query="select idSeccion from 9031_seccionesAlmacen where idPadre=".$id;
		$resE=$con->obtenerFilas($query);
		while($fA=mysql_fetch_row($resE))
		{
			seccion($fA[0]);
			borrar($fA[0]);
		}
	}	
	
	function obtenerPartidas()
	{
		global $con;
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$arrPartidas="";
		$consulta="SELECT  codigoControl FROM 507_objetosGasto WHERE clave LIKE '%00' AND clave NOT LIKE '%000'";
		$lista=$con->obtenerListaValores($consulta);
		
		if($lista=="")
		{
			$numReg=0;
			$arrPartidas="";
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			echo $obj;
		}
		else
		{
			$conPartidas="SELECT clave,nombreObjetoGasto FROM 507_objetosGasto WHERE codigoControlPadre IN(".$lista.")".$condWhere." order by clave";
			$res=$con->obtenerFilas($conPartidas);
			$numReg=$con->filasAfectadas;
			while($fila=mysql_fetch_row($res))
			  {
			  	$consulta="SELECT *FROM 9306_confPartidaAlmacen WHERE clave='".$fila[0]."'";
				$filaC=$con->obtenerPrimeraFila($consulta);
				if(!$filaC)
				{
					$filaC[0]="";
					$filaC[1]="";
					$filaC[2]="-1";
					$filaC[3]="";
					$filaC[4]="";
					$tipoR="Sin Configuraci&oacute;n";
					$tipoI="Sin Configuraci&oacute;n";
				}
				else
				{
				  if($filaC[2]=="")
				  {
					  $filaC[2]="-1";
				  }
				  
				  $fila[3]="Sin Configuraci&oacute;n";
				  if($filaC[3]!="")
				  {
					  if($fila[3]==0)
						  $tipoR="Volumen";
					  else
						  $tipoR="Pieza";
				  }
				  
				  $fila[4]="Sin Configuraci&oacute;n";
				  if($filaC[4]!="")
				  {
					  if($fila[4]==0)
						  $tipoI="Si";
					  else
						  $tipoI="No";
				  }
				}
				
				$nombreForm="SELECT nombreFormulario FROM 900_formularios WHERE idFormulario=".$filaC[2];
				$nombre=$con->obtenerValor($nombreForm);
				if($nombre=="")
				{
					$nombre="Sin Configuraci&oacute;n";
				}
			  
				$obj='{"clave":"'.$fila[0].'","nombreObjetoGasto":"'.$fila[1].'","idFormato":"'.$filaC[2].'","tipoRegistro":"'.$filaC[3].'","inventariable":"'.$filaC[4].'","nombreF":"'.cv($nombre).'","tipoR":"'.$tipoR.'","tipoInv":"'.$tipoI.'"}';	
				if($arrPartidas=="")
					$arrPartidas=$obj;
				else
					$arrPartidas.=",".$obj;
			}
			
		$objArre='{"numReg":"'.$numReg.'","registros":['.$arrPartidas.']}';
		echo $objArre;
			
		}
	}
	
	/////Hugo //////////////
	function obtenerCategoria(){
		global $con;
		
		$consulta="SELECT idCategoria, nombre, fechaCreacion, responsable, clave, fechaModificacion, responsableModificacion FROM 9350_categoriasVsArea";
		$res=$con->obtenerFilas($consulta);
		$numReg = $con->filasAfectadas;
		$arrCategorias = "";
		while($fila=mysql_fetch_row($res)){
		$obj='{"idCategoria":"'.$fila[0].'","nombre":"'.$fila[1].'","fechaCreacion":"'.$fila[2].'","responsable":"'.$fila[3].'","clave":"'.$fila[4].'","fechaModificacion":"'.$fila[5].'","responsableModificacion":"'.$fila[6].'"}';	
			if($arrCategorias=="")
				$arrCategorias=$obj;
			else
				$arrCategorias.=",".$obj;
			}
			
		$objArre='{"numReg":"'.$numReg.'","registros":['.$arrCategorias.']}';
		echo $objArre;
	}
	
	/////////termina Hugo/////////
	
	function obtenerComprometidoEntregas()
	{
		global $con;
		$idAlmacen=$_POST["idAlmacen"];
		$idCiclo=$_POST["idCiclo"];
		$mes=$_POST["mes"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$catAlmacen="SELECT idCategoria FROM 9303_almacenVSCategoria WHERE idAlmacen=".$idAlmacen;
		//echo $catAlmacen;
		$cadenaCat=$con->obtenerListaValores($catAlmacen);
		$arrProductos="";
		
		if($cadenaCat=="")
		{
			$numReg=0;
			$arrPartidas="";
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			echo $obj;
		}
		else
		{
			$union="";
			$conConcep="SELECT claveConcepto,codigoControl FROM 9351_categoriaVSConcepto c,507_objetosGasto o WHERE  clave=claveConcepto AND idCategoria in(".$cadenaCat.")";
			//echo $conConcep;
			$resConcep=$con->obtenerFilas($conConcep);
			$nConcept=$con->filasAfectadas;
			if($nConcept==1)
			{
				
				$fila=$con->obtenerPrimeraFila($conConcep);
				
				$union="SELECT DISTINCT(a.idProducto),nombreProducto,p.descripcion FROM 9101_CatalogoProducto p,507_objetosGasto o ,525_productosAutorizados a,526_distribucionAutorizada d
						   WHERE p.idProducto=a.idProducto AND a.clave=o.clave  AND p.objetoGasto=o.clave AND codigoControlPadre='".$fila[1]."' AND idCiclo=".$idCiclo." AND a.idCodigoGastoCiclo=d.idCodigoGastoCiclo AND mes=".$mes.$condWhere;
			}
			else
			{
				
				while($fConcep=mysql_fetch_row($resConcep))
				{
					$consulta="(SELECT DISTINCT(a.idProducto),nombreProducto,p.descripcion FROM 9101_CatalogoProducto p,507_objetosGasto o ,525_productosAutorizados a,526_distribucionAutorizada d
						   WHERE p.idProducto=a.idProducto AND a.clave=o.clave  AND p.objetoGasto=o.clave AND codigoControlPadre='".$fConcep[1]."' AND idCiclo=".$idCiclo." AND a.idCodigoGastoCiclo=d.idCodigoGastoCiclo AND mes=".$mes.")";
					
					if($union=="")	   
						$union=$consulta;
					else
						$union.="UNION ".$consulta;
				
				}
			}
			//echo $union;
			$union=$union.$condWhere;
			$res=$con->obtenerFilas($union.$condWhere);
			$numReg=$con->filasAfectadas;
			
			while($filaP=mysql_fetch_row($res))
			{
				$conCadena="SELECT idCodigoGastoCiclo FROM 525_productosAutorizados WHERE idProducto=".$filaP[0]." AND idCiclo=".$idCiclo;
				$cadena=$con->obtenerListaValores($conCadena);
				if($cadena=="")
					$cadena="-1";
					
				$conComprometido="SELECT SUM(cantidad) FROM 526_distribucionAutorizada WHERE mes=".$mes." AND  idCodigoGastoCiclo IN (".$cadena.")";
				$comprometido=$con->obtenerValor($conComprometido);
				if($comprometido=="")
					$comprometido=0;
					
				$estado="";
				$conSituacion1="SELECT idEntrega  FROM 9305_fechasEntregasAlmacen WHERE idProducto=".$filaP[0]." AND mes=".$mes." AND idCiclo=".$idCiclo;
				$resS=$con->obtenerFilas($conSituacion1);
				$nEntregas=$con->filasAfectadas;
				if($nEntregas==0)
					$estado="No Entregado";
				
				$conSituacion2="SELECT SUM(cantidad) FROM 9305_fechasEntregasAlmacen WHERE idProducto=".$filaP[0]." AND (estado=0 OR estado=2) AND mes=".$mes." AND idCiclo=".$idCiclo;
				$valorEstado=$con->obtenerValor($conSituacion2);
				if($valorEstado!="")
					$estado="En Proc. Entrega";
				
				$conSituacion3="SELECT SUM(cantidad) FROM 9305_fechasEntregasAlmacen WHERE idProducto=".$filaP[0]." AND estado=1 AND mes=".$mes." AND idCiclo=".$idCiclo;
				$valorEstado1=$con->obtenerValor($conSituacion3);
				if($valorEstado1!="")
				{
					if($comprometido==valorEstado1)
						$estado="En Proc. Entrega";
				}
				
				$existencia=obtenerExistenciaAlmacenProductoE($filaP[0],$idAlmacen);
				
				$obj='{"idProducto":"'.$filaP[0].'","nombreProducto":"'.$filaP[1].'","descripcion":"'.cv($filaP[2]).'","cantidad":"'.$comprometido.'","existencia":"'.$existencia.'","situacion":"'.$estado.'"}';	
				if($arrProductos=="")
					$arrProductos=$obj;
				else
					$arrProductos.=",".$obj;
			}
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			
			echo $obj;
					
			}
	}
	
	function obtenerDeptosEntregaProducto()
	{
		global $con;
		
		$idAlmacen=$_POST["idAlmacen"];
		$idProducto=$_POST["idProducto"];
		$mes=$_POST["mes"];
		$idCiclo=$_POST["idCiclo"];
		$arrDeptos="";
		
		$consulta="SELECT a.idPrograma,tituloPrograma,codDepto,unidad,d.cantidad FROM 525_productosAutorizados a ,817_organigrama org,526_distribucionAutorizada d,517_programas pr
				   WHERE a.idProducto=".$idProducto." AND a.codDepto=org.codigoUnidad AND idCiclo=".$idCiclo." AND a.idCodigoGastoCiclo=d.idCodigoGastoCiclo AND a.idPrograma=pr.idPrograma AND mes=".$mes." order by unidad";	
		$res=$con->obtenerFilas($consulta);
		$numReg=$con->filasAfectadas;
		
		while($filaP=mysql_fetch_row($res))
		{
			$existenciaDepto=obtenerExistenciaDepto($filaP[0],$filaP[2],$idProducto,$idAlmacen);
			
			$conEntregas="SELECT fecha,estado,cantidad FROM 9305_fechasEntregasAlmacen WHERE  idProducto=".$idProducto." AND idAlmacen=".$idAlmacen;
			$resE=$con->obtenerFilas($conEntregas);
			$diferencia=0;
			$sumaE=0;
			$totalE=0;
			$totalP=0;
			$totalA=0;
			$sinA=0;
			while($filaE=mysql_fetch_row($resE))
			{
				$datos=explode("-",$filaE[0]);
				$mesE=$datos[1];
				$cicloE=$datos[0];
				if(($mesE==$mes) && ($cicloE==$idCiclo))
				{
					if(($filaE[1]==1) || ($filaE[1]==2))
					{
						$sumaE=$sumaE+$filaE[2];
					}
					
					if($filaE[1]==1)
					{
						$totalE=$totalE+$filaE[2];
					}
					
					if($filaE[1]==2)
					{
						$totalP=$totalP+$filaP[2];
					}
					
					if($filaE[1]==0)
					{
						$totalA=$totalA+$filaP[2];
					}
				}
			}
			
			$diferencia=$filaP[4]-$sumaE;
			if($existenciaDepto!=0)
			{
				$existenciaDepto=$existenciaDepto-$totalA;
			}
			
			
			$obj='{"idPrograma":"'.$filaP[0].'","tituloPrograma":"'.$filaP[1].'","codigoUnidad":"'.$filaP[2].'","cantidadSolicitada":"'.$filaP[4].'","entregados":"'.$sumaE.'","diferencia":"'.$diferencia.'","unidad":"'.$filaP[3].'","totalE":"'.$totalE.'","totalP":"'.$totalP.'","totalA":"'.$totalA.'","existencia":"'.$existenciaDepto.'"}';	
			if($arrDeptos=="")
				$arrDeptos=$obj;
			else
				$arrDeptos.=",".$obj;
		}
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrDeptos.']}';
		
		echo $obj;
	}
	
	function guardarEntregaAlmacen()
	{
		global $con;
		
		
		$idAlmacen=$_POST["idAlmacen"];
		$cadena=$_POST["cadena"];
		$arreglo=explode(",",$cadena);
		$tamano=sizeof($arreglo);
		$idProducto=$_POST["idProducto"];
		$mes=$_POST["mes"];
		$idCiclo=$_POST["idCiclo"];
		
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$ct=0;
			for($x=0;$x<$tamano;$x++)
			{
				$fila=explode("_",$arreglo[$x]);
				$horaFin="00:00:00";
				$horaInicio=$fila[4];
				$tiempoE=$fila[5];
				if($horaInicio!=0)
				{
					$horaInicio=date('H:i:s',strtotime($horaInicio));
					$horaI=strtotime($fila[3].$horaInicio);
					$horaFin=strtotime('+ '.$tiempoE.' minutes',$horaI);
					$horaFin=date('H:i:s',$horaFin);
				}
				else
				{
					$horaInicio=date('H:i:s',strtotime($horaInicio));
				}
				
				$query[$ct]="INSERT INTO 9305_fechasEntregasAlmacen (idAlmacen,idProducto,codigoUnidad,idPrograma,cantidad,fecha,horaInicio,horaFin,estado,mes,idCiclo)
							VALUES(".$idAlmacen.",".$idProducto.",'".$fila[0]."',".$fila[1].",".$fila[2].",'".$fila[3]."','".$horaInicio."','".$horaFin."',0,".$mes.",".$idCiclo.")";
				$ct++;			
			}
			$query[$ct]="commit";
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function validarFechaTiempoEstimado()
	{
		global $con;
		
		$fecha=$_POST["fecha"];
		$horaInicio=$_POST["horaInicio"];
		$tiempo=$_POST["tiempo"];
		
		$horaInicio=date('H:i:s',strtotime($horaInicio));
		$fechaTiempo=strtotime($fecha.$horaInicio);
		$fechaFin=strtotime('+ '.$tiempo.' minutes',$fechaTiempo);
		$fechaFin=date('Y-m-d',$fechaFin);
		
		if($fecha==$fechaFin)
			echo "1|" ;
		else
			echo "2|";
	}
	
	function obtenerExistenciaDepto($idPrograma,$codigoUnidad,$idProducto,$idAlmacen)
	{
		global $con;
		
		$consulta="SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idProducto=".$idProducto." AND idPrograma=".$idPrograma." AND codigoUnidad='".$codigoUnidad."' AND idAlmacen=".$idAlmacen;
		$existencia=$con->obtenerValor($consulta);
		
		if($existencia=="")
		{
			$existencia=0;
		}
		
		return $existencia;
	}
	
	function guardarConfPartidas()
	{
		global $con;
		
		$cadena=$_POST["cadena"];
		$tipoF=$_POST["tipoF"];
		$tipoR=$_POST["tipoR"];
		$tipoI=$_POST["tipoI"];
		$consulta="begin";
		if($con->ejecutarConsulta($consulta))
		{
			$x=0;
			
			$arreglo=explode(",",$cadena);
			$tamano=sizeof($arreglo);
			for($y=0;$y<$tamano;$y++)
			{
				$conExiste="SELECT idPartidaVSFormularioD FROM 9306_confPartidaAlmacen WHERE clave='".$arreglo[$y]."'";
				$id=$con->obtenerValor($conExiste);
				if($id=="")
				{
					$query[$x]="INSERT INTO 9306_confPartidaAlmacen(clave,idFormulario,tipoRegistro,inventariable) VALUES('".$arreglo[$y]."',".$tipoF.",".$tipoR.",".$tipoI.")";
				}
				else
				{
					$query[$x]="UPDATE 9306_confPartidaAlmacen SET clave='".$arreglo[$y]."',idFormulario=".$tipoF.",tipoRegistro=".$tipoR.",inventariable=".$tipoI." WHERE idPartidaVSFormularioD=".$id;
				}
				$x++;
			}
			$query[$x]="commit";
			
			if($con->ejecutarBloque($query))
				echo "1|";
			else
				echo "|";
		}
	}
	
	function obtenerConfiguracionProducto()
	{
		global $con;
		$idProducto=$_POST["idProducto"];
		
		$conObjGasto="SELECT objetoGasto FROM 9101_CatalogoProducto WHERE idProducto=".$idProducto;
		$objGasto=$con->obtenerValor($conObjGasto);
		if($objGasto=="")
		{
			echo "2|";
		}
		else
		{
			$conClavePadre="SELECT clave FROM 507_objetosGasto WHERE clave=".$objGasto;
			$codPadre=$con->obtenerValor($conClavePadre);
			if($codPadre=="")
			{
				echo "2|";
			}
			else
			{
				$consulta="SELECT *FROM 9306_confPartidaAlmacen WHERE clave=".$codPadre;
				$fila=$con->obtenerPrimeraFila($consulta);
				if(!$fila)
				{
					echo "2|";
				}
				else
				{
					echo "1|".$fila[2]."|".$fila[3]."|".$fila[4];
				}
			}
		}
	}
	
	function obtenerRegistrosForDin()
	{
		global $con;
		
		$idFormulario=$_POST["idFormulario"];
		$idPedidoDetalle=$_POST["idPedidoDetalle"];
		$nombreP=bD($_POST["nombreProducto"]);
		$marca=bD($_POST["marca"]);
		$modelo=bD($_POST["modelo"]);
		$tipoRegistro=$_POST["tipoRegistro"];
		$inventariable=$_POST["inventariable"];
		$cantidad=$_POST["cantidad"];
		
		$arrProductos="";
		$consulta="SELECT nombreTabla FROM 900_formularios  WHERE idFormulario=".$idFormulario;
		$nTabla=$con->obtenerValor($consulta);
		if($nTabla=="")
		{
			$numReg=0;
			$arrPartidas="";
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			echo $obj;
		}
		else
		{
			$arrAux="";
			$consulta="SELECT id_".$nTabla." FROM  ".$nTabla." WHERE idReferencia=".$idPedidoDetalle;
			$res=$con->obtenerFilas($consulta);
			$numReg=$con->filasAfectadas;
			if($numReg!=$cantidad)
			{
				$tamano=$cantidad-$numReg;
				
				for($x=0;$x<$tamano;$x++)
				{
					$objAux='{"idFormulario":"'.$idFormulario.'","idPedidoDetalle":"'.$idPedidoDetalle.'","nombreProducto":"'.cv($nombreP).'","marca":"'.$marca.'","modelo":"'.$modelo.'","tipoRegistro":"'.$tipoRegistro.'","inventariable":"'.$inventariable.'","idRegistro":"-1"}';
					if($arrAux=="")
					    $arrAux=$objAux;
					else
					    $arrAux.=",".$objAux;
				}
			}
			
			while($fila=mysql_fetch_row($res))
			{
				$obj='{"idFormulario":"'.$idFormulario.'","idPedidoDetalle":"'.$idPedidoDetalle.'","nombreProducto":"'.cv($nombreP).'","marca":"'.$marca.'","modelo":"'.$modelo.'","tipoRegistro":"'.$tipoRegistro.'","inventariable":"'.$inventariable.'","idRegistro":"'.$fila[0].'"}';	
				if($arrProductos=="")
					$arrProductos=$obj;
				else
					$arrProductos.=",".$obj;
			}
			$obj='{"numReg":"'.$cantidad.'","registros":['.$arrProductos.$arrAux.']}';
			echo $obj;
		}
	}
	
	function obtenerRegistroForDinVolumen()
	{
		global $con;
		
		$idFormulario=$_POST["idFormulario"];
		$idPedidoDetalle=$_POST["idPedidoDetalle"];
		
		$consulta="SELECT nombreTabla FROM 900_formularios  WHERE idFormulario=".$idFormulario;
		$nTabla=$con->obtenerValor($consulta);
		if($nTabla=="")
		{
			echo "|";
		}
		else
		{
			$consulta="SELECT id_".$nTabla." FROM  ".$nTabla." WHERE idReferencia=".$idPedidoDetalle;
			$fila=$con->obtenerValor($consulta);
			if($fila=="")
				echo "1|";
			else	
				echo "1|".$fila;
		}
	}
	
	function obtenerSolicitudesAlmacen()
	{
		global $con;
		
		$idAlmacen=$_POST["idAlmacen"];
		$idCiclo=$_POST["idCiclo"];
		
		global $con;
		$arrProductos="";
		$consultaUnion="";
		
		$inicio=$_POST["start"];
		$cantidad=$_POST["limit"];
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		$consulta="SELECT idCategoria FROM 9303_almacenVSCategoria WHERE idAlmacen=".$idAlmacen;
		$cadena=$con->obtenerListaValores($consulta);
		if($cadena=="")
		{
			$numReg=0;
			$arrProductos="";
			$obj='{"numReg":"'.$numReg.'","registros":['.$arrProductos.']}';
			echo $obj;
		}
		else
		{
			$conConceptos="SELECT claveConcepto,codigoControl FROM 9351_categoriaVSConcepto c,507_objetosGasto o WHERE claveConcepto=clave AND  idCategoria IN (".$cadena.") ORDER BY claveConcepto";
			$res=$con->obtenerFilas($conConceptos);
			$nCat=$con->filasAfectadas;
			if($nCat>0)
			{
				$ct=0;
				if($nCat==1)
				{
					$filaC=$con->obtenerPrimeraFila($conConceptos);
					$conConcentrado="SELECT clave,p.idProducto,clave_Art,nombreProducto,descripcion,DATE_FORMAT(fechaCreacion,'%d/%m/%Y') AS fecha,codigoUnidad,txtcantidad,idEstado FROM 9101_CatalogoProducto p,507_objetosGasto o ,_604_tablaDinamica d WHERE  codigoControlPadre='".$filaC[1]."' AND clave=objetoGasto AND d.cmbProducto=p.idProducto AND ciclo=".$idCiclo." ".$condWhere." ORDER BY clave,nombreProducto limit ".$inicio.",".$cantidad;
					
					$conSinLimit="SELECT clave,p.idProducto,clave_Art,nombreProducto,descripcion,DATE_FORMAT(fechaCreacion,'%d/%m/%Y') AS fecha,codigoUnidad,txtcantidad,idEstado FROM 9101_CatalogoProducto p,507_objetosGasto o ,_604_tablaDinamica d WHERE  codigoControlPadre='".$filaC[1]."' AND clave=objetoGasto AND d.cmbProducto=p.idProducto AND ciclo=".$idCiclo." ".$condWhere." ORDER BY clave,nombreProducto";
				}
				else
				{
				  while($filaU=mysql_fetch_row($res))
				  {
					  $union="(SELECT clave,p.idProducto,clave_Art,nombreProducto,descripcion,DATE_FORMAT(fechaCreacion,'%d/%m/%Y') AS fecha,codigoUnidad,txtcantidad,idEstado FROM 9101_CatalogoProducto p,507_objetosGasto o ,_604_tablaDinamica d WHERE  codigoControlPadre='".$filaU[1]."' AND clave=objetoGasto AND d.cmbProducto=p.idProducto AND ciclo=".$idCiclo." ".$condWhere.")"; 
					  if($consultaUnion=="")
					  	$consultaUnion=$union;
					  else
					  	$consultaUnion.="UNION ".$union;
				  }
				  $conConcentrado=$consultaUnion." ORDER BY fecha ASC limit ".$inicio.",".$cantidad;
				  $conSinLimit=$consultaUnion;
				}
				$respuesta=$con->obtenerFilas($conConcentrado);
				$resNFilas=$con->obtenerFilas($conSinLimit);
				$nreg=$con->filasAfectadas;
				
					while($filaP=mysql_fetch_row($respuesta))
					{
						
						$conUnidad="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$filaP[6]."'";
						$unidad=$con->obtenerValor($conUnidad);


						$obj='{"clave":"'.cv($filaP[0]).'","idProducto":"'.$filaP[1].'","clave_Art":"'.cv($filaP[2]).'","nombreProducto":"'.cv($filaP[3]).'","descripcion":"'.cv($filaP[4]).'","fechaE":"'.$filaP[5].'","unidad":"'.$unidad.'","codigoUnidad":"'.$filaP[6].'","cantidad":"'.$filaP[7].'","estado":"'.$filaP[8].'","idCiclo":"'.$idCiclo.'"}';	
						if($arrProductos=="")
							$arrProductos=$obj;
						else
							$arrProductos.=",".$obj;
						$ct++;
					}
				$obj='{"numReg":"'.$nreg.'","registros":['.$arrProductos.']}';
				
				echo $obj;
			}
		}
	}
	
	function detallesPedidoDetalle()
	{
		global $con;
		
		$idPedido=$_POST["idPedido"];
		
		$condWhere="";
		if(isset($_POST["filter"]))
		{
			$arrFiltro=$_POST["filter"];
			$ct=sizeof($arrFiltro);
			for($x=0;$x<$ct;$x++)
			{
				switch($arrFiltro[$x]["data"]["type"])
				{
					case 'string':
						$condWhere.=" and ".$arrFiltro[$x]["field"]." like '".$arrFiltro[$x]["data"]["value"]."%'";
					break;
				}
			}
		}
		
		$consulta="SELECT  d.idProducto,nombreProducto,clave_Art,descripcion,cantidad,idPedidoDetalle,idMarca,modelo,costoUnitario,iva,idPedidoDetalle FROM 9103_PedidoDetalle d,9101_CatalogoProducto p WHERE idPedido=".$idPedido." AND p.idProducto=d.idProducto ".$condWhere;
		$res=$con->obtenerFilas($consulta);
		$nreg=$con->filasAfectadas;
		
		$arrPedidos="";
		$ct=1;
		while($fila=mysql_fetch_row($res))
		{
			$conMarca="SELECT descripcion FROM _406_tablaDinamica WHERE id__406_tablaDinamica=".$fila[6];
			$marca=$con->obtenerValor($conMarca);
			
			$total=0;
			$totalPieza=0;
			if(($fila[8]!="") || ($fila[8]!="")) //costoU 
			{
				$importeIva=0;
				if(($fila[9]!="") || ($fila[9]!=0)) //iva
				{
					$importeIva=$fila[8]*$fila[9];
				}
				$totalPieza=$fila[8]+$importeIva;
				if(($fila[4]!="") || ($fila[4]!=0))
				{
					$total=$totalPieza*$fila[4];
				}
			}
			
			$arregloDist='';
			$cabeceraT='<table>';
			$cuerpo='';
			$existeCabecera=0;
			$cuerpoT='';
			
			$distribucion="SELECT unidad,tituloPrograma,cantidad FROM 817_organigrama o,517_programas p,9301_distribucionProducto d 
							WHERE  p.idPrograma=d.idPrograma AND o.codigoUnidad=d.codigoUnidad AND idPedidoDetalle=".$fila[5];
			$resD=$con->obtenerFilas($distribucion);				
			$numDist=$con->filasAfectadas;
			if($numDist>0)
			{
				$cuerpoT='<tr><td width=\"300\" class=\"letraRojaSubrayada8\">Departamento</td><td width=\"450\" class=\"letraRojaSubrayada8\">Programa</td><td width=\"80\" class=\"letraRojaSubrayada8\">Cantidad</td></tr>';
			}
			
			while($filaD=mysql_fetch_row($resD))
			{
					$cuerpo.='<tr><td width=\"300\">'.$filaD[0].'</td><td width=\"450\">'.$filaD[1].'</td><td width=\"80\">'.$filaD[2].'</td></tr>';
			}
			
			$finalT='</table>';
			
			$arregloDist=$cabeceraT.$cuerpoT.$cuerpo.$finalT;
			
			$conEstado="SELECT estado FROM 9300_temporalPedido WHERE idPedido=".$idPedido." AND idProducto=".$fila[0];
			$estado=$con->obtenerValor($conEstado);
			if($estado=="")
				$estado=0;
			
			$obj='{"idProducto":"'.$fila[0].'","nombreProducto":"'.$fila[1].'","clave_Art":"'.$fila[2].'","descripcion":"'.$fila[3].'","cantidad":"'.$fila[4].'","estado":"'.$estado.'","distribucion":"'.$arregloDist.'","marca":"'.cv($marca).'","modelo":"'.cv($fila[7]).'","costoU":"'.$fila[8].'","iva":"'.$fila[9].'","costoNetoUnidad":"'.$totalPieza.'","total":"'.$total.'","idPedidoDetalle":"'.$fila[10].'"}';	
			if($arrPedidos=="")
				$arrPedidos=$obj;
			else
				$arrPedidos.=",".$obj;
		}
		$obj='{"numReg":"'.$nreg.'","registros":['.$arrPedidos.']}';
		echo $obj;
	
	}
	
	function obtenerExistenciaProgramaDepto($idProducto,$idPrograma,$idDepartamento,$idAlmacen)
	{
		global $con;
		$consulta="SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idPrograma=".$idPrograma." AND codigoUnidad='".$idDepartamento."' AND idAlmacen=".$idAlmacen;
		$existencia=$con->obtenerValor($consulta);
		if($existencia=="")
		{
			$existencia=0;
		} 
		return $existencia;
	}
	
	function obtenerProgramaDeptoCiclo()
	{
		global $con;	
		
		$idCiclo=$_POST["idCiclo"];
		$codigoUnidad=$_POST["codigoUnidad"];
		$idProducto=$_POST["idProducto"];
		$mes=$_POST["mes"];
		$idAlmacen=$_POST["idAlmacen"];
		$arrCantP="";
		
		$conUnidad="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$codigoUnidad."'";
		$nombreDepto=$con->obtenerValor(conUnidad);
		
		$consulta="SELECT d.idPrograma,tituloPrograma FROM 9130_departamentoVSPrograma d, 517_programas p WHERE codigoUnidad='".$codigoUnidad."' AND ciclo=".$idCiclo." AND p.idPrograma=d.idPrograma";
		$res=$con->obtenerFilas($consulta);
		
		while($fila=mysql_fetch_row($res))
	    {
			$consulta="SELECT idCodigoGastoCiclo FROM 525_productosAutorizados WHERE codDepto='".$codigoUnidad."' AND idPrograma=".$fila[0]." AND idProducto=".$idProducto." AND idCiclo=".$idCiclo;
			$res=$con->ontenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
	    	{
				$consulta="SELECT cantidad FROM 526_distribucionAutorizada WHERE idObjGastoCantidad=".$fila[0]." AND mes=".$mes;
				$cantidadC=$con->obtenerValor($consulta);
				if($cantidad!="")
				{
					$valorResta=0;
					$existencia=obtenerExistenciaProgramaDepto($idProducto,$fila[0],$codigoUnidad,$idAlmacen);
					if($disponible>0)
					{
						$consulta="SELECT SUM(cantidad) FROM 9305_fechasEntregasAlmacen WHERE idAlmacen=".$idAlamacen." AND idPrograma=".$fila[0]." AND codigoUnidad='".$codigoUnidad."' AND mes=".$mes." AND idCiclo=".$idCiclo." AND (estado=0 OR estado=2 )";
						$valorSum=$con->obtenerValor($consulta);
					}
					$disponible=$existencia-$valorSum;
					
					$obj='{"idPrograma":"'.$filaP[0].'","nombrePrograma":"'.$fila[1].'","disponible":"'.$disponible.'","cantidadA":""}';	
					if($arrCantP=="")
						$arrCantP=$obj;
					else
						$arrCantP.=",".$obj;
				}
			}
			
		}
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrCantP.']}';
		
		echo $obj;
	}
	
	function obtenerExistenciaProductoAlmacen($idProducto,$idAlmacen)
	{
		global $con;
		$bandera=0;
		if(isset($_POST["idProducto"]))
		{
			$idProducto=$_POST["idProducto"];
			$bandera=1;
		}
		if(isset($_POST["idAlmacen"]))
			$idAlmacen=$_POST["idAlmacen"];
		
		$consulta="SELECT SUM(cantidad*operacion) FROM 9302_existenciaAlmacen WHERE idProducto=".$idProducto." AND idAlmacen=".$idAlmacen;
		$existencia=$con->obtenerValor($consulta);
		if($existencia=="")
		{
			$existencia=0;
		}
		
		if($bandera==1)
			echo"1|".$existencia;
		else
			return $existencia;
	}
	
	function obtenerDatosPedido()
	{
		global $con;
		$idPedido=$_POST["idPedido"];
		$bandera=$_POST["bandera"];
		
		if($bandera==1)
		{
		   $consulta="SELECT folioPedido,txtRazonSocial2 FROM _405_tablaDinamica pr,9102_PedidoCabecera p WHERE idProveedor_ult=id__405_tablaDinamica AND idPedido=".$idPedido;
		   $etiqueta="Proveedor:";
		}
		else
		{
			if($bandera==2)
			{
				 $consulta="SELECT folioPedido,nombreAlmacen FROM 9030_almacenes a,9102_PedidoCabecera p 
				 WHERE a.idAlmacen=p.idAlmacen AND idPedido=".$idPedido;
				 $etiqueta="Entregar en:";
			}
		}
		
		$fila=$con->obtenerPrimeraFila($consulta);
		
		echo "1|".$etiqueta."|".$fila[0]."|".$fila[1];
	}
	
	function obtenerHistorialProv()
	{
		global $con;
		
		$idProv=$_POST["idProv"];
		$idAlmacen=$_POST["idAlmacen"];
		$arrCantP="";
		
		$consulta="SELECT folioPedido,noFactura,fechaRecepcion,fechaRecibido,observaciones,status_pedido,observaciones FROM 9102_PedidoCabecera WHERE idProveedor_ult=".$idProv." AND idAlmacen=".$idAlmacen." AND status_pedido=0 order by fechaRecibido ASC" ;
		$res=$con->obtenerFilas($consulta);
		$numReg=$con->filasAfectadas;
		
		while($fila=mysql_fetch_row($res))
	    {
			$tipoE="";
			if(strtotime($fila[2])==strtotime($fila[3]))
			{
				$tipoE="En Tiempo";
			}
			
			if(strtotime($fila[2])<strtotime($fila[3]))
			{
				$tipoE="Anticipado";
			}
			
			$obj='{"folioPedido":"'.$fila[0].'","noFactura":"'.$fila[1].'","fechaRecepcion":"'.$fila[2].'","fechaRecibido":"'.$fila[3].'","estado":"'.$tipoE.'","observaciones":"'.cv($fila[6]).'"}';	
			if($arrCantP=="")
				$arrCantP=$obj;
			else
				$arrCantP.=",".$obj;
			
		}
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrCantP.']}';
		
		echo $obj;
	}
	
	function obtenerPedidosPendiestesPorFecha()
	{
		global $con;
		
		$fecha=cambiaraFechaMysql($_POST["fecha"]);
		$idAlmacen=$_POST["idAlmacen"];
		$arrCantP="";
		
		$consulta="SELECT idPedido,folioPedido,noFactura,txtRazonSocial2,horaInicio,idProveedor_ult FROM 9102_PedidoCabecera p,_405_tablaDinamica pr WHERE fechaAgenda='".$fecha."'  AND idAlmacen=".$idAlmacen." AND status_pedido=1 AND idProveedor_ult=id__405_tablaDinamica ORDER BY horaInicio" ;
		$res=$con->obtenerFilas($consulta);
		$numReg=$con->filasAfectadas;
		
		while($fila=mysql_fetch_row($res))
	    {
			$tipoE="";
			if(strtotime($fila[2])==strtotime($fila[3]))
			{
				$tipoE="En Tiempo";
			}
			
			if(strtotime($fila[2])<strtotime($fila[3]))
			{
				$tipoE="Anticipado";
			}
			
			$obj='{"idPedido":"'.$fila[0].'",folioPedido":"'.$fila[1].'","noFactura":"'.$fila[2].'","txtRazonSocial2":"'.cv($fila[3]).'","horaInicio":"'.$fila[4].'","idProvedor":"'.$fila[5].'"}';	
			if($arrCantP=="")
				$arrCantP=$obj;
			else
				$arrCantP.=",".$obj;
			
		}
		$obj='{"numReg":"'.$numReg.'","registros":['.$arrCantP.']}';
		
		echo $obj;
	}
	
	function obtenerFacturaPedido()
	{
		global $con;
		
		$idProv=$_POST["idProv"];
		$idPedido=$_POST["idPedido"];
		
		$encontrada=1;
		$consulta="SELECT idFacturaPedido FROM 9304_facturaVSPedido WHERE idPedido=".$idPedido." AND idProveedor=".$idProv;
		$idFactura=$con->obtenerValor($consulta);
		if($idFactura=="")
		{
			$idFactura="-1";
			$encontrada=0;
		}
		
		echo "1|".$encontrada."|".$idFactura;
	}
	
	function obtenerInformacionEntregaProgramada()
	{
		global $con;
		$idEntrega=$_POST["idEntrega"];
		
		$consulta="SELECT idProducto,codigounidad,idPrograma,cantidad FROM 9305_fechasEntregasAlmacen WHERE idEntrega=".$idEntrega;
		$fila=$con->obtenerPrimeraFila($consulta);
		if(!$fila)
		{
			$fila[0]="-1";
			$fila[1]="-1";
			$fila[2]="-1";
			$fila[3]="-1";
		}
		
		$producto="SELECT nombreProducto FROM 9101_CatalogoProducto WHERE idProducto=".$fila[0];
		$nombreP=$con->obtenerValor($producto);
		
		$depto="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$fila[1]."'";
		$nombreD=$con->obtenerValor($depto);
		
		$programa="SELECT tituloPrograma FROM 517_programas WHERE idPrograma=".$fila[2];
		$nProg=$con->obtenerValor($programa);
		
		echo "1|".$nombreP."|".$nombreD."|".$nProg."|".$fila[3];
	}
?>