<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
    var gridDetalle=crearGridDetalle();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos,
                                                        gridDetalle
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
		                                                {name: 'txtRazonSocial2'},
		                                                {name:'folioPedido'},
		                                                {name:'fechaRecepcion', type:'date'},
                                                        {name: 'fecha', type:'date'},
                                                        {name: 'motivo'},
                                                        {name: 'Nombre'},
                                                        {name: 'observaciones'},
                                                        {name:'num_entrega'},
                                                        {name:'cond_pago'},
                                                        {name: 'txtRFC'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'txtRazonSocial2'},
                                                                        {type: 'string', dataIndex: 'folioPedido'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'date', dataIndex: 'fecha'},
                                                                        {type: 'int', dataIndex:'num_entrega'},
                                                                        {type: 'string', dataIndex:'txtRFC'},
                                                                        {type: 'string', dataIndex: 'num_Factura'},
                                                                        {type: 'date', dataIndex: 'fecha_entrada'}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='88';
                                        proxy.baseParams.idEstado='3';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var tamPagina=100;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                         {
															header:'Folio pedido',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'No. Entregable',
															width:100,
															sortable:true,
															dataIndex:'num_entrega',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'RFC',
															width:150,
															sortable:true,
															dataIndex:'txtRFC',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
													 	{
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	return val;
                                                                                	
                                                                                }
														},
                                                        {
															header:'Fecha entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaRecepcion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
														
                                                        {
															header:'Fecha de movimiento',
															width:120,
															sortable:true,
															dataIndex:'fecha',
                                                            hideable:true,
                                                            css:'text-align:right !important;',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    }
														},
                                                        {
															header:'Motivo de movimiento',
															width:260,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'motivo',
                                                            hideable:true
                                                            
														},
                                                        {
															header:'Responsable de movimiento',
															width:220,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'Nombre',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Pedidos rechazados</b></span>',
                                                            columnLines:true,
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridDetallePedido').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:registro.get('idPedido'),funcion:86}})
                                                }
    							)                                                 
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:88,idEstado:3,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}


function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'},
                                                        {name: 'clave_Art'},
                                                        {name: 'cve_grupo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='86';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Cve. Producto',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'clave_Art',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Grupo',
															width:80,
                                                            align:'right',
															sortable:true,
															dataIndex:'cve_grupo',
                                                           
                                                            hideable:true
														},
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                    	return Ext.util.Format.usMoney(subTotal)
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                        var total=subTotal+parseFloat(registro.get('iva'));
                                                                    	return Ext.util.Format.usMoney(total)
                                                                    },
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Contenedor',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            
                                                            dataIndex:'contenedor',
                                                            hideable:true
														},
                                                        {
															header:'Unidad de medida',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'unidadMedida',
                                                            hideable:true
														},
                                                        {
															header:'Presentacion',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'presentacion',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:230,
                                                            collapsible:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                           
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}