<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridPendientes();
}

function crearGridPendientes()
{
    var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idPedido'},
                                                                  {name: 'folioPedido'},
                                                                  {name: 'idProv'},
                                                                  {name: 'txtRazonSocial2'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'fechaAgenda'},
                                                                  {name: 'fechaSolicitada'},
                                                                  {name: 'idFacturaPedido'}
                                                                  
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=4;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                    }
                        );
   
    
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'txtRazonSocial2' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'folioPedido' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Folio',
															width:80,
															sortable:true,
															dataIndex:'folioPedido',
                                                            align:'left'
														},
                                                        {
															header:'Proveedor',
															width:320,
                                                            align:'left',
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            renderer:function(val,meta,registro)
                                                            		  {
                                                                        return'<a href="javascript:historialProv('+registro.get('idProv')+',\''+bE(registro.get('txtRazonSocial2'))+'\')"><img height="13" width="13" src="../images/icon_code.gif" alt="Ver Historial" title="Ver Historial" />&nbsp;&nbsp;'+val+'</a>';
                                                                      }	
                                                        },
                                                        {
															header:'Fecha programada',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaRecepcion'
                                                            //,
//                                                            renderer:function(val,meta,registro)
//                                                            				  {
//                                                                                if(val=='')
//                                                                                	return'';
//                                                                                else
//                                                                                	return Ext.util.Format.date(val,'d/m/Y')   ;
//                                                                              }
														},
                                                        {
															header:'Fecha agendada',
															width:95,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaAgenda'
                                                            //,
//                                                            renderer:function(val,meta,registro)
//                                                            				  {
//                                                                                return Ext.util.Format.date(val,'d/m/Y');
//                                                                              }
														},
                                                        {
															header:'Fecha solicitada',
															width:95,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaSolicitada'
														},
                                                        {
															header:'',
															width:70,
															sortable:true,
                                                            align:'center',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	return '<a href="javascript:verDetalle('+registro.get('idPedido')+','+registro.get('folioPedido')+',\''+registro.get('txtRazonSocial2')+'\','+registro.get('idProv')+')"><img height="13" width="13" src="../images/icon_document.gif" alt="Ver Detalle" title="Ver Detalle" /></a>'
                                                                            +'&nbsp;&nbsp;<a href="javascript:agendarProveedor('+registro.get('idPedido')+','+registro.get('folioPedido')+',\''+registro.get('txtRazonSocial2')+'\',\''+registro.get('fechaRecepcion')+'\')"><img height="13" width="13" src="../images/calendar.png" alt="Agendar Entrega" title="Agendar Entrega" /></a>'
                                                                            +'&nbsp;&nbsp;<a href="javascript:verFactura('+registro.get('idFacturaPedido')+')"><img height="13" width="13" src="../images/document_001.gif" alt="Ver factura" title="Ver factura" /></a>';
                                                                    }
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPendientes',
                                                            title:'Pedidos por recibir',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            renderTo:'tabPedidos',
                                                            height:750,
                                                            width:800,
                                                            plugins: [filters]
                                                        }
                                                    );
		
    dsRegistrosP.load()  ;
    return tblGridP;     
}


function verDetalle(idPedido,folio,nombreP,idProv)
{
	var fechaActual=gE('fechaActual').value;
    var gP=gridProductos(idPedido,idProv,folio);
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaHistorial',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:25,
                                                            y:5,
                                                            html:'<span class="letraRojaSubrayada8">No Folio: </span>'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:450,
                                                            y:5,
                                                            hidde:true,
                                                            id:'facturaP',
                                                            html:''
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:90,
                                                            y:5,
                                                            html:'<span class="letraExt">'+folio+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:25,
                                                            html:'<span class="letraRojaSubrayada8">Proveedor:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:90,
                                                            y:25,
                                                            html:'<span class="letraExt">'+nombreP+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:40,
                                                            y:45,
                                                            html:'<span class="letraRojaSubrayada8">Fecha:</span> '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:90,
                                                            y:45,
                                                            html:'<span class="letraExt">'+fechaActual+'</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:18,
                                                            y:65,
                                                            html:'<span class="letraExt"><b>Acepatado:</b> indica que el producto ha sido revisado y se encuentra en condiciones de ingresarlo al almac&eacute;n</span>'
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:16,
                                                            y:85,
                                                            html:'<span class="letraExt"><b>Con detalle:</b> indica que el producto no se encuentra en condiciones de ingresarlo al almac&eacute;n</span>'
                                                        },
                                                        {
                                                        	xtype:'panel',
                                                            x:0,
                                                            y:105,
                                                            items:[
                                                                        gP
                                                                   ]
                                                         }
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Detalle de Pedido',
										width: 840,
										height:580,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaD',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
    
    function funcAjax2()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
             var ban=0;
             var etiqueta=Ext.getCmp('facturaP');
             if(resp[1]==1)
             {
             	var idFactura=resp[2];
                etiqueta.setText('<span class="letraRojaSubrayada8">Este pedido cuenta con factura&nbsp;&nbsp;</span><a href="../media/obtenerFactura.php?idFactura='+idFactura+'"><img src="../images/Save.png" height="18" width="18" alt="Ver factura" title="Ver factura" /></a>',false)
                ban=1;
             }
             
             ventana.show();
             if(ban==1)
             {
             	etiqueta.show();
             }
        }
        else
        {
             Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=49&idPedido='+idPedido+'&idProv='+idProv,true)

}

function gridProductos(idPedido,idProv,folio)
{
    var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                               			{name: 'idProducto'},
                                                        {name: 'nombreProducto'},
                                                        {name: 'clave_Art'},
                                                        {name: 'descripcion'},
                                                        {name: 'cantidad'},
														{name: 'estado'},
                                                        {name: 'distribucion'},
                                                        {name: 'marca'},
                                                        {name: 'modelo'},
														{name: 'costoU'},
                                                        {name: 'iva'},
                                                        {name: 'costoNetoUnidad'},
                                                        {name: 'total'},
                                                        {name: 'idPedidoDetalle'},
                                                        {name: 'idFormulario'},
                                                        {name: 'idTipoR'},
                                                        {name: 'inventariable'},
                                                        {name: 'folio'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                            
                                        }
                                      );
  var dsRegistrosProd=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(
                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'folio'
                                                        })                                      
     
	dsRegistrosProd.on('beforeload',function(proxy)
    								{
										proxy.baseParams.funcion=6;
                                        proxy.baseParams.idPedido=idPedido;
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                        proxy.baseParams.folio=folio;
                                    }
                        )

	var summary = new Ext.ux.grid.HybridSummary();                                      
 
   
   var suma=0;
   dsRegistrosProd.load({callback:function()
   								  {
                                  	   var tamanoS=dsRegistrosProd.getCount();
                                       var y;
                                       for(y=0;y<tamanoS;y++)
                                       {
                                          var elemento=dsRegistrosProd.getAt(y);
                                          if(elemento.get('estado')==2)
                                          {
                                              suma=suma+1;
                                          }
                                       }
                                       
                                       var boton=Ext.getCmp('removerB');
                                       if(suma>0)
                                       {
                                            boton.show();
                                       }
                                       else
                                       {
                                            boton.hide();
                                       }
                                  }
                        }); 
   
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
                                              
                                 
    
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table>'
                                                                             +'<tr>'
                                                                                +'<td width="600" height="10"></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width:"600" align="left"><span class="letraRojaSubrayada8"><b>Distribuci&oacute;n:</b><br /></span></td>'
                                                                             +'</tr>'
                                                                             +'<tr>'
                                                                                +'<td width="600" align="left"><span class="copyrigthSinPadding">{distribucion}</span></td>'
                                                                             +'</tr>'
                                                                         +'</table>'
                                                                        
                                                					  )
                                            });                                           
   
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        expander,
														{
															header:'',
															width:280,
															sortable:true,
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
	                                                                    	var estado=registro.get('estado');
                                                                            
                                                                            var checadoA='';
                                                                            var checadoD='';
                                                                            var muestraD='';
                                                                            switch(estado)
                                                                            {
                                                                            	case '0':
                                                                                	checadoA='';
                                                                                    checadoD='';
                                                                                break;
                                                                                case '1':
                                                                                	var radioG=gE('aceptado_'+registro.get('idProducto'));
                                                                                    checadoA='checked="checked"';
                                                                                    checadoD='';
                                                                                    muestraD='<a href=\"javascript:mostrarVentanaRegistro('+registro.get('idPedidoDetalle')+',\''+bE(registro.get('nombreProducto'))+'\',\''+bE(registro.get('marca'))+'\',\''+bE(registro.get('modelo'))+'\',\''+registro.get('idFormulario')+'\',\''+registro.get('idTipoR')+'\',\''+registro.get('inventariable')+'\',\''+registro.get('cantidad')+'\',\''+idPedido+'\',\''+registro.get('idProducto')+'\',\''+estado+'\')"><img height="13" width="13" src="../images/magnifier.png" alt="ver registro" title="ver registro" /></a>';
                                                                                    
                                                                                break;
                                                                                case '2':
                                                                                	checadoD='checked="checked"';
                                                                                    checadoA='';
                                                                                    muestraD='<a href="javascript:agregarObservacion('+idPedido+','+registro.get('idProducto')+')"><img height="13" width="13" src="../images/paperpencil_48.png" alt="Agregar Observaci&oacute;n" title="Agregar Observaci&oacute;n" />Observaciones</a>';
                                                                                break;
                                                                            
                                                                            }
                                                                            return '<input type="radio" name="producto_'+registro.get('idProducto')+'[]" id="aceptado_'+registro.get('idProducto')+'" onchange="guardaTemp(this,'+idPedido+','+registro.get('idProducto')+','+registro.get('idPedidoDetalle')+',\''+bE(registro.get('nombreProducto'))+'\',\''+bE(registro.get('marca'))+'\',\''+bE(registro.get('nodelo'))+'\','+registro.get('cantidad')+','+registro.get('idFormulario')+','+registro.get('idTipoR')+','+registro.get('inventariable')+')" '+checadoA+' />&nbsp;&nbsp;<b>Aceptado</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                            	  +'<input type="radio" name="producto_'+registro.get('idProducto')+'[]" id="detalle_'+registro.get('idProducto')+'" onchange="guardaTemp(this,'+idPedido+','+registro.get('idProducto')+','+registro.get('idPedidoDetalle')+',\''+bE(registro.get('nombreProducto'))+'\',\''+bE(registro.get('marca'))+'\',\''+bE(registro.get('nodelo'))+'\','+registro.get('cantidad')+','+registro.get('idFormulario')+','+registro.get('idTipoR')+','+registro.get('inventariable')+')" '+checadoD+' />&nbsp;&nbsp;<b>Con detalle</b>&nbsp;&nbsp;<label id="eObserv">'+muestraD+'</label>';
                                                                    }
														},
                                                        {
															header:'Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left',
                                                            summaryRenderer:function()
                                                                          {
                                                                              return "<b>Costo total:</b>";
                                                                          }
														},
                                                        {
															header:'Cantidad',
															width:60,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'Marca',
															width:100,
															sortable:true,
															dataIndex:'marca'
														},
                                                        {
															header:'Modelo',
															width:100,
															sortable:true,
															dataIndex:'modelo'
														},
                                                        {
															header:'Costo Unit.',
															width:100,
															sortable:true,
															dataIndex:'costoU',
                                                            hidden:true,
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:100,
															sortable:true,
															dataIndex:'iva',
                                                            hidden:true
														},
                                                        {
															header:'Costo',
															width:70,
															sortable:true,
															dataIndex:'costoNetoUnidad',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Total',
															width:70,
															sortable:true,
															dataIndex:'total',
                                                            summaryType:'sum',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Folio',
															width:70,
															sortable:true,
															dataIndex:'folio'
														}
													]
												);
	var tblGridProd=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'articulos',
                                                            x:0,
                                                            y:0,
                                                            title:'Art&iacute;culos',
                                                            store:dsRegistrosProd,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:430,
                                                            width:810,
                                                            plugins: [filters,expander,summary],
                                                            tbar:[
                                                            	  {
                                                                      text:'Registrar recepci&oacute;n de pedido',
                                                                      icon:'../images/add.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              registrarPedido(idPedido,0,idProv);
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'Solicitud Vo.Bo. Adquisiciones',
                                                                      id:'removerB',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              registrarPedido(idPedido,3,idProv);
                                                                          }
                                                                  }
                                                                 ],
                                                            view: new Ext.grid.GroupingView(  {
                                                                                                  forceFit:true,
                                                                                                  showGroupName: false,
                                                                                                  enableNoGroups:false,
                                                                                                  enableGroupingMenu:true,
                                                                                                  hideGroupedColumn: true
                                                                                              }   
                                                                                            )  
                                                        }
                                                    );
    return tblGridProd;   
}


function agendarProveedor(idPedido,folioP,nombreP,fechaContrato)
{
	var fechaMaxima=Ext.util.Format.date(fechaContrato,'d/m/Y');
    var fechaActual=gE('fechaActual').value;
    var formA = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:85,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Folio:</b>'
													 },
                                                     {
													 x:120,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+folioP+'</span>'
													 },
                                                     {
													 x:52,
													 y:30,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Proveedor:</b>'
													 },
                                                     {
													 x:120,
													 y:30,
													 xtype:'label',
                                                     align:'center',
													 html:'<span class="letraRojaSubrayada8">'+nombreP+'</span>'
													 },
                                                     {
													 x:30,
													 y:50,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Fecha Entrega:</b>'
													 },
                                                     {
                                                     x:120,
													 y:50,
                                                     xtype:'datefield',
													 id:'fechaAP',
                                                     maxValue:fechaMaxima,
                                                     minValue:fechaActual
                                                     },
                                                     {
													 x:50,
													 y:85,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Hora Inicio:</b>'
													 },
                                                     {
                                                     x:120,
													 y:80,
                                                     xtype:'timefield',
													 id:'horaInicio'
                                                     },
                                                     {
													 x:15,
													 y:110,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Tiempo estimado:</b>'
													 },
                                                     {
                                                     x:120,
													 y:105,
                                                     xtype:'numberfield',
                                                     width:50,
													 id:'tiempoE'
                                                     },
                                                     {
													 x:180,
													 y:110,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>(Min.)</b>'
													 }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
                                        //closable : false,
										width: 450,
										height:270,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formA,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var fechaAgendada=Ext.getCmp('fechaAP').getValue();
                                                                                    if(fechaAgendada=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar la fecha para agendar este pedido');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var horaInicio=Ext.getCmp('horaInicio').getValue();
                                                                                    
                                                                                    if(horaInicio=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar la hora de inicio');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var tiempoE=Ext.getCmp('tiempoE').getValue();
                                                                                    if(tiempoE=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el tiempo estimado');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(tiempoE==0)
                                                                                    {
                                                                                    	msgBox('El tiempo estimado no puede ser igual a <b>0</b>');
                                                                                        return;
                                                                                    }
                                                                                    fechaAgendada=Ext.util.Format.date(fechaAgendada,'Y-m-d');
                                                                                    
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var suma=0;
                                                                                             var almacen=Ext.getCmp('gridPendientes').getStore();
    		  																				 almacen.reload(); 
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=29&idPedido='+idPedido+'&fechaAgendada='+fechaAgendada+'&horaInicio='+horaInicio+'&tiempoE='+tiempoE,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();                           
}

function guardaTemp(radio,idPedido,idProducto,idPedidoDetalle,nombreProducto,marca,modelo,cantidad,idFormulario,idTipoR,inventariable)
{
	//var radio=r;
//	if (typeof(radio)=='string')
//    	radio=gE(r);
    var cadena=radio.id;
    var datos=cadena.split('_');
    var accion=datos[0];
    
    if(accion=='aceptado')
    {
        if(radio.checked)
        {
            
                if(idFormulario!=-1)
                {
                    var estado=1;
                    mostrarVentanaRegistro(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,idTipoR,inventariable,cantidad,idPedido,idProducto,estado);
                }
                else
                {
                    msgBox('La partida a la que pertenece este producto no cuenta con una configuracion de registro');
                    return
                }
            //obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=39&idProducto='+idProducto,true)
        }
    }
    else
    {
    	if(radio.checked)
        {
            var estado=1;
            agregarObservacion(idPedido,idProducto);
        }
    } 
}

function agregarObservacion(idPedido,idProducto)
{
   var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:15,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Indique las observaciones:</b>'
													 },
                                                     {
                                                     x:15,
													 y:30,
                                                     height:100,
													 width:350,
                                                     xtype:'textarea',
													 id:'observaciones'
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
                                        closable : false,
										width: 400,
										height:250,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var observaciones=Ext.getCmp('observaciones').getValue();
                                                                                    if(observaciones=='')
                                                                                    {
                                                                                     	msgBox('Debe escribir las observaciones');
                                                                                        return;
                                                                                    
                                                                                    }
                                                                                    
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var suma=0;
                                                                                             var almacen=Ext.getCmp('articulos').getStore();
    		  																				 almacen.load({callback:function()
                                                                                                                            {
                                                                                                                                 var tamanoS=almacen.getCount();
                                                                                                                                 var y;
                                                                                                                                 for(y=0;y<tamanoS;y++)
                                                                                                                                 {
                                                                                                                                    var elemento=almacen.getAt(y);
                                                                                                                                    if(elemento.get('estado')==2)
                                                                                                                                    {
                                                                                                                                        suma=suma+1;
                                                                                                                                    }
                                                                                                                                 }
                                                                                                                                 
                                                                                                                                 var boton=Ext.getCmp('removerB');
                                                                                                                                 if(suma>0)
                                                                                                                                 {
                                                                                                                                      boton.show();
                                                                                                                                 }
                                                                                                                                 else
                                                                                                                                 {
                                                                                                                                      boton.hide();
                                                                                                                                 }
                                                                                                                            }
                                                                                                                  }); 
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=8&idPedido='+idPedido+'&idProducto='+idProducto+'&observaciones='+observaciones,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		var observaciones=Ext.getCmp('observaciones').getValue();
                                                                        if(observaciones=='')
                                                                        {
                                                                            var radioAceptado=gE('aceptado_'+idProducto);
                                                                            var radioDetalle=gE('detalle_'+idProducto);
                                                                            
                                                                            radioDetalle.checked=false;
                                                                            radioAceptado.checked=true;
                                                                        
                                                                        }
                                                                        ventana.close();
																	}
														}
													]
									}
								);
		
        llenarObservacionesProducto(ventana,idPedido,idProducto);
}

function llenarObservacionesProducto(ventana,idPedido,idProducto)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]=2)
        {
             Ext.getCmp('observaciones').setValue(resp[1]);
             ventana.show();
        }
        else
        {
              if(resp[0]==1)
              {
              	  ventana.show();
              }
              else
              {	
                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
              } 
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=9&idPedido='+idPedido+'&idProducto='+idProducto,true)

}

function registrarPedido(idPedido,tipoR,idProv)
{
	var ventana=Ext.getCmp('ventanaD');
    var almacen=Ext.getCmp('articulos').getStore();
    var tamanoGrid=almacen.getCount();
    var aceptados=0;
    var detalles=0;
    var sinRevisar=0;
    var x;
    var y;
    var mensaje='Una vez registrado el pedido no podra realizar cambios sobre este<br /><br />Esta seguro de registrar el pedido';
    
    for(y=0;y<tamanoGrid;y++)
    {
        var obj=almacen.getAt(y);
        var idProducto=obj.get('idProducto');
        
        var arreglo=document.getElementsByName('producto_'+idProducto+'[]');
    	var tamano=arreglo.length;
        for(x=0;x<tamano;x++)
        {
            var elemento=arreglo[x];
            var cadena=elemento.id;
            var datos=cadena.split('_');
            var idTipo=datos[0];
            if(elemento.checked)
            {
                if(idTipo=='aceptado')
                {
                    aceptados=aceptados+1;
                }
                else
                {
                    detalles=detalles+1;
                }
            }
            else
            {
                sinRevisar=sinRevisar+1;
            }
        }
    }
   
    if(sinRevisar==(tamanoGrid*2))
    {
    	Ext.MessageBox.alert(lblAplicacion,'No puede registrar el pedido sin revisi&oacute;n completa');
        return;
    }
    
    var sumatoria=detalles+aceptados+sinRevisar;
    sumatoria=sumatoria/2;
    if(sumatoria<tamanoGrid)
    {
    	Ext.MessageBox.alert(lblAplicacion,'Uno o mas articulos no han sido revisados');
        return;
    }
    
    if(detalles>0)
    {
        if(tipoR==0)
        {
        	Ext.MessageBox.alert(lblAplicacion,'No puede registrar este pedido ya que presenta detalles');
            return;
            mensaje='Uno o mas articulos presentan detalles.<br /><br />Esta seguro de registrar el pedido ';
        }
        else
        {
        	mensaje='Ha decidido enviar este pedido a validacion.<br /><br />Esta seguro de enviar el pedido ';
        }    
    }
    
    function resp(btn)
    {
        if(btn=='yes')
        {
            if(tipoR==0)
            {
            	function funcAjax2()
                {
                    var resp=peticion_http.responseText.split('|');
                    if(resp[0]==1)
                    {
                         if(resp[1]==1)
                         {
                         	var observacionesF='';
                            var idAlmacen=gE('idAlmacen').value;
                            var fecha=gE('fechaActual').value;
                            
                            function funcAjax3()
                            {
                                var resp=peticion_http.responseText.split('|');
                                if(resp[0]==1)
                                {
                                    
                                     var almacen1=Ext.getCmp('gridPendientes').getStore();
                                     almacen1.reload();
                                     ventana.close();
                                     var ventana1=Ext.getCmp('ventanaD');
                                     ventana1.close();
                                }
                                else
                                {
                                     Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax3, 'POST','funcion=10&idPedido='+idPedido+'&fecha='+fecha+'&tipoR='+tipoR+'&idAlmacen='+idAlmacen+'&noFactura='+resp[3]+'&observacionesF='+observacionesF,true)
                         }
                         else
                         {	
                            mostrarVentanaFactura(idPedido,tipoR,idProv,resp[1],resp[2]);
                         }
                    }
                    else
                    {
                         Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=49&idPedido='+idPedido+'&idProv='+idProv,true)
            }
            else
            {
                var idAlmacen=gE('idAlmacen').value;
                var fecha=gE('fechaActual').value;
                function funcAjax()
                {
                    var resp=peticion_http.responseText.split('|');
                    if(resp[0]==1)
                    {
                        
                         var almacen1=Ext.getCmp('gridPendientes').getStore();
                         almacen1.reload();
                         ventana.close();
                    }
                    else
                    {
                         Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=10&idPedido='+idPedido+'&fecha='+fecha+'&tipoR='+tipoR+'&idAlmacen='+idAlmacen,true)
            }    
        }     
    } 
    msgConfirm(mensaje,resp); 
}


function verObservacionRecibido(idPedido,idProducto)
{
   var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[{
													 x:15,
													 y:10,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Observaciones:</b>'
													 },
                                                     {
                                                     x:15,
													 y:30,
                                                     height:100,
													 width:350,
                                                     xtype:'textarea',
													 id:'observaciones'
                                                     }
											
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: '',
										width: 400,
										height:250,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
		
        llenarObservacionesProducto(ventana,idPedido,idProducto);
}

function llenarObservacionesProducto(ventana,idPedido,idProducto)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==2)
        {
             Ext.getCmp('observaciones').setValue(resp[1]);
             ventana.show();
        }
        else
        {
              if(resp[0]==1)
              {
              	  ventana.show();
              }
              else
              {	
                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
              } 
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=9&idPedido='+idPedido+'&idProducto='+idProducto,true)
}

function mostrarVentanaFactura(idPedido,tipoR,idProv,encontrado,idFactura)
{
	var ligaF='';
   // if(encontrado==1)
//    {
//    	var ligaF='<a href="../Profesores/mostrarDocumento.php?idFactura='+idFactura+'"><img src="../images/Save.png" height="18" width="18" alt="Ver factura" title="Ver factura" /></a> '
//    }
//   
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:15,
													 y:15,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>No.Factura:</b>'
													},
                                                    {
                                                     x:85,
													 y:10,
													 width:280,
                                                     xtype:'textfield',
													 id:'noFactura'
                                                    }
                                                  //  ,
//                                                    {
//                                                     x:85,
//													 y:10,
//													 width:280,
//                                                     xtype:'label',
//													 html:ligaF
//                                                    },
//                                                    {
//													 x:15,
//													 y:50,
//													 xtype:'label',
//                                                     align:'center',
//													 html:'<b>Indique las observaciones:</b>'
//													 },
//                                                     {
//                                                     x:15,
//													 y:70,
//                                                     height:80,
//													 width:350,
//                                                     xtype:'textarea',
//													 id:'observacionesF'
//                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Registro de Pedido',
                                        //closable : false,
										width: 400,
										height:150,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var noFactura=Ext.getCmp('noFactura').getValue();
                                                                                    if(noFactura=='')
                                                                                    {
                                                                                     	msgBox('Debe ingresar el No. de Factura');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var observacionesF='';
                                                                                    //var observacionesF=Ext.getCmp('observacionesF').getValue();
                                                                                    
                                                                                    var idAlmacen=gE('idAlmacen').value;
                                                                                    var fecha=gE('fechaActual').value;
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                            
                                                                                             var almacen1=Ext.getCmp('gridPendientes').getStore();
                                                                                             almacen1.reload();
                                                                                             ventana.close();
                                                                                             var ventana1=Ext.getCmp('ventanaD');
                                                                                             ventana1.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                             Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=10&idPedido='+idPedido+'&fecha='+fecha+'&tipoR='+tipoR+'&idAlmacen='+idAlmacen+'&noFactura='+noFactura+'&observacionesF='+observacionesF,true)
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
                                                                        var ventana1=Ext.getCmp('ventanaD');
                                                                        ventana1.close();
																	}
														}
													]
									}
								);
     ventana.show();                           
}

function mostrarVentanaRegistro(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,tipoRegistro,inventariable,cantidad,idPedido,idProducto,estado)
{
    if(tipoRegistro==1)
    {
    	mostrarVentamaPieza(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,tipoRegistro,inventariable,cantidad,idPedido,idProducto,estado)
    }
    else
    {
       	function funcAjax()
        {
            var resp=peticion_http.responseText.split('|');
            if(resp[0]==1)
            {
                 ventanaGuardarVolumen(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,tipoRegistro,inventariable,cantidad,resp[1],idPedido,idProducto,estado) ;
            }
            else
            {
                 Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=41&idFormulario='+idFormulario+'&idPedidoDetalle='+idPedidoDetalle,true)
    }     
}

function mostrarVentamaPieza(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,tipoRegistro,inventariable,cantidad,idPedido,idProducto,estado)
{
    var dsRegistrosDin=new Ext.data.JsonStore({
                                                    root: 'registros',
                                                    totalProperty: 'numReg',
                                                    autoLoad: false,
                                                    fields: [
                                                              {name: 'idFormulario'},
                                                              {name: 'idPedidoDetalle'},
                                                              {name: 'nombreProducto'},
                                                              {name: 'marca'},
                                                              {name: 'modelo'},
                                                              {name: 'tipoRegistro'},
                                                              {name: 'inventariable'},
                                                              {name: 'idRegistro'},
                                                          ],         
                                                    proxy : new Ext.data.HttpProxy	(

                                                                                      {

                                                                                          url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                      }
                                                                                  )                             
                                                })
	                                                    
	dsRegistrosDin.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=40;
                                        proxy.baseParams.idFormulario=idFormulario;
                                        proxy.baseParams.idPedidoDetalle=idPedidoDetalle;
                                        proxy.baseParams.nombreProducto=nombreProducto;
                                        proxy.baseParams.marca=marca;
                                        proxy.baseParams.modelo=modelo;
                                        proxy.baseParams.tipoRegistro=tipoRegistro;
                                        proxy.baseParams.inventariable=inventariable;
                                        proxy.baseParams.cantidad=cantidad;
                                    }
                        );
     
	dsRegistrosDin.load();	
	var cmP= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto'
                                                            },
                                                            {
                                                                header:'Marca',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'marca'
                                                            },
                                                            {
                                                                header:'Modelo',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'modelo',
                                                                renderer:function(val,meta,registro)
                                                                				{
                                                                                	 if(val==undefined)
                                                                                     {
                                                                                	 	return '';
                                                                                     }   
                                                                                }
                                                            }
                                                            ,
                                                            {
                                                                header:'Registro',
                                                                width:80,
                                                                sortable:true,
                                                                align:'center',
                                                                renderer:function(val,meta,registro)
                                                                				{
                                                                                
                                                                                	 return '<a  href="javascript:enviarFormularioD('+registro.get('idPedidoDetalle')+','+registro.get('idFormulario')+','+registro.get('idRegistro')+',1,'+idPedido+','+idProducto+','+estado+')"><img src="../images/app_48.png" width="16" height="16" /></a>';
                                                                                }
                                                            }   
                                                        ]
                                                    );
											
	var gReg=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'registrosPieza',
                                                        store:dsRegistrosDin,
                                                        frame:true,
                                                        cm: cmP,
                                                        height:400,
                                                        width:770
													}
					
    											);
        
        
        var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                     {
                                                     x:0,
													 y:10,
                                                     xtype:'panel',
													 items:[
                                                     			gReg
                                                           ]
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Registro de Articulos',
                                        id:'ventana2',
										width: 800,
										height:500,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center'
                                        ,
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
                                                                       
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.on('close',function()
     					{	
     						if(tipoRegistro==1)
                            {
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText.split('|');
                                    if(resp[0]==1)
                                    {
                                    	if(resp[1]==0)
                                        {
                                        	var rad=gE('aceptado_'+idProducto);
                                            rad.checked=false;
                                            msgBox('El registro de este articulo esta incompleto');
                                            return;
                                        }   
                                    }
                                    else
                                    {
                                         Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=69&idFormulario='+idFormulario+'&idPedidoDetalle='+idPedidoDetalle+'&cantidad='+cantidad,true)
                            }    
                        }
     			);
     ventana.show(); 
}

function enviarFormularioD(idPedidoDetalle,idFormulario,idRegistro,bandera,idPedido,idProducto,estado)
{
    var cPagina='sFrm=true';
	var arrParam=[['idFormulario',idFormulario],['idReferencia',idPedidoDetalle],['idRegistro',idRegistro],['cPagina',cPagina],['accionCancelar','window.close();'],['eJs',bE('window.opener.regresaID(@idRegistro,'+bandera+','+idPedido+','+idProducto+','+estado+');window.close();return;')]];
    
    window.open('',"vAuxiliar3", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
 	enviarFormularioDatosV("../modeloPerfiles/registroFormulario.php",arrParam,'POST','vAuxiliar3'); 
}

function regresaID(idRegistro,bandera,idPedido,idProducto,estado)
{
	if(bandera==1)
    {
    	gEx('registrosPieza').getStore().reload();
    }    
    
      function funcAjax()
      {
          var resp=peticion_http.responseText.split('|');
          if(resp[0]==1)
          {
              oE('eObserv');
              var suma=0;
              var almacen=Ext.getCmp('articulos').getStore();
              almacen.load({callback:function()
                                            {
                                                 var tamanoS=almacen.getCount();
                                                 var y;
                                                 for(y=0;y<tamanoS;y++)
                                                 {
                                                    var elemento=almacen.getAt(y);
                                                    if(elemento.get('estado')==2)
                                                    {
                                                        suma=suma+1;
                                                    }
                                                 }
                                                 
                                                 var boton=Ext.getCmp('removerB');
                                                 if(suma>0)
                                                 {
                                                      boton.show();
                                                 }
                                                 else
                                                 {
                                                      boton.hide();
                                                 }
                                            }
                                  }); 
          }
          else
          {
                  Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
          }
      }
      obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=7&idPedido='+idPedido+'&idProducto='+idProducto+'&estado='+estado,true)
}

function ventanaGuardarVolumen(idPedidoDetalle,nombreProducto,marca,modelo,idFormulario,tipoRegistro,inventariable,cantidad,idRegistro,idPedido,idProducto,estado)
{
	var form = new Ext.form.FormPanel(	
                                            {
                                                baseCls: 'x-plain',
                                                layout:'absolute',
                                                defaultType: 'textfield',
                                                items: 	[
                                                        {
                                                         x:10,
                                                         y:10,
                                                         xtype:'label',
                                                         align:'center',
                                                         html:'<b>Producto:</b>'
                                                         },
                                                         {
                                                         x:75,
                                                         y:10,
                                                         xtype:'label',
                                                         html:'<span class="letraRojaSubrayada8">'+bD(nombreProducto)+'</span>'
                                                         },
                                                        {
                                                         x:25,
                                                         y:80,
                                                         xtype:'label',
                                                         html:'<b>Marca:</b>'
                                                         },
                                                         {
                                                         x:75,
                                                         y:80,
                                                         xtype:'label',
                                                         html:'<span class="letraRojaSubrayada8">'+bD(marca)+'</span>'
                                                         },
                                                         {
                                                         x:20,
                                                         y:110,
                                                         xtype:'label',
                                                         html:'<b>Modelo:</b>'
                                                         },
                                                         {
                                                         x:75,
                                                         y:110,
                                                         xtype:'label',
                                                         html:'<span class="letraRojaSubrayada8">'+bD(modelo)+'</span>'
                                                         },
                                                         {
                                                         x:15,
                                                         y:140,
                                                         xtype:'label',
                                                         html:'<b>Formato de Registro:</b>'
                                                         },
                                                         {
                                                         x:140,
                                                         y:140,
                                                         xtype:'label',
                                                         html:'<span><a href="javascript:enviarFormularioD('+idPedidoDetalle+','+idFormulario+','+idRegistro+',2,'+idPedido+','+idProducto+','+estado+')"><img src="../images/app_48.png" width="16" height="16" /></a></span>'
                                                         }
                                                        ]
                                            }
                                        );
    
        var ventana = new Ext.Window(
                                        {
                                            title: 'Registro de Articulos',
                                            id:'ventana1',
                                            width: 500,
                                            height:250,
                                            minWidth: 300,
                                            minHeight: 100,
                                            layout: 'fit',
                                            plain:true,
                                            modal:true,
                                            bodyStyle:'padding:5px;',
                                            buttonAlign:'center',
                                            items: form,
                                            listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                        
                                                                    }
                                                                }
                                                    },
                                            buttons:	[
                                                            {
                                                                text: 'Cancelar',
                                                                handler:function()
                                                                        {
                                                                            ventana.close();
                                                                        }
                                                            }
                                                        ]
                                        }
                                    );
         ventana.show();            
}

function historialProv(idProv,razonSocial)
{
	var gridH=crearHistorial(idProv);
    var formA = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                     {
													 x:20,
													 y:20,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Proveedor:</b>'
													 },
                                                     {
                                                     x:100,
													 y:20,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+bD(razonSocial)+'</b>'
                                                     },
                                                     {
													 x:0,
													 y:60,
													 xtype:'panel',
                                                     items:[
                                                           	 gridH
                                                           ]
													 }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Historial de pedidos',
                                        id:'ventana3',
										width: 680,
										height:450,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formA,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();   
}

function crearHistorial(idProv)
{
    var dsRegH=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'folioPedido'},
                                                                  {name: 'noFactura'},
                                                                  {name: 'fechaRecepcion'},
                                                                  {name: 'fechaRecibido'},
                                                                  {name: 'estado'},
                                                                  {name: 'observaciones'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegH.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=47;
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                        proxy.baseParams.idProv=idProv;
                                    }
                        );
   
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Folio',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            align:'left'
														},
                                                        {
															header:'No. Factura',
															width:130,
															sortable:true,
															dataIndex:'noFactura',
                                                            align:'left'
														},
                                                        {
															header:'Fecha Limite',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaRecepcion'
														},
                                                        {
															header:'Fecha de Recepci&oacute;n',
															width:150,
                                                            align:'center',
															sortable:true,
															dataIndex:'fechaRecibido'
														},
                                                        {
															header:'Registro',
															width:120,
                                                            align:'center',
															sortable:true,
															dataIndex:'estado'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'griH',
                                                            store:dsRegH,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:370,
                                                            width:650
                                                        }
                                                    );
		
    dsRegH.load()  ;
    return tblGridP;     
}

function verFactura (idFacturaPedido)
{
	if(idFacturaPedido==-1)
    {
    	msgBox('Este pedido no cuenta con factura registrada');
        return;
    }
    
    function funcAjax2()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
              var idFactura=resp[3];
              if(resp[1]=='')
              {
                  resp[1]='-1';
              }
              datosFactura1(idFacturaPedido,resp[1],resp[2],resp[3]);
        }
        else
        {
             Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=77&idFacturaPedido='+idFacturaPedido,true)
}

function datosFactura1(idFacturaPedido,cadena,folioFactura,tipoFactura)
{
    if(tipoFactura==1)
    {
    	var nombreTipo='Copia digital'
    }
    else
    {
    	var nombreTipo='Factura Electronica';
    }    
    var form1 = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
                                            id:'formulario1',
											defaultType: 'textfield',
											items: 	[
                                                     {
													 x:50,
													 y:20,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>No.Factura:</b>'
													 },
                                                     {
                                                     x:120,
													 y:20,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+folioFactura+'</b>'
                                                     },
                                                     {
                                                     x:70,
													 y:40,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Cadena:</b>'	
                                                     },
                                                     {
                                                     x:120,
													 y:40,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+cadena+'</b>'
                                                     },
                                                     {
                                                     x:25,
													 y:60,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Tipo de factura:</b>'	
                                                     },
                                                     {
                                                     x:120,
													 y:60,
                                                     xtype:'label',
                                                     html:'<b class="letraRojaSubrayada8">'+nombreTipo+'</b>'
                                                     },
                                                     {
                                                     x:45,
                                                     y:80,
                                                     xtype:'label',
                                                     html:'<b>Ver archivo:</b>&nbsp;&nbsp;&nbsp;<a href="../media/obtenerFactura.php?idFactura='+idFacturaPedido+'"><img src="../images/Save.png" height="18" width="18" alt="Ver factura" title="Ver factura" /></a> '
                                                     }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Datos factura',
                                        id:'ventana4',
										width: 350,
										height:250,
										minWidth: 400,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form1,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();   
}