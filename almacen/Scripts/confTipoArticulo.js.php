<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idFormulario,nombreFormulario FROM 900_formularios WHERE idProceso=234";
	$arregloForm=$con->obtenerFilasArreglo($consulta);
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridT();  
}

var arregloF=<?php echo $arregloForm?>;
//var arregloF=[[1,'Sin Formato'],[2,'formato 2'],[3,'formato 3'],[4,'formato 3']];
var arregloT=[[-1,'Sin Configuraci&oacute;n'],[0,'Volumen'],[1,'Pieza']];
var arregloI=[[-1,'Sin Configuraci&oacute;n'],[0,'No'],[1,'Si']];
function llenarComboS(opc)
{
    var arreglo=[[1,'Sin Formato'],[2,'formato 2'],[3,'formato 3'],[4,'formato 3']];
    var tamano=arreglo.length;
    var x;
    var cadena='';
    for(x=0;x< tamano;x++)
    {
    	var elemento=arreglo[x];
        var datos=elemento[0];
        
        var seleccionado='';
        if(parseFloat(elemento[0])==parseFloat(opc))
        {
        	seleccionado='selected="selected"';
        }
        
        if(cadena=='')
        	cadena='<option value="'+elemento[0]+'" '+seleccionado+'>'+elemento[1]+'</option>';
        else
        	cadena+='<option value="'+elemento[0]+'" '+seleccionado+'>'+elemento[1]+'</option>';    
    } 
    return cadena;
}

function crearGridT()
{
    var dsRegistrosT=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'clave'},
                                                                  {name: 'nombreObjetoGasto'},
                                                                  {name: 'idFormato'},
                                                                  {name: 'tipoRegistro'},
                                                                  {name: 'inventariable'},
                                                                  {name: 'nombreF'},
                                                                  {name: 'tipoR'},
                                                                  {name: 'tipoInv'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosT.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=33;
                                    }
                        );
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'clave' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreObjetoGasto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
    //var chkRow=new Ext.grid.CheckboxSelectionModel();
    var chkRow=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
                                                        chkRow,
														{
															header:'Clave',
															width:70,
															sortable:true,
															dataIndex:'clave',
                                                            align:'left'
														},
                                                        {
															header:'Partida',
															width:500,
															sortable:true,
															dataIndex:'nombreObjetoGasto',
                                                            align:'left'
														},
                                                        {
															header:'Formato de registro',
															width:120,
															sortable:true,
                                                            align:'left',
                                                            dataIndex:'nombreF'
														},
                                                        {
															header:'Tipo Registro',
															width:100,
															sortable:true,
															dataIndex:'tipoR',
                                                            align:'left'
														},
                                                        {
															header:'Iventariable',
															width:100,
															sortable:true,
															dataIndex:'tipoInv',
                                                            align:'left'
														}
													]
												);
	var tblGridR=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCatAlm',
                                                            title:'Categor&iacute;as del almac&eacute;n',
                                                            renderTo:'tipoP',
                                                            store:dsRegistrosT,
                                                            frame:true,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            height:650,
                                                            width:850,
                                                            plugins:[filters],
                                                            tbar:[                                                        
                                                                	{
                                                                    	text:'Modificar Configuraci&oacute;n',
                                                                        icon:'../images/pencil.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                          {
                                                                              modificarConf();
                                                                          }
                                                                    }
                                                                ]
                                                        }
                                                    );
		
    dsRegistrosT.load()  ;
    return tblGridR;     
}

function modificarConf()
{
	var filas=Ext.getCmp('gridCatAlm').getSelectionModel().getSelections();
    var tamano=filas.length;
    if(tamano==0)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    
    var cmbTipoF=crearComboExt('cmbTipoF',arregloF,170,10);
    var cmbTipoR=crearComboExt('cmbTipoR',arregloT,170,35);
    var cmbInv=crearComboExt('cmbInv',arregloI,170,60);
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                            		{
													 x:45,
													 y:15,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Formato de registro:</b>'
													 },
                                                     cmbTipoF,
                                                     {
													 x:70,
													 y:40,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Tipo de registro:</b>'
													 },
                                                     cmbTipoR,
                                                     {
													 x:80,
													 y:65,
													 xtype:'label',
                                                     align:'center',
													 html:'<b>Inventariable:</b>'
													 },
                                                     cmbInv
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Configuraci&oacute;n de partidas',
                                        id:'ventana1',
										width: 415,
										height:180,
										minWidth: 230,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptar2',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var tipoF=Ext.getCmp('cmbTipoF').getValue();
                                                                                    if(tipoF=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el formato de registro');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var tipoR=Ext.getCmp('cmbTipoR').getValue();
                                                                                    if(tipoR==='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el tipo de registro');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var tipoI=Ext.getCmp('cmbInv').getValue();
                                                                                    if(tipoI==='')
                                                                                    {
                                                                                     	msgBox('Debe indicar si es inventariable');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var cadena='';
                                                                                    var x;
                                                                                    for(x=0;x<tamano;x++)
                                                                                    {
                                                                                        var clave=filas[x].get('clave');
                                                                                       
                                                                                        if(cadena=='')
                                                                                            cadena=clave;
                                                                                        else
                                                                                            cadena+=','+clave; 
                                                                                    }
                                                                                        
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText.split('|');
                                                                                        if(resp[0]==1)
                                                                                        {
                                                                                             var almacen=Ext.getCmp('gridCatAlm').getStore();
                                                                                             almacen.reload(); 
                                                                                             ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=38&tipoF='+tipoF+'&tipoR='+tipoR+'&tipoI='+tipoI+'&cadena='+cadena,true);
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show(); 
}