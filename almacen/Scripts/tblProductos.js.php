<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__919_tablaDinamica,txtDescripcion FROM _919_tablaDinamica ORDER BY txtDescripcion";
	$arrCondiciones=$con->obtenerFilasArreglo($consulta);
?>

var arrCondiciones=<?php echo $arrCondiciones?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
    var gridProductosRecibidos=crearGridProductosRecibidos();
    var gridProductosRecibir=crearGridProductosPorRecibir();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            border:false,
                                            frame:false,
                                            items:	[
                                            			gridProductos,
                                                        {
                                                        	id:'tabGrid',
                                                        	collapsible:true,
                                                            region:'south',
                                                        	xtype:'tabpanel',
                                                            activeTab:0,
                                                            height:240,
                                                            items:	[
                                                            			 {
                                                                        	xtype:'panel',
                                                                             layout:'anchor',
                                                                        	title:'Pedidos por recibir',
                                                                             items:[gridProductosRecibir]
                                                                        },
                                                            			{
                                                                        	xtype:'panel',
                                                                            layout:'anchor',
                                                                        	title:'Pedidos recibidos',
                                                                            items:[gridProductosRecibidos]
                                                                        }
                                                                       
                                                                       /* ,
                                                                        {
                                                                        	xtype:'panel',
                                                                            hidden:true,
                                                                             layout:'anchor',
                                                                        	title:'Historial de costo por a&ntilde;o',
                                                                             items:[]
                                                                        }*/
                                                                        
                                                            		]
                                                        }
                                            		]
                                        }
                                     ]
						}
                    )   
}

function crearGridProductos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'clave_Art'},
		                                                {name:'nombreProducto'},
		                                                {name:'descripcion'},
		                                                {name: 'nombreObjetoGasto'},
		                                                {name:'ultimoPrecio'},
		                                                {name:'existencia'},
                                                        {name:'status_art'},
                                                        {name: 'nombre'},
                                                        {name: 'cveObjetoGasto'},
                                                        {name: 'cveCategoria'},
                                                        {name:'factor_min'},
                                                        {name:'factor_max'},
                                                        {name:'factor_pdr'},
                                                        {name:'minimo_exit'},
                                                        {name:'maximo_exit'},
                                                        {name :'puntoReorden'}
                                                        
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'clave_Art'},
                                                                        {type: 'string', dataIndex: 'nombre'},
                                                                        {type: 'string', dataIndex: 'nombreProducto'},
                                                                        {type: 'string', dataIndex: 'cveCategoria'},
                                                                        {type: 'string', dataIndex: 'cveObjetoGasto'},
                                                                        {type: 'string', dataIndex: 'nombreObjetoGasto'},
                                                                        {type: 'numeric', dataIndex: 'existencia'},
                                                                        {type: 'list', options:[['1','Activo'],['0','Inactivo']], dataIndex: 'status_art', phpMode: true}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=83;
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	 var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<table >'+
                                                    '<tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr>',
                                                    '</table>'
                                                )
                                            });
	var tamPagina=100;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        expander,
                                                        {
															header:'Cve. Grupo',
															width:80,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'cveCategoria',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Grupo',
															width:300,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'nombre',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
													 	{
															header:'Cve. Producto',
															width:100,
															sortable:true,
															dataIndex:'clave_Art',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	return val;
                                                                                	
                                                                                }
														},
														{
															header:'Producto',
															width:480,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
														},
                                                        {
															header:'Cve. Objeto<br />Gasto',
															width:80,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'cveObjetoGasto',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Objeto gasto',
															width:340,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'nombreObjetoGasto',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
                                                        
														{
															header:'Precio &uacute;ltima compra',
															width:120,
                                                            align :'center',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'ultimoPrecio',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Existencia',
															width:135,
                                                            css:'text-align:right !important;',
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'existencia',
                                                            hideable:true
														},
                                                        {
															header:'Situaci&oacute;n',
															width:135,
                                                            css:'text-align:right !important;',
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'status_art',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='0')
                                                                        	return 'Inactivo';
                                                                        else
                                                                        	return 'Activo';
                                                                    }
														},
                                                        {
															header:'Factor m&iacute;nimo',
															width:100,
															sortable:true,
															dataIndex:'factor_min',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Factor m&aacute;ximo',
															width:100,
															sortable:true,
															dataIndex:'factor_max',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Factor P. de R.',
															width:100,
															sortable:true,
															dataIndex:'factor_pdr',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'M&iacute;nimo',
															width:100,
															sortable:true,
															dataIndex:'minimo_exit',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'M&aacute;ximo',
															width:100,
															sortable:true,
															dataIndex:'maximo_exit',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Punto de reorden',
															width:100,
															sortable:true,
															dataIndex:'puntoReorden',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														}
                                                        
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            plugins:[filters,expander],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idProducto','-2'],['idAlmacen',gE('idAlmacen').value]]
                                                                                        enviarFormularioDatos('../almacen/producto.php',arrParam);
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	var arrParam=[['idProducto',fila.get('idProducto')],['idAlmacen',gE('idAlmacen').value]]
                                                                                        enviarFormularioDatos('../almacen/producto.php',arrParam);
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea eliminar');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGrid.getStore().remove(fila);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=84&idProducto='+fila.get('idProducto'),true);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer eliminar el producto seleccionado? ',resp)
                                                                                        	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                      	{
                                                                        	icon:'../images/icon_changelog.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Generar formato de actualización de inventario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idAlmacen',gE('idAlmacen').value]]
                                                                                        enviarFormularioDatos('../reportes/generarFormatoActualizacionInventario.php',arrParam);
                                                                                    }
                                                                            
                                                                        }  
                                                            		],
                                                            bbar:[paginador],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:83,idAlmacen:gE('idAlmacen').value}});    
    tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('gridProductosRecibidos').getStore().load({url: '../paginasFunciones/funcionesAlmacen.php',params:{funcion:140,idProducto:registro.get('clave_Art'),situacion:0,start:0,limit:100}})
                                                    gEx('gridProductosRecibir').getStore().load({url: '../paginasFunciones/funcionesAlmacen.php',params:{funcion:140,idProducto:registro.get('clave_Art'),situacion:1,start:0,limit:100}})
                                                    
                                                }
                                  );                                              
	return tblGrid;                                                                                                     
}

function crearGridProductosRecibidos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
		                                                {name: 'txtRazonSocial2'},
		                                                {name:'folioPedido'},
                                                        {name:'cantidad', type:'int'},
		                                                {name:'fechaRecepcion', type:'date'},
                                                        {name:'fechaRecibido', type:'date'},
                                                        {name: 'diferencia', type:'int'},
                                                        {name: 'observaciones'},
                                                        {name:'num_entrega'},
                                                        {name:'cond_pago'},
                                                        {name: 'txtRFC'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'txtRazonSocial2'},
                                                                        {type: 'string', dataIndex: 'folioPedido'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'}
                                                                        
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='140';
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var tamPagina=100;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
                                               
	var summary = new Ext.ux.grid.GridSummary();                                               
                                               
	var cModelo= new Ext.grid.ColumnModel   	(

												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Folio pedido',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'No. Entregable',
															width:100,
															sortable:true,
															dataIndex:'num_entrega',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'RFC',
															width:150,
															sortable:true,
															dataIndex:'txtRFC',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
													 	{
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	return val;
                                                                                	
                                                                                }
														},
														{
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad',
                                                            css:'text-align:left !important;',
                                                            hideable:true,
                                                            summaryType:'sum'
														},
                                                        {
															header:'Fecha entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaRecepcion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Fecha recepcion',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaRecibido',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														}
														,
                                                        {
															header:'D&iacute;as restantes',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            hidden:true,
                                                            dataIndex:'diferencia',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='green';
                                                                    	if(val<0)
                                                                        	color="red";                                                                        
                                                                         return '<font color="'+color+'">'+val+'</font>';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'Condici&oacute;n de pago',
															width:130,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'cond_pago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCondiciones,val);
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductosRecibidos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            anchor:'100% 100%', 
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            plugins:[filters,summary],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador,
                                                             {
                                                                  icon:'../images/magnifier.png',
                                                                  cls:'x-btn-text-icon',
                                                                  text:'Ver detalle de pedido',
                                                                  handler:function()
                                                                          {
                                                                              var fila=tblGrid.getSelectionModel().getSelected();
                                                                              if(fila==null)
                                                                              {
                                                                                  msgBox('Debe seleccionar el pedido cuyo detalle desea observar');
                                                                                  return;
                                                                              }
                                                                             mostrarVentanaRegistroEntrada(fila);
                                                                              
                                                                          }
                                                                  
                                                              }
                                                            ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	//if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	//gEx('gridDetallePedido').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:registro.get('idPedido'),funcion:86}})
                                                }
    							)                                                 
	//dsTablaRegistros2.load({params:{start:0,limit:100,funcion:85,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}

function crearGridProductosPorRecibir()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
		                                                {name: 'txtRazonSocial2'},
		                                                {name:'folioPedido'},
                                                        {name:'cantidad', type:'int'},
		                                                {name:'fechaRecepcion', type:'date'},
                                                        {name: 'diferencia', type:'int'},
                                                        {name: 'observaciones'},
                                                        {name:'num_entrega'},
                                                        {name:'cond_pago'},
                                                        {name: 'txtRFC'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'txtRazonSocial2'},
                                                                        {type: 'string', dataIndex: 'folioPedido'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'}
                                                                        
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='140';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	var summary = new Ext.ux.grid.GridSummary();                                               
	var tamPagina=100;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(

												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Folio pedido',
															width:100,
															sortable:true,
															dataIndex:'folioPedido',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'No. Entregable',
															width:100,
															sortable:true,
															dataIndex:'num_entrega',
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'RFC',
															width:150,
															sortable:true,
															dataIndex:'txtRFC',
                                                            css:'text-align:left !important;',
                                                            hideable:true
														},
													 	{
															header:'Proveedor',
															width:350,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	return val;
                                                                                	
                                                                                }
														},
														 {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad',
                                                            css:'text-align:left !important;',
                                                            hideable:true,
                                                            summaryType:'sum'
														},
                                                        {
															header:'Fecha entrega',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'fechaRecepcion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
														
                                                        {
															header:'D&iacute;as restantes',
															width:110,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'diferencia',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='green';
                                                                    	if(val<0)
                                                                        	color="red";                                                                        
                                                                         return '<font color="'+color+'">'+val+'</font>';
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                         {
															header:'Condici&oacute;n de pago',
															width:130,
                                                            align :'center',
															sortable:true,
                                                            dataIndex:'cond_pago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCondiciones,val);
                                                                    },
                                                            css:'text-align:right !important;',
                                                            hideable:true
														},
                                                        {
															header:'Comentarios',
															width:350,
															sortable:true,
															dataIndex:'observaciones',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductosRecibir',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            anchor:'100% 100%',
                                                            plugins:[filters,summary],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador,
                                                            	{
                                                                  icon:'../images/magnifier.png',
                                                                  cls:'x-btn-text-icon',
                                                                  text:'Ver detalle de pedido',
                                                                  handler:function()
                                                                          {
                                                                              var fila=tblGrid.getSelectionModel().getSelected();
                                                                              if(fila==null)
                                                                              {
                                                                                  msgBox('Debe seleccionar el pedido cuyo detalle desea observar');
                                                                                  return;
                                                                              }
                                                                             mostrarVentanaRegistroEntrada(fila);
                                                                              
                                                                          }
                                                                  
                                                              }
                                                            ],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridDetallePedido').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:registro.get('idPedido'),funcion:86}})
                                                }
    							)                                                 
	//dsTablaRegistros2.load({params:{start:0,limit:100,funcion:85,idAlmacen:gE('idAlmacen').value}});                                                  
	return tblGrid;                                                                                                     
}

function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'},
                                                        {name: 'total'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='86';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                    	return Ext.util.Format.usMoney(subTotal)
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                        var total=subTotal+parseFloat(registro.get('iva'));
                                                                        registro.set('total',total);
                                                                    	return Ext.util.Format.usMoney(total)
                                                                    },
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Contenedor',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            
                                                            dataIndex:'contenedor',
                                                            hideable:true
														},
                                                        {
															header:'Unidad de medida',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'unidadMedida',
                                                            hideable:true
														},
                                                        {
															header:'Presentacion',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'presentacion',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:230,
                                                            collapsible:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:[
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registra entrada de producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el pedido cuyo ingreso desea registrar ');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                          		mostrarVentanaRegistroEntrada(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer registrar el ingreso de este pedido? ',resp)
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Rechazar entrada de producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el pedido que desea rechazar');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                          		mostrarVentanaMotivo(fila,3,'Rechazar pedido');
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer rechazar este pedido? ',resp)
                                                                                    	
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/page_accept.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Solicitar autorizaci&oacute;n de entrada',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridProductos').getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea enviar a autorizacion');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	mostrarVentanaMotivo(fila,4,'Enviar pedido a validaci&oacute;n');
                                                                                            	
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer enviar este pedido a validaci&oacute;n? ',resp)
                                                                                        	
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                           
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}

function mostrarVentanaRegistroEntrada(fila)
{
	var gridDesglocePedido=crearGridDesglocePedido(fila);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'<span style="color: #000"><b>Folio de pedido:</b></span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:40,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('folioPedido')+'</b></span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:40,
                                                            html:'<span style="color: #000"><b>No. Entregable:</b></span>'
                                                        },
                                                        {
                                                        	x:330,
                                                            y:40,
                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.get('num_entrega')+'</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'<span style="color: #000"><b>Proveedor:</b></span>'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:70,
                                                            html:'<span class="letraRojaSubrayada8"><b>['+fila.get('txtRFC')+'] '+fila.get('txtRazonSocial2')+'</b></span>'
                                                        },
                                                        gridDesglocePedido
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Detalles del pedido',
										width: 780,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	
																}
															}
												},
										buttons:	[
														
														{
															text: 'Aceptar',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
	
}

function crearGridDesglocePedido(filaPedido)
{
    var x;
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
                                                        {name: 'clave_Art'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'},
                                                        {name: 'total'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
    /* for(x=0;x<almacen.getCount();x++)
    {
    	dsTablaRegistros2.add(almacen.getAt(x).copy());
    }
*/
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var summary = new Ext.ux.grid.GridSummary();
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                        {
															header:'Cve. Producto',
															width:95,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'clave_Art',
                                                            hideable:true
														},
													 	{
															header:'Producto',
															width:250,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														
														{
															header:'Cantidad',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:95,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.data.costoUnitario)*parseFloat(registro.data.cantidad);
                                                                    	return Ext.util.Format.usMoney(subTotal)
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:80,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:100,
                                                            align :'right',
															sortable:true,
                                                            summaryType:'sum',
                                                            dataIndex:'total',
                                                            renderer:'usMoney',
                                                           
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedidoFactura',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            x:5,
                                                            y:100,
                                                            width:750,
                                                            height:360,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            plugins:[summary],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   

	gEx('gridDetallePedidoFactura').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:filaPedido.get('idPedido'),funcion:86}})	
	return tblGrid;   
}