<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
   
}

function enviarFormP()
{
	var id=gE('idAlmacen').value;
    var arrDatos=[['idAlmacen',id]];
    enviarFormularioDatos('../almacen/confAlmacen.php',arrDatos);
}

function agregarCategoria()
{
	var fila=Ext.getCmp('grid_tblTabla').getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar al menos un elemento');
        return;
    }
    var idAlmacen=fila.get('idAlmacen');
    var arrDatos=[['idAlmacen',idAlmacen]];
    enviarFormularioDatos('../almacen/almacenVSCategoria.php',arrDatos);
    
}