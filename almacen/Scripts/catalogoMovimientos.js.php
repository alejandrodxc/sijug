<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT claveElemento,nombreElemento FROM 1018_catalogoVarios WHERE tipoElemento=1";
	$arrSituacion=$con->obtenerFilasArreglo($consulta);
?>
var arrTipoAfectacion=[['0','Debe'],['1','Haber']];

var arrSituacion=<?php echo $arrSituacion?>;
var arrTiempo=[];
var arrAfectacion=[['1','+'],['-1','-']];
Ext.onReady(inicializar);

function inicializar()
{
	arrTiempo=eval(bD(gE('arrTiempos').value));

	var gridAfectacion=crearGridAfectacion();
    var arrTabs=new Array();
    
    arrTabs.push({
                      contentEl:'tblGeneral', 
                      title:'Datos generales'
                  });
    var idPerfilMovimiento=gE('idPerfilMovimiento');
    if(idPerfilMovimiento.value!='-1')
    {
                  
        arrTabs.push	(
                           
                            {
                                                        
                                title:'Afectaci&oacute;n almac&eacute;n',
                                layout:'anchor',
                                items:	[
                                            gridAfectacion
                                        ]
                            }
                        )                  
                      
	}
	var tabs = new Ext.TabPanel({
                                    renderTo: 'tblConceptos',
                                    activeTab: 0,
                                    width:850,
                                    height:480,
                                    items:	arrTabs
                                    
                                    });
	

}



function crearGridAfectacion()
{
    var lector= new Ext.data.JsonReader({
                                        
                                            totalProperty:'numReg',
                                            fields: [
                                            			{name: 'idRegistro'},
                                                        {name: 'tiempoMovimiento'},
                                                        {name: 'tipoAfectacion'},
                                                        {name: 'funcionEjecucion'},
                                                        {name: 'idFuncionAplicacion'},
                                                        {name: 'idTiempoMovimiento'}
                                                        
                                                    ],
                                            root:'registros'
                                            
                                        }
                                      );
    
                                                                                  
    var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'tiempoMovimiento', direction: 'ASC'},
                                              groupField: 'tiempoMovimiento',
                                              remoteGroup:false,
                                              remoteSort: false,
                                              autoLoad:false
                                              
                                          }) 
    alDatos.on('beforeload',function(proxy)
                                {
                                    proxy.baseParams.funcion='135';
                                    proxy.baseParams.idPerfilMovimiento=gE('idPerfilMovimiento').value;
                                    proxy.baseParams.tGrid=2;
                                }
                    )   
    
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'Situaci&oacute;n de movimiento',
                                                            width:250,
                                                            sortable:true,
                                                            dataIndex:'tiempoMovimiento'
                                                        },
                                                        
                                                        {
                                                            header:'Tipo Afectaci&oacute;n',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'tipoAfectacion',
                                                            css:'text-align:center;',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='1')
                                                                        	return '+';
                                                                        return '-';
                                                                    }
                                                            
                                                        },
                                                        
                                                         {
                                                            header:'Funci&oacute;n de ejecuci&oacute;n',
                                                            width:220,
                                                            sortable:true,
                                                            dataIndex:'funcionEjecucion'
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                              {
                                                  id:'gridAfectacion',
                                                  anchor:'100% 100%',
                                                  store:alDatos,
                                                  region:'center',
                                                  frame:true,
                                                  cm: cModelo,
                                                  stripeRows :true,
                                                  loadMask:true,
                                                  columnLines : true,
                                                  stripeRows :true,
                                                  loadMask:true,
                                                  tbar:	[
                                                            {
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Agregar afectaci&oacute;n',
                                                                handler:function()
                                                                        {
                                                                            mostrarVentanaAfectacionPresupuestal();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/pencil.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Modificar afectaci&oacute;n',
                                                                handler:function()
                                                                        {
                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                            if(fila==null)
                                                                            {
                                                                                msgBox('Debe seleccionar la afectaci&oacute;n contable que desea modificar');
                                                                                return;
                                                                            }
                                                                            mostrarVentanaAfectacionPresupuestal(fila);
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/delete.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Remover afectaci&oacute;n',
                                                                handler:function()
                                                                        {
                                                                            var gridAfectacion=gEx('gridAfectacion');
                                                                            var fila=gridAfectacion.getSelectionModel().getSelected();
                                                                            if(fila.length==0)
                                                                            {
                                                                            	msgBox('Debe seleccionar la afectaci&oacute;n que desea remover');
                                                                                return;
                                                                            }
                                                                            function resp(btn)
                                                                            {
                                                                            	if(btn=='yes')
                                                                                {
                                                                                	function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            gridAfectacion.getStore().remove(fila);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=136&idAfectacion='+fila.get('idRegistro')+'&tAfectacion=2',true);
                                                                                }
                                                                            }
                                                                            msgConfirm('Est&aacute; seguro de querer remover la afectación seleccionada?',resp);
                                                                        }
                                                                
                                                            }
                                                            
                                                        ],
                                                  view: new Ext.grid.GroupingView(	{
                                                                                          forceFit:false,
                                                                                          showGroupName: false,
                                                                                          enableNoGroups:false,
                                                                                          enableGroupingMenu:false,
                                                                                          enableGrouping:false,
                                                                                          hideGroupedColumn: false
                                                                                      }   
                                                                                  )
                                              }
                                          );
	tblGrid.getStore().load({idPerfilMovimiento:gE('idPerfilMovimiento').value,tGrid:2});                                          
    return 	tblGrid;	
}



function enviarFormulario()
{
	if(validarFormularios('frmEnvio'))
    {
    	gE('frmEnvio').submit();
    }
}

function recargarGrid(tGrid)
{
	gEx('gridAfectacion').getStore().reload();

	cerrarVentanaFancy();
}


function removerFuncionAplicacion()
{
	gEx('idFuncion').setValue('');
    gEx('idFuncion').idFuncion='';
    
}

function mostrarVentanaAfectacionPresupuestal(fila)
{

	var cmbTiempoPresupuestal=crearComboExt('cmbTiempoPresupuestal',arrTiempo,140,5,400);
    
    var cmbTipoAfectacion=crearComboExt('cmbTipoAfectacion',arrAfectacion,140,35,120);
	var lblTitulo='Agregar configuraci&oacute;n presupuestal';
    if(fila)
    	lblTitulo='Modificar configuraci&oacute;n presupuestal';
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Situaci&oacute;n movimiento:'
                                                        },
                                                        cmbTiempoPresupuestal,
                                                        
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Tipo afectaci&oacute;n:<span class="letraRoja">*</span>'
                                                        },
                                                        cmbTipoAfectacion,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Funci&oacute;n de aplicaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:65,
                                                            xtype:'textfield',
                                                            readOnly:true,
                                                            width:250,
                                                            id:'idFuncion'
                                                        },
                                                        {
                                                        	x:390,
                                                            y:65,
                                                            xtype:'label',
                                                            id:'lblSistema',
                                                             html:'&nbsp;&nbsp;<a href="javascript:mostrarVentanaSistema()"><img src="../images/pencil.png"></a>&nbsp;&nbsp;<a href="javascript:removerFuncionAplicacion()"><img src="../images/delete.png" title="Remover funci&oacute;n de aplicaci&oacute;n" alt="Remover funci&oacute;n de aplicaci&oacute;n" /></a>'
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 600,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('porcentajeAfectacion').focus(500,false);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
                                                                        if(cmbTiempoPresupuestal.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbTiempoPresupuestal.focus();
                                                                            }
                                                                            msgBox('Debe indicar la situaci&oacute;n del movimiento a realizar',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbTipoAfectacion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbTipoAfectacion.focus();
                                                                            }
                                                                            msgBox('Debe indicar el tipo de afectaci&oacute;n a aplicar sobre el movimiento movimiento',resp2);
                                                                            return;
                                                                        }
                                                                        var idFuncion=gEx('idFuncion');
                                                                        var idFuncionAplicacion=-1;
                                                                        if(idFuncion.getValue()!='')
                                                                        	idFuncionAplicacion=idFuncion.idFuncion;
                                                                        var idAfectacion=-1;
                                                                        if(fila)
                                                                        	idAfectacion=fila.get('idRegistro');
                                                                        var cadObj='{"idPerfilMovimiento":"'+gE("idPerfilMovimiento").value+'","idAfectacion":"'+idAfectacion+'","tiempoAfectacion":"'+cv(cmbTiempoPresupuestal.getValue())+
                                                                        		'","tipoAfectacion":"'+(cmbTipoAfectacion.getValue())+
                                                                        		'","idFuncionAplicacion":"'+idFuncionAplicacion+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridAfectacion').getStore().reload();
                                                                             	ventanaAM.close();   
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=134&cadObj='+cadObj,true); 
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    if(fila)	
    {
    	cmbTiempoPresupuestal.setValue(fila.get('idTiempoMovimiento'));
        cmbTipoAfectacion.setValue(fila.get('tipoAfectacion'));
        gEx('idFuncion').setValue(fila.get('funcionEjecucion'));
        gEx('idFuncion').idFuncion=fila.get('idFuncionAplicacion');
    }	
}

function mostrarVentanaSistema()
{
	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                            {
                                                var iConsulta=idConsulta;
                                                var r=new registroConcepto	(
                                                                                {
                                                                                    idConsulta:iConsulta,
                                                                                    nombreConsulta:nombre,
                                                                                    nombreCategoria:'',
                                                                                    descripcion:'',
                                                                                    valorRetorno:'',
                                                                                    parametros:''
                                                                                }
                                                                            )
                                                                            
                                                funcionSistemaSeleccionado(r, gEx('vAgregarExp'));	
                                            }
	mostrarVentanaExpresion(funcionSistemaSeleccionado,true);
    	
}

function funcionSistemaSeleccionado(fila,ventana)
{
	gEx('idFuncion').setValue(fila.get('nombreConsulta'));
    gEx('idFuncion').idFuncion=fila.get('idConsulta');
    ventana.close();
}
