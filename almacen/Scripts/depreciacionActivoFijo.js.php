Ext.onReady(inicializar);

function inicializar()
{
	crearGridDepreciacion();
}

function crearGridDepreciacion()
{
	
   var alDatos=new Ext.data.JsonStore({
                                            proxy : new Ext.data.HttpProxy	(

                                                                              {

                                                                                  url: '../paginasFunciones/funcionesPlaneacion.php'

                                                                              }

                                                                          ),
                                            autoLoad:true,
                                            idProperty:'noInventario',
                                            fields: [
                                                        {name: 'numInventario'},
                                                        {name: 'descripcion'},
                                                        {name: 'anioAdquisicion'},
                                                        {name: 'valorCompra'},
                                                        {name: 'factorDepreciacion'},
                                                        {name: 'valorDepreciacion'},
                                                        //{name: 'depreciacionAcumulada'},
                                                        {name: 'valorLibro'},
                                                        {name: 'CABMS'}
                                                        
                                            ],
                                            root:'registros',
                                             totalProperty:'num'
                                        })    

	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=120;
                                        proxy.baseParams.start=0;
                                        proxy.baseParams.limit=40
                                    }
               )
    
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 40,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
    
    var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'numInventario'},
                                                                        {type: 'string', dataIndex: 'descripcion'},
                                                                        {type: 'string', dataIndex: 'anioAdquisicion'}
                                                        				
                                                                    ]

                                                    }

                                                ); 
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer( {width:25}),
														
														{
															header:'Categor&iacute;a',
															width:200,
															sortable:true,
															dataIndex:'CABMS'
														},
														{
															header:'No. Inventario',
															width:100,
															sortable:true,
															dataIndex:'numInventario'
														},
														{
															header:'Producto',
															width:200,
															sortable:true,
															dataIndex:'descripcion'
														},
                                                        {
															header:'A&ntilde;o compra',
															width:100,
															sortable:true,
															dataIndex:'anioAdquisicion'
														},
                                                        {
															header:'Valor compra',
															width:100,
															sortable:true,
															dataIndex:'valorCompra',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Factor depreciaci&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'factorDepreciacion'
														},
                                                        {
															header:'Valor depreciaci&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'valorDepreciacion',
                                                            renderer:'usMoney'
														},
                                                        /*{
															header:'Depreciaci&oacute;n acumulada',
															width:145,
															sortable:true,
															dataIndex:'depreciacionAcumulada',
                                                            renderer:'usMoney'
														},*/
                                                        {
															header:'Valor Libro',
															width:130,
															sortable:true,
															dataIndex:'valorLibro',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            renderTo:'tblGrid',
                                                            cm: cModelo,
                                                            height:800,
                                                            width:1100,
                                                            bbar: paginador,
                                                            plugins:[filters]
                                                        }
                                                    );
	return 	tblGrid;	
}