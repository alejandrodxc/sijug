<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idCicloFiscal,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoPresupuesto,tituloTipoP FROM 508_tiposPresupuesto";
	$arrTiposPresupuesto=$con->obtenerFilasArreglo($consulta);
	$consulta="select codigoControl,concat('[',clave,'] ',nombreObjetoGasto) from 507_objetosGasto where nivel=1 order by clave";
	$arrCapitulos=$con->obtenerFilasArreglo($consulta);
	$consulta="select idCicloFiscal FROM 550_cicloFiscal where status=1";
	$cicloVigente=$con->obtenerValor($consulta);
?>

Ext.onReady(inicializar);

var arrCiclo=<?php echo $arrCiclo?>;
var arrCapitulos=<?php echo $arrCapitulos?>;

function inicializar()
{
	var gridProductos=crearGridProductos();
    var gridDistribucion=crearGridDistribucion();
    var gridPedidoAlmacen=crearGridPedidoAlmacen();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            tbar:[{
                                                                        	xtype:'label',
                                                                        	html:'<span style="font-size:13px; font-weight:bold; color:#900;">Inventario de productos</span>'
                                                                        }],
                                            items:	[
                                            			gridProductos,
                                                        {
                                                        	xtype:'tabpanel',
                                                            id:'tabGrid',
                                                            region:'south',
                                                            height:220,
                                                            activeTab:0,
                                                            items:[
                                                        			gridDistribucion,
                                                                    gridPedidoAlmacen
                                                                  ]
                                                        }
                                            			
                                            		]
                                        }
                                     ]
						}
					)                        
}

function crearGridProductos()
{
	var tamPagina=100;  
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,120);
    cmbCiclo.setValue(<?php echo $cicloVigente ?>);
    cmbCiclo.on('select',function(combo,registro)
    						{
                            	gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                            }
    			)
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCodigoGastoCiclo'},
                                                        {name: 'idProducto'},
                                                        {name:'nombreProducto'},
                                                        {name:'idPrograma'},
                                                        {name:'programa'},
                                                        {name:'ruta'},
                                                        {name: 'cantidadAutorizada', type:'int'},
                                                        {name: 'cantidadPlaneada', type:'int'},
                                                        {name: 'cantidadEjercida', type:'int'},
                                                        {name: 'existenciaAlmacen', type:'int'},
                                                        {name: 'cantidadProgramada', type:'int'},
                                                        {name: 'almacenResponsable'},
                                                        {name: 'descripcion'},
                                                        {name: 'cve_grupo'},
                                                        {name: 'idAlmacen'},
                                                        {name: 'tipoProducto'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

	var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	{type: 'string', dataIndex: 'nombreProducto'},
                                                        				{type: 'numeric', dataIndex: 'cve_grupo'},
                                                                        {type: 'numeric', dataIndex: 'idAlmacen'}
                                                        				
                                                                    ]

                                                    }

                                                ); 
	                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'programa',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=142;
                                        proxy.baseParams.ciclo=cmbCiclo.getValue();
                                       
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});   
	 var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<table >'+
                                                    '<tr><td><span class="letraRojaSubrayada8"><b>Descripcion:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr>'+
                                                    '</table>'
                                                )
                                            });             
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  	         
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer({width:30}),
                                                    	chkRow,
                                                        expander,
                                                        {
															header:'Programa',
															width:400,
															sortable:true,
															dataIndex:'programa',
                                                            hideable:true
														},
                                                         {
															header:'Grupo',
															width:70,
															sortable:true,
                                                            hidden:true,
															dataIndex:'cve_grupo',
                                                            hideable:true
														},
                                                         {
															header:'Almac&eacute;n',
															width:70,
															sortable:true,
                                                            hidden:true,
															dataIndex:'idAlmacen',
                                                            hideable:true
														},
                                                        {
															header:'Cve. Producto',
															width:90,
															sortable:true,
															dataIndex:'idProducto',
                                                            hideable:true
														},
													 	{
															header:'Producto',
															width:400,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
														},
														{
															header:'Cantidad Autorizada',
															width:120,
															sortable:true,
															dataIndex:'cantidadAutorizada',
                                                            hideable:true
														},
                                                        {
															header:'Planeada ejecuci&oacute;n',
															width:120,
                                                            align:'left',
															sortable:true,
															dataIndex:'cantidadPlaneada',
                                                            hideable:true
														},
                                                        {
															header:'Recibido',
															width:120,
															sortable:true,
															dataIndex:'cantidadEjercida',
                                                            hideable:true

														},
                                                         {
															header:'En programaci&oacute;n <br />de entrega',
															width:120,
															sortable:true,
															dataIndex:'cantidadProgramada',
                                                            hideable:true

														},
                                                        {
															header:'Total ejercido',
															width:120,
															sortable:true,
															hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return registro.get('cantidadEjercida')+registro.get('cantidadProgramada');
                                                                    }

														},
                                                        {
															header:'Diferencia',
															width:120,
															sortable:true,
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return  registro.get('cantidadPlaneada')-(registro.get('cantidadEjercida')+registro.get('cantidadProgramada'));
                                                                    }

														},
														{
															header:'Existencia en almac&eacute;n',
															width:120,
															sortable:true,
															dataIndex:'existenciaAlmacen',
                                                            hideable:true

														},
                                                        {
															header:'Almac&eacute;n responsable',
															width:240,
															sortable:true,
															dataIndex:'almacenResponsable',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            bbar:[paginador],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            plugins:[filters,expander],
                                                            tbar:	[
                                                            			
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000"><b>Ciclo: </b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbCiclo,'-',
                                                                        {
                                                                        	id:'btnSolicitar',
                                                                        	icon:'../images/page_white_edit.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Solicitar producto a almac&eacute;n',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el tipo de producto que desea solicitar');
                                                                                        	return;
                                                                                        }
                                                                                        var filaCopia;
                                                                                    	var gridProductosPedido=gEx('gridProductosPedido');
                                                                                        var pos=obtenerPosFila(gridProductosPedido.getStore(),'idCodigoGastoCiclo',fila.get('idCodigoGastoCiclo'));
                                                                                        gEx('tabGrid').setActiveTab(1);
                                                                                        if(pos==-1)
                                                                                        {	
                                                                                        	filaCopia=fila.copy();
                                                                                            filaCopia.set('cantidadAutorizada',0);
                                                                                        	gridProductosPedido.getStore().add(filaCopia);
                                                                                            gridProductosPedido.startEditing(gridProductosPedido.getStore().getCount()-1,5);
                                                                                        }
                                                                                        else
                                                                                        	gridProductosPedido.startEditing(pos,5);
                                                                                        gEx('tabGrid').setActiveTab(1);
                                                                                    }
                                                                            
                                                                        }
                                                                    ],
                                                            			
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnSolicitar').enable();
                                                	/*if(registro.get('existenciaAlmacen')>0)
                                                    {
                                                    	gEx('btnSolicitar').enable();
                                                    }
                                                    else
                                                    	gEx('btnSolicitar').disable();*/
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridDistribucionProductos').getStore().load({params:{funcion:143,idCodigoGastoCiclo:registro.get('idCodigoGastoCiclo')}});
                                                }
    							)  
	dsTablaRegistros2.load({params:{start:0,limit:tamPagina}});                                                                            
	return tblGrid;  
}

function crearGridDistribucion()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idMes', type:'int'},
                                                        {name: 'mes'},
                                                        {name:'cantidad'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

	
	                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idMes', direction: 'ASC'},
                                                            groupField: 'idMes',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
   
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer({width:30}),
                                                        {
															header:'No. mes',
															width:100,
															sortable:true,
															dataIndex:'idMes',
                                                            hideable:true,
                                                            hidden:true
														},
													 	{
															header:'Mes',
															width:200,
															sortable:true,
															dataIndex:'mes',
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:120,
															sortable:true,
															dataIndex:'cantidad',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:'Distribuci&oacute;n del producto',
                                                            id:'gridDistribucionProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	                                                                           
	return tblGrid;  
}

function mostrarVentanaSolicitud(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cantidad a solicitar:<span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:180,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            id:'txtCantidad',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:100
                                                        },
                                                        {
                                                        	x:295,
                                                            y:10,
                                                            html:'(M&aacute;x. '+fila.get('existenciaAlmacen')+')'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha recomendada de entrega:<span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:180,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            minValue:'<?php echo date('Y-m-d')?>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Comentarios:'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:95,
                                                            width:350,
                                                            height:80,
                                                            id:'txtComentario',
                                                            xtype:'textarea'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Solicitud  de entrega',
										width: 420,
										height:270,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCantidad').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCantidad=gEx('txtCantidad');	
                                                                        
                                                                        if(txtCantidad.getValue()==0)
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar la cantidad solicitada al almac&eacute;n',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        /*if(txtCantidad.getValue()>fila.get('existenciaAlmacen'))
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgConfirm('La cantidad solcitada ('+txtCantidad.getValue()+') no puede exceder la cantidad existente en almac&eacute;n ('+fila.get('existenciaAlmacen')+')',resp3);
                                                                            return;
                                                                        }*/
                                                                        
                                                                        var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgConfirm('Debe ingresar la recomendada  de entrega',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        var obj='{"ciclo":"'+gEx('cmbCiclo').getValue()+'","idProductoConcentrado":"'+fila.get('idCodigoGastoCiclo')+'","cantidad":"'+txtCantidad.getValue()+'","fechaEntrega":"'+dteFechaEntrega.getValue().format("Y-m-d")+
                                                                        		'","comentario":"'+cv(gEx('txtComentario').getValue())+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();
                                                                                gEx('gridDistribucionProductos').getStore().removeAll();
                                                                                ventanaAM.close();
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=100&cadObj='+obj,true);


                                                                                
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridPedidoAlmacen()
{
	
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCodigoGastoCiclo'},
                                                        {name: 'idProducto'},
                                                        {name:'nombreProducto'},
                                                        {name:'idPrograma'},
                                                        {name:'programa'},
                                                        {name:'ruta'},
                                                        {name: 'cantidadAutorizada', type:'int'},
                                                        {name: 'cantidadPlaneada', type:'int'},
                                                        {name: 'cantidadEjercida', type:'int'},
                                                        {name: 'existenciaAlmacen', type:'int'},
                                                        {name: 'cantidadProgramada', type:'int'},
                                                        {name: 'almacenResponsable'},
                                                        {name: 'descripcion'},
                                                        {name: 'cve_grupo'},
                                                        {name: 'idAlmacen'},
                                                        {name: 'tipoProducto'},
                                                        {name:'almacenResponsable'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'almacenResponsable',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
    var chkRow=new Ext.grid.CheckboxSelectionModel();   
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer({width:30}),
                                                    	chkRow,
                                                       
                                                         {
															header:'Almac&eacute;n',
															width:70,
															sortable:true,
															dataIndex:'almacenResponsable',
                                                            hideable:true
														},
                                                        {
															header:'Cve. Producto',
															width:90,
															sortable:true,
															dataIndex:'idProducto',
                                                            hideable:true
														},
													 	{
															header:'Producto',
															width:400,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
														},
														{
															header:'Cantidad solicitada',
															width:120,
															sortable:true,
															dataIndex:'cantidadAutorizada',
                                                            hideable:true,
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														},
                                                        {
															header:'Almac&eacute;n responsable',
															width:240,
															sortable:true,
															dataIndex:'almacenResponsable',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridProductosPedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'center',
                                                            sm:chkRow,
                                                            clicksToEdit:1,
                                                            title:'Solicitud a almac&eacute;n',
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            tbar:	[
                                                                        {
                                                                        	id:'',
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Realizar solicitud de entrega',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	var cadPedido='';
                                                                                                var x;
                                                                                                var fila='';
                                                                                                var obj;
                                                                                                for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                                {
                                                                                                	fila=tblGrid.getStore().getAt(x);
                                                                                                    if((fila.get('cantidadAutorizada')!='0')&&(fila.get('cantidadAutorizada')!=''))
                                                                                                    {
                                                                                                    	obj='{"idAlmacen":"'+fila.get('idAlmacen')+'","idProducto":"'+fila.get('idProducto')+'","cantidad":"'+fila.get('cantidadAutorizada')+
                                                                                                        	'","idPrograma":"'+fila.get('idPrograma')+'","ruta":"'+fila.get('ruta')+'"}';
                                                                                                    	if(cadPedido=='')
                                                                                                        	cadPedido=obj;
                                                                                                        else
                                                                                                        	cadPedido+=','+obj;
                                                                                                    }
                                                                                                }
                                                                                                if(cadPedido=='')
                                                                                                {
                                                                                                	msgBox('Debe indicar la cantidad de producto que desea solicitar');
                                                                                                	return;
                                                                                                }

                                                                                                var cadObj='{"ciclo":"'+gEx('cmbCiclo').getValue()+'","registraEntradaAlmacen":"0","arrSolicitud":['+cadPedido+']}';
                                                                                                
                                                                                                
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                     	msgBox('Su solicitud ha sido registrada existosamente');
                                                                                                        tblGrid.getStore().removeAll();
                                                                                                        return;   
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=138&cadObj='+cadObj,true);
                                                                                                
                                                                                                
                                                                                                
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer realizar la socitud de entrega?',resp);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'',
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto de solicitud',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover de la solicitud');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado',resp);

                                                                                    }
                                                                            
                                                                        }
                                                                    ],
                                                            			
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
                                                    );
	                                                             
	return tblGrid;  
}
