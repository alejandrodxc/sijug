<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$conDeptos="SELECT codigoUnidad,CONCAT('[',codigoDepto,']',' ',unidad) as unidad FROM 817_organigrama WHERE codigoInstitucion='".$_SESSION["codigoInstitucion"]."' AND institucion<>1";
	$arregloDeptos=$con->obtenerFilasArreglo($conDeptos);
	$consulta="SELECT codigoControl,nombreArea FROM 9309_ubicacionesFisicas ORDER BY  nombreArea";
	$arrUbicaciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__406_tablaDinamica,descripcion FROM  _406_tablaDinamica ORDER BY descripcion";
	$arrMarcas=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);	
?>	
var arrSiNo=<?php echo $arrSiNo?>;
var arrMarcas=<?php echo $arrMarcas?>;
var arrConciliado=[['0','No'],['1','Sí']];
var arrTipoValor=[['1','Plan de inversi\xF3n'],['2','Por proyecto'],['4','No especificado']];
var arrSituacion=[['0','Baja'],['1','Activo']];

Ext.onReady(inicializar);
var arrUbicaciones=<?php echo $arrUbicaciones?>;
function inicializar()
{
	var gridInventarios=crearGridFoliado();
    var gridHistorialProducto=crearGridHistorial();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            border:false,
                                            frame:false,
                                            tbar:[{
                                                      xtype:'label',
                                                      html:'<span style="font-size:13px; font-weight:bold; color:#900;">Inventario de bienes no pertenecientes al instituto</span>'
                                                  }],
                                            items:	[
                                            			gridInventarios,
                                                        gridHistorialProducto
                                                        
                                            		]
                                        }
                                     ]
						}
                    ) 
    
}

function crearGridFoliado()
{
    var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idInventario'},
                                                                  {name: 'noInv'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'marca'},
                                                                  {name: 'modelo'},
                                                                  {name: 'numSerie'},
                                                                  {name: 'situacion'},
                                                                  {name: 'idFormularioBaja'},
                                                                  {name: 'idRegistroBaja'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
                                                    
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 50,
                                                      store: dsRegiNoFol,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )                                                     
                                                    
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=63;
                                        proxy.baseParams.tInventario=2;
                                    }
                        );
   
                                           
   var tamPagina=50;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegiNoFol,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                     
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'claveCABMS' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      },
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'noFactura' 
                                                                      },
                                                                     
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'txtRazonSocial2' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'noInv' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'anioInversion' 
                                                                      },
                                                                     {
                                                                          type:'int',
                                                                          dataIndex:'cuenta' 
                                                                      },
                                                                      {
                                                                          type:'int',
                                                                          dataIndex:'subcuenta' 
                                                                      },
                                                                      /*{
                                                                          type:'string',
                                                                          dataIndex:'marca' 
                                                                      },
                                                                      */
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'modelo' 
                                                                      },
                                                                      
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'numSerie' 
                                                                      },                                                                     
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'idPedido' 
                                                                      },                                                                     
                                                                      {
                                                                          type:'list',
                                                                          dataIndex:'conciliado' ,
                                                                          options:arrConciliado,
                                                                          phpMode:true
                                                                      },                                                                     
                                                                      {
                                                                          type:'list',
                                                                          dataIndex:'tipoValor' ,
                                                                          options:arrTipoValor,
                                                                          phpMode:true
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true}); 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'No.Inventario',
															width:100,
															sortable:true,
															dataIndex:'noInv',
                                                            renderer:function(val)
                                                            		{
                                                                    	return str_pad(val,6,'0','STR_PAD_LEFT');
                                                                    },
                                                            align:'left'
														},
                                                       
                                                        {
															header:'Nombre Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Marca',
															width:130,
															sortable:true,
															dataIndex:'marca',
                                                            align:'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrMarcas,val);
                                                                        if(pos==-1)
                                                                        	return val;
                                                                         return arrMarcas[pos][1];
                                                                    }
														},
                                                        {
															header:'Modelo',
															width:130,
															sortable:true,
															dataIndex:'modelo',
                                                            align:'right'
														},
                                                        {
															header:'No. Serie',
															width:130,
															sortable:true,
															dataIndex:'numSerie',
                                                            align:'right'
														}
                                                        
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridNoFolio',
                                                            store:dsRegiNoFol,
                                                            frame:false,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            region:'center',
                                                            loadMask:true,
                                                            border:false,
                                                            columnLines:true,
                                                            stripeRows :true,
                                                            bbar:[paginador],
                                                            plugins: [filters],
                                                            viewConfig: {
                                                                            getRowClass: function(record, rowIndex, rp, ds)
                                                                            {
                                                                            	if(record.get('situacion')=='0')
	                                                                            	return 'letraRojaFila';
                                                                            }
                                                                        },  
                                                            tbar:	[
                                                            			
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar nuevo producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaProducto();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar producto inventariado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuya informaci&oacute;n desea modificar');
                                                                                            return;
                                                                                        }
                                                                                    	mostrarVentanaProducto(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto inventariado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover');
                                                                                            return;
                                                                                        }
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGridP.getStore().remove(fila);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=146&idInventario='+fila.get('idInventario'),true);
                                                                                                
                                                                                             }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                        
                                                            		]
                                                        }
                                                    );
		
    dsRegiNoFol.load({params:{start:0 ,limit:50}})  ;
    tblGridP.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGridP.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridHistorial').getStore().load({params:{funcion:107,idInventario:registro.get('idInventario')}});
                                                }
    							)  
    return tblGridP;     
}

function crearGridHistorial()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idMovimiento'},
		                                                {name: 'fechaAsignacion', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'idResponsable'},
		                                                {name:'titularAsignacion'},
                                                        {name:'codigoUbicacion'},
		                                                {name:'ubicacion'},
                                                        {name:'comentarios'},
                                                        {name:'codigoUnidad'},
                                                        {name:'departamento'},
                                                        {name: 'idFormularioRef'},
                                                        {name: 'idRegistroRef'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
                                      

                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            
                                                            groupField: 'fechaAsignacion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false,
                                                            sortInfo: {field: 'idMovimiento', direction: 'DESC'}
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=107;
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
													 	{
															header:'Fecha de asignaci&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'fechaAsignacion',
                                                            hideable:true,
                                                            renderer:function (val,meta,registro)
                                                            					{
                                                                                	if((val!='')&&(val!=null))
	                                                                                	return val.format('d/m/Y');
                                                                                	
                                                                                }
														},
														
                                                        {
															header:'Asignado a',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'titularAsignacion',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes asignados a '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(1,\''+bE(titulo)+'\',\''+bE(registro.get('idResponsable'))+'\')">'+val+'</a>';
                                                                    }
														},
                                                        {
															header:'Departamento / &aacute;rea',
															width:250,
                                                            align :'left',
															sortable:true,
															dataIndex:'departamento',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes asignados concentrados en el departamento/&aacute;rea '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(3,\''+bE(titulo)+'\',\''+bE(registro.get('codigoUnidad'))+'\')">'+val+'</a>';
                                                                    }
														},
														{
															header:'Ubicaci&oacute;n f&iacute;sica',
															width:250,
                                                            align :'left',
															sortable:true,
															dataIndex:'ubicacion',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var titulo='Listado de bienes ubicados en '+val;
                                                                    	return '<a href="javascript:motrarVentanaBienesAsociados(2,\''+bE(titulo)+'\',\''+bE(registro.get('codigoUbicacion'))+'\')">'+val+'</a>';
                                                                    }
														},
                                                        {
															header:'Comentario',
															width:350,
                                                            sortable:true,
                                                            dataIndex:'comentarios',
                                                            hideable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(registro.get('idFormularioRef')!='')
                                                                        {
	                                                                       	comp='<a href="javascript:verInformacionBaja(\''+bE(registro.get('idFormularioRef'))+'\',\''+bE(registro.get('idRegistroRef'))+'\')">&nbsp;<img width="13" height="13" src="../images/magnifier.png" title="Ver información" alt="Ver información"></a>';
                                                                        }
                                                                        return comp+' '+val;
                                                                    }
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridHistorial',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            region:'south',
                                                            height:250,
                                                            cm: cModelo,
                                                            collapsible:true,
                                                            columnLines:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  	
	return tblGrid;                                                
}

function mostrarVentanaAsignacionResponsable(fila)
{
	var cmbUbicacion=crearComboExt('cmbUbicacion',arrUbicaciones,135,95,350);
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha de asignaci&oacute;n: '
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaAsignacion',
                                                            value:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:35,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Asignar a: '
                                                        },
                                                       cmbComboBusqueda,
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Ubicaci&oacute;n f&iacute;sica:'
                                                        },
                                                        cmbUbicacion,
														{
                                                        	x:10,
                                                            y:140,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:165,
                                                        	xtype:'textarea',
                                                            width:460,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar entrega de producto',
										width: 560,
										height:360,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaAsignacion');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de asginaci&oacute;n del bien',resp1);
                                                                        }
                                                                    	
                                                                        
                                                                        if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbComboBusqueda.focus();
                                                                            }
                                                                            msgBox('Debe indicar la persona a la cual se le asigna el bien',resp3);
                                                                            return;
                                                                        }
                                                                
                                                                		if(cmbUbicacion.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbUbicacion.focus();
                                                                            }
                                                                            msgBox('Debe indicar la ubicaci&oacute;n f&iacute;sica del bien',resp2);
                                                                            return;
                                                                        }
                                                                
                                                                		var obj='{"fechaAsignacion":"'+dteFechaEntrega.getValue().format('Y-m-d')+'","responsable":"'+cmbComboBusqueda.getValue()+'","ubicacion":"'+cmbUbicacion.getValue()+
                                                                        		'","comentarios":"'+cv(gEx('txtMotivo').getValue())+'","idInventario":"'+fila.get('idInventario')+'"}';
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridHistorial').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=111&cadObj='+obj,true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesPlaneacion.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'departamento'}
											]
										);
	var parametros=	{
						funcion:'19',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;
        dSet.baseParams.ambitoAsignacion=0
	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br><span class="letraRojaSubrayada8">Departamento:</span> {departamento}',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:135,
                                                        y:65
													}
												 );
	
   	return comboNombre;
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}

var accionVigente;
var parametroVigente;


function motrarVentanaBienesAsociados(accion,titulo,valor)
{
	accionVigente=accion;
	parametroVigente=bD(valor);

	var gridBienes=crearGridBienesAsociados();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridBienes		

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: bD(titulo),
										width: 850,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridBienesAsociados()
{
	var dsRegiNoFol=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idInventario'},
                                                                  {name: 'noInv', type:'int'},
                                                                  {name: 'claveCABMS'},
                                                                  {name: 'nombreProducto'},
                                                                  {name: 'noFactura'},
                                                                  {name: 'precioCompra'},
                                                                  {name: 'txtRazonSocial2'},
                                                                  {name: 'idProveedor'},
                                                                  {name: 'fechaCompra'},
                                                                  {name: 'anioInversion'},
                                                                  {name: 'unidad'},
                                                                  {name: 'codigoU'},
                                                                  {name: 'idResponsable'},
                                                                  {name: 'Nombre'},
                                                                  {name: 'idDetallePedido'},
                                                                  {name: 'idFormulario'},
                                                                  {name: 'idTablaDinamica'},
                                                                  {name: 'idAlmacen'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          }
                                                                                      )                             
                                                    })
                                                    
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 50,
                                                      store: dsRegiNoFol,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )                                                     
                                                    
	dsRegiNoFol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=112;
                                        proxy.baseParams.accion=accionVigente;
                                        proxy.baseParams.valor=parametroVigente;
                                    }
                        );
   
                                           
   var tamPagina=50;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegiNoFol,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                     
  
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true}); 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'No.Inventario',
															width:100,
															sortable:true,
															dataIndex:'noInv',
                                                            align:'left',
                                                            renderer:function(val)
                                                            			{
                                                                        	return str_pad(val,6,'0','STR_PAD_LEFT');
                                                                        }
														},
                                                        {
															header:'CABMS',
															width:80,
															sortable:true,
															dataIndex:'claveCABMS',
                                                            align:'left'
														},
                                                        {
															header:'Nombre Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														},
                                                        {
															header:'Factura',
															width:100,
															sortable:true,
															dataIndex:'noFactura',
                                                            align:'right'
														},
                                                        {
															header:'Costo de Inversi&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'precioCompra',
                                                            align:'right',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Proveedor',
															width:200,
															sortable:true,
															dataIndex:'txtRazonSocial2',
                                                            align:'left',
                                                            renderer:function(val,meta,registro)
                                                            		{
																		
                                                                        	return ''+val+'';
                                                                    }
                                                            
														},
                                                        {
															header:'Fecha Adquisici&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'fechaCompra',
                                                            align:'right'
														},
                                                        {
															header:'A&ntilde;o de Inversi&oacute;n',
															width:130,
															sortable:true,
															dataIndex:'anioInversion',
                                                            align:'right'
														}
													]
												);
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridBienesAsociados',
                                                            store:dsRegiNoFol,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            x:10,
                                                            y:10,
                                                            width:800,
                                                            height:350,
                                                            loadMask:true,
                                                            border:true,
                                                            columnLines:true,
                                                            stripeRows :true,
                                                            bbar:[paginador],
                                                            tbar:[{
                                                                        	icon:'../images/page_white_magnify.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Ver datos de compra del bien',
                                                                            hidden:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGridP.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto cuyos datos de compra desea observar');
                                                                                        	return;
                                                                                        }
                                                                                        mostrarVentanaDatosCompra(fila);
                                                                                    }
                                                                            
                                                                        }]
                                                        }
                                                    );
		
    dsRegiNoFol.load({params:{start:0 ,limit:50}})  ;
    return tblGridP;   
}

function mostrarVentanaDatosCompra(fila)
{
	msgBox('No existen datos de pedido asociados al bien');
}

var propiedad=null;

function mostrarVentanaProducto(fila)
{
	var idRegistro=-1;
    if(fila!=null)
    	idRegistro=fila.get('idInventario');
	var obj={};
    obj.ancho=750;
    obj.alto=480;
    obj.titulo='';
    obj.url='../modeloPerfiles/registroFormulario.php';
    obj.params=[['idFormulario',932],['idRegistro',idRegistro],['cPagina','sFrm=true'],['eJs',bE('window.parent.recargarGrid()')],['accionCancelar','window.parent.cerrarVentanaFancy()']];
	abrirVentanaFancy(obj)
}

function funcResponsableCheck(chk,value)
{
	if(value)
    {
    	var cmbResponsable=gEx('cmbResponsable');
        cmbResponsable.hide();
        var txtOtro=gEx('txtOtro');
        txtOtro.hide();
        if(chk.id=='rdoPersona')
        	cmbResponsable.show();
        else
        {
        	txtOtro.show();
        }
    }
}

function funcPropiedad(chk,value)
{
	if(value)
		propiedad=chk.value;

}

function verInformacionBaja(iF,iR)
{
	/*var objConf={};
    objConf.ancho=720;
    objConf.alto=460;
    objConf.titulo='Datos de baja';
    objConf.url='../modeloPerfiles/registroFormulario.php';
    objConf.params=[['idFormulario',bD(iF)],['idRegistro',bD(iR)],['cPagina','Sfrm=true']];
	abrirVentanaFancy(objConf);*/
    if(bD(iF)!='932')
	    verRegistroProyecto(iR,0,iF);
    else
    {
    	var obj={};
        obj.url='../modeloPerfiles/verFichaFormulario.php';
        obj.params=[['cPagina','sFrm=true'],['idFormulario',932],['idRegistro',bD(iR)],['sLectura','1']];
        obj.titulo='Ficha registro de activo';
        obj.ancho='100%';
        obj.alto='100%';
        abrirVentanaFancy(obj);
    }
}

function recargarGrid()
{
	var gridNoFolio=gEx('gridNoFolio');
    gridNoFolio.getStore().reload();
    gEx('gridHistorial').getStore().removeAll();
    cerrarVentanaFancy();
}


