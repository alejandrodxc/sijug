<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b></b></span>',
                                                items:	[
                                                         	crearGridEstructura()   
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearGridEstructura()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : false,
                                                                url: '../paginasFunciones/funcionesPlanteles.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'id',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCaracteristica'},
                                                                                                                    {name: 'caracteristica'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'}
                                                                                                                    
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    var grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    region:'center',
                                                    enableDD: false,
                                                    border:false,
                                                    autoScroll:true,
                                                    id:'arbolCaracteristica',
													store: store,
                                                    stripeRows: true,
                                                    columnLines :true,
													loadMask :true,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: '',
                                                                        dataIndex: 'caracteristica',
                                                                        width: 450
                                                                    }
                                                                    
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );

	return grid;
}