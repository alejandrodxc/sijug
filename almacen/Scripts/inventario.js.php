<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>	

Ext.onReady(inicializar);

function inicializar()
{
    crearGridInventario();
}

function crearGridInventario()
{
    var idAlmacen=gE('idAlmacen').value;
    var dsRegistrosProd=new Ext.data.JsonStore({
                                                    root: 'registros',
                                                    totalProperty: 'numReg',
                                                    autoLoad: false,
                                                    fields: [
                                                              {name: 'clave'},
                                                              {name: 'idProducto'},
                                                              {name: 'clave_Art'},
                                                              {name: 'nombreProducto'},
                                                              {name: 'existencia'},
                                                              {name: 'descripcion'}
                                                          ],         
                                                    proxy : new Ext.data.HttpProxy	(

                                                                                      {

                                                                                          url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                      }
                                                                                  )                             
                                                })
	                                                    
	dsRegistrosProd.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=28;
                                        proxy.baseParams.idAlmacen=idAlmacen;
                                    }
                        );
   
   
   var filters = new Ext.ux.grid.GridFilters	(
                                                  {
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreProducto' 
                                                                      }
                                                                  ]
                                                  }
                                              ); 
    
   var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                                        '<table ><tr height="23"><td></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td></tr><tr><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr></table>'
                                                                        
                                                					  )
                                            });  
   
   var tamPagina=100;
   var paginador=	new Ext.PagingToolbar	(
                                              {
                                                  pageSize: tamPagina,
                                                  store: dsRegistrosProd,
                                                  displayInfo: true,
                                                  disabled:false
                                              }
                                            );                                                                                    
                                               
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:25}),
                                                        expander,
                                                        {
															header:'Clave',
															width:100,
															sortable:true,
															dataIndex:'clave_Art',
                                                            align:'left'
														},
														{
															header:'Producto',
															width:500,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            align:'left'
														}
                                                        ,
                                                        {
															header:'Existencia',
															width:100,
															sortable:true,
															dataIndex:'existencia'
														}
                                                    //    ,
//                                                        {
//															header:'Acciones',
//															width:250,
//															sortable:true,
//                                                            align:'left',
//                                                            dataIndex:'cantidad'
//														}
													]
												);
	var tblGridProd=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'articulos',
                                                            x:0,
                                                            y:0,
                                                            title:'Art&iacute;culos',
                                                            store:dsRegistrosProd,
                                                            frame:true,
                                                            renderTo:'inventario',
                                                            cm: cModelo,
                                                            height:450,
                                                            width:800,
                                                            bbar: paginador,
                                                            loadMask:true,
                                                            plugins: [filters,expander]
                                                           // ,
//                                                            tbar:[
//                                                            	  {
//                                                                      text:'Registrar',
//                                                                      icon:'../images/add.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                          {
//                                                                              registrarPedido(idPedido,0);
//                                                                          }
//                                                                  }
//                                                                  ,
//                                                                  {
//                                                                      text:'Solicitud Vo.Bo. Adquisiciones',
//                                                                      id:'removerB',
//                                                                      icon:'../images/cancel_round.png',
//                                                                      cls:'x-btn-text-icon',
//                                                                      handler:function()
//                                                                          {
//                                                                              registrarPedido(idPedido,3);
//                                                                          }
//                                                                  }
//                                                                 ]
                                                        }
                                                    );
     dsRegistrosProd.load({params:{start:0,limit:tamPagina,funcion:28}})  ;     
    //return tblGridProd;   
}