<?php
session_start();
error_reporting(E_ALL);
include("latis/conexionBD.php");
include("latis/funcionesPortal.php");



if (esUsuarioLog()) {

    header('Location:' . $paginaInicioLogin);
    return;
}

$consulta = "SELECT DISTINCT archivoInclude FROM 808_configuracionEstilosMenu WHERE idConfiguracion=" . $iEstiloMenu;
$nombreRendererMenu = $con->obtenerValor($consulta);
if ($nombreRendererMenu != "") {
    include($nombreRendererMenu);
}


$consulta = "SELECT * FROM 903_variablesSistema";
$fVariable = $con->obtenerPrimeraFilaAsoc($consulta);
$tipoAutenticacion = $fVariable["tipoAutenticacion"]; //1 Sistema; 2 Servidor LDAP
?>
<!Doctype html>
<html>

<head>
    <meta charset="utf-8">

    <title><?php echo $fVariable["tituloSistema"] ?></title>

    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../estilos/fontAwesome.min.css">
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz" />
    <script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
    <script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
    <script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/xtheme-gray.css" />
    <link rel="stylesheet" type="text/css" href="./css/estilos2016.css" />

    <script type="text/javascript" src="../Scripts/base64.js"></script>
    <script type="text/javascript" src="../Scripts/funcionesAjaxV2.js"></script>
    <script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>

    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" href="../Scripts/nivoSlider/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="../Scripts/nivoSlider/themes/default/default.css" type="text/css" />
    <link rel="stylesheet" href="../Scripts/nivoSlider/themes/light/light.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../Scripts/nivoSlider/themes/dark/dark.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../Scripts/nivoSlider/themes/bar/bar.css" type="text/css" media="screen" />
    <script src="../Scripts/infiniteCarousel/jquery.infinitecarousel3Modif.js" type="text/javascript"></script>
    <!--<link rel="stylesheet" href="../Scripts/onePageScroll/onepage-scroll.css" type="text/css" media="screen" />
-->
    <script src="../Scripts/nivoSlider/jquery.nivo.slider.js" type="text/javascript"></script>
    <!--<script src="../Scripts/onePageScroll/jquery.onepage-scroll.js" type="text/javascript"></script>-->

    <script type="text/javascript" src="../modulosEspeciales_SGJ/Scripts/index.js.php"></script>
    <!--<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
-->

    <style>
        <?php
        generarFuentesLetras();


        $nConfiguracion = 0;
        $colorBoton = "";
        $tituloSistema = "";

        ?>
    </style>
    <link rel="stylesheet" type="text/css" href="../principalPortal/css/controlesFrameWork.css" />
    <link rel="stylesheet" type="text/css" href="../principalPortal/css/estiloSIUGJ.css" />

</head>

<body style="background:#F4F5F7;" id="bodyDocument">
    <table width="100%" class="tablaGlobal">
        <tr>
            <td align="center">
                <div class="rectangulo1">
                    <img class="imagenLogoGov" src="../principalPortal/imagesSIUGJ/logo_gov.png">
                </div>
                <div class="rectangulo2Index">
                    <img class="imagenLogo" src="../principalPortal/imagesSIUGJ/logoRamaJudicial2.png">
                    <img class="imagenLogo2" src="../principalPortal/imagesSIUGJ/Paleta_SIUGJ_Mesa_de_trabajo_1.png">
                    <div class="tablaOpciones">
                        <table cellspacing="0" width="350">
                            <tr>
                                <td width="150"> <span style="text-decoration:underline;">INICIO</span></td>
                                <td width="200">CONT&Aacute;CTENOS</td>
                            </tr>
                        </table>
                    </div>
                    <div class="tablaLogin">
                        <table cellspacing="0">
                            <tr>
                                <?php
                                if ($tipoAutenticacion == 2) {
                                ?>

                                    <td>

                                        <input type="text" class="campoFormulario" id="txtDominio" placeholder="Dominio" onKeyPress="ocultarError(event)">
                                    </td>
                                    <td width="60">&nbsp;&nbsp;&nbsp;
                                    </td>
                                <?php
                                }
                                ?>

                                <td>

                                    <input type="text" class="campoFormulario" id="txtUsuario" placeholder="Usuario" onKeyPress="ocultarError(event)">
                                </td>
                                <td width="20">&nbsp;&nbsp;&nbsp;
                                </td>

                                <td>
                                    <input type="password" class="campoFormulario" id="txtPasswd" placeholder="Contrase&ntilde;a" onKeyPress="ocultarError(event)">
                                </td>
                                <td width="20">&nbsp;&nbsp;&nbsp;
                                </td>
                                <td>
                                    <a href="javascript:autenticar()" style="text-decoration:none"><img src="./imagesSIUGJ/goLogin.png"></a>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="8" align="right">

                                    <span id="lblErr1" class="letraLogin" style="display:none;color:#F00; font-weight:bold">Usuario / Contrase&ntilde;a incorrecta</span>
                                    <span id="lblErr2" class="letraLogin" style="display:none;color:#F00; font-weight:bold">Debe ingresar el dominio del usuario</span>

                                </td>
                            </tr>
                            <tr height="30">
                                <td colspan="8" align="right">

                                    <table style="padding-right:30px">
                                        <tr>
                                            <td width="200">
                                                <a href="javascript:mostrarPantallaRecuperar()" class="letraLogin"><span class="">¿Olvidaste tu contrase&ntilde;a?</span></a>
                                            </td>
                                            <td width="140">
                                                <a href="javascript:mostrarPantallaRegistro()" class="letraLogin"><span class="">Reg&iacute;strate ahora</span></a>
                                            </td>
                                            <td width="140" align="right">
                                                <a href="javascript:mostrarPantallaSoporte()" class="letraLogin"><span class="">¿Necesitas ayuda?</span></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>


                </div>

            </td>
        </tr>
    </table>
    <div class="areaCentral1">
        <br>
        <table width="100%">

            <tr>
                <td align="center"><br>
                    <table width="900">
                        <tr>
                            <td width="100%" valign="top" align="center">
                                <ul id="carousel">
                                    <?php
                                    $fechaActual = strtotime(date("Y-m-d H:i:s"));
                                    $arrPublicaciones = array();
                                    $consulta = "SELECT * FROM _661_tablaDinamica WHERE bannerActivo=1";
                                    $res = $con->obtenerFilas($consulta);
                                    while ($fila = mysql_fetch_assoc($res)) //
                                    {

                                        $publicar = true;
                                        if ($fila["tipoPublicacion"] != 1) {
                                            $fInicio = strtotime($fila["fechaInicioVigencia"] . " " . $fila["horaInicioVigencia"]);
                                            $fFin = strtotime($fila["fechaFinVigencia"] . " " . $fila["horaFinVigencia"]);

                                            if (!(($fechaActual >= $fInicio) && ($fechaActual <= $fFin))) {
                                                $publicar = false;
                                            }
                                        }

                                        if (!$publicar)
                                            continue;
                                        array_push($arrPublicaciones, $fila);
                                    }

                                    foreach ($arrPublicaciones as $fila) {
                                        $ref = 'href="#"';
                                        if ($fila["vinculadoEnlace"] == 1) {
                                            switch ($fila["tipoEnlaceWeb"]) {
                                                case 1:
                                                    $ref = 'href="' . $fila["urlFuncionJava"] . '"';
                                                    break;
                                                case 2:
                                                    $ref = 'href="' . $fila["urlFuncionJava"] . '" target="blank"';
                                                    break;
                                                case 3:
                                                    $ref = 'href="javascript:' . $fila["urlFuncionJava"] . '"';
                                                    break;
                                            }
                                        }

                                        if ($fila["tipoBanner"] == 2) //Imagen
                                        {
                                            echo ' <li>
																					<a ' . $ref . '>
																					<img src="../paginasFunciones/obtenerArchivos.php?id=' . bE($fila["archivoBanner"]) . '" >
																					</a>
																				</li>';
                                        } else {

                                            //if(strpos($fila["htmlBanner"],"https://www.facebook.com")===false)
                                            {
                                                echo ' <li>
																						<iframe style="width:600px; 300px;" src="../modulosEspeciales_SGJ/contenidoPagina.php?iC=' . bE($fila["id__661_tablaDinamica"]) . '">
																						</iframe>
																						</li>';
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                                <input type="hidden" value="<?php echo count($arrPublicaciones) ?>" id="totalPublicaciones">
                            </td>
                        </tr>
                    </table>
                    <br>

                </td>


            </tr>
            <tr>
                <td colspan="3" align="right">

                </td>
            </tr>
            <tr height="250" id="filaInferior">
                <td colspan="3" align="center" valign="bottom">


                    <table width="100%" cellspacing="0">

                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="areaCentral2">
        <div class="tituloSeccionIndex">
            Noticias
        </div>
        <div class="tituloSeccionIndex2">
            Eventos
        </div>
    </div>
    <div class="areaCentral3">

    </div>
    <div class="rectangulo4">
        <br>
        <table width="100%">
            <tr>
                <td align="center">
                    <br>
                    <table>
                        <tr>
                            <td width="350" align="left" class="celdaInferior"><span class="letraBarraInferior">Cuentas de correo para Notificaciones Judiciales</span></td>
                            <td width="351" align="center" class="celdaInferior"><span class="letraBarraInferior">Pol&iacute;ticas de Privacidad y Condiciones de Uso</span></td>
                            <td width="155" align="center" class="celdaInferior"><span class="letraBarraInferior">Correo Institucional</span></td>
                            <td width="268" align="center" class="celdaInferiorUltima"><span class="letraBarraInferior">Directorio de Correos electr&oacute;nicos</span></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left"><br><br>
                                <span class="letraBarraInferior2">
                                    Calle 12 No. 7 - 65, Palacio de Justicia Alfonso Reyes Echandía, Bogotá Colombia<br>
                                    Horario de Atención Lunes a Viernes<br>
                                    8:00 a.m. a 1:00 p.m. - 2:00 p.m. a 5:00 p.m.
                                </span>

                            </td>
                            <td colspan="2"><br><br><br>
                                <span class="letraBarraInferior2">
                                    PBX: (571) 565 8500 - E-mail: info@cendoj.ramajudicial.gov.co<br>
                                    Acceder a los Canales de Atención<br>
                                    Mapa del sitio
                                </span>

                            </td>

                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <br><br>
                                <table>
                                    <tr>
                                        <td align="center" width="120">
                                            <img src="../principalPortal/imagesSIUGJ/logoFace.png">
                                        </td>
                                        <td align="center" width="120">
                                            <img src="../principalPortal/imagesSIUGJ/logoTwitter.png">
                                        </td>
                                        <td align="center" width="120">
                                            <img src="../principalPortal/imagesSIUGJ/logoInstagram.png">
                                        </td>
                                        <td align="center" width="120">
                                            <img src="../principalPortal/imagesSIUGJ/logoYoutube.png">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>
</body>

</html>