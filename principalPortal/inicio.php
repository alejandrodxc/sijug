<?php session_start(); 
	include("latis/conexionBD.php"); 
	include("latis/funcionesPortal.php");
	include("latis/configurarIdioma.php");

	if(!esUsuarioLog())
	{
		header('Location:'.$paginaCierreLogin);
		return;
	}

	
	$consulta="SELECT DISTINCT archivoInclude FROM 808_configuracionEstilosMenu WHERE idConfiguracion=".$iEstiloMenu;
	$nombreRendererMenu=$con->obtenerValor($consulta);
	if($nombreRendererMenu!="")
	{
		include($nombreRendererMenu);

	}
	if(isset($_SESSION["statusCuenta"]))
	{
		if(($_SESSION["statusCuenta"]=="2")||($_SESSION["statusCuenta"]=="3"))
			header('Location:../cuentasUsuario/nUsuariosIntermedia.php');
	}

	
	$nUsuario=obtenerNombreUsuario($_SESSION["idUsr"]);

	$nomPagina="inicio";
	
	
	$consulta="SELECT tituloSistema FROM 903_variablesSistema";
	$fSistema=$con->obtenerPrimeraFila($consulta);
	
	$consulta="SELECT * FROM 4081_configuracionPortal";
	$fRegistroConfiguracion=$con->obtenerPrimeraFilaAsoc($consulta);
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<title><?php echo $fSistema[0]?></title>

<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>

<link rel="stylesheet" type="text/css" href="./css/estilos2016.css"/>

<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/funcionesAjaxV2.js"></script>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>

<script type="text/javascript" src="../Scripts/jQuery/jquery-1.9.1.js"></script>
<script src="../Scripts/collapsible/jquery.collapsible.js"></script>

<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox.js"></script>

<link rel="stylesheet" type="text/css" href="../Scripts/ux/treeGrid/treegrid.css"/>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGridSorter.js"></script>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGridNodeUIV2.js"></script>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGridLoader.js"></script>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGridColumns.js"></script>
<script type="text/javascript" src="../Scripts/ux/treeGrid/TreeGrid.js"></script>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../modulosEspeciales_SGJP/Scripts/cGeneracionDocumentos.js.php"></script>
<link rel="stylesheet" type="text/css" href="../Scripts/classNotify/jquery.classynotty.css"/>
<script src="../Scripts/classNotify/jquery.classynotty.js"></script>

<script type="text/javascript" src="../modulosEspeciales_SGJP/Scripts/controlAgenda.js.php"></script>
<script type="text/javascript" src="../Scripts/funcionesAjaxV2.js"></script>
<script type="text/javascript" src="../Scripts/controlNotificaciones.js.php"></script>
<script type="text/javascript" src="../modulosEspeciales_SGJP/Scripts/cValidarFirmaDocumento.js.php"></script>
<script type="text/javascript" src="../modulosEspeciales_SGJP/Scripts/funcionesTableroControl.js.php"></script>
<script type="text/javascript" src="./Scripts/cNotificadores.js.php"></script>
<script type="text/javascript" src="../modulosEspeciales_SGJP/Scripts/cRevisionAlertas.js.php"></script>


<script type="text/javascript" src="../principalPortal/Scripts/funcionesAdministracion.js.php"></script>

<script type="text/javascript" src="./Scripts/inicio.js.php"></script>
<link rel="stylesheet" type="text/css" href="../principalPortal/css/controlesFrameWork.css"/>
<link rel="stylesheet" type="text/css" href="../principalPortal/css/estiloSIUGJ.css"/>
<style>
<?php
	generarFuentesLetras();
?>

</style>
<?php

	generarConfiguracionesMenus($iEstiloMenu);
	
	$nConfiguracion=0;

?>	


</head>

<body style="background-color:#1A3E9A">

<table width="100%"  class="tablaGlobal">
<tr>
	<td align="center">
    	<div class="rectangulo1">
        	<img class="imagenLogoGov" src="../principalPortal/imagesSIUGJ/logo_gov.png">
        </div>
        <div class="rectangulo2">
			<img class="imagenLogo" src="../principalPortal/imagesSIUGJ/logoRamaJudicial2.png">
            <img class="imagenLogo2" src="../principalPortal/imagesSIUGJ/Paleta_SIUGJ_Mesa_de_trabajo_1.png">
            
            <div class="etiquetaBienvenido" >Bienvenido:&nbsp; <span class="nombreUsuario"><?php echo $nUsuario?></span></div>
            <?php
			if(esAdscripcionUnidadGestion()>0)
			{
			?>
			<div class="divBusqueda" id="lblCarpetaJudicial"></div>
			<?php
			}
			?>
            
             <div class="etiquetaSalir" ><table width="70"><tr><td><a href="javascript:cerrarSesionPrincipal()">Salir</a></td><td><a href="javascript:cerrarSesionPrincipal()"><img class="" src="../principalPortal/imagesSIUGJ/Vector.png"></a></td></tr></table></div>
        </div>
        <table width="100%" cellspacing="0" style="padding:0px; border:0px">
        <tr>
            <td colspan="2">
                
            </td> 
        </tr>
       
        <tr>
            <td colspan="8" valign="top" align="center">
                
            <div >
                <div class="wrap">
                    <nav>
                    <ul class="menu" >
                        
            <?php
                genearOpcionesMenusPrincipal($nomPagina,3);
                

            ?>		</ul>
                    <div class="clearfix"></div>
                    </nav>
                </div>	
            </div>

            
                
            </td>
        </tr>
        <tr>
        	<td  colspan="8">
            	<div class="rectangulo3">
            	<div id="tblNotificacionesBar" class="tblNotificacionesBar">
                 
                </div>
               	</div>
            </td>
        </tr>
        
        
        <tr>
            
            <td align="center" colspan="8">
                <table width="100%">
                <tr>
                    
                    <td align="center" style="vertical-align:top">
                    <span id="tblTabla">
                    </span>
                    </td>
                </tr>
                </table>
                
            </td>
        </tr>
        
        </table>
        
        <div class="rectangulo4">
        <br>
			<table width="100%">
            <tr>
            	<td align="center">
                <br>
                	<table>
                    	<tr>
                        	<td width="350" align="left" class="celdaInferior"><span class="letraBarraInferior">Cuentas de correo para Notificaciones Judiciales</span></td>
                            <td width="351" align="center" class="celdaInferior"><span class="letraBarraInferior">Pol&iacute;ticas de Privacidad y Condiciones de Uso</span></td>
                            <td width="155" align="center" class="celdaInferior"><span class="letraBarraInferior">Correo Institucional</span></td>
                            <td width="268" align="center" class="celdaInferiorUltima"><span class="letraBarraInferior">Directorio de Correos electr&oacute;nicos</span></td>
                        </tr>
                        <tr>
                        	<td colspan="2" align="left"><br><br>
                            <span class="letraBarraInferior2">
                            Calle 12 No. 7 - 65, Palacio de Justicia Alfonso Reyes Echandía, Bogotá Colombia<br>
                            Horario de Atención Lunes a Viernes<br>
                            8:00 a.m. a 1:00 p.m. - 2:00 p.m. a 5:00 p.m.
                            </span>

                            </td>
                            <td colspan="2"><br><br><br>
                            <span class="letraBarraInferior2">
                            PBX: (571) 565 8500 - E-mail: info@cendoj.ramajudicial.gov.co<br>
                            Acceder a los Canales de Atención<br>
                            Mapa del sitio
                            </span>

                            </td>
                            
                        </tr>
                        <tr>
                        	<td colspan="4" align="center">
                            <br><br>
                            	<table>
                                	<tr>
                                    	<td align="center" width="120">
                                        	<img src="../principalPortal/imagesSIUGJ/logoFace.png">
                                        </td>
                                        <td align="center" width="120">
                                        	<img src="../principalPortal/imagesSIUGJ/logoTwitter.png">
                                        </td>
                                        <td align="center" width="120">
                                        	<img src="../principalPortal/imagesSIUGJ/logoInstagram.png">
                                        </td>
                                        <td align="center" width="120">
                                        	<img src="../principalPortal/imagesSIUGJ/logoYoutube.png">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            </table>
        </div>
    </td>
</tr>
</table>   
		<iframe id="framePrincipal" name="framePrincipal" src="" width="1" height="1" onload="frameLoadPrincipal(this)"></iframe> 
        <br><br>

       <form method="post"	action="" id='frmEnvioDatos'>
        <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
        
      	<input type="hidden" id="tipoFormato" value="" />
		<input type="hidden" id="idRegistroFormato" value="" />
		<input type="hidden" id="idFormulario" name="idFormulario" value="" />
		<input type="hidden" id="idRegistro" value="" />
		<input type="hidden" id="idReferencia" value="" />
		<input type="hidden" id="sL" value="" />
		<input type="hidden" id="idFormularioProceso" value="-1" />
        
    </form>
    
   
</body>
</html>