<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT ciclo,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT ciclo FROM 550_cicloFiscal where status=1";
	$ciclo=$con->obtenerValor($consulta);

	
?>

Ext.onReady(inicializar);

function inicializar()
{
	crearGridProduccion();
}

function crearGridProduccion()
{
	var arrCiclo=<?php echo $arrCiclo?>;
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,130);
    cmbCiclo.setValue('<?php echo $ciclo?>');
    cmbCiclo.on('select',function(cmb,r)
    					{
                        	gEx('gridPlaneacionMenu').getStore().reload();
                        }
    			)
	var alDatos= new Ext.data.JsonStore({
                                                
                                                totalProperty :'numReg',
                                                fields: [
                                                            {name:'idSemana'},
                                                            {name: 'lblSemana'},
                                                            {name: 'montoTotal'}
                                                        ],
                                                 proxy : new Ext.data.HttpProxy	(
                                                                                      {
                                                                                          url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          
                                                                                      }
    
                                                                                  ),
                                                sortInfo: {field: 'lblSemana', direction: 'ASC'},
                                                autoLoad:true,
                                                root:'registros',
                                                remoteSort: false
                                            }
                                          );
        
        alDatos.on('beforeload',function(proxy)
                                        {
                                            proxy.baseParams.funcion=124;
                                            proxy.baseParams.ciclo=gEx('cmbCiclo').getValue();
                                            proxy.baseParams.tipoProduccion=gE('tipoProduccion').value;
                                        }
                            )

		                
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Semana planeada',
                                                                width:400,
                                                                sortable:true,
                                                                dataIndex:'lblSemana'
                                                            },
                                                            {
                                                                header:'Monto total',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'montoTotal',
                                                                renderer:'usMoney'
                                                            }
                                                        ]
                                                    )
		var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridPlaneacionMenu',
                                                                store:alDatos,
                                                                renderTo:'tblGridProduccion',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                stripeRows :true,
                                                                width:750,
                                                                height:350,
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                                html:'<span class="letraRojaSubrayada8"><b>Ciclo:</b></span>&nbsp;&nbsp;&nbsp;'
                                                                            },
                                                                            cmbCiclo,'-',
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar planeaci&oacute;n por semana',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaAgregarProduccion();
                                                                                        }
                                                                                
                                                                            },
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover planeaci&oacute;n',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(fila==null)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la planeaci&oacute;n semanal que desea eliminar');
                                                                                            	return;
                                                                                            }
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                     	tblGrid.getStore().remove(fila);   
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=125&idSemana='+fila.get('idSemana')+'&ciclo='+gEx('cmbCiclo').getValue()+'&tipoProduccion='+gE('tipoProduccion').value,true);
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer eliminar la planeaci&oacute;n semanal seleccionada?',resp);
                                                                                        }
                                                                                
                                                                            },'-'
                                                                            ,
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Modificar planeaci&oacute;n',
                                                                                handler:function()
                                                                                        {
                                                                                         	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(fila==null)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la planeaci&oacute;n semanal que desea modificar');
                                                                                            	return;
                                                                                            } 
                                                                                            var arrParam=[['idSemana',fila.get('idSemana')],['ciclo',gEx('cmbCiclo').getValue()],['tipoProduccion',gE('tipoProduccion').value]];
                                                                                            enviarFormularioDatos('../almacenesProduccion/planeacionProduccionViveres.php',arrParam);  
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                		]
                                                             }
                                                 )
}      

function mostrarVentanaAgregarProduccion()
{
	var cmbSemana=crearComboExt('cmbSemana',[],135,5,420);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Semana a planear:'
                                                        },cmbSemana

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar planeaci&oacute;n semanal',
										width: 600,
										height:130,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	if(cmbSemana.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar la semana cuya planeaci&oacute;n desea realizar');
                                                                        	return;
                                                                        }
                                                                    	function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                var arrParam=[['idSemana',cmbSemana.getValue()],['ciclo',gEx('cmbCiclo').getValue()],['tipoProduccion',gE('tipoProduccion').value]];
																			    enviarFormularioDatos('../almacenesProduccion/planeacionProduccionViveres.php',arrParam); 
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=126&idSemana='+cmbSemana.getValue()+'&ciclo='+gEx('cmbCiclo').getValue()+'&tipoProduccion='+gE('tipoProduccion').value,true);
                                                                    	
																		 
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
    obtenerSemanasDisponibles(ventanaAM);
}                                           

function obtenerSemanasDisponibles(ventana)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            gEx('cmbSemana').getStore().loadData(arrDatos);
            ventana.show();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=127&ciclo='+gEx('cmbCiclo').getValue()+'&tipoProduccion='+gE('tipoProduccion').value,true);
              
               
}