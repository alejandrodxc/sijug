<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select min(fechaCreacion) from _894_tablaDinamica s where s.idEstado=1";
	$minSolicitud=$con->obtenerValor($consulta);
	if($minSolicitud=="")
		$minSolicitud=date("Y-m-d");
	else
		$minSolicitud=date("Y-m-d",strtotime($minSolicitud));	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
	///var gridInsumos=crearGridInsumosRequeridos();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos
                                                        
                                                        
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var tamPagina=100;
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idSolicitudEntrega'},
		                                                {name: 'fechaCreacion', type:'date', dateFormat:'Y-m-d'},
		                                                {name:'txtFolioPedido'},
                                                         {name:'idProducto'},
		                                                {name:'txtNombreProducto'},
                                                        {name:'cantidad', type:'int'},
                                                        {name: 'unidad'},
                                                        {name: 'Nombre'},
                                                        {name: 'existenciaAlmacen', type:'int'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				
                                                                        {type: 'string', dataIndex: 'txtNombreProducto'},
                                                                        {type: 'string', dataIndex: 'unidad'},
                                                                        {type: 'string', dataIndex: 'Nombre'},
                                                                        {type: 'date', dataIndex: 'fechaCreacion'}
                                                                        
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaCreacion', direction: 'ASC'},
                                                            groupField: 'fechaCreacion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='108';
                                        proxy.baseParams.fechaInicio=gEx('dtePeriodoDel').getValue().format('Y-m-d');
                                        proxy.baseParams.fechaFin=gEx('dtePeriodoAl').getValue().format('Y-m-d');
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Fecha solicitud',
															width:110,
															sortable:true,
                                                            dataIndex:'fechaCreacion',
                                                            hideable:true,
                                                             renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!='')&&(val!='0000-00-00'))
	                                                                    	return val.format('d/m/Y');
                                                                         return '';
                                                                    },
														},
													 	{
															header:'Folio pedido',
															width:120,
															sortable:true,
															dataIndex:'txtFolioPedido',
                                                            hideable:true
														},
														{
															header:'Producto solicitado',
															width:130,
															sortable:true,
															dataIndex:'txtNombreProducto',
                                                           
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Cantidad',
															width:110,
															sortable:true,
															dataIndex:'cantidad',
                                                            
                                                            hideable:true
														},
                                                         {
															header:'Existencia almac&eacute;n',
															width:130,
															sortable:true,
															dataIndex:'existenciaAlmacen',
                                                            
                                                            hideable:true
														},
                                                        {
															header:'Diferencia',
															width:130,
															sortable:true,
															renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color='green';
                                                                        var diferencia=registro.get('existenciaAlmacen')-registro.get('cantidad');
                                                                        if(diferencia<0)
                                                                        {
                                                                        	color='red';
                                                                        }
                                                                        return '<font color="'+color+'">'+diferencia+'</font>';
                                                                    },
                                                            
                                                            hideable:true
														},
                                                        
														{
															header:'Unidad solicitante',
															width:350,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'unidad',
                                                            hideable:true
														},
                                                        {
															header:'Responsable solicitud',
															width:250,
                                                            align :'left',
															sortable:true,
                                                            dataIndex:'Nombre',
                                                            hideable:true
														}
                                                         
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'Periodo del: &nbsp;&nbsp;'
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            id:'dtePeriodoDel',
                                                                            value:'<?php echo $minSolicitud ?>',
                                                                            listeners:	{
                                                                            				select:function()
                                                                                            		{
                                                                                                    	gEx('gridProductos').getStore().reload();
                                                                                                        gEx('gridInsumos').getStore().reload();
                                                                                                    }
                                                                                            
                                                                            			}
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:' &nbsp;&nbsp; &nbsp;&nbsp;Periodo al:&nbsp;&nbsp;'
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            id:'dtePeriodoAl',
                                                                            value:'<?php echo date("Y-m-d",strtotime("+7 days ",strtotime($minSolicitud)))?>',
                                                                            listeners:	{
                                                                            				select:function()
                                                                                            		{
                                                                                                    	gEx('gridProductos').getStore().reload();
                                                                                                        gEx('gridInsumos').getStore().reload();
                                                                                                    }
                                                                                            
                                                                            			}
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Entregado',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la solicitud que desea marcar como entregado');
                                                                                        	return;
                                                                                        }
                                                                                        if(parseFloat(fila.get('cantidad'))>parseFloat(fila.get('existenciaAlmacen')))
                                                                                        {
                                                                                        	msgBox('La cantidad existente en el almac&eacute;n es menor que la cantidad solicitada');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                                var idFormulario=605;
                                                                                                var cadObj='{"idFormulario":"'+idFormulario+'","cantidad":"'+fila.get('cantidad')+'","idProducto":"'+fila.get('idProducto')+'","idSolicitud":"'+fila.get('idSolicitudEntrega')+'"}';
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGrid.getStore().reload();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=133&cadObj='+cadObj,true);
                                                                                                
                                                                                                
                                                                                        	}
                                                                                       	}
                                                                                        msgConfirm('Est&aacute; seguro de querer marcar la solicitud seleccionada como entregada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Rechazar Solicitud',
                                                                            handler:function()
                                                                            		{
                                                                                    }
                                                                         }
                                                            		],
                                                            	
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Solicitudes de productos</b></span>',
                                                            columnLines:true,
                                                            plugins:[filters],
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            bbar:[paginador],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:108}});                                                  
	return tblGrid;                                                                                                     
}

function mostrarVentanaRechazarSolicitud(fila)
{	

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Motivo del rechazo: <font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Rechazar solicitud',
										width: 420,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                                														
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=102&idSolicitud='+fila.get('idSolicitudEntrega')+'&situacion=3&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             }
                                                                    	
                                                                       	}
                                                                        msgConfirm('Est&aacute; seguro de querer rechazar la solicitud de entrega seleccionada?',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	

}


function mostrarVentanaAgendarSolicitud(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha solicitada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:10,
                                                            xtype:'label',
                                                            html:fila.get('fechaLimitePrefenteEntrega').format('d/m/Y')
                                                        },
                                            			{
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha programada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            value:fila.get('fechaLimitePrefenteEntrega').format('d/m/Y'),
                                                            minValue:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        
														{
                                                        	x:10,
                                                            y:70,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:100,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agendar entrega',
										width: 420,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de entrega del producto',resp1);
                                                                        }
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=103&fechaEntrega='+dteFechaEntrega.getValue().format('Y-m-d')+'&idSolicitud='+fila.get('idSolicitudEntrega')+'&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridInsumosRequeridos()
{
	var tamPagina=100;
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'producto'},
		                                                {name:'cantidad', type:'int'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'producto', direction: 'ASC'},
                                                            groupField: 'producto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='109';
                                        proxy.baseParams.fechaInicio=gEx('dtePeriodoDel').getValue().format('Y-m-d');
                                        proxy.baseParams.fechaFin=gEx('dtePeriodoAl').getValue().format('Y-m-d');
                                        
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
    
	   
    
	
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Producto',
															width:350,
															sortable:true,
                                                            dataIndex:'producto',
                                                            hideable:true
														},
													 	{
															header:'Cantidad',
															width:120,
															sortable:true,
															dataIndex:'cantidad',
                                                            hideable:true
														}
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	
                                                            	
                                                            id:'gridInsumos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            collapsible:true,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Insumos requeridos para cubrir la demanda de productos</b></span>',
                                                            columnLines:true,
                                                            height:250,
                                                            region: 'south',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Solicitar producto a almacen',
                                                                            handler:function()
                                                                            		{
                                                                                    	var listaProductos='';
                                                                                        var x;
                                                                                        var obj='';
                                                                                        
                                                                                        
                                                                                        
                                                                                        for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                        {
                                                                                        	obj='{"idProducto":"'+tblGrid.getStore().getAt(x).get('idProducto')+'","cantidad":"'+tblGrid.getStore().getAt(x).get('cantidad')+'"}';
                                                                                            if(listaProductos=='')
                                                                                            	listaProductos=obj;
                                                                                            else
                                                                                            	listaProductos+=','+obj;
                                                                                        }
                                                                                        
                                                                                        var gridProductos=gEx('gridProductos');
                                                                                         var listaSolicitudes='';
                                                                                        for(x=0;x<gridProductos.getStore().getCount();x++)
                                                                                        {
                                                                                        	
                                                                                            if(listaSolicitudes=='')
                                                                                            	listaSolicitudes=gridProductos.getStore().getAt(x).get('idSolicitudEntrega');
                                                                                            else
                                                                                            	listaSolicitudes+=','+gridProductos.getStore().getAt(x).get('idSolicitudEntrega');
                                                                                        }
                                                                                        
                                                                                        var fechaEntrega=gEx('dteFechaEntrega').getValue().format('Y-m-d');
                                                                                        var obj='{"productos":['+listaProductos+'],"listaSolicitudes":"'+listaSolicitudes+'","fechaEntrega":"'+fechaEntrega+'"}';

                                                                                       function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                gEx('gridProductos').getStore().reload();
                                                                                                gEx('gridInsumos').getStore().reload();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=110&cadObj='+obj,true);

                                                                                        
                                                                                        
                                                                                    }
                                                                               },
                                                                                    '-',
                                                                                    {
                                                                                    	html:'Fecha requerida entrega producto:&nbsp;&nbsp;',
                                                                                        xtype:'label',
                                                                                    },
                                                                                    {
                                                                                    	xtype:'datefield',
                                                                                        id:'dteFechaEntrega',
                                                                                        value:'<?php echo date('Y-m-d')?>',
                                                                                        minValue:'<?php echo date('Y-m-d')?>'
                                                                                    }
                                                                            
                                                                        
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:109}});                                                  
	return tblGrid;     
}