<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__698_tablaDinamica,txtTipoBaja FROM  _698_tablaDinamica ORDER BY txtTipoBaja";
	$arrTiposBaja=$con->obtenerFilasArreglo($consulta);
	
?>

var arrTiposBaja=<?php echo $arrTiposBaja?>;

function mostrarVentanaBaja()
{
	var fila=gEx('grid_tblTabla').getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el producto cuya baja desea registrar');
    	return;
    }
    
//    
	var cmbMotivo=crearComboExt('cmbMotivo',arrTiposBaja,100,35,250);
    cmbMotivo.setValue(arrTiposBaja[0][0]);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Cantidad:'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            id:'txtCantidad',
                                                            allowDecimals:true,
                                                            allowNegative:true,
                                                            width:60
                                                        },
                                                        {
                                                        	x:175,
                                                            y:10,
                                                            html:' de '+fila.get('existencia')
                                                        },
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Motivo de baja:'
                                                        },
                                                        cmbMotivo,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Comentarios:'
                                                        }
                                                        ,
                                                        {
                                                        	x:100,
                                                            y:65,
                                                            id:'txtComentarios',
                                                            xtype:'textarea',
                                                            width:350,
                                                            height:80
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Baja de producto',
										width: 500,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCantidad').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCantidad=gEx('txtCantidad');
                                                                        if(txtCantidad.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar la cantidad de producto a dar de baja');
                                                                        	return;
                                                                        }
                                                                       
                                                                        if(txtCantidad.getValue()>parseFloat(fila.get('existencia')))
                                                                        {
                                                                        	msgBox('La cantidad de producto que desea dar de baja es mayor que la existente');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idProducto":"'+fila.get("idProducto")+'","idFormulario":"'+gE('idFormulario').value+
                                                                        	'","cantidad":"'+txtCantidad.getValue()+'","idMotivo":"'+cmbMotivo.getValue()+
                                                                            '","comentarios":"'+cv(gEx('txtComentarios').getValue())+'"}';
                                                                            
                                                                        function resp(btn)    
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('grid_tblTabla').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=130&cadObj='+cadObj,true);
                                                                        	}
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la baja del producto seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}