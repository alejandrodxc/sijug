<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT ciclo,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT ciclo FROM 550_cicloFiscal where status=1";
	$ciclo=$con->obtenerValor($consulta);
	$consulta="SELECT idSemana,descripcion FROM 9355_semanaReceta WHERE anio=".$ciclo;
	$arrSemanas=$con->obtenerFilasArreglo($consulta);
?>
var arrCiclo=<?php echo $arrCiclo?>;
var arrSemanas=<?php echo $arrSemanas?>;
var arrSituaciones=[['0','En espera de producci&oacute;n'],['1','Producido'],['2','Producci&oacute;n con retraso'],['3','Cancelado']];

Ext.onReady(inicializar);

function inicializar()
{
	var gridProductos=crearGridProductos();
	var gridInsumos=crearGridInsumosRequeridos();
	new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			gridProductos,
                                                        gridInsumos
                                                        
                                            		]
                                        }
                                     ]
						}
                    )   

}

function crearGridProductos()
{
	var tamPagina=100;
    var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,120);
    cmbCiclo.setValue(<?php echo $ciclo?>);
    cmbCiclo.on('select',function(cmb,registro)
    					{
                        	gEx('gridProductos').getStore().load({url:'../paginasFunciones/funcionesAlmacen.php',params:{idSemana:gEx('cmbSemana').getValue(),ciclo:gEx('cmbCiclo').getValue(),tipoProduccion:gE('tipoProduccion').value}});
                        }
    			)
    var cmbSemana=crearComboExt('cmbSemana',arrSemanas,0,0,350);
    cmbSemana.setValue(arrSemanas[0][0]);
    cmbSemana.on('select',function(cmb,registro)
    					{
                        	gEx('gridProductos').getStore().load({url:'../paginasFunciones/funcionesAlmacen.php',params:{idSemana:gEx('cmbSemana').getValue(),ciclo:gEx('cmbCiclo').getValue(),tipoProduccion:gE('tipoProduccion').value}});
                        }
    			)
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			
		                                                {name: 'fechaMenu'},
                                                        {name: 'idReceta'},
                                                        {name: 'fecha', type:'date', dateFormat:'d/m/Y'},
		                                                {name:'receta'},
		                                                {name:'cantidad'},
                                                        {name: 'situacion'},
                                                        {name: 'comentarios'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fecha', direction: 'ASC'},
                                                            groupField: 'fechaMenu',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=128;
                                        proxy.baseParams.idSemana=gEx('cmbSemana').getValue();
                                        proxy.baseParams.ciclo=gEx('cmbCiclo').getValue();
                                        proxy.baseParams.tipoProduccion=gE('tipoProduccion').value;
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel();              
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        {
															header:'Fecha Men&uacute;',
															width:110,
															sortable:true,
                                                            dataIndex:'fechaMenu',
                                                            hideable:true
														},
                                                        {
															header:'Fecha Programada',
															width:110,
															sortable:true,
                                                            dataIndex:'fecha',
                                                            hideable:true,
                                                            renderer:function(val)
                                                            		{
																		if(val)
                                                                        	
                                                                    		return val.format('d/m/Y');
                                                                        return val;
                                                                    }
														},
													 	{
															header:'Receta',
															width:300,
															sortable:true,
															dataIndex:'receta',
                                                            hideable:true
														},
														{
															header:'Porciones',
															width:130,
															sortable:true,
															dataIndex:'cantidad',
                                                            hideable:true
														},
														{
															header:'Situacion',
															width:230,
															sortable:true,
															dataIndex:'situacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='#030';
                                                                        if(val=='2')
                                                                        	color='#F00';
                                                                    	return '<font color="'+color+'">'+formatearValorRenderer(arrSituaciones,val)+'</font>';
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'Comentarios',
															width:350,
															sortable:true,
															dataIndex:'comentarios',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span class="letraRojaSubrayada8"><b>Ciclo: </b></span>&nbsp;&nbsp;'
                                                                        },
                                                                       cmbCiclo,'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span class="letraRojaSubrayada8"><b>Semana: </b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbSemana,'-',
                                                                        {
                                                                        	icon:'../images/Reinscribir.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Producido',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelections();
                                                                                        if(fila.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la producci&oacute;n que desea marcar como producido');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        
                                                                                                var arrRecetas='';
                                                                                                var x;
                                                                                                var obj;
                                                                                                for(x=0;x<fila.length;x++)
                                                                                                {
                                                                                                    obj='{"cantidad":"'+fila[x].get('cantidad')+'","idReceta":"'+fila[x].get('idReceta')+'","fecha":"'+fila[x].get('fechaMenu')+'"}';
                                                                                                    if(arrRecetas=='')
                                                                                                        arrRecetas=obj;
                                                                                                    else
                                                                                                        arrRecetas+=','+obj;
                                                                                                }
                                                                                                
                                                                                                var idFormulario=gE('idFormulario').value;
                                                                                                var cadObj='{"idFormulario":"'+idFormulario+'","arrRecetas":['+arrRecetas+']}';
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        tblGrid.getStore().reload();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=131&cadObj='+cadObj,true);
                                                                                                
                                                                                                
                                                                                        	}
                                                                                       	}
                                                                                        msgConfirm('Est&aacute; seguro de querer marcar los elementos seleccionados como producidos?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Cancelar producci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelections();
                                                                                        if(fila.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la producci&oacute;n que desea marcar como cancelada');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                        
                                                                                                var arrRecetas='';
                                                                                                var x;
                                                                                                var obj;
                                                                                                for(x=0;x<fila.length;x++)
                                                                                                {
                                                                                                    obj='{"cantidad":"'+fila[x].get('cantidad')+'","idReceta":"'+fila[x].get('idReceta')+'","fecha":"'+fila[x].get('fechaMenu')+'"}';
                                                                                                    if(arrRecetas=='')
                                                                                                        arrRecetas=obj;
                                                                                                    else
                                                                                                        arrRecetas+=','+obj;
                                                                                                }
                                                                                                
                                                                                                var idFormulario=gE('idFormulario').value;
                                                                                                var cadObj='{"motivo":"@motivo","idFormulario":"'+idFormulario+'","arrRecetas":['+arrRecetas+']}';
                                                                                                mostrarVentanaMotivo(cadObj);
                                                                                                
                                                                                                
                                                                                        	}
                                                                                       	}
                                                                                        msgConfirm('Est&aacute; seguro de querer marcar los elementos seleccionados como cancelados?',resp);
                                                                                        	
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            	
                                                            id:'gridProductos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Producci&oacute;n programada</b></span>',
                                                            columnLines:true,
                                                            region: 'center',
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
	dsTablaRegistros2.load({params:{start:0,limit:100,funcion:128}});  
    chkRow.on('rowselect',function()
    						{
                            	gEx('gridInsumos').getStore().removeAll();
                                gEx('btnPedido').disable();
                            }
    		)   
    chkRow.on('rowdeselect',function()
    						{
                            	gEx('gridInsumos').getStore().removeAll();
                                gEx('btnPedido').disable();
                            }
    		)                                                
	return tblGrid;                                                                                                     
}

function mostrarVentanaRechazarSolicitud(fila)
{	

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Motivo del rechazo: <font color="red">*</font>'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Rechazar solicitud',
										width: 420,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridProductos').getStore().load({params:{start:0,limit:tamPagina}});    
                                														
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=102&idSolicitud='+fila.get('idSolicitudEntrega')+'&situacion=3&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             }
                                                                    	
                                                                       	}
                                                                        msgConfirm('Est&aacute; seguro de querer rechazar la solicitud de entrega seleccionada?',resp)
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	

}


function mostrarVentanaAgendarSolicitud(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha solicitada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:10,
                                                            xtype:'label',
                                                            html:fila.get('fechaLimitePrefenteEntrega').format('d/m/Y')
                                                        },
                                            			{
                                                        	x:10,
                                                            y:40,
                                                            html:'Fecha programada de entrega: '
                                                        },
                                                        {
                                                        	x:170,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega',
                                                            value:fila.get('fechaLimitePrefenteEntrega').format('d/m/Y'),
                                                            minValue:'<?php echo date("Y-m-d") ?>'
                                                        },
                                                        
														{
                                                        	x:10,
                                                            y:70,
                                                            html:'Comentarios: '
                                                        },
                                                        {
                                                        	x:20,
                                                            y:100,
                                                        	xtype:'textarea',
                                                            width:360,
                                                            height:80,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agendar entrega',
										width: 420,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var dteFechaEntrega=gEx('dteFechaEntrega');
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de entrega del producto',resp1);
                                                                        }
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el motivo del rechazo',resp2);
                                                                        }
                                                                
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();    
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=103&fechaEntrega='+dteFechaEntrega.getValue().format('Y-m-d')+'&idSolicitud='+fila.get('idSolicitudEntrega')+'&comentarios='+cv(txtMotivo.getValue()),true);
                                                                             
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridInsumosRequeridos()
{
	var tamPagina=100;
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'cantidad'},
                                                        {name: 'existencia'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=129;
                                        var gridInsumos=gEx('gridProductos');
                                        var fila=gridInsumos.getSelectionModel().getSelections();
                                        var arrInsumos='';
                                        var x;
                                        for(x=0;x<fila.length;x++)
                                        {
                                            objInsumo='{"idReceta":"'+fila[x].get('idReceta')+'","cantidad":"'+fila[x].get('cantidad')+'"}';
                                            if(arrInsumos=='')
                                                arrInsumos=objInsumo;
                                            else
                                                arrInsumos+=','+objInsumo;
                                        }
                                      	gEx('btnPedido').disable();
                                        proxy.baseParams.arrInsumos='['+arrInsumos+']';
                                        proxy.baseParams.tipoProduccion=gE('tipoProduccion').value
                                    }
                        )   
    
	
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	{
															header:'Producto',
															width:350,
															sortable:true,
                                                            dataIndex:'nombreProducto',
                                                            hideable:true
														},
													 	{
															header:'Cantidad',
															width:120,
															sortable:true,
															dataIndex:'cantidad',
                                                            hideable:true
														},
                                                        {
															header:'Existencia en almac&eacute;n',
															width:120,
															sortable:true,
															dataIndex:'existencia',
                                                            hideable:true
														},
                                                        {
															header:'Cantidad d&eacute;ficit',
															width:120,
															renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color="#006";
                                                                       
                                                                    	var diferencia=parseFloat(registro.get('existencia'))-parseFloat(registro.get('cantidad'));
                                                                        if(diferencia<0)
                                                                        {
                                                                        	color="#FF0000";
                                                                            gEx('btnPedido').enable();
                                                                        }
                                                                        return '<font color="'+color+'">'+diferencia+'</font>';
                                                                        	
                                                                    }
														}
                                                       
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	
                                                            	
                                                            id:'gridInsumos',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            collapsible:true,
                                                            title:'<span class="letraRojaSubrayada8" style="font-size:12px"><b>Insumos requeridos para cubrir la demanda</b></span>',
                                                            columnLines:true,
                                                            height:250,
                                                            region: 'south',
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/formularios/calculator_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Calcular insumos requeridos',
                                                                            handler:function()
                                                                            		{
                                                                                    	var gridInsumos=gEx('gridProductos');
                                                                                        var fila=gridInsumos.getSelectionModel().getSelections();
                                                                                        if(fila.length==0)
                                                                                        {
                                                                                            msgBox('Debe seleccionar al menos una receta para calcular los insumos requeridos');
                                                                                            return;
                                                                                        }
                                                                                     	gEx('gridInsumos').getStore().reload();
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-'
                                                            			,
                                                            			{
                                                                        	icon:'../images/Reinscribir.png',
                                                                            id:'btnPedido',
                                                                            disabled:true,
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:true,
                                                                            text:'Generar pedido de producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var listaProductos='';
                                                                                        var x;
                                                                                        var obj='';
                                                                                        for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                        {
                                                                                        	obj='{"idProducto":"'+tblGrid.getStore().getAt(x).get('idProducto')+'","cantidad":"'+tblGrid.getStore().getAt(x).get('cantidad')+'"}';
                                                                                            if(listaProductos=='')
                                                                                            	listaProductos=obj;
                                                                                            else
                                                                                            	listaProductos+=','+obj;
                                                                                        }
                                                                                        
                                                                                        var gridProductos=gEx('gridProductos');
                                                                                        var listaSolicitudes='';
                                                                                       
                                                                                        
                                                                                        var fechaEntrega=gEx('dteFechaEntrega').getValue().format('Y-m-d');
                                                                                        var obj='{"productos":['+listaProductos+'],"listaSolicitudes":"'+listaSolicitudes+'","fechaEntrega":"'+fechaEntrega+'"}';
																						
                                                                                       function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                gEx('gridProductos').getStore().reload();
                                                                                                gEx('gridInsumos').getStore().reload();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=110&cadObj='+obj,true);

                                                                                        
                                                                                        
                                                                                    }
                                                                               },
                                                                                    '-',
                                                                                    {
                                                                                    	html:'Fecha requerida entrega producto:&nbsp;&nbsp;',
                                                                                        xtype:'label',
                                                                                        hidden:true
                                                                                    },
                                                                                    {
                                                                                    	xtype:'datefield',
                                                                                        hidden:true,
                                                                                        id:'dteFechaEntrega',
                                                                                        value:'<?php echo date('Y-m-d')?>',
                                                                                        minValue:'<?php echo date('Y-m-d')?>'
                                                                                    }
                                                                            
                                                                        
                                                            		],
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)  
	
    							                                               
                                               
	return tblGrid;     
}

function mostrarVentanaMotivo(cadObj)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el motivo de la cancelaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:30,
                                                            xtype:'textarea',
                                                            id:'txtMotivo',
                                                            width:400,
                                                            height:80
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelaci&oacute;n de producci&oacute;n',
										width: 470,
										height:200,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar el motivo de la cancelaci&oacute;n');
                                                                        	return;
                                                                        }
                                                                        
                                                                        cadObj=cadObj.replace('@motivo',cv(txtMotivo.getValue()));
																		function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProductos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=132&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
	
}