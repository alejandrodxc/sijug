<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$tipoProduccion=$_GET["tProduccion"];
	$consulta="";
	$lblReceta="Receta";
	$ocultarHorarioMenu='false';
	$consulta="SELECT id__621_tablaDinamica,txtNomenu FROM _621_tablaDinamica ORDER BY txtNomenu";
	if(($tipoProduccion==0)||($tipoProduccion==1)||($tipoProduccion==2))
	{}		
	else
	{
		$lblReceta="Sustancia";
		$ocultarHorarioMenu='true';
	}
	$arrHorario=$con->obtenerFilasArreglo($consulta);
	if(($tipoProduccion==0)||($tipoProduccion==1)||($tipoProduccion==2))
		$consulta="SELECT id__609_tablaDinamica,idracion,CostoTotal FROM _609_tablaDinamica ORDER BY idracion";
	else
		$consulta="SELECT id__605_tablaDinamica,txtNombreProducto,costoTotalProducto FROM _605_tablaDinamica ORDER BY txtNombreProducto";
	
	$arrRecetas=$con->obtenerFilasArreglo($consulta);
?>

var arrHorario=<?php echo $arrHorario?>;
var arrRecetas=<?php echo $arrRecetas?>;
var ocultarHorarioMenu=<?php echo $ocultarHorarioMenu?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridProduccion=creaGridProduccion();
}

function creaGridProduccion()
{
	var cmbHorario=crearComboExt('cmbHorario',arrHorario);
    var cmbRecetas=crearComboExt('cmbRecetas',arrRecetas);
    
	/*var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPlaneacion'},
		                                                {name: 'idReceta'},
		                                                {name:'lunes'},
		                                                {name: 'martes'},
		                                                {name:'miercoles'},
		                                                {name:'jueves'},
                                                        {name:'viernes'},
                                                        {name: 'sabado'},
                                                        {name: 'domingo'},
                                                        {name: 'cantidadTotal'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'montoTotal'},
                                                        {name: 'horarioMenu'}
                                                        
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreReceta', direction: 'ASC'},
                                                            groupField: 'horarioMenu',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        })  */                                     
	
     var alDatos= new Ext.data.JsonStore({
                                                
                                                totalProperty :'numReg',
                                                fields: [
                                                            {name:'idPlaneacion'},
                                                            {name: 'idReceta'},
                                                            {name:'lunes'},
                                                            {name: 'martes'},
                                                            {name:'miercoles'},
                                                            {name:'jueves'},
                                                            {name:'viernes'},
                                                            {name: 'sabado'},
                                                            {name: 'domingo'},
                                                            {name: 'cantidadTotal'},
                                                            {name: 'costoUnitario'},
                                                            {name: 'montoTotal'},
                                                            {name: 'horarioMenu'}
                                                        ],
                                                 proxy : new Ext.data.HttpProxy	(
                                                                                      {
                                                                                          url: '../paginasFunciones/funcionesAlmacen.php'
                                                                                          
                                                                                      }
    
                                                                                  ),
                                                sortInfo: {field: 'horarioMenu', direction: 'DESC'},
                                                autoLoad:true,
                                                root:'registros',
                                                remoteSort: false
                                            }
                                          );
        
        alDatos.on('beforeload',function(proxy)
                                        {
                                            proxy.baseParams.funcion=122;
                                            proxy.baseParams.ciclo=gE('ciclo').value;
                                            proxy.baseParams.semana=gE('semana').value;
                                            proxy.baseParams.tipoProduccion=gE('tipoProduccion').value;
                                        }
                            )

		var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editorFila',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:2
                                                        }
                                                    );                            
		
        
        editorFila.on('afteredit',funcEditorFilaAfterEdit)                                                
        editorFila.on('beforeedit',funcEditorFilaBeforeEdit)
        editorFila.on('validateedit',funcEditorValida);
        editorFila.on('canceledit',funcEditorCancelEdit);
        var summary = new Ext.ux.grid.GridSummary();                
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Horario men&uacute;',
                                                                width:140,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrHorario,val);
                                                                        },
                                                                dataIndex:'horarioMenu',
                                                                hidden:ocultarHorarioMenu,
                                                                editor:cmbHorario
                                                            },
                                                            {
                                                                header:'<?php echo $lblReceta?>',
                                                                width:200,
                                                                sortable:true,
                                                                editor:cmbRecetas,
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrRecetas,val);
                                                                        },
                                                                dataIndex:'idReceta'
                                                            },
                                                            {
                                                                header:'Lun',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'lunes',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Mar',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'martes',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Mi&eacute;',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'miercoles',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Jue',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'jueves',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Vie',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'viernes',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'S&aacute;b',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'sabado',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Dom',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'domingo',
                                                                editor:{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Cant. total',
                                                                width:60,
                                                                sortable:true,
                                                                dataIndex:'cantidadTotal'
                                                            },
                                                            {
                                                                header:'Costo unitario',
                                                                width:90,
                                                                sortable:true,
                                                                renderer:'usMoney',
                                                                dataIndex:'costoUnitario'
                                                            },
                                                            {
                                                                header:'Monto total',
                                                                width:100,
                                                                sortable:true,
                                                                renderer:'usMoney',
                                                                summaryType:'sum',
                                                                dataIndex:'montoTotal'
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridPlaneacionMenu',
                                                                store:alDatos,
                                                                renderTo:'tblGridProduccion',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                stripeRows :true,
                                                                width:1000,
                                                                height:350,
                                                                plugins:[editorFila,summary],
                                                                loadMask:true,
                                                                tbar:	[
                                                                			{
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar receta',
                                                                                id:'btnAgregar',
                                                                                handler:function()
                                                                                        {
                                                                                            
                                                                                            var editorFila=gEx('editorFila');
                                                                                            var registroGrid=crearRegistro	(
                                                                                            									[
                                                                                                                                    {name:'idPlaneacion'},
                                                                                                                                    {name: 'idReceta'},
                                                                                                                                    {name:'lunes'},
                                                                                                                                    {name: 'martes'},
                                                                                                                                    {name:'miercoles'},
                                                                                                                                    {name:'jueves'},
                                                                                                                                    {name:'viernes'},
                                                                                                                                    {name: 'sabado'},
                                                                                                                                    {name: 'domingo'},
                                                                                                                                    {name: 'cantidadTotal'},
                                                                                                                                    {name: 'costoUnitario'},
                                                                                                                                    {name: 'montoTotal'},
                                                                                                                                    {name: 'horarioMenu'}
                                                                                                                                ]
                                                                                            								);
                                                                                            var nReg=new registroGrid	(
                                                                                                                            {
                                                                                                                            	idPlaneacion:-1,
                                                                                                                                idReceta:'',
                                                                                                                                lunes:0,
                                                                                                                                martes:0,
                                                                                                                                miercoles:0,
                                                                                                                                jueves:0,
                                                                                                                                viernes:0,
                                                                                                                                sabado:0,
                                                                                                                                domingo:0,
                                                                                                                                cantidadTotal:0,
                                                                                                                                costoUnitario:0,
                                                                                                                                montoTotal:0,
                                                                                                                                horarioMenu:''
                                                                                                                            }
                                                                                                                        )
                                                                                            
                                                                                            editorFila.stopEditing();
                                                                                            tblGrid.getStore().add(nReg);
                                                                                            tblGrid.nuevoRegistro=true;
                                                                                            editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                            Ext.getCmp('btnAgregar').disable();
                                                                                            Ext.getCmp('btnRemover').disable();	
                                                                                        }
                                                                                
                                                                            },
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover receta',
                                                                                id:'btnRemover',
                                                                                handler:function()
                                                                                        {
                                                                                            var filas=tblGrid.getSelectionModel().getSelected();
                                                                                            if(filas==null)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la planeaci&oacute;n que desea eliminar');
                                                                                            	return;
                                                                                            }
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            tblGrid.getStore().remove(filas);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=123&idPlaneacion='+filas.get('idPlaneacion'),true);
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer eliminar la planeaci&oacute;n seleccionada?',resp);
                                                                                        }
                                                                                
                                                                            }
                                                                		]/*,
                                                                view: new Ext.grid.GroupingView(	{
                                                                                                        forceFit:false,
                                                                                                        showGroupName: false,
                                                                                                        enableNoGroups:false,
                                                                                                        enableGroupingMenu:false,
                                                                                                        enableGrouping:false,
                                                                                                        hideGroupedColumn: false
                                                                                                    }   
                                                                                                )*/
                                                            }
                                                        );
		tblGrid.nuevoRegistro=false;                                                        
        return 	tblGrid;	
}

function funcEditorFilaAfterEdit(rowEdit,obj,registro,nFila)
{
	var total=parseInt(obj.lunes)+parseInt(obj.martes)+parseInt(obj.miercoles)+parseInt(obj.jueves)+parseInt(obj.viernes)+parseInt(obj.sabado)+parseInt(obj.domingo);
    registro.set('cantidadTotal',total);
    var pos=existeValorMatriz(arrRecetas,obj.idReceta);
    var costo=parseFloat(arrRecetas[pos][2]);
    registro.set('costoUnitario',costo);
    montoTotal=total*costo;
    registro.set('montoTotal',montoTotal);
	return true;
}

function funcAntesEditCampo(e)
{
	if((e.grid.soloLectura)&&(!e.grid.nuevoRegistro))
		e.cancel=true;
}

function funcEditorFilaBeforeEdit(rowEdit,fila)
{
	var idGrid='gridPlaneacionMenu';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

var validarActivo=false;

function funcEditorValida(rowEdit,obj,registro,nFila)
{
	var idGrid='gridPlaneacionMenu';
	var grid=Ext.getCmp(idGrid);
    
    <?php
	$lblExiste='La receta ya se encuentra asignado al horario seleccionado';
	if(($tipoProduccion==0)||($tipoProduccion==1)||($tipoProduccion==2))
	{
	?>
    if(obj.horarioMenu=='')
    {
    	msgBox('Debe indicar el horario en el cual ser&aacute; incluida la receta');
    	return false;
    }
    <?php
	}
	else
		$lblExiste="La sustancia ya ha sido agregada previamente";
	?>
    if(obj.idReceta=='')
    {
    	msgBox('Debe indicar la <?php echo $lblReceta?> a incluir en el men&uacute;');
    	return false;
    }

	var x;
    var fila;
    if(obj.horarioMenu==undefined)
    	obj.horarioMenu=0;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        if((fila.get('idReceta')==obj.idReceta)&&(fila.get('horarioMenu')==obj.horarioMenu)&&(fila.get('idPlaneacion')!=registro.get('idPlaneacion')))
        {
        	msgBox('<?php echo $lblExiste?>');
        	return false;
        }
    }

	if(obj.lunes=='')
    {
    	obj.lunes=0;
        registro.set('lunes',0);
    }
     
    if(obj.martes=='')
    {
    	obj.martes=0;
        registro.set('martes',0);
    }
    if(obj.miercoles=='')
    {
    	obj.miercoles=0;
        registro.set('miercoles',0);
    }
    if(obj.jueves=='')
    {
    	obj.jueves=0;
        registro.set('jueves',0);
    }
    if(obj.viernes=='')
    {
    	obj.viernes=0;
        registro.set('viernes',0);
    }
    if(obj.sabado=='')
    {
    	obj.sabado=0;
        registro.set('sabado',0);
    }
    if(obj.domingo=='')
    {
    	obj.domingo=0;
        registro.set('domingo',0);
    }
    
    var total=parseInt(obj.lunes)+parseInt(obj.martes)+parseInt(obj.miercoles)+parseInt(obj.jueves)+parseInt(obj.viernes)+parseInt(obj.sabado)+parseInt(obj.domingo);
    var pos=existeValorMatriz(arrRecetas,obj.idReceta);
    var costo=parseFloat(arrRecetas[pos][2]);
    montoTotal=total*costo;
   
    var cadObj='{"tipoProduccion":"'+gE('tipoProduccion').value+'","cantidadTotal":"'+total+'","costoUnitario":"'+costo+'","montoTotal":"'+montoTotal+'","semana":"'+gE('semana').value+'","ciclo":"'+gE('ciclo').value+'","idPlaneacion":"'+registro.get('idPlaneacion')+'","horarioMenu":"'+obj.horarioMenu+
    			'","idReceta":"'+obj.idReceta+'","lunes":"'+obj.lunes+'","martes":"'+obj.martes+'","miercoles":"'+obj.miercoles+'","jueves":"'+obj.jueves+
                '","viernes":"'+obj.viernes+'","sabado":"'+obj.sabado+'","domingo":"'+obj.domingo+'"}';
	
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            grid.nuevoRegistro=false;
            registro.set('idPlaneacion',arrResp[1]);
            Ext.getCmp('btnAgregar').enable();
            Ext.getCmp('btnRemover').enable();
        }
        else
        {
        	Ext.getCmp('btnAgregar').enable();
            Ext.getCmp('btnRemover').enable();
        	if(registro.get('idPlaneacion')!='-1')
            {
                var copiaRegistro=grid.copiaRegistro;
                var x=0;
                var arrCampos=grid.getStore().fields;
                var filaDestino=grid.registroEdit;
                for(x=0;x<arrCampos.items.length;x++)
                {
                    filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
            
                }
            }
            else
            {
            	grid.getStore().removeAt(grid.getStore().getCount()-1);
            }
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=121&cadObj='+cadObj,true);
                
    
    
    
    return true;   
}

function funcEditorCancelEdit(rowEdit,cancelado)
{
	
	var idGrid='gridPlaneacionMenu';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnAgregar').enable();
    Ext.getCmp('btnRemover').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }
   
	grid.nuevoRegistro=false;
	
}