<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idStatus,nombreStatus FROM 4005_status";
	$arrSituacion=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
	
?>
var arrElementos=[];
var arrSiNo=<?php echo $arrSiNo?>;
var arrSituacion=<?php echo $arrSituacion?>;
Ext.onReady(inicializar);

function inicializar()
{
	var arrObjPerfil=eval(bD(gE('arrObjPerfil').value));

    var objPerfil=arrObjPerfil.length>0?arrObjPerfil[0]:{};
    
    var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion,140,105,120);
    cmbSituacion.setValue(objPerfil.situacionActual?objPerfil.situacionActual:'1');
    
	var arrPaneles=[];
    arrPaneles.push({
                        xtype:'panel',
                        title:'Datos Generales',
                        baseCls: 'x-plain',
                        defaultType: 'label',
                        layout:'absolute',                        
                        items:	[
                                    {
                                        x:10,
                                        y:10,
                                        xtype:'label',
                                        html:'Nombre del perfil:'
                                    },
                                    {
                                        x:140,
                                        y:5,
                                        value:objPerfil.nombrePerfil?objPerfil.nombrePerfil:'',
                                        id:'txtPerfil',
                                        xtype:'textfield',
                                        width:350
                                    },
                                    {
                                        x:10,
                                        y:40,
                                         xtype:'label',
                                        html:'Descripci&oacute;n:'
                                    },
                                    {
                                        x:140,
                                        y:35,
                                        id:'txtDescripcion',
                                        xtype:'textarea',
                                        value:objPerfil.descripcion?escaparBR(objPerfil.descripcion,true):'',
                                        height:60,
                                        width:350
                                    },
                                    {
                                        x:10,
                                        y:110,
                                         xtype:'label',
                                        html:'Situaci&oacute;n:'
                                    },
                                    cmbSituacion,
                                    {
                                        x:140,
                                        y:150,
                                        width:80,
                                        heigt:25,
                                        xtype:'button',
                                        icon:'../images/guardar.PNG',
                                        cls:'x-btn-text-icon',
                                        text:'Guardar',
                                        handler:function()
                                                {
                                                    var cadObj='{"nombrePerfil":"'+cv(gEx('txtPerfil').getValue())+
                                                                '","descripcion":"'+cv(gEx('txtDescripcion').getValue())+
                                                                '","situacion":"'+cmbSituacion.getValue()+'","idRegistro":"'+gE('idRegistro').value+
                                                                '","configuracion":"'+gEN('configuracion')[1].value+'"}';
                                                
                                                
                                                    function funcAjax()
                                                    {
                                                        var resp=peticion_http.responseText;
                                                        arrResp=resp.split('|');
                                                        if(arrResp[0]=='1')
                                                        {
                                                        	function respAux()
                                                            {
                                                                var arrParam=[['configuracion',gEN('configuracion')[1].value]];
                                                                enviarFormularioDatos('../moduloPresupuestal/perfilEstructuraPresupuestal.php',arrParam);
															}
                                                            msgBox('La informaci&oacute;n ha sido almacenada correctamente',respAux);
                                                        }
                                                        else
                                                        {
                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                        }
                                                    }
                                                    obtenerDatosWeb('../moduloPresupuestal/paginasFunciones/funcionesModuloPresupuestal.php',funcAjax, 'POST','funcion=1&cadObj='+cadObj,true);
                                                    
                                                
                                                }
                                        
                                    }
                                ]
                   } );
	if(gE('idRegistro').value!='-1')
    {                   
        arrPaneles.push(
                            
                           {
                                xtype:'panel',
                                title:'Definici&oacute;n de estructura program&aacute;tica',
                                baseCls: 'x-plain',
                                defaultType: 'label',
                                layout:'border',
                                items:	[
                                			crearGridElementosProgramaticos()
                                        ]
                            }
                        )                   
                   
	}

	
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Crear Perfil de Estructura Program&aacute;tica</b></span>',
                                                items:	[
                                                			{
                                                            	xtype:'tabpanel',
                                                                region:'center',
                                                                id:'tPanel',
                                                                defaultType: 'label',
                                                                activeTab:arrPaneles.length>1?1:0,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/salir.gif',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Regresar',
                                                                                handler:function()
                                                                                        {
                                                                                            regresarPagina();
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                items:	arrPaneles
                                                            }
                                                           
                                                        ]
                                            }
                                         ]
                            }
                        )   
	gEx('tPanel').setActiveTab(0);
}


function crearGridElementosProgramaticos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
		                                                {name: 'orden'},
		                                                {name:'nombreElemento'},		                                                
                                                        {name:'abreviatura'},
                                                        {name: 'descripcion'},
                                                        {name: 'dependeOtroElemento'},
                                                        {name: 'elementoDependencia'},
                                                        {name: 'urlModulo'},
                                                        {name: 'espacioNombres'},
                                                        {name: 'etiquetaSingular'},
                                                        {name: 'articuloSingular'},
                                                        {name: 'etiquetaPlural'},
                                                        {name: 'articuloPlural'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../moduloPresupuestal/paginasFunciones/funcionesModuloPresupuestal.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'orden', direction: 'ASC'},
                                                            groupField: 'orden',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='3';
                                        proxy.baseParams.idPerfil=gE('idRegistro').value;
                                    }
                        )   
	alDatos.on('load',function(proxy)
    								{	
                                    	arrElementos=[];
                                    	var x;
                                        var o;
                                        for(x=0;x<proxy.reader.jsonData.registros.length;x++)
                                        {
                                        	o=proxy.reader.jsonData.registros[x];
                                            arrElementos.push([o.idRegistro, o.nombreElemento+' ('+o.abreviatura+')']); 
                                        }
                                    	gEx('gElementosEstructura').getView().refresh();
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	{
                                                                header:'Orden',
                                                                width:65,
                                                                sortable:true,
                                                                dataIndex:'orden'
                                                            },
                                                            {
                                                                header:'Nombre Elemento',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'nombreElemento'
                                                            },
                                                            {
                                                                header:'Abreviatura',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'abreviatura'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:370,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	meta.attr='style="white-space:normal;"';
                                                                            
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Depende de<br>otro elemento',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'dependeOtroElemento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSiNo,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Elemento del cual depende',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'elementoDependencia',
                                                                renderer:function(val)
                                                                		{

                                                                        	return formatearValorRenderer(arrElementos,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'URL del m&oacute;dulo',
                                                                width:370,
                                                                sortable:true,
                                                                dataIndex:'urlModulo'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gElementosEstructura',
                                                                store:alDatos,
                                                                
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar Nuevo Elemento',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaElementoProgramatico();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Modificar Elemento',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            mostrarVentanaElementoProgramatico(fila);
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover Elemento',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                    function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            gEx('gElementosEstructura').getStore().remove(fila);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[1]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../moduloPresupuestal/paginasFunciones/funcionesModuloPresupuestal.php',funcAjax, 'POST','funcion=4&idPerdilEstructura='+gE('idRegistro').value+'&idElemento='+fila.data.idRegistro,true);
                                                                                        		}
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function mostrarVentanaElementoProgramatico(f)
{
	var arrOrden=[];
    var x;
    var totalOrden=gEx('gElementosEstructura').getStore().getCount();
    if(!f)
    	totalOrden++;
    for(x=1;x<=totalOrden;x++)
    {
	    arrOrden.push([x,x]);
    }
    
    var cmbOrden=crearComboExt('cmbOrden',arrOrden,140,65,90);
    
    if(!f)
    {
    	cmbOrden.setValue(arrOrden[arrOrden.length-1][0]);
    }
    else
    {
    	cmbOrden.setValue(f.data.orden);
    }
    var cmbDependeOtroElemento=crearComboExt('cmbDependeOtroElemento',arrSiNo,175,5,115);
    
    cmbDependeOtroElemento.on('select',function(cmb,registro)
    									{
                                        	if(registro.data.id=='1')
                                            {
                                            	
                                            	cmbElementoDependencia.enable();
                                            }
                                            else
                                            {
                                            	
                                            	cmbElementoDependencia.setValue('');
                                            	cmbElementoDependencia.disable();
                                            }
                                        }
    						)
    
    var arrIgnorar=[];
    if(f)
    	arrIgnorar.push(f.data.idElemento);
     var arrDependencia=obtenerElementosProgramaticos(arrIgnorar);

    var cmbElementoDependencia=crearComboExt('cmbElementoDependencia',arrDependencia ,175,35,280);
    cmbElementoDependencia.disable();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'border',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'tabpanel',
                                                            region:'center',
                                                            id:'tabElemento',
                                                            baseCls: 'x-plain',
                                                            activeTab:0,
                                                            items:	[
                                                            			{
                                                                        	xtype:'panel',
                                                                            title:'Datos Generales',
                                                                            layout:'absolute',
                                                                            baseCls: 'x-plain',
                                                                            defaultType: 'label',
                                                                            items:	[
                                                                            			{
                                                                                            x:10,
                                                                                            y:10,
                                                                                            html:'Nombre del Elemento:'
                                                                                        },
                                                                                        {
                                                                                        	x:140,
                                                                                            y:5,
                                                                                            id:'nombreElemento',
                                                                                            width:300,
                                                                                            xtype:'textfield'
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:40,
                                                                                            html:'Abreviatura:'
                                                                                        },
                                                                                        {
                                                                                        	x:140,
                                                                                            y:35,
                                                                                            id:'abreviatura',
                                                                                            width:150,
                                                                                            xtype:'textfield'
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:70,
                                                                                            html:'Orden:'
                                                                                        },
                                                                                        cmbOrden,
                                                                                        {
                                                                                            x:10,
                                                                                            y:100,
                                                                                            html:'Descripci&oacute;n:'
                                                                                        },
                                                                                        {
                                                                                        	x:10,
                                                                                            y:130,
                                                                                            width:450,
                                                                                            height:60,
                                                                                            xtype:'textarea',
                                                                                            id:'txtDescripcion'
                                                                                        }	
                                                                            		]
                                                                        },
                                                                        {
                                                                        	xtype:'panel',
                                                                            title:'Configuraci&oacute;n de m&oacute;dulo',
                                                                            layout:'absolute',
                                                                            baseCls: 'x-plain',
                                                                            defaultType: 'label',
                                                                            items:	[
                                                                            			{
                                                                                            x:10,
                                                                                            y:10,
                                                                                            html:'¿Depende de otro Elemento?:'
                                                                                        },
                                                                                        cmbDependeOtroElemento,
                                                                                        {
                                                                                            x:10,
                                                                                            y:40,
                                                                                            html:'Indique el Elemento:'
                                                                                        },
                                                                                        cmbElementoDependencia,
                                                                                        {
                                                                                            x:10,
                                                                                            y:70,
                                                                                            html:'URL m&oacute;dulo de administraci&oacute;n:'
                                                                                        },
                                                                                        {
                                                                                        	x:175,
                                                                                            y:65,
                                                                                            xtype:'textfield',
                                                                                            id:'txtUrlModulo',
                                                                                            value:'',
                                                                                            width:280
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:100,
                                                                                            html:'Espacio de nombre del modulo:'
                                                                                        },
                                                                                        {
                                                                                        	x:175,
                                                                                            y:95,
                                                                                            xtype:'textfield',
                                                                                            id:'txtEspacioNombre',
                                                                                            value:'',
                                                                                            width:200
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:130,
                                                                                            html:'Etiqueta Singular / Art&iacute;culo:'
                                                                                        },
                                                                                        {
                                                                                        	x:175,
                                                                                            y:125,
                                                                                            xtype:'textfield',
                                                                                            id:'txtEtiquetaSingular',
                                                                                            value:'',
                                                                                            width:150
                                                                                        },
                                                                                        {
                                                                                            x:338,
                                                                                            y:130,
                                                                                            html:' / '
                                                                                        },
                                                                                        {
                                                                                        	x:355,
                                                                                            y:125,
                                                                                            xtype:'textfield',
                                                                                            id:'txtArticuloSingular',
                                                                                            value:'',
                                                                                            width:80
                                                                                        },
                                                                                        {
                                                                                        
                                                                                            x:10,
                                                                                            y:160,
                                                                                            html:'Etiqueta Plural / Art&iacute;culo:'
                                                                                        },
                                                                                        {
                                                                                        	x:175,
                                                                                            y:155,
                                                                                            xtype:'textfield',
                                                                                            id:'txtEtiquetaPlural',
                                                                                            value:'',
                                                                                            width:150
                                                                                        },
                                                                                        {
                                                                                            x:338,
                                                                                            y:160,
                                                                                            html:' / '
                                                                                        },
                                                                                        {
                                                                                        	x:355,
                                                                                            y:155,
                                                                                            xtype:'textfield',
                                                                                            id:'txtArticuloPlural',
                                                                                            value:'',
                                                                                            width:80
                                                                                        }
                                                                            		]
                                                                        }
                                                            		]
                                                            
                                                        }
                                            			
                                                        		
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !f?'Agregar Nuevo Elemento':'Modificar Elemento',
										width: 500,
										height:310,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nombreElemento').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var nombreElemento=gEx('nombreElemento');
                                                                        var abreviatura=gEx('abreviatura');
                                                                        var cmbOrden=gEx('cmbOrden');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        var urlModulo=gEx('txtUrlModulo');
                                                                        var txtEspacioNombre=gEx('txtEspacioNombre');
                                                                        var txtEtiquetaSingular=gEx('txtEtiquetaSingular');
                                                                        var txtEtiquetaPlural=gEx('txtEtiquetaPlural');
                                                                        var txtArticuloSingular=gEx('txtArticuloSingular');                                                                        
                                                                        var txtArticuloPlural=gEx('txtArticuloPlural');
                                                                        
                                                                        
                                                                        if(nombreElemento.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(0);
                                                                            	nombreElemento.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del elemento',resp1);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(abreviatura.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(0);
                                                                            	abreviatura.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la abreviatura del elemento',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbOrden.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(0);
                                                                            	cmbOrden.focus();
                                                                            }
                                                                            msgBox('Debe indicar el orden del elemento',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbDependeOtroElemento.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	cmbDependeOtroElemento.focus();
                                                                            }
                                                                            msgBox('Debe indicar si el elemento tiene dependencia con otro elemento',resp4);
                                                                            return;
                                                                        }
                                                                        else
                                                                        {
                                                                        	if(cmbDependeOtroElemento.getValue()=='1')
                                                                            {
                                                                                if(cmbElementoDependencia.getValue()=='')
                                                                                {
                                                                                    function resp5()
                                                                                    {
                                                                                        gEx('tabElemento').setActiveTab(1);
                                                                                        cmbElementoDependencia.focus();
                                                                                    }
                                                                                    msgBox('Debe indicar el elemento con el cual se tiene dependencia',resp5);
                                                                                    return;
                                                                                }
																			}                                                                        
                                                                        }
                                                                        
                                                                        
                                                                        if(urlModulo.getValue()=='')
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	urlModulo.focus();
                                                                            }
                                                                            msgBox('Debe indicar la URL del módulo del elemento',resp6);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtEspacioNombre.getValue()=='')
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	txtEspacioNombre.focus();
                                                                            }
                                                                            msgBox('Debe indicar el espacio de nombres del m&oacute;dulo asociado al elemento',resp7);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtEtiquetaSingular.getValue()=='')
                                                                        {
                                                                        	function resp8()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	txtEtiquetaSingular.focus();
                                                                            }
                                                                            msgBox('Debe indicar la etiqueta en singular del elemento',resp8);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtArticuloSingular.getValue()=='')
                                                                        {
                                                                        	function resp9()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	txtArticuloSingular.focus();
                                                                            }
                                                                            msgBox('Debe indicar el art&iacute;culo en singular del elemento',resp9);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtEtiquetaPlural.getValue()=='')
                                                                        {
                                                                        	function resp10()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	txtEtiquetaPlural.focus();
                                                                            }
                                                                            msgBox('Debe indicar la etiqueta en plural del elemento',resp10);
                                                                            return;
                                                                        }
                                                                        
                                                                         if(txtArticuloPlural.getValue()=='')
                                                                        {
                                                                        	function resp11()
                                                                            {
                                                                            	gEx('tabElemento').setActiveTab(1);
                                                                            	txtArticuloPlural.focus();
                                                                            }
                                                                            msgBox('Debe indicar el art&iacute;culo en plural del elemento',resp11);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        
																		 var cadObj='{"idPerdilEstructura":"'+gE('idRegistro').value+'","nombreElemento":"'+cv(nombreElemento.getValue())+
                                                                         '","abreviatura":"'+cv(abreviatura.getValue())+'","orden":"'+cmbOrden.getValue()+
                                                                         '","descripcion":"'+cv(txtDescripcion.getValue())+'","dependeOtroElemento":"'+
                                                                         cmbDependeOtroElemento.getValue()+'","elementoDependencia":"'+cmbElementoDependencia.getValue()+'",'+
                                                                         '"urlModulo":"'+cv(urlModulo.getValue())+'","espacioNombres":"'+cv(txtEspacioNombre.getValue())+
                                                                         '","etiquetaSingular":"'+cv(txtEtiquetaSingular.getValue())+'","articuloSingular":"'+
                                                                         cv(txtArticuloSingular.getValue())+'","etiquetaPlural":"'+cv(txtEtiquetaPlural.getValue())+
                                                                         '","articuloPlural":"'+cv(txtArticuloPlural.getValue())+'","idElemento":"'+(!f?-1:f.data.idRegistro)+'"}';
																	
                                                                    	
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	reestructurarGridEstrutura();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../moduloPresupuestal/paginasFunciones/funcionesModuloPresupuestal.php',funcAjax, 'POST','funcion=2&cadObj='+cadObj,true);
                                                                    
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
    if(f)
    {
    	var nombreElemento=gEx('nombreElemento');
        nombreElemento.setValue(f.data.nombreElemento);
        var abreviatura=gEx('abreviatura');
        abreviatura.setValue(f.data.abreviatura);
        var cmbOrden=gEx('cmbOrden');
        cmbOrden.setValue(f.data.orden);
        var txtDescripcion=gEx('txtDescripcion');
        txtDescripcion.setValue(escaparBR(f.data.descripcion,true));
        var urlModulo=gEx('txtUrlModulo');
        urlModulo.setValue(f.data.urlModulo);
        var txtEspacioNombre=gEx('txtEspacioNombre');
        txtEspacioNombre.setValue(f.data.espacioNombres);
        var txtEtiquetaSingular=gEx('txtEtiquetaSingular');
        txtEtiquetaSingular.setValue(f.data.etiquetaSingular);
        var txtEtiquetaPlural=gEx('txtEtiquetaPlural');
        txtEtiquetaPlural.setValue(f.data.etiquetaPlural);
        var txtArticuloSingular=gEx('txtArticuloSingular');
        txtArticuloSingular.setValue(f.data.articuloSingular);                                                                        
        var txtArticuloPlural=gEx('txtArticuloPlural');
        txtArticuloPlural.setValue(f.data.articuloPlural);
        
        cmbDependeOtroElemento.setValue(f.data.dependeOtroElemento);
        dispararEventoSelectCombo('cmbDependeOtroElemento');  
        cmbElementoDependencia.setValue(f.data.elementoDependencia);
        
    }
    
}


function obtenerElementosProgramaticos(arrIgnorar)
{
	var arrElementos=[];
    var x;
    var fila;
    var gElementosEstructura=gEx('gElementosEstructura');
    for(x=0;x<gElementosEstructura.getStore().getCount();x++)
    {
    	fila=gElementosEstructura.getStore().getAt(x);
        
        if(existeValorArreglo(arrIgnorar,fila.data.idElemento)==-1)
        {
        	arrElementos.push([fila.data.idRegistro,fila.data.nombreElemento+' ('+fila.data.abreviatura+')']);
    	}
    }

    return arrElementos;
    
}

function reestructurarGridEstrutura()
{
	gEx('gElementosEstructura').getStore().reload();
}