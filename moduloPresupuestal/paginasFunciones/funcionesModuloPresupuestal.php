<?php session_start();
	include("latis/conexionBD.php"); 
	include_once("latis/funcionesEnvioMensajes.php"); 
	
	$parametros="";
	if(isset($_POST["funcion"]))
	{
		$funcion=$_POST["funcion"];
		if(isset($_POST["param"]))
		{
			$p=$_POST["param"];
			$parametros=json_decode($p,true);
			
		}
	}	
	
	switch($funcion)
	{
		case 1: 
			registrarEstructuraProgramatica();
		break;
		case 2: 
			registrarElementoEstructuraProgramatica();
		break;
		case 3: 
			obtenerElementoEstructuraProgramatica();
		break;
		case 4: 
			removerElementoEstructuraProgramatica();
		break;
	}
	
	
	
	function registrarEstructuraProgramatica()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		if($obj->idRegistro==-1)
		{
		
			$consulta="INSERT INTO 1300_perfilesEstructuraProgramatica(nombrePerfil,descripcion,situacionActual,fechaRegistro,responsableRegistro)
				VALUES('".cv($obj->nombrePerfil)."','".cv($obj->descripcion)."',".$obj->situacion.",'".date("Y-m-d H:i:s")."',".$_SESSION["idUsr"].")";	
		}
		else
		{
			$consulta="UPDATE 1300_perfilesEstructuraProgramatica SET nombrePerfil='".cv($obj->nombrePerfil)."',descripcion='".cv($obj->descripcion).
					"',situacionActual='".$obj->situacion."' WHERE idRegistro=".$obj->idRegistro;	
		}
		
		
		if($con->ejecutarConsulta($consulta))
		{
			if($obj->idRegistro==-1)
			{
				$obj->idRegistro=$con->obtenerUltimoID();
				setAtributoCadJson($_SESSION["configuracionesPag"][$obj->configuracion]["parametros"],"idRegistro",$obj->idRegistro);
			}
			
			
			
			echo "1|".$obj->idRegistro;
		}
		
		
	}
	
	function registrarElementoEstructuraProgramatica()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		$x=0;
		$consulta[$x]="begin";
		$x++;
		
		
		if($obj->idElemento==-1)
		{
			$consulta[$x]="UPDATE 1301_elementosEstructuraProgramatica SET orden=orden+1 WHERE idPerfilEstructura=".$obj->idPerdilEstructura." AND orden>=".$obj->orden;
			$x++;
			$consulta[$x]="INSERT INTO 1301_elementosEstructuraProgramatica(idPerfilEstructura,nombreElemento,abreviatura,orden,descripcion,
							dependeOtroElemento,elementoDependencia,urlModulo,espacioNombres,etiquetaSingular,articuloSingular,etiquetaPlural,articuloPlural)
							VALUES(".$obj->idPerdilEstructura.",'".cv($obj->nombreElemento)."','".cv($obj->abreviatura)."',".$obj->orden.
							",'".cv($obj->descripcion)."',".$obj->dependeOtroElemento.",".($obj->elementoDependencia==""?"NULL":$obj->elementoDependencia).
							",'".cv($obj->urlModulo)."','".cv($obj->espacioNombres)."','".cv($obj->etiquetaSingular)."','".cv($obj->articuloSingular).
							"','".cv($obj->etiquetaPlural)."','".cv($obj->articuloPlural)."')";
			$x++;
		}
		else
		{
			$query="SELECT orden FROM 1301_elementosEstructuraProgramatica WHERE idRegistro=".$obj->idElemento;
			$orden=$con->obtenerValor($query);
			
			
			if($orden!=$obj->orden)
			{
				if($orden>$obj->orden)
				{
					$consulta[$x]="UPDATE 1301_elementosEstructuraProgramatica SET orden=orden+1 WHERE idPerfilEstructura=".$obj->idPerdilEstructura.
							" AND orden>=".$obj->orden." and orden<".$orden;
					$x++;
				}
				else
				{
					$consulta[$x]="UPDATE 1301_elementosEstructuraProgramatica SET orden=orden-1 WHERE idPerfilEstructura=".$obj->idPerdilEstructura.
							" AND orden>=".$orden." and orden<=".$obj->orden;
					$x++;
				}
			}
			
			$consulta[$x]="update 1301_elementosEstructuraProgramatica set nombreElemento='".cv($obj->nombreElemento)."',abreviatura='".cv($obj->abreviatura).
						"',orden=".$obj->orden.",descripcion='".cv($obj->descripcion)."',dependeOtroElemento=".$obj->dependeOtroElemento.
						",elementoDependencia=".($obj->elementoDependencia==""?"NULL":$obj->elementoDependencia).",urlModulo='".cv($obj->urlModulo).
						"',espacioNombres='".cv($obj->espacioNombres)."',etiquetaSingular='".cv($obj->etiquetaSingular).
						"',articuloSingular='".cv($obj->articuloSingular)."',etiquetaPlural='".cv($obj->etiquetaPlural).
						"',articuloPlural='".cv($obj->articuloPlural)."' where idRegistro=".$obj->idElemento;

			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		
		eB($consulta);	
	}
	
	function obtenerElementoEstructuraProgramatica()
	{
		global $con;
		$idPerfil=$_POST["idPerfil"];
		
		$consulta="SELECT * FROM 1301_elementosEstructuraProgramatica WHERE idPerfilEstructura=".$idPerfil." ORDER BY orden";
		$arrRegistros=utf8_encode($con->obtenerFilasJSON($consulta));
		
		echo '{"numReg":"'.$con->filasAfectadas.'","registros":'.$arrRegistros.'}';
	}
	
	function removerElementoEstructuraProgramatica()
	{
		global $con;

		$idElemento=$_POST["idElemento"];
		$idPerdilEstructura=$_POST["idPerdilEstructura"];
		
		
		$query="SELECT CONCAT(nombreElemento,' (',abreviatura,')') FROM 1301_elementosEstructuraProgramatica WHERE elementoDependencia=".$idElemento." order by nombreElemento";
		$listaElementos=$con->obtenerListaValores($query);
		
		
		if($listaElementos!="")
		{
			echo "0|<br>Lo siguientes elementos: <b>".$listaElementos."</b> tienen dependencia con el elemento que desea remover";
			return;
		}
		
		$query="SELECT orden FROM 1301_elementosEstructuraProgramatica WHERE idRegistro=".$obj->idElemento;
		$orden=$con->obtenerValor($consulta);
		
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="DELETE FROM 1301_elementosEstructuraProgramatica WHERE idRegistro=";
		$x++;
		$consulta[$x]="UPDATE 1301_elementosEstructuraProgramatica SET orden=orden-1 WHERE idPerfilEstructura=".$idPerdilEstructura.
							" AND orden>=".$orden;
		$x++;
		$consulta[$x]="commit";
		$x++;
		eB($consulta);
	}
	
?>