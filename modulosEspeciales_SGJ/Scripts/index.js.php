<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var arrDimensiones=obtenerDimensionesNavegador();
    gE('filaInferior').setAttribute('height',arrDimensiones[0]-390);
    if(gE('totalPublicaciones').value!='0')
    {
        $('#carousel').infiniteCarousel	(
                                            {
                                                imagePath:'../Scripts/infiniteCarousel/images/',
                                                autoPilot :true
                                            }
                                        );       
	}
    
    if(window.location.href!=window.parent.location.href)
    {
    	obtenerVentanaPadre(window,location.href);  
	}
}

function obtenerVentanaPadre(ventana,ultimaPagina)
{
	
    if((ventana.parent)&&(ventana.parent.location.href!=ultimaPagina))
	    obtenerVentanaPadre(ventana.parent,ventana.location.href)
	else
    {
    	ventana.parent.location.href='<?php echo $paginaInicioLogin?>';
    }

}

function autenticar()
{
	var txtUsuario=gE('txtUsuario');
    var txtPasswd=gE('txtPasswd');
    var dominio=gE('txtDominio');
    
    if(dominio)
    {
    	if((dominio.value.trim()=='')||(dominio.value.trim()==''))
        {
            mE('lblErr2');
            oE('lblErr1');
            dominio.focus();
            return;
        } 
    }
    
    var l='';
    var p='';
    var d='';
   

    if((txtUsuario.value.trim()=='')||(txtPasswd.value.trim()==''))
    {
    	mE('lblErr1');
        oE('lblErr2');
        txtUsuario.focus();
        return;
    }    
    
    <?php
	if(isset($Enable_AES_Ecrypt)&&($Enable_AES_Ecrypt==true))
	{
	?>
		l=AES_Encrypt(txtUsuario.value.trim());
		p=AES_Encrypt(txtPasswd.value.trim());
        if(dominio)
	        d=AES_Encrypt(dominio.value.trim());
	<?php        
	}
	else
	{
	?>
		l=bE(txtUsuario.value.trim());
		p=bE(txtPasswd.value.trim());
        if(dominio)
	        d=bE(dominio.value.trim());
	<?php        
	}
	?>
    
    function funcAjax(peticion_http)
    {
    	var rutaRedireccion='../principalPortal/inicio.php';
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
         	if( arrResp[1] =='0')
            {
            	gE('lblErr1').innerHTML='Usuario / Contraseña incorrecta';
            	mE('lblErr1');
                txtUsuario.focus();
            }
            else
            {
            	if( arrResp[1] =='1')
	            	location.href=rutaRedireccion;
               else
               {
	               if(arrResp[1] =='2')
                   {
                   		var iU=arrResp[2];
                        var txtUsuario=gE('txtUsuario');
                        var txtPasswd=gE('txtPasswd');
                        var dominio=gE('txtDominio');
                        txtUsuario.value='';
                        txtPasswd.value='';
                        if(dominio)
	                        dominio.value='';
                        
                        mostrarVentana2FA(iU,rutaRedireccion);
                        
                   }
                   else
                   {
                   		if( arrResp[1] =='3')
                        {
                        	gE('lblErr1').innerHTML='Cuenta Bloqueada';
                            mE('lblErr1');
                            txtUsuario.focus();
                        }
                   }
               }
            }
        }
        else
        {
            msgBox('No se ha podido realizar la operaci&oacute;n debido al siguiente problema: '+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWebV2('../paginasFunciones/funciones.php',funcAjax, 'POST','funcion=8&d='+d+'&l='+l+'&p='+p,true);
    
    
    
    
}

function ocultarError(evt)
{
	oE('lblErr1');
    oE('lblErr2');
    var key= evt.which;
	if(Ext.isIE)
		key=evt.keyCode;
        
    if(key==13)
    {
    	autenticar();
    }
}

function mostrarPantallaRegistro()
{
	var obj={};
    obj.ancho='100%';
    obj.alto='100%';
    obj.url='../modulosEspeciales_SGJ/registroUsuario.php';
    abrirVentanaFancy(obj);
}

function mostrarPantallaRecuperar()
{
	var obj={};
    obj.ancho='100%';
    obj.alto='90%';
    obj.url='../modulosEspeciales_SGJ/recuperarDatosAcceso.php';
    abrirVentanaFancy(obj);
}


function mostrarPantallaSoporte()
{
	mostrarVentanaDuda();
}



function mostrarVentana2FA(iU,pR)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el C&oacute;digo de Acceso:'
                                                        },
                                                        {
                                                        	x:180,
                                                            y:5,
                                                            id:'txtCodigoAcceso',
                                                            xtype:'textfield',
                                                            width:110
                                                        },
                                                        {
                                                        	x:90,
                                                            y:40,
                                                            html:'<a href="javascript:reenviarCodigoAcceso(\''+bE(iU)+'\')">Reenviar C&oacute;digo de Acceso</a>'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'C&oacute;digo de Acceso',
										width: 350,
										height:140,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCodigoAcceso').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',                                                            
															handler: function()
																	{
                                                                    
                                                                    	if(gEx('txtCodigoAcceso').getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	gEx('txtCodigoAcceso').focus();
                                                                            }
                                                                        	msgBox('Debe ingresar el C&oacute;digo de Acceso',resp)
                                                                        	return;
                                                                        }
                                                                    
																		function funcAjax(peticion_http)
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                if( arrResp[1] =='0')
                                                                                {
                                                                                    function resp2()
                                                                                    {
                                                                                        gEx('txtCodigoAcceso').focus();
                                                                                    }
                                                                                    msgBox('El C&oacute;digo de Acceso ingresado es Incorrecto',resp2)
                                                                                    return;
                                                                                }
                                                                                else
                                                                                {
                                                                                	location.href=(pR);
                                                                                    ventanaAM.close();
                                                                                  
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWebV2('../paginasFunciones/funciones.php',funcAjax, 'POST','funcion=13&cA='+bE(gEx('txtCodigoAcceso').getValue())+'&iU='+bE(iU),true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function reenviarCodigoAcceso(iU)
{
	function funcAjax(peticion_http)
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            msgBox('El C&oacute;digo de Acceso ha sido Enviado de Nuevo');
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWebV2('../paginasFunciones/funciones.php',funcAjax, 'POST','funcion=12&iU='+iU,true);
    

}
