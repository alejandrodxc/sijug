<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


function registrarFactura()
{
	var gridRegistros=gEx('gridRegistros');
    var fila=gridRegistros.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el registro cuya factura desea integrar');
        return;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj={};
            obj.titulo='Agregar factura';
            obj.ancho=650;
            obj.alto=400;
            obj.url='../modeloPerfiles/registroFormulario.php';
            obj.params=[['cPagina','sFrm=true'],['idFormulario','961'],['idRegistro',arrResp[1]]];
            abrirVentanaFancy(obj);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=148&idPedido='+fila.get('noPedido'),true);
    
    
}

function verDatosFactura()
{
	var gridRegistros=gEx('gridRegistros');
    var fila=gridRegistros.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el registro cuya factura desea observar');
        return;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj={};
            obj.titulo='Ver datos de factura';
            obj.ancho=650;
            obj.alto=400;
            obj.url='../modeloPerfiles/verFichaFormulario.php';
            obj.params=[['cPagina','sFrm=true'],['idFormulario','961'],['idRegistro',arrResp[1]],['sLectura','1']];
            abrirVentanaFancy(obj);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=148&idPedido='+fila.get('noPedido'),true);
    
    
}

function recargarGrid()
{
	var gridRegistros=gEx('gridRegistros');
    gridRegistros.getStore().reload();
    gEx('btnRegistrarFactura').disable();
    gEx('btnVerFactura').disable();
    gEx('btnValidarFactura').disable();
    gEx('btnCancelarPago').disable();
}

function cancelarPago()
{
	var gridRegistros=gEx('gridRegistros');
    var fila=gridRegistros.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el registro cuyo pago desea cancelar');
        return;
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            html:'Ingrese el motivo de la cancelaci&oacute;n del pago:',
                                                            x:10,
                                                            y:10,
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            width:350,
                                                            height:80,
                                                            xtype:'textarea',
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar pago',
										width: 400,
										height:225,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
																		if(txtMotivo.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe ingresar el motivo de la cancelaci&oacute;n del pago');
                                                                            return;
                                                                        }
                                                                        
                                                                        function resp(btn)
                                                                        {
                                                                            function funcAjax()
                                                                            {
                                                                                var resp=peticion_http.responseText;
                                                                                arrResp=resp.split('|');
                                                                                if(arrResp[0]=='1')
                                                                                {
                                                                                    recargarGrid();
                                                                                    ventanaAM.close();
                                                                                }
                                                                                else
                                                                                {
                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                }
                                                                            }
                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=149&idPedido='+fila.get('noPedido')+'&motivo='+cv(txtMotivo.getValue()),true);
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer realizar la cancelaci&oacute;n del pago del registro seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function validarFactura()
{
	var arrResultado=[['1','Factura aceptada'],['2','Factura rechazada']];
	var cmbValidacion=crearComboExt('cmbValidacion',arrResultado,170,5,200);
	var gridRegistros=gEx('gridRegistros');
    var fila=gridRegistros.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el registro cuya factura desea validar');
        return;
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            html:'Resultado de la validaci&oacute;n:',
                                                            x:10,
                                                            y:10,
                                                        },
                                                        cmbValidacion,
														{
                                                        	xtype:'label',
                                                            html:'Comentarios:',
                                                            x:10,
                                                            y:40,
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:390,
                                                            height:80,
                                                            xtype:'textarea',
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Validaci&oacute;n de factura',
										width: 430,
										height:255,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtMotivo=gEx('txtMotivo');
                                                                        if(cmbValidacion.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar el resultado de la validaci&oacute;n de la factura');
                                                                            return;
                                                                        }
                                                                        if(cmbValidacion.getValue()=='2')
                                                                        {
                                                                            if(txtMotivo.getValue()=='')
                                                                            {
                                                                                msgBox('Debe ingresar el motivo del rechazo de la factura');
                                                                                return;
                                                                            }
                                                                        }
                                                                        function resp(btn)
                                                                        {
                                                                            function funcAjax()
                                                                            {
                                                                                var resp=peticion_http.responseText;
                                                                                arrResp=resp.split('|');
                                                                                if(arrResp[0]=='1')
                                                                                {
                                                                                    recargarGrid();
                                                                                    ventanaAM.close();
                                                                                }
                                                                                else
                                                                                {
                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                }
                                                                            }
                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=150&idPedido='+fila.get('noPedido')+'&motivo='+cv(txtMotivo.getValue())+'&validacion='+cmbValidacion.getValue(),true);
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la validaci&oacute;n de la factura del registro seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function filaSeleccionada(sm,nFila,registro)
{
	gEx('btnRegistrarFactura').disable();
    gEx('btnVerFactura').disable();
    gEx('btnValidarFactura').disable();
    gEx('btnCancelarPago').disable();
    switch(parseFloat(registro.get('idEtapaRegistro')))
    {
    	case 1:
        	gEx('btnRegistrarFactura').enable();
        break;
        case 2:
        	gEx('btnVerFactura').enable();
            gEx('btnValidarFactura').enable();
            gEx('btnCancelarPago').enable();
        break;
        case 3:
        	gEx('btnVerFactura').enable();
        	gEx('btnCancelarPago').enable();
        break;
        case 4:
        	gEx('btnRegistrarFactura').enable();
            gEx('btnCancelarPago').enable();
        break;
        case 5:
        break;
        case 6:
        	gEx('btnVerFactura').enable();
        break;
    }
}
