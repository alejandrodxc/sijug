<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idCiclo=$_GET["idCiclo"];
	$codDepartamento=$_GET["codDepartamento"];
	$codInstitucion=$_GET["codInstitucion"];
	$arrCapitulos=explode(",",$_GET["capitulos"]);
	global $tipoOG;
	$cadCapitulos="";
	$cadCapitulos2="";
	foreach($arrCapitulos as $c)
	{
		$cFin=substr($c,0,1);
		if($tipoOG==1)
			$cFin=str_pad($cFin,strlen($c),"9",STR_PAD_RIGHT);
		else
		{
			$cFin=str_pad($cFin,strlen($c)+1,"9",STR_PAD_RIGHT);
			$c.="0";
		}
		$particula="(clave>=".$c." and clave<=".$cFin.")";	
		$particula2="(p.objetoGasto>=".$c." and p.objetoGasto<=".$cFin.")";	
		if($cadCapitulos=="")
		{
			$cadCapitulos=$particula;	
			$cadCapitulos2=$particula2;
		}
		else
		{
			$cadCapitulos.=" or ".$particula;	
			$cadCapitulos2.=" or ".$particula2;
		}
	}
	$consulta="SELECT idSolicitud,c.clave,CONCAT('[',c.clave,']',nombreObjetoGasto) AS nombreObj,idCiclo,p.nombreProducto,justificacion,c.cantidad 
			   FROM 9112_solicitudesCompras c,507_objetosGasto o,9101_CatalogoProducto p 
			   WHERE  codDepto='".$codDepartamento."' AND c.codInstitucion='".$codInstitucion."' 
			   AND c.clave=o.clave AND ".$cadCapitulos2." and p.idProducto=c.idProducto" ;

	$arrCodigosUnidad=$con->obtenerFilasArreglo($consulta); 
	
	$consulta="SELECT codigoControl,concat('[',clave,'] ',nombreObjetoGasto) FROM 507_objetosGasto WHERE ".$cadCapitulos." AND CONCAT(clave,'') LIKE '%00' order by nombreObjetoGasto";
	$arrSubPartidas=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT clave,CONCAT('[',clave,'] ',nombreObjetoGasto) AS objetoGasto FROM 507_objetosGasto WHERE ".$cadCapitulos." AND CAST(clave AS CHAR) NOT LIKE '%00' ORDER BY nombreObjetoGasto";
	$arrobjetoGasto=$con->obtenerFilasArreglo($consulta);
	$arrMeses="[[0,0,0,0,0,0,0,0,0,0,0,0]]"; 
	$consulta="select idTipoPresupuesto,tituloTipoP from 508_tiposPresupuesto order by tituloTipoP";
	$arrTipoPresupuesto=$con->obtenerFilasArreglo($consulta);
	$lblTPresupuesto="";
	$idCiclo="2010";
?>	

var arrMeses=<?php echo $arrMeses?>;
Ext.onReady(inicializar);

function inicializar()
{
	crearGrid();
    inicializarCombos();
    inicializarCombos2();
}

var registroGrid=crearRegistro([  {name:'idCodigoGastoCiclo'},
                                  {name:'clave'},
                                  {name: 'nombreObj'},
                                  {name: 'idCiclo'},
                                  {name: 'nombreProducto'},
                                  {name: 'justificacion'},
                                  {name: 'cantidad'},
                                  {name: 'cadenaMeses'},
                                  {name: 'observaciones'}
                               ]
                              );

function crearGrid()
{
	var permisos=bD(gE('permisos').value);
    var ocultarBtnAgregar=true;
    var ocultarBtnModificar=true;
    var ocultarBtnRemover=true;
    var ocultarBtnSometer=true;
    if(permisos.indexOf('A')!=-1)
    	ocultarBtnAgregar=false;
    if(permisos.indexOf('M')!=-1)
    	ocultarBtnModificar=false;
    if(permisos.indexOf('E')!=-1)
    	ocultarBtnRemover=false;
    if(permisos.indexOf('S')!=-1)
    	ocultarBtnRemover=false;
   	
    var tipoPresupuesto=gE('tipoPresupuesto').value;
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idProducto',
                                            fields: [
                                               			{name:'idCodigoGastoCiclo'},
                                                        {name:'clave'},
                                                        {name: 'nombreObj'},
                                                        {name: 'idCiclo'},
                                                        {name: 'nombreProducto'},
                                                        {name: 'justificacion'},
                                                        {name: 'cantidad'},
                                                        {name: 'cadenaMeses'},
                                                        {name: 'observaciones'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'costoTotal'},
                                                        {name: 'tipoPresupuesto'},
                                                        {name: 'descripcion'},
                                                        {name: 'depto'},
                                                        {name: 'modificable'},
                                                        {name: 'etapa'},
                                                        {name: 'programa'},
                                                        {name: 'responsableSolicitud'},
                                                        {name: 'tipoPresupuesto'},
                                                        {name: 'fechaSolicitud'}
                                                        
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
                                      
		var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesCompras.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'depto'
                                                        })                                      
     
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=9;
                                        proxy.baseParams.ciclo=bE(gE('ciclo').value);
                                        proxy.baseParams.codigoUnidad=bE(gE('codigoDepto').value);
                                        proxy.baseParams.idPrograma=bE(gE('idPrograma').value);
                                        proxy.baseParams.capitulos=(gE('capitulos').value);
                                        proxy.baseParams.idProceso=(gE('idProceso').value);
                                        proxy.baseParams.actor=(gE('actor').value);
                                        proxy.baseParams.numEtapa=(gE('numEtapa').value);
                                        proxy.baseParams.idActorProcesoEtapa=(gE('idActorProcesoEtapa').value);
                                    }
                        )
    
    var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	{type: 'string', dataIndex: 'nombreProducto'},
				                                                        {type: 'date', dataIndex: 'fechaSolicitud', phpMode: true},
                                                        				{type: 'list', dataIndex: 'nombreObj', options:<?php echo uEJ($arrobjetoGasto)?>, phpMode: true}
                                                                    ]

                                                    }

                                                ); 
    
    var summary = new Ext.ux.grid.HybridSummary();
    
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
    var tamPagina=30;
    var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Producto:</b></span></td><td><span class="copyrigthSinPadding">{nombreProducto}</span><br /><br /></td></tr>'+
                                                    '<tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr>',
                                                    '<tr><td><span class="letraRojaSubrayada8"><b>Distribuci&oacute;n:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{cadenaMeses}</span><br /><br /></td></tr>',
                                                    '<tr><td><span class="letraRojaSubrayada8"><b>Justificaci&oacute;n:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{justificacion}</span><br /><br /></td></tr>',
                                                    '<tr><td><span class="letraRojaSubrayada8"><b>Observaciones:</b></span></td><td></td></tr><tr><td></td><td><span class="copyrigthSinPadding">{observaciones}</span><br /><br /></td></tr></table>'
                                                )
                                            });
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros2,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        expander,
                                                        {
															header:'Fecha de solicitud',
															width:100,
															sortable:true,
															dataIndex:'fechaSolicitud',
                                                            groupable:true
														},
                                                        {
															header:'Producto',
															width:400,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            summaryRenderer:function()
                                                            				{
                                                                            	return "<b>Costo total:</b>";
                                                                            }
														},
                                                        {
															header:'Cantidad',
															width:60,
															sortable:true,
															dataIndex:'cantidad',
                                                            align :'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearNumero(val,2,'.',',',false);
                                                                    }
														},
                                                        {
															header:'Costo Unitario',
															width:90,
															sortable:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney',
                                                             align :'right'
														},
                                                        {
															header:'Costo Total',
															width:90,
															sortable:true,
															dataIndex:'costoTotal',
                                                            renderer:'usMoney',
                                                            summaryType:'sum',
                                                            align :'right'
														},
                                                        {
															header:'Tipo de presupuesto',
															width:150,
															sortable:true,
															dataIndex:'tipoPresupuesto',
                                                            groupable:true
                                                            
														},
                                                        {
															header:'Departamento',
															width:200,
															sortable:true,
															dataIndex:'depto',
                                                            groupable:true
                                                            
														},
                                                        {
															header:'Programa',
															width:200,
															sortable:true,
															dataIndex:'programa',
                                                            groupable:true
                                                            
														},
                                                        {
															header:'Solicitado por',
															width:200,
															sortable:true,
															dataIndex:'responsableSolicitud',
                                                            groupable:true
                                                            
														},
                                                        {
															header:'Situaci&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'etapa',
                                                            groupable:true
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            title:'Conceptos',
                                                            frame:true,
                                                            renderTo:'gridObjGasto',
                                                            cm: cModelo,
                                                            height:700,
                                                            width:1950,
                                                            sm:chkRow,
                                                            bbar: paginador,
                                                            id:'gridProductos',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Agregar concepto',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBtnAgregar,
                                                                            handler:function()
                                                                            	{
                                                                                	agregarProducto('-1');
                                                                                }
                                                                        },
                                                                        {
                                                                        	text:'Modificar concepto',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBtnModificar,
                                                                            handler:function()
                                                                            	{
                                                                                	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                    if(fila==null)
                                                                                	{
                                                                                        msgBox('Debe seleccionar el producto a modificar');
                                                                                        return;
                                                                                    }
                                                                                	agregarProducto(fila.get('idCodigoGastoCiclo'));
                                                                                    
                                                                                }
                                                                        },
                                                                        {
                                                                        	text:'Remover concepto',
                                                                            icon:'../images/cancel_round.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBtnRemover,
                                                                            handler:function()
                                                                            	{
                                                                                	removerProducto();
                                                                                }
                                                                        }
                                                                        ,
                                                                        
                                                                        '-',
                                                                        
                                                                        {
                                                                        	text:'Someter a revisi&oacute;n',
                                                                            icon:'../images/table_row_insert.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBtnSometer,
                                                                        	handler:function()
                                                                            		{
                                                                                    	someterRevision()
                                                                                    }
                                                                        }
                                                                       
                                                            		],
                                                         	plugins:[expander,filters,summary],
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:true,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );
		
    dsTablaRegistros2.load({params:{start:0,limit:tamPagina,funcion:9}})  ;     
	return 	tblGrid;
}

function removerProducto()
{
	var tblProductos=Ext.getCmp('gridProductos');
    var fila=tblProductos.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el producto que desea eliminar');
        return;
    }
    function respB(btn)
	{
		if(btn=='yes')
		{
           
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1') 
                {
                 	Ext.getCmp('gridProductos').getStore().reload();    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=3&idCodigoGastoCiclo='+fila.get('idCodigoGastoCiclo'),true);
       }
    }
    msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',respB);
}

function modificarProducto(ventana,idRegistroObjeto)
{
      function funcAjax()
      {
          var resp=peticion_http.responseText;
          arrResp=resp.split('|');
          if(arrResp[0]=='1') 
          {
          		
                var aNombre=Ext.getCmp('cmbNombre');
                aNombre.setReadOnly(true);
                aNombre.hide();
                
                gEx('lblBuscar').hide();
                var aNombreP=Ext.getCmp('cmbNombreP')
                var eCantidad=Ext.getCmp('cantidad');
                eCantidad.setValue(arrResp[3]);
                gE('idProovedor').value=arrResp[7];
                gE('idProducto').value=arrResp[1];
                gEx('justificacion').value=dv(arrResp[4]);
                gEx('observaciones').value=dv(arrResp[8]);
                
                gEx('tblMeses').getStore().loadData(eval(arrResp[5]));
                var cadena=arrResp[2];
                if(cadena.length>65)
                    cadena=cadena.substr(0,65)+'...';
                aNombre.setValue(cadena);
                aNombreP.setValue(arrResp[9]);
                gEx('lblNombreProducto').setText(cadena,false);
                gEx('lblNombreProducto').show();
                
                gE('costoUnitario').value=arrResp[10];
                gEx('lblCosto').setText('$ '+formatearNumero(arrResp[10],2,'.',',',false));
                gE('costoTotal').value=arrResp[11];
                 gEx('lblCostoTotal').setText('$ '+formatearNumero(arrResp[11],2,'.',',',false));
                
                ventana.show();
          }
          else
          {
              msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
          }
      }
      obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=4&id='+idRegistroObjeto,true);
}


function verHistorial()
{
	 var codDepto=bE(gE('codigoDepto').value);
     var idPrograma=bE(gE('idPrograma').value);
    
	 var idProducto=gE('idProducto').value;
     if(idProducto=='')
     {
     	msgBox('Debe seleccionar un producto');
        return;
     }
     var idCiclo=<?php echo $idCiclo?>;
     function funcAjax()
      {
          var resp=peticion_http.responseText;
          arrResp=resp.split('|');
          if(arrResp[0]=='1') 
          {
              var arreglo=eval(arrResp[1]);
              var nombre=arrResp[2];
              ventanaHistorial(arreglo,nombre);
          }
          else
          {
              if(arrResp[0]==2)
              {
              		Ext.MessageBox.alert(lblAplicacion,'El producto seleccionado no cuenta con registros previos');
              }
              else
              {
              		msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
              }
          }
      }
      obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=5&codigoUnidad='+codDepto+'&idPrograma='+idPrograma+'&idProducto='+idProducto+'&idCiclo='+idCiclo,true);
}

Ext.chart.Chart.CHART_URL = '../Scripts/ext/resources/charts.swf';

function ventanaHistorial(arreglo,nombre)
{
    var tablaHistorial=crearTablaHistorial(arreglo,nombre);
    var arrValores=obtenerArregloValores(tablaHistorial);
    
    var x;
    var arrCampos=new Array();
    arrCampos.push('mes');
    var arrSeries=new Array();
    var serie;
    for(x=0;x<tablaHistorial.getStore().getCount();x++)
    {
    	var fila=tablaHistorial.getStore().getAt(x);
    	arrCampos.push('ciclo_'+fila.get('idCiclo'));
        serie={
					displayName:'Ciclo: '+fila.get('idCiclo'),
                    yField:'ciclo_'+fila.get('idCiclo')
                    
              }
        arrSeries.push(serie)
    }
    
    var almacen = new Ext.data.JsonStore	(
                                                {
                                                    fields:arrCampos,
                                                    data: arrValores
                                                }
                                            );

    
	var form = new Ext.form.FormPanel(	
										{
                                        	id:'ventanaHistorial',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">Producto: '
                                                        },
                                                        {
                                                            xtype:'label',
                                                            x:110,
                                                            y:10,
                                                            html:'<span class="letraExt">'+nombre+'</span>'
                                                        }

														,
                                                        {
                                                        	xtype:'panel',
                                                            x:10,
                                                            y:65,
                                                            items:[
                                                                        {
                                                                            x:0,
                                                                            y:0,
                                                                            width:800,
                                                                            height:275,
                                                                            xtype:'linechart',
                                                                            store:almacen,
                                                                            xField:'mes',
                                                                            series:	arrSeries,
                                                                            extraStyle:
                                                                                        {
                                                                                            legend:
                                                                                            {
                                                                                                display: 'bottom',
                                                                                                padding: 5,
                                                                                                font:
                                                                                                {
                                                                                                    family: 'Tahoma',
                                                                                                    size: 11
                                                                                                }
                                                                                            }
                                                                                        }
                                                                        }
                                                                   ]
                                                         },
                                            			tablaHistorial
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Historial de producto',
										width: 840,
										height:580,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaH',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Cerrar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
									}
								);
		ventana.show();
}

function obtenerArregloValores(grid)
{
	var almacen=grid.getStore();
    var x;
    var fila;
    var arrFinal=new Array();
    var nMes;
    var arrMeses=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
    var obj;
    for(nMes=0;nMes<12;nMes++)
    {
    	obj={
        		"mes":arrMeses[nMes]
        	}
        for(x=0;x<almacen.getCount();x++)
        {
            fila=almacen.getAt(x);
            eval('obj.ciclo_'+fila.get('idCiclo')+'='+fila.get('mes'+nMes));
           
        }
        arrFinal.push(obj);
	}
    return arrFinal;
}

function crearTablaHistorial(arreglo,nombre)
{
	var arrDatos=arreglo;
    var dSetHistorial= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idCodigoGastoCiclo'},
                                                                    {name:'idProducto'},
                                                                    {name:'idCiclo'},
                                                                    {name:'cantidad'},
                                                                    {name:'mes0'},
                                                                    {name:'mes1'},
                                                                    {name:'mes2'},
                                                                    {name:'mes3'},
                                                                    {name:'mes4'},
                                                                    {name:'mes5'},
                                                                    {name:'mes6'},
                                                                    {name:'mes7'},
                                                                    {name:'mes8'},
                                                                    {name:'mes9'},
                                                                    {name:'mes10'},
                                                                    {name:'mes11'}
                                                                ]
                                                    }
                                                 )
    
	dSetHistorial.loadData(arrDatos);	
	var cmHistorial= new Ext.grid.ColumnModel   	(
                                                        [
                                                            {
                                                                header:'Accion',
                                                                width:50,
                                                                sortable:true,
                                                                align :'center',
                                                                renderer:function(val,ambito,registro)
                                                                		{
                                                                                var idCodigoG=registro.get('idCodigoGastoCiclo');
                                                                                var idCiclo=registro.get('idCiclo');
                                                                                var idProducto=registro.get('idProducto');
                                                                                var cantidad=registro.get('cantidad');
                                                                                var cadena="";
                                                                                for(x=0;x< 12;x++)
                                                                                {
                                                                                	var mes=registro.get('mes'+x);
                                                                                    if(cadena=='')
                                                                                    	cadena=x+'_'+mes
                                                                                    else
                                                                                        cadena+=','+x+'_'+mes;		    
                                                                                }
                                                                                
                                                                                return '<a href="javascript:cargarProducto('+idCodigoG+')"><img height="13" width="13" src="../images/table_row_insert.png" alt="Copiar Registro" title="Copiar Registro"/></a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Ciclo',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'idCiclo',
                                                                align :'center'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:65,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
    															align :'right'
                                                            },
                                                            {
                                                                header:'Ene',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes0',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Feb',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes1',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Mar',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes2',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Abr',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes3',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'May',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes4',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Jun',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes5',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Jul',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes6',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Ago',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes7',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Sep',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes8',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Oct',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes9',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Nov',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes10',
                                                                align :'right'
                                                            },
                                                            {
                                                                header:'Dic',
                                                                width:50,
                                                                sortable:true,
                                                                dataIndex:'mes11',
                                                                align :'right'
                                                            }
                                                        ]
                                                    );
											
	
	tblHistorial=	new Ext.grid.GridPanel	(
                                                {
                                                    x:5,
                                                    y:350,
                                                    id:'tblHistorial',
                                                    store:dSetHistorial,
                                                    frame:true,
                                                    cm: cmHistorial,
                                                    height:140,
                                                    width:810,
                                                    columnLines:true
                                                    
                                                }
                                            );
	tblHistorial.enEdicion=false;                                            
	return tblHistorial;
}

function cargarProducto(idCodigoG)
{						
    var cicloActual=gE('ciclo').value;
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
            var aNombre=Ext.getCmp('cmbNombre')
            aNombre.setRawValue(arrResp[5]);
            var aNombre=Ext.getCmp('cmbNombreP')
            aNombre.setRawValue(arrResp[6]);
            gE('idProovedor').value=arrResp[3];
            gE('idProducto').value=arrResp[1];
            gE('cantidad').value=arrResp[2];
            var ventana=Ext.getCmp('ventanaH');
            var arrMeses=eval(arrResp[4]);
            gEx('tblMeses').getStore().loadData(arrMeses);
            calcularCostoTotal();
            var tipoPresupuesto=arrResp[12];
            gE('tipoPresupuesto').value=tipoPresupuesto;
            gEx('cmbTipoPresupuesto').setValue(tipoPresupuesto);
            ventana.close();

        }
        else
        {
            if(arrResp[0]==2)
            {
                  msgBox('El producto seleccionado no cuenta con registros previos');
            }
            else
            {
                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=7&idCodigoG='+idCodigoG+'&cicloActual='+cicloActual,true);
}

function mostrarVentanaFiltroProducto()
{
	var grid=crearGridFiltro();
    grid.on('dblclick',function(e)
    					{
                        	var fila=grid.getSelectionModel().getSelected();
                            if(fila==null)
                            {
                                msgBox('Debe seleccionar el producto a agregar');
                                return;
                            }
                            gEx('cmbNombre').setRawValue(fila.get('nombreProducto'));
                            gE('idProducto').value=fila.get('idProducto');
                            gEx('ventanaFiltro').close();
                        }
    		);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														grid

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'ventanaFiltro',
										title: 'B&uacute;squeda de producto',
										width: 840,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var fila=grid.getSelectionModel().getSelected();
                                                                        if(fila==null)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto a agregar');
                                                                            return;
                                                                        }
                                                                        gEx('cmbNombre').setRawValue(fila.get('nombreProducto'));
                                                                        gE('idProducto').value=fila.get('idProducto');
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridFiltro()
{
	
    var dsTablaRegistros = new Ext.data.JsonStore	(

                                                      {
                                                          root: 'registros',
                                                          totalProperty: 'numReg',
                                                          idProperty: 'idProducto',
                                                          fields: [
                                                          				{name:'subPartida'},
                                                                        {name:'idProducto'},
                                                                        {name: 'nombreObjetoGasto'},
                                                                        {name: 'nombreProducto'},
                                                                        {name: 'descripcion'}
                                                          			],
                                                          remoteSort:true,
                                                          proxy: new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesCompras.php'

                                                                                              }

                                                                                          )

                                                      }
                                                   )

	function cargarDatos(proxy,parametros)
    {
        proxy.baseParams.funcion=8;
        proxy.baseParams.ciclo=bE(gE('ciclo').value);
        proxy.baseParams.codDepto=bE(gE('codigoDepto').value);
        proxy.baseParams.programa=bE(gE('idPrograma').value);
        proxy.baseParams.capitulos=(gE('capitulos').value);
        
    }
    
    var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	{type: 'string', dataIndex: 'nombreProducto'},
                                                        				{type: 'list', dataIndex: 'subPartida', options:<?php echo uEJ($arrSubPartidas)?>, phpMode: true},
                                                                        {type: 'list', dataIndex: 'nombreObjetoGasto', options:<?php echo uEJ($arrobjetoGasto)?>, phpMode: true}
                                                                    ]

                                                    }

                                                ); 
    
    dsTablaRegistros.on('beforeload',cargarDatos); 
    var tamPagina=30;
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: dsTablaRegistros,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )   
                                                 
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
    var expander = new Ext.ux.grid.RowExpander({
    											column:3,
										    	tpl : new Ext.Template(
                                                    '<br /><p><span class="letraAzulSubrayada7"><b>Nombre:</b></span><span class="copyrigthSinPadding">{nombreProducto}</span></p><br>',
                                                    '<p><span class="letraAzulSubrayada7"><b>Descripcion:</b></span><span class="copyrigthSinPadding">{descripcion}</span></p>'
                                                )
                                            });
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        expander,
														
                                                        {
															header:'Producto',
															width:370,
															sortable:true,
															dataIndex:'nombreProducto'
														},
                                                        {
															header:'Sub partida',
															width:250,
															sortable:true,
															dataIndex:'subPartida'
														},
                                                        {
															header:'Objeto de gasto',
															width:250,
															sortable:true,
															dataIndex:'nombreObjetoGasto'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros,
                                                            frame:true,
                                                            y:10,
                                                            cm: cModelo,
                                                            height:460,
                                                            width:810,
                                                            sm:chkRow,
                                                            loadMask: true,
                                                            bbar: paginador,
                                                            plugins:[expander,filters]


                                                            
                                                        }
                                                    );
	dsTablaRegistros.load({params:{start:0,limit:tamPagina,funcion:8}})  ;                                            
	return 	tblGrid;
}



function suma()
{
    var arregloMeses=document.getElementsByName('meses[]');
    var tamano=arregloMeses.length;
    var total=0;
    for(x=0;x< tamano;x++)
    {
        var cantidadM=arregloMeses[x].value;
        if(cantidadM=='')
        {
            cantidadM=0;
        }     
        
        var valor=parseInt(cantidadM);
        
        total=total+valor; 
    }
	var etiqueta=gE('total');
    etiqueta.innerHTML=total;
}

function agregarProducto(idRegistroObjeto)
{
	var arrTipoPresupuesto=<?php echo $arrTipoPresupuesto?>;
	var cmbTipoPresupuesto=crearComboExt('cmbTipoPresupuesto',arrTipoPresupuesto,130,100);
    cmbTipoPresupuesto.on	('select',	function(cmb,registro)
    									{
                                        	gE('tipoPresupuesto').value=registro.get('id');
                                        }
    						)
    
    var arrDatos=eval(arrMeses);
    var dSetMeses= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'mes0'},
                                                                    {name:'mes1'},
                                                                    {name:'mes2'},
                                                                    {name:'mes3'},
                                                                    {name:'mes4'},
                                                                    {name:'mes5'},
                                                                    {name:'mes6'},
                                                                    {name:'mes7'},
                                                                    {name:'mes8'},
                                                                    {name:'mes9'},
                                                                    {name:'mes10'},
                                                                    {name:'mes11'}
                                                                ]
                                                    }
                                                 )
    
	dSetMeses.loadData(arrDatos);	
	var cmMeses= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	{
                                                            	header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	var x=0;
                                                                            var total=0;
                                                                            for(x=0;x<12;x++)
                                                                            {
                                                                            	total+=parseFloat(registro.get('mes'+x));
                                                                            }
                                                                            var color="#030";
                                                                            var cantidad=gEx('cantidad').getValue();
                                                                            if(cantidad>total)
                                                                            {
                                                                            	color='#F60';
                                                                            }
                                                                            else
                                                                            {
                                                                            	if(cantidad<total)
                                                                                	color='#FF0000';
                                                                            }
                                                                            return '<b><font color="'+color+'">'+total+'</font></b>';
                                                                    }
                                                            },
                                                            {
                                                                header:'Ene',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes0',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            },
                                                            {
                                                                header:'Feb',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes1',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Mar',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes2',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Abr',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes3',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'May',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes4',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Jun',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes5',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Jul',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes6',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Ago',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes7',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Sep',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes8',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Oct',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes9',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Nov',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes10',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            }
                                                            ,
                                                            {
                                                                header:'Dic',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'mes11',
                                                                editor: new Ext.form.NumberField(
                                                                								 {
                                                                                                  allowBlank: false,
                                                                                                  allowNegative: false
                                                                                          	     }
                                                                                               ),
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	return val;
                                                                    }
                                                            },
                                                            {
                                                            	header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	var x=0;
                                                                            var total=0;
                                                                            for(x=0;x<12;x++)
                                                                            {
                                                                            	total+=parseFloat(registro.get('mes'+x));
                                                                            }
                                                                            var color="#030";
                                                                            var cantidad=gEx('cantidad').getValue();
                                                                            if(cantidad>total)
                                                                            {
                                                                            	color='#F60';
                                                                            }
                                                                            else
                                                                            {
                                                                            	if(cantidad<total)
                                                                                	color='#FF0000';
                                                                            }
                                                                            return '<b><font color="'+color+'">'+total+'</font></b>';
                                                                    }
                                                            }
                                                        ]
                                                    );
											
	var editorFila=new Ext.ux.grid.RowEditor	(
    												{
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
	var tipoPresupuesto=gE('esquemaPresupuesto').value;
    var ocultarTipoPresupuesto=true;

    if(tipoPresupuesto!='0')                                               
    {
    	ocultarTipoPresupuesto=false;
        cmbTipoPresupuesto.hide();
    }
    
    
	tblMeses=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	
                                                    	title:'Distibuci&oacute;n del producto',
                                                        x:10,
                                                        y:140,
														id:'tblMeses',
                                                        store:dSetMeses,
                                                        frame:true,
                                                        clicksToEdit: 2,
                                                        cm: cmMeses,
                                                        height:150,
                                                        width:750,
                                                        plugins:[editorFila]
													}
    											);
        
        
        var comboNombre= inicializarCombos();
        var comboNombreP= inicializarCombos2();
        var form = new Ext.form.FormPanel(	
                                            {
                                                title:'Generales',
                                                id:'panel1',
                                                baseCls: 'x-plain',
                                                layout:'absolute',
                                                defaultType: 'textfield',
                                                items: 	[
                                                            {
                                                                     x:65,
                                                                     y:15,
                                                                     xtype:'label',
                                                                     align:'center',
                                                                     html:'Producto:&nbsp;<font color="#FF0000">*</font>'
                                                                 },
                                                                 comboNombre,
                                                                 {
                                                                 	x:130,
                                                                    y:15,
                                                                 	xtype:'label',
                                                                    id:'lblNombreProducto',
                                                                    html:'',
                                                                    hidden:true
                                                                 },
                                                             {
                                                                 x:640,
                                                                 y:15,
                                                                 xtype:'label',
                                                                 align:'center',
                                                                 id:'lblBuscar',
                                                                 html:'<a href="javascript:mostrarVentanaFiltroProducto()"><img src="../images/add.png" title="Buscar producto" alt="Buscar producto"/></a>'
                                                             },
                                                             
                                                             
                                                             {
                                                                 x:10,
                                                                 y:45,
                                                                 xtype:'label',
                                                                 html:'Proveedor sugerido:'
                                                             },
                                                             comboNombreP
                                                             ,
                                                             {
                                                                 x:35,
                                                                 y:75,
                                                                 xtype:'label',
                                                                 html:'Costo Unitario:&nbsp;'
                                                             },
                                                             {
                                                                 x:130,
                                                                 y:75,
                                                                 id:'lblCosto',
                                                                 xtype:'label',
                                                                 html:'0.00'
                                                             }
                                                             ,
                                                             {
                                                                 x:220,
                                                                 y:75,
                                                                 xtype:'label',
                                                                 html:'Cantidad:&nbsp;<font color="#FF0000">*</font>'
                                                             },
                                                             {
                                                                 x:290,
                                                                 y:70,
                                                                 xtype:'numberfield',
                                                                 width:80,
                                                                 id:'cantidad',
                                                                 listeners:	{
                                                                 				change:function()
                                                                                		{
                                                                                        	calcularCostoTotal();
                                                                                        }
                                                                 			}
                                                             },
                                                            
                                                             {
                                                                 x:380,
                                                                 y:75,
                                                                 xtype:'label',
                                                                 align:'center',
                                                                 html:'<a href="javascript:verHistorial()"><img src="../images/magnifier.png" title="Ver Historial del Producto" alt="Ver Historial del Producto"/></a>'
                                                             },
                                                              {
                                                                 x:420,
                                                                 y:75,
                                                                 xtype:'label',
                                                                 html:'Costo Total:&nbsp;'
                                                             },
                                                             {
                                                                 x:520,
                                                                 y:75,
                                                                 id:'lblCostoTotal',
                                                                 xtype:'label',
                                                                 html:'0.00'
                                                             },
                                                             {
                                                             	 x:0,
                                                                 y:105,
                                                                 xtype:'label',
                                                                 html:'Tipo de presupuesto:&nbsp;'
                                                             },
                                                             cmbTipoPresupuesto,
                                                             {
                                                                 x:130,
                                                                 y:105,
                                                                 hidden:ocultarTipoPresupuesto,
                                                                 xtype:'label',
                                                                 html:gE('lblTipoPresupuesto').value
                                                             },
                                                             tblMeses
                                                        ]
                                            }
                                        );
     var form2 = new Ext.form.FormPanel(	
										{
											title:'Justificaci&oacute;n',
                                            id:'panel2',
                                            baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                        {
                                                             x:30,
                                                             y:30,
                                                             xtype:'label',
                                                             align:'center',
                                                             html:'Justificacion:&nbsp;<font color="#FF0000">*</font>'
                                                         },
                                                         {
                                                             x:130,
                                                             y:30,
                                                             height:100,
                                                             width:500,
                                                             xtype:'textarea',
                                                             id:'justificacion'
                                                         },
                                                         {
                                                             x:30,
                                                             y:150,
                                                             xtype:'label',
                                                             html:'Observaciones:'
                                                         },
                                                         {
                                                             x:130,
                                                             y:150,
                                                             height:100,
                                                             width:500,
                                                             xtype:'textarea',
                                                             id:'observaciones'
                                                         }
													]
										}
									);                               
	 var tabs = new Ext.TabPanel	(
                                        {
                                            x:10,
                                            y:10,
                                            activeTab:1,
                                            width:650,
                                            height:400,
                                            baseCls: 'x-plain',
                                            items:	[	
                                                     form,
                                                     form2
                                                    ]
                                        
                                        
                                            
                                        }
                                    );
    var ventana = new Ext.Window(
									{
                                    	id:'vAgregarProducto',
										title: 'Agregar producto',
										width: 800,
										height:400,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: tabs,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	tabs.setActiveTab(0);	
                                                                    
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var producto=Ext.getCmp('cmbNombre').getValue();
                                                                                    if(producto=='')
                                                                                    {
                                                                                    	msgBox('Debe indicar el concepto que desea agregar');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var cantidad=Ext.getCmp('cantidad').getValue();
                                                                                    if(cantidad=='')
                                                                                    {
                                                                                    	msgBox('Debe ingresar la cantidad del concepto solicitado');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var tipoPresupuesto=gE('tipoPresupuesto').value;
                                                                                    if(tipoPresupuesto=='')
                                                                                    {
                                                                                    	msgBox('Dbe indicar el tipo de presupuesto que desea utilizar');
                                                                                        return;
                                                                                    }

                                                                                    var idCiclo=gE('ciclo').value;
                                                                                    var codDepartamento=gE('codigoDepto').value;
                                                                                    var codInstitucion=gE('codigoInst').value;
                                                                                    var idPrograma=gE('idPrograma').value;
                                                                                    var idProveedor=gE('idProovedor').value;
                                                                                    var idProducto=gE('idProducto').value;
                                                                                    var arrMeses="";
                                                                                    var x=0;
                                                                                    var total=0;
                                                                                    var registro;
                                                                                    var grid=gEx('tblMeses');
                                                                                    registro=grid.getStore().getAt(0);
                                                                                    var justificacion=gEx('justificacion').getValue();
                                                                                    var observaciones=gEx('observaciones').getValue();
                                                                                    var costoUnitario=normalizarValor(gE('costoUnitario').value);
                                                                                    var costoTotal=normalizarValor(gE('costoTotal').value);
                                                                                    for(x=0;x<12;x++)
                                                                                    {
                                                                                    	
                                                                                        total+=parseFloat(registro.get('mes'+x));
                                                                                        if(arrMeses=='')
                                                                                        	arrMeses=registro.get('mes'+x);
                                                                                        else
                                                                                        	arrMeses+=','+registro.get('mes'+x);
                                                                                    }
                                                                                    
                                                                                    var cantidad=gEx('cantidad').getValue();
                                                                                    if(total!=cantidad)
                                                                                    {
                                                                                    	function resp()
                                                                                        {
                                                                                        	gEx('cantidad').focus(true,10);
                                                                                        }
                                                                                        msgBox('La suma de la distribuci&oacute;n del producto ('+total+') no coincide con la cantidad total solicitada ('+cantidad+')',resp);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var cadObj='{"idRegistroObjeto":"'+idRegistroObjeto+'","idCiclo":"'+idCiclo+'","codDepto":"'+codDepartamento+'","codInstitucion":"'+codInstitucion+
                                                                                    		'","idProducto":"'+idProducto+'","cantidad":"'+cantidad+'","justificacion":"'+cv(justificacion)+'","idProveedorSugerido":"'+idProveedor+
                                                                                            '","observaciones":"'+cv(observaciones)+'","idPrograma":"'+idPrograma+'","distribucion":"'+arrMeses+'","costoUnitario":"'+costoUnitario+
                                                                                            '","costoTotal":"'+costoTotal+'","tipoPresupuesto":"'+tipoPresupuesto+'"}';
                                                                                    
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                        	gEx('gridProductos').getStore().reload();
                                                                                        	ventana.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=1&cadObj='+cadObj,true);
                                                                                    

																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
									}
								);
		if(idRegistroObjeto=='-1')
        	ventana.show();
        else
        	modificarProducto(ventana,idRegistroObjeto);
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesCompras.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idProducto'
											},
											[
											 	{name:'idProducto', mapping:'idProducto'},
												{name:'nombreProducto', mapping:'nombreProducto'},
                                                {name:'descripcion', mapping:'descripcion'},
                                                {name:'costoUnitario'}
											]
										);
	var parametros=	{
						funcion:'2',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}

function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getValue();
		dSet.baseParams.criterio=aNombre;
        dSet.baseParams.ciclo=bE(gE('ciclo').value);
        dSet.baseParams.codDepto=bE(gE('codigoDepto').value);
        dSet.baseParams.programa=bE(gE('idPrograma').value);
        dSet.baseParams.capitulos=gE('capitulos').value;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'<span class="letraRojaSubrayada8">Producto: </span><span class="corpo7">{nombreProducto}</span><br /><br><span class="letraAzulSubrayada7">Descripci&oacute;n: </span><span class="copyrigthSinPadding">{descripcion}</span>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:130,
                                                        y:10,
                                                        id:'cmbNombre',
														store:ds,
														displayField:'producto',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:500,
														pageSize:30,
														hideTrigger:true,
														tpl:resultTpl,
														//applyTo:'producto',
														itemSelector:'div.search-item',
														listWidth :490
													}
												 );
	function funcElemSeleccionado(combo,registro)
	{	
		gE('idProducto').value=registro.get('idProducto');
        var cadena=registro.get('nombreProducto');
        if(cadena.length>65)
        	cadena=cadena.substr(0,65)+'...';
        comboNombre.setRawValue(cadena);
        gEx('lblCosto').setText('$ '+registro.get('costoUnitario'));
        gE('costoUnitario').value=registro.get('costoUnitario');
        calcularCostoTotal();
	}
	
	comboNombre.on('select',funcElemSeleccionado);	
    return comboNombre;
}

function calcularCostoTotal()
{
    var cantidad=gEx('cantidad').getValue();
    if(cantidad=='')
        cantidad=0;
    var costoUnitario=parseFloat(normalizarValor(gE('costoUnitario').value));
    var costoTotal=cantidad*costoUnitario;
    gE('costoTotal').value=costoTotal;
    gEx('lblCostoTotal').setText('$ '+formatearNumero(costoTotal,2,'.',',',false));
}

function inicializarCombos2()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesCompras.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'proovedores',
												totalProperty:'num',
												id:'idProovedor'
											},
											[
											 	{name:'idProovedor', mapping:'idProovedor'},
												{name:'nombreProovedor', mapping:'nombreProovedor'},
                                                {name:'RFC', mapping:'RFC'},
                                                {name:'direccion', mapping:'direccion'},
                                                {name:'tel1', mapping:'tel1'},
                                                {name:'tel2', mapping:'tel2'},
                                                {name:'fax', mapping:'fax'},
                                                {name:'correo', mapping:'correo'}
											]
										);
	var parametros=	{
						funcion:'6',
						criterio2:''
					};
	return inicializarCmbNombreP(pPagina,lector,parametros);
}

function inicializarCmbNombreP(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
		var aNombreP=Ext.getCmp('cmbNombreP').getValue();
		dSet.baseParams.criterio=aNombreP;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'<span class="letraRojaSubrayada8">Proveedor: </span> <span class="corpo7">{nombreProovedor}</span><br /><span class="corpo8_bold">RFC: </span><span class="copyrigthSinPadding">{RFC}</span><br><span class="corpo8_bold">Direccion: </span><span class="copyrigthSinPadding">{direccion}</span><br /><span class="corpo8_bold">Tel. 1: </span><span class="copyrigthSinPadding"> {tel1}</span><br><span class="corpo8_bold">Tel. 2: </span><span class="copyrigthSinPadding">{tel2}</span><br /><span class="corpo8_bold">Fax: </span><span class="copyrigthSinPadding">{fax}</span><br /><span class="corpo8_bold">E-mail: </span><span class="copyrigthSinPadding">{correo}</span>',
										'</div></tpl>'
									 )
	
	var comboNombreP= new Ext.form.ComboBox	(
												 	{
														x:130,
                                                        y:40,
                                                        id:'cmbNombreP',
														store:ds,
														displayField:'proovedor',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:500,
														pageSize:30,
														hideTrigger:true,
														tpl:resultTpl,
														//applyTo:'proovedor',
														itemSelector:'div.search-item',
														listWidth :490
													}
												 );
	function funcElemSeleccionado(combo,registro)
	{	
		gE('idProovedor').value=registro.get('idProovedor');
        comboNombreP.setRawValue(registro.get('nombreProovedor'));
	}
	
	comboNombreP.on('select',funcElemSeleccionado);	
    return comboNombreP;
}


function someterRevision()
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var etapaSomete=gE('etapaSomete').value;
            var registros=obtenerListadoArregloFilas(arrFilas,'idCodigoGastoCiclo');
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesProyectos.php',funcAjax, 'POST','funcion=12&etapa='+etapaSomete+'&registros='+registros,true);


        }
    }
    msgConfirm('Est&aacute; seguro de querer someter a revisi&oacute;n la requisici&oacute;n',resp);
}