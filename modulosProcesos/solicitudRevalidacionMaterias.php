<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../Scripts/superFish/superfish-1.4.8/css/superfish-vertical.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../Scripts/superFish/superfish-1.4.8/css/superfish-navbar.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<script type="text/javascript" src="../Scripts/superFish/superfish-1.4.8/js/hoverIntent.js"></script>
<script type="text/javascript" src="../Scripts/superFish/superfish-1.4.8/js/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="../Scripts/superFish/superfish-1.4.8/js/superfish.js"></script>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$_POST["cPagina"]="sFrm=true";
	$ctPOST=sizeof(array_values($_POST))
	
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ux/css/columnNodeUI.css" />
<link rel="stylesheet" type="text/css" href="../Scripts/ux/css/column-tree.css" />
<script type="text/javascript" src="../Scripts/ux/columnNodeUI.js"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../modulosProcesos/Scripts/solicitudRevalidacionMaterias.js.php"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>



</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog())
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"; style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					$consulta="select distinct(pO.idOpcion),tM.textoMenu,tM.colorFondo from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM,808_titulosMenu tM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2 and tM.idMenu=pO.idOpcion order by tM.textoMenu";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$colorFondo=$filaM[2];
						if($colorFondo=="")
							$colorFondo=$unico[4];
						echo '<li><a href="javascript:function(){}" style="z-index:190 !important">'.$filaM[1].'</a><ul>';
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino,oP.nombreBullet,oP.idOpciones from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							$comp="<img src='../images/s.gif' width='16' height='16'> ";
							if($fila[2]!="")
							{
								$comp="<img src='../media/verBullet.php?id=".$fila[3]."' width='16' height='16'> ";
							}
							echo '<li style="background-color:#'.$colorFondo.' ; z-index:200000 !important"><a href="'.$fila[1].'">'.$comp.$fila[0].'</a></li>';
						}
						echo "</ul></li>";
					}
					
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion),tM.textoMenu,tM.colorFondo from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM,808_titulosMenu tM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1 and tM.idMenu=pO.idOpcion order by tM.textoMenu";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
						$colorFondo=$filaM[2];
						if($colorFondo=="")
							$colorFondo=$unico[4];
						echo '<li><a href="javascript:function(){}" style="z-index:190 !important">'.$filaM[1].'</a><ul>';
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino,oP.nombreBullet,oP.idOpciones from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							$comp="";
							if($fila[2]!="")
							{
								$comp="<img src='../media/verBullet.php?id=".$fila[3]."' width='16' height='16'> ";
							}
							echo '<li style="background-color:#'.$colorFondo.'; z-index:190000 !important"><a href="'.$fila[1].'">'.$comp.$fila[0].'</a></li>';
						}
						echo "</ul></li>";
					}
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        		<ul class="sf-menu sf-vertical">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu,tm.colorFondo 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									$tabla="";
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$colorFondo=$fila[2];
										$colorFondo="382637";
										$comp="style='z-index:185000 !important'";
										if($colorFondo=="")
											$colorFondo=$unico[7];
										if($colorFondo!="")
										{
											$comp='style="background-color:#'.$colorFondo.' !important;z-index:185000"';
										}
										$opciones=generarOpciones($fila[1],"",$colorFondo,$unico[5]);
										$menuComp="";
										if($opciones!="")
										{
											$menuComp="<ul>".$opciones."</ul>";
										}
										$obj="
												<li class='current' ".$comp.">
													<a href='javascript:function(){}' style='color:#".$unico[5]."'>&nbsp;&nbsp;".$fila[0]."</a>".$menuComp."
												</li>
												";
										/*$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';*/
										if($tabla=="")
											$tabla=$obj;
										else
											$tabla.="".$obj;
										
									}
									echo $tabla;
								?>
                                </ul>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        	<table>
                            	<tr>
                            	<td align="left">
                                
                                	<?php
										$idFormulario=-1;
										if(isset($_POST["idFormulario"]))
											$idFormulario=$_POST["idFormulario"];
										$idRegistro=-1;
										if(isset($_POST["idRegistro"]))
											$idRegistro=$_POST["idRegistro"];
										
										$rvoe="";
										$nPEstudio="";
										$descripcion="";
										$gradoInscribe="";
										
										
										$consulta="SELECT idReferencia,codigoInstitucion FROM _678_tablaDinamica WHERE id__678_tablaDinamica=".$idRegistro;

										$fSolicitud=$con->obtenerPrimeraFila($consulta);
										$idReferencia=$fSolicitud[0];
										$plantel=$fSolicitud[1];
										$consulta="SELECT * FROM 4573_solicitudesInscripcion WHERE idSolicitudInscripcion=".$idReferencia;
										$fRegistro=$con->obtenerPrimeraFila($consulta);
										$objInscripcion=json_decode($fRegistro[3]);
										
										
										$consulta="SELECT id__685_tablaDinamica,idEstado FROM _685_tablaDinamica WHERE idReferencia=".$idRegistro;
										$fSolicitudReval=$con->obtenerPrimeraFila($consulta);
										$gradoInscribe=0;
										$idEstado=0;	
										$idSolicitudEval=-1;
										$lblSituacion="";
										if($fSolicitudReval)
										{
											$idSolicitudEval=$fSolicitudReval[0];
											$consulta="SELECT gradoInscribe,comentarios FROM 4574_solicitudesRevalidacion WHERE idFormulario=685 AND idReferencia=".$idSolicitudEval;
											$fSolicitud=$con->obtenerPrimeraFila($consulta);
											$gradoInscribe=$fSolicitud[0];
											$comentarios=$fSolicitud[1];
											$idEstado=$fSolicitudReval[1];	
											
											$consulta="SELECT nombreEtapa FROM 4037_etapas WHERE idProceso=105 AND numEtapa=".$idEstado;
											$lblSituacion=$con->obtenerValor($consulta);
											
										}
										$consulta="SELECT nombre FROM 800_usuarios WHERE idUsuario=".$fRegistro[2];
										$nSolicitante=$con->obtenerValor($consulta);
										$idOrigen="";
										$idEscuelaOrigen=2;
										$nPlanEstudio="";
										$arrMaterias="";
										$idPlanEstudioDestino=$objInscripcion->idInstanciaPlan;
										$idPlanEstudioOrigen="";
										$tipoPlan="";
										if($idEscuelaOrigen==2)//Externa
										{
											$consulta="SELECT nombreEscuela,planEstudios FROM _708_tablaDinamica WHERE idReferencia=".$idRegistro;
											$fDatosOrigen=$con->obtenerPrimeraFila($consulta);
											$idOrigen=$fDatosOrigen[0];
											$idPlanEstudioOrigen=$fDatosOrigen[1];
											$tipoPlan=1;
											$cEscuela="SELECT nombreEscuela FROM 6017_escuelaOrigenInscripcion WHERE idEscuelaOrigen=".$idOrigen;
											$cPlanEstudio="SELECT CONCAT('[',IF(rvoe IS NULL,'',rvoe),'] ',nombrePlanEstudios,' (',descripcion,')'),rvoe,nombrePlanEstudios,descripcion FROM 6018_planesEstudioEscuelaOrigen WHERE idPlanEstudioEscuelas=".$idPlanEstudioOrigen;
											$fPlanEstudio=$con->obtenerPrimeraFila($cPlanEstudio);
											$nPlanEstudio=$fPlanEstudio[0];
											$rvoe=$fPlanEstudio[1];
											$nPEstudio=$fPlanEstudio[2];
											$descripcion=$fPlanEstudio[3];
											
											
										}
										else
										{
											$tipoPlan=0;
											$idOrigen='';
											$cEscuela="SELECT unidad FROM 817_organigrama where codigoUnidad='".$idOrigen."'";
											$nPlanEstudio=obtenerNombreInstanciaPlan($idPlanEstudioOrigen);
											$consulta="SELECT idPlanEstudio,cveRvoe,nombrePlanEstudios,p.descripcion FROM 4513_instanciaPlanEstudio i,4500_planEstudio p  
														WHERE idInstanciaPlanEstudio=".$idPlanEstudioOrigen." and i.idPlanEstudio=p.idPlanEstudio";
											
											$fPlanDatos=$con->obtenerPrimeraFila($consulta);
											$idPlanEstudioOrigen=$fPlanDatos[0];
											$rvoe=$fPlanEstudio[1];
											$nPEstudio=$fPlanEstudio[2];
											$descripcion=$fPlanEstudio[3];
											
											
										}
										$consulta="SELECT idPlanEstudio FROM 4513_instanciaPlanEstudio i WHERE idInstanciaPlanEstudio=".$idPlanEstudioDestino;
										$idPlanEstudioDestino=$con->obtenerValor($consulta);
										
										
										$consulta="SELECT idEquivalenciaPlanEstudio FROM 4500_equivalenciasPlanEstudio WHERE  idPlanEstudioBase=".$idPlanEstudioDestino." AND 
																tipoPlanEstudioEquivalencia=".$tipoPlan." AND idPlanEstudioEquivalencia=".$idPlanEstudioOrigen;
										$idEquivalenciaPlanEstudio=$con->obtenerValor($consulta);
										
										$nEscuelaOrigen=$con->obtenerValor($cEscuela);
										$nPlanEstudioDestino=obtenerNombreInstanciaPlan($idPlanEstudioDestino);
										$idInstanciaPlan=$idPlanEstudioDestino;
										
										$consulta="SELECT g.idGrado,leyendaGrado  FROM 4505_estructuraCurricular e,4501_Grado g WHERE g.idGrado=e.idUnidad AND e.tipoUnidad=3 AND e.idPlanEstudio=".$idPlanEstudioDestino." ORDER BY ordenGrado";
										$arrGrados=$con->obtenerFilasArreglo($consulta);
										
										
									?>
                                    <span id="tRevalidacion" class="x-hide-display">
                                    <br /><br />
                                    
                                    <table>
                                    <tr>
                                        <td width="20">
                                        </td>
                                        <td>
                                            <span id="tblDatosRevalidacion"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20">
                                        </td>
                                        <td>
                                            <span id="tblSituacion"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20">
                                        </td>
                                        <td>
                                            <span id="tblRevalidacion"></span>
                                        </td>
                                    </tr>
                                    </table>
                                       
                                    </span>
                               </td>
                               </tr>
                           </table>
                           <input type="hidden" id="idInstanciaPlan" value="<?php echo $idInstanciaPlan ?>" />
                           <input type="hidden" id="idPlanEstudioOrigen" value="<?php echo $idPlanEstudioOrigen ?>" />
                           <input type="hidden" id="gradoInscribe" value="<?php echo $gradoInscribe?>" />
                           <input type="hidden" id="comentarios" value="<?php echo bE($comentarios)?>" />
                            <input type="hidden" id="idRegistroInscripcion" value="<?php echo $idRegistro?>" />
                           
                           <input type="hidden" id="idEstado" value="<?php echo $idEstado?>" />
                           <input type="hidden" id="idEscuelaOrigen" value="<?php echo $idOrigen ?>" />
                           <input type="hidden" id="nSolicitante" value="<?php echo bE($nSolicitante) ?>" />
                           <input type="hidden" id="nEscuelaOrigen" value="<?php echo bE($nEscuelaOrigen) ?>" />
                           <input type="hidden" id="nPlanEstudio" value="<?php echo bE($nPlanEstudio) ?>" />
                           <input type="hidden" id="nPlanEstudioDestino" value="<?php echo bE($nPlanEstudioDestino) ?>" />
							<input type="hidden" id="nPEstudio"  value="<?php echo bE($nPEstudio)?>" />
                            <input type="hidden" id="rvoe"  value="<?php echo bE($rvoe)?>" />
                            <input type="hidden" id="descripcion"  value="<?php echo bE($descripcion)?>" />
                           <input type="hidden" id="idFormulario"  value="685" />
                           <input type="hidden" id="idRegistro"  value="<?php echo $idSolicitudEval?>" />
                           <input type="hidden" id="idReferencia"  value="<?php echo $idRegistro?>" />
                           
                           <input type="hidden" id="lblSituacion"  value="<?php echo $lblSituacion?>" />
                           <input type="hidden" id="arrGrados"  value="<?php echo bE($arrGrados)?>" />
                           
                           
                        	
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
