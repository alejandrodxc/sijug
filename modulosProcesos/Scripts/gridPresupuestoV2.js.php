<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$arrPosiciones[0]="Primer";
	$arrPosiciones[1]="Segundo";
	$arrPosiciones[2]="Tercer";
	$arrPosiciones[3]="Cuarto";
	$arrPosiciones[4]="Quinto";
	$arrPosiciones[5]="Sexto";
	$arrPosiciones[6]="Septimo";
	$arrPosiciones[7]="Octavo";
	$arrPosiciones[8]="Noveno";
	$arrPosiciones[9]="Décimo";
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	$consulta="SELECT idProceso FROM `900_formularios` WHERE idFormulario=".$idFormulario;
	$idProceso=$con->obtenerValor($consulta);
	$consulta="SELECT idConceptoGrid,nombreConcepto,nombreCortoConcepto,montoMaximo,funcionConstruccion FROM `100_conceptosGridPresupuesto`
			WHERE idProceso=".$idProceso." ORDER BY nombreConcepto";
	$monto=0;
	$resConceptos=$con->obtenerFilas($consulta);
	$ct=0;
	while($fila=mysql_fetch_row($resConceptos))
	{
		
		$arreglo[$ct]["idRubro"]=$fila[0];
		$arreglo[$ct]["tituloPanel"]=$fila[1];
		$arreglo[$ct]["tituloConcepto"]=$fila[2];
		$arreglo[$ct]["arreglo"]="";
		$arreglo[$ct]["montoMaximo"]=$fila[3];
		$arreglo[$ct]["funcionConstruccion"]=$fila[4];
		$ct++;
	}

	$aRubro="";
	$tamano=sizeof($arreglo);
	foreach($arreglo as $key=>$resto)
	{
		$consulta="SELECT idGridVSCalculo,concepto,costoUnitario,cantidad,total,idRubro,montoAutorizado  FROM 100_gridPresupuesto WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro." AND idRubro=".$resto["idRubro"]." and eliminado=0 order by concepto";
		$storeA=$con->obtenerFilasArreglo($consulta);
		$arreglo[$key]["arreglo"]=$storeA;
		if($aRubro=="")
			$aRubro=$resto["idRubro"];
		else
			$aRubro.=",".$resto["idRubro"];
	}
	
	
	$ocultarBtn="false";
	if(isset($_GET["sL"]))
		$ocultarBtn="true";
	$consulta="select codigoCompleto,tituloCentroC from 506_centrosCosto order by tituloCentroC";
	$arrCentroC=uEJ($con->obtenerFilasArreglo($consulta));
	$consulta="select idPais,upper(nombre) from 238_paises order by nombre";
	$arrPaises=uEJ($con->obtenerFilasArreglo($consulta));	
	$consulta="SELECT cveEstado,upper(estado) FROM 820_estados ORDER BY estado";
	$arrEstados=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT valor,texto FROM 1004_siNo";	
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idSituacionPatrocinio,situacionPatrocinio FROM 1000_situacionesPatrocinio";
	$arrSituacionesPatrocinio=$con->obtenerFilasArreglo($consulta);
	
	$consulta="select Fechainiciopro,fechatermino from _".$idFormulario."_tablaDinamica where id__".$idFormulario."_tablaDinamica=".$idRegistro;
	$filaProto=$con->obtenerPrimeraFila($consulta);
	$mesIni=date("Y-m-01",strtotime($filaProto[0]));
	$mesFin=date("Y-m-01",strtotime($filaProto[1]));
	$cadMeses="";
	$arrCampos="";
	$arrColumnas="";
	$ct=0;
	
	while($mesIni!=$mesFin)
	{
		if($cadMeses=="")
			$cadMeses="'0'";
		else
			$cadMeses.=",'0'";
		if($arrCampos=="")
			$arrCampos="{name:'mes".$ct."'}";
		else
			$arrCampos.=",{name:'mes".$ct."'}";
		$columna=" {
					  header:'Mes ".($ct+1)."',
					  width:100,
					  sortable:true,
					  dataIndex:'mes".$ct."',
					  editor: new Ext.form.NumberField(
													   {
														allowBlank: false,
														allowNegative: false
													   }
													 ),
					  renderer:function(val,meta,registro)
						  {
								  return Ext.util.Format.usMoney(val);
						  }
				  }";
		if($arrColumnas=="")
			$arrColumnas=$columna;
		else
			$arrColumnas.=",".$columna;													
		$ct++;
		$mesIni=date("Y-m-01",strtotime("+1 month",strtotime($mesIni)));
	}
	
	if($cadMeses=="")
		  $cadMeses="'0'";
	  else
		  $cadMeses.=",'0'";
	  if($arrCampos=="")
		  $arrCampos="{name:'mes".$ct."'}";
	  else
		  $arrCampos.=",{name:'mes".$ct."'}";
	  $columna=" {
					header:'Mes ".($ct+1)."',
					width:100,
					sortable:true,
					dataIndex:'mes".$ct."',
					editor: new Ext.form.NumberField(
													 {
													  allowBlank: false,
													  allowNegative: false
													 }
												   ),
					renderer:function(val,meta,registro)
						{
								return Ext.util.Format.usMoney(val);
						}
				}";
	  if($arrColumnas=="")
		  $arrColumnas=$columna;
	  else
		  $arrColumnas.=",".$columna;		
	  $ct++;
		
	
	
	$arrMeses="[".$cadMeses."]";
	
	
	$consulta="SELECT idPatrocinador,p.codigoInstitucion,unidad,montoPatrocinio,observaciones,porcentajeRetencion,montoRetencion,(montoPatrocinio-montoRetencion)
			 FROM 9036_patrocinadoresProyectos p,817_organigrama o WHERE o.codigoUnidad=p.codigoInstitucion and idFormulario=".$idFormulario." and idReferencia=".$idRegistro;

	$arrPatrocinio=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT fechainiciopro,fechatermino FROM _278_tablaDinamica WHERE id__278_tablaDinamica=".$idRegistro;
	$filaFecha=$con->obtenerPrimeraFila($consulta);
	$fInicio="";
	$fFin="";
	if($filaFecha[0]!="")
		$fInicio=strtotime($filaFecha[0]);
	if($filaFecha[1]!="")
		$fFin=strtotime($filaFecha[1]);	
	$nAnios=1;
	if($filaFecha[0]!="")
	{
		$nAnios=(date("Y",$fFin)-date("Y",$fInicio))+1;
	}

	$arrDatos="";
	for($x=0;$x<$nAnios;$x++)
	{
		$consulta="SELECT nAnio,trimestre1,trimestre2,trimestre3,trimestre4,idAnioPresupuesto FROM 100_anioPresupuestoDistribucion
					WHERE idFormulario=".$idFormulario." AND idRegistro=".$idRegistro." AND nAnio=".$x;
		$filaA=$con->obtenerPrimeraFila($consulta);
		if(!$filaA)
			$obj="['".$arrPosiciones[$x]." a&ntilde;o','".$x."','0','0','0','0','-1']";
		else
			$obj="['".$arrPosiciones[$x]."  a&ntilde;o','".$x."','".$filaA[1]."','".$filaA[2]."','".$filaA[3]."','".$filaA[4]."','".$filaA[5]."']";
		if($arrDatos=="")
			$arrDatos=$obj;
		else
			$arrDatos.=",".$obj;
	}
	
	$arrDatos="[".$arrDatos."]";
	
	
	$consulta="SELECT o.codigoUnidad,o.unidad FROM 817_elementosOrganigramaVSCategorias e,817_organigrama o WHERE idCategoria=1 AND o.idOrganigrama=e.idOrganigrama ORDER BY o.unidad";
	$arrInstituciones=$con->obtenerFilasArreglo($consulta);
	
			
?>
var arrRubros=[<?php echo $aRubro?>];
var arrInstituciones=<?php echo $arrInstituciones?>;
var arrAnios=['Primer a&ntilde;o','Segundo a&ntilde;o','Tercer a&ntilde;o','Cuarto a&ntilde;o','Quinto a&ntilde;o','Sexto a&ntilde;o','S&eacute;ptimo a&ntilde;o','Octavo a&ntilde;o','Noveno a&ntilde;o','D&eacute;cimo a&ntilde;o'];

Ext.onReady(inicializar);
var arrEstados=<?php echo $arrEstados?>;
var arrMeses=[<?php echo $arrMeses?>];
var ocultarBotones=<?php echo $ocultarBtn?>;
var arrSiNo=<?php echo $arrSiNo?>;
var arrSituacionesPatrocinio=<?php echo $arrSituacionesPatrocinio?>;
var dsDatos=<?php echo $arrDatos?>;

function inicializar()
{
	if(gE('inicializarGrid').value=='0')
    	return;
        
        
    var tblPanel=new Ext.TabPanel	(
    									{
                                        	height:450,
                                            width:900,
                                            renderTo:'tblPanel',
                                            activeTab:0,
                                            tbar:	[
                                                        {
                                                            icon:'../images/coins.png',
                                                            cls:'x-btn-text-icon',
                                                            text:'Ver situaci&oacute;n presupuestal',
                                                            handler:function()
                                                                    {
                                                                        verSituacionPresupuestal();
                                                                    }
                                                            
                                                        }
                                            		],
                                            items:	[
                                                        {
                                                        	id:'tPatrocinadores',
                                                            contentEl:'tblPatrocinadores',
                                                            title:'Patrocinadores del protocolo'
                                                        } ,
                                                        {
                                                        	id:'tDesgloce',
                                                            contentEl:'tblRubros',
                                                            title:'Desgloce de gasto'
                                                        },
                                                        {
                                                        	id:'tMinistraciones',
                                                            contentEl:'tblMinistraciones',
                                                            title:'Ministraciones'
                                                        }  
                                                        
                                                    ]
                                    	}
    								) 

	tblPanel.on('tabchange',function(tab,p)        
    						{
                            	switch(p.id)
                                {
                                	case 'tDesgloce':
                                    	gEx('tabGridRubros').setActiveTab(0);
                                    break;
                                    case 'tMinistraciones':
                                    	gEx('gridRecepcion').getView().refresh();
                                    break;
                                }
                            }
                )
	var mascara=new Mask('$#,###.00','number');
    var gridInstitucionesPatrocinadoras=crearGridInstitucionesPatrocinadoras();
    var gridFondos=crearGridFondos();
    var tabPresupuesto=mostrarTab();
	sumarTotal();   
    sumarDistribucion();                 
}

function crearGridFondos()
{
	
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'nAnio'},
                                                                {name: 'numAnio'},
                                                                {name: 'primer'},
                                                                {name: 'segundo'},
                                                                {name: 'tercer'},
                                                                {name: 'cuarto'},
                                                                {name: 'idAnioPresupuesto'}
                                                                
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editor_gridRecepcion',
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:2
                                                        }
                                                    );
    editorFila.on('beforeedit',funcEditorFilaBeforeEditDist)
    editorFila.on('validateedit',funcEditorValidaDist);
    editorFila.on('canceledit',funcEditorCancelEditDist);
    var summary = new Ext.ux.grid.GridSummary();
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'',
															width:120,
															sortable:true,
															dataIndex:'nAnio'
														},
														{
															header:'Primer trimestre',
															width:150,
															sortable:true,
															dataIndex:'primer',
                                                            renderer:'usMoney',
                                                            summaryType:'sum',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:true,
                                                                        id:'txtPrimer'
                                                            		}
														},
                                                        {
															header:'Segundo trimestre',
															width:150,
															sortable:true,
															dataIndex:'segundo',
                                                            renderer:'usMoney',
                                                            summaryType:'sum',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:true
                                                            		}
														},
                                                        {
															header:'Tercer trimestre',
															width:150,
															sortable:true,
															dataIndex:'tercer',
                                                            renderer:'usMoney',
                                                            summaryType:'sum',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:true
                                                            		}
														},
                                                        {
															header:'Cuarto trimestre',
															width:150,
															sortable:true,
															dataIndex:'cuarto',
                                                            renderer:'usMoney',
                                                            summaryType:'sum',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:true
                                                            		}
														}
                                                       
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridRecepcion',
                                                            store:alDatos,
                                                            frame:true,
                                                            renderTo:'grid3',
                                                            cm: cModelo,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            plugins:[editorFila,summary],
                                                            height:260,
                                                            width:850,
                                                            x:30,
                                                            y:650,
                                                            sm:chkRow,
                                                            tbar:	[
                                                                        {
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar',
                                                                            hidden:<?php echo $ocultarBtn?>,
                                                                            handler:function()
                                                                                    {
                                                                                        var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el registro que desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        var pos=obtenerPosFila(tblGrid.getStore(),'numAnio',fila.get('numAnio'));
                                                                                        editorFila.stopEditing();
                                                                                        editorFila.startEditing(pos);	
                                                                                    }
                                                                            
                                                                        },'-'
                                                                    ],
                                                              bbar:	[		
                                                              			{
                                                                              xtype:'label',
                                                                              html:'&nbsp;&nbsp;<font color="black">Monto patrocinio:</font>'              	
                                                                                                
                                                                        },
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<label id="lblMontoPresupuestadoDist"></label>'
                                                                        },'-',
                                                                         {
                                                                              xtype:'label',
                                                                              html:'<font color="black">Monto total desglozado:</font>'              	
                                                                                                
                                                                        },
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<label id="sumaTotalDist"></label>'
                                                                        },'-',
                                                                         
                                                                         {
                                                                              xtype:'label',
                                                                              html:'&nbsp;&nbsp;<font color="black">Diferencia:</font>'              	
                                                                                                
                                                                        },
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<label id="lblDiferenciaDist"></label>'
                                                                        }
                                                                    ]
                                                        }
                                                    );
	tblGrid.on('beforeedit',antesEditPresupuestoRecepcion);                                                    
	return 	tblGrid;	
}

function sumarDistribucion(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridRecepcion').getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var total=parseFloat(elemento.get('primer'))+parseFloat(elemento.get('segundo'))+parseFloat(elemento.get('tercer'))+parseFloat(elemento.get('cuarto'));
        sumaGrid=sumaGrid+total;
    }
	
    
    var et=gE('sumaTotalDist');
    et.innerHTML='<font color=\'#005500\'><span id="spSumaTotalDist">'+Ext.util.Format.usMoney(sumaGrid)+'</span></font>';
    et=gE('spTotalPresupuestadoDist');
    var totalPresupuestado=parseFloat(normalizarValor(et.innerHTML));

    var diferencia=totalPresupuestado-sumaGrid;
    et=gE('lblDiferenciaDist');
    if(diferencia==0)
    	et.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(diferencia)+'</span></font>';
    else
    	if(diferencia<0)
	    	et.innerHTML='<font color=\'#FF0000\'><span id="spTotal">'+Ext.util.Format.usMoney(diferencia)+'</span></font>';
        else
        	et.innerHTML='<font color=\'#0000FF\'><span id="spTotal">'+Ext.util.Format.usMoney(diferencia)+'</span></font>';
   return sumaGrid;
}

function antesEditPresupuestoRecepcion(e)
{
	if(ocultarBotones)
    	e.cancel=true;
}

function funcEditorFilaBeforeEditDist(rowEdit,fila)
{
    var grid=gEx('gridRecepcion');
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
    gEx('txtPrimer').focus(false,500);
    if(ocultarBotones)
    	return false;

}

function funcEditorValidaDist(rowEdit,obj,registro,nFila)
{
	var grid=Ext.getCmp('gridRecepcion');
  	
  	var x;
    var fila;
    var totalGlobal=0;
    var totalFila=0;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	if(x!=nFila)
        {
            fila=grid.getStore().getAt(x);
            totalGlobal+=parseFloat(fila.data.primer)+parseFloat(fila.data.segundo)+parseFloat(fila.data.tercer)+parseFloat(fila.data.cuarto);
		}
    }
   totalGlobal+=parseFloat(obj.primer)+parseFloat(obj.segundo)+parseFloat(obj.tercer)+parseFloat(obj.cuarto);
   var montoPatrocinio=parseFloat(normalizarValor(Ext.util.Format.stripTags(gE('lblMontoPresupuestadoDist').innerHTML)));
	
   if(totalGlobal>montoPatrocinio)
    {
        function respPatrocinio()
        {
            var copiaRegistro=grid.copiaRegistro;
    
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=registro;
        
            for(x=0;x<arrCampos.items.length;x++)
            {
                filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
            }
            
            rowEdit.startEditing(nFila);
            //grid.getView().refresh();	
        
        }
        msgBox('El monto total de las ministraciones (<b>'+Ext.util.Format.usMoney(totalGlobal)+'</b>) excede el monto presupuestado (<b>'+Ext.util.Format.usMoney(montoPatrocinio)+'</b>)',respPatrocinio);
        return false;
    }
     
   
    var cadObj='{"idAnioPresupuesto":"'+registro.data.idAnioPresupuesto+'","idRegistro":"<?php echo $idRegistro ?>","idFormulario":"<?php echo $idFormulario?>","nAnio":"'+registro.data.numAnio+'","t1":"'+obj.primer+
    			'","t2":"'+obj.segundo+'","t3":"'+obj.tercer+'","t4":"'+obj.cuarto+'"}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
        	sumarDistribucion();
        	registro.data.idAnioPresupuesto=arrResp[1];
            grid.getView().refresh();	
        }
        else
        {
        	var copiaRegistro=grid.copiaRegistro;
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=grid.registroEdit;
            for(x=0;x<arrCampos.items.length;x++)
            {
                registro.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
            }
            grid.getView().refresh();	
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=9&cadObj='+cadObj,true);
   
    return true;
}

function funcEditorCancelEditDist(rowEdit,obj,registro,nFila)
{
	var idGrid='gridRecepcion';
	var grid=Ext.getCmp(idGrid);
	var copiaRegistro=grid.copiaRegistro;
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;
    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }
}

function crearGridInstitucionesPatrocinadoras()
{
	var dsDatos=<?php echo $arrPatrocinio?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name:'idInstitucionPatrocinadora'},
                                                                    {name:'codInstitucion'},
                                                                    {name:'institucion'},
                                                                    {name: 'montoPatrocinado'},
                                                                    {name: 'observaciones'},
                                                                    {name: 'porcentajeRetencion'},
                                                                    {name: 'montoRetencion'},
                                                                    {name: 'montoDisponible'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
    var summary = new Ext.ux.grid.GridSummary();
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Instituci&oacute;n',
															width:250,
															sortable:true,
															dataIndex:'institucion'
														},
														{
															header:'Monto patrocinado',
															width:110,
															sortable:true,
                                                            summaryType:'sum',
                                                            css:'text-align:right;',
															dataIndex:'montoPatrocinado',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'% Fondo<br>de apoyo',
															width:75,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'porcentajeRetencion'
														},
                                                        {
															header:'Monto Fondo de apoyo',
															width:130,
															sortable:true,
                                                            summaryType:'sum',
                                                            css:'text-align:right;',
															dataIndex:'montoRetencion',
                                                            renderer:'usMoney'
														},
                                                         {
															header:'Monto disponible',
															width:130,
															sortable:true,
                                                            summaryType:'sum',
                                                            css:'text-align:right;',
															dataIndex:'montoDisponible',
                                                            renderer:function(val)
                                                            		{
                                                                    	return '<b>'+Ext.util.Format.usMoney(val)+'</b>';
                                                                    }
														},
                                                        {
															header:'Observaciones',
															width:450,
															sortable:true,
															dataIndex:'observaciones',
                                                            renderer:function(val)
                                                            			{
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPatrocinio',
                                                            store:alDatos,
                                                            frame:true,
                                                            x:30,
                                                            y:90,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            cm: cModelo,
                                                            height:290,
                                                            width:850,
                                                            sm:chkRow,
                                                            plugins:[summary],
                                                            renderTo:'grid1',
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBotones,
                                                                            text:'Agregar instituci&oacute;n patrocinadora',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarInstitucion(-1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBotones,
                                                                            text:'Modificar instituci&oacute;n patrocinadora',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la instituci&oacute;n patrocinadora que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaAgregarInstitucion(fila.get('idInstitucionPatrocinadora'));
                                                                                    }
                                                                            
                                                                        },'-',
                                                                         {
                                                                        	icon:'../images/magnifier.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:!ocultarBotones,
                                                                            text:'Ver ficha del patrocinio',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la instituci&oacute;n patrocinadora cuya ficha desea observar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaAgregarInstitucion(fila.get('idInstitucionPatrocinadora'));
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:ocultarBotones,
                                                                            text:'Remover instituci&oacute;n patrocinadora',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la instituci&oacute;n patrocinadora que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                         tblGrid.getStore().remove(fila);
                                                                                                         sumarTotal();
                                                                                                         refrescarMenuDTD();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=92&idPatrocinador='+fila.get('idInstitucionPatrocinadora'),true);
                                                                                               
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Esta seguro de querer remover la instituci&oacute;n patrocinaora seleccionada?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		],
                                                        	 bbar:	[		
                                                                         {
                                                                              xtype:'label',
                                                                              hidden:true,
                                                                              html:'<font color="black">Monto total:</font>'              	
                                                                                                
                                                                        },
                                                                        {
                                                                            xtype:'label',
                                                                            hidden:true,
                                                                            html:'&nbsp;&nbsp;<label id="lblMontoTotal"></label>'
                                                                        }
                                                                    ]
                                                        }
                                                    );
	return 	tblGrid;
}

function  mostrarTab()
{
    var arregloTabs=[];
    var panel;
    var grid;
    <?php

	foreach($arreglo as $key=>$resto)
    {
       
       $titulo=$resto["tituloPanel"];
       $storeGrid=$resto["arreglo"];
       $idRubro=$resto["idRubro"];
	   $tituloConcepto=$resto["tituloConcepto"];
	   $montoMaximo=$resto["montoMaximo"];
	   $funcionConstruccion=$resto["funcionConstruccion"];
	   echo '	
	   			grid=	gridGenerico('.$storeGrid.',"'.$tituloConcepto.'",'.$idRubro.','.$montoMaximo.',"'.$funcionConstruccion.'");
	   			panel=		{
								xtype:"panel",
								title:"'.$titulo.'",
								items:[grid]
							};
				arregloTabs.push(panel);
				
				';
	   
    }
    ?>
    var tabs = new Ext.TabPanel	(
									{
                                    	x:30,
                                        y:320,
                                        height:290,
                                        width:850,
										enableTabScroll  :true,
										id:'tabGridRubros',
                                        frame:true,
                                        border:true,
                                        renderTo:'grid2',
                                        items:	arregloTabs,
                                        bbar:	[		
                                        			{
                                                          xtype:'label',
                                                          html:'&nbsp;&nbsp;<font color="black">Monto patrocinio:</font>'              	
                                                                            
                                                    },
                                                    {
                                                        xtype:'label',
                                                        html:'&nbsp;&nbsp;<label id="lblMontoPresupuestado"></label>'
                                                    },'-',
                                        			 {
                                                          xtype:'label',
                                                          html:'<font color="black">Monto total desglozado:</font>'              	
                                                                            
													},
                                                    {
                                                    	xtype:'label',
                                                        html:'&nbsp;&nbsp;<label id="sumaTotalE"></label>'
                                                    },'-',
                                                     
                                                     {
                                                          xtype:'label',
                                                          html:'&nbsp;&nbsp;<font color="black">Diferencia:</font>'              	
                                                                            
                                                    },
                                                    {
                                                        xtype:'label',
                                                        html:'&nbsp;&nbsp;<label id="lblDiferencia"></label>'
                                                    }
                                        		]
									}
								);
	return tabs;                                
    
}

function gridGenerico(arregloG,titulo,idRubro,montoMaximo,funcionConstruccion)
{
	var arrDatos=arregloG;
    var iR=idRubro;
    if((funcionConstruccion==undefined) ||(funcionConstruccion==''))
    {
        var dSetGenerico= new Ext.data.SimpleStore	(
                                                        {
                                                            fields:	[
                                                                        
                                                                        {name:'idGridVSCalculo'},
                                                                        {name:'calculo'},
                                                                        {name:'costoUnitario'},
                                                                        {name:'cantidad'},
                                                                        {name: 'total'},
                                                                        {name:'idRubro'},
                                                                        {name: 'montoAutorizado'}
                                                                    ]
                                                        }
                                                     )
        
        dSetGenerico.loadData(arrDatos);	
        var summary = new Ext.ux.grid.GridSummary();
        var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
        var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            id:'editor_'+idRubro,
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:2
                                                        }
                                                    );
        editorFila.on('beforeedit',funcEditorFilaBeforeEditGridCalculo)
        editorFila.on('validateedit',funcEditorValidaGridCalculo);
        editorFila.on('canceledit',funcEditorCancelEditGridCalculo);
        var cmGenerico= new Ext.grid.ColumnModel   	(
                                                            [
                                                                new  Ext.grid.RowNumberer(),
                                                                columnaCheck,
                                                                {
                                                                    header:titulo,
                                                                    width:350,
                                                                    sortable:true,
                                                                    dataIndex:'calculo',
                                                                    editor:{xtype:'textfield'}
                                                                },
                                                                {
                                                                    header:'Costo Unitario',
                                                                    width:90,
                                                                    sortable:true,
                                                                    dataIndex:'costoUnitario',
                                                                    editor:{xtype:'numberfield'},
                                                                    renderer:'usMoney'
                                                                },
                                                                {
                                                                    header:'Cantidad',
                                                                    width:90,
                                                                    sortable:true,
                                                                    dataIndex:'cantidad',
                                                                    editor:{xtype:'numberfield'},
                                                                    renderer:function(val,meta,registro)
                                                                            {
                                                                            	var cantidadR=registro.data.cantidad;
                                                                                cantidadR=parseFloat(cantidadR);
                                                                                var costoU=registro.data.costoUnitario;
                                                                                costoU=parseFloat(costoU);
                                                                                var total=cantidadR*costoU;
                                                                                registro.data.total=total;
                                                                                return Ext.util.Format.number(val,'0,0.00');
                                                                                
                                                                            }
                                                                },
                                                                {
                                                                    header:'Total',
                                                                    width:100,
                                                                    sortable:true,
                                                                    dataIndex:'total',
                                                                    summaryType:'sum',
                                                                    renderer:'usMoney'
                                                                   
                                                                },
                                                                 {
                                                                    header:'Monto autorizado',
                                                                    width:110,
                                                                    sortable:true,
                                                                    dataIndex:'montoAutorizado',
                                                                    hidden:true,
                                                                    renderer:function(val,meta,registro)
                                                                            {
                                                                                var cantidadR=registro.data.cantidad;
                                                                                cantidadR=parseFloat(cantidadR);
                                                                                var costoU=registro.data.costoUnitario;
                                                                                costoU=parseFloat(costoU);
                                                                                var total=cantidadR*costoU;
                                                                                if(parseFloat(val)==total)
                                                                                {
                                                                                    return '<font color="#005500">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                                }
                                                                                else
                                                                                    return '<font color="#B0281A">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                            }
                                                                }
                                                            ]
                                                        );
                                                
                                                    
        tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridCalculo_'+idRubro,
                                                            store:dSetGenerico,
                                                            frame:true,
                                                            stripeRows :true,
                                                            cm: cmGenerico,
                                                            sm:columnaCheck,
                                                            height:293,
                                                            width:850,
                                                            plugins:[editorFila,summary],
                                                            tbar:[
                                                                    {
                                                                        id:'btnAdd_'+idRubro,
                                                                        text:'Agregar '+titulo,
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        hidden:ocultarBotones,
                                                                        handler:function()
                                                                                {
                                                                                    
                                                                                    var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                    
                                                                                    var editorFila=gEx('editor_'+idRubro);
                                                                                    var registroGrid=crearRegistro([
                                                                                                                    {name:'idGridVSCalculo'},
                                                                                                                    {name:'calculo'},
                                                                                                                    {name:'costoUnitario'},
                                                                                                                    {name:'cantidad'},
                                                                                                                    {name:'total'},
                                                                                                                    {name:'idRubro'},
                                                                                                                    {name: 'montoAutorizado'}
                                                                                                                ]);
                                                                                   
                                                                                    
                                                                                    var nReg=new registroGrid	(
                                                                                                                    {
                                                                                                                        idGridVSCalculo:'-1',
                                                                                                                        calculo:'',
                                                                                                                        costoUnitario:'',
                                                                                                                        cantidad:'',
                                                                                                                        total:'0',
                                                                                                                        idRubro:iR,
                                                                                                                        montoAutorizado:'0'
                                                                                                                    }
                                                                                                                )
                                                                                    
                                                                                    editorFila.stopEditing();
                                                                                    tblGrid.getStore().add(nReg);
                                                                                    tblGrid.nuevoRegistro=true;
                                                                                    editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                    Ext.getCmp('btnAdd_'+idRubro).disable();
                                                                                    Ext.getCmp('btnDel_'+idRubro).disable();																	
                                                                                }
                                                                    },'-',
                                                                    {
                                                                        id:'btnDel_'+idRubro,
                                                                        text:'Remover '+titulo,
                                                                        icon:'../images/delete.png',
                                                                        cls:'x-btn-text-icon',
                                                                        hidden:ocultarBotones,
                                                                        handler:function()
                                                                                {
                                                                                    
                                                                                    var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                    var fila=tblGrid.getSelectionModel().getSelections();
                                                                                    var tamano=fila.length;
                                                                                    if(tamano==0)
                                                                                    {
                                                                                        msgBox('Primero debe seleccionar el elemento a eliminar');
                                                                                        return;	
                                                                                    }
                                                                                    
                                                                                    function resp(btn)
                                                                                    {
                                                                                        if(btn=='yes')
                                                                                        {
                                                                                            var cadena='';
                                                                                            var y;
                                                                                            for(y=0;y< tamano;y++)
                                                                                            {
                                                                                                var idFila=fila[y].get('idGridVSCalculo');
                                                                                                if(cadena=='')
                                                                                                    cadena=idFila;
                                                                                                else
                                                                                                    cadena+=','+idFila;    
                                                                                            }
                                                                                            
                                                                                             function funcAjax1()
                                                                                             {
                                                                                                  var resp=peticion_http.responseText;
                                                                                                  arrResp=resp.split('|');
                                                                                                  if(arrResp[0]=='1') 
                                                                                                  {
                                                                                                        
                                                                                                        tblGrid.getStore().remove(fila);
                                                                                                        sumarTotal();	
                                                                                                        refrescarMenuDTD();
                                                                                                  }
                                                                                                  else
                                                                                                  {
                                                                                                      msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                  }
                                                                                             }
                                                                                             obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax1, 'POST','funcion=8&cadena='+cadena,true);
       
                                                                                            
                                                                                            
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer eliminar los conceptos seleccionados?',resp);
                                                                                }
                                                                    }
                                                                ]
                                                        }
                                                    );
        tblGrid.nuevoRegistro=false;  
        tblGrid.montoMaximo=montoMaximo;
        tblGrid.rubro=titulo;
        tblGrid.on('beforeedit',antesEditPresupuesto);
		return tblGrid;
	}
    else
    {
    	var tabla;
        
    	eval('tabla='+funcionConstruccion+'(arregloG,titulo,idRubro,montoMaximo);');
        return tabla;
   	}
}

function antesEditPresupuesto(e)
{
	if(ocultarBotones)
    	e.cancel=true;
}

function funcEditorFilaBeforeEditGridCalculo(rowEdit,fila)
{
	var dRubro=rowEdit.id;
    arrRubro=dRubro.split('_');
    var grid=gEx('gridCalculo_'+arrRubro[1]);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
    var registro=grid.getStore().getAt(fila);
	var cantidadR=registro.get('cantidad');
    cantidadR=parseFloat(cantidadR);
    var costoU=registro.get('costoUnitario');
    costoU=parseFloat(costoU);
    var total=cantidadR*costoU;
    if(ocultarBotones)
    	return false;
    
    
}

function funcEditorValidaGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='gridCalculo_'+datosEditor[1];
    var grid=Ext.getCmp(idGrid);
    if(obj.calculo=='')
    {
    	var columna=grid.getColumnModel().getColumnHeader(2);
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna '+columna+' no puede ser vac&iacute;o')
        return false;
    }
   
    if(obj.costoUnitario==='')
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Costo Unitario no puede ser vac&iacute;o')
        return false;
    }
    
    if(obj.cantidad==='')
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Cantidad no puede ser vac&iacute;o')
        return false;
    }
    
    var montoActual=parseFloat(obj.cantidad)*parseFloat(obj.costoUnitario);
    
    obj.total=montoActual;
    
    var montoRubro=0;
    
    var x;
    
    var fila;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        if(fila.get('idGridVSCalculo')!=registro.get('idGridVSCalculo'))
	        montoRubro+=parseFloat(fila.get('costoUnitario'))*parseFloat(fila.get('cantidad'));

        
   	}
    
    montoRubro+=montoActual;
    
    var mTotal=0;
    var x;
    for(x=0;x<arrRubros.length;x++)
    {
    	if(arrRubros[x]!=parseInt(datosEditor[1]))
        {
        	mTotal+=sumarRubro(arrRubros[x],0,true);
        }
    }
    
	mTotal+=montoRubro;
    var montoPatrocinio=parseFloat(normalizarValor(Ext.util.Format.stripTags(gE('lblMontoPresupuestado').innerHTML)));
    if(mTotal>montoPatrocinio)
    {
    	function respPatrocinio()
        {
        	var copiaRegistro=grid.copiaRegistro;
    
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=registro;
        
            for(x=0;x<arrCampos.items.length;x++)
            {
                filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
            }
            
            rowEdit.startEditing(nFila);
            //grid.getView().refresh();	
        
        }
        msgBox('El monto total del presupuesto (<b>'+Ext.util.Format.usMoney(mTotal)+'</b>) excede el monto disponible del proyecto (<b>'+Ext.util.Format.usMoney(montoPatrocinio)+'</b>)',respPatrocinio);
    	return false;
    }
     
    var cadObj='{"idRegistro":"<?php echo $idRegistro ?>","idFormulario":"<?php echo $idFormulario?>","idMonto":"'+registro.get('idGridVSCalculo')+'","costoUnitario":"'+obj.costoUnitario+
    		'","cantidad":"'+obj.cantidad+'","total":"'+obj.total+'","idRubro":"'+datosEditor[1]+'","concepto":"'+cv(obj.calculo)+'"}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
        	
        	sumarTotal();
            Ext.getCmp('btnDel_'+datosEditor[1]).enable();
        	Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
            registro.set('idGridVSCalculo',arrResp[1])
        	grid.nuevoRegistro=false;
           
           	refrescarMenuDTD();
            grid.getView().refresh();
        }
        else
        {
        	var copiaRegistro=grid.copiaRegistro;
    
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=registro;
        
            for(x=0;x<arrCampos.items.length;x++)
            {
                filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
            }
            grid.getView().refresh();
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=7&cadObj='+cadObj,true);
    return true;
}

function funcEditorCancelEditGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='gridCalculo_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	
	Ext.getCmp('btnDel_'+datosEditor[1]).enable();
    Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }
	grid.nuevoRegistro=false;
    grid.getView().refresh();
	
}

function sumarTotal()
{
	var sumaTotal=0;
    
    <?php
	for($y=0;$y<$tamano;$y++)
    {
       
       $idRubro=$arreglo[$y]["idRubro"];
	   echo '	
	   			sumaTotal+=sumarGrid('.$idRubro.');
				';
    }
    ?>
    
 	var etiqueta=gE('sumaTotalE');
    etiqueta.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(sumaTotal)+'</span></font>';
    var gridPresupuesto=gEx('gridPatrocinio');
    var x;
    var totalPresupuestado=0;
    var fila;
    for(x=0;x<gridPresupuesto.getStore().getCount();x++)
    {
    	fila=gridPresupuesto.getStore().getAt(x);
    	totalPresupuestado+=parseFloat(fila.get('montoDisponible'))
    }
    var et=gE('lblMontoPresupuestado');
    et.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(totalPresupuestado)+'</span></font>';
    et=gE('lblMontoPresupuestadoDist');
    et.innerHTML='<font color=\'#005500\'><span id="spTotalPresupuestadoDist">'+Ext.util.Format.usMoney(totalPresupuestado)+'</span></font>';
    et=gE('lblMontoTotal');
    et.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(totalPresupuestado)+'</span></font>';
    var diferencia=totalPresupuestado-sumaTotal;
    et=gE('lblDiferencia');
    if(diferencia==0)
    	et.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(diferencia)+'</span></font>';
    else
    	et.innerHTML='<font color=\'#FF0000\'><span id="spTotal">'+Ext.util.Format.usMoney(diferencia)+'</span></font>';
   sumarDistribucion();
}

function sumarGrid(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridCalculo_'+idRubro).getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var cUni=elemento.get('costoUnitario');
    	cUni=parseFloat(cUni);
        var cantidad=elemento.get('cantidad');
        cantidad=parseFloat(cantidad);
        var total=cUni*cantidad;
        sumaGrid=sumaGrid+total;
    }

   return sumaGrid;
}

function funcAfterEdit(e)
{
  
   sumarTotal();
}

function sumaPresupuestoTotal()
{

}

function refrescarMenuDTD()
{
	window.parent.mostrarMenuDTD()
}

function sumarRubro(idRubro,monto,contabilizarUltimo)
{
	var grid=gEx('gridCalculo_'+idRubro);
    var x;
    var montoTotal=0;
    var fila;
    for(x=0;x<grid.getStore().getCount()-1;x++)
    {
    	fila=grid.getStore().getAt(x);
        montoTotal+=parseFloat(fila.get('costoUnitario'))*parseFloat(fila.get('cantidad'));
        
   	}
    if(contabilizarUltimo)
    {
    	fila=grid.getStore().getAt(grid.getStore().getCount()-1);
        if(fila)
        {
        	montoTotal+=parseFloat(fila.get('costoUnitario'))*parseFloat(fila.get('cantidad'));
        }
    }
    return montoTotal+monto;
}

function mostrarVentanaAgregarInstitucion(idRegistro)
{
	var arrPeriodicidad=[['1','D\xEDas'],['2','Semanas'],['3','Meses']];
	var cmbInformeTecnico=crearComboExt('cmbInformeTecnico',arrSiNo,160,85,120);
    cmbInformeTecnico.setValue('0');
    cmbInformeTecnico.on('select',function(combo,registro)
    								{
                                    	var periodicidadInfTecnico=gEx('periodicidadInfTecnico');
                                    	if(registro.get('id')=='1')
                                        {
                                            periodicidadInfTecnico.enable();
                                        	periodicidadInfTecnico.focus(true,500);
                                            gEx('cmbPeriodoInfTecnico').enable();
                                        }
                                        else
                                        {
                                        	periodicidadInfTecnico.disable();
                                        	periodicidadInfTecnico.setValue('');
                                            gEx('cmbPeriodoInfTecnico').disable();
                                        }
                                    }
    						)
    var cmbInformeFinanciero=crearComboExt('cmbInformeFinanciero',arrSiNo,160,115,120);
    cmbInformeFinanciero.setValue('0');

    cmbInformeFinanciero.on('select',function(combo,registro)
    								{
                                    	var periodicidadInfFinanciero=gEx('periodicidadInfFinanciero');
                                    	if(registro.get('id')=='1')
                                        {
                                            periodicidadInfFinanciero.enable();
                                        	periodicidadInfFinanciero.focus(true,500);
                                            gEx('cmbPeriodoInfFinanciero').enable();
                                        }
                                        else
                                        {
                                        	periodicidadInfFinanciero.disable();
                                        	periodicidadInfFinanciero.setValue('');
                                            gEx('cmbPeriodoInfFinanciero').disable();
                                        }
                                    }
    						)
    var cmbSituaciones=crearComboExt('cmbSituaciones',arrSituacionesPatrocinio,160,145);
    cmbSituaciones.setValue('1');
    var cmbPeriodoInfTecnico=crearComboExt('cmbPeriodoInfTecnico',arrPeriodicidad,460,85,110);
    cmbPeriodoInfTecnico.disable();
    cmbPeriodoInfTecnico.setValue('1');
    var cmbPeriodoInfFinanciero=crearComboExt('cmbPeriodoInfFinanciero',arrPeriodicidad,460,115,110);
    cmbPeriodoInfFinanciero.disable();
    cmbPeriodoInfFinanciero.setValue('1');
	/*var dsRelaciones= new Ext.data.SimpleStore	(
													{
														fields:	[
																	{name:'codigoUnidad'},
																	{name:'unidad'}
																]
													}
												)

	
	var cmbRel=document.createElement('select');
	
	var parametros2=	{
							funcion:'50',
							criterio:''
						};
	
	var comboPapa=inicializarCmbPadre(parametros2);*/
	var comboPapa=crearComboExt('cmbNombrePadre',arrInstituciones,160,15,450);
    //var gridDristribucionPresupuesto=crearGridDistribucionPresupuesto();


	var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
														 	new Ext.form.Label	(
																				 	{
																						x:5,
																						y:20,
																						text:'Institución patrocinadora: '
																					}
																				)
															,
															comboPapa,
                                                             new Ext.form.Label	(
                                                                                    {
																						x:270,
																						y:45,
                                                                                        hidden:ocultarBotones,
																						html:'<b>¿No se encuentra registrada la instituci&oacute;n patrocinadora en el sistema? Para agregarla de click <a href="javascript:agregarInstitucion2(1)"><font color="#FF0000">AQUI</font></a></b>'
																					}
                                                                                ),
                                                        	  {
                                                              	x:5,
                                                                y:60,
                                                                xtype:'label',
                                                                html:'Monto patrocinado:'
                                                              },
                                                              {
                                                              	x:160,
                                                                y:55,
                                                                width:100,
                                                                id:'txtMontoPatrocinio',
                                                                xtype:'numberfield',
                                                                allowDecimals:true,
                                                                allowNegative:false
                                                              },
                                                              
                                                               {
                                                              	x:5,
                                                                y:90,
                                                                 xtype:'label',
                                                                html:'Observaciones:'
                                                              },
                                                              {
                                                              	x:160,
                                                                y:85,
                                                                id:'txtObservaciones',
                                                                xtype:'textarea',
                                                                width:400,
                                                                height:80
                                                              }	                      
                                                                                 
														]
											}
										);
	
	var ventana = new Ext.Window	(
									{
										title: lblAplicacion,
										width: 820,
										height:270,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										listeners : {
														show : {
																	buffer : 500,
																	fn : function() 
																	{
																		comboPapa.focus();
																	}
																}
													},
										buttons:	[
														{
															text: 'Aceptar',
                                                            hidden:ocultarBotones,
															handler:function()
																	{
																		var codigoUnidad=comboPapa.getValue();
                                                                        if(codigoUnidad=='')
                                                                        {
                                                                        	function respUsr()
                                                                            {
                                                                            	comboPapa.focus();
                                                                            }
                                                                        	msgBox('Debe seleccionar la instituci&oacute;n patrocinadora a agregar',respUsr);
                                                                            return;
                                                                        }
                                                                        
                                                                        var txtMontoPatrocinio=gEx('txtMontoPatrocinio');
                                                                        if(txtMontoPatrocinio.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtMontoPatrocinio.focus();
                                                                               
                                                                            }
                                                                            msgBox('Debe ingresar el monto obtenido por la instituci&oacute;n patrocinadora',resp2)
                                                                            return;
                                                                        }
                                                                        
                                                                       
                                                                        
                                                                        
                                                                        var obj='{"idRegistro":"'+idRegistro+'","codigoInstitucion":"'+codigoUnidad+'","montoPatrocinio":"'+txtMontoPatrocinio.getValue()+'","reqInformeTecnico":"0","periodInfTecnico":"0",'+
                                                                        		'"cadaInfTecnico":"1","reqInformeFinanciero":"0","periodInfFinanciero":"0","cadaInfFinanciero":"1","edoPatrocinio":"1","observaciones":"'+cv(gEx('txtObservaciones').getValue())+'"}';
                                                                        
                                                                        var idFormulario=<?php echo $idFormulario?>;
                                                                        var idReferencia=<?php echo $idRegistro?>;
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText.split('|');
                                                                            if(resp[0]=='1')
                                                                            {
                                                                                gEx('gridPatrocinio').getStore().loadData(eval(resp[1]));
                                                                                sumarTotal();
                                                                                refrescarMenuDTD();
                                                                                ventana.close();
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=51&cadObj='+obj+'&idFormulario='+idFormulario+'&idReferencia='+idReferencia,true)
                                                                    }
                                                                    
														},
														{
															text: 'Cancelar',
                                                            hidden:ocultarBotones,
															handler:function()
																	{
																		ventana.close();
																	}
														},
                                                        {
															text: 'Aceptar',
                                                            hidden:!ocultarBotones,
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);
	if(idRegistro!='-1')
    	obtenerDatosInstitucionPatrocinadora(idRegistro,ventana);                                
    else
		ventana.show();                                

}

function obtenerDatosInstitucionPatrocinadora(idRegistro,ventana)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj=eval('['+arrResp[1]+']')[0];
            var cmbNombrePadre=gEx('cmbNombrePadre');

            cmbNombrePadre.setValue(obj.codigoInstitucion);
            gE('codigoUnidad').value=obj.codigoInstitucion;
            gEx('txtMontoPatrocinio').setValue(obj.montoPatrocinio);
            
            gEx('txtObservaciones').setValue(obj.observaciones);
            
            if(ocultarBotones)
            	desHabilitarControles();
            ventana.show();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=93&idPatrocinador='+idRegistro,true);

}


function desHabilitarControles()
{
	gEx('cmbNombrePadre').disable();
    gEx('txtMontoPatrocinio').setReadOnly(true);
    gEx('cmbInformeTecnico').disable();
    gEx('periodicidadInfTecnico').setReadOnly(true);
    gEx('cmbPeriodoInfTecnico').disable();
    gEx('cmbInformeFinanciero').disable();
    gEx('periodicidadInfFinanciero').setReadOnly(true);
    gEx('cmbPeriodoInfFinanciero').disable();
    gEx('cmbSituaciones').disable();
    gEx('txtObservaciones').setReadOnly(true);
}

function crearGridDistribucionPresupuesto()
{
	var arrDatos=(arrMeses);
    var dSetMeses= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    <?php
																		echo $arrCampos;
																	?>
                                                                ]
                                                    }
                                                 )
    
	dSetMeses.loadData(arrDatos);	
	var cmMeses= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	{
                                                            	header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	var x=0;
                                                                            var total=0;
                                                                            for(x=0;x<<?php echo $ct?>;x++)
                                                                            {
                                                                            	total+=parseFloat(registro.get('mes'+x));
                                                                            }
                                                                            var color="#030";
                                                                            var cantidad=gEx('txtMontoPatrocinio').getValue();
                                                                            if(cantidad>total)
                                                                            {
                                                                            	color='#F60';
                                                                            }
                                                                            else
                                                                            {
                                                                            	if(cantidad<total)
                                                                                	color='#FF0000';
                                                                            }
                                                                            return '<b><font color="'+color+'">'+Ext.util.Format.usMoney(total)+'</font></b>';
                                                                    }
                                                            },
                                                            <?php
																echo $arrColumnas;
															?>
                                                           ,
                                                            {
                                                            	header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                
                                                                renderer:function(val,meta,registro)
                                                            		{
                                                                        	var x=0;
                                                                            var total=0;
                                                                            for(x=0;x<<?php echo $ct?>;x++)
                                                                            {
                                                                            	total+=parseFloat(registro.get('mes'+x));
                                                                            }
                                                                            var color="#030";
                                                                            var cantidad=gEx('txtMontoPatrocinio').getValue();
                                                                            if(cantidad>total)
                                                                            {
                                                                            	color='#F60';
                                                                            }
                                                                            else
                                                                            {
                                                                            	if(cantidad<total)
                                                                                	color='#FF0000';
                                                                            }
                                                                            return '<b><font color="'+color+'">'+Ext.util.Format.usMoney(total)+'</font></b>';
                                                                    }
                                                            }
                                                        ]
                                                    );
	
    if(!ocultarBotones)										
    {
        var editorFila=new Ext.ux.grid.RowEditor	(
                                                        {
                                                            saveText: 'Guardar',
                                                            cancelText:'Cancelar',
                                                            clicksToEdit:1
                                                        }
                                                    );
	
	}    
    var confGrid={
                      title:'Distibuci&oacute;n del presupuesto',
                      x:10,
                      y:260,
                      id:'tblMeses',
                      store:dSetMeses,
                      frame:true,
                      clicksToEdit: 1,
                      cm: cmMeses,
                      height:130,
                      width:750
                  }
    if(!ocultarBotones)              
		confGrid.plugins=[editorFila];                  
                  
	tblMeses=	new Ext.grid.EditorGridPanel	(
                                                    confGrid
    											);
	tblMeses.on('beforeedit',function(e)
    						{
                                if(ocultarBotones)
                                    e.cancel=true;
                          	}

    			)
	return tblMeses;                                                
}

function inicializarCmbPadre(parametros2)
{

	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesContabilidad.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'instuciones',
												totalProperty:'num',
												id:'codigoUnidad'
											},
											[
											 	{name:'codigoUnidad', mapping:'codigoUnidad'},
												{name:'unidad', mapping:'unidad'},
											]
										);

	var ds=new Ext.data.Store	(
								 	{
										proxy:pPagina,
										reader:lector,
										baseParams:parametros2
									}
								 );
	
	function cargarDatos(dSet)
	{
		gE('codigoUnidad').value='';
		var aNombre=Ext.getCmp('cmbNombrePadre').getValue();
		dSet.baseParams.criterio=aNombre;
        dSet.baseParams.idFormulario=<?php echo $idFormulario?>;
        dSet.baseParams.idReferencia=<?php echo $idRegistro?>;
	}
	
	ds.on('beforeload',cargarDatos);

	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{unidad}&nbsp;<br>---<br>',
										'</div></tpl>'
									 );
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:160,
														y:15,
														id:'cmbNombrePadre',
														store:ds,
														displayField:'unidad',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:570,
                                                        listWidth :550,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item'
														
													}
												 );
	
    function funcElemSeleccionado(combo,registro)
	{	
		var codigoUnidad=registro.get('codigoUnidad');
		gE('codigoUnidad').value=codigoUnidad;
    }
	comboNombre.on('select',funcElemSeleccionado);	
	return comboNombre;
}


function agregarInstitucion2(tipoUnidad,accion)
{
	var idOrganigrama="-1";
	var arrCentrosC=<?php echo $arrCentroC?>;
	var arrPaises=<?php echo $arrPaises?>;
    var arrUnidades=[['1','Departamento'],['2','Instituci\u00F3n']];
	var cmbPais=crearComboExt('cmbPais',arrPaises,110,130);
    cmbPais.setWidth(220);
    cmbPais.minListWidth=220;
    cmbPais.setValue('146');
    cmbPais.on('select',function(cmb,registro)
    					{
                        	if(registro.get('id')==146)
                            {
                            	gEx('cmbEstado').show();
                                gEx('txtEstado').setValue('');
                                gEx('txtEstado').hide();
                                gEx('cmbMunicipio').show();
                                gEx('txtCiudad').hide();
                            }
                            else
                            {
                            	gEx('cmbEstado').hide();
                                gEx('cmbEstado').reset();
                                gEx('txtEstado').show();
                                gEx('cmbMunicipio').hide();
                                gEx('txtCiudad').show();
                            }
                        }
    				)
    var cmbEstado=crearComboExt('cmbEstado',arrEstados,110,160);
    cmbEstado.on('select',	function(combo,registro,indice,obj)
    							{
                                    function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                        	cmbMunicipio.getStore().loadData(eval(arrResp[1]));
                                            cmbMunicipio.setValue('');
                                            if(obj!=undefined)
                                            {
                                            	cmbMunicipio.setValue(obj.municipio);
                                                pos=obtenerPosFila(gEx('cmbMunicipio').getStore(),'id',obj.municipio);
                                                if(pos!='-1')
                                                {
                                                    var registro=gEx('cmbMunicipio').getStore().getAt(pos);
                                                    gEx('cmbMunicipio').fireEvent('select',gEx('cmbMunicipio'),registro,pos,obj);
                                                }
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax, 'POST','funcion=60&accion=1&codigo='+registro.get('id'),true);
                                }
    				)
    var cmbMunicipio=crearComboExt('cmbMunicipio',[],110,190,160);
	var cmbCentroCosto=crearComboExt('cmbCentroCosto',arrCentrosC,150,315,260);
    var controlTelefono='<table  border="0" cellspacing="1" cellpadding="1">'+
                        '<tr><td  >&nbsp;<select name="cmbTelefonoInst" id="cmbTelefonoInst" size="5" style="width:240px"></select><input type="hidden" name="telefonos" id="telefonos" value="" />'+
                        '</td><td width="5"  align="left">&nbsp;</td><td width="19"><table><tr><td>'+
                        '<a href="javaScript:solicitarTel(\'cmbTelefonoInst\')"><img src="../images/icon_big_tick.gif" alt="Agregar" height="15" title="Agregar Teléfono" border="0"/></a>'+
                        '</td></tr><tr><td>'+
                        '<a href="javaScript:eliminarTelefono(\'cmbTelefonoInst\')"><img src="../images/cancel_round.png" alt="Eliminar" title="Eliminar Teléfono" border="0"/></a>'+
                        '</td></tr></table><br /></td></tr></table>';
                        
	var controlTelefonoD='<table  border="0" cellspacing="1" cellpadding="1">'+
                        '<tr><td  >&nbsp;<select name="cmbTelefonoDepto" id="cmbTelefonoDepto" size="5" style="width:240px"></select><input type="hidden" name="telefonos" id="telefonos" value="" />'+
                        '</td><td width="5"  align="left">&nbsp;</td><td width="19"><table><tr><td>'+
                        '<a href="javaScript:solicitarTel(\'cmbTelefonoDepto\')"><img src="../images/icon_big_tick.gif" alt="Agregar" height="15" title="Agregar Teléfono" border="0"/></a>'+
                        '</td></tr><tr><td>'+
                        '<a href="javaScript:eliminarTelefono(\'cmbTelefonoDepto\')"><img src="../images/cancel_round.png" alt="Eliminar" title="Eliminar Teléfono" border="0"/></a>'+
                        '</td></tr></table><br /></td></tr></table>';                        
    
    var txtCod;
    var codigoPadre='';
    var longCod=4;
    var ancho=80;
    
    
    var panelInst=new Ext.Panel(
    								{
                                    	id:'panelInst',
                                    	x:10,
                                        y:10,
                                        baseCls: 'x-plain',
										layout:'absolute',
                                        width:585,
                                        height:380,
                                        hidden:true,
                                    	items:[
                                        		
                                                  {
                                                      x:10,
                                                      y:10,
                                                      baseCls: 'x-plain',
                                                      html:'Instituci&oacute;n:<font color="red">*</font>'
                                                  },
                                                  {
                                                      x:110,
                                                      y:5,
                                                      id:'txtInstitucionNueva',
                                                      xtype:'textfield',
                                                      width:350
                                                  },
                                                  {
	                                               	  x:10,
                                                      y:40,
                                                      baseCls: 'x-plain',
                                                      html:'Descripci&oacute;n:'
                                                  },
                                                  {
                                                  	x:110,
                                                    y:35,
                                                    xtype:'textarea',
                                                    width:350,
                                                    height:80,
                                                    id:'txtDescripcion'
                                                  },
                                                  {
                                                      x:10,
                                                      y:135,
                                                      baseCls: 'x-plain',
                                                      html:'Pa&iacute;s:<font color="red">*</font>'
                                                  }
                                                  ,
                                                  cmbPais,
                                                  {
                                                      x:10,
                                                      y:165,
                                                      baseCls: 'x-plain',
                                                      html:'Estado:<font color="red">*</font>'
                                                  },
                                                  cmbEstado,
                                                  {
                                                      x:110,
                                                      y:160,
                                                      hidden:true,
                                                      id:'txtEstado',
                                                      xtype:'textfield',
                                                      width:200
                                                  },
                                                  {
                                                      x:10,
                                                      y:195,
                                                      baseCls: 'x-plain',
                                                      html:'Ciudad:'
                                                  },
                                                  cmbMunicipio,
                                                  {
                                                      x:110,
                                                      y:190,
                                                      id:'txtCiudad',
                                                      hidden:true,
                                                      xtype:'textfield',
                                                      width:200
                                                  },
                                                  
                                                  {
                                                  	x:10,
                                                    y:225,
                                                    baseCls: 'x-plain',
                                                    html:'Tel&eacute;fono:'
                                                  },
                                                  {
                                                  	x:110,
                                                    y:220,
                                                    baseCls: 'x-plain',
                                                    html:controlTelefono
                                                  }
                                              ]
                                    }
                                )
    
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
                                            defaultType:'label',
											items:	[
                                            			
                                            			panelInst
                                                        
                                                   ]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar Instituci&oacute;n patrocinadora',
										width:570,
										height:420,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,
                                                    fn:function()
														{
															
														}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	if(accion!=undefined)
                                                                {
                                                                	idOrganigrama=nodoSel.id;
                                                                }
                                                            	
                                                                var codUnidad='';//Ext.getCmp(txtCod).getValue();
                                                                var telefonos=recoletarValoresCombo('cmbTelefonoInst');
                                                                var txtInstitucion=Ext.getCmp('txtInstitucionNueva');
                                                                var txtCp=Ext.getCmp('txtCp');
                                                                var txtCiudad=Ext.getCmp('txtCiudad');
                                                                var txtEstado=Ext.getCmp('txtEstado');
                                                                if(txtInstitucion.getValue()=='')
                                                                {
                                                                    function resp()
                                                                    {
                                                                        txtInstitucion.focus();
                                                                    }
                                                                    msgBox("El campo de instituci&oacute;n es obligatorio",resp);
                                                                    return;
                                                                }
                                                                
                                                               
                                                                
                                                               /* if(txtCiudad.getValue()=='')
                                                                {
                                                                    function resp()
                                                                    {
                                                                        txtCiudad.focus();
                                                                    }
                                                                    msgBox("El campo de ciudad es obligatorio",resp);
                                                                    return;
                                                                }*/
                                                                
                                                                var estado='';
                                                                if(cmbPais.getValue()=='146')
	                                                                estado=cmbEstado.getValue();
                                                                else
                                                                	estado=txtEstado.getValue();
                                                                
                                                                if(estado=='')
                                                                {
                                                                    function resp()
                                                                    {
                                                                    	if(cmbPais.getValue()=='146')
	                                                                        cmbEstado.focus();
                                                                        else
                                                                        	txtEstado.focus();
                                                                    }
                                                                    msgBox("El campo de estado es obligatorio",resp);
                                                                    return;
                                                                }
                                                                var descripcion=Ext.getCmp('txtDescripcion').getValue();
                                                                var ciudad=cv(txtCiudad.getValue());
                                                                if(cmbPais.getValue()=='146')
                                                                {
                                                                	ciudad=cmbMunicipio.getValue();
                                                                    
                                                                }
                                                                var objIns='{"ciudad":"'+ciudad+'","estado":"'+cv(estado)+'","idPais":"'+cmbPais.getValue()+'","cp":"0"}';
                                                                var objParam='{"institucionPatrocinadora":"1","idOrganigrama":"'+idOrganigrama+'","codUnidad":"'+codUnidad+'","codigoUPadre":"","nombre":"'+cv(txtInstitucion.getValue())+'","descripcion":"'+descripcion+'","institucion":"1","objInst":'+objIns+',"telefonos":"'+telefonos+'"}';
                                                                guardarInstitucion(objParam,ventana);    
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
																
															}
													}
												 ]
									}
							   )
	
    Ext.getCmp('panelInst').show();    


	                            
                               
	if(accion!=undefined)
    	llenarDatosUnidad(ventana,nodoSel);
    else
    {                               
		ventana.show();   
                 
    }
}

function guardarInstitucion(objInst,ventana)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var r=new regCombo({id:arrResp[1],nombre:arrResp[2],valorComp:''});
            
        	gEx('cmbNombrePadre').getStore().add(r);
            gEx('cmbNombrePadre').setValue(arrResp[1]);
            
            ventana.close();
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=52&param='+objInst,true);
}

var tipoTel;

function solicitarTel(idCombo)
{
	tipoTel='0';
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
                                                    	new Ext.form.Label(
																		   		{
																					x:5,
																					y:20,
																					html: 'C&oacute;digo de &aacute;rea:'
																				}
																		   ),
                                                       	
															{
																x:100,
                                                                y:15,
                                                                width:40,
                                                                height:20,
                                                                id:'txtCodArea',
                                                                allowDecimals:false,
                                                                allowNegative:false,
                                                                value:52,
                                                                maxLength:3,
                                                                xtype:'textfield',
                                                                maskRe:/^[0-9]$/
															}
														,   
                                                    
													 	new Ext.form.Label(
																		   		{
																					x:5,
																					y:43,
																					html: 'Lada/Tel&eacute;fono:'
																				}
																		   ),
                                                        
                                                       {
                                                                x:100,
                                                                y:38,
                                                                width:40,
                                                                height:20,
                                                                id:'txtLada',
                                                                maxLengthText:'La lada debe contener solo 3 n&uacute;meros',
                                                                maxLength:3,
                                                                allowDecimals:false,
                                                                allowNegative:false,
                                                                xtype:'textfield',
                                                                maskRe:/^[0-9]$/
															}
														,     
                                                        
                                                       
															{
																x:150,
                                                                y:38,
                                                                width:100,
                                                                height:20,
                                                                id:'txtTelefono',
                                                                allowDecimals:false,
                                                                allowNegative:false,
                                                                xtype:'textfield',
                                                                 maskRe:/^[0-9]$/
															}
														,                   
                                                                           
                                                                      
                                                                           
														new Ext.form.Label(
																		   		{
																					x:5,
																					y:66,
																					html: 'Extensi&oacute;n:'
																				}
																		   ),
                                                      	
														
															{
                                                                x:100,
                                                                y:61,
                                                                width:100,
                                                                height:20,
                                                                id:'txtExtensiones',
                                                                allowDecimals:false,
                                                                allowNegative:false,
                                                                xtype:'textfield',
                                                                maskRe:/^[0-9]$/
															}
														,
														
														new Ext.form.Label(
																		   		{
																					x:5,
																					y:90,
																					text: 'Tipo:'
																				}
																		   ),
														new Ext.form.Radio
														(
															{
                                                                x:100,
                                                                y:85,
                                                                checked:true,
                                                                boxLabel:'Tel.',
                                                                allowBlank :true,
                                                                value:'0',
                                                                id:'Tel'
															}
														),
														
														new Ext.form.Radio
														(
															{
                                                                x:150,
                                                                y:85,
                                                                checked:false,
                                                                boxLabel:'Fax',
                                                                allowBlank :true,
                                                                value:'2',
                                                                id:'Fax'
															}
														)
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar N&uacute;mero Telef&oacute;nico',
										width:300,
										height:190,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																Ext.getCmp('txtLada').focus(false,1000);
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var codArea=Ext.getCmp('txtCodArea').getValue();
																var tel=Ext.getCmp('txtTelefono').getValue();
																var exten=Ext.getCmp('txtExtensiones').getValue();
																var lada=Ext.getCmp('txtLada').getValue();
                                                                if(codArea.length==0)
                                                                {
                                                                	function enfocarCodArea()
                                                                    {
                                                                    	Ext.getCmp('txtCodArea').focus();
                                                                    }
                                                                	msgBox('El c&oacute;digo de &aacute;rea ingresado no es v&aacute;lido',enfocarCodArea);
                                                                    return;
                                                                }
                                                                
                                                                if(tel.length==0)
                                                                {
                                                                	function enfocarTelefono()
                                                                    {
                                                                    	Ext.getCmp('txtTelefono').focus();
                                                                    }
                                                                	msgBox('El n&uacute;mero de tel&eacute;fono ingresado no es v&aacute;lido',enfocarTelefono);
                                                                    return;
                                                                }
                                                                
                                                                if(lada.length==0)
                                                                {
                                                                	function enfocarLada()
                                                                    {
                                                                    	Ext.getCmp('txtLada').focus();
                                                                    }
                                                                	msgBox('El n&uacute;mero Lada ingresado no es v&aacute;lido',enfocarLada);
                                                                    return;
                                                                }
                                                                
                                                                var opcion;
                                                                
                                                                opcion=cE('option');
                                                                opcion.value=tipoTel+'_'+codArea+'_'+lada+'_'+tel+'_'+exten;
                                                                var extens='Ext.: '+exten;
                                                                
                                                                switch(tipoTel)
                                                                {
                                                                    case "0":
                                                                        tipoTel='Tel\u00E9fono';
                                                                    break;
                                                                    /*case "1":
                                                                        tipoTel='Celular';
                                                                    break;*/
                                                                    case "2":
                                                                        tipoTel='Fax';
                                                                    break;
                                                                }
                                                                
                                                                if (exten!="")
                                                                    opcion.text='['+tipoTel+'] ('+codArea+') '+lada+"-"+tel + " ("+extens+")";
                                                                else
                                                                    opcion.text='['+tipoTel+'] ('+codArea+') '+lada+"-"+tel+" ()";

                                                               var cmbTelefono=gE(idCombo);
                                                               var resp=existeValor(cmbTelefono,opcion.value);
                                                               if(resp==-1)
                                                               {
                                                               		cmbTelefono.options[cmbTelefono.options.length]=opcion;
                                                               }
                                                               ventana.close();
	
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
																
															}
													}
												 ]
									}
							   )
	
	var tel=Ext.getCmp('Tel');
	var fax=Ext.getCmp('Fax');
	//var movil=Ext.getCmp('Movil');
	tel.on('check',radioCheck);
	fax.on('check',radioCheck);
	//movil.on('check',radioCheck);
	ventana.show();
	
}

function radioCheck(chk,valor)
{
	if(valor==true)
	{
		var tel=Ext.getCmp('Tel');
		var fax=Ext.getCmp('Fax');
		//var movil=Ext.getCmp('Movil');
		tipoTel=chk.value;
		if(tel.id!=chk.id)
			tel.setValue(false);
		if(fax.id!=chk.id)
			fax.setValue(false);
		/*if(movil.id!=chk.id)
			movil.setValue(false);*/
	}
	
}

function eliminarTelefono(idCombo)
{
	var cmbTelefono;
	cmbTelefono=gE(idCombo);
	if(cmbTelefono.selectedIndex==-1)
	{
		msgBox('Debe seleccionar el n&uacute;mero telef&oacute;nico a eliminar');
		return;
	}
	function resp(btn)
	{
		if(btn=='yes')
		{
			cmbTelefono.options[cmbTelefono.selectedIndex]=null;
		}
	}
	Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer eliminar el n&uacute;mero telef&oacute;nico seleccionado?',resp);
	
}

function comboPreguntaChange(combo,tipo)
{
	var valor=combo.options[combo.selectedIndex].value;
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            if(combo.options[0].value=='-1')
            {
            	combo.remove(0);
            }
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=15&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&valor='+valor+'&tipo='+tipo,true);
}

function guardarPreguntas()
{
	var proyectoUSA;
    var cmbUSA=gE('cmbFinanciadoUSA');
    proyectoUSA=cmbUSA.options[cmbUSA.selectedIndex].value;
    var proyectoDFA;
    var cmbFDA=gE('cmbFinanciadoFDA');
    proyectoDFA=cmbFDA.options[cmbFDA.selectedIndex].value;
    
    if(proyectoUSA=='-1')
    {
    	msgBox('Debe indicar si su proyecto es financiado por el Gobierno de Estados Unidos');
    	return;
    }
    
    if(proyectoDFA=='-1')
    {
    	msgBox('Debe indicar si su proyecto  se encuentra bajo regulación de la FDA');
    	return;
    }
    
     function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	refrescarMenuDTD();
            recargarPagina();
       }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=16&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&proyectoUSA='+proyectoUSA+'&proyectoDFA='+proyectoDFA,true);
    
}

function verSituacionPresupuestal()
{
	var arrDimensiones='{"dimension":"IDProyecto","valor":"'+gE('idRegistro').value+'"}';
	var cadObjParam='{"idPerfilPresupuestal":"2","arrDimensiones":['+arrDimensiones+']}';
	var obj={};
    obj.titulo='Situaci&oacute;n presupuestal';
    obj.ancho='100%';
    obj.alto='100%';
    obj.url='../presupuesto/visorPresupuestal.php';
    obj.params=[['cPagina','sFrm=true'],['cadObjParam',bE(cadObjParam)]]
    window.parent.abrirVentanaFancy(obj);
    
}
