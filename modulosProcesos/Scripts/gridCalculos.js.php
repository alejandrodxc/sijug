<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	
	
	$consulta="SELECT categorias,txtPresupuestoCensida FROM _293_tablaDinamica WHERE id__293_tablaDinamica=".$idRegistro;
	$filaPresupuesto=$con->obtenerPrimeraFila($consulta);
	
	$idCategoria=$filaPresupuesto[0];
	$monto=$filaPresupuesto[1];
	
	$consulta="SELECT porcentajeHonorarios FROM _312_montosCategoria WHERE categoria=".$idCategoria;
	$porcentaje=$con->obtenerValor($consulta);
	$montoHono=($monto * $porcentaje)/100;
	$arreglo[0]["idRubro"]="0";
	$arreglo[0]["tituloPanel"]="Honorarios";
	$arreglo[0]["tituloConcepto"]="Honorarios";
	$arreglo[0]["arreglo"]="";
	$arreglo[0]["montoMaximo"]=$montoHono;

	$arreglo[1]["idRubro"]="1";
	$arreglo[1]["tituloPanel"]="Material e impresiones";
	$arreglo[1]["tituloConcepto"]="Material e impresiones";
	$arreglo[1]["arreglo"]="";
	$arreglo[1]["montoMaximo"]="-1";

	$arreglo[2]["idRubro"]="2";
	$arreglo[2]["tituloPanel"]="Equipo de cómputo, de proyección, multimedia";
	$arreglo[2]["tituloConcepto"]="Equipo";
	$arreglo[2]["arreglo"]="";
	$arreglo[2]["montoMaximo"]="-1";
	
	$arreglo[3]["idRubro"]="3";
	$arreglo[3]["tituloPanel"]="Vi&aacute;ticos";
	$arreglo[3]["tituloConcepto"]="Vi&aacute;ticos";
	$arreglo[3]["arreglo"]="";
	$arreglo[3]["montoMaximo"]="-1";
	
	$arreglo[4]["idRubro"]="4";
	$arreglo[4]["tituloPanel"]="Otros";
	$arreglo[4]["tituloConcepto"]="Concepto";
	$arreglo[4]["arreglo"]="";
	$arreglo[4]["montoMaximo"]="-1";
	
	$tamano=sizeof($arreglo);
	for($x=0;$x<$tamano;$x++)
	{
		$consulta="SELECT idGridVSCalculo,calculo,costoUnitario,cantidad,total,idRubro,montoAutorizado  FROM 100_calculosGrid WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro." AND idRubro=".$arreglo[$x]["idRubro"]." and eliminado=0 order by calculo";
		$storeA=$con->obtenerFilasArreglo($consulta);
		//echo $consulta;
		$arreglo[$x]["arreglo"]=$storeA;
	}

?>

Ext.onReady(inicializar);

function inicializar()
{
	
	var mascara=new Mask('$#,###.00','number');
    mascara.attach(gE('txtRecursoOrganizacion'));
    mascara.attach(gE('txtRecursoOtrosDonantes'));
    gE('txtRecursoOrganizacion').value=Ext.util.Format.usMoney(gE('txtRecursoOrganizacion').value);
    gE('txtRecursoOtrosDonantes').value=Ext.util.Format.usMoney(gE('txtRecursoOtrosDonantes').value);
    gE('mTotal').innerHTML='&nbsp;/ <b>$ <?php echo number_format($monto,2,".",",")?></b>';
    mostrarTab();
    sumarTotal();
}

function  mostrarTab()
{
    var arregloTabs=[];
    var panel;
    var grid;
    <?php

	for($z=0;$z<$tamano;$z++)
    {
       
       $titulo=$arreglo[$z]["tituloPanel"];
       $storeGrid=$arreglo[$z]["arreglo"];
       $idRubro=$arreglo[$z]["idRubro"];
	   $tituloConcepto=$arreglo[$z]["tituloConcepto"];
	   $montoMaximo=$arreglo[$z]["montoMaximo"];
	   echo '	
	   			grid=	gridGenerico('.$storeGrid.',"'.$tituloConcepto.'",'.$idRubro.','.$montoMaximo.');
	   			panel=		{
								xtype:"panel",
								title:"'.$titulo.'",
								items:[grid]
							};
				arregloTabs.push(panel);
				
				';
	   
    }
    ?>
    var tabs = new Ext.TabPanel	(
									{
										renderTo: 'grids',
										activeTab: 0,
										width:850,
										height:370,
										items:	arregloTabs
									}
								);
    
}


function gridGenerico(arregloG,titulo,idRubro,montoMaximo)
{
	var arrDatos=arregloG;
    var iR=idRubro;
    var dSetGenerico= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idGridVSCalculo'},
                                                                    {name:'calculo'},
                                                                    {name:'costoUnitario'},
                                                                    {name:'cantidad'},
                                                                    {name: 'total'},
                                                                    {name:'idRubro'},
                                                                    {name: 'montoAutorizado'}
                                                                ]
                                                    }
                                                 )
    
	dSetGenerico.loadData(arrDatos);	
	var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editor_'+idRubro,
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
    editorFila.on('beforeedit',funcEditorFilaBeforeEditGridCalculo)
    editorFila.on('validateedit',funcEditorValidaGridCalculo);
    editorFila.on('canceledit',funcEditorCancelEditGridCalculo);
	var cmGenerico= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            columnaCheck,
                                                            {
                                                                header:titulo,
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'calculo',
                                                                editor:{xtype:'textfield'}
                                                            },
                                                            {
                                                                header:'Costo Unitario',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'costoUnitario',
                                                                editor:{xtype:'numberfield'},
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
                                                                editor:{xtype:'numberfield'},
                                                                renderer:function(val)
                                                                		{
                                                                        	return Ext.util.Format.number(val,'0,0.00');
                                                                        }
                                                            },
                                                            {
                                                                header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                
                                                                renderer: function(val,meta,registro)
                                                                			{
                                                                  				var cantidadR=registro.get('cantidad');
                                                                                cantidadR=parseFloat(cantidadR);
                                                                                var costoU=registro.get('costoUnitario');
                                                                            	costoU=parseFloat(costoU);
                                                                                var total=cantidadR*costoU;
                                                                                return Ext.util.Format.usMoney(total);
                                                                            }
                                                               
                                                            },
                                                             {
                                                                header:'Monto autorizado',
                                                                width:110,
                                                                sortable:true,
                                                                hidden:false,
                                                                dataIndex:'montoAutorizado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var cantidadR=registro.get('cantidad');
                                                                            cantidadR=parseFloat(cantidadR);
                                                                            var costoU=registro.get('costoUnitario');
                                                                            costoU=parseFloat(costoU);
                                                                            var total=cantidadR*costoU;
                                                                        	if(parseFloat(val)==total)
                                                                            {
                                                                            	return '<font color="#005500">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                            }
                                                                            else
                                                                            	return '<font color="#B0281A">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                        }
                                                            }
                                                        ]
                                                    );
											
												
	tblGrid=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	id:'gridCalculo_'+idRubro,
                                                        store:dSetGenerico,
                                                        frame:true,
                                                        cm: cmGenerico,
                                                        sm:columnaCheck,
                                                        height:350,
                                                        width:850,
                                                        plugins:[editorFila],
                                                        tbar:[
                                                              	{
                                                                    id:'btnAdd_'+idRubro,
                                                                    text:'Agregar '+titulo,
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    hidden:true,
                                                                    handler:function()
                                                                            {
                                                                                
                                                                                var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                
                                                                                var editorFila=gEx('editor_'+idRubro);
                                                                                var registroGrid=crearRegistro([
                                                                                                                {name:'idGridVSCalculo'},
                                                                                                                {name:'calculo'},
                                                                                                                {name:'costoUnitario'},
                                                                                                                {name:'cantidad'},
                                                                                                                {name:'total'},
                                                                                                                {name:'idRubro'},
                                                                                                                {name: 'montoAutorizado'}
                                                                                                            ]);
                                                                               
                                                                                
                                                                                var nReg=new registroGrid	(
                                                                                                                {
                                                                                                                	idGridVSCalculo:'-1',
                                                                                                                    calculo:'',
                                                                                                                    costoUnitario:'',
                                                                                                                    cantidad:'',
                                                                                                                    total:'0',
                                                                                                                    idRubro:iR,
                                                                                                                    montoAutorizado:'0'
                                                                                                                }
                                                                                                            )
                                                                                
                                                                                editorFila.stopEditing();
                                                                                tblGrid.getStore().add(nReg);
                                                                                tblGrid.nuevoRegistro=true;
                                                                                editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                Ext.getCmp('btnAdd_'+idRubro).disable();
                                                                                Ext.getCmp('btnDel_'+idRubro).disable();																	
                                                                            }
                                                                },
                                                                {
                                                                    id:'btnDel_'+idRubro,
                                                                    text:'Remover '+titulo,
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    hidden:false,
                                                                    hidden:true,
                                                                    handler:function()
                                                                            {
                                                                                
                                                                                var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                var fila=tblGrid.getSelectionModel().getSelections();
                                                                                var tamano=fila.length;
                                                                                if(tamano==0)
                                                                                {
                                                                                    msgBox('Primero debe seleccionar el elemento a eliminar');
                                                                                    return;	
                                                                                }
                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                    if(btn=='yes')
                                                                                    {
                                                                                        var cadena='';
                                                                                        var y;
                                                                                        for(y=0;y< tamano;y++)
                                                                                        {
                                                                                        	var idFila=fila[y].get('idGridVSCalculo');
                                                                                            if(cadena=='')
                                                                                            	cadena=idFila;
                                                                                            else
                                                                                            	cadena+=','+idFila;    
                                                                                        }
                                                                                        
                                                                                         function funcAjax1()
                                                                                         {
                                                                                              var resp=peticion_http.responseText;
                                                                                              arrResp=resp.split('|');
                                                                                              if(arrResp[0]=='1') 
                                                                                              {
                                                                                              		sumarTotal();
                                                                                                	tblGrid.getStore().remove(fila);	
                                                                                                	refrescarMenuDTD();
                                                                                              }
                                                                                              else
                                                                                              {
                                                                                                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                              }
                                                                                         }
                                                                                         obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax1, 'POST','funcion=2&cadena='+cadena,true);
   
                                                                                        
                                                                                        
                                                                                    }
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer eliminar el elemento seleccionados?',resp);
                                                                            }
                                                                }
                                                        	]
													}
    											);
	tblGrid.nuevoRegistro=false;  
    tblGrid.montoMaximo=montoMaximo;
    tblGrid.rubro=titulo;
    tblGrid.on('beforeedit',antesEditPresupuesto);
	return tblGrid;
}

function antesEditPresupuesto(e)
{
	/*var registro=e.record;
	var cantidadR=registro.get('cantidad');
    cantidadR=parseFloat(cantidadR);
    var costoU=registro.get('costoUnitario');
    costoU=parseFloat(costoU);
    var total=cantidadR*costoU;
    if(parseFloat(registro.get('montoAutorizado'))==total)
    	e.cancel=true;*/
}

function funcEditorFilaBeforeEditGridCalculo(rowEdit,fila)
{
	var dRubro=rowEdit.id;
    arrRubro=dRubro.split('_');
    var grid=gEx('gridCalculo_'+arrRubro[1]);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
    
    
    var registro=grid.getStore().getAt(fila);
	var cantidadR=registro.get('cantidad');
    cantidadR=parseFloat(cantidadR);
    var costoU=registro.get('costoUnitario');
    costoU=parseFloat(costoU);
    var total=cantidadR*costoU;
    
    /*grid.getColumnModel().setEditor(3,null);
    grid.getColumnModel().setEditor(4,null);
    
    if(parseFloat(registro.get('montoAutorizado'))!=total)
    {
    	grid.getColumnModel().setEditor(3,{xtype:'numberfield'});
	    grid.getColumnModel().setEditor(4,{xtype:'numberfield'});
    }*/
}


function funcEditorValidaGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='gridCalculo_'+datosEditor[1];
    var grid=Ext.getCmp(idGrid);
    if(obj.calculo=='')
    {
    	var columna=grid.getColumnModel().getColumnHeader(2);
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna '+columna+' no puede ser vacio')
        return false;
    }
   
    if(obj.costoUnitario==='')
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Costo Unitario no puede ser vacio')
        return false;
    }
    
    if(obj.cantidad==='')
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Cantidad no puede ser vacio')
        return false;
    }
    
    var montoActual=parseFloat(obj.cantidad)*parseFloat(obj.costoUnitario);
    var montoRubro=sumarRubro(datosEditor[1],montoActual);
    
    /*if(grid.montoMaximo>-1)
    {
    	if(montoRubro>grid.montoMaximo)
        {
        	msgBox('No se puede agregar el concepto debido a que el monto total del rubro '+grid.rubro+' ( <b>'+Ext.util.Format.usMoney(montoRubro)+'</b>) excede el monto m&aacute;ximo permitido para la catagor&iacute;a del proyecto (<b>'+Ext.util.Format.usMoney(grid.montoMaximo)+'</b>)');
        	return false;
        }
    }*/
    var montoSolicitadoCensida=parseFloat(normalizarValor(gE('mTotal').innerHTML));
    var montoTotalRubro=parseFloat(normalizarValor(gE('spTotal').innerHTML));
    if(parseFloat(registro.get('idGridVSCalculo'))==-1)
    	montoTotalRubro+=montoActual;
	/*if(montoTotalRubro>montoSolicitadoCensida)
    {
        msgBox('No se puede agregar el concepto debido a que el monto total ( <b>'+Ext.util.Format.usMoney(montoTotalRubro)+'</b>) excede el monto solicitado a CENSIDA (<b>'+Ext.util.Format.usMoney(montoSolicitadoCensida)+'</b>)');
        return false;
    }*/
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
        	sumarTotal();
            Ext.getCmp('btnDel_'+datosEditor[1]).enable();
        	Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
        	grid.nuevoRegistro=false;
           	refrescarMenuDTD();
        }
        else
        {
        	var copiaRegistro=grid.copiaRegistro;
    
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=grid.registroEdit;
        
            for(x=0;x<arrCampos.items.length;x++)
            {
                filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
        
            }
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=1&idRegistro='+<?php echo $idRegistro ?>+'&idFormulario='+<?php echo $idFormulario?>+'&id='+registro.get('idGridVSCalculo')+'&costoUnitario='+obj.costoUnitario+'&cantidad='+obj.cantidad+'&total='+obj.total+'&idRubro='+datosEditor[1]+'&calculo='+obj.calculo,true);
   
    return true;
}

function funcEditorCancelEditGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='gridCalculo_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	
	Ext.getCmp('btnDel_'+datosEditor[1]).enable();
    Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }
	grid.nuevoRegistro=false;
	
}

function sumarTotal()
{
	var sumaTotal=0;
    
    <?php
	for($y=0;$y<$tamano;$y++)
    {
       
       $idRubro=$arreglo[$y]["idRubro"];
	   echo '	
	   			sumaTotal+=sumarGrid('.$idRubro.');
				';
    }
    ?>
    
 	var etiqueta=gE('sumaTotalE');
    etiqueta.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(sumaTotal)+'</span></font>';
    sumaPresupuestoTotal();
}

function sumarGrid(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridCalculo_'+idRubro).getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var cUni=elemento.get('costoUnitario');
    	cUni=parseFloat(cUni);
        var cantidad=elemento.get('cantidad');
        cantidad=parseFloat(cantidad);
        var total=cUni*cantidad;
        sumaGrid=sumaGrid+total;
    }
   return sumaGrid;
}

function funcAfterEdit(e)
{
  
   sumarTotal();
}

function sumaPresupuestoTotal()
{

	var sumaTotalE=parseFloat(normalizarValor(gE('spTotal').innerHTML));
    var recursoOrganizacion=normalizarValor(gE('txtRecursoOrganizacion').value);
    if(recursoOrganizacion=='')
    	recursoOrganizacion=0;
    else
    	recursoOrganizacion=parseFloat(recursoOrganizacion);
    var recursoOtros=normalizarValor(gE('txtRecursoOtrosDonantes').value);
    if(recursoOtros=='')
    	recursoOtros=0;
    else
    	recursoOtros=parseFloat(recursoOtros);
	var sumaTotalPresupuesto=gE('sumaTotalPresupuesto');
    var total=sumaTotalE+recursoOrganizacion+recursoOtros;
    sumaTotalPresupuesto.innerHTML=Ext.util.Format.usMoney(total);
}

function guardarMonto(tMonto,ctrl)
{
	if(ctrl.value=='')
    	ctrl.value='0';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	sumaPresupuestoTotal();
            refrescarMenuDTD();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=3&idFormulario='+gE('idFormulario').value+'&idReferencia='+gE('idReferencia').value+'&monto='+normalizarValor(ctrl.value)+'&tMonto='+tMonto,true);
}

function refrescarMenuDTD()
{
	if(typeof(funcAgregar)!='undefined')
	   	funcAgregar();
}

function sumarRubro(idRubro,monto)
{
	var grid=gEx('gridCalculo_'+idRubro);
    var x;
    var montoTotal=0;
    var fila;
    for(x=0;x<grid.getStore().getCount()-1;x++)
    {
    	fila=grid.getStore().getAt(x);
        montoTotal+=parseFloat(fila.get('costoUnitario'))*parseFloat(fila.get('cantidad'));
        
   	}
    return montoTotal+monto;
}