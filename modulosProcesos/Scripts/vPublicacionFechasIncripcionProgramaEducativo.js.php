<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");

?>	

var sL=false;
var arrPeriodos;
var plantel;
var nodoSel=false;
Ext.onReady(inicializar);

function inicializar()
{
	var gridPlanesEstudio=crearGridPlanesEstudio();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            border:false,
                                            items:	[
                                                    
                                                        {
                                                            xtype:'panel',
                                                            border:false,
                                                            region:'center',
                                                            layout:'border',
                                                            
                                                            items:	[
                                                                        gridPlanesEstudio
                                                                    ]
                                                        }
                                    				 ]
										}
									]                                        
                                                     
						}
                    )   
}


function crearGridPlanesEstudio()
{
	
    
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
		var cargadorArbol=new Ext.tree.TreeLoader(
                                                        {
                                                            baseParams:{
                                                                            funcion:'106'
                                                                        },
                                                            dataUrl:'../paginasFunciones/funcionesPlanteles.php'
                                                        }	


		                                         )		                                        
       
       
       cargadorArbol.on('beforeload',function(proxy)
       								{
                                    	proxy.baseParams.idPrograma=gE('idProgramaEducativo').value;
                                        proxy.baseParams.plantel=gE('plantel').value;
                                        
                                        
                                    }
       					)
                                
                                      
                                        
		var arbol = new Ext.ux.tree.TreeGrid	(
                                                            {
                                                                id:'arbolPlanes',
                                                                useArrows:true,
                                                                autoScroll:false,
                                                                animate:true,
                                                                enableDD:true,
                                                                containerScroll: true,
                                                                root:raiz,
                                                                region:'center',
                                                                border:false,
                                                                loader: cargadorArbol,
                                                                rootVisible:false,
                                                                draggable:false,
                                                                border:false,
                                                                enableSort : false,
                                                                
                                                                
                                                                columns:[
                                                                			
                                                                            {
                                                                                header:'',
                                                                                width:470,
                                                                                dataIndex:'text'
                                                                            },
                                                                            {
                                                                                header:'Fechas de Inscripci&oacute;n<br />(Nuevo ingreso)',
                                                                                width:160,
                                                                                menuDisabled :true,
                                                                                align:'center',
                                                                                css:'text-align:right !important;',
                                                                                sortable:false,
                                                                                dataIndex:'fechasInscripcion'
                                                                            },
                                                                             {
                                                                                header:'',
                                                                                width:155,
                                                                                dataIndex:'opcion'
                                                                            }
                                                                           
                                                                         ]
                                                               
                                                            }
                                                    );
        arbol.on('click',nodoClick);
        arbol.expandAll();    	
        return arbol;
}



function nodoClick(nodo)
{
	nodoSel=nodo;
    
}

function recargarFechas()
{
	gEx('arbolPlanes').getRootNode().reload();
    gEx('arbolPlanes').expandAll();
}

function solicitarInscripcion(btn)
{
	var arrDatos=btn.id.split('_');
    var obj={};
    obj.idCiclo=arrDatos[0];
    obj.idPeriodo=arrDatos[1];
    obj.idInstancia=arrDatos[2];
	window.parent.parent.ingresarInscripcion(obj);
}