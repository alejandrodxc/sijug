
<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT cveEstado,UPPER(estado) FROM 820_estados ORDER BY estado";
	$arrEstado=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idPais,nombre FROM 238_paises ORDER BY idPais";
	$arrPais=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCategoriaUnidadOrganigrama,nombreCategoria FROM 817_categoriasUnidades ORDER BY nombreCategoria";
	$arrCategorias=$con->obtenerFilasArreglo($consulta);
?>
var arrEstado=<?php echo $arrEstado?>;
var arrPais=<?php echo $arrPais?>;
var arrCategorias=<?php echo $arrCategorias?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridInstitucionesPatrocinadorasNuevas=crearGridInstitucionesPatrocinadorasNuevas();
	var gridInstitucionesPatrocinadoras=crearGridInstitucionesPatrocinadoras();
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Instituciones Patrocinadoras</b></span>',
                                               
                                                items:	[
	                                                		gridInstitucionesPatrocinadorasNuevas,
                                                            gridInstitucionesPatrocinadoras
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridInstitucionesPatrocinadoras()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idOrganigrama'},
		                                                {name: 'codigoUnidad'},
		                                                {name:'unidad'},
                                                        {name:'descripcion'},
                                                        {name:'idPais'},
                                                        {name: 'estado'},
                                                        {name: 'ciudad'},
                                                        {name: 'numReferencias'},
                                                        {name: 'porcentajeRetencion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosProcesos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'unidad', direction: 'ASC'},
                                                            groupField: 'unidad',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='23';
                                        proxy.baseParams.idUnidad=-1;
                                        
                                    }
                        )   
                        
                        
                        
	var tamPagina =	50;     

																							

    var paginador=	new Ext.PagingToolbar	(

                                                {

                                                      pageSize: tamPagina,

                                                      store: alDatos,

                                                      displayInfo: true,

                                                      disabled:false

                                                  }

                                               )                                                    
                        
      var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ {type: 'string', dataIndex: 'unidad'}]
                                                    }
                                                );                                                    

                                                    
                  
      var chkRow=new Ext.grid.CheckboxSelectionModel(); 
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'ID',
                                                                width:45,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'idOrganigrama'
                                                            },
                                                            {
                                                                header:'',
                                                                width:50,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'idOrganigrama',
                                                                renderer:function(val,meta,registro,nFila)
                                                                			{
                                                                            	return '&nbsp;&nbsp<a href="javascript:modificarInstitucion(\''+bE(val)+'\',0,\''+bE(registro.data.porcentajeRetencion)+'\')"><img src="../images/pencil.png" width="13" height="13" title="Modificar instituci&oacute;n" alt="Modificar instituci&oacute;n"></a>'+
                                                                                '&nbsp;&nbsp<a href="javascript:removerInstitucion(\''+bE(nFila)+'\')"><img src="../images/delete.png" width="13" height="13" title="Remover instituci&oacute;n" alt="Remover instituci&oacute;n"></a>';
                                                                            }
                                                            },
                                                            {
                                                                header:'Instituci&oacute;n patrocinadora',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'unidad'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:400,
                                                                sortable:true,
                                                                dataIndex:'descripcion'
                                                            },
                                                            {
                                                                header:'% retenci&oacute;n',
                                                                width:90,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'porcentajeRetencion'
                                                            },
                                                            {
                                                                header:'# referencias',
                                                                width:90,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'numReferencias'
                                                            },
                                                            {
                                                                header:'Pa&iacute;s',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'idPais',
                                                                renderer:function(val)	
                                                                      {
                                                                          return formatearValorRenderer(arrPais,val);
                                                                      }
                                                            },
                                                            {
                                                                header:'Estado',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'estado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(registro.get('idPais')=='146')
                                                                            	return formatearValorRenderer(arrEstado,val);
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Ciudad',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'ciudad'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridInstituciones',
                                                                store:alDatos,
                                                                title: '<span class="letraRojaSubrayada8" style="font-size:12px"><b>Instituciones Autorizadas</b></span>',
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                columnLines:true,
                                                                loadMask:true,
																sm:chkRow, 
                                                                tbar:	[
                                                                			{
                                                                                icon:'../images/arrow_switch.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Unificar instituciones',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	
                                                                                            	msgBox('Debe seleccionar la instituci&oacute;n que desea reemplace a otras instituciones');
                                                                                                return;
                                                                                            }
                                                                                            mostrarVentanaReemplazar(fila,0);
                                                                                        }
                                                                                
                                                                            }
                                                                		],
                                                                bbar:[paginador], 
                                                                plugins:[filters],                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                    );
	tblGrid.getStore().load({url: '../paginasFunciones/funcionesModulosProcesos.php',params:{start:0,limit:tamPagina}});                                                        
        return 	tblGrid;	
}

function crearGridInstitucionesPatrocinadorasNuevas()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idOrganigrama'},
		                                                {name: 'codigoUnidad'},
		                                                {name:'unidad'},
                                                        {name:'descripcion'},
                                                        {name:'idPais'},
                                                        {name: 'estado'},
                                                        {name: 'ciudad'},
                                                        {name: 'porcentajeRetencion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosProcesos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'unidad', direction: 'ASC'},
                                                            groupField: 'unidad',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='24';
                                        
                                    }
                        )   
                        
                        
                        
	
                                                    
                  
      var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true}); 
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'',
                                                                width:50,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'idOrganigrama',
                                                                renderer:function(val,meta,registro,nFila)
                                                                			{
                                                                            	return '&nbsp;&nbsp<a href="javascript:modificarInstitucion(\''+bE(val)+'\',0,\''+bE(registro.data.porcentajeRetencion)+'\')"><img src="../images/icon_big_tick.gif" width="13" height="13" title="Autorizar instituci&oacute;n" alt="Autorizar instituci&oacute;n"></a>'+
                                                                                '&nbsp;&nbsp<a href="javascript:reemplazarInstitucion(\''+bE(nFila)+'\')"><img src="../images/arrow_switch.png" width="13" height="13" title="Reemplazar instituci&oacute;n" alt="Reemplazar instituci&oacute;n"></a>';
                                                                            }
                                                            },
                                                            {
                                                                header:'Instituci&oacute;n patrocinadora',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'unidad'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:400,
                                                                sortable:true,
                                                                dataIndex:'descripcion'
                                                            },
                                                            {
                                                                header:'% retenci&oacute;n',
                                                                width:90,
                                                                hidden:true,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'porcentajeRetencion'
                                                            },
                                                            {
                                                                header:'Pa&iacute;s',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'idPais',
                                                                renderer:function(val)	
                                                                      {
                                                                          return formatearValorRenderer(arrPais,val);
                                                                      }
                                                            },
                                                            {
                                                                header:'Estado',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'estado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(registro.get('idPais')=='146')
                                                                            	return formatearValorRenderer(arrEstado,val);
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Ciudad',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'ciudad'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridInstitucionesNuevas',
                                                                store:alDatos,
                                                                region:'north',
                                                                height:200,
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                columnLines:true,
                                                                loadMask:true,
																sm:chkRow, 
                                                                title: '<span class="letraRojaSubrayada8" style="font-size:12px"><b>Instituciones Por Autorizar </b></span>',                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                    );
	
        return 	tblGrid;	
}

function modificarInstitucion(iO,accion,p)
{
	
	var arrPaises=arrPais;
    var arrUnidades=[['1','Departamento'],['2','Instituci\u00F3n']];
	var cmbPais=crearComboExt('cmbPais',arrPaises,120,15);
    cmbPais.setWidth(220);
    cmbPais.minListWidth=220;
    cmbPais.setValue('146');
    var controlTelefono='<table  border="0" cellspacing="1" cellpadding="1">'+
                        '<tr><td  >&nbsp;<select name="cmbTelefonoInst" id="cmbTelefonoInst" size="5" style="width:270px"></select><input type="hidden" name="telefonos" id="telefonos" value="" />'+
                        '</td><td width="5"  align="left">&nbsp;</td><td width="19"><table><tr><td>'+
                        '<a href="javaScript:solicitarTel(\'cmbTelefonoInst\')"><img src="../images/icon_big_tick.gif" alt="Agregar" height="15" title="Agregar Teléfono" border="0"/></a>'+
                        '</td></tr><tr><td>'+
                        '<a href="javaScript:eliminarTelefono(\'cmbTelefonoInst\')"><img src="../images/cancel_round.png" alt="Eliminar" title="Eliminar Teléfono" border="0"/></a>'+
                        '</td></tr></table><br /></td></tr></table>';

	var controlMail='<table  border="0" cellspacing="1" cellpadding="1">'+
                        '<tr><td  >&nbsp;<select name="cmbMail" id="cmbMail" size="5" style="width:270px"></select><input type="hidden" name="mailes" id="mailes" value="" />'+
                        '</td><td width="5"  align="left">&nbsp;</td><td width="19"><table><tr><td>'+
                        '<a href="javaScript:solicitarEMail()"><img src="../images/icon_big_tick.gif" alt="Agregar E-mail" height="15" title="Agregar E-mail" border="0"/></a>'+
                        '</td></tr><tr><td>'+
                        '<a href="javascript:eliminarEmail()"><img src="../images/cancel_round.png" alt="Eliminar E-mail" title="Eliminar E-mail" border="0"/></a>'+
                        '</td></tr></table><br /></td></tr></table>';                        
	
    var panelDir=	{
    					id:'panelDir1',
                        layout:'absolute',
                        xtype:'panel',
                        baseCls: 'x-plain',
                        x:0,
                        y:40,
                        width:650,
                        height:400,
                        hidden:true,
                        items:	[
                                    {
                                        x:10,
                                        y:10,
                                        baseCls: 'x-plain',
                                        html:'Estado/Provincia:'
                                    },
                                    {
                                        x:120,
                                        y:5,
                                        id:'txtEstado1',
                                        xtype:'textfield',
                                        width:200
                                    },
                                    {
                                        x:10,
                                        y:40,
                                        baseCls: 'x-plain',
                                        html:'Ciudad:'
                                    },
                                    {
                                        x:120,
                                        y:35,
                                        id:'txtCiudad1',
                                        xtype:'textfield',
                                        width:200
                                    },
                                    
                                    {
                                        x:10,
                                        y:70,
                                        html:'Calle:',
                                        baseCls: 'x-plain'
                                    },
                                    {
                                        x:120,
                                        y:65,
                                        id:'txtCalle1',
                                        xtype:'textfield',
                                        width:220
                                    },
                                    {
                                        x:380,
                                        y:70,
                                        html:'N&uacute;mero:',
                                        baseCls: 'x-plain'
                                    },						
                                    {
                                        x:440,
                                        y:65,
                                        id:'txtNumero1',
                                        xtype:'textfield',
                                        width:80
                                        
                                    },
                                    {
                                        x:10,
                                        y:100,
                                        html:'CP.:',
                                        baseCls: 'x-plain'
                                    },						
                                    {
                                        x:120,
                                        y:95,
                                        id:'txtCp1',
                                        xtype:'numberfield',
                                        width:80,
                                        allowDecimals:false,
                                        allowNegative:false
                                    }
                                    
                                ]
                    }
	
    
    var cmbEstado=crearComboExt('cmbEstado',arrEstado,120,5); 
    var cmbMunicipio=crearComboExt('cmbMunicipio',[],120,35,320);
    
    cmbPais.on('select',function (combo,registro)
    					{
                        	cmbEstado.setValue('');
                        	cmbMunicipio.setValue('');
                            var cmbColonia=gEx('cmbColonia');
							cmbColonia.setValue('');
                        	if(registro.get('id')=='146')
                            {
                            	gEx('cmbEstado').reset();
                                gEx('cmbMunicipio').reset();
                                gEx('txtCp').setValue('');
                                gEx('txtCalle').setValue('');
                                gEx('txtNumero').setValue();
                            	gEx('panelDir2').show();
                                gEx('panelDir1').hide();
                            }
                            else
                            {
                            	gEx('txtEstado1').setValue('');
                                gEx('txtCiudad1').setValue('');
                                gEx('txtCp1').setValue('');
                                gEx('txtCalle1').setValue('');
                                gEx('txtNumero1').setValue();
                            	gEx('panelDir1').show();
                                gEx('panelDir2').hide();
                            }
                        }
    
    	)
    cmbEstado.on('select',	function(combo,registro,indice,obj)
    							{
                                    function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                        	cmbMunicipio.getStore().loadData(eval(arrResp[1]));
                                            cmbMunicipio.setValue('');
                                            if(obj!=undefined)
                                            {
                                            	cmbMunicipio.setValue(obj.municipio);
                                                pos=obtenerPosFila(gEx('cmbMunicipio').getStore(),'id',obj.municipio);
                                                if(pos!='-1')
                                                {
                                                    var registro=gEx('cmbMunicipio').getStore().getAt(pos);
                                                    gEx('cmbMunicipio').fireEvent('select',gEx('cmbMunicipio'),registro,pos,obj);
                                                }
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax, 'POST','funcion=60&accion=1&codigo='+registro.get('id'),true);
                                }
    				)
	
   
    
	var panelDir2=	{
    					id:'panelDir2',
                        layout:'absolute',
                        xtype:'panel',
                        baseCls: 'x-plain',
                        x:0,
                        y:40,
                        width:650,
                        height:400,
                        items:[
                                    {
                                        x:10,
                                        y:10,
                                        baseCls: 'x-plain',
                                        html:'Estado:'
                                    },
                                    cmbEstado,
                                    {
                                        x:10,
                                        y:40,
                                        baseCls: 'x-plain',
                                        html:'Municipio:'
                                    },
                                    cmbMunicipio,
                                    {
                                        x:10,
                                        y:70,
                                        html:'Calle:',
                                        baseCls: 'x-plain'
                                    },						
                                    {
                                        x:120,
                                        y:65,
                                        id:'txtCalle',
                                        xtype:'textfield',
                                        width:220
                                    },
                                    {
                                        x:380,
                                        y:70,
                                        html:'N&uacute;mero:',
                                        baseCls: 'x-plain'
                                    },						
                                    {
                                        x:440,
                                        y:65,
                                        id:'txtNumero',
                                        xtype:'textfield',
                                        width:80
                                        
                                    }
                                    ,						
                                    {
                                        x:120,
                                        y:125,
                                        id:'txtCp',
                                        xtype:'numberfield',
                                        width:80,
                                        allowDecimals:false,
                                        allowNegative:false
                                    },
                                    {
                                        x:10,
                                        y:100,
                                        baseCls: 'x-plain',
                                        html:'Colonia:'
                                    },
                                    {
                                    	x:120,
                                        y:95,
                                        id:'cmbColonia',
                                        xtype:'textfield',
                                        width:220
                                    },
                                    {
                                        x:10,
                                        y:130,
                                        html:'CP.:',
                                        baseCls: 'x-plain'
                                    }
                                    
                                    
                                    
                                ]
                    }                    
        
    var txtCod;
    var codigoPadre='';
    var longCod=4;
    var ancho=80;
    var lblVentana;
    
    if(accion==1)
        lblVentana='Agregar Unidad'
    else
        lblVentana='Modificar Instituci&oacute;n'
    
    
    codigoPadre='';
    txtCod='txtCodigoInst';

	var cmbTipoUnidad=crearComboExt('cmbTipoUnidad',arrCategorias,140,155,300);
	cmbTipoUnidad.setValue('2');
    cmbTipoUnidad.disable();
    var panelInst=new Ext.Panel(
    								{
                                    	id:'panelInst',
                                    	x:10,
                                        y:10,
                                        baseCls: 'x-plain',
										layout:'absolute',
                                        width:680,
                                        height:500,
                                    	items:[
                                        		{
                                                      
                                                      y:10,
                                                      baseCls: 'x-plain',
                                                      hidden:true,
                                                      html:'Cve. Instituci&oacute;n:'
                                                  },
                                                  {
                                                      x:140,
                                                      hidden:true,
                                                      id:'txtCodigoInst',
                                                      xtype:'textfield',
                                                      disbaled:true,
                                                      maxLength:4,
                                                      width:200,
                                                      y:5
                                                  },
                                                  {
                                                      
                                                      y:40,
                                                      baseCls: 'x-plain',
                                                      html:'Instituci&oacute;n: <font color="red">*</font>'
                                                  },
                                                  {
                                                      x:140,
                                                      y:35,
                                                      id:'txtInstitucionNueva',
                                                      xtype:'textfield',
                                                      width:430
                                                  },
                                                  {
	                                               	  
                                                      y:70,
                                                      baseCls: 'x-plain',
                                                      html:'Descripci&oacute;n:'
                                                  },
                                                  {
                                                  	x:140,
                                                    y:65,
                                                    xtype:'textarea',
                                                    width:430,
                                                    height:80,
                                                    id:'txtDescripcion'
                                                  },
                                                  {

                                                    y:160,
                                                    baseCls: 'x-plain',
                                                    html:'% de retenci&oacute;n:<font color="red">*</font>'
                                                  },
                                                  {
                                                  	x:140,
                                                    y:155,
                                                    xtype:'numberfield',
                                                    allowDecimals:true,
                                                    allowNegative:false,
                                                    value:bD(p),
                                                    id:'txtPorcentajeRetencion',
                                                    width:80
                                                  }
                                                  
                                              ]
                                    }
                                )
    
    
     
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
                                            defaultType:'label',
											items:	[
                                            			{
                                                        	xtype:'tabpanel',
                                                            id:'tabGral',
                                                            activeTab:2,
                                                            height:500,
                                                            width:700,
                                                            baseCls: 'x-plain',
                                                            items:	[
                                                            			{
                                                                        	xtype:'panel',
                                                                            baseCls: 'x-plain',
                                                                            title:'Datos generales',
                                                                            layout:'absolute',
                                                                            items:[panelInst]
                                                                        },
                                                                        {
                                                                        	xtype:'panel',
                                                                            baseCls: 'x-plain',
                                                                            title:'Datos de ubicaci&oacute;n',
                                                                            layout:'absolute',
                                                                            items:	[
                                                                                        {
                                                                                              x:10,
                                                                                              y:20,
                                                                                              baseCls: 'x-plain',
                                                                                              
                                                                                              html:'Pa&iacute;s:<font color="red">*</font>'
                                                                                          }
                                                                                          ,
                                                                                          cmbPais,
                                                                                          panelDir,
                                                                                          panelDir2,
                                                                                    ]
                                                                        },
                                                                        {
                                                                        	xtype:'panel',
                                                                            baseCls: 'x-plain',
                                                                            title:'Datos de contacto',
                                                                            layout:'absolute',
                                                                            items:	[
                                                                                        {
                                                                                          x:10,
                                                                                          y:20,
                                                                                          baseCls: 'x-plain',
                                                                                          html:'Tel&eacute;fono:'
                                                                                        },
                                                                                        {
                                                                                          x:90,
                                                                                          y:15,
                                                                                          baseCls: 'x-plain',
                                                                                          html:controlTelefono
                                                                                        },
                                                                                        {
                                                                                          x:10,
                                                                                          y:100,
                                                                                          baseCls: 'x-plain',
                                                                                          html:'E-mail:'
                                                                                        },
                                                                                        {
                                                                                          x:90,
                                                                                          y:95,
                                                                                          baseCls: 'x-plain',
                                                                                          html:controlMail
                                                                                        }
                                                                                    ]
                                                                        }
                                                            		]
                                                                    
                                                        }
                                                        

                                                   ]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:lblVentana,
										width:620,
										height:320,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,
                                                    fn:function()
														{
																Ext.getCmp(txtCod).focus(false,500);
														}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	if(accion=='0')
                                                                	idOrganigrama=bD(iO);
                                                            	
                                                                var codUnidad=Ext.getCmp('txtCodigoInst').getValue();
                                                                var telefonos=recoletarValoresCombo('cmbTelefonoInst');
                                                                var txtInstitucion=Ext.getCmp('txtInstitucionNueva');
                                                                var objIns='';
                                                                if(cmbPais.getValue()=='146')
                                                                {
                                                                    var estado=cmbEstado.getValue();
                                                                    var municipio=cmbMunicipio.getValue();
                                                                    var localidad='';
                                                                    var colonia=gEx('cmbColonia').getValue();
                                                                    var cp=gEx('txtCp').getValue();
                                                                    var calle=gEx('txtCalle').getValue();
                                                                    var numero=gEx('txtNumero').getValue();
                                                                    objIns='{"idPais":"'+cmbPais.getValue()+'","estado":"'+estado+'","municipio":"'+municipio+'","localidad":"'+localidad+'","colonia":"'+cv(colonia)+'","cp":"'+cp+'","calle":"'+cv(calle)+'","numero":"'+numero+'"}';
                                                                }
                                                                else
                                                                {
                                                                    var estado=gEx('txtEstado1').getValue();
                                                                    var municipio='';
                                                                    var localidad=gEx('txtCiudad1').getValue();
                                                                    var colonia='';
                                                                    var cp=gEx('txtCp1').getValue();
                                                                    var calle=gEx('txtCalle1').getValue();
                                                                    var numero=gEx('txtNumero1').getValue();
                                                                    objIns='{"idPais":"'+cmbPais.getValue()+'","estado":"'+estado+'","municipio":"'+municipio+'","localidad":"'+localidad+'","colonia":"'+colonia+'","cp":"'+cp+'","calle":"'+calle+'","numero":"'+numero+'"}';
                                                                }
                                                                var txtCp=Ext.getCmp('txtCp');
                                                                var txtCiudad=Ext.getCmp('txtCiudad');
                                                                var txtEstado=Ext.getCmp('txtEstado');
                                                                if(txtInstitucion.getValue()=='')
                                                                {
                                                                    function resp()
                                                                    {
                                                                    	gEx('tabGral').setActiveTab(0);
                                                                        txtInstitucion.focus();
                                                                    }
                                                                    msgBox("El campo de instituci&oacute;n es obligatorio",resp);
                                                                    return;
                                                                }
                                                                var tUnidad=1;
                                                                
                                                                var cmbMail=gE('cmbMail');
                                                                var email='';
                                                                var x;
                                                                for(x=0;x<cmbMail.options.length;x++)
                                                                {
                                                                	if(email=='')
                                                                    	email=cmbMail.options[x].value;
                                                                    else
                                                                    	email+=','+cmbMail.options[x].value;
                                                                    
                                                                }
                                                                var porcentajeRetencion=gEx('txtPorcentajeRetencion');
                                                                if(porcentajeRetencion.getRawValue()=='')
                                                                {
                                                                	function respX()
                                                                    {
                                                                    	gEx('tabGral').setActiveTab(0);
                                                                    	porcentajeRetencion.focus();
                                                                    }
                                                                    msgBox('El porcentaje de valor ingresado no es v&aacute;lido',respX);
                                                                    return;
                                                                }
                                                                var descripcion=cv(Ext.getCmp('txtDescripcion').getValue());
                                                                var objParam='{"instPatrocinadora":"1","porcentajeRetencion":"'+porcentajeRetencion.getRawValue()+'","idOrganigrama":"'+bD(iO)+'","codUnidad":"'+codUnidad+'","codigoUPadre":"'+codigoPadre+'","nombre":"'+cv(txtInstitucion.getValue())+
                                                                			'","descripcion":"'+descripcion+'","institucion":"'+tUnidad+'","objInst":'+objIns+',"telefonos":"'+telefonos+'","email":"'+email+'"}';
                                                               
                                                                guardarInstitucion(objParam,ventana);    
                                                            	
                                                                                                                          
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
																
															}
													}
												 ]
									}
							   )
	ventana.show();  
	if(accion=='0')
    	llenarDatosUnidad(ventana,iO);
    else
    {                               
		ventana.show();   
        var x=2;
        for(x=2;x>=0;x--)
	        gEx('tabGral').setActiveTab(x);
                 
    }
}

function llenarDatosUnidad(ventana,iO)
{
    var alto;
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        
            var datosIns=eval(arrResp[1])[0];
            var telefonos=datosIns.telefonos;
            var descTel=datosIns.descTel;
            
            var arrTelefonos=new Array();
            if(telefonos!='')
            {
                var arrTel=telefonos.split(',');
                var arrDescTel=descTel.split('<br>');
                var arr;
                var x;
                for(x=0;x<arrTel.length;x++)
                {
                    arr=new Array();
                    arr[0]=arrTel[x];
                    arr[1]=arrDescTel[x];
                    arrTelefonos[x]=arr;
                }
            }    
            
            
            var mails=datosIns.mails;
            var arrMail=new Array();
            var aMail=new Array();
            if(mails!='')
            {
                var arrMail=mails.split(',');
                var arr;
                var x;
                for(x=0;x<arrMail.length;x++)
                {
                    arr=new Array();
                    arr[0]=arrMail[x];
                    arr[1]=arrMail[x];
                    aMail[x]=arr;
                }
            }    
            
            
            var combo;
            var txtCodigo;
            
            txtCodigo=Ext.getCmp('txtCodigoInst');
            txtCodigo.setValue(datosIns.codDepto);
            combo=gE('cmbTelefonoInst');
            rellenarCombo(combo,arrTelefonos)
            combo=gE('cmbMail');
            rellenarCombo(combo,aMail)
            var codigoU=datosIns.codigoU;
            
            
            
            var txtInstitucion=Ext.getCmp('txtInstitucionNueva');
            txtInstitucion.setValue(datosIns.institucion);
            var txtCp=Ext.getCmp('txtCp');
            txtCp.setValue(datosIns.cp);
            var cmbPais=Ext.getCmp('cmbPais');
            cmbPais.setValue(datosIns.idPais);
            var pos=obtenerPosFila(cmbPais.getStore(),'id',datosIns.idPais);
            if(pos!='-1')
            {
                var registro=cmbPais.getStore().getAt(pos);
                cmbPais.fireEvent('select',cmbPais,registro);
            }
            if(datosIns.idPais!='146')
            {
                gEx('txtEstado1').setValue(datosIns.estado);
                gEx('txtCiudad1').setValue(datosIns.ciudad);
                gEx('txtCp1').setValue(datosIns.cp);
                gEx('txtCalle1').setValue(datosIns.calle);
                gEx('txtNumero1').setValue(datosIns.numero);
            }
            else
            {
                gEx('cmbEstado').setValue(datosIns.estado);
                pos=obtenerPosFila(gEx('cmbEstado').getStore(),'id',datosIns.estado);
                if(pos!='-1')
                {
                    var registro=gEx('cmbEstado').getStore().getAt(pos);
                    gEx('cmbEstado').fireEvent('select',gEx('cmbEstado'),registro,pos,datosIns);
                }
                
                
                gEx('txtCp').setValue(datosIns.cp);
                gEx('txtCalle').setValue(datosIns.calle);
                gEx('txtNumero').setValue(datosIns.numero);
               
                gEx('cmbColonia').setValue(datosIns.colonia);
                
            }
            
            var descripcion=Ext.getCmp('txtDescripcion');
            descripcion.setValue(dv(datosIns.descripcion));
            ventana.show();   
            gEx('tabGral').setActiveTab(0);
            txtInstitucion.focus(false,1000);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax, 'POST','funcion=27&idOrganigrama='+bD(iO),true);
    
}

function guardarInstitucion(objInst,ventana)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('gridInstituciones').getStore().reload();
            gEx('gridInstitucionesNuevas').getStore().reload();
            ventana.close();
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax, 'POST','funcion=33&param='+objInst,true);
}

function mostrarVentanaReemplazar(fila,accion)
{
	var lblAccion='Indique las instituciones que ser&aacute;n unificadas bajo la instituci&oacute;n referente';
    var lblTitulo='Unificar instituciones autorizadas';
    var lblEt='Instituci&oacute;n referente:';
    var confirmacion='Est&aacute; seguro de querer sustituir las instituci&oacute;nes seleccionadas por la instituci&oacute;n: <b>'+fila.get('unidad')+'</b>';
    if(accion==1)
    {
    	lblAccion='Indique la instituci&oacute;n que reemplazar&aacute; a la Nueva Instituci&oacute;n';
        lblTitulo='Reemplazar Instituci&oacute;n Nueva';
        lblEt='Nueva Instituci&oacute;n:'
        
    }
	var gridInstitucionesAutorizadas=crearGridInstitucionesUnificacion(fila,accion);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:lblEt
                                                        },
                                                        {
                                                        	x:130,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8">'+fila.get('unidad')+'</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:lblAccion
                                                        },
                                                        gridInstitucionesAutorizadas

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 800,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var filas=gridInstitucionesAutorizadas.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Al menos debe seleccionar una instituci&oacute;n para unificar con la instituci&oacute;n referente');
                                                                        	return;
                                                                        }
                                                                        var cadObj='';
                                                                        var listaInstituciones='';
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                            if(listaInstituciones=='')
                                                                                listaInstituciones=filas[x].get('idOrganigrama');
                                                                            else
                                                                                listaInstituciones+=','+filas[x].get('idOrganigrama');
                                                                        }
                                                                        
                                                                        if(accion==0)
                                                                        {
                                                                        	
                                                                        	cadObj='{"idBase":"'+fila.get('idOrganigrama')+'","arrUnificacion":"'+listaInstituciones+'"}';
                                                                        }
                                                                        else
                                                                        {
                                                                        	confirmacion='Est&aacute; seguro de querer sustituir la instituci&oacute;n: <b>'+fila.get('unidad')+'</b> por la instituci&oacute;n: <b>'+filas[0].get('unidad')+'</b>';
                                                                        	cadObj='{"idBase":"'+listaInstituciones+'","arrUnificacion":"'+fila.get('idOrganigrama')+'"}';
                                                                        }
                                                                        function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridInstituciones').getStore().reload();
                                                                                        gEx('gridInstitucionesNuevas').getStore().reload(); 
                                                                                        ventanaAM.close();  
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=25&cadObj='+cadObj,true);
                                                                        	}
                                                                        }
                                                                        msgConfirm(confirmacion,resp);
	
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridInstitucionesUnificacion(fila,accion)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idOrganigrama'},
		                                                {name: 'codigoUnidad'},
		                                                {name:'unidad'},
                                                        {name:'descripcion'},
                                                        {name:'idPais'},
                                                        {name: 'estado'},
                                                        {name: 'ciudad'},
                                                        {name: 'porcentajeRetencion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosProcesos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'unidad', direction: 'ASC'},
                                                            groupField: 'unidad',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='23';
                                        proxy.baseParams.idUnidad=fila.get('idOrganigrama');
                                        
                                    }
                        )   
                        
                        
                        
	var tamPagina =	50;     

																							

    var paginador=	new Ext.PagingToolbar	(

                                                {

                                                      pageSize: tamPagina,

                                                      store: alDatos,

                                                      displayInfo: true,

                                                      disabled:false

                                                  }

                                               )                                                    
                        
      var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ {type: 'string', dataIndex: 'unidad'}]
                                                    }
                                                );                                                    

                                                    
                  
      var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:(accion==1)}); 
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'ID',
                                                                width:45,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'idOrganigrama'
                                                            },
                                                            
                                                            {
                                                                header:'Instituci&oacute;n patrocinadora',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'unidad'
                                                            },
                                                            {
                                                                header:'% retenci&oacute;n',
                                                                width:90,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'porcentajeRetencion'
                                                            },
                                                            {
                                                                header:'Pa&iacute;s',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'idPais',
                                                                renderer:function(val)	
                                                                      {
                                                                          return formatearValorRenderer(arrPais,val);
                                                                      }
                                                            },
                                                            {
                                                                header:'Estado',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'estado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(registro.get('idPais')=='146')
                                                                            	return formatearValorRenderer(arrPais,val);
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Ciudad',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'ciudad'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridInstitucionesAutorizadas',
                                                                store:alDatos,
                                                                x:10,
                                                                y:70,
                                                                width:750,
                                                                height:290,
                                                                
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                columnLines:true,
                                                                loadMask:true,
																sm:chkRow, 
                                                                
                                                                bbar:[paginador], 
                                                                plugins:[filters],                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                    );
	tblGrid.getStore().load({url: '../paginasFunciones/funcionesModulosProcesos.php',params:{start:0,limit:tamPagina,idUnidad:fila.get('idOrganigrama')}});                                                        
        return 	tblGrid;	
}

function reemplazarInstitucion(nFila)
{
	var fila=gEx('gridInstitucionesNuevas').getStore().getAt(parseInt(bD(nFila)));
   
    mostrarVentanaReemplazar(fila,1);
    
}

function removerInstitucion(nFila)
{
	var fila=gEx('gridInstituciones').getStore().getAt(parseInt(bD(nFila)));
    if(fila.get('numReferencias')!='0')
    {
    	msgBox('S&oacute;lo puede remover aquellas instituciones que no cuenten con referencias asociadas');
    	return;
    }
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    gEx('gridInstituciones').getStore().reload();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=26&idOrganigrama='+fila.get('idOrganigrama'),true);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover la instituci&oacute;n: <b>'+fila.get('unidad')+'</b>',resp);
}