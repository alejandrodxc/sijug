<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$sL=false;
	if(isset($_GET["sL"]))
		$sL=true;
?>
Ext.onReady(inicializar);

function inicializar()
{
   var grid=crearProveedoresParticipantes();
   new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			grid
                                            			
                                            		]
                                        }
                                     ]
						}
                    )   
}

function crearProveedoresParticipantes()
{
    var idFormulario=gE('idFormulario').value;
    var idRegistro=gE('idRegistro').value;
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idLicitacionVSProveedor',
                                            fields: [
                                               			{name: 'idLicitacionVSProveedor'},
                                                        {name: 'idProveedor'},
                                                        {name: 'proveedor'},
                                                        {name: 'rfc'},
                                                        {name: 'noProveedorLicitacion', type :'int'}
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: false
                                        }
                                      );
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesCompras.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'noProveedorLicitacion'},
                                                            groupField: 'proveedor'
                                                        })                                      
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=18;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
                                    }
                        )
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
    
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                        {
															header:'No. Proveedor',
															width:90,
															sortable:true,
															dataIndex:'noProveedorLicitacion'
														},
                                                        {
															header:'RFC',
															width:200,
															sortable:true,
															dataIndex:'rfc'
														},
                                                         {
															header:'Proveedor',
															width:450,
															sortable:true,
															dataIndex:'proveedor'
														}
													]
												);
                                               
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            title:'<span style="font-size:13px; font-weight:bold; color:#900;">Proveedores participantes</span>',
                                                            frame:false,
                                                            border:false,
                                                            region:'center',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            id:'gridProveedores',
                                                            <?php
																if(!$sL)
																{
															?>
                                                            tbar:	[
                                                            			{
                                                                        	text:'Agregar proveedor',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	agregarProveedor();
                                                                                }
                                                                        }
                                                                        ,
                                                                        {
                                                                        	text:'Remover proveedor',
                                                                            icon:'../images/cancel_round.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	function resp(btn)
                                                                                    {
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el proveedor participante que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                			function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {

                                                                                                    tblGrid.getStore().remove(fila);
                                                                                                    window.parent.mostrarMenuDTD();
                                                                                                    
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=19&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&idProveedor='+fila.get('idProveedor'),true);
                                                                                            

                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer remover la partida seleccionada?',resp);
                                                                                    
                                                                                }
                                                                        }
                                                            		],
                                                            <?php
																}
															?>
                                                         	
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: false,
                                                                                                     enableGrouping :false
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );
		
    dsTablaRegistros2.load(); 
	return 	tblGrid;
}

function agregarProveedor()
{
	var oRFC=	{
    				idCombo:'cmbRFC',
                    posX:130,
                    posY:5,
                    anchoCombo:200,
                    anchoLista:350,
                    campoDesplegar:'rfc',
                    campoID:'idProveedor',
                    campoHDestino:'idProveedor',
                    funcionBusqueda:21,
                    paginaProcesamiento:'../paginasFunciones/funcionesCompras.php',
                    confVista:'<tpl for="."><div class="search-item"><b>{rfc}</b> <br> {proveedor}</div></tpl>',
                    campos:	[
                    			{name:'idProveedor'},
                                {name:'rfc'},
                                {name:'proveedor'}
                    		],
                    funcAntesCarga:function(dSet,combo)
                    				{
                                    	gEx('cmbProveedor').reset();
                                    	gE('idProveedor').value=-1;
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        dSet.baseParams.tBusqueda=1;
                                        dSet.baseParams.idFormulario=gE('idFormulario').value;
                                        dSet.baseParams.idRegistro=gE('idRegistro').value;
                                    },
					funcElementoSel:function(combo,registro)
                    				{
                                    	gE('idProveedor').value=registro.get('idProveedor');
                                    	gEx('cmbProveedor').setRawValue(registro.get('proveedor'));
                                    }                                    
    			};
    var oProveedor=	{
    					idCombo:'cmbProveedor',
                        posX:130,
                        posY:35,
                        anchoCombo:350,
                        campoDesplegar:'proveedor',
                        campoID:'idProveedor',
                        campoHDestino:'idProveedor',
                        funcionBusqueda:21,
                        paginaProcesamiento:'../paginasFunciones/funcionesCompras.php',
                        confVista:'<tpl for="."><div class="search-item"><b>{proveedor}</b> <br><span class="letraRojaSubrayada8">RFC:</span> {rfc}</div></tpl>',
                        campos:	[
                                    {name:'idProveedor'},
                                    {name:'rfc'},
                                    {name:'proveedor'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	gEx('cmbRFC').reset();
                                    	gE('idProveedor').value=-1;
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        dSet.baseParams.tBusqueda=2;
                                        dSet.baseParams.idFormulario=gE('idFormulario').value;
                                        dSet.baseParams.idRegistro=gE('idRegistro').value;
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('idProveedor').value=registro.get('idProveedor');
                                    	gEx('cmbRFC').setRawValue(registro.get('rfc'));
                                    }  
    				};
	var cmbRFC=crearComboExtAutocompletar(oRFC);
    var cmbProveedor=crearComboExtAutocompletar(oProveedor);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'RFC del proveedor:'
                                                        },
                                                        cmbRFC,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Proveedor:'
                                                        },
                                                        cmbProveedor,
                                                        {
                                                        	x:130,
                                                            y:70,
                                                            html:'¿No se encuentra el proveedor registrado en el sistema? Para agregarlo de click <a href="javascript:registrarProveedor()"><span class="letraRoja">AQUÍ</span></a>'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar proveedor participante',
										width: 600,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbRFC').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var idProveedor=gE('idProveedor').value;
																		if(idProveedor==-1)
                                                                        {
                                                                        	msgBox('Debe seleccionar el proveedor que desea agregar');
                                                                            return;
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProveedores').getStore().reload();
                                                                                window.parent.mostrarMenuDTD();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=20&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&idProveedor='+idProveedor,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function registrarProveedor()
{
	var conf={};
    conf.url='../modeloPerfiles/registroFormulario.php';
    conf.ancho=800;
    conf.alto=420;
    conf.titulo='Agregar proveedor';
    conf.params=[['idFormulario',405],['idRegistro',-1],['cPagina','sFrm=true'],['funcPHPEjecutarNuevo',bE('asignarDatosProveedor(idRegPadre)')]];
    abrirVentanaFancy(conf);
}

function asignarDatosProveedor(idProveedor,rfc,nProveedor)
{
	gE('idProveedor').value=idProveedor;
    gEx('cmbRFC').setRawValue(rfc);
    gEx('cmbProveedor').setRawValue(nProveedor);
	cerrarVentanaFancy();    
}


