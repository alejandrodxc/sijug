Ext.onReady(inicializar);

function inicializar()
{
	var gridProveedores=crearGridProveedores();
}

function crearGridProveedores()
{
	var dsDatos=eval(bD(gE('aProveedor').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProveedor'},
                                                                    {name: 'proveedor'},
                                                                    {name: 'docAnexo'}
                                                                ]
                                                    }
                                            );
    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Proveedor',
															width:400,
															sortable:true,
															dataIndex:'proveedor'
														},
														{
															header:'Doc. anexo',
															width:130,
															sortable:true,
															dataIndex:'docAnexo',
                                                            renderer: function(val)
                                                            			{
                                                                        	if(val!='')
	                                                                            return '<a href="../media/obtenerDocumentoProveedor.php?iD='+bE(val)+'"><img src="../images/download.png" title="Descargar documento" alt="Descargar documento"/></a>';
	                                                                        else 
                                                                            	return '';
                                                                        }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            renderTo:'divGridProveedores',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:360,
                                                            width:600,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;
}