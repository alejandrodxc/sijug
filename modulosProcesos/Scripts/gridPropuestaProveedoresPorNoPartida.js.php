<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$sL=false;
	if(isset($_GET["sL"]))
		$sL=true;
		
	$consulta="SELECT id__406_tablaDinamica,descripcion FROM _406_tablaDinamica ORDER BY descripcion";
	$arrMarca=$con->obtenerFilasArreglo($consulta);
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	
	$consulta="SELECT noInternoLicitacion,(IF(tipoProducto=1,(SELECT nombreProducto FROM 9101_CatalogoProducto WHERE idProducto=c.idProducto),
					(IF(complementario IS NULL,(SELECT nombreObjetoGasto FROM 507_objetosGasto WHERE codigoControl=c.objetoGasto),complementario)))) AS nombreProducto,
					CONCAT('[',cat.cveCategoria,'] ',cat.nombre) AS categoria,CONCAT('[',al.cveAlmacen,'] ',al.nombreAlmacen) AS almacen,idProductoVSLicitacion,p.clave_Art,
					c.costoUnitario 
					FROM 527_concentradoProductoTipoCompra c, 9101_CatalogoProducto p, 529_licitaciones l, 508_tiposPresupuesto pre,9030_categoriasObjetoGasto cat,9030_almacenes al
					WHERE c.idProducto=p.idProducto AND c.idCompraVSProducto=l.idCompraVSProducto AND pre.idTipoPresupuesto=c.fuenteFinanciamiento 
					AND cat.idCategoria=c.idCategoriaProducto AND al.idAlmacen=cat.idAlmacen AND l.idFormulario=".$idFormulario." AND l.idReferencia=".$idRegistro;
	$arrPartidas=$con->obtenerFilasArreglo($consulta);
	
	$consulta="select id__972_tablaDinamica,situacion,colorIdentificador from _972_tablaDinamica";
	$arrSituacion=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT porcentajeIgnora FROM _971_tablaDinamica";
	$porcentaje=$con->obtenerValor($consulta)/100;

?>
var arrPartidas=<?php echo $arrPartidas?>;
var arrMarca=<?php echo $arrMarca?>;
var arrSituacion=<?php echo $arrSituacion?>;
var nIngreso=0;
var pocentajeLimite=<?php echo $porcentaje ?>;
Ext.onReady(inicializar);

function inicializar()
{
   var grid=crearProveedoresPropuesta();
   new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			grid
                                            		]
                                        }
                                     ]
						}
                    )   
}

function crearProveedoresPropuesta()
{
	var idFormulario=gE('idFormulario').value;
    var idRegistro=gE('idRegistro').value;
    var arrProv=eval(eval(bD(gE('arrProv').value)))
   	var cmbProveedor=crearComboExt('cmbProveedor',arrProv,0,0,350);
    cmbProveedor.on('select',function(combo,registro)
    						{
                            	if((registro.get('valorComp')!='')&&(registro.get('valorComp')!='0'))
                                	inhabilitarBotones();
                                else
                                	habilitarBotones();
                            	gEx('gridProveedores').getStore().reload();
                            }
    				)
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idPartida',
                                            fields: [
                                            			{name: 'noPartida'},
                                               			{name: 'idPropuesta'},
                                                        {name: 'idPartida'},
                                                        {name: 'grupo'},
                                                        {name: 'almacen'},
                                                        {name: 'cve_Art'},
                                                        {name: 'producto'},
                                                        {name: 'costo'},
                                                        {name: 'situacionCosto'},
                                                        {name: 'costoBase'}
                                                        
                                                        
                                                        
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesCompras.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'producto', direction: 'ASC'},
                                                            groupField: 'producto',
                                                            autoLoad:false
                                                        })                                      
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=22;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
	                                    proxy.baseParams.idProveedor=cmbProveedor.getValue();
                                    }
                        )
	var chkRow=new Ext.grid.CheckboxSelectionModel();
    
    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editor_propuesta',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
	editorFila.on('afteredit',funcEditorAfterEditPropuesta)                                                
    editorFila.on('beforeedit',funcEditorBeforeEditPropuesta)
    editorFila.on('validateedit',funcEditorValidaPropuesta);
    editorFila.on('canceledit',funcEditorCancelPropuesta);
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                         {
															header:'No. partida *',
															width:80,
															sortable:true,
															dataIndex:'noPartida',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        id:'editor_noPartida',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
                                                           
														},
                                                        {
															header:'Costo *',
															width:150,
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney',
                                                             editor:	{
                                                                            xtype:'numberfield',
                                                                            id:'editor_costo',
                                                                            allowDecimals:true,
                                                                            allowNegative:false
                                                            		}
														},
                                                         {
															header:'Cve. Producto',
															width:120,
															sortable:true,
															dataIndex:'cve_Art'
														},
                                                        {
															header:'Producto',
															width:300,
															sortable:true,
															dataIndex:'producto'
														},
                                                        {
															header:'Almac&eacute;n',
															width:180,
															sortable:true,
															dataIndex:'almacen'
														},
                                                        {
															header:'Grupo',
															width:180,
															sortable:true,
															dataIndex:'grupo'
														},
                                                        {
															header:'Costo L&iacute;mite',
															width:150,
															sortable:true,
															dataIndex:'costoBase',
                                                            renderer:'usMoney'
                                                            
														},
                                                        {
															header:'Situaci&oacute;n',
															width:250,
															sortable:true,
															dataIndex:'situacionCosto',
                                                            renderer:function(val)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrSituacion,val);
                                                                        if(pos!=-1)
	                                                                        return '<span style="color:#'+arrSituacion[pos][2]+'">'+arrSituacion[pos][1]+'</span>';	
                                                                    }
                                                            
														}
                                                        
													]
												);
                                               
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            title:'<span style="font-size:13px; font-weight:bold; color:#900;">Proveedores participantes</span>',
                                                            frame:false,
                                                            border:false,
                                                            region:'center',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            id:'gridProveedores',
                                                            plugins:[editorFila],
                                                            tbar:	[
                                                            			{
                                                                        	html:'<span class="letraRojaSubrayada8"><b>Proveedor:</b></span>&nbsp&nbsp;'
                                                                        },
                                                                        cmbProveedor
                                                            		
                                                            <?php
																if(!$sL)
																{
															?>
                                                            			,'-',
                                                                        {
                                                                        	id:'btnAgregar',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar partida',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	agregarNuevaFila();
                                                                                        
                                                                                        
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	id:'btnRemover',
                                                                            disabled:true,
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover partida',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la partida que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')	
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                    	window.parent.mostrarMenuDTD();
                                                                                                        tblGrid.getStore().remove(fila);
                                                                                                        
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=67&idPropuesta='+fila.get('idPropuesta'),true);

                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la partida seleccionada?',resp);
                                                                                    }
                                                                       },
                                                                        
                                                                        '-'
                                                                        ,{
                                                                        	text:'Cerrar registro de propuestas del proveedor',
                                                                            icon:'../images/lock.png',
                                                                            id:'btnCerrar',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            id:'btnCerrar',
                                                                            handler:function()
                                                                            	{
                                                                                	var cmbProveedor=gEx('cmbProveedor');
                                                                                    if(cmbProveedor.getValue()=='')
                                                                                    {
	                                                                                    msgBox('Debe seleccionar el proveedor cuyo registro de propuestas desea cerrar');
                                                                                        return;
                                                                                    }
                                                                                	var x;
                                                                                    var fila;
                                                                                    for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                    {
                                                                                    	fila=tblGrid.getStore().getAt(x);
                                                                                        if((fila.get('situacion')=='-1')||(fila.get('situacion')==-1))
                                                                                        {
                                                                                        	
                                                                                        	msgBox('Ninguna partida puede quedar marcada como "Sin propuesta"');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    var cmbProveedor=gEx('cmbProveedor');
                                                                                    var idProveedor=cmbProveedor.getValue();
                                                                                    var cadObj='{"idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+'","idProveedor":"'+idProveedor+'"}';
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                	var almacen=cmbProveedor.getStore();
                                                                                                	var pos=obtenerPosFila(almacen,'id',idProveedor);
                                                                                                    almacen.getAt(pos).set('valorComp','1');
                                                                                                    inhabilitarBotones();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=25&cadObj='+cadObj,true);
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer cerrar el registro de propuestas de este proveedor?',resp);
                                                                                }
                                                                        }
                                                            		
                                                            <?php
																}
															?>
                                                         			],
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: false,
                                                                                                     enableGrouping :false
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );
		
    tblGrid.nuevoRegistro=false;
    tblGrid.soloLectura=false;
    <?php
		if($sL)
			echo " tblGrid.soloLectura=true;";
	?>
    tblGrid.on('beforeedit',function(e)
    						{
                            	e.cancel=tblGrid.soloLectura;
                            }
               )
	return 	tblGrid;
}


function agregarNuevaFila()
{
	nIngreso=0;
	var tblGrid=gEx('gridProveedores');
    var editorFila=gEx('editor_propuesta');
    var registroGrid=crearRegistro([
                                            {name: 'noPartida'},
                                            {name: 'idPropuesta'},
                                            {name: 'idPartida'},
                                            {name: 'grupo'},
                                            {name: 'almacen'},
                                            {name: 'producto'},
                                            {name: 'costo'},
                                            {name: 'costoBase'},
                                            {name: 'situacionCosto'}
                                            
                                ]);
   
    var nReg=new registroGrid	(
                                    {
                                        noPartida:'',
                                        idPropuesta:-1,
                                        idPartida:-1,
                                        grupo:'',
                                        almacen:'',
                                        producto:'',
                                        costo:0,
                                        costoBase:0,
                                        situacion:''
                                    }
                                )
    
    editorFila.stopEditing();
    tblGrid.getStore().add(nReg);
    tblGrid.nuevoRegistro=true;
    editorFila.startEditing(tblGrid.getStore().getCount()-1);	
    Ext.getCmp('btnAgregar').disable();
    Ext.getCmp('btnRemover').disable();	
    Ext.getCmp('btnCerrar').disable();	
    gEx('editor_noPartida').focus(false,500);
}


function funcEditorAfterEditPropuesta(rowEdit,obj,registro,nFila)                                               
{
	nIngreso=0;
}

function funcEditorBeforeEditPropuesta(rowEdit,fila)
{
	nIngreso=0;
	var grid=Ext.getCmp('gridProveedores');
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
    if(grid.getStore().getAt(fila).get('idPropuesta')!='-1')
    	gEx('editor_noPartida').disable();
    else
    	gEx('editor_noPartida').enable();

	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaPropuesta(rowEdit,obj,registro,nFila)
{
	nIngreso++;
    if(nIngreso>1)
    	return;
   
	var grid=Ext.getCmp('gridProveedores');
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
    var pos;
   if(registro.get('idPropuesta')=='-1')
   {
       pos=obtenerPosFila(grid.getStore(),'noPartida',obj.noPartida);
       if(pos!=-1)
       {
            msgBox("El n&uacute;mero de partida ya ha sido ingresado anteriormente");
            return false;
       }
   }
   pos=existeValorMatriz(arrPartidas,obj.noPartida+'');
   if(pos==-1)
   {
   		function funcResp2()
        {
        	gEx('editor_noPartida').focus(true);
        }
   		msgBox("El n&uacute;mero de partida ingresado no existe",funcResp2);
        return false;
   }
   
   	var cadObj='{"idPropuesta":"'+registro.get('idPropuesta')+'","idProveedor":"'+gEx('cmbProveedor').getValue()+'","idPartida":"'+arrPartidas[pos][4]+'","idMarca":"0","caracteristicas":"","costo":"'+obj.costo+'","idFormulario":"<?php echo $idFormulario?>","idRegistro":"<?php echo $idRegistro?>"}';
	
    function funcAjax()
    {
    	window.parent.mostrarMenuDTD();
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	  var costoBase=parseFloat(arrPartidas[pos][6]);
         	  registro.set('producto',arrPartidas[pos][1]);
              registro.set('almacen',arrPartidas[pos][3]);
              registro.set('grupo',arrPartidas[pos][2]);
              registro.set('idPropuesta',arrResp[1]);
              registro.set('idPartida',arrPartidas[pos][4]);
              registro.set('cve_Art',arrPartidas[pos][5]);
              registro.set('costoBase',costoBase);
              var situacion=0;
              var limite=costoBase-(costoBase*pocentajeLimite);
              if(parseFloat(obj.costo)<limite)
              	situacion='1';
              if((parseFloat(obj.costo)>=limite)&&(parseFloat(obj.costo)<=costoBase))
              	situacion='3';
              if(parseFloat(obj.costo)>costoBase)
              	situacion='2';
              
              registro.set('situacionCosto',situacion);
              grid.nuevoRegistro=false;
              habilitarBotones();
              function respAux(btn)
              {
                  if(btn=='yes')
                  {
                      agregarNuevaFila();
                  }
              }
              msgConfirm('Desea agregar otra partida a este proveedor?',respAux)

        }
        else
        {
        	var copiaRegistro=grid.copiaRegistro;
            var x=0;
            var arrCampos=grid.getStore().fields;
            var filaDestino=grid.registroEdit;
            for(x=0;x<arrCampos.items.length;x++)
            {
                filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
        
            }
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            grid.nuevoRegistro=false;
            habilitarBotones();
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=24&cadObj='+cadObj,true);
    return true;
}

function funcEditorCancelPropuesta()
{
	var grid=Ext.getCmp('gridProveedores');
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnAgregar').enable();
    Ext.getCmp('btnRemover').enable();	
    Ext.getCmp('btnCerrar').enable();	
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
    }
	grid.nuevoRegistro=false;
}

function mostrarVentanaRegistroPropuesta(fila)
{
	
	var cmbMarca=crearComboExt('cmbMarca',arrMarca,130,65,250);
    cmbMarca.setValue(fila.get('idMarca'));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Producto:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:10,
                                                            html:fila.get('producto')
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Marca:'
                                                        },
                                                        cmbMarca,
                                                        {
                                                        	x:390,
                                                            y:65,
                                                            html:'<a href="javascript:agregarMarca()"><img src="../images/add.png"></a>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Caracter&iacute;sticas:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:95,
                                                            xtype:'textarea',
                                                            id:'txtCaracteristicas',
                                                            value:fila.get('caracteristicas'),
                                                            width:420,
                                                            height:90
                                                        },
                                                        {
                                                        	x:10,
                                                            y:205,
                                                            html:'Costo:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:200,
                                                            id:'txtCosto',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            value:fila.get('costo'),
                                                            width:100
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar datos de partida',
										width: 600,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbMarca.getValue()=='')	
                                                                        {
                                                                        	msgBox('Debe indicar la marca ofrecida por el proveedor');
                                                                            return;
                                                                        }
                                                                        var txtCaracteristicas=gEx('txtCaracteristicas');
                                                                        if(txtCaracteristicas.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar las ofrecidas por el proveedor');
                                                                            return;
                                                                        }
                                                                        var txtCosto=gEx('txtCosto');
                                                                        if(txtCosto.getValue()=="")
                                                                        {
                                                                        	msgBox('Debe indicar el costo ofertado por el proveedor');
                                                                            return;
                                                                        }
                                                                        var cadObj='{"idProveedor":"'+gEx('cmbProveedor').getValue()+'","idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+
                                                                        			'","idPartida":"'+fila.get('idPartida')+'","idPropuesta":"'+fila.get('idPropuesta')+'","idMarca":"'+cmbMarca.getValue()+'","caracteristicas":"'+
                                                                        			cv(txtCaracteristicas.getValue())+'","costo":"'+txtCosto.getValue()+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProveedores').getStore().reload();
                                                                                window.parent.mostrarMenuDTD();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=24&cadObj='+cadObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function guardarMarcaNoParticipaProveedor(listaPartidas)
{
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('gridProveedores').getStore().reload();
            window.parent.mostrarMenuDTD();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=23&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&listaPartidas='+listaPartidas+'&idProveedor='+gEx('cmbProveedor').getValue(),true);
}

function agregarMarca()
{
	var obj={};
    obj.ancho=600,
    obj.height=400,
    obj.url='../modeloPerfiles/registroFormulario.php';
    obj.titulo='Agregar marca';
    var params=[['idFormulario','406'],['idRegistro','-1'],['cPagina','sFrm=true'],['funcPHPEjecutarNuevo',bE('asignarDatosMarca(idRegPadre,"cmbMarca")')]];
    obj.params=params;
	abrirVentanaFancy(obj);
}

function asignarMarca(idMarca,arrMarcas)
{
	arrMarca=arrMarcas;
	gEx('cmbMarca').getStore().loadData(arrMarcas);
    gEx('cmbMarca').setValue(idMarca);
    cerrarVentanaFancy();
}

function inhabilitarBotones()
{
    if(Ext.getCmp('btnAgregar')!=null)
    {
    	Ext.getCmp('btnAgregar').disable();
        Ext.getCmp('btnRemover').disable();	
        Ext.getCmp('btnCerrar').disable();	
        gEx('gridProveedores').soloLectura=true;
    }
    
}

function habilitarBotones()
{
	if(Ext.getCmp('btnAgregar')!=null)
    {
    	Ext.getCmp('btnAgregar').enable();
        Ext.getCmp('btnRemover').enable();	
        Ext.getCmp('btnCerrar').enable();	
        gEx('gridProveedores').soloLectura=false;
    }
}