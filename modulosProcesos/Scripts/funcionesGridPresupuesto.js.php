<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	$consulta="SELECT  ciclo FROM `550_cicloFiscal` WHERE STATUS=1";
	$ciclo=$con->obtenerValor($consulta);
	
	$consulta="	SELECT id__580_tablaDinamica,CONCAT('[',txtClave,'] ',txtEstudio) AS estudio,(SELECT CostoTotal FROM _578_tablaDinamica WHERE  id__578_tablaDinamica=t.id__580_tablaDinamica AND cmbCiclo=".$ciclo."  ) AS 'costo' FROM `_580_tablaDinamica` t WHERE 
				id__580_tablaDinamica IN (SELECT cmbServicios FROM `_578_tablaDinamica` WHERE cmbCiclo=".$ciclo." UNION SELECT concepto FROM `100_gridPresupuesto` 
				WHERE idRubro=11 AND idReferencia=".$idRegistro." AND idFormulario=".$idFormulario." AND eliminado=0)";	
	$arrOpciones=$con->obtenerFilasArreglo($consulta);
	
	
	

	
?>	

function crearGridServicio(arregloG,titulo,idRubro,montoMaximo)
{
	var arrServicios=<?php echo $arrOpciones?>;
	var cmbServicio=crearComboExt('cmbServicio_'+idRubro,arrServicios);
    
	var arrDatos=arregloG;
    var iR=idRubro;
    var dSetGenerico= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idGridVSCalculo'},
                                                                    {name:'calculo'},
                                                                    {name:'costoUnitario'},
                                                                    {name:'cantidad'},
                                                                    {name: 'total'},
                                                                    {name:'idRubro'},
                                                                    {name: 'montoAutorizado'}
                                                                ]
                                                    }
                                                 )
    
    dSetGenerico.loadData(arrDatos);	
    var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
    var editorFila=new Ext.ux.grid.RowEditor	(
                                                    {
                                                        id:'editor_'+idRubro,
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
    editorFila.on('beforeedit',funcEditorFilaBeforeEditGridCalculo)
    editorFila.on('validateedit',funcEditorValidaGridCalculo);
    editorFila.on('canceledit',funcEditorCancelEditGridCalculo);
     var summary = new Ext.ux.grid.GridSummary();
    var cmGenerico= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            columnaCheck,
                                                            {
                                                                header:titulo,
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'calculo',
                                                                editor:cmbServicio,
                                                                renderer:function(val)
                                                                			{
                                                                            	return formatearValorRenderer(arrServicios,val);
                                                                            }
                                                            },
                                                            {
                                                                header:'Presupuesto',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'costoUnitario',
                                                                editor:{id:'txtPresupuesto_'+idRubro,xtype:'numberfield',disabled:true},
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
                                                                
                                                                editor:{xtype:'numberfield',allowDecimals:false},
                                                                renderer:function(val,meta,registro)
                                                                        {
                                                                        	var cantidadR=registro.data.cantidad;
                                                                            cantidadR=parseFloat(cantidadR);
                                                                            var costoU=registro.data.costoUnitario;
                                                                            costoU=parseFloat(costoU);
                                                                            var total=cantidadR*costoU;
                                                                            registro.data.total=total;
                                                                            return Ext.util.Format.number(val,'0,0.00');
                                                                        }
                                                            },
                                                            {
                                                                header:'Total',
                                                                width:100,
                                                                summaryType:'sum',
                                                                sortable:true,
                                                                dataIndex:'total',
                                                                renderer:'usMoney'
                                                               
                                                            },
                                                            <?php
															//if($_SESSION["idUsr"]==1)
															{
															?>
																{
																	header:'Instituci&oacute;n patrocinadora',
																	width:300,
																	sortable:true,
                                                                    hidden:true,
																	renderer: function(val,meta,registro)
																				{
																				   
																				}
																   
																},
															<?php
															}
															?>
                                                             {
                                                                header:'Monto autorizado',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'montoAutorizado',
                                                                hidden:true,
                                                                renderer:function(val,meta,registro)
                                                                        {
                                                                            var cantidadR=registro.get('cantidad');
                                                                            cantidadR=parseFloat(cantidadR);
                                                                            var costoU=registro.get('costoUnitario');
                                                                            costoU=parseFloat(costoU);
                                                                            var total=cantidadR*costoU;
                                                                            if(parseFloat(val)==total)
                                                                            {
                                                                                return '<font color="#005500">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                            }
                                                                            else
                                                                                return '<font color="#B0281A">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                        }
                                                            }
                                                        ]
                                                    );
                                            
                                                
    tblGrid=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                        id:'gridCalculo_'+idRubro,
                                                        store:dSetGenerico,
                                                        frame:true,
                                                        cm: cmGenerico,
                                                        sm:columnaCheck,
                                                        height:293,
                                                        width:850,
                                                        stripeRows :true,
                                                        plugins:[editorFila,summary],
                                                        tbar:[
                                                                {
                                                                    id:'btnAdd_'+idRubro,
                                                                    text:'Agregar '+titulo,
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    hidden:ocultarBotones,
                                                                    handler:function()
                                                                            {
                                                                                
                                                                                var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                
                                                                                var editorFila=gEx('editor_'+idRubro);
                                                                                var registroGrid=crearRegistro([
                                                                                                                {name:'idGridVSCalculo'},
                                                                                                                {name:'calculo'},
                                                                                                                {name:'costoUnitario'},
                                                                                                                {name:'cantidad'},
                                                                                                                {name:'total'},
                                                                                                                {name:'idRubro'},
                                                                                                                {name: 'montoAutorizado'}
                                                                                                            ]);
                                                                               
                                                                                
                                                                                var nReg=new registroGrid	(
                                                                                                                {
                                                                                                                    idGridVSCalculo:'-1',
                                                                                                                    calculo:'',
                                                                                                                    costoUnitario:'',
                                                                                                                    cantidad:'',
                                                                                                                    total:'0',
                                                                                                                    idRubro:iR,
                                                                                                                    montoAutorizado:'0'
                                                                                                                }
                                                                                                            )
                                                                                
                                                                                editorFila.stopEditing();
                                                                                tblGrid.getStore().add(nReg);
                                                                                tblGrid.nuevoRegistro=true;
                                                                                editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                Ext.getCmp('btnAdd_'+idRubro).disable();
                                                                                Ext.getCmp('btnDel_'+idRubro).disable();																	
                                                                            }
                                                                },'-',
                                                                {
                                                                    id:'btnDel_'+idRubro,
                                                                    text:'Remover '+titulo,
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    hidden:ocultarBotones,
                                                                    handler:function()
                                                                            {
                                                                                
                                                                                var tblGrid=gEx('gridCalculo_'+idRubro);
                                                                                var fila=tblGrid.getSelectionModel().getSelections();
                                                                                var tamano=fila.length;
                                                                                if(tamano==0)
                                                                                {
                                                                                    msgBox('Primero debe seleccionar el elemento a eliminar');
                                                                                    return;	
                                                                                }
                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                    if(btn=='yes')
                                                                                    {
                                                                                        var cadena='';
                                                                                        var y;
                                                                                        for(y=0;y< tamano;y++)
                                                                                        {
                                                                                            var idFila=fila[y].get('idGridVSCalculo');
                                                                                            if(cadena=='')
                                                                                                cadena=idFila;
                                                                                            else
                                                                                                cadena+=','+idFila;    
                                                                                        }
                                                                                        
                                                                                         function funcAjax1()
                                                                                         {
                                                                                              var resp=peticion_http.responseText;
                                                                                              arrResp=resp.split('|');
                                                                                              if(arrResp[0]=='1') 
                                                                                              {
                                                                                                    sumarTotal();
                                                                                                    tblGrid.getStore().remove(fila);	
                                                                                                    refrescarMenuDTD();
                                                                                              }
                                                                                              else
                                                                                              {
                                                                                                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                              }
                                                                                         }
                                                                                         obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax1, 'POST','funcion=8&cadena='+cadena,true);
   
                                                                                        
                                                                                        
                                                                                    }
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer eliminar los conceptos seleccionados?',resp);
                                                                            }
                                                                }
                                                            ]
                                                    }
                                                );
    tblGrid.nuevoRegistro=false;  
    tblGrid.montoMaximo=montoMaximo;
    tblGrid.rubro=titulo;
    tblGrid.on('beforeedit',antesEditPresupuesto);
    cmbServicio.on('select',function(combo,registro)
    						{
                            	gEx('txtPresupuesto_'+idRubro).setValue(registro.get('valorComp'));
                            }
    				)
    return tblGrid;
	
}