<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");

?>	

var sL=false;
var arrPeriodos;
var plantel;
var nodoSel=false;
Ext.onReady(inicializar);

function inicializar()
{
	
    if(gE('sL').value=='1')
    	sL=true;
	plantel=gE('plantel').value;
	arrPeriodos=eval(bD(gE('arrPeriodos').value));
    var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodos,0,0,300);
    cmbPeriodo.on('select',function(cb,rtegistro)
    						{
                            	obtenerProgramasEducativosRegistrados();
                                gEx('arbolPlanes').getRootNode().reload();
/*                                gEx('arbolPlanes').expandAll();*/
                            }
    			)
    var cmbProgramaEducativo=crearComboExt('cmbProgramaEducativo',[],0,0,400);
    cmbProgramaEducativo.on('select',function(cb,rtegistro)
    						{
                            	 gEx('arbolPlanes').getRootNode().reload();
                                 /*gEx('arbolPlanes').expandAll();*/
                            }
    			)
	var gridPlanesEstudio=crearGridPlanesEstudio();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<span class="letraVino14N">Configuraci&oacute;n de Fechas de Inscripci&oacute;n</span>'
                                                        }
                                                    ],
                                            items:	[
                                                    
                                                        {
                                                            xtype:'panel',
                                                            region:'center',
                                                            layout:'border',
                                                            tbar:	[
                                                                        {
                                                                            xtype:'label',
                                                                            html:'<span class="letraVino12N">Periodo:</span>&nbsp;&nbsp;&nbsp;'
                                                                        },
                                                                        cmbPeriodo,
                                                                        {
                                                                            xtype:'label',
                                                                            hidden:sL,
                                                                            html:'&nbsp;&nbsp;<a href="javascript:agregarPeriodo()"><img src="../images/add.png" title="Agregar Periodo" alt="Agregar Periodo"></a>'
                                                                        },
                                                                        
                                                                        '-',
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span class="letraVino12N">Programa Educativo:</span>&nbsp;&nbsp;&nbsp;'
                                                                        },
                                                                        
                                                                        cmbProgramaEducativo,
                                                                        {
                                                                            xtype:'label',
                                                                            hidden:sL,
                                                                            html:'&nbsp;&nbsp;<a href="javascript:agregarProgramaEducativo()"><img src="../images/add.png" title="Agregar Programa Educativo" alt="Agregar Periodo"></a>'
                                                                        }
                                                                    ],
                                                            items:	[
                                                                        gridPlanesEstudio
                                                                    ]
                                                        }
                                    				 ]
										}
									]                                        
                                                     
						}
                    )   
}


function crearGridPlanesEstudio()
{
	var iPeriodo=-1;
    var cmbPeriodo=gEx('cmbPeriodo');
    if(cmbPeriodo.getValue()!='')
    {
    	iPeriodo=cmbPeriodo.getValue();
    }
    var iPrograma=-1;
    var cmbProgramaEducativo=gEx('cmbProgramaEducativo');
    if(cmbProgramaEducativo.getValue()!='')
    {
    	iPrograma=cmbProgramaEducativo.getValue();
    }
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
		var cargadorArbol=new Ext.tree.TreeLoader(
                                                        {
                                                            baseParams:{
                                                                            funcion:'100'
                                                                            
                                                                            
                                                                        },
                                                            dataUrl:'../paginasFunciones/funcionesPlanteles.php'
                                                        }	


		                                         )		                                        
       
       
       cargadorArbol.on('beforeload',function(proxy)
       								{
                                   	    gEx('btnPlanReceptor').disable();
                                        nodoSel=null;
                                    
                                    	var iPeriodo=-1;
                                        var cmbPeriodo=gEx('cmbPeriodo');
                                        if(cmbPeriodo.getValue()!='')
                                        {
                                            iPeriodo=cmbPeriodo.getValue();
                                        }
                                        var iPrograma=-1;
                                        var cmbProgramaEducativo=gEx('cmbProgramaEducativo');
                                        if(cmbProgramaEducativo.getValue()!='')
                                        {
                                            iPrograma=cmbProgramaEducativo.getValue();
                                        }
                                    	proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
                                        proxy.baseParams.idPeriodo=iPeriodo;
                                        proxy.baseParams.idPrograma=iPrograma;
                                        if(sL)
                                        	proxy.baseParams.sL=1;
                                        else
                                      		proxy.baseParams.sL=0;
                                    }
       					)
                                
                                      
                                        
		var arbol = new Ext.ux.tree.TreeGrid	(
                                                            {
                                                                id:'arbolPlanes',
                                                                useArrows:true,
                                                                autoScroll:false,
                                                                animate:true,
                                                                enableDD:true,
                                                                containerScroll: true,
                                                                root:raiz,
                                                                region:'center',
                                                                border:false,
                                                                loader: cargadorArbol,
                                                                rootVisible:false,
                                                                draggable:false,
                                                                enableSort : false,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:sL,
                                                                                text:'Agregar Plan de Estudio',
                                                                                handler:function()
                                                                                        {
                                                                                         	agregarPlanEstudios();   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:sL,
                                                                                text:'Remover Plan de Estudio',
                                                                                handler:function()
                                                                                        {
                                                                                         	var listaPlanEstudio='';   
                                                                                            var arrRaiz=gEN('chkRaiz');
                                                                                            var arrGrados;
                                                                                            var x;
                                                                                            var aux;
                                                                                            for(x=0;x<arrRaiz.length;x++)
                                                                                            {
                                                                                            	if(arrRaiz[x].checked)
                                                                                                {
                                                                                                	if(listaPlanEstudio=='')
                                                                                                    	listaPlanEstudio=arrRaiz[x].getAttribute('idRegistro');
                                                                                                    else
                                                                                                    	listaPlanEstudio+=','+arrRaiz[x].getAttribute('idRegistro');
                                                                                                }
                                                                                            }
                                                                                            
                                                                                            if(listaPlanEstudio=='')
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el Plan de Estudios que desea remover de la presente convocatoria');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        gEx('arbolPlanes').getRootNode().reload();
																		                                 /*gEx('arbolPlanes').expandAll();*/
                                                                                                         refrescarMenuDTD();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=103&listaPlanEstudio='+listaPlanEstudio,true);

                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el plan de estudios selccionado?',resp);
                                                                                            
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/calendar.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:sL,
                                                                                text:'Modificar fechas',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaAgregarFecha ();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/icon_big_tick.gif',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:sL,
                                                                                text:'Activar/Desactivar Fechas Grado',
                                                                                handler:function()
                                                                                        {
                                                                                         	var listGrados=obtenerListaGrados();
                                                                                            if(listGrados=='')
                                                                                            {
                                                                                                msgBox('Debe seleccionar el grado cuyas fechas desea Activar/Desactivar');
                                                                                                return;
                                                                                            }
                                                                                            
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                    function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                             gEx('arbolPlanes').getRootNode().reload();
																			                                 /*gEx('arbolPlanes').expandAll();*/
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=102&lista='+listGrados,true);
                                                                                                    
                                                                                                }
                                                                                                
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer Activar/Desactivar los grados seleccionados?',resp);
                                                                                            
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/user_edit.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:sL,
                                                                                disabled:true,
                                                                                id:'btnPlanReceptor',
                                                                                text:'Designar Plan de Estudios Receptor de Nuevos Ingresos',
                                                                                handler:function()
                                                                                        {
                                                                                         	asignarPlanEstudioReceptor();
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                columns:[
                                                                			
                                                                            {
                                                                                header:'',
                                                                                width:470,
                                                                                dataIndex:'text'
                                                                            },
                                                                            {
                                                                                header:'Fecha de Inicio<br>de Inscripci&oacute;n',
                                                                                width:100,
                                                                                menuDisabled :true,
                                                                                align:'center',
                                                                                css:'text-align:right !important;',
                                                                                sortable:false,
                                                                                renderer:formatearSoloFecha,
                                                                                dataIndex:'fechaInicioInscripcion'
                                                                            },
                                                                            {
                                                                                header:'Fecha de T&eacute;rmino<br>de Inscripci&oacute;n',
                                                                                width:100,
                                                                                menuDisabled :true,
                                                                                sortable:false,
                                                                                align:'center',
                                                                                renderer:formatearSoloFecha,
                                                                                css:'text-align:right !important;',
                                                                                dataIndex:'fechaTerminoInscripcion'
                                                                            },
                                                                            {
                                                                                header:'Fecha de Inicio<br>de ReInscripci&oacute;n',
                                                                                width:100,
                                                                                menuDisabled :true,
                                                                                sortable:false,
                                                                                align:'center',
                                                                                renderer:formatearSoloFecha,
                                                                                css:'text-align:right !important;',
                                                                                dataIndex:'fechaInicioResincripcion'
                                                                            },
                                                                            {
                                                                                header:'Fecha de T&eacute;rmino<br>de ReInscripci&oacute;n',
                                                                                width:100,
                                                                                menuDisabled :true,
                                                                                align:'center',
                                                                                renderer:formatearSoloFecha,
                                                                                css:'text-align:right !important;',
                                                                                sortable:false,
                                                                                dataIndex:'fechaTerminoRenscripcion'
                                                                            }/*,
                                                                            {
                                                                                header:'Situaci&oacute;n',
                                                                                width:140,
                                                                                menuDisabled :true,
                                                                                align:'center',
                                                                                
                                                                                css:'text-align:left !important;',
                                                                                sortable:false,
                                                                                dataIndex:'situacion'
                                                                            }*/
                                                                         ]
                                                               
                                                            }
                                                    );
        arbol.on('click',nodoClick);
        /*arbol.expandAll();    	*/
        return arbol;
}

function nodoClick(nodo)
{
	nodoSel=nodo;
    gEx('btnPlanReceptor').disable();
    switch(nodoSel.attributes.tipo)
    {
    	case '-1':
        	gEx('btnPlanReceptor').enable();
        break;
    }
    
}


function agregarPeriodo()
{
	var arrTipoPeriodo=eval(bD(gE('arrTiposPeriodos').value));
	var cmbTipoPeriodo=crearComboExt('cmbTipoPeriodo',arrTipoPeriodo,120,5,250);
    cmbTipoPeriodo.on('select',function(cmb,registro)
    							{
                                	gEx('gridPeriodos').getStore().reload();
                                    
                                }
    				)
    var gridPeriodos=crearGridPeriodos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Tipo de periodo:'
                                                        },
                                                        cmbTipoPeriodo,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'Seleccione los periodos a considerar en la presente convocatoria:'
                                                        },
                                                        gridPeriodos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar periodo',
										width: 500,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filas=gridPeriodos.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Almenos debe seleccionar un periodo a agregar');
                                                                            retorn
                                                                        }
                                                                        
                                                                        var listPeriodos='';
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(listPeriodos=='')
                                                                            	listPeriodos=filas[x].get('idPeriodo');
                                                                            else
                                                                            	listPeriodos+=','+filas[x].get('idPeriodo');
                                                                        }
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	var arrDatos=eval(arrResp[1]);
                                                                                gEx('cmbPeriodo').getStore().loadData(arrDatos);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=94&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&listPeriodos='+listPeriodos,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();		
}

function crearGridPeriodos()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPeriodo'},
		                                                {name: 'nombrePeriodo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombrePeriodo', direction: 'ASC'},
                                                            groupField: 'nombrePeriodo',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='93';
                                        var tipoPeriodo=-1;
                                        if(gEx('cmbTipoPeriodo').getValue()!='')
                                        	tipoPeriodo=gEx('cmbTipoPeriodo').getValue();
                                        proxy.baseParams.tipoPeriodo=tipoPeriodo;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idReferencia=gE('idRegistro').value;
                                        
                                    }
                        )   
       
       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Periodo',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'nombrePeriodo'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridPeriodos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                sm:chkRow,
                                                                x:10,
                                                                y:70,
                                                                height:200,
                                                                width:450,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function agregarProgramaEducativo()
{
	var idPeriodo=gEx('cmbPeriodo').getValue();
    
    if(idPeriodo=='')
    {
    	msgBox('Primero debe seleccionar el periodo al cual desea agregar el programa educativo');
    	return;
    }
    
  	
    var gridProgramasEducativos=crearGridProgramasEducativos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Seleccione los programas educativos que desea agregar a la presente convocatoria:'
                                                        },
                                                        gridProgramasEducativos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar programas educativos',
										width: 650,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filas=gridProgramasEducativos.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Almenos debe seleccionar un programa educativo a agregar');
                                                                            retorn
                                                                        }
                                                                        
                                                                        var lista='';
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(lista=='')
                                                                            	lista=filas[x].get('idProgramaEducativo');
                                                                            else
                                                                            	lista+=','+filas[x].get('idProgramaEducativo');
                                                                        }
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	obtenerProgramasEducativosRegistrados();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=96&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&lista='+lista+'&idPeriodo='+idPeriodo,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();		
}  
    
function crearGridProgramasEducativos()
{
	var idPeriodo=gEx('cmbPeriodo').getValue();
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProgramaEducativo'},
		                                                {name: 'nombrePrograma'},
                                                        {name: 'gradoAcademico'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'gradoAcademico', direction: 'ASC'},
                                                            groupField: 'gradoAcademico',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='95';
                                        proxy.baseParams.idPeriodo=idPeriodo;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idReferencia=gE('idRegistro').value;
                                        proxy.baseParams.plantel=gE('plantel').value;
                                        
                                        
                                        
                                    }
                        )   
       
       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Nivel acad&eacute;mico',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'gradoAcademico'
                                                            },
                                                            {
                                                                header:'Programa acad&eacute;mico',
                                                                width:470,
                                                                sortable:true,
                                                                dataIndex:'nombrePrograma'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridProgramasAcademicos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                sm:chkRow,
                                                                x:10,
                                                                y:40,
                                                                height:400,
                                                                width:600,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function obtenerProgramasEducativosRegistrados()
{
	var idPeriodo=gEx('cmbPeriodo').getValue();
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	gEx('cmbProgramaEducativo').reset();
            gEx('cmbProgramaEducativo').getStore().loadData(eval(arrResp[1]));
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=97&idPeriodo='+idPeriodo+'&idFormulario='+gE('idFormulario').value+'&idReferencia='+gE('idRegistro').value,true);
    
}

function agregarPlanEstudios()
{
	var idPeriodo=gEx('cmbPeriodo').getValue();
    
    if(idPeriodo=='')
    {
    	msgBox('Primero debe seleccionar el periodo al cual desea agregar el plan de estudios');
    	return;
    }
    var idPrograma=gEx('cmbProgramaEducativo').getValue();
    
    if(idPrograma=='')
    {
    	msgBox('Primero debe seleccionar el programa educativo al cual pertenece el plan de estudios que desea agregar');
    	return;
    }
  	
    var grid=crearGridPlanEstudios();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Seleccione los planes de estudio que desea agregar a la presente convocatoria:'
                                                        },
                                                        grid

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar plan de estudios',
										width: 800,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filas=grid.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Almenos debe seleccionar un plan de estudios a agregar');
                                                                            retorn
                                                                        }
                                                                        
                                                                        var lista='';
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(lista=='')
                                                                            	lista=filas[x].get('idInstanciaPlan');
                                                                            else
                                                                            	lista+=','+filas[x].get('idInstanciaPlan');
                                                                        }
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('arbolPlanes').getRootNode().reload();
                                                                                refrescarMenuDTD();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=99&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&lista='+lista+'&idPeriodo='+idPeriodo,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();		
}  
    
function crearGridPlanEstudios()
{
	var idPeriodo=gEx('cmbPeriodo').getValue();
    var idPrograma=gEx('cmbProgramaEducativo').getValue();
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idInstanciaPlan'},
		                                                {name: 'nombrePlanEstudios'},
                                                        {name: 'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombrePlanEstudios', direction: 'ASC'},
                                                            groupField: 'nombrePlanEstudios',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='98';
                                        proxy.baseParams.idPrograma=idPrograma;
                                        proxy.baseParams.idPeriodo=idPeriodo;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idReferencia=gE('idRegistro').value;
                                        proxy.baseParams.plantel=gE('plantel').value;
                                        
                                        
                                        
                                    }
                        )   
       
       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Plan de estudios',
                                                                width:500,
                                                                sortable:true,
                                                                dataIndex:'nombrePlanEstudios',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Descripcion',
                                                                width:470,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                renderer:mostrarValorDescripcion
                                                            }
                                                        ]
                                                    );
                                                    
    var tblGrid=	new Ext.grid.GridPanel	(
                                              {
                                                  id:'gridPlanEstudios',
                                                  store:alDatos,
                                                  region:'center',
                                                  frame:true,
                                                  cm: cModelo,
                                                  stripeRows :true,
                                                  loadMask:true,
                                                  columnLines : true,
                                                  sm:chkRow,
                                                  x:10,
                                                  y:40,
                                                  height:400,
                                                  width:750,
                                                  view:new Ext.grid.GroupingView({
                                                                                      forceFit:false,
                                                                                      showGroupName: false,
                                                                                      enableGrouping :false,
                                                                                      enableNoGroups:false,
                                                                                      enableGroupingMenu:false,
                                                                                      hideGroupedColumn: false,
                                                                                      startCollapsed:false
                                                                                  })
                                              }
                                          );
        return 	tblGrid;	
}

function mostrarVentanaAgregarFecha ()
{
	var listGrados=obtenerListaGrados();
    if(listGrados=='')
    {
    	msgBox('Debe seleccionar el grado cuyas fechas desea modificar');
    	return;
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'fieldset',
                                                            title:'Fechas de Inscripci&oacute;n',
                                                            width:480,
                                                            height:80,
                                                            layout:'absolute',
                                                            items:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'Fecha de inicio:<span class="letraRoja">*</span>',
                                                                            x:10,
                                                                            y:10
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            x:100,
                                                                            id:'fechaInicioIns',
                                                                            y:5
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'Fecha de t&eacute;rmino:<span class="letraRoja">*</span>',
                                                                            x:230,
                                                                            y:10
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            x:330,
                                                                            id:'fechaFinIns',
                                                                            y:5
                                                                        }
                                                            		]
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'fieldset',
                                                            title:'Fechas de Reinscripci&oacute;n',
                                                            width:480,
                                                            layout:'absolute',
                                                            height:80,
                                                            items:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'Fecha de inicio:<span class="letraRoja">*</span>',
                                                                            x:10,
                                                                            y:10
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            x:100,
                                                                            id:'fechaInicioReIns',
                                                                            y:5
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'Fecha de t&eacute;rmino:<span class="letraRoja">*</span>',
                                                                            x:230,
                                                                            y:10
                                                                        },
                                                                        {
                                                                        	xtype:'datefield',
                                                                            x:330,
                                                                            id:'fechaFinReIns',
                                                                            y:5
                                                                        }
                                                            		]
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Asignar fechas',
										width: 530,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var fechaInicioIns=gEx('fechaInicioIns');
                                                                        var fechaFinIns=gEx('fechaFinIns');
                                                                        var fechaInicioReIns=gEx('fechaInicioReIns');
                                                                        var fechaFinReIns=gEx('fechaFinReIns');
                                                                        
                                                                        if(fechaInicioIns.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	fechaInicioIns.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de inicio de la inscripci&oacute;n',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(fechaFinIns.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	fechaFinIns.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de t&eacute;rmino de inscripci&oacute;n',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(fechaInicioIns.getValue()>fechaFinIns.getValue())
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	fechaInicioIns.focus();
                                                                            }
                                                                             msgBox('La fecha de inicio de inscripci&oacute;n no puede ser mayor que la fecha de t&eacute;rmino',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(fechaInicioReIns.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	fechaInicioReIns.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de inicio de la Reinscripci&oacute;n',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(fechaFinReIns.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	fechaFinReIns.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de t&eacute;rmino de la Reinscripci&oacute;n',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(fechaInicioReIns.getValue()>fechaFinReIns.getValue())
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	fechaInicioReIns.focus();
                                                                            }
                                                                             msgBox('La fecha de inicio de Reinscripci&oacute;n no puede ser mayor que la fecha de t&eacute;rmino',resp6);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        var cadObj='{"arrGrados":"'+listGrados+'","fechaInicioIns":"'+fechaInicioIns.getValue().format('Y-m-d')+'","fechaFinIns":"'+fechaFinIns.getValue().format('Y-m-d')+
                                                                        			'","fechaInicioReIns":"'+fechaInicioReIns.getValue().format('Y-m-d')+'","fechaFinReIns":"'+fechaFinReIns.getValue().format('Y-m-d')+'"}';
                                                                                    
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('arbolPlanes').getRootNode().reload();
												                                /*gEx('arbolPlanes').expandAll();*/
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=101&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function planClick(chk)
{
	var arrCheck=gEN('chk_'+chk.id);
    var x;
    for(x=0;x<arrCheck.length;x++)
    {
    	arrCheck[x].checked=chk.checked;
    }
}


function obtenerListaGrados()
{
	var lista='';
	var arrRaiz=gEN('chkRaiz');
    var arrGrados;
    var x;
    var aux;
    for(x=0;x<arrRaiz.length;x++)
    {
    	arrGrados=gEN('chk_'+arrRaiz[x].id);
        for(aux=0;aux<arrGrados.length;aux++)
        {
        	if(arrGrados[aux].checked)
            {
	            if(lista=='')
                	lista=arrGrados[aux].id;
                else
                	lista+=','+arrGrados[aux].id;
            }
        }
        
    }
    
    return lista;
}

function refrescarMenuDTD()
{
	window.parent.mostrarMenuDTD()
}


function asignarPlanEstudioReceptor()
{
	var arrInstanciasPlan=[['-1','NINGUNO']];
    var x;
    var nodo;
    var arrDatos;
    for(x=0;x<nodoSel.childNodes.length;x++)
    {
    	nodo=nodoSel.childNodes[x];
        arrDatos=nodo.id.split('_');
        arrInstanciasPlan.push([arrDatos[3],nodo.attributes.nombrePlan]);
    }
    
	var cmbInstanciasPlanEstudios=crearComboExt('cmbInstanciasPlanEstudios',arrInstanciasPlan,10,40,450);
    cmbInstanciasPlanEstudios.setValue(nodoSel.attributes.inscribeNuevoIngreso);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Especifique el Plan de Estudios al cual se inscribir&aacute;n los Alumnos de Nuevo Ingreso:'
                                                        },
                                                        cmbInstanciasPlanEstudios

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Designaci&oacute;n de Plan de Estudios Receptor de Nuevos Ingresos',
										width: 500,
										height:160,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbInstanciasPlanEstudios.getValue()=='')
                                                                        {
                                                                        	function resp(btn)
                                                                            {
                                                                            	cmbInstanciasPlanEstudios.focus();
                                                                            }
                                                                            msgBox('Debe indicar el Plan de Estudios al cual se inscribir&aacute;n los Alumnos de Nuevo Ingreso',resp);
                                                                            return;
                                                                        }	
                                                                        var listInstancias='';
                                                                        var x;
                                                                        for(x=1;x<arrInstanciasPlan.length;x++)
                                                                        {
                                                                        	if(listInstancias=='')
                                                                            	listInstancias=arrInstanciasPlan[x][0];
                                                                            else
                                                                            	listInstancias+=','+arrInstanciasPlan[x][0];
                                                                        }
                                                                        var cadObj='{"restoInstancias":"'+listInstancias+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+'","idInstanciaPlan":"'+cmbInstanciasPlanEstudios.getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                               	                                gEx('arbolPlanes').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=107&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}