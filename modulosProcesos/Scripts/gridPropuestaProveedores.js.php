<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$sL=false;
	if(isset($_GET["sL"]))
		$sL=true;
		
	$consulta="SELECT id__406_tablaDinamica,descripcion FROM _406_tablaDinamica ORDER BY descripcion";
	$arrMarca=$con->obtenerFilasArreglo($consulta);	
?>
var arrMarca=<?php echo $arrMarca?>;
var arrSituacion=[['-1','Sin propuesta'],['0','No participa'],['1','Con propuesta']];


Ext.onReady(inicializar);

function inicializar()
{
   var grid=crearProveedoresPropuesta();
   new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			grid
                                            			
                                            		]
                                        }
                                     ]
						}
                    )   
}

function crearProveedoresPropuesta()
{
	var idFormulario=gE('idFormulario').value;
    var idRegistro=gE('idRegistro').value;
    var arrProv=eval(eval(bD(gE('arrProv').value)))
   	var cmbProveedor=crearComboExt('cmbProveedor',arrProv,0,0,350);
    cmbProveedor.on('select',function(combo,registro)
    						{
                            	if((registro.get('valorComp')!='')&&(registro.get('valorComp')!='0'))
                                	inhabilitarBotones();
                                else
                                	habilitarBotones();
                            	gEx('gridProveedores').getStore().reload();
                            }
    				)
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idPartida',
                                            fields: [
                                            			{name: 'situacion'},
                                               			{name: 'idPropuesta'},
                                                        {name: 'idPartida'},
                                                        {name: 'grupo'},
                                                        {name: 'almacen'},
                                                        {name: 'producto'},
                                                        {name: 'idMarca'},
                                                        {name: 'caracteristicas'},
                                                        {name: 'costo'}
                                            ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesCompras.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'producto', direction: 'ASC'},
                                                            groupField: 'producto',
                                                            autoLoad:false
                                                        })                                      
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=22;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
	                                    proxy.baseParams.idProveedor=cmbProveedor.getValue();
                                    }
                        )
	var chkRow=new Ext.grid.CheckboxSelectionModel();
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                         {
															header:'Situaci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'situacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='';
                                                                        switch(val)
                                                                        {
                                                                        	case '-1':
                                                                            	color='#F60';
                                                                            break;
                                                                            case '0':
                                                                            	color="#900";
                                                                            break;
                                                                            case '1':
                                                                            	color="#009";
                                                                            break;
                                                                        }
                                                                    	return '<font color="'+color+'">'+formatearValorRenderer(arrSituacion,val)+'</font>';
                                                                    }
														},
                                                        {
															header:'Almac&eacute;n',
															width:100,
															sortable:true,
															dataIndex:'almacen'
														},
                                                        {
															header:'Grupo',
															width:100,
															sortable:true,
															dataIndex:'grupo'
														},
                                                        {
															header:'Producto',
															width:250,
															sortable:true,
															dataIndex:'producto'
														},
                                                         {
															header:'Marca',
															width:150,
															sortable:true,
															dataIndex:'idMarca',
                                                            renderer:function (val)
                                                            		{
                                                                    	return formatearValorRenderer(arrMarca,val);
                                                                    }
														},
                                                        {
															header:'Caracter&iacute;sticas',
															width:350,
															sortable:true,
															dataIndex:'caracteristicas'
														},
                                                        {
															header:'Costo',
															width:150,
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney'
														}
													]
												);
                                               
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            title:'<span style="font-size:13px; font-weight:bold; color:#900;">Proveedores participantes</span>',
                                                            frame:false,
                                                            border:false,
                                                            region:'center',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            id:'gridProveedores',
                                                            tbar:	[
                                                            			{
                                                                        	html:'<span class="letraRojaSubrayada8"><b>Proveedor:</b></span>&nbsp&nbsp;'
                                                                        },
                                                                        cmbProveedor
                                                            		
                                                            <?php
																if(!$sL)
																{
															?>
                                                            			,{
                                                                        	text:'Modificar partida',
                                                                            id:'btnModificar',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                    if(fila==null)
                                                                                    {
	                                                                                    msgBox('Debe seleccionar la partida que desea modificar');
                                                                                    }
                                                                                	mostrarVentanaRegistroPropuesta(fila);
                                                                                }
                                                                        }
                                                                        ,'-',
                                                                        {
                                                                        	text:'Marcar como "No participa"',
                                                                            id:'btnMarcar',
                                                                            icon:'../images/cancel_round.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	var fila=tblGrid.getSelectionModel().getSelections();
                                                                                    if(fila.length==0)
                                                                                    {
                                                                                        msgBox('Debe seleccionar la partida que desea marcar como no participa');
                                                                                        return;
                                                                                    }
                                                                                    var listaPartidas="";
                                                                                	function resp(btn)
                                                                                    {
                                                                                    	var confirmar=false;
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	var x;
                                                                                            var obj;
                                                                                            for(x=0;x<fila.length;x++)
                                                                                            {
                                                                                            	
                                                                                            	obj='{"idPropuesta":"'+fila[x].get('idPropuesta')+'","idPartida":"'+fila[x].get('idPartida')+'"}';
                                                                                            	if(listaPartidas=='')
                                                                                                	listaPartidas=obj;
                                                                                                else
                                                                                                	listaPartidas+=','+obj;
                                                                                                if(fila[x].get('situacion')==1)
                                                                                                	confirmar=true;
                                                                                                
                                                                                            }
                                                                                            listaPartidas='['+listaPartidas+']';
                                                                                            if(!confirmar)
                                                                                            	guardarMarcaNoParticipaProveedor(listaPartidas);
                                                                                            else
                                                                                            {
                                                                                            	function resp2(btn)
                                                                                                {
                                                                                                	guardarMarcaNoParticipaProveedor(listaPartidas);
                                                                                                }
                                                                                                msgConfirm('Existen algunas partidas que tienen registrada una propuesta econ&oacute;mica, si las marca como "No participa" dicha propuesta ser&aacute; eliminada, desea continuar?',resp2);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer marcar las partida seleccionadas como "No participa"?',resp);
                                                                                    
                                                                                }
                                                                        },'-',
                                                                        {
                                                                        	text:'Cerrar registro de propuestas del proveedor',
                                                                            icon:'../images/lock.png',
                                                                            id:'btnCerrar',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:false,
                                                                            handler:function()
                                                                            	{
                                                                                	var cmbProveedor=gEx('cmbProveedor');
                                                                                    if(cmbProveedor.getValue()=='')
                                                                                    {
	                                                                                    msgBox('Debe seleccionar el proveedor cuyo registro de propuestas desea cerrar');
                                                                                        return;
                                                                                    }
                                                                                	var x;
                                                                                    var fila;
                                                                                    for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                    {
                                                                                    	fila=tblGrid.getStore().getAt(x);
                                                                                        if((fila.get('situacion')=='-1')||(fila.get('situacion')==-1))
                                                                                        {
                                                                                        	
                                                                                        	msgBox('Ninguna partida puede quedar marcada como "Sin propuesta"');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    var cmbProveedor=gEx('cmbProveedor');
                                                                                    var idProveedor=cmbProveedor.getValue();
                                                                                    var cadObj='{"idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+'","idProveedor":"'+idProveedor+'"}';
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                	var almacen=cmbProveedor.getStore();
                                                                                                	var pos=obtenerPosFila(almacen,'id',idProveedor);
                                                                                                    almacen.getAt(pos).set('valorComp','1');
                                                                                                    inhabilitarBotones();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=25&cadObj='+cadObj,true);
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer cerrar el registro de propuestas de este proveedor?',resp);
                                                                                }
                                                                        }
                                                            		
                                                            <?php
																}
															?>
                                                         			],
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: false,
                                                                                                     enableGrouping :false
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );
		
    
	return 	tblGrid;
}

function mostrarVentanaRegistroPropuesta(fila)
{
	
	var cmbMarca=crearComboExt('cmbMarca',arrMarca,130,65,250);
    cmbMarca.setValue(fila.get('idMarca'));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Producto:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:10,
                                                            html:fila.get('producto')
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Marca:'
                                                        },
                                                        cmbMarca,
                                                        {
                                                        	x:390,
                                                            y:65,
                                                            html:'<a href="javascript:agregarMarca()"><img src="../images/add.png"></a>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Caracter&iacute;sticas:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:95,
                                                            xtype:'textarea',
                                                            id:'txtCaracteristicas',
                                                            value:fila.get('caracteristicas'),
                                                            width:420,
                                                            height:90
                                                        },
                                                        {
                                                        	x:10,
                                                            y:205,
                                                            html:'Costo:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:200,
                                                            id:'txtCosto',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            value:fila.get('costo'),
                                                            width:100
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar datos de partida',
										width: 600,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbMarca.getValue()=='')	
                                                                        {
                                                                        	msgBox('Debe indicar la marca ofrecida por el proveedor');
                                                                            return;
                                                                        }
                                                                        var txtCaracteristicas=gEx('txtCaracteristicas');
                                                                        if(txtCaracteristicas.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe indicar las ofrecidas por el proveedor');
                                                                            return;
                                                                        }
                                                                        var txtCosto=gEx('txtCosto');
                                                                        if(txtCosto.getValue()=="")
                                                                        {
                                                                        	msgBox('Debe indicar el costo ofertado por el proveedor');
                                                                            return;
                                                                        }
                                                                        var cadObj='{"idProveedor":"'+gEx('cmbProveedor').getValue()+'","idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+
                                                                        			'","idPartida":"'+fila.get('idPartida')+'","idPropuesta":"'+fila.get('idPropuesta')+'","idMarca":"'+cmbMarca.getValue()+'","caracteristicas":"'+
                                                                        			cv(txtCaracteristicas.getValue())+'","costo":"'+txtCosto.getValue()+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridProveedores').getStore().reload();
                                                                                window.parent.mostrarMenuDTD();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=24&cadObj='+cadObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function guardarMarcaNoParticipaProveedor(listaPartidas)
{
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('gridProveedores').getStore().reload();
            window.parent.mostrarMenuDTD();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=23&idFormulario='+gE('idFormulario').value+'&idRegistro='+gE('idRegistro').value+'&listaPartidas='+listaPartidas+'&idProveedor='+gEx('cmbProveedor').getValue(),true);
}

function agregarMarca()
{
	var obj={};
    obj.ancho=600,
    obj.height=400,
    obj.url='../modeloPerfiles/registroFormulario.php';
    obj.titulo='Agregar marca';
    var params=[['idFormulario','406'],['idRegistro','-1'],['cPagina','sFrm=true'],['funcPHPEjecutarNuevo',bE('asignarDatosMarca(idRegPadre,"cmbMarca")')]];
    obj.params=params;
	abrirVentanaFancy(obj);
}

function asignarMarca(idMarca,arrMarcas)
{
	arrMarca=arrMarcas;
	gEx('cmbMarca').getStore().loadData(arrMarcas);
    gEx('cmbMarca').setValue(idMarca);
    cerrarVentanaFancy();
}

function inhabilitarBotones()
{
	var btnModificar=gEx('btnModificar');
    var btnMarcar=gEx('btnMarcar');
    var btnCerrar=gEx('btnCerrar');
    if(btnModificar!=null)
    {
    	btnModificar.disable();
        btnMarcar.disable();
        btnCerrar.disable();
    }
    
}

function habilitarBotones()
{
	var btnModificar=gEx('btnModificar');
    var btnMarcar=gEx('btnMarcar');
    var btnCerrar=gEx('btnCerrar');
    if(btnModificar!=null)
    {
    	btnModificar.enable();
        btnMarcar.enable();
        btnCerrar.enable();
    }
    
}