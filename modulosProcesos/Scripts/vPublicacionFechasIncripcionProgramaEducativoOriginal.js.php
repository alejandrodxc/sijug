<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");

?>	

var sL=false;
var arrPeriodos;
var plantel;
var nodoSel=false;
Ext.onReady(inicializar);

function inicializar()
{
	var gridPlanesEstudio=crearGridPlanesEstudio();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            border:false,
                                            items:	[
                                                    
                                                        {
                                                            xtype:'panel',
                                                            border:false,
                                                            region:'center',
                                                            layout:'border',
                                                            
                                                            items:	[
                                                                        gridPlanesEstudio
                                                                    ]
                                                        }
                                    				 ]
										}
									]                                        
                                                     
						}
                    )   
}


function crearGridPlanesEstudio()
{
	var arrModalidades=eval(bD(gE('arrModalidades').value));
	var cmbModalidad=crearComboExt('cmbModalidad',arrModalidades,0,0,300);
    cmbModalidad.on('select',function(cmb,registro)
    						{
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                    	gEx('cmbTurno').reset();
                                     	gEx('cmbTurno').getStore().loadData(eval(arrResp[1]));   
                                        recargarFechas();
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=104&plantel='+gE('plantel').value+'&idPrograma='+gE('idProgramaEducativo').value+'&modalidad='+registro.get('id'),true);

                            }
    			)
    var cmbTurno=crearComboExt('cmbTurno',[],0,0,200);

	cmbTurno.on('select',function(cmb,registro)
    					{
                        	recargarFechas();
                        }
    			)
    
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
                                        
		var cargadorArbol=new Ext.tree.TreeLoader(
                                                        {
                                                            baseParams:{
                                                                            funcion:'105'
                                                                        },
                                                            dataUrl:'../paginasFunciones/funcionesPlanteles.php'
                                                        }	


		                                         )		                                        
       
       
       cargadorArbol.on('beforeload',function(proxy)
       								{
                                    	var iModalidad=-1;
                                        if(cmbModalidad.getValue()!='')
                                        	iModalidad=cmbModalidad.getValue()
                                        var iTurno=-1;
										if(cmbTurno.getValue()!='')
                                        	iTurno=cmbTurno.getValue()
                                    	proxy.baseParams.idModalidad=iModalidad;
                                        proxy.baseParams.idTurno=iTurno;
                                        proxy.baseParams.idPrograma=gE('idProgramaEducativo').value;
                                        proxy.baseParams.plantel=gE('plantel').value;
                                        
                                        
                                    }
       					)
                                
                                      
                                        
		var arbol = new Ext.ux.tree.TreeGrid	(
                                                            {
                                                                id:'arbolPlanes',
                                                                useArrows:true,
                                                                autoScroll:false,
                                                                animate:true,
                                                                enableDD:true,
                                                                containerScroll: true,
                                                                root:raiz,
                                                                region:'center',
                                                                border:false,
                                                                loader: cargadorArbol,
                                                                rootVisible:false,
                                                                draggable:false,
                                                                border:false,
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                                html:'<span class="letraVino12N">Seleccione la Modalidad:</span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbModalidad,
                                                                            
                                                                            '-',
                                                                            {
                                                                            	xtype:'label',
                                                                                html:'<span class="letraVino12N">Seleccione el Turno:</span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbTurno
                                                                		],
                                                                enableSort : false,
                                                                columns:[
                                                                			
                                                                            {
                                                                                header:'',
                                                                                width:450,
                                                                                dataIndex:'text'
                                                                            },
                                                                            {
                                                                                header:'Fechas de Inscripci&oacute;n<br />(Nuevo ingreso)',
                                                                                width:160,
                                                                                menuDisabled :true,
                                                                                align:'center',
                                                                                css:'text-align:right !important;',
                                                                                sortable:false,
                                                                                dataIndex:'fechasInscripcion'
                                                                            },
                                                                            {
                                                                                header:'Fechas de Reinscripci&oacute;n',
                                                                                width:160,
                                                                                menuDisabled :true,
                                                                                sortable:false,
                                                                                align:'center',
                                                                                css:'text-align:right !important;',
                                                                                dataIndex:'fechasReInscripcion'
                                                                            }
                                                                           
                                                                         ]
                                                               
                                                            }
                                                    );
        arbol.on('click',nodoClick);
        arbol.expandAll();    	
        return arbol;
}



function nodoClick(nodo)
{
	nodoSel=nodo;
    
}

function recargarFechas()
{
	gEx('arbolPlanes').getRootNode().reload();
    gEx('arbolPlanes').expandAll();
}