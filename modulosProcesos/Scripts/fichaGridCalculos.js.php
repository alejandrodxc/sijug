<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	
	
	$arreglo[0]["idRubro"]="0";
	$arreglo[0]["tituloPanel"]="Honorarios";
	$arreglo[0]["tituloConcepto"]="Honorarios";
	$arreglo[0]["arreglo"]="";

	$arreglo[1]["idRubro"]="1";
	$arreglo[1]["tituloPanel"]="Material e impresiones";
	$arreglo[1]["tituloConcepto"]="Material e impresiones";
	$arreglo[1]["arreglo"]="";

	$arreglo[2]["idRubro"]="2";
	$arreglo[2]["tituloPanel"]="Equipo de cómputo, de proyección, multimedia";
	$arreglo[2]["tituloConcepto"]="Equipo";
	$arreglo[2]["arreglo"]="";
	
	$arreglo[3]["idRubro"]="3";
	$arreglo[3]["tituloPanel"]="Vi&aacute;ticos";
	$arreglo[3]["tituloConcepto"]="Vi&aacute;ticos";
	$arreglo[3]["arreglo"]="";
	
	$arreglo[4]["idRubro"]="4";
	$arreglo[4]["tituloPanel"]="Otros";
	$arreglo[4]["tituloConcepto"]="Concepto";
	$arreglo[4]["arreglo"]="";
	
	$tamano=sizeof($arreglo);
	for($x=0;$x<$tamano;$x++)
	{
		$consulta="SELECT idGridVSCalculo,calculo,costoUnitario,cantidad,total,idRubro,montoAutorizado  FROM 100_calculosGrid WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro." AND idRubro=".$arreglo[$x]["idRubro"]." and eliminado=0 order by calculo";
		$storeA=$con->obtenerFilasArreglo($consulta);
		$arreglo[$x]["arreglo"]=$storeA;
	}

?>

Ext.onReady(inicializar);

function inicializar()
{
    mostrarTab();
    sumarTotal();
    
}

function  mostrarTab()
{
    var arregloTabs=[];
    var panel;
    var grid;
    <?php

	for($z=0;$z<$tamano;$z++)
    {
       
       $titulo=$arreglo[$z]["tituloPanel"];
       $storeGrid=$arreglo[$z]["arreglo"];
       $idRubro=$arreglo[$z]["idRubro"];
	   $tituloConcepto=$arreglo[$z]["tituloConcepto"];
	   echo '	
	   			grid=	gridGenerico('.$storeGrid.',"'.$tituloConcepto.'",'.$idRubro.');
	   			panel=		{
								xtype:"panel",
								title:"'.$titulo.'",
								items:[grid]
							};
				arregloTabs.push(panel);
				
				';
	   
    }
    ?>
    var tabs = new Ext.TabPanel	(
									{
										renderTo: 'grids',
										activeTab: 0,
										width:850,
										height:370,
										items:	arregloTabs
									}
								);
    
}


function gridGenerico(arregloG,titulo,idRubro)
{
	var arrDatos=arregloG;
    var iR=idRubro;
    var dSetGenerico= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idGridVSCalculo'},
                                                                    {name:'calculo'},
                                                                    {name:'costoUnitario'},
                                                                    {name:'cantidad'},
                                                                    {name:'idRubro'},
                                                                    {name:'total'},
                                                                    {name: 'montoAutorizado'}
                                                                ]
                                                    }
                                                 )
    
	dSetGenerico.loadData(arrDatos);	
	var cmGenerico= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:titulo,
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'calculo'
                                                            },
                                                            {
                                                                header:'Costo Unitario',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'costoUnitario',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
                                                                editor:{xtype:'numberfield'},
                                                                renderer:function(val)
                                                                		{
                                                                        	return Ext.util.Format.number(val,'0,0.00');
                                                                        }
                                                            },
                                                            {
                                                                header:'Total',
                                                                width:100,
                                                                sortable:true,
                                                                
                                                                renderer: function(val,meta,registro)
                                                                			{
                                                                  				var cantidadR=registro.get('cantidad');
                                                                                cantidadR=parseFloat(cantidadR);
                                                                                var costoU=registro.get('costoUnitario');
                                                                            	costoU=parseFloat(costoU);
                                                                                var total=cantidadR*costoU;
                                                                                return Ext.util.Format.usMoney(total);
                                                                            }
                                                               
                                                            },
                                                             {
                                                                header:'Monto autorizado',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'montoAutorizado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var cantidadR=registro.get('cantidad');
                                                                            cantidadR=parseFloat(cantidadR);
                                                                            var costoU=registro.get('costoUnitario');
                                                                            costoU=parseFloat(costoU);
                                                                            var total=cantidadR*costoU;
                                                                        	if(parseFloat(val)==total)
                                                                            {
                                                                            	return '<font color="#005500">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                            }
                                                                            else
                                                                            	return '<font color="#B0281A">'+Ext.util.Format.usMoney(val)+'</font>';
                                                                        }
                                                            }
                                                        ]
                                                    );
											
												
	tblGrid=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	id:'gridCalculo_'+idRubro,
                                                        store:dSetGenerico,
                                                        frame:true,
                                                        cm: cmGenerico,
                                                        height:350,
                                                        width:850
													}
    											);
	return tblGrid;
}


function funcEditorValidaGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='gridCalculo_'+datosEditor[1];
   
    var grid=Ext.getCmp(idGrid);
    
    if(obj.calculo=='')
    {
    
    	var columna=grid.getColumnModel().getColumnHeader(2);
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna '+columna+' no puede ser vacio')
        return false;
    }
    
    if((obj.costoUnitario=='') || (obj.costoUnitario==0.00))
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Costo Unitario no puede ser vacio')
        return false;
    }
    
    if((obj.cantidad=='') || (obj.cantidad==0.00))
    {
    	Ext.MessageBox.alert(lblAplicacion,'El valor de la columna Cantidad no puede ser vacio')
        return false;
    }
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
        	sumarTotal();
            Ext.getCmp('btnDel_'+datosEditor[1]).enable();
        	Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
        	grid.nuevoRegistro=false;
           
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=1&idRegistro='+<?php echo $idRegistro ?>+'&idFormulario='+<?php echo $idFormulario?>+'&id='+registro.get('idGridVSCalculo')+'&costoUnitario='+obj.costoUnitario+'&cantidad='+obj.cantidad+'&total='+obj.total+'&idRubro='+datosEditor[1]+'&calculo='+obj.calculo,true);
   
    return true;
}

function sumarTotal()
{
	var sumaTotal=0;
    
    <?php
	for($y=0;$y<$tamano;$y++)
    {
       
       $idRubro=$arreglo[$y]["idRubro"];
	   echo '	
	   			sumaTotal+=sumarGrid('.$idRubro.');
				';
    }
    ?>
    
 	var etiqueta=gE('sumaTotalE');
    etiqueta.innerHTML='<font color=\'#009900\'><span id="spTotal">'+Ext.util.Format.usMoney(sumaTotal)+'</span></font>';
    sumaPresupuestoTotal();
}

function sumarGrid(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridCalculo_'+idRubro).getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var cUni=elemento.get('costoUnitario');
    	cUni=parseFloat(cUni);
        var cantidad=elemento.get('cantidad');
        cantidad=parseFloat(cantidad);
        var total=cUni*cantidad;
        sumaGrid=sumaGrid+total;
    }
   return sumaGrid;
}

function sumaPresupuestoTotal()
{

	var sumaTotalE=parseFloat(normalizarValor(gE('spTotal').innerHTML));
    var recursoOrganizacion=normalizarValor(gE('txtRecursoOrganizacion').innerHTML);
    if(recursoOrganizacion=='')
    	recursoOrganizacion=0;
    else
    	recursoOrganizacion=parseFloat(recursoOrganizacion);
    var recursoOtros=normalizarValor(gE('txtRecursoOtrosDonantes').innerHTML);
    if(recursoOtros=='')
    	recursoOtros=0;
    else
    	recursoOtros=parseFloat(recursoOtros);
	var sumaTotalPresupuesto=gE('sumaTotalPresupuesto');
    var total=sumaTotalE+recursoOrganizacion+recursoOtros;
    sumaTotalPresupuesto.innerHTML=Ext.util.Format.usMoney(total);
}