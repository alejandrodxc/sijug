<?php
	session_start();
	include("latis/conexionBD.php");
	
?>
Ext.onReady(inicializar);
var arrListaAsistencia=[['1','De apertura'],['2','De cierre']];
var busquedaRealizada=false;
var editaAsistencia=true;
var idCurso=-1;
function inicializar()
{
	
	var cmbTipoLista=crearComboExt('cmbTipoLista',arrListaAsistencia,0,0,130);
    cmbTipoLista.setValue('1');
	var alDatos= new Ext.data.JsonStore({
                                                
                                                totalProperty :'numReg',
                                                fields: [
                                                            {name: 'idUsuarioAsistente'},
                                                            {name: 'nombre'}
                                                            
                                                        ],
                                                 proxy : new Ext.data.HttpProxy	(
                                                                                      {
                                                                                          url: '../paginasFunciones/funcionesModulosEspeciales_Galileo.php'
                                                                                          
                                                                                      }
    
                                                                                  ),
                                                sortInfo: {field: 'nombre', direction: 'ASC'},
                                                autoLoad:false,
                                                root:'registros',
                                                remoteSort: false
                                            }
                                          );
        alDatos.setDefaultSort('nombre', 'ASC');
        
        alDatos.on('beforeload',function(proxy)
                                        {
                                        	
                                           	proxy.baseParams.funcion=8;
                                            var cmbPlantel=gE('cmbPlantel');
                                            var plantel=cmbPlantel.options[cmbPlantel.selectedIndex].value;
                                            proxy.baseParams.plantel=plantel;
                                        }
                            )
                            
		var checkColumn = new Ext.grid.CheckColumn	(
	 												{
													   header: '',
													   dataIndex: 'asistencia',
													   width: 70,
                                                      
													}
												);                            
                            
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Profesor',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'nombre',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href="javascript:mostrarVentanaHorario(\''+bE(registro.get('idUsuarioAsistente'))+'\')">'+val+'</a>';
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                            	tbar:	[
                                                                			
                                                                            {
                                                                            	id:'btnRegistraAsistencia',
                                                                            	icon:'../images/users.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:true,
                                                                                text:'<span class="letraRoja"><b>Confirmar asistencia</b></span>',
                                                                                handler:function()
                                                                                        {
                                                                                            guardarNoAsistentes2();
                                                                                        }
                                                                            },'-',
                                                                            {
                                                                            	id:'btnRegistraNuevo',
                                                                                hidden:true,
                                                                            	icon:'../images/user_add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar nuevo asistente',
                                                                                handler:function()
                                                                                        {
                                                                                            var arrDatos=[['idFormulario','292'],['idRegistro',idCurso],['cveParticipacion','35'],['cPagina','sFrm=true']];
                                                                                            var obj={};
                                                                                            obj.titulo='Registrar Asistente';
                                                                                            obj.url='../modulosEspeciales/agregarAlumnosCursoLinea.php';
                                                                                            obj.params=arrDatos;
                                                                                            obj.ancho=900;
                                                                                            obj.alto=450;
                                                                                            abrirVentanaFancy(obj);
                                                                                            
                                                                                        }
                                                                            }
                                                                		],
                                                                id:'gridAsistencia',
                                                                store:alDatos,
                                                                renderTo:'tblAsistencia',
                                                                frame:true,
                                                                cm: cModelo,
                                                                plugins:[checkColumn],
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                height:400,
                                                                width:600
                                                            }
                                                        );
                                                        
    tblGrid.on('beforeedit',function(e)
    						{
                            	e.cancel=!editaAsistencia;
                            }
    			)                                                 
	fechaSesionChange(gE('cmbSesion'));	                                                        
        return 	tblGrid;	
}

function refrescarGrid()
{
	var gridAsistencia=gEx('gridAsistencia');
	gridAsistencia.getStore().reload();
}

function buscarGruposSede()
{
	inicializarCampos();
    var valor=gE('cveCCT').value;
    if(valor=='')
    {
    	function resp()
        {
        	gE('cveCCT').focus();
        }
    	msgBox('Debe ingresar la clave CCT del plantel que desea buscar',resp);
    	return;
    }
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        switch(arrResp[0])
        {
        	case '1':
                
                busquedaRealizada=true;
                var arrGrupos=eval(arrResp[1]);
                llenarCombo(gE('cmbGrupo'),arrGrupos,true);
                hE('cmbGrupo');
                gE('lblPlantel').innerHTML=arrResp[2];
                gE('plantel').value=arrResp[3];
           	break;
            case '2':
            	gE('lblError').innerHTML='La clave CCT ingresada no existe o no cuenta con alg&uacute;n grupo asociado';
                gE('cveCCT').focus();
                dE('cmbGrupo');
            break;
        	default:    
	            msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
            break;
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=9&idCurso='+gE('idCurso').value+'&cveCCT='+valor,true);
}

function validarTecla(evt)
{
	
    
	var key= evt.which;
	if(Ext.isIE)
		key=evt.keyCode;
	if(key==13)
    {
    	buscarGruposSede();
    }
}

function obtenerDiasSesion(cmb)
{
	oE('gridDetalle');
     gE('lblError').innerHTML='';
   
    if(cmb.selectedIndex==0)
    	return;
    var fechaActual=new Date();
    gE('noSesion').value=-1;
    oE('filaSesion');
    gE('lblSesion').innerHTML='';
	var idGrupo=cmb.options[cmb.selectedIndex].value;
    var comp='';
    if(gE('fechaSesion').value!='')
    {
    	comp='&fechaSesion='+gE('fechaSesion').value;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        switch(arrResp[0])
        {
        	case '1':
	            gE('lblSesion').innerHTML='<?php date("d/m/Y")?> (Sesi&oacute;n: '+arrResp[1]+')';
                mE('filaSesion');
            	gE('lblError').innerHTML='';
                 gEx('gridAsistencia').getStore().removeAll();
                mE('gridDetalle');
	            gEx('gridAsistencia').getStore().reload();
                gE('noSesion').value=arrResp[1];
                if(arrResp[2]=='1')
                {
                	 gE('lblError').innerHTML='La asistencia a esta sesi&oacute;n ya ha sido registrada';
                	bloquearGrid();
                    oE('filaInstrucciones');
                }
                else
                {
                	desBloquearGrid();
               		mE('filaInstrucciones');
                }
            break;
            case '2':
            	gE('lblError').innerHTML='No se encuentra dentro de los l&iacute;mites de horario permitido para registrar asistencia';
            break;
            case '3':
            	gE('lblError').innerHTML='No puede registrar asistencia debido a que el d&iacute;a de hoy no cuenta con una sesi&oacute;n de trabajo';
            break;
            default:
            	 msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
            break;
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=10&idCurso='+gE('idCurso').value+'&plantel='+gE('plantel').value+'&idGrupo='+idGrupo+comp,true);
}

function inicializarCampos()
{

	oE('lblCTTNO');
    oE('filaSesion');
    limpiarCombo(gE('cmbGrupo'));
    limpiarCombo(gE('cmbPlantel'));
    gE('cmbCategoria').selectedIndex=0;
    dE('cmbGrupo');
    oE('gridDetalle');
    gEx('gridAsistencia').getStore().removeAll();
    gE('lblPlantel').innerHTML='';
    gE('lblError').innerHTML='';
}

function teclaUP(e) 
{ 
    if(busquedaRealizada)
    {
    	inicializarCampos();
    	busquedaRealizada=false;
    }
}  

function sesionChange(cmb)
{
	oE('btnGuardar');
    oE('gridDetalle');
    dE('noAsistentes');
    gE('noAsistentes').value=0;
    var cmbGrupo=gE('cmbGrupo');
    var idGrupo=cmbGrupo.options[cmbGrupo.selectedIndex].value;
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval('['+arrResp[1]+']')[0];
            refrescarGrid();
            idCurso=arrDatos.idCurso;
            if(arrDatos.noAsistentes!='-1')
            {
            	gE('noAsistentes').value=arrDatos.noAsistentes;
                mE('gridDetalle');
                dE('noAsistentes');
                oE('btnGuardar');
                if(arrDatos.asistenciaCerrada=='1')
                	bloquearGrid();
                else
                	desBloquearGrid();
            }
            else
            {
            	gE('noAsistentes').value=0;
                oE('gridDetalle');
                mE('btnGuardar');
                hE('noAsistentes');
                gE('noAsistentes').focus();
                gE('noAsistentes').select();
                mE('btnGuardar');
                
            }
        }
        else
        {
            msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=11&fechaSesion='+cmb.options[cmb.selectedIndex].value+'&plantel='+gE('plantel').value+'&idGrupo='+idGrupo,true);
}

function guardarNoAsistentes1()
{
	var cmb=gE('cmbGrupo');
    var fechaActual=new Date();
	var idGrupo=cmb.options[cmb.selectedIndex].value;
    var cmbFecha=gE('cmbFechaSesion');
    var fechaSesion=cmbFecha.options[cmbFecha.selectedIndex].value;
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    
                    mE('gridDetalle');
                    dE('noAsistentes');
                    oE('btnGuardar');
                    gEx('gridAsistencia').getStore().reload();
                    desBloquearGrid();
                }
                else
                {
                    msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=12&fechaSesion='+fechaSesion+'&plantel='+gE('plantel').value+'&idGrupo='+idGrupo+'&horaRegistro='+fechaActual.format('H:i')+'&noAsistentes='+gE('noAsistentes').value,true);
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer guardar el n&uacute;mero de asistentes ingresado?<br /><span style="color:#000"><b>Nota:</b></span><span> Esta operaci&oacute;n no es reversible</span>',resp)
}



function guardarNoAsistentes2()
{
	var cmb=gE('cmbGrupo');
   	var idGrupo=cmb.options[cmb.selectedIndex].value;
    var noSesion=gE('noSesion').value;
    
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var gridAsistencia=gEx('gridAsistencia');
            var fila;
            var x;
            var listaAsistentes='';
            for(x=0;x<gridAsistencia.getStore().getCount();x++)
            {
            	fila=gridAsistencia.getStore().getAt(x);
                if(fila.get('asistencia'))
                {
                	if(listaAsistentes=='')
                    	listaAsistentes=fila.get('idUsuarioAsistente');
                    else
                    	listaAsistentes+=','+fila.get('idUsuarioAsistente');
                }	
            }
            
        	
            var comp='';
            if(gE('fechaSesion').value!='')
            {
                comp='&fechaSesion='+gE('fechaSesion').value;
            }
            
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                   	function respFinal()
					{
                    	bloquearGrid();
                    	inicializarCampos();	
                    }
			       	msgBox('La asistencia ha sido registrada',respFinal);
                   	return;
                }
                else
                {
                    msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=13&idCurso='+gE('idCurso').value+'&listaAsistencia='+listaAsistentes+'&noSesion='+noSesion+'&plantel='+gE('plantel').value+'&idGrupo='+idGrupo+comp,true);
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer guardar la asistencia de este grupo?<br /><span style="color:#000"><b>Nota:</b></span><span> Esta operaci&oacute;n no es reversible</span>',resp)
}

function bloquearGrid()
{
	gEx('btnRegistraAsistencia').disable();
    gEx('btnRegistraNuevo').disable();
    editaAsistencia=false;
   // editaAsistencia=true;
}

function desBloquearGrid()
{
	gEx('btnRegistraAsistencia').enable();
    gEx('btnRegistraNuevo').enable();
    editaAsistencia=true;
}

function fechaSesionChange(cmb)
{
	gEx('gridAsistencia').getStore().removeAll();
    var cmbCategoria=gE('cmbCategoria');
    cmbCategoria.selectedIndex=0;
    var cmbPlantel=gE('cmbPlantel');
    cmbPlantel.selectedIndex=0;
    if(cmb.selectedIndex==0)
    {
    	cmbCategoria.disabled=true;
        cmbPlantel.disabled=true;
    }
    else
    {
    	cmbCategoria.disabled=false;
        cmbPlantel.disabled=false;
    
    }
}

function obtenerPlanteles(cmb)
{
	gEx('gridAsistencia').getStore().removeAll();
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
         	llenarCombo(gE('cmbPlantel'),eval(arrResp[1]),true);   
        }	
        else
        {
            msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_Galileo.php',funcAjax, 'POST','funcion=6&idMunicipio='+cmb.options[cmb.selectedIndex].value,true);
    
}

function obtenerGrupos(cmb)
{
	gEx('gridAsistencia').getStore().removeAll();
	gEx('gridAsistencia').getStore().reload();
}

function cambiaBusqueda(rdo)
{
	inicializarCampos();
	if(rdo.value=='1')
    {
    	oE('filaCategoria');
        oE('cmbPlantel');
        mE('filaCCT');
        mE('lblPlantel');
    }
    else
    {
    	mE('filaCategoria');
        mE('cmbPlantel');
        oE('filaCCT');
        oE('lblPlantel');
    }
}

function mostrarVentanaHorario(iP)
{
	var gridHorario=crearGridHorario(bD(iP));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridHorario

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Pase de lista',
										width: 550,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: 'Aceptar',
                                                            
															handler: function()
																	{
																		var fila;
                                                                        var obj='';
                                                                        var cadObj='';
                                                                        var cmbfecha=gE('cmbSesion');
                                                                        
                                                                        var fechaSesion=cmbfecha.options[cmbfecha.selectedIndex].value;
                                                                        for(x=0;x<gridHorario.getStore().getCount();x++)
                                                                        {
	                                                                        fila=gridHorario.getStore().getAt(x);
                                                                            obj='{"idGrupo":"'+fila.get('idHorario')+'","noAsistentes":"'+fila.get('totalAlumno')+'"}';
                                                                            if(cadObj=='')
                                                                            	cadObj=obj;
                                                                            else
                                                                            	cadObj+=','+obj;
                                                                        }
                                                                        cadObj='{"idUsuario":"'+bD(iP)+'","fechaSesion":"'+fechaSesion+'","registros":['+cadObj+']}';
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                function respFinal()
                                                                                {
                                                                                	var cmbSesion=gE('cmbSesion');
                                                                                    cmbSesion.selectedIndex=0;
                                                                                    fechaSesionChange(cmbSesion);
                                                                                    ventanaAM.close();
                                                                                }
                                                                                msgBox('La asistencia a sido guardada correctamente',respFinal)
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente error:'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_Galileo.php',funcAjax, 'POST','funcion=9&cadObj='+cadObj,true);
                                                                        
                                                                                                                                            }
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridHorario(iP)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idHorario'},
		                                                {name: 'horario'},
		                                                {name:'totalAlumno'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_Galileo.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'horario', direction: 'ASC'},
                                                            groupField: 'horario',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	var cmbSesion=gE('cmbSesion');                                                        
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='7';
                                        proxy.baseParams.idCurso=gE('idCurso').value;
                                        proxy.baseParams.fechaSesion=cmbSesion.options[cmbSesion.selectedIndex].value;
                                        proxy.baseParams.idProfesor=iP;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Horario',
                                                                width:110,
                                                                align:'center',
                                                               
                                                                sortable:true,
                                                                dataIndex:'horario'
                                                            },
                                                            {
                                                                header:'# total de alumnos(incluyendo todos sus grupos en caso <br>de tener más de un grupo en un horario)',
                                                                width:340,
                                                                align:'center',
                                                                css:'text-align:right !important;',
                                                                sortable:true,
                                                                dataIndex:'totalAlumno',
                                                                editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false}
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridHorariosAlumno',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                width:525,
                                                                clicksToEdit:1,
                                                                height:230,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}