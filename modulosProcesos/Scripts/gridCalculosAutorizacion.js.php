<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idFormulario=$_GET["idFormulario"];
	$idRegistro=$_GET["idRegistro"];
	
	
	$consulta="SELECT categorias,txtPresupuestoCensida FROM _293_tablaDinamica WHERE id__293_tablaDinamica=".$idRegistro;
	$filaPresupuesto=$con->obtenerPrimeraFila($consulta);
	
	$idCategoria=$filaPresupuesto[0];
	$monto=$filaPresupuesto[1];
	
	$consulta="SELECT porcentajeHonorarios FROM _312_montosCategoria WHERE categoria=".$idCategoria;
	$porcentaje=$con->obtenerValor($consulta);
	$montoHono=($monto * $porcentaje)/100;
	$arreglo[0]["idRubro"]="0";
	$arreglo[0]["tituloPanel"]="Honorarios";
	$arreglo[0]["tituloConcepto"]="Honorarios";
	$arreglo[0]["arreglo"]="";
	$arreglo[0]["montoMaximo"]=$montoHono;

	$arreglo[1]["idRubro"]="1";
	$arreglo[1]["tituloPanel"]="Material e impresiones";
	$arreglo[1]["tituloConcepto"]="Material e impresiones";
	$arreglo[1]["arreglo"]="";
	$arreglo[1]["montoMaximo"]="-1";

	$arreglo[2]["idRubro"]="2";
	$arreglo[2]["tituloPanel"]="Equipo de cómputo, de proyección, multimedia";
	$arreglo[2]["tituloConcepto"]="Equipo";
	$arreglo[2]["arreglo"]="";
	$arreglo[2]["montoMaximo"]="-1";
	
	$arreglo[3]["idRubro"]="3";
	$arreglo[3]["tituloPanel"]="Vi&aacute;ticos";
	$arreglo[3]["tituloConcepto"]="Vi&aacute;ticos";
	$arreglo[3]["arreglo"]="";
	$arreglo[3]["montoMaximo"]="-1";
	
	$arreglo[4]["idRubro"]="4";
	$arreglo[4]["tituloPanel"]="Otros";
	$arreglo[4]["tituloConcepto"]="Concepto";
	$arreglo[4]["arreglo"]="";
	$arreglo[4]["montoMaximo"]="-1";
	
	$tamano=sizeof($arreglo);
	for($x=0;$x<$tamano;$x++)
	{
		$consulta="SELECT idGridVSCalculo,calculo,costoUnitario,cantidad,total,idRubro,montoAutorizado  FROM 100_calculosGrid WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro." AND idRubro=".$arreglo[$x]["idRubro"]." order by calculo";
		$storeA=$con->obtenerFilasArreglo($consulta);
		//echo $consulta;
		$arreglo[$x]["arreglo"]=$storeA;
	}

?>

Ext.onReady(inicializar);

function inicializar()
{
	var mascara=new Mask('$#,###.00','number');
    mostrarTab();
    sumarTotal();
    sumarTotalAutorizado();

}

function  mostrarTab()
{
    var arregloTabs=[];
    var panel;
    var grid;
    <?php

	for($z=0;$z<$tamano;$z++)
    {
       
       $titulo=$arreglo[$z]["tituloPanel"];
       $storeGrid=$arreglo[$z]["arreglo"];
       $idRubro=$arreglo[$z]["idRubro"];
	   $tituloConcepto=$arreglo[$z]["tituloConcepto"];
	   $montoMaximo=$arreglo[$z]["montoMaximo"];
	   echo '	
	   			grid=	gridGenerico('.$storeGrid.',"'.$tituloConcepto.'",'.$idRubro.','.$montoMaximo.');
	   			panel=		{
								xtype:"panel",
								title:"'.$titulo.'",
								items:[grid]
							};
				arregloTabs.push(panel);
				
				';
	   
    }
    ?>
    var tabs = new Ext.TabPanel	(
									{
										renderTo: 'grids',
										activeTab: 0,
										width:850,
										height:285,
										items:	arregloTabs
									}
								);
    
}


function gridGenerico(arregloG,titulo,idRubro,montoMaximo)
{
	var arrDatos=arregloG;
    var iR=idRubro;
    var dSetGenerico= new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    
                                                                    {name:'idGridVSCalculo'},
                                                                    {name:'calculo'},
                                                                    {name:'costoUnitario'},
                                                                    {name:'cantidad'},
																	{name:'total'},
                                                                    {name:'idRubro'},
                                                                    {name: 'montoAutorizado'}
                                                                ]
                                                    }
                                                 )
    
	dSetGenerico.loadData(arrDatos);	
	var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editor_'+idRubro,
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
    editorFila.on('beforeedit',funcEditorFilaBeforeEditGridCalculo)
    editorFila.on('validateedit',funcEditorValidaGridCalculo);
    editorFila.on('canceledit',funcEditorCancelEditGridCalculo);
	var cmGenerico= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            columnaCheck,
                                                            {
                                                                header:titulo,
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'calculo'
                                                            },
                                                            {
                                                                header:'Costo Unitario',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'costoUnitario',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'cantidad',
                                                                renderer:function(val)
                                                                		{
                                                                        	return Ext.util.Format.number(val,'0,0.00');
                                                                        }
                                                            },
                                                            {
                                                                header:'Total',
                                                                width:110,
                                                                sortable:true,
                                                                renderer: function(val,meta,registro)
                                                                			{
                                                                  				var cantidadR=registro.get('cantidad');
                                                                                cantidadR=parseFloat(cantidadR);
                                                                                var costoU=registro.get('costoUnitario');
                                                                            	costoU=parseFloat(costoU);
                                                                                var total=cantidadR*costoU;
                                                                                return Ext.util.Format.usMoney(total);
                                                                            }
                                                               
                                                            },
                                                            {
                                                                header:'Monto autorizado',
                                                                width:110,
                                                                sortable:true,
                                                                editor:{xtype:'numberfield'},
                                                                dataIndex:'montoAutorizado',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Diferencia',
                                                                width:110,
                                                                sortable:true,
                                                               
                                                                renderer:function (val,meta,registro)
                                                                		{
                                                                        	var cantidadR=registro.get('cantidad');
                                                                            cantidadR=parseFloat(cantidadR);
                                                                            var costoU=registro.get('costoUnitario');
                                                                            costoU=parseFloat(costoU);
                                                                            var total=cantidadR*costoU;
                                                                            
                                                                        	return Ext.util.Format.usMoney(total-parseFloat(registro.get('montoAutorizado')));
                                                                        }
                                                            }
                                                        ]
                                                    );
											
												
	tblGrid=	new Ext.grid.EditorGridPanel	(
                                                    {
                                                    	id:'gridCalculo_'+idRubro,
                                                        store:dSetGenerico,
                                                        frame:true,
                                                        cm: cmGenerico,
                                                        sm:columnaCheck,
                                                        height:250,
                                                        width:820,
                                                        plugins:[editorFila]
													}
    											);
	tblGrid.nuevoRegistro=false;  
    tblGrid.montoMaximo=montoMaximo;
    tblGrid.rubro=titulo;
    <?php
	if($_SESSION["idUsr"]!="3")
	{
	?>
    tblGrid.on('beforeedit',function(e){e.cancel=true});
    <?php
	}
	?>
	return tblGrid;
}

function funcEditorFilaBeforeEditGridCalculo(rowEdit,fila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='gridCalculo_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
	grid.registroEdit=grid.getStore().getAt(fila);	
    
    if((grid.registroEdit.get('montoAutorizado')=='')||(parseFloat(grid.registroEdit.get('montoAutorizado'))==0))
    {
    	grid.registroEdit.set('montoAutorizado',(parseFloat(grid.registroEdit.get('costoUnitario'))*parseFloat(grid.registroEdit.get('cantidad'))))
    }
}


function funcEditorValidaGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='gridCalculo_'+datosEditor[1];
    var grid=Ext.getCmp(idGrid);
    if(obj.montoAutorizado=='')
    	obj.montoAutorizado='0';
    
    var montoActual=parseFloat(obj.cantidad)*parseFloat(obj.costoUnitario);
    if(parseFloat(obj.montoAutorizado)>montoActual)
    {
    	Ext.MessageBox.alert(lblAplicacion,'No puede autorizar un monto mayor al solicitado ($ '+formatearNumero(montoActual,2,'.',',')+')');
        return false;	
   	}

    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1') 
        {
           	var sumaTotal=sumarTotalAutorizado();
            window.parent.actualizarTotalAutorizado(<?php echo $idRegistro?>,Ext.util.Format.number(sumaTotal,'0,000.00'));
        	grid.nuevoRegistro=false;
           	refrescarMenuDTD();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=4&id='+registro.get('idGridVSCalculo')+'&montoAut='+obj.montoAutorizado,true);
   
    return true;
}

function funcEditorCancelEditGridCalculo(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='gridCalculo_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	else
    	grid.getStore().rejectChanges();
	
	grid.nuevoRegistro=false;
     var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

	var sumaTotal=sumarTotalAutorizado();
    window.parent.actualizarTotalAutorizado(<?php echo $idRegistro?>,Ext.util.Format.number(sumaTotal,'0,000.00'));
}

function sumarTotal()
{
	var sumaTotal=0;
    
    <?php
	for($y=0;$y<$tamano;$y++)
    {
       
       $idRubro=$arreglo[$y]["idRubro"];
	   echo '	
	   			sumaTotal+=sumarGrid('.$idRubro.');
				';
    }
    ?>
    
 	var etiqueta=gE('sumaTotalE');
    etiqueta.innerHTML='<font color=\'#005500\'><span id="spTotal">'+Ext.util.Format.usMoney(sumaTotal)+'</span></font>';
    
}

function sumarTotalAutorizado()
{
	var sumaTotal=0;
    
    <?php
	for($y=0;$y<$tamano;$y++)
    {
       
       $idRubro=$arreglo[$y]["idRubro"];
	   echo '	
	   			sumaTotal+=sumarGridAutorizado('.$idRubro.');
				';
    }
    ?>
    
 	var etiqueta=gE('sumaAutorizado');
    etiqueta.innerHTML='<font color=\'#005500\'><span id="spTotalAutorizado">'+Ext.util.Format.usMoney(sumaTotal)+'</span></font>';
    obtenerDiferencia();
    return sumaTotal;
    
}



function sumarGridAutorizado(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridCalculo_'+idRubro).getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var total=parseFloat(elemento.get('montoAutorizado'));
        sumaGrid=sumaGrid+total;
    }
   return sumaGrid;
}

function funcAfterEdit(e)
{
  
   sumarTotal();
}

function obtenerDiferencia()
{

	var sumaTotalE=parseFloat(normalizarValor(gE('spTotal').innerHTML));
    var totalAutorizado=parseFloat(normalizarValor(gE('spTotalAutorizado').innerHTML));
	gE('diferencia').innerHTML=Ext.util.Format.usMoney(sumaTotalE-totalAutorizado);

}

function sumarGrid(idRubro)
{
	var sumaGrid=0;
    var almacen=Ext.getCmp('gridCalculo_'+idRubro).getStore();
    var tamanoS=almacen.getCount();
    var a;
    for(a=0;a<tamanoS;a++)
    {
    	var elemento=almacen.getAt(a);
        var cUni=elemento.get('costoUnitario');
    	cUni=parseFloat(cUni);
        var cantidad=elemento.get('cantidad');
        cantidad=parseFloat(cantidad);
        var total=cUni*cantidad;
        sumaGrid=sumaGrid+total;
    }
   return sumaGrid;
}

function guardarMonto(tMonto,ctrl)
{
	if(ctrl.value=='')
    	ctrl.value='0';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	sumaPresupuestoTotal();
            refrescarMenuDTD();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=3&idFormulario='+gE('idFormulario').value+'&idReferencia='+gE('idReferencia').value+'&monto='+normalizarValor(ctrl.value)+'&tMonto='+tMonto,true);
}

function refrescarMenuDTD()
{
	if(typeof(funcAgregar)!='undefined')
	   	funcAgregar();
}

function sumarRubro(idRubro,monto)
{
	var grid=gEx('gridCalculo_'+idRubro);
    var x;
    var montoTotal=0;
    var fila;
    for(x=0;x<grid.getStore().getCount()-1;x++)
    {
    	fila=grid.getStore().getAt(x);
        montoTotal+=parseFloat(fila.get('costoUnitario'))*parseFloat(fila.get('cantidad'));
   	}
    return montoTotal+monto;
}

function guardarComentario(text)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=5&idProyecto='+gE('idReferencia').value+'&comentario='+cv(text.value),true);
    
        
}

function guardarDictamen(combo)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                 	msgBox('El dictamen ha sido guardado correctamente');   
                    combo.setAttribute('estadoOriginal',combo.options[combo.selectedIndex].value);
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                    selElemCombo(combo,combo.getAttribute('estadoOriginal'));
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=6&idProyecto='+gE('idReferencia').value+'&dictamen='+combo.options[combo.selectedIndex].value,true);
        }
        else
        {
        	selElemCombo(combo,combo.getAttribute('estadoOriginal'));
        }
    }
    msgConfirm('Est&aacute; seguro de querer asentar el dict&aacute;men seleccionado?',resp);
}