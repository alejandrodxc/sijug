<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$sL=false;
	if(isset($_GET["sL"]))
		$sL=true;
		
?>

var arrSituacion=[['-1','Sin dict\xE1men'],['1','Aceptado'],['2','Rechazado']];
var arrSituacion2=[['1','Aceptado'],['2','Rechazado']];

Ext.onReady(inicializar);
var propuestaCerrada=false;

function inicializar()
{
	var nProveedores=gE('nProveedores').value;
    if(nProveedores=='0')
    	propuestaCerrada=true;
   var grid=crearProveedoresPropuesta();
   new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			
                                        {
                                        	region: 'center',
                                        	layout:'border',
                                            items:	[
                                            			grid
                                            			
                                            		]
                                        }
                                     ]
						}
                    )   
}

function crearProveedoresPropuesta()
{
	var cmbDictamen=crearComboExt('cmbDictamen',arrSituacion,0,0,200);
	var idFormulario=gE('idFormulario').value;
    var idRegistro=gE('idRegistro').value;
    var arrPartidas=eval(eval(bD(gE('arrPartidas').value)))
   	var cmbPartida=crearComboExt('cmbPartida',arrPartidas,0,0,550);
    cmbPartida.on('select',function(combo,registro)
    						{
                            	if((registro.get('valorComp')!='')&&(registro.get('valorComp')!='0'))
                                	inhabilitarBotones();
                                else
                                	habilitarBotones();
                            	gEx('gridProveedores').getStore().reload();
                            }
    				)
    if(!propuestaCerrada)
	    cmbPartida.hide();
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idPartida',
                                            fields: [
                                            			{name:'idDictamenPropuesta'},
                                                        {name: 'idProveedor'},
                                                        {name: 'proveedor'},
                                                        {name: 'marca'},
                                                        {name: 'caracteristicas'},
                                                        {name: 'costo'},
                                                        {name: 'dictamen'},
                                                        {name: 'comentarios'},
                                                        {name: 'noProveedor', type:'int'},
                                                        {name: 'costoBase'}
                                                        
                                           			 ],
                                            root:'registros',
                                            remoteGroup:true,
                                            remoteSort: true
                                        }
                                      );
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {
                                                                                                  url: '../paginasFunciones/funcionesCompras.php'
                                                                                              }
                                                                                          ),
                                                            sortInfo: {field: 'noProveedor', direction: 'ASC'},
                                                            groupField: 'proveedor',
                                                            autoLoad:false
                                                        })                                      
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=26;
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
	                                    proxy.baseParams.idPartida=cmbPartida.getValue();
                                    }
                        )
	var chkRow=new Ext.grid.CheckboxSelectionModel();
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                         {
															header:'No. Proveedor',
															width:95,
															sortable:true,
															dataIndex:'noProveedor'
														},
                                                        {
															header:'Proveedor',
															width:250,
															sortable:true,
															dataIndex:'proveedor'
														},
                                                         {
															header:'Marca',
															width:150,
															sortable:true,
                                                            hidden:true,
															dataIndex:'marca'
														},
                                                        {
															header:'Caracter&iacute;sticas',
															width:350,
															sortable:true,
                                                             hidden:true,
															dataIndex:'caracteristicas'
														},
                                                        {
															header:'Costo de la<br>propuesta',
															width:110,
															sortable:true,
															dataIndex:'costo',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Costo l&iacute;mite',
															width:110,
															sortable:true,
															dataIndex:'costoBase',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Dict&aacute;men',
															width:120,
															sortable:true,
															dataIndex:'dictamen',
                                                            editor:cmbDictamen,
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='';
                                                                        switch(val)
                                                                        {
                                                                        	case '-1':
                                                                            	color='#F60';
                                                                            break;
                                                                            case '2':
                                                                            	color="#900";
                                                                            break;
                                                                            case '1':
                                                                            	color="#009";
                                                                            break;
                                                                        }
                                                                    	return '<font color="'+color+'">'+formatearValorRenderer(arrSituacion,val)+'</font>';
                                                                    }
														},
                                                         {
															header:'Comentarios',
															width:350,
															sortable:true,
															dataIndex:'comentarios'
														}
													]
												);
                                               
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:dsTablaRegistros2,
                                                            title:'<span style="font-size:13px; font-weight:bold; color:#900;">Evaluaci&oacute;n de partidas</span>',
                                                            frame:false,
                                                            border:false,
                                                            region:'center',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            clicksToEdit:1,
                                                            id:'gridProveedores',
                                                            tbar:	[
                                                            			{
                                                                        	html:'<img src="../images/exclamation.png" />&nbsp;<span class="letraRojaSubrayada8"><b>S&oacute;lo puede dictaminar una vez que ha cerrado todas las propuestas econ&oacute;micas de los proveedores en su respectivas partidas</b></span>&nbsp&nbsp;',
                                                                            hidden:propuestaCerrada
                                                                        },
                                                            			{
                                                                        	html:'<span class="letraRojaSubrayada8"><b>Partida a evaluar:</b></span>&nbsp&nbsp;',
                                                                            hidden:!propuestaCerrada
                                                                        },
                                                                        cmbPartida
                                                            		
                                                            <?php
																if(!$sL)
																{
															?>
                                                                        ,
                                                                        {
                                                                        	text:'Cerrar dict&aacute;menes de esta partida',
                                                                            icon:'../images/lock.png',
                                                                            id:'btnCerrar',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:false,
                                                                            hidden:!propuestaCerrada,
                                                                            handler:function()
                                                                            	{
                                                                                	var cmbPartida=gEx('cmbPartida');
                                                                                    if(cmbPartida.getValue()=='')
                                                                                    {
	                                                                                    msgBox('Debe seleccionar la partida cuyas propuestas de proveedor desea cerrar');
                                                                                        return;
                                                                                    }
                                                                                	var x;
                                                                                    var fila;
                                                                                    for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                    {
                                                                                    	fila=tblGrid.getStore().getAt(x);
                                                                                        if((fila.get('dictamen')=='-1')||(fila.get('dictamen')==-1))
                                                                                        {
                                                                                        	
                                                                                        	msgBox('Ninguna propuesta del proveedor puede quedar marcada como "Sin dict&aacute;men"');
                                                                                        	return;
                                                                                        }
                                                                                    }
                                                                                    var cmbPartida=gEx('cmbPartida');
                                                                                    var idPartida=cmbPartida.getValue();
                                                                                    var cadObj='{"idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+'","idPartida":"'+idPartida+'"}';
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                	var almacen=cmbPartida.getStore();
                                                                                                	var pos=obtenerPosFila(almacen,'id',idPartida);
                                                                                                    almacen.getAt(pos).set('valorComp','1');
                                                                                                    inhabilitarBotones();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=28&cadObj='+cadObj,true);
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer cerrar la dictaminaci&oacute;n de propuestas de proveedores para esta partida?',resp);
                                                                                }
                                                                        }
                                                            		
                                                            <?php
																}
															?>
                                                         			],
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: false,
                                                                                                     enableGrouping :false
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );
		
    tblGrid.on('afteredit',funcAfterEdit);
    tblGrid.on('beforeedit',funcBeforeEdit);
    tblGrid.soloLectura=false;
    <?php
		if($sL)
			echo "tblGrid.soloLectura=true;";
	?>
	return 	tblGrid;
}

function funcBeforeEdit(e)
{
	e.cancel=e.grid.soloLectura;
}

function funcAfterEdit(e)
{
	if((e.value=='2')||(e.value=='1'))
    {
    	mostrarVentanaRegistroDictamen(e.record,e);
    }
    else
    {
    	var fila=e.record;
    	var cadObj='{"dictamen":"'+e.value+'","idDictamenPropuesta":"'+fila.get('idDictamenPropuesta')+'","idProveedor":"'+fila.get('idProveedor')+'","idFormulario":"'+gE('idFormulario').value+
                    '","idRegistro":"'+gE('idRegistro').value+'","idPartida":"'+gEx('cmbPartida').getValue()+'","comentarios":""}';
                                                                        
          function funcAjax()
          {
              var resp=peticion_http.responseText;
              arrResp=resp.split('|');
              if(arrResp[0]=='1')
              {
                  fila.set('idDictamenPropuesta',arrResp[1]);
                  fila.set('comentarios','');
                  window.parent.mostrarMenuDTD();
              }
              else
              {
                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
              }
          }
          obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=27&cadObj='+cadObj,true);
    }
}


function inhabilitarBotones()
{
	var btnModificar=gEx('btnCerrar');
    if(btnModificar!=null)
    {
    	btnModificar.disable();

    }
    
}

function habilitarBotones()
{
	var btnModificar=gEx('btnCerrar');
    if(btnModificar!=null)
    {
    	btnModificar.enable();

    }
    
}

function mostrarVentanaRegistroDictamen(fila,e)
{
	
    var lblEtiqueta='Motivo del rechazo';
	if(e.value=='1')
	     lblEtiqueta='Comentarios';
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:5,
                                                            y:10,
                                                            html:lblEtiqueta+':'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:5,
                                                            xtype:'textarea',
                                                            id:'txtComentarios',
                                                            value:fila.get('comentarios'),
                                                            width:420,
                                                            height:90
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblAplicacion,
										width: 600,
										height:190,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtComentarios').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
                                                                        var comentarios=gEx('txtComentarios');
                                                                        if(e.value=='2')
                                                                        {
                                                                            if(comentarios.getValue()=='')
                                                                            {
                                                                                function resp2()
                                                                                {
                                                                                    comentarios.focus();
                                                                                }
                                                                                msgBox('Debe indicar el motivo por el cual rechaza la propuesta seleccionada',resp2);
                                                                                return;
                                                                            }
																		}                                                                       
                                                                        
                                                                        var cadObj='{"dictamen":"'+e.value+'","idDictamenPropuesta":"'+fila.get('idDictamenPropuesta')+'","idProveedor":"'+fila.get('idProveedor')+'","idFormulario":"'+gE('idFormulario').value+
                                                                        			'","idRegistro":"'+gE('idRegistro').value+'","idPartida":"'+gEx('cmbPartida').getValue()+'","comentarios":"'+cv(comentarios.getValue())+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                fila.set('idDictamenPropuesta',arrResp[1]);
                                                                                fila.set('comentarios',comentarios.getValue());
                                                                                window.parent.mostrarMenuDTD();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesCompras.php',funcAjax, 'POST','funcion=27&cadObj='+cadObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	e.record.set('dictamen',e.originalValue);
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}
