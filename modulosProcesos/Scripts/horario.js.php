<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
?>


App = function() {
					

    				return 	{
        						init : 	function() 
                                		{
            								Ext.BLANK_IMAGE_URL = '../images/s.gif';
                                            this.calendarStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                storeId: 'calendarStore',
                                                                                                root: 'calendarios',
                                                                                                idProperty: 'id',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                autoLoad: true,
                                                                                                fields: [
                                                                                                            {name:'CalendarId', mapping: 'id', type: 'string'},
                                                                                                            {name:'Title', mapping: 'title', type: 'string'}
                                                                                                        ],
                                                                                                sortInfo: 	{
                                                                                                                field: 'CalendarId',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=13;
                                                                                                                                
                                
                                                                                                                            }
                                                                                                			}
                                                                                            }
                                                                                         );
                                            
                                            this.eventStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                id: 'eventStore',
                                                                                                root: 'evts',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                fields: Ext.calendar.EventRecord.prototype.fields.getRange(),
                                                                                                sortInfo: 	{
                                                                                                                field: 'StartDate',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=14;
                                                                                                                                proxy.baseParams.idFormulario=gE('idFormulario').value;
                                                                                                                                proxy.baseParams.idReferencia=gE('idRegistro').value;
                                                                                                                                proxy.baseParams.rO=gE('rO').value;
                                                                                                                                
                                	
                                                                                                                            }	
                                                                                                			}
                                                                                            }
                                                                                       );
            								new Ext.Viewport	(
                                                                    {
                                                                        layout: 'border',
                                                                        renderTo: 'calendario',
                                                                        items: 	[
                                                                                    
                                                                                    {
                                                                                        id: 'app-center',
                                                                                        region: 'center',
                                                                                        layout: 'border',
                                                                                        items: 	[
                                                                                                    
                                                                                                    {
                                                                                                        xtype: 'calendarpanel',
                                                                                                        eventStore: this.eventStore,
                                                                                                        calendarStore: this.calendarStore,
                                                                                                        border: false,
                                                                                                        id:'app-calendar',
                                                                                                        region: 'center',
                                                                                                        activeItem: 1, // month view
                                                                                                        monthViewCfg:	{
                                                                                                                            showHeader: false,
                                                                                                                            showWeekLinks: true,
                                                                                                                            showWeekNumbers: false
                                                                                                                        },
                                                                                                        weekViewCfg:	{
                                                                                                                            showHeader: false,
                                                                                                                            showWeekLinks: false,
                                                                                                                            showWeekNumbers: false
                                                                                                                        },
                                                        												showDayView: false,
                                                                                                        showWeekView: true,
                                                                                                        showMonthView:false,
                                                                                                        showNavBar: false,
                                                                                                        showTodayText: false,
                                                                                                        initComponent: 	function()
                                                                                                                        {
                                                                                                                            App.calendarPanel = this;
                                                                                                                            this.constructor.prototype.initComponent.apply(this, arguments);
                                                                                                                        },
                                                        
                                                                                                        listeners: 	{
                                                                                                                        'dayclick': 	{
                                                                                                                                            fn: function(vw, dt, ad, el)
                                                                                                                                                {
                                                                                                                                                	if(rO=='1')
                                                                                                                                                    	return;
                                                                                                                                                	var et= dt.add(Date.HOUR, 1);
                                                                                                                                                    var ciclo=gE('ciclo').value;
                                                                                                                                                    var obj='{"idPeriodo":"'+gE('idPeriodo').value+'","ciclo":"'+ciclo+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                                                                    '","fechaInicio":"'+dt.format('Y-m-d H:i')+'","fechaFin":"'+et.format('Y-m-d H:i')+'"}';
                                                                                                                                                    function funcAjax()
                                                                                                                                                    {
                                                                                                                                                        var resp=peticion_http.responseText;
                                                                                                                                                        arrResp=resp.split('|');
                                                                                                                                                        if(arrResp[0]=='1')
                                                                                                                                                        {
                                                                                                                                                        	if(gEx('app-calendar').eventStore.getCount()==0)
	                                                                                                                                                        	window.parent.mostrarMenuDTD();
                                                                                                                                                            recargarPagina();
                                                                                                                                                            return;
                                                                                                                                                            var M = Ext.calendar.EventMappings;
                                                                                                                                                            rec = new Ext.calendar.EventRecord();
                                                                                                                                                            rec.data[M.Title.name] = 'Disponible <a href="javascript:removerBloque(\''+bE(arrResp[1])+'\')"><img src="../images/delete.png" title="Remover bloque" alt="Remover bloque"/></a>';
                                                                                                                                                            rec.data[M.CalendarId.name] =1;
                                                                                                                                                            rec.data[M.EventId.name] = arrResp[1];
                                                                                                                                                            rec.data[M.StartDate.name] =dt;
                                                                                                                                                            rec.data[M.EndDate.name] = et;
                                                                                                                                                            rec.data[M.IsAllDay.name] = 0;
                                                                                                                                                            rec.data[M.IsNew.name] = true;
                                                                                                                                                            gEx('app-calendar').eventStore.add(rec);
                                                                                                                                                            if(gEx('app-calendar').eventStore.getCount()==1)
                                                                                                                                                            {
                                                                                                                                                            	window.parent.mostrarMenuDTD();
                                                                                                                                                            }
                                                                                                                                                            onComplete();
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                                                                            
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=15&obj='+obj,true);
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'rangeselect': {
                                                                                                                                            fn: function(win, dates, onComplete)
                                                                                                                                                {

																																					if(rO=='1')
                                                                                                                                                    {
                                                                                                                                                    	onComplete();
                                                                                                                                                    	return;
                                                                                                                                                    }
                                                                                                                                                	var et= dates.EndDate;
                                                                                                                                                    var dt=dates.StartDate;
                                                                                                                                                    var ciclo=gE('ciclo').value;
                                                                                                                                                    
                                                                                                                                                    var obj='{"idPeriodo":"'+gE('idPeriodo').value+'","ciclo":"'+ciclo+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                                                                    '","fechaInicio":"'+dt.format('Y-m-d H:i')+'","fechaFin":"'+et.format('Y-m-d H:i')+'"}';

                                                                                                                                                    function funcAjax()
                                                                                                                                                    {
                                                                                                                                                        var resp=peticion_http.responseText;
                                                                                                                                                        arrResp=resp.split('|');
                                                                                                                                                        if(arrResp[0]=='1')
                                                                                                                                                        {
                                                                                                                                                        	if(gEx('app-calendar').eventStore.getCount()==0)
	                                                                                                                                                        	window.parent.mostrarMenuDTD();
                                                                                                                                                            recargarPagina();
                                                                                                                                                            return;
                                                                                                                                                            var M = Ext.calendar.EventMappings;
                                                                                                                                                            rec = new Ext.calendar.EventRecord();
                                                                                                                                                            rec.data[M.Title.name] = 'Disponible <a href="javascript:removerBloque(\''+bE(arrResp[1])+'\')"><img src="../images/delete.png" title="Remover bloque" alt="Remover bloque"/></a>';
                                                                                                                                                            rec.data[M.CalendarId.name] =1;
                                                                                                                                                            rec.data[M.EventId.name] = arrResp[1];
                                                                                                                                                            rec.data[M.StartDate.name] =dt;
                                                                                                                                                            rec.data[M.EndDate.name] = et;
                                                                                                                                                            rec.data[M.IsAllDay.name] = 0;
                                                                                                                                                            rec.data[M.IsNew.name] = true;
                                                                                                                                                            gEx('app-calendar').eventStore.add(rec);
                                                                                                                                                            if(gEx('app-calendar').eventStore.getCount()==1)
                                                                                                                                                            {
                                                                                                                                                            	window.parent.mostrarMenuDTD();
                                                                                                                                                            }
                                                                                                                                                            onComplete();
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                                                                            
                                                                                                                                                        }
                                                                                                                                                	}
                                                                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=15&obj='+obj,true);
                                                                                                                                                    
                                                                                                                                                    
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventmove': 	{
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	if(rO=='1')
                                                                                                                                                {
                                                                                                                                                	rec.reject();
                                                                                                                                                    return;
                                                                                                                                                }
                                                                                                                                            	var obj='{"idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                                                                    '","idRegistro":"'+rec.data.EventId+'","fechaInicio":"'+rec.data.StartDate.format('Y-m-d H:i')+'","fechaFin":"'+rec.data.EndDate.format('Y-m-d H:i')+'"}';
                                                                                                                                                function funcAjax()
                                                                                                                                                    {
                                                                                                                                                        var resp=peticion_http.responseText;
                                                                                                                                                        arrResp=resp.split('|');
                                                                                                                                                        if(arrResp[0]=='1')
                                                                                                                                                        {
                                                                                                                                                            rec.commit();
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                                                                            rec.reject();
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=16&obj='+obj,true);
                                                                                                                                            	
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventresize': {
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	if(rO=='1')
                                                                                                                                                {
                                                                                                                                                	rec.reject();
                                                                                                                                                    return;
                                                                                                                                                }
                                                                                                                                            	var obj='{"idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                                                                    '","idRegistro":"'+rec.data.EventId+'","fechaInicio":"'+rec.data.StartDate.format('Y-m-d H:i')+'","fechaFin":"'+rec.data.EndDate.format('Y-m-d H:i')+'"}';
                                                                                                                                                function funcAjax()
                                                                                                                                                    {
                                                                                                                                                        var resp=peticion_http.responseText;
                                                                                                                                                        arrResp=resp.split('|');
                                                                                                                                                        if(arrResp[0]=='1')
                                                                                                                                                        {
                                                                                                                                                            rec.commit();
                                                                                                                                                        }
                                                                                                                                                        else
                                                                                                                                                        {
                                                                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                                                                            rec.reject();
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=16&obj='+obj,true);
                                                                                                                                                
                                                                                                                                            	
                                                                                                                                            	
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventdelete': {
                                                                                                                                            fn: function(win, rec)
                                                                                                                                            {
                                                                                                                                                this.eventStore.remove(rec);
                                                                                                                                                
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        }
                                                                                                                       
                                                                                                                    }
                                                                                                    }
                                                                                                ]
                                                                                    }
                                                                                ]
                                                                    }
                                                               )
                                                               var fechaInicial=new Date(2011,5,6);
                                               					gEx('app-calendar').setStartDate(fechaInicial);
                                                                var rO=gE('rO').value;
                                            
                                        },
        
                                
                                
                                updateTitle: function(startDt, endDt)
                                			{
                                                var p = Ext.getCmp('app-center');
                                                
                                                if(startDt.clearTime().getTime() == endDt.clearTime().getTime())
                                                {
                                                    p.setTitle(startDt.format('F j, Y'));
                                                }
                                                else
                                                	if(startDt.getFullYear() == endDt.getFullYear())
                                                    {
                                                        if(startDt.getMonth() == endDt.getMonth())
                                                        {
                                                            p.setTitle(startDt.format('F j') + ' - ' + endDt.format('j, Y'));
                                                        }
                                                    	else
                                                        {
                                                        	p.setTitle(startDt.format('F j') + ' - ' + endDt.format('F j, Y'));
                                                    	}
                                                	}
                                                	else
                                                    {
                                                    	p.setTitle(startDt.format('F j, Y') + ' - ' + endDt.format('F j, Y'));
                                                	}
                                            },
                                showMsg: function(msg)
                                		{
                                           
                                        },
                                clearMsg: function()
                                        {
                                           
                                        }
    						}
				}();

Ext.onReady(App.init, App);                                   

function removerBloque(iB)
{
	
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                 	var almacen=gEx('app-calendar').eventStore   ;
                    var pos=obtenerPosFila(almacen,'EventId',bD(iB));
                    var fila=almacen.getAt(pos);
                    almacen.remove(fila);
                    if(gEx('app-calendar').eventStore.getCount()==0)
                      {
                          window.parent.mostrarMenuDTD();
                      }
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=17&idRegistro='+bD(iB),true);
            
            

        }
    }
    msgConfirm('Est&aacute; seguro de querer remover el bloque seleccionado?',resp)
}
