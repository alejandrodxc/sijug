<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$plantel=$_SESSION["codigoInstitucion"];
	$consulta="SELECT id__871_tablaDinamica,txtTipo FROM _871_tablaDinamica ORDER BY txtTipo";
	$arrTipoMovimiento=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__743_tablaDinamica,txtNombre FROM _743_tablaDinamica ORDER BY txtNombre";
	$arrMotivosJustificacion=$con->obtenerFilasArreglo($consulta);
	
?>
var swfu=null;
var idAlumno=-1;
var arrTipoMovimiento=<?php echo $arrTipoMovimiento?>;
var arrMotivosJustificacion=<?php echo $arrMotivosJustificacion?>;
var idComprobante=-1;
var nombreDocumentoComprobante='';

Ext.onReady(inicializar);

var nodoSel=null;
var nodoTemarioSel=null;
var filaSesion=null;
function inicializar()
{
	
	Ext.QuickTips.init();
	var detailEl;
    
    

	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
											{
												baseParams:{
																funcion:'62'
															},
												dataUrl:'../paginasFunciones/funcionesProgramaAcademicoV2.php'
											}
										)		
										
											
	cargadorArbol.on('beforeload',function(proxy)
    							{
                                	proxy.baseParams.idFormulario=gE('idFormulario').value;
                                    proxy.baseParams.idRegistro=gE('idReferencia').value;
                                    if(gE('sL').value=='1')
                                    {
                                    	proxy.baseParams.funcion=64;
                                    }
                                }
    				)										
	cargadorArbol.on('load',function()
    					{
                        	if(gE('sL').value=='0')
                            {
                                if(gEx('arbolGeneral').getRootNode().childNodes.length>0)
                                {
                                    gEx('btnRegistraAsistencia').enable();
                                    gEx('btnChkYes').enable();
                                    gEx('btnChkNo').enable();
                                }
                                else
                                {
                                    gEx('btnRegistraAsistencia').disable();
                                    gEx('btnChkYes').disable();
                                    gEx('btnChkNo').disable();
                                }
                        	}
                        }
    				)  
    var arrBotones=[];   
    var lblTitulo='Sesiones cuya asistencia se modifica mediante este registro';             
    if(gE('sL').value=='0')
    {
    	lblTitulo='Selecione las sesiones cuya asistencia desea modificar mediante este registro';    
	 	arrBotones=	[
    					{
                            id:'btnRegistraAsistencia',
                            icon:'../images/guardar.PNG',
                            disabled:true,
                            cls:'x-btn-text-icon',
                            text:'Registrar modificaci&oacute;n',
                            handler:function()
                                    {
                                        var arrSesiones=arbolOpciones.getChecked();
                                        if(arrSesiones.length==0)
                                        {
                                            msgBox('Al menos debe seleccionar una sesi&oacute;n para registrar la modificaci&oacute;n de asistencia');
                                            return;
                                        }
                                        
                                        guardarDatosMovimiento(); 
                                        
                                    }
                            
                        },
                        {xtype: 'tbspacer',width:50},
                        '-',
                        {
                            id:'btnChkYes',
                            disabled:true,
                            icon:'../images/chkYes.png',
                            cls:'x-btn-text-icon',
                            text:'Seleccionar todos los alumnos',
                            handler:function()
                                    {
                                        checarNodosHijos(arbolOpciones.getRootNode(),true);
                                    }
                            
                        },'-',
                        {
                            id:'btnChkNo',
                            disabled:true,
                            icon:'../images/chkNo.png',
                            cls:'x-btn-text-icon',
                            text:'Deseleccionar todos los alumnos',
                            handler:function()
                                    {
                                        checarNodosHijos(arbolOpciones.getRootNode(),false);
                                    }
                            
                        }
    				]  
	}                  
                      					
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															id:'arbolGeneral',
                                                            border:false,
                                                            frame:false,
                                                            height:obtenerDimensionesNavegador()[0]-30,
                                                            containerScroll: true,
                                                            autoScroll: true,
															region:'center',
															animate:true,
															root:raiz,
															loader: cargadorArbol,
															rootVisible:false,
                                                            tbar:	[
                                                            			arrBotones
                                                            		]
														}
													)
	
							

                               
	var contentPanel = 
                        {
                            id: 'content-panel',
                            region: 'center', 
                            layout: 'Border',
                            margins: '2 5 5 0',
                            activeItem: 0,
                            border: false,
                            items: 	[
                                     	{
                                        	xtype:'panel',
                                            region:'center',
                                            items:[arbolOpciones]
                                        }  
                                    ]
                        };
    new Ext.Viewport(	
    
    					{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:[
                                            		
                                                    {
                                                       
                                                        xtype:'label',
                                                        html:'<span class="letraRojaSubrayada8" style="font-size:14px"><b>'+lblTitulo+'</b></span>&nbsp;&nbsp;&nbsp;'
                                                    }
                                            		
                                                  ],
                                            items:	[
                                            			
                                                        contentPanel
                                                    
                                            		]
                                        }
                                    ]
                        }
    					
                    );	
	
  
}



function actualizarDatosAsistencia()
{
	gEx('arbolGeneral').getRootNode().reload();	
}

function guardarDatosMovimiento()
{
	var arrSesiones=gEx('arbolGeneral').getChecked();
   	var arrAsistencia='';
    var x;
    var obj;
    for(x=0;x<arrSesiones.length;x++)
    {
    	if(arrAsistencia=='')
        	arrAsistencia=arrSesiones[x].id;
        else
        	arrAsistencia+=','+arrSesiones[x].id;
    	
    }
	var cadObj='{"idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idReferencia').value+'","arrAsistencia":"'+arrAsistencia+'"}';

	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	
        	if((window.parent)&&(window.parent.mostrarMenuDTD))
            	window.parent.mostrarMenuDTD()
            msgBox('La informaci&oacute;n ha sido registrada satisfactoriamente');
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=63&cadObj='+cadObj,true);

}