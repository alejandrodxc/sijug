<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	Ext.QuickTips.init();
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)

	var funcionQuery=11;
    if(gE('sL').value=='1')
    	funcionQuery=27;
										
	var cargadorArbol=new Ext.tree.TreeLoader(
											{
												baseParams:{
																funcion:funcionQuery
															},
												dataUrl:'../paginasFunciones/funcionesModulosProcesos.php'
											}
										)		
										
	cargadorArbol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idReferencia=gE('idReferencia').value;
                                        proxy.baseParams.sL=gE('sL').value;
                                    }
                     )  											
										
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															title:'<span class="letraRojaSubrayada8">Seleccione las sesiones de clase que ser&aacute;n afectadas por este proceso:</span>',
															id:'arbolSesiones',
															el:'tblSesiones',
															height:400,
															width:850,
                                                            border:true,
                                                            //frame:true,
															useArrows:true,
															autoScroll:true,
															animate:true,
															enableDD:true,
															containerScroll: true,
															root:raiz,
															loader: cargadorArbol,
															rootVisible:false
														}
													)
	arbolOpciones.render();			
	arbolOpciones.expandAll();				
//	arbolOpciones.on('click',funcClikArbol);
}

function guardarDatos()
{
	var arbol=gEx('arbolSesiones');
    var arrNodos=arbol.getChecked();
    var x;
    var nodo;
    var cadObj='';
    var obj;
    for(x=0;x<arrNodos.length;x++)
    {
    	nodo=arrNodos[x];
        if(nodo.attributes.idGrupo!=undefined)
        {
        	obj='{"idGrupo":"'+nodo.attributes.idGrupo+'","hInicio":"'+nodo.attributes.horaInicio+'","hFin":"'+nodo.attributes.horaFin+'","fechaSesion":"'+nodo.attributes.fecha+'","noSesion":"'+nodo.attributes.noSesion+'"}';
            if(cadObj=='')
            	cadObj=obj;
            else
            	cadObj+=","+obj;
        }	
    }
	cadObj='{"idReferencia":"'+gE('idReferencia').value+'","idFormulario":"'+gE('idFormulario').value+'","arrSesiones":['+cadObj+']}'; 
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        
            msgBox('La informaci&oacute;n ha sido registrada exitosamente');
            if(window.parent.mostrarMenuDTD!=undefined)
	            window.parent.mostrarMenuDTD();
            return;
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=12&cadObj='+cadObj,true);
     
}


function nodoCheck(nodo,valor)
{
	checarNodosHijos(nodo,valor);
}