<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama  WHERE institucion=1 ORDER BY unidad";
	$arrPlanteles=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT valor,texto FROM 1004_siNo WHERE idIdioma=1";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
?>
var arrSiNo=<?php echo $arrSiNo?>;
var regMateria;
var regEval=null;
var regPlan=null;
var arrPlanteles=<?php echo $arrPlanteles?>	;
    
Ext.onReady(inicializar);

function inicializar()
{

	var cmbContinuar=crearComboExt('cmbContinuar',arrSiNo,280,5,115);
	var arrGrados=eval(bD(gE('arrGrados').value));
	regMateria=crearRegistro(	[
    								{name:'idMateria'},
                                    {name: 'cveMateria'},
                                    {name: 'nombreMateria'}
                                 ]
                            )

	regPlan=crearRegistro(	[
    							{name:'idPlanEstudioEscuelas'},
		                        {name: 'rvoe'},
		                        {name:'nombrePlanEstudios'},
                                {name: 'descripcion'}
                            ]
                          )
	regEval=crearRegistro([
                              {name: 'idMateriasPlanOrigen'},
                              {name: 'materiasPlanOrigen'},
                              {name: 'idMateriasPlanDestino'},
                              {name: 'materiasPlanDestino'},
                              {name: 'calificacion'},
                              {name: 'nodoAsignado'},
                              {name: 'modificable'}
                              
                          ])
	var gridMaterias=crearGridMaterias();
    
	var lblSituacion='En espera de pago de tr&aacute;mite&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:descargarFormaPago()"><img src="../images/download.png" /> Descargar formato de pago</a>';
    var tamTab=50;
    var mostrarGrado=true;
    var lblGrado='';
    var ajuste=8;
    var respuestDictamen=true;
    if(gE('idEstado').value!='0')
    {
    	ajuste=0;
    	lblSituacion=gE('lblSituacion').value;
        if(parseInt(gE('idEstado').value)!='2')
        {
            tamTab=350;
            mostrarGrado=false;
	        lblGrado=formatearValorRenderer(arrGrados,gE('gradoInscribe').value);
            if(parseInt(gE('idEstado').value)=='3')
            {
            	respuestDictamen=false;
                 tamTab=350;
            }
    	}    
    }
     new Ext.form.FieldSet(
                               {
                                    
                                    width:980,
                                    height:tamTab,
                                    layout:'absolute',
                                    xtype:'fieldset',
                                    renderTo:'tblSituacion',
                                    title:'Situaci&oacute;n de la solicitud:',
                                    items:	[
                                                {
                                                	x:10,
                                                    y:10,
                                                    style: {color:'#000', fontWeight:'bold'},
                                                    xtype:'label',
                                                    html:'Situaci&oacute;n:'
                                                },
                                                {
                                                	x:90,
                                                    y:10-ajuste,
                                                    style: {color:'#900', fontWeight:'bold'},
                                                    xtype:'label',
                                                    html:lblSituacion
                                                },
                                                {
                                                	x:300,
                                                    y:10,
                                                    hidden:mostrarGrado,
                                                    style: {color:'#000', fontWeight:'bold'},
                                                    xtype:'label',
                                                    html:'Grado al que inscribe:'
                                                },
                                                 {
                                                	x:440,
                                                    y:10,
                                                    hidden:mostrarGrado,
                                                    style: {color:'#900', fontWeight:'bold'},
                                                    xtype:'label',
                                                    html:lblGrado
                                                },
                                                {
                                                	x:10,
                                                    y:40,
                                                    style: {color:'#000', fontWeight:'bold'},
                                                    xtype:'label',
                                                    hidden:mostrarGrado,
                                                    html:'Comentarios:'
                                                },
                                                {
                                                	x:90,
                                                    y:40,
                                                    id:'txtComentarios',
                                                    xtype:'textarea',
                                                    height:90,
                                                    width:600,
                                                    value:bD(gE('comentarios').value),
                                                    hidden:mostrarGrado,
                                                    readOnly:true
                                                },
                                                
                                                {
                                                	x:10,
                                                    y:145,
                                                    width:900,
                                                    height:150,
                                                    layout:'absolute',
                                                    xtype:'fieldset',
                                                    hidden:respuestDictamen,
                                                    title:'Respuesta a dict&aacute;men',
                                                    items:	[
                                                    			{
                                                                    x:10,
                                                                    y:10,
                                                                    style: {color:'#000', fontWeight:'bold'},
                                                                    xtype:'label',
                                                                    hidden:mostrarGrado,
                                                                    html:'¿Desea continuar con el proceso de inscripci&oacute;n:?'
                                                                },
                                                                cmbContinuar,
                                                                {
                                                                	xtype:'panel',
                                                                    x:410,
                                                                    y:5,
                                                                    width:200,
                                                                    border:false,
                                                                    height:50,
                                                                    items:	[
                                                                    			{
                                                                                	xtype:'button',
                                                                                    width:110,
                                                                                    icon:'../images/user_go.png',
                                                                                    cls:'x-btn-text-icon',
                                                                                    text:'Enviar respuesta',
                                                                                    handler:function()
                                                                                            {
                                                                                            	if(cmbContinuar.getValue()=='')
                                                                                                {
                                                                                                	msgBox('Debe indicar la respuesta a enviar ante el resultado del dict&aacute;men de revalidaci&oacute;n/equivalencia de materias');
                                                                                                	return;
                                                                                                }
                                                                                                function resp(btn)
                                                                                                {
                                                                                                	if(btn=='yes')
                                                                                                    {
                                                                                                    	function funcAjax()
                                                                                                        {
                                                                                                            var resp=peticion_http.responseText;
                                                                                                            arrResp=resp.split('|');
                                                                                                            if(arrResp[0]=='1')
                                                                                                            {
																												window.parent.close();																						
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            }
                                                                                                        }
                                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=109&idRegistro='+gE('idRegistroInscripcion').value+'&respuesta='+cmbContinuar.getValue()+'&comentarios='+cv(gEx('txtComentarios').getValue()),true);
                                                                                                        
                                                                                                    }
                                                                                                }
                                                                                                msgConfirm('¿Est&aacute; seguro de querer enviar la respuesta al dict&aacute;men de revalidaci&oacute;n/equivalencia de materias?',resp);
                                                                                            }
                                                                                    
                                                                                }
                                                                    		]
                                                                },
                                                                {
                                                                    x:10,
                                                                    y:40,
                                                                    style: {color:'#000', fontWeight:'bold'},
                                                                    xtype:'label',
                                                                    hidden:mostrarGrado,
                                                                    html:'Comentarios adicionales:'
                                                                },
                                                                {
                                                                    x:200,
                                                                    y:35,
                                                                    xtype:'textarea',
                                                                    height:90,
                                                                    width:600
                                                                    
                                                                }
                                                    		]
                                                    
                                                }
                                                
                                            ]
                                    
                                }
                             )
    
   
    new Ext.form.FieldSet(
                           {
                                
                                width:980,
                                height:520,
                                layout:'absolute',
                                xtype:'fieldset',
                                renderTo:'tblRevalidacion',
                                hidden:mostrarGrado,
                                title:'Resultado de Dict&aacute;men de la solicitud:',
                                items:	[
                                			{
                                                xtype:'panel',
                                                width:950,
                                                layout:'absolute',
                                                border:false,
                                                height:500,
                                                items:	[
                                                            {
                                                                xtype:'panel',
                                                                layout:'absolute',
                                                                height:500,
                                                                border:false,
                                                                width:950,
                                                                items:	[
                                                                            gridMaterias
                                                                        ]
                                                        	}
                                                        ]
                                        	}
                                        ]
                                
                            }
                         )
	
    new Ext.form.FieldSet(
                           {
                                
                                width:980,
                                height:160,
                                layout:'absolute',
                                xtype:'fieldset',
                                
                                renderTo:'tblDatosRevalidacion',
                                title:'Datos del plan de estudio:',
                                items:	[
                                			{
                                            	x:10,
                                                y:5,
                                                xtype:'label',
                                                style: {color:'#000', fontWeight:'bold'},
                                                html:'Nombre del Solicitante:'
                                            },
                                            {
                                            	x:140,
                                                y:5,
                                                id:'lblSolicitante',
                                                xtype:'label',
                                                html:''
                                            },
                                            {
                                            	x:10,
                                                y:35,
                                                style: {color:'#000', fontWeight:'bold'},
                                                xtype:'label',
                                                html:'Escuela de origen:'
                                            },
                                            {
                                            	x:140,
                                                y:35,
                                                id:'lblEO',
                                                xtype:'label',
                                                html:''
                                            },
                                            {
                                            	x:10,
                                                y:65,
                                                style: {color:'#000', fontWeight:'bold'},
                                                xtype:'label',
                                                html:'Plan de estudios de origen:'
                                            },
                                            {
                                            	x:160,
                                                y:65,
                                                height:60,
                                                width:780,
                                                id:'lblPEO',
                                                xtype:'label',
                                                html:''
                                            },
                                             
                                            {
                                            	x:10,
                                                y:110,
                                                style: {color:'#000', fontWeight:'bold'},
                                                xtype:'label',
                                                html:'Plan de estudios al cual solicita equivalencia:'
                                            },
                                            {
                                            	x:190,
                                                y:110,
                                                id:'lblPED',
                                                xtype:'label',
                                                html:''
                                            }
                                        ]
                                
                            }
                         )                         
    
    
    gEx('lblSolicitante').setText('<span class="letraRojaSubrayada8">'+bD(gE('nSolicitante').value)+'</span>',false);
    var nEscuelaOrigen=bD(gE('nEscuelaOrigen').value);
    gEx('lblEO').setText('<span class="corpo8_bold" alt="'+nEscuelaOrigen+'" title="'+nEscuelaOrigen+'">'+Ext.util.Format.ellipsis(nEscuelaOrigen,90)+'</span>',false);
    var nPlanEstudio=bD(gE('nPlanEstudio').value);
    gEx('lblPEO').setText('<span class="corpo8_bold" alt="'+nPlanEstudio+'" title="'+nPlanEstudio+'">'+nPlanEstudio+'</span>',false);
    var nPlanEstudioDestino=bD(gE('nPlanEstudioDestino').value);
    gEx('lblPED').setText('<span class="corpo8_bold" alt="'+nPlanEstudioDestino+'" title="'+nPlanEstudioDestino+'">'+Ext.util.Format.ellipsis(nPlanEstudioDestino,90)+'</span>',false);
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            autoScroll :true,
                                            contentEl:'tRevalidacion'
                                        }
                                     ]
						}
                    )   
}


function crearGridMaterias()
{
	
    var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idMateriasPlanOrigen'},
                                                        {name: 'materiasPlanOrigen'},
                                                        {name: 'idMateriasPlanDestino'},
                                                        {name: 'materiasPlanDestino'},
                                                        {name: 'calificacion'},
                                                        {name: 'nodoAsignado'},
                                                        {name: 'modificable'},
                                                        {name: 'situacion'},
                                                        {name: 'idRevalidacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
    
    var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'materiasPlanOrigen', direction: 'ASC'},
                                                            groupField: 'materiasPlanOrigen',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
    
    alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='89';
                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
                                        
                                    }
                        )  
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();

	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	{
                                                        	header:'Situaci&oacute;n',
															width:80,
                                                            dataIndex:'situacion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var imgSrc='';
                                                                        var titulo='';
                                                                    	switch(val)
                                                                        {
                                                                        	case '0':  //Sin resultado
                                                                            	titulo='No Evaluado';
                                                                                imgSrc='../images/control_pause.png';
                                                                                
                                                                                return '<img src="'+imgSrc+'" alt="'+titulo+'" title="'+titulo+'" width="13" height="13">';
                                                                            break;
                                                                            case '1':	//Aceptado/aprobado
                                                                            	titulo='Aprobado';
                                                                                imgSrc='../images/icon_big_tick.gif';
                                                                                return '<img src="'+imgSrc+'" alt="'+titulo+'" title="'+titulo+'" width="13" height="13">';
                                                                            break;
                                                                            case '2':	//No aprobado
                                                                            	titulo='No Aprobado, la materia no cumple con la calificaci&oacute;n m&iacute;nima requerida';
                                                                                imgSrc='../images/cross.png';
                                                                                return '<img src="'+imgSrc+'" alt="'+titulo+'" title="'+titulo+'" width="13" height="13">';
                                                                            break;
                                                                            case '3':    //Aprobacion condicionada a otras materias predecesoras
                                                                            	imgSrc='../images/exclamation.png';
                                                                            	titulo='No aprobado por no cubrir materias requisito';
                                                                                return '<a href="javascript:mostrarMateriasRequisitos(\''+bE(registro.get('idRevalidacion'))+'\',\''+bE(registro.get('materiasPlanDestino'))+'\')"><img src="'+imgSrc+'" alt="'+titulo+'" title="'+titulo+'" width="13" height="13"></a>';
                                                                            break;
                                                                        }
                                                                        
                                                                    }
                                                                    
                                                            
                                                        },
													 	{
															header:'Materias del Plan de estudio origen',
															width:320,
															sortable:false,
															dataIndex:'materiasPlanOrigen',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
                                                            
														},
                                                        {
															header:'Calificaci&oacute;n',
															width:70,
															sortable:false,
                                                            css:'text-align:right !important;',
															dataIndex:'calificacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '0.00';
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimal:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Materias del Plan de estudio a ingresar',
															width:320,
															sortable:false,
															dataIndex:'materiasPlanDestino',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '';
                                                                        return mostrarValorDescripcion(val);
                                                                    }
														}
                                                         
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridRevalidacion',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,
                                                            clicksToEdit:1,
                                                            height:430,
                                                            width:800,
                                                            title:'Listado de materias evaluadas'
                                                            
                                                            
                                                        }
                                                    );
	
    
                                   
	return 	tblGrid;	
}

function mostrarMateriasRequisitos(idRevalidacion,lblMateria)
{
	var gridMaterias=crearGridMateriasDependencia(bD(idRevalidacion));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span class="letraVino12N" style="font-size:11px !important">Materias requeridas para aprobar la materia: </span>&nbsp;&nbsp;<span class="negrita12" style="font-size:11px !important">'+bD(lblMateria)+'</span>'
                                                        },
                                                        gridMaterias
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Requisitos no cumplidos',
										width: 600,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridMateriasDependencia(idRevaliacion)
{
 	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idMateria'},
		                                                {name: 'nombreMateria'}
		                                                
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'nombreMateria', direction: 'ASC'},
                                              groupField: 'nombreMateria',
                                              remoteGroup:false,
                                              remoteSort: false,
                                              autoLoad:true
                                              
                                          }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='91';
                                        
                                        proxy.baseParams.idRevaliacion=idRevaliacion;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Materia Prerequisito',
                                                                width:480,
                                                                sortable:true,
                                                                dataIndex:'nombreMateria'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridMateriasRequeridas',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                x:10,
                                                                y:40,
                                                                height:320,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function descargarFormaPago()
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var ref=arrResp[1];
            window.parent.obtenerFormatoPagoReferenciado(ref);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProyectos.php',funcAjax, 'POST','funcion=338&idConcepto=17&idFormulario=678&idRegistro='+gE('idReferencia').value,true);
}