<?php 
include("latis/conexionBD.php"); 
include("latis/conexionBDGalileo.php"); 
$arrConfiguraciones="";
$actorEtapa1="";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<link rel="stylesheet" type="text/css" href="../estilos/layout-browser.css"/>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/checkColumn.js"></script>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<style type="text/css">
<!--
@import url("../css/estiloFinal.css");
-->
</style>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
$paramPOST=true;
$paramGET=true;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}

$fechaSesion="";
if(isset($objParametros->fechaSesion))
	$fechaSesion=$objParametros->fechaSesion;


$idCurso=2;


?>
<title><?php echo $tituloPagina ?></title>
</head>
<body style="background-color:#FFF">
    	<table width="100%">
        <tr height="10">
        	<td align="center">
            <script type="text/javascript" src="../modulosProcesos/Scripts/listaAsistenciaV.js.php"></script>
              <br />
              
              <table width="800">
              <tr>
              		<td valign="top">
                    	<img src="../images/foto_galileo.png" />
                    
                    </td>
                    <td width="10">
                    </td>
                  <td align="left">
                      
                      <fieldset class="frameHijo"><legend>Lista de asistencia</legend>
                          <table>
                          <tr>
                          	<td width="300">
                             <span class="corpo8_bold">Sesión:</span>
                            </td>
                            <td>
                            	<select id="cmbSesion"   onchange="fechaSesionChange(this)" >
                                      <option value="-1">Seleccione</option>
                                          <?php
                                             $consulta="SELECT g.fechaSesion,DATE_FORMAT(g.fechaSesion,'%d/%m/%Y') AS fecha FROM _348_tablaDinamica t,_348_gridFechasSesion g WHERE t.idReferencia=".$idCurso." AND g.idReferencia=t.id__348_tablaDinamica";
											  $con->generarOpcionesSelect($consulta,date("Y-m-d"));
                                          ?>
                                     </select>
                            </td>
                          </tr>
                          
                          <tr id="filaCategoria" >
                              <td align="left">
                                      <span class="corpo8_bold">Elija el municio en el cual se encuentre su plantel:</span>
                                  </td>
                                  <td ><select id="cmbCategoria"  disabled="disabled" onchange="obtenerPlanteles(this)">
                                          <option value="-1">Seleccione</option>
                                          <?php
                                              $consulta="SELECT OID_O,Nombre FROM geos_oid_c2 WHERE CVE_Ent=30 ORDER BY Nombre";
                                              $conGalileo->generarOpcionesSelect($consulta);
                                          ?>
                                      </select>
                                  </td>
                                  <td>
                                  </td>
                          </tr>
                         
                          
                          <tr height="21">
                                  <td  align="left" >
                                      <span class="corpo8_bold">Seleccione su plantel:</span>
                                  </td>
                                  <td  align="left">
                                      
                                      <select id="cmbPlantel" disabled="disabled" onchange="obtenerGrupos(this)"></select>
                                  </td>
                                  <td >
                                      
                                  </td>
                              </tr>
                              
                               </table>
                          
                         
                          
                      </fieldset>
                      <br /><br />
                      <fieldset id="gridDetalle" class="frameHijo"  ><legend>Lista de asistencia</legend>
                          <table>
                              <tr height="21" >
                                  <td width="150" align="left">
                                      <br /><span class="letraRoja" id='filaInstrucciones'><span style="font-size:11px !important">Para registrar la asistencia primero de click sobre su nombre en listado siguiente</span></span><br /><br />
                                      <span id="tblAsistencia"></span>
                                  </td>
                              </tr>
                          </table>
                      </fieldset>
                      <input type="hidden" id="idCurso" value="<?php echo $idCurso?>" />
                      <input type="hidden" id="plantel" value="" />
                      <input type="hidden" id="noSesion" value="" />
                      <input type="hidden" id="fechaSesion" value="<?php echo $fechaSesion?>" />
                      
                  </td>
                  
              </tr>
              </table>
            </td>
        </tr>
        
        </table>

    
    
    
    
</body>

</html>