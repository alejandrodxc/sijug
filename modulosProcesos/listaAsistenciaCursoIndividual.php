<?php 
include("latis/conexionBD.php"); 
$arrConfiguraciones="";
$actorEtapa1="";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<link rel="stylesheet" type="text/css" href="../estilos/layout-browser.css"/>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/checkColumn.js"></script>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<style type="text/css">
<!--
@import url("../css/estiloFinal.css");
-->
</style>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
$paramPOST=true;
$paramGET=true;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}

$fechaSesion="";
if(isset($objParametros->fechaSesion))
	$fechaSesion=$objParametros->fechaSesion;


$idCurso=-1;

if(isset($objParametros->c))
	$idCurso=bD($objParametros->c);

$consulta="SELECT Curso FROM _246_tablaDinamica WHERE id__246_tablaDinamica=".$idCurso;
$nCurso=$con->obtenerValor($consulta);
?>
<title><?php echo $tituloPagina ?></title>
</head>
<body style="background-color:#FFF">
    	<table width="100%">
        <tr height="10">
        	<td align="center">
            <script type="text/javascript" src="../modulosProcesos/Scripts/listaAsistenciaCursoIndividual.js.php"></script>
              <br />
              
              <table width="800">
              <tr>
              		<td valign="top">
                    	<img src="../images/foto_galileo.png" />
                    
                    </td>
                    <td width="10">
                    </td>
                  <td align="left">
                      
                      <fieldset class="frameHijo"><legend>Lista de asistencia</legend>
                          <table>
                          <tr>
                          	<td>
                             <span class="corpo8_bold">Curso:</span>
                            </td>
                            <td>
                            <span class="letraRojaSubrayada8"><?php echo $nCurso?></span>
                            </td>
                          </tr>
                          <tr height="22">
                              
                                  <td width="130" align="left">
                                      <span class="corpo8_bold">Buscar por:</span>
                                  </td>
                                  <td align="left" width="400"> 
                                      <input type="radio" value="1" name="buscar" checked="checked" onclick="cambiaBusqueda(this)" /> CCT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input onclick="cambiaBusqueda(this)" name="buscar" type="radio" value="2"  /> Plantel
                                  </td>
                                  <td>
                                  </td>

                          </tr>
                          <tr id="filaCategoria" style="display:none">
                              <td width="130" align="left">
                                      <span class="corpo8_bold">Subsistema:</span>
                                  </td>
                                  <td ><select id="cmbCategoria"  onchange="obtenerPlanteles(this)">
                                          <option value="-1">Seleccione</option>
                                          <?php
                                              $consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE unidadPadre='' AND STATUS=2 and codigoUnidad in 
											  			(SELECT  subsistema FROM _337_tablaDinamica t,_337_subsistemas s WHERE cursos=".$idCurso." AND s.idReferencia=t.id__337_tablaDinamica) ORDER BY unidad";
                                              $con->generaropcionesSelect($consulta);
                                          ?>
                                      </select>
                                  </td>
                                  <td>
                                  </td>
                          </tr>
                         
                          <tr height="21" id="filaCCT">
                                  <td width="130" align="left">
                                      <span class="corpo8_bold">Clave CCT:</span>
                                  </td>
                                  <td >
                                      <input type="text" id="cveCCT" value="" onkeyup="teclaUP(event)" onkeypress="validarTecla(event)" />&nbsp;&nbsp;&nbsp;<a href="javascript:javascript:buscarGruposSede()"><img src="../images/magnifier.png" alt="Buscar plantel" title="Buscar plantel" /></a>
                                  </td>
                                  <td>&nbsp;&nbsp;
                                      <span class="letraRoja" id="lblCTTNO" style="display:none"></span>
                                  </td>
                          </tr>
                          <tr height="21">
                                  <td  align="left" >
                                      <span class="corpo8_bold">Plantel:</span>
                                  </td>
                                  <td  align="left">
                                      <span class="letraRojaSubrayada8" id='lblPlantel'></span>
                                      <select style="display:none" id="cmbPlantel" onchange="obtenerGrupos(this)"></select>
                                  </td>
                                  <td >
                                      
                                  </td>
                              </tr>
                              <tr height="21">
                                      <td  align="left" >
                                          <span class="corpo8_bold">Grupo:</span>
                                      </td>
                                      <td>
                                          <select id="cmbGrupo" disabled="disabled" onchange="obtenerDiasSesion(this)" >
                                          <option value="-1">Seleccione</option>
                                          </select>
                                      </td>
                                      <td >
                                          
                                      </td>
                                  </tr>
                                  <tr height="21" id="filaSesion" style="display:none">
                                      <td  align="left" >
                                          <span class="corpo8_bold">Sesi&oacute;n:</span>
                                      </td>
                                      <td>
                                         <span class="letraRojaSubrayada8" id='lblSesion'></span>
                                      </td>
                                      <td >
                                          
                                      </td>
                                  </tr>
                                  <tr height="21">
                                      <td  align="left" colspan="2" >
                                          <span class="letraRoja" id="lblError"></span>
                                      </td>
                                  </tr>
                               </table>
                          
                         
                          
                      </fieldset>
                      <br /><br />
                      <fieldset id="gridDetalle" class="frameHijo" style="display:none" ><legend>Lista de asistencia</legend>
                          <table>
                              <tr height="21" >
                                  <td width="150" align="left">
                                      <br /><span class="letraRoja" id='filaInstrucciones'><span style="font-size:11px !important">Por favor seleccione del listado su nombre y confirme su asistencia dando click <br />sobre el bot&oacute;n:&nbsp;&nbsp;</span><a href="javascript:guardarNoAsistentes2()"><span class="letraRojaSubrayada8" style="font-size:11px !important"><img src="../images/users.png"> Confirmar asistencia</span></span></a><br /><br />
                                      <span id="tblAsistencia"></span>
                                  </td>
                              </tr>
                          </table>
                      </fieldset>
                      <input type="hidden" id="idCurso" value="<?php echo $idCurso?>" />
                      <input type="hidden" id="plantel" value="" />
                      <input type="hidden" id="noSesion" value="" />
                      <input type="hidden" id="fechaSesion" value="<?php echo $fechaSesion?>" />
                      
                  </td>
                  
              </tr>
              </table>
            </td>
        </tr>
        
        </table>

    
    
    
    
</body>

</html>