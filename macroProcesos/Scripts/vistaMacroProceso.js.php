<?php	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$iM=bD($_GET["iM"]);
	$param="-1";
	if(isset($_GET["param"]))
		$param=$_GET["param"];

?>
Ext.onReady(inicializar);

var primeraCarga=true;
var nodoSel=null;
var criterioB=1;
var idProcesoC=130;
var vListadoRegistros=false;
function inicializar()
{
	Ext.QuickTips.init();
	var detailEl;
    var pagRegresar=gE('pagRegresar').value;
    var tb=new Ext.Toolbar	(
    							{
									region: 'north',
                                    height:28,
                                    items:	[
                                    			{
                                                	xtype:'spacer',
                                                    width:15
                                                },
                                                {
                                                	xtype:'label',
                                                	html:'<table><tr><td><a href="'+pagRegresar+'"><img src="../images/flechaizq.gif" /></a></td><td>&nbsp;&nbsp;&nbsp;&nbsp;<u><span style="font-size:13px; font-weight:bold; color:#900;">'+gE('nMacroProceso').value+' (Macroproceso)</span></u></td><td width="100"></td><?php ?></tr></table></a>'
                                                    
                                                },
                                                {
                                                	xtype:'spacer',
                                                    width:15
                                                }
                                               
                                            ]
                                }
    						);
    
    new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                            			{
                                            layout: 'border',
                                            region:'center',
                                            items: [
                                                        tb,
                                                        {
                                                              id: 'tblActores',
                                                              title: '',
                                                              collapsible:true,
                                                              region: 'west',
                                                              autoScroll: true,
                                                              hidden:false,
                                                              width:255,
                                                              border:false,
                                                              autoLoad:	{
                                                                            url:'../macroProcesos/opcionesMacroProceso.php',
                                                                            scripts:true,
                                                                            params:	{
                                                                            
                                                                                        iM:<?php echo $iM?>
                                                                                    } 
                                                                           
                                                                        }
                                                         }
                                                        ,
                                                       /* {
                                                            id: 'content',
                                                            region: 'center',
                                                            xtype:'iframepanel',
                                                            border:false,
                                                            frame:false,
                                                            loadMask:	{
                                                                            msg:'Cargando'
                                                                        }
                                                        },*/
                                                        new Ext.ux.IFrameComponent({ 
                                                                                        id: 'content', 
                                                                                        region: 'center',
                                                                                        anchor:'100% 100%',
                                                                                        url: '../paginasFunciones/white.php',
                                                                                        style: 'width:100%;height:100%' 
                                                                                	}) 
                                                     ]
                                        }
                            			
                                    ]
						}
                    );
                    
	switch('<?php echo $iM ?>')                    
    {
    	case '8':
        case '10':
        case '11':
        case '12':
	        gEx('content').load({url:'../almacen/tblProductos.php',params:{idAlmacen:<?php echo $param?>,cPagina:'sFrm=true'},scripts:true})
        break;
    }
}

function ejecutarOpcion1(url)
{
	vListadoRegistros=false;
	var URL=bD(url);


	var arrUrl=URL.split('?'); 
 
    var arrParams=new Array();
    var param;
    var x;
    var arrDatos=new Array();
    var cadParam='"cPagina":"sFrm=true"';
    if(arrUrl.length>1)
    {
    	var arrP=arrUrl[1].split('&');	
        
        for(x=0;x<arrP.length;x++)
        {
        	var posIgual=arrP[x].indexOf('=');
            if(posIgual!=-1)
            {
            	var nParam=arrP[x].substring(0,posIgual);
                var valorParam=arrP[x].substring(posIgual+1);
                param=new Array();
                param[0]=nParam;
                param[1]=valorParam;
                arrDatos.push(param);                cadParam+=',"'+param[0]+'":"'+param[1]+'"';
            }
        }
    }
    
    var cadObjParam='[{'+cadParam+'}]';
    var objParam=eval(cadObjParam)[0];
   
	var content=gEx('content');
    content.load	(
    					{
                        	url:arrUrl[0],
                            params:objParam,
                            scripts:true
                        }
    				)
}

function ejecutarOpcion2(iF)
{
   vListadoRegistros=false;
    var objParam={};
    
    
    objParam.sL=0;
    objParam.idFormulario=bD(iF);
    objParam.cPagina='sFrm=true';
    
	var content=gEx('content');
    content.load	(
    					{
                        	url:'../modeloProyectos/visorRegistrosProcesos.php',
                            params:objParam,
                            scripts:true
                        }
    				)
}



function enviarAsignacionResponsable(iP,iA,c)
{
	vListadoRegistros=false;
	var content=Ext.getCmp('content');
    var iP=(iP);	
    content.load({url:'../planeacion/asignacionResponsables.php',scripts:true,params:{ciclo:bD(c),idProceso:bD(iP),idAccion:bD(iA),cPagina:'sFrm=true'}})
}

function enviarActorProc(a,e,iP,c)
{
	vListadoRegistros=false;
	var content=Ext.getCmp('content');
    content.load({url:'../modeloPerfiles/proxyProcesos.php',scripts:true,params:{ciclo:bD(c),actor:a,idProceso:iP,cPagina:'sFrm=true',numEtapa:e}})
}

function enviarActorArrEtapas(a,arr,iP,c)
{
	vListadoRegistros=false;
	var content=Ext.getCmp('content');
    content.load({url:'../modeloPerfiles/proxyProcesos.php',scripts:true,params:{ciclo:bD(c),actor:a,idProceso:iP,cPagina:'sFrm=true',arrEtapas:arr}})
}

function enviarResponsableRegistro(iP,c)
{
	vListadoRegistros=false;
	var content=Ext.getCmp('content');
    content.load({url:'../modeloPerfiles/proxyProcesos.php',scripts:true,params:{ciclo:bD(c),idProceso:iP,cPagina:'sFrm=true'}})
}


function abrirPaginaAlmacen(p)
{
	var objParam={};
    vListadoRegistros=false;
    objParam.cPagina='sFrm=true';
	objParam.idAlmacen='<?php echo $param?>';
	var content=gEx('content');
    content.load	(
    					{
                        	url:p,
                            params:objParam,
                            scripts:true
                        }
    				)	
}

function enviarListadoRegistros(iP,c,iF,iR)
{
	
    var content=Ext.getCmp('content');
    vListadoRegistros=true;
    content.load({url:'../modeloProyectos/visorRegistrosProcesosV2.php',scripts:true,params:{ciclo:bD(c),idProceso:bD(iP),cPagina:'sFrm=true',idReferencia:bD(iR),idFormulario:bD(iF)}})
}


function enviarDatosIframe(paginaURL,parametros)
{
	window.parent.enviarFormularioDatos(paginaURL,parametros,'POST','iImprimir');
}


function imprimirContenido(ctrl)
{
	if(!primeraCarga)
    {
		ctrl.contentWindow.print();

    }
    primeraCarga=false;
}

function regresar1Pagina()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().regresar1Pagina))
		gEx('content').getFrameWindow().regresar1Pagina();
    else
		recargarPagina();
}

function regresar2Pagina()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().regresar2Pagina))
		gEx('content').getFrameWindow().regresar2Pagina();
    else
		recargarPagina();
	
}

function recargarContenedorCentral()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().recargarContenedorCentral))
		gEx('content').getFrameWindow().recargarContenedorCentral();
    else
		recargarPagina();

    
}

function regresar1PaginaContenedor()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().regresar1PaginaContenedor))
		gEx('content').getFrameWindow().regresar1PaginaContenedor();
    else
		recargarPagina();


}

function regresarPagina2Contenedor()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().regresarPagina2Contenedor))
		gEx('content').getFrameWindow().regresarPagina2Contenedor();
    else
		recargarPagina();


}

function regresarContenedorCentral()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().recargarPagina))
		gEx('content').getFrameWindow().recargarPagina();
    else
		recargarPagina();


}

function funcionAntesCerrar()
{
	if((gEx('content'))&&( gEx('content').getFrameWindow().funcionAntesCerrar))
		gEx('content').getFrameWindow().funcionAntesCerrar();
    else
    {
    	recargarContenedorCentral();
		//recargarPagina();
    }

	
    
}


function invocarEjecucionFuncionContenedor(funcion,params)
{
	
	eval("gEx('content').getFrameWindow()."+funcion+"("+params+");");
}
