<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");
include ("latis/funcionesActores.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
</script>	
<?php
$paramPOST=true;
$paramGET=true;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);

		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}


?>

<?php 
	$iM="-1";
	if(isset($objParametros->iM))
		$iM=$objParametros->iM;
	$consulta=" select distinct(tm.textoMenu),tm.idMenu 
				from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM
				where oM.idOpcion=pO.idOpcion and 
				tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and pO.idPagina=-".$iM." 
				and oM.idRol in(".$_SESSION["idRol"].") order by tm.textoMenu";
	//echo $consulta;
	$resMenu=$con->obtenerFilas($consulta);	 
  ?>
  <link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
  <link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
  <title></title>
<style>
	.box_body_td
	{
		padding: 2px 12px 2px 13px !important;	
	}
</style>
</head>

<body>

	<div >
    	<table width="100%">
        	<tr height="6">
            	<td colspan="3"></td>
            </tr>
        	<tr>
            <td width="2%">
            </td>
            <td>
    <?php
		$arrMenu=array();
		while($filaMenu=mysql_fetch_row($resMenu))
		{
			$arrMenu[$filaMenu[0]."_".$filaMenu[1]]["tMenu"]="0";
			$arrMenu[$filaMenu[0]."_".$filaMenu[1]]["idMenu"]=$filaMenu[1];
			$arrMenu[$filaMenu[0]."_".$filaMenu[1]]["titulo"]=$filaMenu[0];
		}
		
		$consulta="SELECT idProceso FROM 810_macroprocesosVSProcesos WHERE idMacroProceso=".$iM;
		$listProcesoMacro=$con->obtenerListaValores($consulta);
		if($listProcesoMacro=="")
			$listProcesoMacro="-1";
		
		$idUsuario=$_SESSION["idUsr"];
		$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$idUsuario. " and codigoRol not like '%_-1'";
		$roles=$con->obtenerListaValores($consulta,"'");
		
		$consulta="SELECT DISTINCT idProceso FROM 9116_responsablesPAT WHERE idResponsable=".$idUsuario." and idProceso in (".$listProcesoMacro.")";
		$procesos=$con->obtenerListaValores($consulta);
				
		$consulta="select idComite from 2007_rolesVSComites where rol in (".$roles.")";
	
		$idComites=$con->obtenerListaValores($consulta);
		if($idComites=='')
			$idComites="-1";
		
		$consulta="select distinct(idProceso)  from 943_rolesActoresProceso where rol in (".$roles.") and idProceso in (".$listProcesoMacro.")";
		$idProcesos=$con->obtenerListaValores($consulta);
		if($idProcesos=="")
			$idProcesos="-1";
		
		$consulta="select distinct(idProyecto) as idProceso from 235_proyectosVSComites where idComite in (".$idComites.") and idProyecto in (".$listProcesoMacro.")";
		$resAux=$con->obtenerListaValores($consulta);
		if($resAux!="")
			$idProcesos.=",".$resAux;
			
		$consulta="SELECT DISTINCT(idProceso) FROM 955_revisoresProceso WHERE idUsuarioRevisor=".$idUsuario." and estado in (1,2) and idProceso in (".$listProcesoMacro.")";
		$resAux=$con->obtenerListaValores($consulta);
		if($resAux!="")
			$idProcesos.=",".$resAux;
					
		$consulta="select f.idProceso from 246_autoresVSProyecto a,900_formularios f where f.idFormulario=a.idFormulario and idProceso not  in(".$idProcesos.") and a.idUsuario=".$idUsuario." and idProceso in (".$listProcesoMacro.")";
		$resAux=$con->obtenerListaValores($consulta);
		if($resAux!="")
			$idProcesos.=",".$resAux;
		
		if($procesos!="")
		{
			if($idProcesos!="")
				$idProcesos.=",".$procesos;
			else	
				$idProcesos=$procesos;
		}			
		
		$consulta="SELECT paginaUrlDestino FROM 811_menusVSOpciones m,809_opciones o  WHERE idMenu IN(SELECT idOpcion FROM  813_paginasVSOpciones WHERE idPagina=-".$iM.") AND m.idOpcion=o.idOpcion AND paginaUrlDestino LIKE '%ingresarProceso%'";
		$resPaginas=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($resPaginas))			
		{
			$idProcesoRef="";
			$arrDatos=explode("('",$fila[0]);
			$idProcesoRef=$arrDatos[1];
			$arrDatos=explode("')",$idProcesoRef);
			$idProcesoRef=bD($arrDatos[0]);
			$consulta="SELECT publicableConvocatoria FROM 4001_procesos WHERE idProceso=".$idProcesoRef;
			$publicable=$con->obtenerValor($consulta);
			$mostrarOpcion=false;
			if($publicable==1)
			{
				$procesos=$idProcesoRef;
				if($idProcesos!="")
					$idProcesos.=",".$procesos;
				else	
					$idProcesos=$procesos;
			}
		}

		$consulta="SELECT idProceso,nombre FROM 4001_procesos where idProceso in(".$idProcesos.") and publicableConvocatoria=0 order by nombre";
		$res=$con->obtenerFilas($consulta);
		$arrProcesos=array();
		while($fila=mysql_fetch_row($res))
		{
			$consulta="SELECT leyenda FROM 810_macroprocesosVSProcesos WHERE idMacroProceso=".$iM." AND idProceso=".$fila[0];
			$leyenda=$con->obtenerValor($consulta);
			$arrMenu[$leyenda."_".$fila[0]]["tMenu"]="1";
			$arrMenu[$leyenda."_".$fila[0]]["idMenu"]=$fila[0];
			$arrMenu[$leyenda."_".$fila[0]]["titulo"]=$leyenda;
			$arrMenu[$leyenda."_".$fila[0]]["idFormulario"]=-1;
			$arrMenu[$leyenda."_".$fila[0]]["idReferencia"]=-1;
		}
		
		$consulta="SELECT idFormulario,idRegistro,ciclo FROM 9118_convocatoriasPublicadas WHERE STATUS=1 AND '".date("Y-m-d")."'>=fechaIniPublica AND '".date("Y-m-d")."'<=fechaFinPublica";
		$resConvocatoria=$con->obtenerFilas($consulta);
		$arrProcesosConv=array();
		$listProcesosConv="";
		$arrCiclosProceso=array();
		$arrReferenciaProceso=array();
		while($filaConv=mysql_fetch_row($resConvocatoria))
		{
			$consulta="SELECT idProceso FROM 9043_procesosVinculadosConvocatoria WHERE idProceso in(".$idProcesos.") 
						and idFormulario=".$filaConv[0]." AND idReferencia=".$filaConv[1];

			$resProceso=$con->obtenerFilas($consulta);
			while($fProc=mysql_fetch_row($resProceso))
			{
				
				if(!isset($arrProcesosConv[$fProc[0]]))	
				{
					$objRef["idFormularioConv"]=$filaConv[0];
					$objRef["idRegistroConv"]=$filaConv[1];
					$arrProcesosConv[$fProc[0]]=$objRef;
				}
				if($filaConv[2]!="")
				{
					
					if(!isset($arrCiclosProceso[$fProc[0]]))
					{
						$arrCiclosProceso[$fProc[0]]=array();
					}
					$objRef["idFormularioConv"]=$filaConv[0];
					$objRef["idRegistroConv"]=$filaConv[1];
					$arrCiclosProceso[$fProc[0]][$filaConv[2]]=$objRef;
				}
				
			}
			
			$idProcesoConvocatoria=obtenerIdProcesoFormulario($filaConv[0]);
			$consulta="SELECT idProcesoAsociado FROM 9043_procesoConvocatoriaVsProcesosVinculados WHERE idProcesoAsociado in(".$idProcesos.") and idProcesoConvocatoria=".$idProcesoConvocatoria;
			$resProceso=$con->obtenerFilas($consulta);	
			while($fProc=mysql_fetch_row($resProceso))
			{
				
				if(!isset($arrProcesosConv[$fProc[0]]))	
				{
					$objRef["idFormularioConv"]=$filaConv[0];
					$objRef["idRegistroConv"]=$filaConv[1];
					$arrProcesosConv[$fProc[0]]=$objRef;
				}
				if($filaConv[2]!="")
				{
					if(!isset($arrCiclosProceso[$fProc[0]]))
					{
						$arrCiclosProceso[$fProc[0]]=array();
					}
					$objRef["idFormularioConv"]=$filaConv[0];
					$objRef["idRegistroConv"]=$filaConv[1];
					$arrCiclosProceso[$fProc[0]][$filaConv[2]]=$objRef;
				}
			}
		}
		 
		  
		if(sizeof($arrProcesosConv)>0)
		{
			  foreach($arrProcesosConv as $p=>$resto)
			  {
				  if($listProcesosConv=="")
					  $listProcesosConv=$p;
				  else
					  $listProcesosConv.=",".$p;
			  }
		  }
		if($listProcesosConv=="")
			$listProcesosConv="-1";
		
		$consulta="SELECT idProceso,nombre FROM 4001_procesos where idProceso in(".$listProcesosConv.") 
					and idProceso in(".$idProcesos.") and publicableConvocatoria=1 order by nombre";
		$res=$con->obtenerFilas($consulta);
		$arrProcesos=array();
		while($fila=mysql_fetch_row($res))
		{
			if(!isset($arrCiclosProceso[$fila[0]]))
			{
				$consulta="SELECT leyenda FROM 810_macroprocesosVSProcesos WHERE idMacroProceso=".$iM." AND idProceso=".$fila[0];
				
				$leyenda=$con->obtenerValor($consulta);
				if($leyenda!="")
				{
					$arrMenu[$leyenda."_".$fila[0]]["tMenu"]="1";
					$arrMenu[$leyenda."_".$fila[0]]["idMenu"]=$fila[0];
					$arrMenu[$leyenda."_".$fila[0]]["titulo"]=$leyenda;
					
					$arrMenu[$leyenda."_".$fila[0]]["idFormulario"]=-1;
					$arrMenu[$leyenda."_".$fila[0]]["idReferencia"]=-1;
					if(isset($arrProcesosConv[$fila[0]]))
					{
						$arrMenu[$leyenda."_".$fila[0]]["idFormulario"]=$arrProcesosConv[$fila[0]]["idFormularioConv"];
						$arrMenu[$leyenda."_".$fila[0]]["idReferencia"]=$arrProcesosConv[$fila[0]]["idRegistroConv"];
					}
				}
			}
			else
			{
				$consulta="SELECT leyenda FROM 810_macroprocesosVSProcesos WHERE idMacroProceso=".$iM." AND idProceso=".$fila[0];
				$leyenda=$con->obtenerValor($consulta);
				if($leyenda!="")
				{
					foreach($arrCiclosProceso[$fila[0]] as $ciclo=>$resto)
					{
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["tMenu"]="1";
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["idMenu"]=$fila[0];
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["titulo"]=$leyenda." [".$ciclo."]";
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["ciclo"]=$ciclo;
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["idFormulario"]=$resto["idFormularioConv"];
						$arrMenu[$leyenda."[".$ciclo."]_".$fila[0]]["idReferencia"]=$resto["idRegistroConv"];
					}
				}
			}
		}
		
		
		if(sizeof($arrMenu)>0)
		{
			ksort($arrMenu);
			foreach($arrMenu as $menu=>$objMenu)
			{
				$arrMenu=explode("_",$menu);
				$generarMenu=true;
				if(isset($objMenu["tMenu"]))
				{
					
					if($objMenu["tMenu"]==1)
					{
						
						$idProceso=$objMenu["idMenu"];
						$idFormulario=obtenerFormularioBase($idProceso);
						if($idFormulario=="")
							$idFormulario=-1;
						$consulta="SELECT vistaListadoRegistros FROM 900_formularios WHERE idFormulario=".$idFormulario;
						$vista=$con->obtenerValor($consulta);
						if(($vista==="0")||($vista===0)) 
						{  
							
							$generarMenu=false;
							$ciclo="";
							$idFormularioRef=-1;
							$idRegistroRef=-1;
							if(isset($objMenu["ciclo"]))
								$ciclo=$objMenu["ciclo"];
							$idFormularioRef=$objMenu["idFormulario"];
							$idRegistroRef=$objMenu["idReferencia"];
							
							
						?>
						<table width="100%" class="box_body_table">
						<tr >
							<td class="box_body_l"></td>
							<td class="box_body box_body_td" width="100%"><ul><li class="bg_list_un"><a href="javascript:enviarListadoRegistros('<?php echo bE($idProceso)?>','<?php echo bE($ciclo)?>','<?php echo bE($idFormularioRef)?>','<?php echo bE($idRegistroRef)?>')"><?php  echo $objMenu["titulo"]; ?></a></li></ul></td>
							<td class="box_body_r"></td>
						</tr>
					
						<tr  >
							
							<td  align="left" colspan="2" style="padding: 5px 5px 5px 5px">
							<span class="letraTituloMenuIzq">
							 <?php
						   
						?>
							</span>
							</td>
						</tr>
					</table>   
						<?php	
						}
					
					}
					
				}
				else
					$generarMenu=false;
					if($generarMenu)
					{
						
				?>
				
				<table width="100%" class="box_heading2_table">
					<tr  >
						
						<td  align="left" colspan="2" style="padding: 5px 5px 5px 5px">
						<span class="letraTituloMenuIzq">
						 <?php
						echo $objMenu["titulo"];
							?>
						</span>
						</td>
					</tr>
				</table>   
				<table width="100%" class="box_body_table">
					   
					<?php
						
						switch($objMenu["tMenu"])
						{
							case 0:
								$consulta="select distinct(pO.textoOpcion),pO.paginaUrlDestino,pO.idOpcion from 811_menusVSOpciones mO, 809_opciones 
											pO  where pO.idOpcion=mO.idOpcion and pO.idIdioma=".$_SESSION["leng"]." and  mO.idMenu=".$objMenu["idMenu"]." order by mO.orden";
								$filas=$con->obtenerFilas($consulta);
								$opciones="";
								while($fila=mysql_fetch_row($filas))
								{
	
									if(strpos($fila[1],"javascript")!==false)
									{
										if(strpos($fila[1],"javascript:ingresarProceso")!==false)
										{
											
											$ciclo="";
											$idFormulario=-1;
											$idReferencia=-1;
											
											
											$idProcesoRef="";
											$arrDatos=explode("('",$fila["1"]);
											$idProcesoRef=$arrDatos[1];
											$arrDatos=explode("')",$idProcesoRef);
											$idProcesoRef=bD($arrDatos[0]);
											$consulta="SELECT publicableConvocatoria FROM 4001_procesos WHERE idProceso=".$idProcesoRef;
											$publicable=$con->obtenerValor($consulta);
											$mostrarOpcion=false;
											if($publicable==0)
											{
												$mostrarOpcion=true;
											}
											else
											{
												if(isset($arrProcesosConv[$idProcesoRef]))
												{
													$idFormulario=$arrProcesosConv[$idProcesoRef]["idFormularioConv"];
													$idReferencia=$arrProcesosConv[$idProcesoRef]["idRegistroConv"];
													$mostrarOpcion=true;
													
												}
												else
												{
													/*if(isset(arrCiclosProceso[$idProcesoRef]))
													{
														
													}*/
												}
											}
											$url=substr($fila[1],0,strlen($fila[1])-1);
											$url.=",'".bE($ciclo)."','".bE($idReferencia)."','".bE($idFormulario)."')";
											
											if($mostrarOpcion)
											{

							?>
                                            <tr >
                                                <td class="box_body_l"></td>
                                                <td class="box_body box_body_td" width="100%" ><ul><li class="bg_list_un"><a href="<?php echo $url ?>"><?php echo $fila[0]?></a></li></ul></td>
                                                <td class="box_body_r"></td>
                                            </tr>
							<?php	
											}
										}
										else
										{
											
									?>
                                            <tr >
                                                <td class="box_body_l"></td>
                                                <td class="box_body box_body_td" width="100%" ><ul><li class="bg_list_un"><a href="<?php echo $fila[1] ?>"><?php echo $fila[0]?></a></li></ul></td>
                                                <td class="box_body_r"></td>
                                            </tr>
							<?php	
										}
									}
									else
									{
										if(strpos($fila[1],"tblFormularios.php?")===false)
										{
						?>
									<tr >
										<td class="box_body_l"></td>
										<td class="box_body box_body_td" width="100%" ><ul><li class="bg_list_un"><a href="javascript:ejecutarOpcion1('<?php echo bE($fila[1])?>')"><?php echo $fila[0]?></a></li></ul></td>
										<td class="box_body_r"></td>
									</tr>
						<?php	
										}
										else
										{
											
											
											
											$arrFormulario=explode("=",$fila[1]);
											$idFormulario=$arrFormulario[1];
											?>
									<tr >
										<td class="box_body_l"></td>
										<td class="box_body box_body_td" width="100%"><ul><li class="bg_list_un"><a href="javascript:ejecutarOpcion2('<?php echo bE($idFormulario)?>')"><?php echo $fila[0]?></a></li></ul></td>
										<td class="box_body_r"></td>
									</tr>
						<?php
										}
									}
								}
																									
							break;
							case 1:
								$ciclo=null;
								if(isset($objMenu["ciclo"]))
									$ciclo=$objMenu["ciclo"];
								
								generarOpcionesProceso($objMenu["idMenu"],$ciclo,$objMenu["idFormulario"],$objMenu["idReferencia"]);
							break;	
						}
				?>
				
			</table>
			<br />
			<?php
				}
				
			}
		}
	?>
    	</td>
        <td width="2%">
        </td>
        </tr>
        </table>
    
    
    	
    </div>
    <!--<input type="hidden" id="pagRegresar" value="<?php echo $pagRegresar ?>" />
	 <form method="post"	action="" id='frmEnvioDatos'>
    	<input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
    </form>
    <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
          <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
    </form>
    <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
          <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
    </form> -->
</body>

</html>