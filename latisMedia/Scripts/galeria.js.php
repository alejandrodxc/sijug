<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var cxt;
var video;
var video2;
Ext.onReady(inicializar);

function inicializar()
{
	  var canvas =gE('canvas');
      ctx= canvas.getContext('2d');
      
      new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b></b></span>',
                                               
                                                items:	[
                                                         	{
                                                            	region:'center',
                                                                border:true,
                                                                items:	[]
                                                            },
                                                            {
                                                            	region:'south',
                                                                border:false,
                                                                height:200,
                                                                layout:'border',
                                                                items:	[
                                                                			{
                                                                            	region:'west',
                                                                                width:400,
                                                                                xtype:'panel',
                                                                                baseCls: 'x-plain',
                                                                                border:true,
                                                                                frame:true,
                                                                                layout:'absolute',
                                                                                items:	[
                                                                                			{
                                                                                            	x:20,
                                                                                                y:0,
                                                                                                xtype:'label',
                                                                                                html:'<video style="background-color:#FFF" id="video" width="225" height="184" autoplay="autoplay"></video>'
                                                                                            },
                                                                                            {
                                                                                            	x:255,
                                                                                                y:0,
                                                                                                xtype:'toolbar',
                                                                                                items:	[
                                                                                                			{
                                                                                                            	xtype: 'buttongroup',
                                                                                                                columns: 1,
                                                                                                                items:	[
                                                                                                                			{
                                                                                                                                icon:'../images/camera.png',
                                                                                                                                cls:'x-btn-text-icon',
                                                                                                                                width:130,
                                                                                                                                height:40,
                                                                                                                                id:'btnPreparar',
                                                                                                                                text:'Preparar C&aacute;mara',
                                                                                                                                handler:function()
                                                                                                                                        {
                                                                                                                                        	
                                                                                                                                            prepararCamara();
                                                                                                                                            gEx('btnPreparar').disable();
                                                                                                                                            gEx('btnCapturar').enable();
                                                                                                                                            gEx('btnPausar').enable();
                                                                                                                                        }
                                                                                                                                
                                                                                                                            },
                                                                                                                            {
                                                                                                                                icon:'../images/pictures.png',
                                                                                                                                cls:'x-btn-text-icon',
                                                                                                                                width:130,
                                                                                                                                height:40,
                                                                                                                                id:'btnCapturar',
                                                                                                                                disabled:true,
                                                                                                                                text:'Capturar imagen',
                                                                                                                                handler:function()
                                                                                                                                        {
                                                                                                                                            tomarFoto();
                                                                                                                                            mostrarVentanaFoto();
                                                                                                                                            
                                                                                                                                        }
                                                                                                                                
                                                                                                                            },
                                                                                                                            {
                                                                                                                                icon:'../images/control_pause.png',
                                                                                                                                cls:'x-btn-text-icon',
                                                                                                                                width:130,
                                                                                                                                height:40,
                                                                                                                                disabled:true,
                                                                                                                                id:'btnPausar',
                                                                                                                                text:'Detener C&aacute;mara',
                                                                                                                                handler:function()
                                                                                                                                        {
                                                                                                                                            gEx('btnPausar').disable();
                                                                                                                                            gEx('btnPreparar').enable();
                                                                                                                                            gEx('btnCapturar').disable();
                                                                                                                                            video.pause();
                                                                                                                                            video2.pause();
                                                                                                                                            
                                                                                                                                        }
                                                                                                                                
                                                                                                                            }
                                                                                                                		]
                                                                                                            }
                                                                                                			
                                                                                                             
                                                                                                            
                                                                                                		]
                                                                                            }
                                                                                		]	
                                                                            },
                                                                            {
                                                                            	region:'center',
                                                                                border:false,
                                                                                layout:'absolute',
                                                                                items:	[
                                                                                		]	
                                                                            }
                                                                		]
                                                            }    
                                                        ]
                                            }
                                         ]
                            }
                        )  
	
}


function prepararCamara()
{
	video = gE('video');
    video2 = gE('video2');
	if (navigator.getUserMedia) 
    {
          navigator.getUserMedia
                                  (
                                      { 'video': true },function(stream)
                                                      {
                                                          video.src = stream;
                                                          video.play();
                                                          video2.src = stream;
                                                          video2.play();
                                                      }
                                  );
    } 
    else 
      if (navigator.webkitGetUserMedia) 
      {
          navigator.webkitGetUserMedia
                                      (
                                          { 'video': true },
                                          function(stream)
                                          {
                                              video.src = window.webkitURL.createObjectURL(stream);
                                              video.play();
                                              video2.src = window.webkitURL.createObjectURL(stream);
                                              video2.play();
                                          }
                                      );
      } 
      else 
          if (navigator.mozGetUserMedia) 
          {
              navigator.mozGetUserMedia
                                      (
                                          { 'video': true },
                                          function(stream)
                                          {
                                              video.mozSrcObject = stream;
                                              video.play();
                                              video2.mozSrcObject = stream;
                                              video2.play();
                                          },
                                          function(err)
                                          {
                                              alert('An error occured! '+err);
                                          }
                                      );
          }
}

function tomarFoto()
{
	ctx.drawImage(video2, 0, 0, 675, 552);
}

function mostrarVentanaFoto()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:5,
                                                            y:5,
                                                            html:'<canvas id="canvasTmp" style="border-style:solid; border-width:1px; border-color:#000; width:450px;height:368px"></canvas>'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Imagen capturada',
										width: 490,
										height:475,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'right',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
                                                            icon:'../images/icon_big_tick.gif',
                                                            cls:'x-btn-text-icon',
                                                            width:130,
                                                            height:40,
                                                            id:'btnMantener',
                                                            text:'Mantener imagen',
                                                            handler:function()
                                                                    {
                                                                        
                                                                        
                                                                    }
                                                            
                                                        },
														{
                                                            icon:'../images/cross.png',
                                                            cls:'x-btn-text-icon',
                                                            width:130,
                                                            height:40,
                                                            id:'btnDesechar',
                                                            text:'Desechar imagen',
                                                            handler:function()
                                                                    {
                                                                       function resp(btn)
                                                                       {
                                                                       		if(btn=='yes')
                                                                            {
                                                                            	ventanaAM.close();
                                                                            }
                                                                       }
                                                                       msgConfirm('Est&aacute; seguro de querer desechar la imagen capturada?',resp);
                                                                        
                                                                    }
                                                            
                                                        }
													]
									}
								);
	ventanaAM.show();	
    var canvasTmp =gE('canvasTmp');
    var ctxTmp= canvasTmp.getContext('2d');
    ctxTmp.scale(0.45,0.30);
    ctxTmp.drawImage(gE('canvas'), 0, 0,675,552);
    
}