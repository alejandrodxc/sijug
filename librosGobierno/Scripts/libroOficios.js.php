<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT DISTINCT anio,anio FROM 7044_procesosLibrosGobierno WHERE tipoLibro=8";
	$arrAnios=$con->obtenerFilasArreglo($consulta);
	$anioActual=date('Y');
	
	if($arrAnios=="[]")
		$arrAnios="[['".$anioActual."','".$anioActual."']]";
	
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
	
	$arrEtapas="";
	$consulta="SELECT numEtapa,nombreEtapa FROM 4037_etapas WHERE idProceso=244 ORDER BY numEtapa";
	$resEtapas=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($resEtapas))
	{
		$o="[".$fila[0].",'".removerCerosDerecha($fila[0]).".- ".$fila[1]."']";
		if($arrEtapas=="")
			$arrEtapas=$o;
		else
			$arrEtapas.=",".$o;
	}
	$arrEtapas="[".$arrEtapas."]";
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama";
	$arrIntituciones=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT valor,contenido FROM 902_opcionesFormulario WHERE idGrupoElemento=9771";
	$arrIntitucionesDestino=$con->obtenerFilasArreglo($consulta);
?>
var arrIntitucionesDestino=<?php echo $arrIntitucionesDestino?>;
var arrInstituciones=<?php echo $arrIntituciones?>;
var arrEtapas=<?php echo $arrEtapas?>;
var anioActual='<?php echo $anioActual?>';
var arrAnios=<?php echo $arrAnios?>;


Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Libro de Oficios</b></span>',
                                                items:	[
                                                            crearGridLibroGobierno()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridLibroGobierno()
{
	var cmbAnio=crearComboExt('cmbAnio',arrAnios,0,0,150);
    var pos=obtenerPosFila(gEx('cmbAnio').getStore(),'id',anioActual);
    if(pos!=-1)
    {
    	cmbAnio.setValue(anioActual);
    }
    else
    {
    	if(arrAnios.length>0)
        {
        	cmbAnio.setValue(arrAnios[arrAnios.length-1]);
        }
    }
    
    cmbAnio.on('select',function()
    					{
                        	gEx('gLibro').getStore().reload();
                        }
    			)
    
    
    
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'idFormulario'},
                                                        {name:'carpetaAdministrativa'},
                                                        {name:'imputado'},
                                                        {name:'delitos'},
                                                        {name:'folio'},
		                                                {name: 'noOficioAsignado'},
		                                                {name:'dirigidoA'},
		                                                {name:'asunto'},
                                                        {name: 'situacionActual'},
                                                        {name: 'comentariosAdicionales'},
                                                        {name:'responsableRegistro'},
                                                        {name: 'institucionDestinataria'},
                                                        {name: 'areaDestinataria'},                                                        
                                                        {name: 'fechaRegistro',type:'date', dateFormat:'Y-m-d H:i:s'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_SGP_LibrosGobierno.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='1';
                                        proxy.baseParams.tLibro=13;
                                        proxy.baseParams.anioJudicial=cmbAnio.getValue()==''?anioActual:cmbAnio.getValue();
                                        
                                    }
                        )   
    
    var paginador=	new Ext.PagingToolbar	(
                                              {
                                                    pageSize: 100,
                                                    store: alDatos,
                                                    displayInfo: true,
                                                    disabled:false
                                                }
                                             )       
    
	var expander = new Ext.ux.grid.RowExpander({
                                                    column:2,
                                                    tpl : new Ext.Template(
                                                        '<table width="100%" >'+
                                                        '<tr><td  style="padding:10px">{datosParticipantes}</td></tr>'+
                                                        '</table>'
                                                    )
                                                }); 
       
	var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'Folio',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'folio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href="javascript:abrirFormularioProcesoLibro(\''+bE(registro.data.idFormulario)+'\',\''+bE(registro.data.idRegistro)+'\')">'+val+'</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:150,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        },
                                                                dataIndex:'fechaRegistro'
                                                            }
                                                            
                                                            ,
                                                            {
                                                                header:'No. de oficio',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'noOficioAsignado'
                                                            },
                                                            {
                                                                header:'Instituci&oacute;n destinataria',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'institucionDestinataria',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrInstituciones,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'&Aacute;rea destinataria',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'areaDestinataria',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(registro.data.institucionDestinataria=='000')
                                                                            	return mostrarValorDescripcion(val);
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrInstituciones,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Carpeta Judicial',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'carpetaAdministrativa',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Imputados',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'imputado',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Delito',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'delitos',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Dirigido a',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'dirigidoA',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Asunto',
                                                                width:400,
                                                                sortable:true,
                                                                dataIndex:'asunto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Registrado por',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'responsableRegistro',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n actual',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'situacionActual',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRendererNumerico(arrEtapas,val);
                                                                        }
                                                            }
                                                        ]
                                              	);
                                                    
	var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gLibro',
                                                    store:alDatos,
                                                    border:false,
                                                    region:'center',
                                                    frame:false,
                                                    cm: cModelo,
                                                    bbar:[paginador],
                                                    plugins:[expander],
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    tbar:	[
                                                    			{
                                                                	xtype:'label',
                                                                    html:'<b>A&ntilde;o Judicial:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                },
                                                                cmbAnio
                                                    		],
                                                    columnLines : true,                                                    
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: false,
                                                                                        enableGrouping :false,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: false,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
	
    
    tblGrid.getStore().load(	{
    								params:	{
                                    			url:'../paginasFunciones/funcionesModulosEspeciales_SGP_LibrosGobierno.php',
                                                start:0, 
                                                limit:100
                                                
                                    		}
    							}
                           )  
    
    return 	tblGrid;
}


function abrirFormularioProcesoLibro(iF,iR)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",bE(0)],['dComp',bE(accion)]];
    
    
    var obj={};
    obj.url="../modeloPerfiles/vistaDTDv3.php";
    obj.params=arrDatos;
    obj.ancho='100%';
    obj.alto='100%';
    
    abrirVentanaFancySuperior(obj);
    
    
    
}