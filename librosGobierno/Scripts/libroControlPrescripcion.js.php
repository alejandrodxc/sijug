<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	
	$consulta="SELECT DISTINCT anio,anio FROM 7044_procesosLibrosGobierno WHERE tipoLibro=11";
	$arrAnios=$con->obtenerFilasArreglo($consulta);
	$anioActual=date('Y');
	
	if($arrAnios=="[]")
		$arrAnios="[['".$anioActual."','".$anioActual."']]";
		
		
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);	
	
	
?>

var arrResoluciones=[];
var arrSiNo=<?php echo $arrSiNo?>;
var arrAnios=<?php echo $arrAnios?>;

var anioActual=arrAnios[arrAnios.length-1][0];
Ext.onReady(inicializar);
function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Libro Control de Prescripci&oacute;n</b></span>',
                                                items:	[
                                                            crearGridLibroGobierno()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridLibroGobierno()
{
	var cmbAnio=crearComboExt('cmbAnio',arrAnios,0,0,150);
    var pos=obtenerPosFila(gEx('cmbAnio').getStore(),'id',anioActual);
    if(pos!=-1)
    {
    	cmbAnio.setValue(anioActual);
    }
    else
    {
    	if(arrAnios.length>0)
        {
        	cmbAnio.setValue(arrAnios[arrAnios.length-1]);
        }
    }
    
    cmbAnio.on('select',function()
    					{
                        	gEx('gLibro').getStore().reload();
                        }
    			)
    
    
    
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idRegistro'},
                                                        {name: 'idFormulario'},
                                                        {name: 'folio'},
                                                        {name: 'fechaRegistro'},
                                                        {name: 'carpetaAdministrativa'},
                                                        {name: 'imputado'},
                                                        {name: 'delitos'},
                                                        {name: 'penaMedidaSeguridad'},
                                                        {name: 'periodoPenaPrivativa'},
                                                        {name: 'fechaSustraccionSentencia'},
                                                        {name: 'fechaUltimoActoAutoridad'},
                                                        {name: 'ubicacionSentenciado'},
                                                        {name: 'abonoPrisionPreventiva'},
                                                        {name: 'abonoPrisionPunitiva'},
                                                        {name: 'fechaPrescripcion'},
                                                        {name: 'responsableRegistro'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_SGP_LibrosGobierno.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRegistros', direction: 'ASC'},
                                                            groupField: 'fechaRegistros',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='1';
                                        proxy.baseParams.tLibro=11;
                                        proxy.baseParams.anioJudicial=cmbAnio.getValue()==''?anioActual:cmbAnio.getValue();
                                        
                                    }
                        )   
    
    var paginador=	new Ext.PagingToolbar	(
                                              {
                                                    pageSize: 100,
                                                    store: alDatos,
                                                    displayInfo: true,
                                                    disabled:false
                                                }
                                             )       
    
	
	var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            new  Ext.grid.RowNumberer({width:30}),
                                                            {
                                                                header:'Folio',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'folio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(registro.data.idFormulario!='')
	                                                                        	return '<a href="javascript:abrirFormularioProcesoLibro(\''+bE(registro.data.idFormulario)+'\',\''+bE(registro.data.idRegistro)+'\')">'+val+'</a>';
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:145,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var tiempo=Date.parseDate(val,'Y-m-d H:i:s');
                                                                            return tiempo.format('d/m/Y  h:i A');
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Carpeta Judicial',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'carpetaAdministrativa',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Imputado/Sentenciado',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'imputado',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Delito',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'delitos',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Pena o medida de seguridad',
                                                                width:230,
                                                                sortable:true,
                                                                dataIndex:'penaMedidaSeguridad',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'A&ntilde;os, meses y d&iacute;as<br>de la Pena privativa de libertad',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'periodoPenaPrivativa',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Fecha de sustración o fecha<br>de ejecutoria de la sentencia',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'fechaSustraccionSentencia',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var arrDatosFecha=val.split(' ');
                                                                            
                                                                            var arrFecha=(arrDatosFecha[0]+'').split('-');
                                                                            return arrFecha[2]+'/'+arrFecha[1]+'/'+arrFecha[0];	
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Fecha de último<br>acto de autoridad',
                                                                width:170,
                                                                sortable:true,
                                                                dataIndex:'fechaUltimoActoAutoridad',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var arrDatosFecha=val.split(' ');
                                                                            
                                                                            var arrFecha=(arrDatosFecha[0]+'').split('-');
                                                                            return arrFecha[2]+'/'+arrFecha[1]+'/'+arrFecha[0];	
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'El sentenciado se encuentra en',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'ubicacionSentenciado',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Abono de prisión preventiva',
                                                                width:190,
                                                                sortable:true,
                                                                dataIndex:'abonoPrisionPreventiva',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Abono de prisión punitiva',
                                                                width:190,
                                                                sortable:true,
                                                                dataIndex:'abonoPrisionPunitiva',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Fecha de prescripci&oacute;n',
                                                                width:160,
                                                                sortable:true,
                                                                dataIndex:'fechaPrescripcion',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var arrDatosFecha=val.split(' ');
                                                                            
                                                                            var arrFecha=(arrDatosFecha[0]+'').split('-');
                                                                            return arrFecha[2]+'/'+arrFecha[1]+'/'+arrFecha[0];	
                                                                        }
                                                                
                                                            },	
                                                            {
                                                                header:'Responsable de solicitud',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'responsableRegistro',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return mostrarValorDescripcion(val);
                                                                        }
                                                                
                                                            }
                                                        ]
                                              	);
                                                    
	var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gLibro',
                                                    store:alDatos,
                                                    border:false,
                                                    region:'center',
                                                    frame:false,
                                                    cm: cModelo,
                                                    bbar:[paginador],
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    tbar:	[
                                                    			{
                                                                	xtype:'label',
                                                                    html:'<b>A&ntilde;o Judicial:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                },
                                                                cmbAnio
                                                    		],
                                                    columnLines : true,                                                    
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: false,
                                                                                        enableGrouping :false,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: false,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
	
    
    tblGrid.getStore().load(	{
    								params:	{
                                    			url:'../paginasFunciones/funcionesModulosEspeciales_SGP_LibrosGobierno.php',
                                                start:0, 
                                                limit:100
                                                
                                    		}
    							}
                           )  
    
    return 	tblGrid;
}


function abrirFormularioProcesoLibro(iF,iR)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",bE(0)],['dComp',bE(accion)]];
    
    var obj={};
    obj.url="../modeloPerfiles/vistaDTDv3.php";
    obj.params=arrDatos;
    obj.ancho='100%';
    obj.alto='100%';
    
    abrirVentanaFancySuperior(obj);
}