<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="select concat(idRol,'_',extensionRol),nombreGrupo from 8001_roles where idIdioma=".$_SESSION["leng"]." order by nombreGrupo";
	$arrRoles=uEJ($con->obtenerFilasArreglo($consulta));	
	

?>
var nodoSel=null;
var uploadControl;
var documento;
var plantillaDetalle;
var plantillaVacia;
var iCategoria=-1;

var accionDocumento;
var campoBusqueda;
var mostrarBarraSel=false;
var regComparte=crearRegistro([
                                  {name: 'idUnidad'},
                                  {name: 'nombreUnidad'},
                                  {name: 'tipo'},
                                  {name: 'permisos'}
                              ]);


Ext.onReady(inicializar);

var tipoVisualizacion1=[['0','Descarga de documento']];
var tipoVisualizacion2=[['0','Descarga de documento'],['1','Visualizaci\xF3n de documento en l\xEDnea']];

function inicializar()
{
	
	var cmbTipoVisualizacion=crearComboExt('cmbTipoVisualizacion',[],0,0,250);
	Ext.QuickTips.init();
    if((gE('funcionSel').value!=''))//&&(gE('soloDescarga').value!='1')
	    mostrarBarraSel=true;
    plantillaVacia=new Ext.XTemplate(
                                        '<div ></div>'
                                     )
	plantillaVacia.compile();                                
	plantillaDetalle=new Ext.XTemplate(
                                        '<div class="details">'+
                                            '<tpl for="."><br />'+
                                            '<table width="100%">'+
                                            '<tr>'+
	                                            '<td colspan="2" align="center"><a href="javascript:descargarDocumento()"><div ><img width="{ancho}" height="{alto}" src="{url}"></div></a><br /><br /></td>'+
                                            '</tr>'+
                                            '<tr>'+
	                                            '<td  style="padding:5px 5px 5px 5px" class="letraRojaSubrayada8" align="left"><b>T&iacute;tulo del documento:</b></td>'+
                                            '</tr>'+
                                            '<tr>'+
	                                            '<td style="padding:5px 5px 5px 5px" align="left"><span class="letraFichaRespuesta">{tituloDocumento}</span></td>'+
                                            '</tr>'+
                                            '<tr height="1"  style="background-color:#006">'+
	                                            '<td ></td>'+
                                            '</tr>'+
                                            '<tr>'+
                                            	'<td align="left">'+
                                                    '<table>'+
                                                        '<tr>'+
                                                            '<td style="padding:5px 5px 5px 5px"><a href="javascript:descargarDocumento()"><img src="../images/download.png" /></a></td><td>&nbsp;<a href="javascript:descargarDocumento()"><span class="letraRoja">Dercargar documento</span></a></b></td>'+
                                                        '</tr>'+
                                                        '<tr id="filaVisor">'+
                                                            '<td style="padding:5px 5px 5px 5px"><a href="javascript:visualizarDocumento()"><img src="../images/film_go.png" /></a></td><td>&nbsp;<a href="javascript:visualizarDocumento()"><span class="letraRoja">Visualizar documento</span></a></b></td>'+
                                                        '</tr>'+
                                                     '</table>'+
                                            	'</td>'+
                                            '</tr>'+
                                            '<tr height="1"  style="background-color:#006">'+
	                                            '<td ></td>'+
                                            '</tr>'+
                                            '<tr>'+
                                            	'<td align="left">'+
                                                    '<table>'+
                                                        '<tr>'+
                                                            '<td style="padding:5px 5px 5px 5px" class="letraRojaSubrayada8"><b>Tama&ntilde;o del documento:</b></td><td style="padding:5px 5px 5px 5px"><span class="letraFichaRespuesta">{tamano}</span></td>'+
                                                        '</tr>'+
                                                        '<tr id="filaVisor">'+
                                                            '<td style="padding:5px 5px 5px 5px" class="letraRojaSubrayada8"><b>Ultima modificaci&oacute;n:</b></td><td style="padding:5px 5px 5px 5px"><span class="letraFichaRespuesta">{fechaUltimoCambio}</span></div></td>'+
                                                        '</tr>'+
                                                     '</table>'+
                                            	'</td>'+
                                            '</tr>'+
                                            '<tr id="filaCompartidoPor1">'+
                                            	'<td align="left">'+
                                                    '<table width="100%">'+
                                                        '<tr>'+
                                                            '<td style="padding:5px 5px 5px 5px" class="letraRojaSubrayada8"><b>Documento compartido por:</b></td><td style="padding:5px 5px 5px 5px"></td>'+
                                                        '</tr>'+
                                                        '<tr >'+
                                                            '<td  align="center" colspan="2" style="padding:5px 5px 5px 5px" class="letraFichaRespuesta">{autor}</td>'+
                                                        '</tr>'+
                                                     '</table>'+
                                            	'</td>'+
                                            '</tr>'+
                                            '<tr>'+
	                                            '<td style="padding:5px 5px 5px 5px" class="letraRojaSubrayada8" align="left"><b>Descripci&oacute;n:</b></td>'+
                                            '</tr>'+
                                            '<tr>'+
	                                            '<td style="padding:5px 5px 5px 5px;text-align:justify" ><div  class="letraFichaRespuesta" style="width:92%; height:200px; overflow:auto; padding:5px 10px 5px 5px">{descripcion}</div></td>'+
                                            '</tr>'+
                                            '</table>'+
                                            '</tpl>'+
                                        '</div>'
                                    );
	plantillaDetalle.compile();    
	var arbolCategorias=crearArbolCategorías();
    var visorDocumentos=crearVisorDocumentos();
    var itemsBar=new Array();
	if(mostrarBarraSel)    
    {
    	itemsBar=	[
                         {
                              xtype:'label',
                              html:'&nbsp;&nbsp;<span class="letrarojaSubrayada8"><b>Tipo de visualizaci&oacute;n</b></span>:&nbsp;&nbsp;'
                          },
                          cmbTipoVisualizacion,
                          '-',
                          {
                              icon:'../images/icon_big_tick.gif',
                              cls:'x-btn-text-icon',
                              text:'Seleccionar documento',
                              handler:function()
                                      {
                                      	
                                          if(documento==null)
                                          {
                                              msgBox('Debe seleccionar un documento');
                                              return;
                                          }
                                          
                                          var funcion=gE('funcionSel').value;
                                          url='';
                                          if(cmbTipoVisualizacion.getValue()=='0')
                                          {	
                                          	url='javascript:getDocumento(\''+bE('../media/obtenerDocumentoGaleria.php')+'\',\''+bE(documento.get('idDocumento'))+'\')';	
                                          }
                                          else
                                          {
                                          	url='javascript:showDocumento(\''+bE(documento.get('tituloDocumento'))+'\',\''+bE(documento.get('extension'))+'\',\''+bE(documento.get('idDocumento'))+'\')';	
                                          }
                                          var objDoc={idDocumento:documento.get('idDocumento'),urlDoc:url,tamano:documento.data.tamano,nombreArchivo:documento.data.nombreArchivo};
                                          eval(funcion+'('+documento.get('idDocumento')+',url,objDoc);');
                                          if(window.parent.cerrarVentanaFancy!=undefined)
	                                          window.parent.cerrarVentanaFancy();
                                      }
                              
                          },'-',
                
                          {
                              icon:'../images/cross.png',
                              cls:'x-btn-text-icon',
                              text:'Cancelar',
                              handler:function()
                                      {
                                      		if(window.parent.cerrarVentanaFancy!=undefined)
                                     			window.parent.cerrarVentanaFancy();
                                      }
                              
                          }
					]                          
    }
	new Ext.Viewport(	{
    							
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            title:'<span class="letraRojaSubrayada8" style="font-size:14px !important"><b>Galeria de documentos</b></span>',	
                                            region:'center',
                                            layout:'border',
                                            items:	[
                                            			arbolCategorias,
                                                       	visorDocumentos,
                                                        {
                                                        	id:'panelDetalle',
                                                        	xtype:'panel',
                                                            region:'east',
                                                            width:260,
                                                            autoScroll:true,
                                                            collapsible:true,
                                                            split:true,
                                                        }
                                            		],
                                            bbar:	itemsBar
                                        }
                                     ]
						}
                    )   
}

function crearArbolCategorías()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'74'
                                                                    
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesPortal.php'
                                                }
                                            );		
	cargadorArbol.on('load',function(proxy)
    								{
                                    	if(iCategoria!=-1)
                                        {
                                        	var arbolCategorias=gEx('arbolCategorias');
                                            var raiz=arbolCategorias.getRootNode();
                                            var x;
                                            var obj;
                                            for(x=0;x<raiz.childNodes.length;x++)
                                            {
                                            	obj=raiz.childNodes[x];
                                                if(obj.id==(''+iCategoria))
                                                {
	                                                obj.select();
                                                    nodoSel=obj;
                                                    funcClikArbol(nodoSel);
                                                    break;
                                                }
                                            }
                                            //gEx('visorDocumentos').getStore().reload();
                                            
                                        }
                                        
                                    }
                     )                
                
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															x:10,
															y:40,
															id:'arbolCategorias',
															region:'west',
															width:220,
															useArrows:true,
															autoScroll:true,
															animate:true,
															enableDD:true,
															containerScroll: true,
															root:raiz,
															loader: cargadorArbol,
															rootVisible:false,
                                                            collapsible:true,
                                                            split:true,
                                                            title:'Categor&iacute;as de documentos',
															tbar:
															[
                                                            	{
                                                                	id:'addCategoria',
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    tooltip :'Agregar categor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                                crearVentanaDatosCategoria(1);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                	id:'modCategoria',
                                                                    icon:'../images/pencil.png',
                                                                    cls:'x-btn-text-icon',
                                                                    disabled:true,
                                                                    tooltip :'Modificar categor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                            	if(nodoSel==null)
                                                                                {
                                                                                	msgBox('Debe seleccionar la categor&iacute;a que desea modificar');
                                                                                	return;
                                                                                }
                                                                                if(nodoSel.id=='0')
                                                                                {
                                                                                	msgBox('Esta categor&iacute;a no es modificable');
                                                                                    return;
                                                                                }
                                                                                crearVentanaDatosCategoria(0);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                	id:'delCategoria',
                                                                    disabled:true,
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    tooltip :'Remover categor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                                if(nodoSel==null)
                                                                                {
                                                                                	msgBox('Debe seleccionar la categor&iacute;a que desea remover');
                                                                                	return;
                                                                                }
                                                                                if(nodoSel.id=='0')
                                                                                {
                                                                                	msgBox('Esta categor&iacute;a no es removible');
                                                                                    return;
                                                                                }
                                                                                if(nodoSel.attributes.noDocumentos!='0')
                                                                                {
                                                                                	msgBox('No se puede eliminar categor&iacute;s con documentos asociados');
                                                                                    return;
                                                                                }
                                                                                function resp(btn)
                                                                                {
                                                                                	if(btn=='yes')
                                                                                    {
                                                                                        function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                nodoSel.remove();
                                                                                                iCategoria=-1;
                                                                                                nodoSel=null;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPortal.php',funcAjax, 'POST','funcion=76&idCategoria='+nodoSel.id,true);
                                                                                	}
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer remover la categor&iacute;a seleccionada?',resp);
                                                                            }
                                                                    
                                                                }
															]
														}
													)
		
							
	arbolOpciones.on('click',funcClikArbol);	
    return arbolOpciones ;              

}

function funcClikArbol(nodo, evento)
{
	nodoSel=nodo;
    var visorDocumentos=gEx('visorDocumentos');
    visorDocumentos.getStore().reload();
    gEx('panelVisor').setTitle('<span style="color:#000 !important">Documentos asociados a la categor&iacute;a: </span><span class="letraRojaSubrayada8">'+nodoSel.attributes.nombre+'</span>');
    gEx('modCategoria').disable();
    gEx('delCategoria').disable();
    gEx('addDocument').disable();
    var permisosCategoria=nodoSel.attributes.permisosCategoria+'';
    var permisos=nodoSel.attributes.permisos+'';
    if(permisosCategoria.indexOf('M')!=-1)
    {
    	gEx('modCategoria').enable();
    }
    if(permisosCategoria.indexOf('E')!=-1)
    {
    	gEx('delCategoria').enable();
    }
    
    if(permisos.indexOf('A')!=-1)
    {
    	gEx('addDocument').enable();
    }
    
    


}

function crearVentanaDatosCategoria(accion)
{
	var gridDocumentoCompartir=crearGridDocumentoCompartir(true);
	var lblTitulo;
    var nomCategoria='';
    var descripcion='';
    if(accion==1)
    	lblTitulo='Agregar categor&iacute;a de documento';
    else
    {
    	lblTitulo='Modificar nombre de categor&iacute;a de documento';
        nomCategoria=nodoSel.attributes.nombre;
        descripcion=nodoSel.attributes.descripcion
        gridDocumentoCompartir.getStore().loadData(eval(nodoSel.attributes.arrPermisosDefault));
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre de la categor&iacute;a:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:300,
                                                            id:'txtNombreCategoria',
                                                            value:nomCategoria
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n:'
                                                        },
                                                         {
                                                        	x:150,
                                                            y:35,
                                                            xtype:'textarea',
                                                            width:400,
                                                            height:80,
                                                            id:'txtDescripcion',
                                                            value:descripcion
                                                        },
                                                        {
                                                        	x:10,
                                                            y:150,
                                                            xtype:'fieldset',
                                                            width:580,
                                                            height:200,
                                                            title:'Compartir categor&iacute;a con:',
                                                            items:	[
                                                            			gridDocumentoCompartir
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 625,
										height:440,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombreCategoria').focus(true,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtNombreCategoria=gEx('txtNombreCategoria');
                                                                        if(txtNombreCategoria.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtNombreCategoria.focus();
                                                                            }
                                                                            msgBox('El nombre de la categor&iacute;a es obligatorio',resp)
                                                                        	return;
                                                                        }
                                                                        var idCategoria=-1;
                                                                        if(accion!=1)
                                                                        	idCategoria=nodoSel.id;
                                                                            
                                                                        var cadComparte='';
                                                                        var x;
                                                                        var tblGridComparte=gEx('tblGridComparte');
                                                                        var fila;
                                                                        var obj='';
                                                                        
                                                                        for(x=0;x<tblGridComparte.getStore().getCount();x++)    
                                                                        {
                                                                            fila=tblGridComparte.getStore().getAt(x);
                                                                            obj='{"idUnidad":"'+fila.get('idUnidad')+'","tipoUnidad":"'+fila.get('tipo')+'","permisos":"'+fila.get('permisos')+'"}';
                                                                            if(cadComparte=='')
                                                                                cadComparte=obj;
                                                                            else
                                                                                cadComparte+=','+obj;
                                                                        }    
                                                                            
                                                                            
                                                                        var cadObj='{"arrComparte":['+cadComparte+'],"idCategoria":"'+idCategoria+'","nombreCategoria":"'+cv(txtNombreCategoria.getValue())+'","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	iCategoria=arrResp[1];
                                                                                gEx('arbolCategorias').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPortal.php',funcAjax, 'POST','funcion=75&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearVisorDocumentos()
{
	var almacenDocumentos=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        autoLoad:true,
                                                        fields: [
                                                                  {name: 'idDocumento'},
                                                                  {name: 'url'},
                                                                  {name: 'nombreArchivo'},
                                                                  {name: 'descripcion'},
                                                                  {name: 'tamano'},
                                                                  {name: 'fechaUltimoCambio'},
                                                                  {name: 'tituloDocumento'},
                                                                  {name: 'idCategoria'},
                                                                  {name: 'extension'},
                                                                  {name: 'ancho'},
                                                                  {name: 'alto'},
                                                                  {name: 'usuariosComparte'},
                                                                  {name: 'permisos'},
                                                                  {name: 'autor'},
                                                                  {name: 'lblComplementario'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {
                                                                                              url: '../paginasFunciones/funcionesPortal.php'
                                                                                          }
                                                                                      )                             
                                                    })
	almacenDocumentos.on('beforeload',function(proxy)
                                        {
                                            proxy.baseParams.funcion='77';
                                            if(nodoSel!=null)
                                            	iCategoria=nodoSel.id;
                                            proxy.baseParams.idCategoria=iCategoria;
                                            documento=null;
                                            var panelDetalle=gEx('panelDetalle').body;
                                            panelDetalle.hide();
                                            plantillaVacia.overwrite(panelDetalle, {});
                                            panelDetalle.slideIn('l', {stopFx:true,duration:.2});
                                        }
                        )    
                        
	var plantilla='<tpl for=".">'+
                        '<div class="thumb-wrap" id="{idDocumento}">'+
                        '<div class="thumb"><img width="32" height="32" src="{url}" title="{descripcion}"></div>'+
                        '<span class="letraRojaSubrayada8">{lblComplementario}{tituloDocumento}</span></div>'+
                    '</tpl>'                                                                           
	var tpl = new Ext.XTemplate(plantilla);	
	 var visorDocumentos=	new Ext.DataView(	{
                                                      store: almacenDocumentos,
                                                      tpl: tpl,
                                                      id:'visorDocumentos',
                                                      anchor:'100% 100%',
                                                      singleSelect:true,
                                                      multiSelect: true,
                                                      overClass:'x-view-over',
													  itemSelector: 'div.thumb-wrap',
                                                      autoScroll  : true,
											          emptyText: '<div style="padding:40px;" class="letraRojaSubrayada8">No existen documentos disponibles</div>'
                                                  }
                                              ) 
	visorDocumentos.on('click',function(dv,idx,nodo,e)
    							{
                                	mostrarDetalle(nodo);
                                }
    					) 
	visorDocumentos.on('dblclick',function(dv,idx,nodo,e)
    							{
                                	mostrarDetalle(nodo);
                                    if(!mostrarBarraSel)
	                                    descargarDocumento();
                                }
    					)   
	visorDocumentos.on('containerclick',function(dv,e)
    							{
                                	
                                	mostrarDetalle(null);

                                }
    					)                                                                                             
	var panelVisorDocumentos=new Ext.Panel	(
    											{
                                                	id:'panelVisor',
                                                	region:'center',

                                                    layout:'fit',
                                                    title:'<span style="color:#000 !important">Documentos asociados a la categor&iacute;a:</span> <span class="letraRojaSubrayada8">Ninguno</span>',
                                                	items:	[
                                                    			visorDocumentos
                                                    		],
                                                   	tbar:	[
                                                    			{
                                                                	id:'addDocument',
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text :'Agregar documento',
                                                                    disabled:true,
                                                                    handler:function()
                                                                            {
                                                                                mostrarVentanaAgregarDocumento();
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                	id:'modifyDocument',
                                                                    icon:'../images/pencil.png',
                                                                    cls:'x-btn-text-icon',
                                                                    disabled:true,
                                                                    text :'Modificar documento',
                                                                    handler:function()
                                                                            {
                                                                            	if(documento==null)
                                                                                {
                                                                                	msgBox('Debe seleccionar el documento que desea modificar');
                                                                                	return;
                                                                                }
                                                                               mostrarVentanaAgregarDocumento(documento);
                                                                                
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                	id:'delDocument',
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    disabled:true,
                                                                    text :'Remover documento',
                                                                    handler:function()
                                                                            {
                                                                                if(documento==null)
                                                                                {
                                                                                	msgBox('Debe seleccionar el documento que desea remover');
                                                                                	return;
                                                                                }
                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                	if(btn=='yes')
                                                                                    {
                                                                                        function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                //visorDocumentos.getStore().reload();
                                                                                                gEx('arbolCategorias').getRootNode().reload();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesPortal.php',funcAjax, 'POST','funcion=79&idDocumento='+documento.get('idDocumento'),true);
                                                                                	}
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer remover el documento seleccionado?',resp);
                                                                            }
                                                                    
                                                                }
                                                    		]
                                               }
    										)                                                                             
	return panelVisorDocumentos;
}
	
function mostrarVentanaAgregarDocumento(elemento)
{
	var gridDocumentoCompartir=crearGridDocumentoCompartir(false);
	var arrCategorias=new Array();
    var arbolCategorias=gEx('arbolCategorias');
    var raiz=arbolCategorias.getRootNode();
    var x;
    var obj;
    for(x=0;x<raiz.childNodes.length;x++)
    {
    	obj=new Array();
        obj[0]=raiz.childNodes[x].id;
        obj[1]=raiz.childNodes[x].attributes.nombre;
    	arrCategorias.push(obj);
    }
	var cmbCategoria=crearComboExt('cmbCategoria',arrCategorias,150,125,250);
    var idCategoria='0';
    if(iCategoria!='-1')
    {
	   	idCategoria=iCategoria;
    }
    cmbCategoria.setValue(idCategoria);
    var titulo='';
    var descripcion='';
    accionDocumento=1;
    if(elemento!=undefined)
    {
    	accionDocumento=0;
    	titulo=elemento.get('tituloDocumento');
        descripcion=elemento.get('descripcion');
        cmbCategoria.setValue(elemento.get("idCategoria"));
        gridDocumentoCompartir.getStore().loadData(eval(elemento.get("usuariosComparte")));
    }
    else
    	gridDocumentoCompartir.getStore().loadData(eval(nodoSel.attributes.arrPermisosDefault));
    
	var lblTitulo='Agregar documento';
    if(elemento!=undefined)
    {
    	lblTitulo='Modificar documento';
    }
   
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'T&iacute;tulo del documento:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'txtTitulo',
                                                            width:300,
                                                            value:titulo
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Descripci&oacute;n:'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:35,
                                                            id:'txtDescripcion',
                                                            width:400,
                                                            xtype:'textarea',
                                                            height:80,
                                                            value:descripcion
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Categor&iacute;a del documento:'
                                                        },
                                                        cmbCategoria,
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Documento:'
                                                        },
                                                        {
                                                            x:150,
                                                            y:155,
                                                            html:	'<table width="290"><tr><td><div id="uploader"><p>Your browser doesn\'t have Flash, Silverlight or HTML5 support.</p></div></td></tr><tr id="filaAvance" style="display:none"><td align="right">Porcentaje de avance: <span id="porcentajeAvance"> 0%</span></td></tr></table>'
                                                        },
                                                       
                                                        {
                                                            x:445,
                                                            y:156,
                                                            id:'btnUploadFile',
                                                            xtype:'button',
                                                            text:'Seleccionar...',
                                                            handler:function()
                                                                    {
                                                                        $('#containerUploader').click();
                                                                    }
                                                        },
                                                        {
                                                            x:185,
                                                            y:10,
                                                            hidden:true,
                                                            html:	'<div id="containerUploader"></div>'
                                                        },
                                                        {
                                                            x:290,
                                                            y:160,
                                                            xtype:'hidden',
                                                            id:'idArchivo'

                                                        },
                                                        {
                                                            x:290,
                                                            y:160,
                                                            xtype:'hidden',
                                                            id:'nombreArchivo'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            xtype:'fieldset',
                                                            width:580,
                                                            height:200,
                                                            title:'Compartir documento con:',
                                                            items:	[
                                                            			gridDocumentoCompartir
                                                            		]
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vDocumento',
										title: lblTitulo,
										width: 625,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtTitulo').focus(true,500);
                                                                    
                                                                    
                                                                    
                                                                    var cObj={
                                                                                        // Backend settings
                                                                                        upload_url: "../paginasFunciones/procesarDocumento.php", //lquevedor
                                                                                        file_post_name: "archivoEnvio",
                                                                         
                                                                                        // Flash file settings
                                                                                        file_size_limit : "100 MB",
                                                                                        file_types : "*.*",			// or you could use something like: "*.doc;*.wpd;*.pdf",
                                                                                        file_types_description : "Todos los archivos",
                                                                                        file_upload_limit : 0,
                                                                                        file_queue_limit : 1,
                                                                         
                                                                                        
                                                                                        upload_success_handler : subidaCorrecta
                                                                                    };
                                                                 	crearControlUploadHTML5(cObj);   
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var txtTitulo=gEx('txtTitulo');
                                                                        if(txtTitulo.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtTitulo.focus();
                                                                            }
                                                                            msgBox('Debe indicar el t&iacute;tulo del documento',resp)
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                       
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                                                        
                                                                        
																		if(uploadControl.files.length>0)
                                                                     		uploadControl.start();
                                                                        else
                                                                        {
                                                                        	if(accionDocumento==1)
                                                                            {
                                                                            	msgBox('Debe indicar el documento que desea agregar a la galer&iacute;a');
                                                                            	return;
                                                                            }
                                                                        	guardarDocumento();
                                                                        }
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
}    

function crearGridDocumentoCompartir(pAgregar)
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idUnidad'},
                                                                    {name: 'nombreUnidad'},
                                                                    {name: 'tipo'},
                                                                    {name: 'permisos'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Compartido con:',
															width:250,
															sortable:true,
															dataIndex:'nombreUnidad'
														},
														{
															header:'Tipo',
															width:100,
															sortable:true,
															dataIndex:'tipo',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='1')
                                                                        	return 'Rol';
                                                                        return 'Usuario';
                                                                    }
														},
                                                        {
															header:'Permisos',
															width:100,
															sortable:true,
															dataIndex:'permisos'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            x:10,
                                                            id:'tblGridComparte',
                                                            y:10,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:160,
                                                            width:550,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar rol',
                                                                            handler:function()
                                                                            		{
	                                                                                   	agregarRol(pAgregar);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	agregarUsuario(pAgregar);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover rol/usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el rol/usuario con el cual desea dejar de compartir el documento');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover al rol/usuario seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function subidaCorrecta(file, serverData) 
{
	try 
    {
		file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
		
		var arrDatos=serverData.split('|');
		if ( arrDatos[0]!='1') 
		{
			
		} 
		else 
		{
			gEx("idArchivo").setValue(arrDatos[1]);
            gEx("nombreArchivo").setValue(arrDatos[2]);
            guardarDocumento();
			
            
		}
		
	} 
    catch (e) 
	{
		alert(e);
	}
}

function guardarDocumento()
{
	var idDocumento=-1;
    if(accionDocumento==0)
    	idDocumento=documento.get('idDocumento');
        
        
    var cadComparte='';
    var x;
    var tblGridComparte=gEx('tblGridComparte');
    var fila;
    var obj='';
    
    for(x=0;x<tblGridComparte.getStore().getCount();x++)    
    {
    	fila=tblGridComparte.getStore().getAt(x);
        obj='{"idUnidad":"'+fila.get('idUnidad')+'","tipoUnidad":"'+fila.get('tipo')+'","permisos":"'+fila.get('permisos')+'"}';
        if(cadComparte=='')
        	cadComparte=obj;
        else
        	cadComparte+=','+obj;
    }
        
	var cadObj='{"idDocumento":"'+idDocumento+'","titulo":"'+cv(gEx('txtTitulo').getValue())+'","descripcion":"'+cv(gEx('txtDescripcion').getValue())+'","categoria":"'+gEx('cmbCategoria').getValue()+
    			'","archivoId":"'+gEx('idArchivo').getValue()+'","nombreArchivo":"'+gEx('nombreArchivo').getValue()+'","arrComparte":['+cadComparte+']}';	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('arbolCategorias').getRootNode().reload();
            gEx('vDocumento').close();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesPortal.php',funcAjax, 'POST','funcion=78&cadObj='+cadObj,true);
}

function mostrarDetalle(nodo)
{
	var panelDetalle=gEx('panelDetalle').body;
    panelDetalle.hide();
    gEx('modifyDocument').enable();
    gEx('delDocument').enable();
	if(nodo!=null)
    {
    	var registro=gEx('visorDocumentos').getRecord(nodo);
        documento=registro;
        var datos=registro.data;
        plantillaDetalle.overwrite(panelDetalle, datos);
        var extension=registro.get('extension');
        var pos=existeValorMatriz(arrVisores,extension,4);
        if(pos==-1)
            oE('filaVisor');
        else
            mE('filaVisor');
        if(registro.data.lblComplementario!='')
        {
        	mE('filaCompartidoPor1');

        }
        else
        {
        	oE('filaCompartidoPor1');

        }
        var permisos=registro.data.permisos;
        if(permisos.indexOf('M')==-1)
        	gEx('modifyDocument').disable()
        if(permisos.indexOf('E')==-1)
        	gEx('delDocument').disable()
        if(mostrarBarraSel)
        {
        	var cmbTipoVisualizacion=gEx('cmbTipoVisualizacion');
        	if(gE('soloDescarga').value!='1')
            {
                if(pos==-1)
                    cmbTipoVisualizacion.getStore().loadData(tipoVisualizacion1);
                else
                    cmbTipoVisualizacion.getStore().loadData(tipoVisualizacion2);
            }
            else
            	cmbTipoVisualizacion.getStore().loadData(tipoVisualizacion1);
            cmbTipoVisualizacion.setValue('0');
        }
    }
	else
    {
    	documento=null;
        plantillaVacia.overwrite(panelDetalle, {});
    }
    panelDetalle.slideIn('l', {stopFx:true,duration:.2});

}

function descargarDocumento()
{
	var arrParam=[['iD',bE(documento.get('idDocumento'))]];
    enviarFormularioDatos('../media/obtenerDocumentoGaleria.php',arrParam);
}

function visualizarDocumento()
{
	var obj={};
    var extension=documento.get('extension');
    var pos=existeValorMatriz(arrVisores,extension,4);
    var fila=arrVisores[pos];
    obj.titulo=documento.get('tituloDocumento');
    obj.url=fila[1];
    obj.ancho=parseFloat(fila[2]);
    obj.alto=parseFloat(fila[3]);
    obj.params=[['iD',bE(documento.get('idDocumento'))],['cPagina','sFrm=true']];
    abrirVentanaFancy(obj);
}

function agregarRol(pAgregar)
{
    var arrRoles=<?php echo $arrRoles;?>;
    var cmbExtensiones=crearComboExt('cmbExtensiones',[],100,35,250);
	cmbExtensiones.hide();
	var cmbRoles=crearComboExt('cmbRoles',arrRoles,100,5,250);
    function rolSeleccionado(combo,registro,indice)
    {
    	cmbExtensiones.reset();
    	var idRegistro=registro.get('id');
        var arrId=idRegistro.split('_');
        if(arrId[1]!=0)
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
					var arrExtensiones=eval(arrResp[1]);
                    cmbExtensiones.getStore().loadData(arrExtensiones);                
                	cmbExtensiones.show();
		            Ext.getCmp('lblExtension').show();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=20&extension='+arrId[1],true);
        
        	
        }
        else
        {
        	cmbExtensiones.hide();
            Ext.getCmp('lblExtension').hide();
        }
        
    }
    var desplazamiento=0;
    if(pAgregar)
    	desplazamiento=80;
    cmbRoles.on('select',rolSeleccionado);
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
													 	{
                                                        	id:'lblRol',
                                                            x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Rol:'
                                                        },
                                                        cmbRoles,
                                                        {
                                                        	id:'lblExtension',
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'Extensi&oacute;n:',
                                                            hidden:true
                                                        },
                                                        cmbExtensiones,
                                                        {
                                                        	id:'lblPermisos',
                                                            x:10,
                                                            y:70,
                                                            xtype:'label',
                                                            html:'Permisos:'
                                                        },
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:100,
                                                            y:65,
                                                        	id:'chk_A',
                                                            hidden:!pAgregar,
                                                            boxLabel:'Agregar'
                                                        },
                                                       
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:100+desplazamiento,
                                                            y:65,
                                                        	id:'chk_M',
                                                            boxLabel:'Modificar'
                                                        },
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:180+desplazamiento,
                                                            y:65,
                                                        	id:'chk_E',
                                                            boxLabel:'Eliminar'
                                                        }
                                                        
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar rol',
										width:390,
										height:180,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var rol=cmbRoles.getValue();
                                                                var arrId=rol.split('_');
                                                                var extension='0';
                                                                if(arrId[1]!=0)
                                                                	extension=cmbExtensiones.getValue();	
                                                                if(extension=='')
                                                                {
                                                                	function resp()
                                                                    {
                                                                    	cmbExtensiones.focus();
                                                                    }
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe seleccionar una extensi&oacute;n del rol',resp);
                                                                    return;
                                                                }
                                                                var listRoles=gE('listRoles');
                                                                var codigoRolN=arrId[0]+'_'+extension;
                                                                var tblRoles=Ext.getCmp('tblGridComparte');
                                                                var almacenRoles=tblRoles.getStore();
                                                                var rolExiste=obtenerPosFila(almacenRoles,'idUnidad',codigoRolN);
                                                                if(rolExiste==-1)
                                                                {
                                                                	var nExtension=cmbExtensiones.getValue();
                                                                    var txtExtension='';
                                                                    if(nExtension!='')
                                                                    {
                                                                    	txtExtension=' ('+cmbExtensiones.getRawValue()+')';
                                                                    }
                                                                    var textoRol=cmbRoles.getRawValue()+txtExtension;
                                                                    var lPermisos='C';
                                                                    if(gEx('chk_A').getValue())
                                                                    	lPermisos+='A';   
                                                                    if(gEx('chk_M').getValue())
                                                                    	lPermisos+='M';
                                                                    if(gEx('chk_E').getValue())
                                                                    	lPermisos+='E';    
                                                                    var registro=new regComparte	(
                                                                                                        {
                                                                                                            idUnidad:codigoRolN,
                                                                                                            nombreUnidad:textoRol,
                                                                                                            tipo:'1',
                                                                                                            permisos:lPermisos
                                                                                                        }
                                                                                                    );
                                                                    
                                                                    almacenRoles.add(registro);
                                                                    
                                                                }
                                                                
                                                                ventana.close();
																
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();

}

function agregarUsuario(pAgregar)
{
	campoBusqueda=1;
    
    var oConf=	{
                    idCombo:'cmbUsuario',
                    posX:100,
                    posY:35,
                    raiz:'personas',
                    nRegistros:'num',
                    anchoCombo:400,
                    campoDesplegar:'Nombre',
                    campoID:'idUsuario',
                    campoHDestino:'idUsuario',
                    funcionBusqueda:13,
                    paginaProcesamiento:'../paginasFunciones/funcionesAuxiliares.php',
                    confVista:'<tpl for="."><div class="search-item">{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br><b><span class="corpo8_bold">Institucion:</span></b> <span class="letraRojaSubrayada8">{institucion}</span><br><b><span class="corpo8_bold">Departamento:</span></b> <span class="letraRojaSubrayada8">{departamento}</span><br>---<br></div></tpl>',
                    campos:	[
                                {name:'idUsuario'},
                                {name:'Nombre'},
                                {name: 'Paterno'},
                                {name: 'Materno'},
                                {name: 'Nom'},
                                {name: 'institucion'},
                                {name: 'departamento'}
                                
                            ],
                    funcAntesCarga:function(dSet,combo)
                                {
                                    gE('idUsuario').value='-1';
                                    var aValor=combo.getRawValue();
                                    dSet.baseParams.criterio=aValor;
                                    dSet.baseParams.campoBusqueda=campoBusqueda;
                                },
                    funcElementoSel:function(combo,registro)
                                {
                                    gE('idUsuario').value=registro.get('idUsuario');
                                   
                                }  
                };
    
    
    var cmbComboBusqueda=crearComboExtAutocompletar(oConf);
    var desplazamiento=0;
    if(pAgregar)
    	desplazamiento=80;
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Buscar por:'
                                                        },
                                            			{
                                                        	x:100,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:180,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:260,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Usuario:'
                                                        },
                                                        cmbComboBusqueda,
                                                        {
                                                        	id:'lblPermisos',
                                                            x:10,
                                                            y:70,
                                                            xtype:'label',
                                                            html:'Permisos:'
                                                        },
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:100,
                                                            y:65,
                                                        	id:'chk_A',
                                                            hidden:!pAgregar,
                                                            boxLabel:'Agregar'
                                                        },
                                                       
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:100+desplazamiento,
                                                            y:65,
                                                        	id:'chk_M',
                                                            boxLabel:'Modificar'
                                                        },
                                                        {
                                                        	xtype:'checkbox',
                                                        	x:180+desplazamiento,
                                                            y:65,
                                                        	id:'chk_E',
                                                            boxLabel:'Eliminar'
                                                        }

													]
										}
									);
                                    
                                    
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar usuario',
										width: 580,
										height:190,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbComboBusqueda.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    
                                                                    
                                                                    	var tblRoles=Ext.getCmp('tblGridComparte');
                                                                        var almacenRoles=tblRoles.getStore();
                                                                        var idUsuario=gE('idUsuario').value;
                                                                        if(idUsuario=='-1')
                                                                        {
                                                                        	msgBox('Debe seleccionar el usuario con el cual desea compartir el documento');
                                                                        	return;
                                                                        }
                                                                        var usrExiste=obtenerPosFila(almacenRoles,'idUnidad',idUsuario);
                                                                    	if(usrExiste==-1)
                                                                        {
                                                                            var textoRol=cmbComboBusqueda.getRawValue();
                                                                            var lPermisos='C';
                                                                            if(gEx('chk_A').getValue())
                                                                    			lPermisos+='A';   
                                                                            if(gEx('chk_M').getValue())
                                                                                lPermisos+='M';
                                                                            if(gEx('chk_E').getValue())
                                                                                lPermisos+='E';    
                                                                            var registro=new regComparte	(
                                                                                                                {
                                                                                                                    idUnidad:idUsuario,
                                                                                                                    nombreUnidad:textoRol,
                                                                                                                    tipo:'0',
                                                                                                                    permisos:lPermisos
                                                                                                                }
                                                                                                            );
                                                                            
                                                                            almacenRoles.add(registro);
																		}
                                                                        ventanaAM.close();
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();                                    
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}