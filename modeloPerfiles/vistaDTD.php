<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");
$arrConfiguraciones="";
$actorEtapa1="";


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<link rel="stylesheet" type="text/css" href="../estilos/layout-browser.css"/>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<script type="text/javascript" src="../Scripts/miFrame/multidom.js.jgz"></script>
<script type="text/javascript" src="../Scripts/miFrame/mif.js.jgz"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/ventanaMensajeErrores.js.php"></script>

<script type="text/javascript" src="../Scripts/libreriaVisoresDocumentos.js.php"></script>
<?php
	$idFormulario=-1;
	if(isset($_POST["idFormulario"]))
		$idFormulario=$_POST["idFormulario"];
	else
		if(isset($_GET["idFormulario"]))
			$idFormulario=$_GET["idFormulario"];
			
			
	$idRegistro=-1;
	if(isset($_POST["idRegistro"]))
		$idRegistro=$_POST["idRegistro"];
	else
		if(isset($_GET["idRegistro"]))
			$idRegistro=$_GET["idRegistro"];
	$consulta="select idProceso from 900_formularios  where idFormulario=".$idFormulario;
	$idProceso=$con->obtenerValor($consulta);
	$consulta="SELECT * FROM 9057_configuracionSeccionesProceso WHERE idProceso=".$idProceso;
	$resSecc=$con->obtenerFilas($consulta);
	while($fSecc=mysql_fetch_row($resSecc))
	{
		if($fSecc[2]!="")
			echo '<script type="text/javascript" src="'.$fSecc[2].'?iR='.$idRegistro.'"></script>';
	}
	
	if($con->existeCampo("archivosJS","4001_procesos"))
	{
		$consulta="SELECT archivosJS FROM 4001_procesos WHERE idProceso=".$idProceso;
		$archivosJS=$con->obtenerValor($consulta);
		if($archivosJS!="")
		{
			$aArchivos=explode(",",$archivosJS);
			
			foreach($aArchivos as $a)
			{
				echo '<script type="text/javascript" src="'.$a.'?iF='.$idFormulario.'&iR='.$idRegistro.'"></script>';
			}
			
		}
	}
	
?>
<style type="text/css">
<!--
@import url("../css/estiloFinal.css");
-->
</style>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
$paramPOST=true;
$paramGET=true;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}

if(isset($objParametros->titulo))
	$tituloPagina=$objParametros->titulo;

?>
<title><?php echo $tituloPagina ?></title>
</head>
<body>

<?php 
	  $idUsuario=$_SESSION["idUsr"];
	  if(isset($objParametros->idUsuario))
		  $idUsuario=$objParametros->idUsuario;
	  $nProfesor=obtenerNombreUsuario($idUsuario);
	  
	  if(!isset($objParametros->idFormulario))
	  {
		  
   		  enviarPagina("../principal/inicio.php");
		  return;
	  }
	  $actor=0;
	  

	  if(isset($objParametros->actor))
	  {
		  
			$actor=base64_decode($objParametros->actor);
			if($actor=="")	  
				$actor=0;
		 
		  
	  }
	  $accion="";
	  if(isset($objParametros->dComp))
			$accion=base64_decode($objParametros->dComp);
	 
	  $dComp=$objParametros->dComp;
	  $idFormulario=$objParametros->idFormulario;
	  $idRegistro=$objParametros->idRegistro;
	  $idProceso=obtenerIdProcesoFormulario($idFormulario);

	  $consulta="select version,date_format(fechaRegistro,'%d/%m/%Y') as fechaRegistro,version,u.Nombre from 9036_respaldosProceso r,800_usuarios u where u.idUsuario=r.idUsuarioResp and idProceso=".$idProceso." and idRegistro=".$idRegistro;
	  $arrVersiones=$con->obtenerFilasArreglo($consulta);
	  
	  $participante="0";
	  if(isset($objParametros->participante))
	  	$participante=$objParametros->participante;
	  
	  
	  
	  	$idProcesoP="";
	  	if(isset($objParametros->idProcesoP))
	  		$idProcesoP=$objParametros->idProcesoP;
	  	$idReferencia="";
		if(isset($objParametros->idReferencia))
	  		$idReferencia=$objParametros->idReferencia;
	  
		  $funcEjecutarNuevo="";
		  if(isset($objParametros->funcEjecutarNuevo))
			$funcEjecutarNuevo=$objParametros->funcEjecutarNuevo;
		  $funcEjecutarModif="";
		  if(isset($objParametros->funcEjecutarModif))
			$funcEjecutarNuevo=$objParametros->funcEjecutarModif;
			
			
		  $funcPHPEjecutarNuevo="";
		  if(isset($objParametros->funcPHPEjecutarNuevo))
			$funcPHPEjecutarNuevo=$objParametros->funcPHPEjecutarNuevo;
		  
		  $funcPHPEjecutarModif="";
		  if(isset($objParametros->funcPHPEjecutarModif))
			$funcPHPEjecutarModif=$objParametros->funcPHPEjecutarModif;
	  
	  	$funcEJs="";
	  	if(isset($objParametros->funcEJs))
			$funcEJs=$objParametros->funcEJs;
	  
	  if($idRegistro=="-1")
	  {
		  $idProceso=obtenerIdProcesoFormulario($idFormulario);	
		  
		  if(!isset($objParametros->actorInicio))
		  {
			  
		  	$consulta="select actor from 949_actoresVSAccionesProceso where idActorVSAccionesProceso=".$actor;
		  }
		  else
		  {
		  	$consulta="SELECT actor FROM 950_actorVSProcesoInicio WHERE idActorProcesoInicio=".$actor;
			
			
			
			
		  }
		  $rolActor=$con->obtenerValor($consulta);
		  $consulta="select idActorProcesoEtapa from 944_actoresProcesoEtapa where idProceso=".$idProceso." and numEtapa=1 and actor='".$rolActor."' and tipoActor=1";

		  $actorEtapa1=$con->obtenerValor($consulta);
		  if($actorEtapa1=="")
		  	$actorEtapa1="0";
	  }
	  else
	  {
	  	$actorEtapa1=$actor;
		$consulta="select idReferencia from _".$idFormulario."_tablaDinamica where id__".$idFormulario."_tablaDinamica=".$idRegistro;
		$idReferencia=$con->obtenerValor($consulta);
		if($idReferencia=="-1")
			$idReferencia="";
		
	  }
	  $idProceso=obtenerIdProcesoFormulario($idFormulario);

	  
?>


	<div id="header">
    	<script type="text/javascript" src="../Scripts/ext/menus/uxMenu.js"></script>
    	<script type="text/javascript" src="Scripts/vistaDTD.js.php?idP=<?php echo bE($participante)?>&iP=<?php echo bE($idProceso)?>&iF=<?php echo bE($idFormulario)?>&iR=<?php echo bE($idRegistro)?>&iA=<?php echo bE($actor)?>"></script>
    	<table width="100%">
        <tr height="10">
        	<td></td>
        </tr>
        <tr>
        	<td width="10">
            	
            </td><!--
        	<td width="60">
            	<a href="<?php echo $pagRegresar ?>" class="letraVerde">
                	<img width="24" height="24" src="../images/flechaizq.gif" border="0" />
                </a>
            </td>-->
            <td align="left" width="300"><span class="letraExt"><b>Usuario: </b>&nbsp;<?php  echo $nProfesor?> </span></td>
            <td align="left" width="200">
            </td>
            <td >
            </td>
            <td align="left">
            </td>
        </tr>
        </table>
    </div>
    
    <input type="hidden" id="regVersiones" value="<?php echo $arrVersiones?>" />
    <input type="hidden" name="idFormulario" value="<?php echo $idFormulario?>" id="idFormularioAux" />
    <input type="hidden" name="idRegistroAux" value="<?php echo $idRegistro?>" id="idRegistroAux" />
    <input type="hidden" name="accion" value="<?php echo base64_encode($accion)?>" id="accionAux" />
    <input type="hidden" name="eJsAux" value="<?php echo base64_encode("window.parent.mostrarMenuDTD()") ?>" id="eJsAux" />
    <input type="hidden" name="funcPHPEjecutarNuevo" id="funcPHPEjecutarNuevo" value="<?php echo $funcPHPEjecutarNuevo?>" />
   	<input type="hidden" name="funcEJs" id="funcEJs" value="<?php echo $funcEJs?>" />
    <?php 
        
		
        $consulta="select pagPrincipalReg from 921_tiposProceso tp,4001_procesos p where tp.idTipoProceso=p.idTipoProceso and p.idProceso=".$idProceso;
		
        $pagPrincipal=$con->obtenerValor($consulta);
		$consulta="SELECT * FROM 9046_botonesProceso WHERE idProceso=".$idProceso." order by texto";
		$resBotones=$con->obtenerFilas($consulta);
		$arrBtn="";
		while($fila=mysql_fetch_row($resBotones))
		{
			$icono="../images/Icono_txt.gif";
			if($fila[3]!="")
				$icono=$fila[3];
			
			$obj="{
					  icon:'".$icono."',
					  cls:'x-btn-text-icon',
					  text:'".$fila[2]."',
					  handler:function()
							  {";
			if($fila[5]==0)							  
			{
				$obj.="var arrParam=[['idFormulario','".$idFormulario."'],['idRegistro','".$idRegistro."']];
						enviarFormularioDatos('".$fila[1]."',arrParam);
				";
			}
			else
			{
				$obj.=$fila[1]."();";
			}
								  
			$obj.="			  }
					  
				  }";
			if($arrBtn=="")
				$arrBtn=$obj;
			else
				$arrBtn.=",".$obj;
		}
		$arrBtn="[".$arrBtn."]";
		
		$arrParamIgnorar=array();
		array_push($arrParamIgnorar,"confReferencia");
		array_push($arrParamIgnorar,"cPagina");
		array_push($arrParamIgnorar,"idRegistro");
		array_push($arrParamIgnorar,"idFormulario");
		array_push($arrParamIgnorar,"dComp");
		array_push($arrParamIgnorar,"actor");
		array_push($arrParamIgnorar,"paginaConf");
		$arrParamComp='';
		$cadParam="";
		$reflectionClase = new ReflectionObject($objParametros);
		foreach ($reflectionClase->getProperties() as $property => $value) 
		{
			$nombre=$value->getName();
			$valor=$value->getValue($objParametros);
			if(!existeValor($arrParamIgnorar,$nombre))
			{
				$o='<input type="hidden" id="'.$nombre.'" name="'.$nombre.'" value='.$valor.'>';
				if($cadParam=="")
					$cadParam=$o;
				else
					$cadParam.="".$o;
				if($arrParamComp=="")
					$arrParamComp="['".$nombre."','".$valor."']";
				else
					$arrParamComp.=",['".$nombre."','".$valor."']";
			}
		}
		
    ?>
    
    <input type="hidden" id="actor" name="actor" value="<?php echo base64_encode($actor)?>" />
    <input type="hidden" id="pagPrincipal" value="<?php echo $pagPrincipal?>" />
    <input type="hidden" name="dComp" id="dComp" value="<?php echo base64_encode($accion)?>" />
    <form id="frmEnvioNuevo" method="post" action="vistaDTD.php">
    <?php
		echo $cadParam;
	?>
        <input type="hidden" id="actorEtapa1" name="actor" value="<?php echo base64_encode($actorEtapa1)?>" />
        <input type="hidden" name="confReferencia" value="<?php echo $nConfRegresar ?>" />
        <input type="hidden" name="dComp" id="dCompAux" value="<?php echo base64_encode("modificar")?>" />
        <input type="hidden" name="idFormulario" value="<?php echo $idFormulario?>" />
        <input type="hidden" name="idRegistro" id="registroRecarga" value="" />
        <input type="hidden" name="participante" value="<?php echo $participante?>" id="participante" />
        <input type="hidden" name="idProcesoP" id='idProcesoP' value="<?php echo $idProcesoP ?>" />
        <input type="hidden" name="idReferencia" id="idReferencia" value="<?php echo $idReferencia ?>" />
        <input type="hidden" name="idUsuario" id="idUsuario" value="<?php echo $idUsuario ?>" />

    </form>
    
	<!--<form method="post" action="vistaProfesor.php" id='frmEnvio'>
        <input type="hidden" id="idUsuario" name="idUsuario" value="<?php echo $idUsuario?>" />
        
    </form>-->
    <input type="hidden" id="nProfesor" value="<?php echo $nProfesor ?>" />
    <input type="hidden" id="arrParamComp" value="<?php echo bE("[".$arrParamComp."]")?>" />
    <input type="hidden" id="arrBtn" value="<?php echo bE($arrBtn) ?>" />
    <form method="post"	action="" id='frmEnvioDatos'>
    	<input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
    </form>
    <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
        <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
    </form>
    <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
        <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
    </form>
    <script>
		<?php 
			//echo $arrConfiguraciones;
			//echo $scriptFrmEnvio;
		?>
	</script>
</body>

</html>