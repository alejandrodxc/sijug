<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago";
	$arrPago=$con->obtenerFilasArreglo($consulta);
	$fechaActual=date("Y-m-d");
	
	$consulta="SELECT idBanco,nombreCorto FROM 6000_bancos ORDER BY nombreCorto";
	$arrBancos=$con->obtenerFilasArreglo($consulta);
	
	
?>


var arrBancos=<?php echo $arrBancos?>;

function generarPanelEfectivo(iFPago)
{
	
	var panel=	 new Ext.Panel	(
                                	{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:695,
                                        border:false,
                                        baseCls: 'x-plain',
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	
                                                                        
                                                                        
                                                                         $('#cantidadRecibidaEfectivo').ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDiferenciasEfectivo(gE('cantidadRecibidaEfectivo'));
                                                                                                                     }
                                                                                                        )
                                                                        
                                                                        
                                                                        registrarEventosCobroHTML();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                        
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        
                                                                        
                                                                        gE('cantidadRecibidaEfectivo').focus();
                                                                    }
                                                                }
                                                    },
                                        height:180,
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:675,
                                                        height:160,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago en efectivo</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        id:'lblCantidadRecibida',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        id:'txtCantidadRecibidaEfectivo',
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasEfectivo(this)" />'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        id:'lblEtCambio',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:60,
                                                                        id:'lblValorEtCambio',
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobro">$ 0.00</span>'
                                                                    }
                                                            	]           
                                                    }    
                                                ]	
                                    }
								)
	                         
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo');
                        if(cantidadRecibidaEfectivo.value=='')
                            cantidadRecibidaEfectivo.value=0;
                        var cantidadRec=parseFloat(normalizarValor(cantidadRecibidaEfectivo.value));
                        if(cantidadRec<total)
                        {
                            function resp()
                            {
                                cantidadRecibidaEfectivo.focus();
                            }
                            msgBox('La cantidad recibida no cubre el total de la venta',resp);
                            return false;
                        }
                        diferencia=cantidadRec-total;
                        if(diferencia<0)
                            diferencia=0;
                        gE('lblCambioCobro').innerHTML=Ext.util.Format.usMoney(diferencia);
			            var dCompra={};
                        dCompra.cadObj='{"cantidadRecibida":"'+cantidadRecibidaEfectivo.value+'","cambio":"'+normalizarValor(gE('lblCambioCobro').innerHTML)+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('cantidadRecibidaEfectivo').value="$ 0.00";
                            gE('lblCambioCobro').innerHTML="$ 0.00";
                        	
                        }                        
                                                                                        
	return panel;                                
}

function calcularDiferenciasEfectivo(ctrl)
{
	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
    if(ctrl.value=='')
    	ctrl.value=0;
    var cantidadRec=parseFloat(normalizarValor(ctrl.value));
    var diferencia;
    
    diferencia=cantidadRec-total;
    if(diferencia<0)
    	diferencia=0;
    cambioUltimaVenta=diferencia;
    gE('lblCambioCobro').innerHTML=Ext.util.Format.usMoney(diferencia);	
}

///

function generarPanelTarjeta(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('noAutorizacion').focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Tarjeta de Crédito/Débito</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        
                                                                        id:'lblNoAutorizacion',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        
                                                                        id:'txtNoAutorizacion',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        
                                                                        id:'lblNoAutorizacion2',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        
                                                                        id:'txtNoAutorizacion2',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        
                                                                        id:'lblDigitos',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:95,
                                                                        
                                                                        id:'txtDigitosTarjeta',
                                                                        html:'<input class="btnEvento" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var noAutorizacion=gE('noAutorizacion');
                        var noAutorizacion2=gE('noAutorizacion2');
                        var txtDigitosTarjeta=gE('digitosTarjeta');
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('noAutorizacion').value="";
                            gE('noAutorizacion2').value="";
                            gE('digitosTarjeta').value="";
                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelTarjetaEfectivo(iFPago)
{
	var altoPanel=300;
	var panel=	 new Ext.Panel	(
									{
                                        x:5,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:780,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAbonoTarjeta_3').focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
																	
                                                                    	 $('.btnEnter_3').ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDiferenciaPagoCombinado();
                                                                                                                     }
                                                                                                        )
                                                                                                                                                                                                             
                                                                                                     
                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:760,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Combinado (Efectivo-Tarjeta)</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                    	x:0,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:380,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Pago tarjeta cr&eacute;dito/d&eacutebito</span>',
                                                                    	items:	[
                                                                        			 {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Pago:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        
                                                                                        id:'txtCantidadAbonoTarjeta_3',
                                                                                        html:'<input type="text" onkeyup="cantidadAbonoTarjetaChange(this,3)" class="btnEvento btnEnter_3" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoTarjeta_3" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciaPagoCombinado(this)" />'
                                                                                    },
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        
                                                                                        id:'lblNoAutorizacion1_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        
                                                                                        id:'txtNoAutorizacion1_3',
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_3" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion1_3" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                     {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:85,
                                                                                        
                                                                                        id:'lblNoAutorizacion2_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:80,
                                                                                        
                                                                                        id:'txtNoAutorizacion2_3',
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_3" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2_3" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        
                                                                                        id:'lblDigitos_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:120,
                                                                                        
                                                                                        id:'txtDigitosTarjeta_3',
                                                                                        html:'<input disabled=true class="btnEvento btnEnter_3" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_3" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    }
                                                                        		]
                                                                    },
                                                                    {
                                                                    	x:390,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:340,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Pago en efectivo</span>',
                                                                    	items:	[
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Pago:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:5,
                                                                                        id:'lblValorPagoEfectivo_3',
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoPagoEfectivo_3">$ 0.00</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        id:'lblCantidadRecibidaFectivo_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        id:'txtCantidadRecibidaEfectivo_3',
                                                                                        html:'<input type="text" class="btnEvento btnEnter_3" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_3" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciaPagoCombinado(this)" />'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        id:'lblEtCambioEfectivo_3',
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:125,
                                                                                        id:'lblValorEtCambioEfectivo_3',
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroEfectivo_3">$ 0.00</span>'
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                        		]
                                                                      },
                                                                    {
                                                                        xtype:'label',
                                                                        x:390,
                                                                        y:210,
                                                                        id:'lblMontoRestante_3',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto Restante:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:600,
                                                                          y:210,
                                                                          id:'lblValorMontoRestante_3',
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoRestanteTotal_3">$ 0.00</span>'
                                                                      }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        
                        
                        
                    	var cantidadAbonoTarjeta=gE('cantidadAbonoTarjeta_3');
                        
                        
                        var montoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta.value));
                        
                        
                        if(montoTarjeta>total)
                        {
                        	function resp10()
                            {
                                cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad recibida atrav&eacute;s de tarjeta es mayor que el total de la venta',resp10);
                            return false;
                        }
                        
                    	var noAutorizacion=gE('noAutorizacion1_3');
                        var noAutorizacion2=gE('noAutorizacion2_3');
                        var txtDigitosTarjeta=gE('digitosTarjeta_3');
                        
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_3');
                        
                        if(cantidadRecibidaEfectivo.value=='')
                        	cantidadRecibidaEfectivo.value='$ 0.00';
                        
                        if(cantidadAbonoTarjeta.value=='')
                        {
                            function resp0()
                            {
                                cantidadAbonoTarjeta.focus();
                            }
                            msgBox('Debe ingresar el onto que abona atrv&eacute;s de tarjeta',resp0);
                            return false;
                        }
                        
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        
                        var montoRestante=parseFloat(normalizarValor(gE('lblMontoRestanteTotal_3').innerHTML));
                        
                        
                        if(montoRestante>0)
                        {
                            function resp()
                            {
                                cantidadRecibidaEfectivo.focus();
                            }
                            msgBox('La cantidad recibida atrav&eacute;s de tarjeta y efectivo NO cubren el total de la venta',resp);
                            return false;
                        }
                        
                        
                       	var dCompra={};
                        dCompra.cadObj='{"pagoEfectivo":{"montoPagado":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'","cambio":"'+normalizarValor(gE('lblCambioCobroEfectivo_3').innerHTML)+'"},"pagoTarjeta":{"montoPagado":"'+normalizarValor(cantidadAbonoTarjeta.value)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}}';
                        dCompra.desgloceFormaPago='[{"formaPago":"2","montoPago":"'+normalizarValor(cantidadAbonoTarjeta.value)+'"},{"formaPago":"1","montoPago":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'"}]';
                        return dCompra;
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                            gE('cantidadAbonoTarjeta_3').value="$ 0.00";
                        	gE('noAutorizacion1_3').value="";
                            gE('noAutorizacion2_3').value="";
                            gE('digitosTarjeta_3').value="";
                        	gE('noAutorizacion1_3').disabled=true
                            gE('noAutorizacion2_3').disabled=true
                            gE('digitosTarjeta_3').disabled=true
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function calcularDiferenciaPagoCombinado(ctrl)
{
	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
    var cantidadAbonoTarjeta_3=gE('cantidadAbonoTarjeta_3');
    if(cantidadAbonoTarjeta_3.value=='')
    	cantidadAbonoTarjeta_3.value='$ 0.00';
        
    var cantidadRecibidaEfectivo_3=gE('cantidadRecibidaEfectivo_3');
    if(cantidadRecibidaEfectivo_3.value=='')
    	cantidadRecibidaEfectivo_3.value='$ 0.00';
    
    var totalAbonoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta_3.value));
    var totalAbonoEfectivo=parseFloat(normalizarValor(cantidadRecibidaEfectivo_3.value));
    
        
    var cantidadRec=totalAbonoTarjeta+totalAbonoEfectivo;
    var diferencia=total-totalAbonoTarjeta;
    
	if(diferencia<0)
    	diferencia=0;
   
    gE('lblMontoPagoEfectivo_3').innerHTML=Ext.util.Format.usMoney(diferencia);		
    
    diferencia=totalAbonoEfectivo-parseFloat(normalizarValor(gE('lblMontoPagoEfectivo_3').innerHTML));
    if(diferencia<0)
    	diferencia=0;
    cambioUltimaVenta=diferencia;
    gE('lblCambioCobroEfectivo_3').innerHTML=Ext.util.Format.usMoney(diferencia);		
    
    diferencia=total-cantidadRec;
    if(diferencia<0)
    	diferencia=0;
        
        
        
        
    gE('lblMontoRestanteTotal_3').innerHTML=Ext.util.Format.usMoney(diferencia);		

}

function generarPanelAbono()
{
	var panel=	 new Ext.Panel	(
                                	{
                                        x:50,
                                        y:70,
                                        id:'pEfectivo',
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:250,
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:230,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago en efectivo</span>',
                                                    	items:	[
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:40,
                                                                        id:'lblAbono',
                                                                       
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Pago:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:35,
                                                                        
                                                                        id:'txtCantidadAbono',
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAbono" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="cantidadRecibidaUnfocus(this)" />'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:80,
                                                                        id:'lblCantidadRecibida',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:85,
                                                                        id:'txtCantidadRecibida',
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadRecibida" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasEfectivo(this)" />'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:120,
                                                                        id:'lblEtCambio',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:120,
                                                                        id:'lblValorEtCambio',
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobro">$ 0.00</span>'
                                                                    }
                                                            	]           
                                                    }    
                                                ]	
                                    }
								)
	panel.getDatosVenta=function()
    				{
                    
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('cantidadRecibidaEfectivo').value="$ 0.00";
                            gE('lblCambioCobro').innerHTML="$ 0.00";
                        	
                        }                                                                    
	return panel;                                
}

function generarPanelVentaCredito(iPago)
{
	var iFPago=parseInt(iPago)+10;
	var altoPanel=300;
	var panel=	 new Ext.Panel	(
									{
                                        x:5,
                                        y:70,
                                        id:'panel_'+iPago,
                                        layout:'absolute',
                                        width:780,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAbonoTarjeta_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
																	
                                                                    	 $('.btnEnter_'+iFPago).ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDatosOperacion(iFPago);
                                                                                                                     }
                                                                                                        )
                                                                                                                                                                                                             
                                                                                                     
                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:760,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Venta a crédito</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                    	x:0,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:380,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono tarjeta cr&eacute;dito/d&eacutebito</span>',
                                                                    	items:	[
                                                                        			 {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        
                                                                                        id:'txtCantidadAbonoTarjeta_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoTarjeta_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onkeyup="cantidadAbonoTarjetaChange(this,'+iFPago+')" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        
                                                                                        id:'lblNoAutorizacion1_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprobaci&oacute;n:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        
                                                                                        id:'txtNoAutorizacion1_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                     {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:85,
                                                                                        
                                                                                        id:'lblNoAutorizacion2_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:80,
                                                                                        
                                                                                        id:'txtNoAutorizacion2_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        
                                                                                        id:'lblDigitos_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:120,
                                                                                        id:'txtDigitosTarjeta_'+iFPago,
                                                                                        html:'<input disabled=true class="btnEvento btnEnter_'+iFPago+'" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_'+iFPago+'" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    }
                                                                        		]
                                                                    },
                                                                    {
                                                                    	x:390,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:340,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono en efectivo</span>',
                                                                    	items:	[
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        id:'txtCantidadAbonoEfectivo_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoEfectivo_'+iFPago+'" value="$ 0.00" onkeyup="cantidadAbonoEfectivoChange(this,'+iFPago+')" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        id:'lblCantidadRecibidaFectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        id:'txtCantidadRecibidaEfectivo_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        id:'lblEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:125,
                                                                                        id:'lblValorEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroEfectivo_'+iFPago+'">$ 0.00</span>'
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                        		]
                                                                      },
                                                                       {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:210,
                                                                        id:'lblMontoTotalAbono_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto total abono:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:220,
                                                                          y:210,
                                                                          id:'lblValorMontoAbono_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoAbonoTotal_'+iFPago+'">$ 0.00</span>'
                                                                      },
                                                                    {
                                                                        xtype:'label',
                                                                        x:390,
                                                                        y:210,
                                                                        id:'lblMontoRestante_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto Restante:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:600,
                                                                          y:210,
                                                                          id:'lblValorMontoRestante_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoRestanteTotal_'+iFPago+'">$ 0.00</span>'
                                                                      }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        
                    	var cantidadAbonoTarjeta=gE('cantidadAbonoTarjeta_'+iFPago);
                        
                        var montoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta.value));
                        
                        if(montoTarjeta>total)
                        {
                        	function resp10()
                            {
                                cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad recibida atrav&eacute;s de tarjeta es mayor que el total de la venta',resp10);
                            return false;
                        }
                        
                    	var noAutorizacion=gE('noAutorizacion1_'+iFPago);
                        var noAutorizacion2=gE('noAutorizacion2_'+iFPago);
                        var txtDigitosTarjeta=gE('digitosTarjeta_'+iFPago);
                        
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_'+iFPago);
                        
                        if(cantidadRecibidaEfectivo.value=='')
                        	cantidadRecibidaEfectivo.value='$ 0.00';
                        
                        if(parseFloat(normalizarValor(cantidadAbonoTarjeta.value))>0)
                        {
                           
                        
                            if(noAutorizacion.value=='')
                            {
                                function resp1()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                                return false;
                            }
                            if(noAutorizacion.value!=noAutorizacion2.value)
                            {
                                function resp2()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                                return false;
                            }
                            if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                            {
                                function resp3()
                                {
                                    txtDigitosTarjeta.focus();
                                }
                                msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                                return false;
                            }
                        }
                        
                       var cantidadAbonoEfectivo=gE('cantidadAbonoEfectivo_'+iFPago);
                       var montoCantidadAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo.value));
                       
                       if(montoCantidadAbonoEfectivo>0)
                       {
                       		var montoCantidadRecibidaEfectivo=parseFloat(normalizarValor(cantidadRecibidaEfectivo.value));
                            if(montoCantidadRecibidaEfectivo<montoCantidadAbonoEfectivo)
                            {
                            	function resp100()
                                {
                                	cantidadRecibidaEfectivo.focus();
                                }
                                msgBox('La cantidad recibida en efectivo es menor que la cantidad por abonar',resp100);
                                return;
                            
                            }
                       }
                       
                       
                       var totalAbono=parseFloat(normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML));
                       
                       if(totalAbono>total)
                       {
                       		function resp101()
                            {
                            	cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad a abonar excede el monto total de la venta',resp101);
                            return;
                       }
                       else
                       {
                       		if(totalAbono==0)
                            {
                                function resp102()
                                {
                                    cantidadAbonoTarjeta.focus();
                                }
                                msgBox('La cantidad a abonar no puede ser igual a $ 0.00',resp102);
                                return;
                            }
                       }
                       
                       
                       	var dCompra={};
                        dCompra.cadObj='{"montoAbono":"'+normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML)+'","pagoEfectivo":{"montoPagado":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'","cambio":"'+normalizarValor(gE('lblCambioCobroEfectivo_'+iFPago).innerHTML)+'"},"pagoTarjeta":{"montoPagado":"'+normalizarValor(cantidadAbonoTarjeta.value)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}}';
                        dCompra.desgloceFormaPago='[{"formaPago":"2","montoPago":"'+normalizarValor(cantidadAbonoTarjeta.value)+'"},{"formaPago":"1","montoPago":"'+montoCantidadAbonoEfectivo+'"}]';
                        
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                            gE('cantidadAbonoTarjeta_'+iFPago).value="$ 0.00";
                        	gE('noAutorizacion1_'+iFPago).value="";
                            gE('noAutorizacion2_'+iFPago).value="";
                            gE('digitosTarjeta_'+iFPago).value="";
                            
                            gE('noAutorizacion1_'+iFPago).disabled=true;
                            gE('noAutorizacion2_'+iFPago).disabled=true;
                            gE('digitosTarjeta_'+iFPago).disabled=true;
                            
                            gE('cantidadAbonoEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).disabled=true;
                            
                            gE('lblCambioCobroEfectivo_'+iFPago).value="$ 0.00";
                            gE('lblMontoAbonoTotal_'+iFPago).value="$ 0.00";
                            gE('lblMontoRestanteTotal_'+iFPago).value="$ 0.00";
                        }    
                    
                                                    
	return panel;                                  
}

function calcularDatosOperacion(iFPago)
{
	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
    var cantidadAbonoTarjeta_3=gE('cantidadAbonoTarjeta_'+iFPago);
    if(cantidadAbonoTarjeta_3.value=='')
    	cantidadAbonoTarjeta_3.value='$ 0.00';



	var cantidadAbonoEfectivo_3=gE('cantidadAbonoEfectivo_'+iFPago);
    if(cantidadAbonoEfectivo_3.value=='')
    	cantidadAbonoEfectivo_3.value='$ 0.00';
        
    var cantidadRecibidaEfectivo_3=gE('cantidadRecibidaEfectivo_'+iFPago);
    if(cantidadRecibidaEfectivo_3.value=='')
    	cantidadRecibidaEfectivo_3.value='$ 0.00';
    
    var totalAbonoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta_3.value));
    /*
    if(totalAbonoTarjeta==0)
    {
    	gE('noAutorizacion1_'+iFPago).disabled=true;
        gE('noAutorizacion2_'+iFPago).disabled=true;
        gE('digitosTarjeta_'+iFPago).disabled=true;
    }
    else
    {
    	gE('noAutorizacion1_'+iFPago).disabled=false;
        gE('noAutorizacion2_'+iFPago).disabled=false;
        gE('digitosTarjeta_'+iFPago).disabled=false;
    
    }*/
    
    var totalAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo_3.value));
    
        
    var cantidadRec=totalAbonoTarjeta+totalAbonoEfectivo;
    
    
    
    var diferencia=parseFloat(normalizarValor(cantidadRecibidaEfectivo_3.value))-totalAbonoEfectivo;
    if(diferencia<0)
    	diferencia=0;
    cambioUltimaVenta=diferencia;
    gE('lblCambioCobroEfectivo_'+iFPago).innerHTML=Ext.util.Format.usMoney(diferencia);		
    
    diferencia=total-cantidadRec;
    if(diferencia<0)
    	diferencia=0;

	gE('lblMontoAbonoTotal_'+iFPago).innerHTML=Ext.util.Format.usMoney(cantidadRec);		        
        
    gE('lblMontoRestanteTotal_'+iFPago).innerHTML=Ext.util.Format.usMoney(diferencia);		

}

function cantidadAbonoTarjetaChange(ctrl,iFPago)
{
	var valor;
	if(normalizarValor(ctrl.value)=='')
    {
    	valor=0;
    }
    else
     	valor=parseFloat(normalizarValor(ctrl.value));
    if(valor==0)
    {
    	gE('noAutorizacion1_'+iFPago).value="";
		gE('noAutorizacion2_'+iFPago).value="";
        gE('digitosTarjeta_'+iFPago).value="";
        gE('noAutorizacion1_'+iFPago).disabled=true;
		gE('noAutorizacion2_'+iFPago).disabled=true;
        gE('digitosTarjeta_'+iFPago).disabled=true;
    }
    else
    {
    	gE('noAutorizacion1_'+iFPago).disabled=false;
		gE('noAutorizacion2_'+iFPago).disabled=false;
        gE('digitosTarjeta_'+iFPago).disabled=false;
    }
    
}

function cantidadAbonoEfectivoChange(ctrl,iFPago)
{
	var valor;
	if(normalizarValor(ctrl.value)=='')
    {
    	valor=0;
    }
    else
     	valor=parseFloat(normalizarValor(ctrl.value));
    if(valor==0)
    {
    	gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
        gE('cantidadRecibidaEfectivo_'+iFPago).disabled=true;
    }
    else
    {
        gE('cantidadRecibidaEfectivo_'+iFPago).disabled=false;
    }
}

function generarPanelVentaApartado(iPago)
{
	var iFPago=parseInt(iPago)+10;
	var altoPanel=300;
	var panel=	 new Ext.Panel	(
									{
                                        x:5,
                                        y:70,
                                        id:'panel_'+iPago,
                                        layout:'absolute',
                                        width:780,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAbonoTarjeta_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
																	
                                                                    	 $('.btnEnter_'+iFPago).ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDatosOperacion(iFPago);
                                                                                                                     }
                                                                                                        )
                                                                                                                                                                                                             
                                                                                                     
                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:760,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Apartado con anticipo</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                    	x:0,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:380,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono tarjeta cr&eacute;dito/d&eacutebito</span>',
                                                                    	items:	[
                                                                        			 {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        
                                                                                        id:'txtCantidadAbonoTarjeta_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoTarjeta_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onkeyup="cantidadAbonoTarjetaChange(this,'+iFPago+')" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        
                                                                                        id:'lblNoAutorizacion1_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprobaci&oacute;n:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        
                                                                                        id:'txtNoAutorizacion1_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                     {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:85,
                                                                                        
                                                                                        id:'lblNoAutorizacion2_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:80,
                                                                                        
                                                                                        id:'txtNoAutorizacion2_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        
                                                                                        id:'lblDigitos_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:120,
                                                                                        id:'txtDigitosTarjeta_'+iFPago,
                                                                                        html:'<input disabled=true class="btnEvento btnEnter_'+iFPago+'" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_'+iFPago+'" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    }
                                                                        		]
                                                                    },
                                                                    {
                                                                    	x:390,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:340,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono en efectivo</span>',
                                                                    	items:	[
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        id:'txtCantidadAbonoEfectivo_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoEfectivo_'+iFPago+'" value="$ 0.00" onkeyup="cantidadAbonoEfectivoChange(this,'+iFPago+')" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        id:'lblCantidadRecibidaFectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        id:'txtCantidadRecibidaEfectivo_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        id:'lblEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:125,
                                                                                        id:'lblValorEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroEfectivo_'+iFPago+'">$ 0.00</span>'
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                        		]
                                                                      },
                                                                       {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:210,
                                                                        id:'lblMontoTotalAbono_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto total abono:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:220,
                                                                          y:210,
                                                                          id:'lblValorMontoAbono_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoAbonoTotal_'+iFPago+'">$ 0.00</span>'
                                                                      },
                                                                    {
                                                                        xtype:'label',
                                                                        x:390,
                                                                        y:210,
                                                                        id:'lblMontoRestante_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto Restante:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:600,
                                                                          y:210,
                                                                          id:'lblValorMontoRestante_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoRestanteTotal_'+iFPago+'">$ 0.00</span>'
                                                                      }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        
                    	var cantidadAbonoTarjeta=gE('cantidadAbonoTarjeta_'+iFPago);
                        
                        var montoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta.value));
                        
                        if(montoTarjeta>total)
                        {
                        	function resp10()
                            {
                                cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad recibida atrav&eacute;s de tarjeta es mayor que el total de la venta',resp10);
                            return false;
                        }
                        
                    	var noAutorizacion=gE('noAutorizacion1_'+iFPago);
                        var noAutorizacion2=gE('noAutorizacion2_'+iFPago);
                        var txtDigitosTarjeta=gE('digitosTarjeta_'+iFPago);
                        
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_'+iFPago);
                        
                        if(cantidadRecibidaEfectivo.value=='')
                        	cantidadRecibidaEfectivo.value='$ 0.00';
                        
                        if(parseFloat(normalizarValor(cantidadAbonoTarjeta.value))>0)
                        {
                           
                        
                            if(noAutorizacion.value=='')
                            {
                                function resp1()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                                return false;
                            }
                            if(noAutorizacion.value!=noAutorizacion2.value)
                            {
                                function resp2()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                                return false;
                            }
                            if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                            {
                                function resp3()
                                {
                                    txtDigitosTarjeta.focus();
                                }
                                msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                                return false;
                            }
                        }
                        
                       var cantidadAbonoEfectivo=gE('cantidadAbonoEfectivo_'+iFPago);
                       var montoCantidadAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo.value));
                       
                       if(montoCantidadAbonoEfectivo>0)
                       {
                       		var montoCantidadRecibidaEfectivo=parseFloat(normalizarValor(cantidadRecibidaEfectivo.value));
                            if(montoCantidadRecibidaEfectivo<montoCantidadAbonoEfectivo)
                            {
                            	function resp100()
                                {
                                	cantidadRecibidaEfectivo.focus();
                                }
                                msgBox('La cantidad recibida en efectivo es menor que la cantidad por abonar',resp100);
                                return;
                            
                            }
                       }
                       
                       
                       var totalAbono=parseFloat(normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML));
                       
                       if(totalAbono>total)
                       {
                       		function resp101()
                            {
                            	cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad a abonar excede el monto total de la venta',resp101);
                            return;
                       }
                       else
                       {
                       		if(totalAbono==0)
                            {
                                function resp102()
                                {
                                    cantidadAbonoTarjeta.focus();
                                }
                                msgBox('La cantidad a abonar no puede ser igual a $ 0.00',resp102);
                                return;
                            }
                       }
                       
                       
                       	var dCompra={};
                        dCompra.cadObj='{"montoAbono":"'+normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML)+'","pagoEfectivo":{"montoPagado":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'","cambio":"'+normalizarValor(gE('lblCambioCobroEfectivo_'+iFPago).innerHTML)+'"},"pagoTarjeta":{"montoPagado":"'+normalizarValor(cantidadAbonoTarjeta.value)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}}';
                        dCompra.desgloceFormaPago='[{"formaPago":"2","montoPago":"'+normalizarValor(cantidadAbonoTarjeta.value)+'"},{"formaPago":"1","montoPago":"'+montoCantidadAbonoEfectivo+'"}]';
                        
                        return dCompra;
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                            gE('cantidadAbonoTarjeta_'+iFPago).value="$ 0.00";
                        	gE('noAutorizacion1_'+iFPago).value="";
                            gE('noAutorizacion2_'+iFPago).value="";
                            gE('digitosTarjeta_'+iFPago).value="";
                            
                            gE('noAutorizacion1_'+iFPago).disabled=true;
                            gE('noAutorizacion2_'+iFPago).disabled=true;
                            gE('digitosTarjeta_'+iFPago).disabled=true;
                            
                            gE('cantidadAbonoEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).disabled=true;
                            
                            gE('lblCambioCobroEfectivo_'+iFPago).value="$ 0.00";
                            gE('lblMontoAbonoTotal_'+iFPago).value="$ 0.00";
                            gE('lblMontoRestanteTotal_'+iFPago).value="$ 0.00";
                        }    
                    
                                                    
	return panel;                                       
}

function generarPanelPedido(iPago)
{
	var iFPago=parseInt(iPago)+10;
	var altoPanel=300;
	var panel=	 new Ext.Panel	(
									{
                                        x:5,
                                        y:70,
                                        id:'panel_'+iPago,
                                        layout:'absolute',
                                        width:780,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAbonoTarjeta_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
																	
                                                                    	 $('.btnEnter_'+iFPago).ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDatosOperacion(iFPago);
                                                                                                                     }
                                                                                                        )
                                                                                                                                                                                                             
                                                                                                     
                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:760,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Registro de pedido</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                    	x:0,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:380,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono tarjeta cr&eacute;dito/d&eacutebito</span>',
                                                                    	items:	[
                                                                        			 {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        
                                                                                        id:'txtCantidadAbonoTarjeta_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoTarjeta_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onkeyup="cantidadAbonoTarjetaChange(this,'+iFPago+')" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        
                                                                                        id:'lblNoAutorizacion1_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprobaci&oacute;n:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        
                                                                                        id:'txtNoAutorizacion1_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                     {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:85,
                                                                                        
                                                                                        id:'lblNoAutorizacion2_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:80,
                                                                                        
                                                                                        id:'txtNoAutorizacion2_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:150px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        
                                                                                        id:'lblDigitos_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:120,
                                                                                        id:'txtDigitosTarjeta_'+iFPago,
                                                                                        html:'<input disabled=true class="btnEvento btnEnter_'+iFPago+'" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_'+iFPago+'" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                                    }
                                                                        		]
                                                                    },
                                                                    {
                                                                    	x:390,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:340,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono en efectivo</span>',
                                                                    	items:	[
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        id:'txtCantidadAbonoEfectivo_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoEfectivo_'+iFPago+'" value="$ 0.00" onkeyup="cantidadAbonoEfectivoChange(this,'+iFPago+')" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        id:'lblCantidadRecibidaFectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        id:'txtCantidadRecibidaEfectivo_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacion('+iFPago+')" />'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        id:'lblEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:125,
                                                                                        id:'lblValorEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroEfectivo_'+iFPago+'">$ 0.00</span>'
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                        		]
                                                                      },
                                                                       {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:210,
                                                                        id:'lblMontoTotalAbono_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto total abono:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:220,
                                                                          y:210,
                                                                          id:'lblValorMontoAbono_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoAbonoTotal_'+iFPago+'">$ 0.00</span>'
                                                                      },
                                                                    {
                                                                        xtype:'label',
                                                                        x:390,
                                                                        y:210,
                                                                        id:'lblMontoRestante_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto Restante:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:600,
                                                                          y:210,
                                                                          id:'lblValorMontoRestante_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoRestanteTotal_'+iFPago+'">$ 0.00</span>'
                                                                      }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        
                    	var cantidadAbonoTarjeta=gE('cantidadAbonoTarjeta_'+iFPago);
                        
                        var montoTarjeta=parseFloat(normalizarValor(cantidadAbonoTarjeta.value));
                        
                        if(montoTarjeta>total)
                        {
                        	function resp10()
                            {
                                cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad recibida atrav&eacute;s de tarjeta es mayor que el total de la venta',resp10);
                            return false;
                        }
                        
                    	var noAutorizacion=gE('noAutorizacion1_'+iFPago);
                        var noAutorizacion2=gE('noAutorizacion2_'+iFPago);
                        var txtDigitosTarjeta=gE('digitosTarjeta_'+iFPago);
                        
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_'+iFPago);
                        
                        if(cantidadRecibidaEfectivo.value=='')
                        	cantidadRecibidaEfectivo.value='$ 0.00';
                        
                        if(parseFloat(normalizarValor(cantidadAbonoTarjeta.value))>0)
                        {
                           
                        
                            if(noAutorizacion.value=='')
                            {
                                function resp1()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                                return false;
                            }
                            if(noAutorizacion.value!=noAutorizacion2.value)
                            {
                                function resp2()
                                {
                                    noAutorizacion.focus();
                                }
                                msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                                return false;
                            }
                            if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                            {
                                function resp3()
                                {
                                    txtDigitosTarjeta.focus();
                                }
                                msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                                return false;
                            }
                        }
                        
                       var cantidadAbonoEfectivo=gE('cantidadAbonoEfectivo_'+iFPago);
                       var montoCantidadAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo.value));
                       
                       if(montoCantidadAbonoEfectivo>0)
                       {
                       		var montoCantidadRecibidaEfectivo=parseFloat(normalizarValor(cantidadRecibidaEfectivo.value));
                            if(montoCantidadRecibidaEfectivo<montoCantidadAbonoEfectivo)
                            {
                            	function resp100()
                                {
                                	cantidadRecibidaEfectivo.focus();
                                }
                                msgBox('La cantidad recibida en efectivo es menor que la cantidad por abonar',resp100);
                                return;
                            
                            }
                       }
                       
                       
                       var totalAbono=parseFloat(normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML));
                       
                       if(totalAbono>total)
                       {
                       		function resp101()
                            {
                            	cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad a abonar excede el monto total de la venta',resp101);
                            return;
                       }
                       else
                       {
                       		if(totalAbono==0)
                            {
                                function resp102()
                                {
                                    cantidadAbonoTarjeta.focus();
                                }
                                msgBox('La cantidad a abonar no puede ser igual a $ 0.00',resp102);
                                return;
                            }
                       }
                       
                       
                       	var dCompra={};
                        dCompra.cadObj='{"montoAbono":"'+normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML)+'","pagoEfectivo":{"montoPagado":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'","cambio":"'+normalizarValor(gE('lblCambioCobroEfectivo_'+iFPago).innerHTML)+'"},"pagoTarjeta":{"montoPagado":"'+normalizarValor(cantidadAbonoTarjeta.value)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}}';
                        dCompra.desgloceFormaPago='[{"formaPago":"2","montoPago":"'+normalizarValor(cantidadAbonoTarjeta.value)+'"},{"formaPago":"1","montoPago":"'+montoCantidadAbonoEfectivo+'"}]';
                        
                        return dCompra;
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                            gE('cantidadAbonoTarjeta_'+iFPago).value="$ 0.00";
                        	gE('noAutorizacion1_'+iFPago).value="";
                            gE('noAutorizacion2_'+iFPago).value="";
                            gE('digitosTarjeta_'+iFPago).value="";
                            
                            gE('noAutorizacion1_'+iFPago).disabled=true;
                            gE('noAutorizacion2_'+iFPago).disabled=true;
                            gE('digitosTarjeta_'+iFPago).disabled=true;
                            
                            gE('cantidadAbonoEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).disabled=true;
                            
                            gE('lblCambioCobroEfectivo_'+iFPago).value="$ 0.00";
                            gE('lblMontoAbonoTotal_'+iFPago).value="$ 0.00";
                            gE('lblMontoRestanteTotal_'+iFPago).value="$ 0.00";
                        }    
                    

	panel.cerrarOperacionCaja=function(tipoOperacion,formaPago,cadObj,ventanaOperacion)
                            {
                            	
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        function respFinal()
                                        {
                                            imprimirTicketModuloDinamico(tipoOperacion,arrResp[1],formaPago);
                                            limpiarCaja();
                                            ignorarGlobal=false;
                                            ventanaOperacion.close();
                                            
                                            
                                        }
                                        msgBox('El pedido ha sido guardado',respFinal);            
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=9&tipoOperacion=2&idCaja='+gE('idCaja').value+'&cadObj='+cadObj,true);
                                
                                /*var arrProductos='';
                                var obj='';
                                var cadObj='';
                                var grid=gEx('grid');
                                var x;
                                var fila;
                                var totalDescuento=0;
                                var descuento=0;
                                for(x=0;x<grid.getStore().getCount();x++)
                                {
                                    fila=grid.getStore().getAt(x);
                                    descuento=(parseFloat(fila.data.descuento)*parseFloat(fila.get('cantidad')));
                                    obj='{"descuentoUnitario":"'+fila.data.descuento+'","descuento":"'+descuento+'","idRegistro":"'+fila.get('idRegistro')+'","cveProducto":"'+fila.get('cveProducto')+'","costoUnitario":"'+fila.get('costoUnitario')+'","cantidad":"'+fila.get('cantidad')+
                                        '","subtotal":"'+fila.get('subtotal')+'","iva":"'+fila.get('iva')+'","total":"'+fila.get('total')+'","tipoConcepto":"'+fila.get('tipoConcepto')+'","idProducto":"'+fila.get('idProducto')+
                                        '","dimensiones":"'+fila.get('dimensiones')+'","tipoMovimiento":"'+fila.get('tipoMovimiento')+'","porcentajeIVA":"'+fila.get('porcentajeIVA')+'","llave":"'+fila.get('llave')+'"}';
                                    if(arrProductos=='')
                                        arrProductos=obj;
                                    else
                                        arrProductos+=','+obj;
                                        
                                    totalDescuento+=descuento;      
                                }
                                
                                
                                cadObj='{"totalDescuento":"'+totalDescuento+'","idPedido":"'+idPedidoActivo+'","total":"'+normalizarValor(gE('lblTotal').innerHTML)+'","subtotal":"'+normalizarValor(gE('lblSubtotal').innerHTML)+'","iva":"'+normalizarValor(gE('lblIVA').innerHTML)+'","idCaja":"'+
                                        gE('idCaja').value+'","arrProductos":['+arrProductos+']}';
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        function respFinal()
                                        {
                                           	var arrParam=[['idTipoOperacion',2],['idOperacion',arrResp[1]],['idFormaPago',formaPago]];
                                            enviarFormularioDatos(moduloImpresionTicket,arrParam,'POST','iImprimir');
                                            limpiarCaja();
                                            ignorarGlobal=false;
                                            ventanaOperacion.close();
                                             
                                           
                                        }
                                        msgBox('El pedido ha sido guardado',respFinal);            
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=9&tipoOperacion=3&idCaja='+gE('idCaja').value+'&cadObj='+cadObj,true);*/
                                
                                
                                
                            }
                                                    
	return panel;                                       
}

function generarPanelTarjetaDebito(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('noAutorizacion_2').focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Tarjeta de Débito</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        
                                                                        id:'lblNoAutorizacion',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        
                                                                        id:'txtNoAutorizacion',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_2" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        
                                                                        id:'lblNoAutorizacion2',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        
                                                                        id:'txtNoAutorizacion2',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_22" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        
                                                                        id:'lblDigitos',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:95,
                                                                        
                                                                        id:'txtDigitosTarjeta',
                                                                        html:'<input class="btnEvento" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_2" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var noAutorizacion=gE('noAutorizacion_2');
                        var noAutorizacion2=gE('noAutorizacion_22');
                        var txtDigitosTarjeta=gE('digitosTarjeta_2');
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('noAutorizacion_2').value="";
                            gE('noAutorizacion_22').value="";
                            gE('digitosTarjeta_2').value="";
                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelTarjetaCredito(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('noAutorizacion').focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Tarjeta de Crédito</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        
                                                                        id:'lblNoAutorizacion',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        
                                                                        id:'txtNoAutorizacion',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        
                                                                        id:'lblNoAutorizacion2',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        
                                                                        id:'txtNoAutorizacion2',
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion2" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        
                                                                        id:'lblDigitos',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:95,
                                                                        
                                                                        id:'txtDigitosTarjeta',
                                                                        html:'<input class="btnEvento" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var noAutorizacion=gE('noAutorizacion');
                        var noAutorizacion2=gE('noAutorizacion2');
                        var txtDigitosTarjeta=gE('digitosTarjeta');
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('noAutorizacion').value="";
                            gE('noAutorizacion2').value="";
                            gE('digitosTarjeta').value="";
                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}


function generarPanelTransferencia(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('txtReferencia1_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Transferencia bancaria</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cve. Transacci&oacute;n:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:15,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir Cve. Transacci&oacute;n:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:55,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var txtReferencia1=gE('txtReferencia1_'+iFPago);
                        var txtReferencia2=gE('txtReferencia2_'+iFPago);

                    	if(txtReferencia1.value=='')
                        {
                            function resp1()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Debe ingresar la clave de transacci&oacute;n',resp1);
                            return false;
                        }
                        if(txtReferencia1.value!=txtReferencia2.value)
                        {
                            function resp2()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Las claves de transaci&oacute;n ingresadas NO coinciden',resp2);
                            return false;
                        }
                        
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"cveTransaccion":"'+txtReferencia1.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('txtReferencia1_'+iFPago).value="";
                            gE('txtReferencia2_'+iFPago).value="";

                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}


function generarPanelCheque(iFPago)
{
	var cmbBancos=crearComboExt('cmbBancos_'+iFPago,arrBancos,280,0,300);
	var altoPanel=230;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('txtReferencia1_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Cheque</span>',
                                                    	items:	[
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:0,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Banco:</span>'
                                                                    },
                                                                    
                                                                    cmbBancos,
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:40,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">N&uacute;m. de cheque:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:35,
                                                                        html:'<input type="text" class="btnEvento" style="width:130px; height:26px; font-size:18px; text-align:right" id="txtReferencia1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:80,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir N&uacute;m. de cheque:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:75,
                                                                        html:'<input type="text" class="btnEvento" style="width:130px; height:26px; font-size:18px; text-align:right" id="txtReferencia2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:120,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Fecha de cobro:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'datefield',
                                                                        x:280,
                                                                        y:120,
                                                                        id:'dteFechaCobro_'+iFPago,
                                                                        value:'<?php echo $fechaActual?>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var txtReferencia1=gE('txtReferencia1_'+iFPago);
                        var txtReferencia2=gE('txtReferencia2_'+iFPago);
                        
                        
                        var cmbBanco=gEx('cmbBancos_'+iFPago);
                        var fechaCobro=gEx('dteFechaCobro_'+iFPago);
                        

                    	if(txtReferencia1.value=='')
                        {
                            function resp1()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de cheque con el cual registro la transacci&oacute;n',resp1);
                            return false;
                        }
                        
                        if(txtReferencia1.value!=txtReferencia2.value)
                        {
                            function resp2()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Los n&uacute;meros de cheques ingresados NO coinciden',resp2);
                            return false;
                        }
                        
                        if(cmbBanco.getValue()=='')
                        {
                            function resp3()
                            {
                                cmbBanco.focus();
                            }
                            msgBox('Debe indicar el banco al cual pertenece el cheque',resp3);
                            return false;
                        }
                        
                        if(fechaCobro.getValue()=='')
                        {
                            function resp4()
                            {
                                fechaCobro.focus();
                            }
                            msgBox('Debe indicar la fecha de cobro del cheque',resp4);
                            return false;
                        }
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"montoAbono":"'+normalizarValor(gE('lblCobroTotal').innerHTML)+'","montoAdeudo":"'+total+'","noCheque":"'+txtReferencia1.value+'","idBanco":"'+cmbBanco.getValue()+'","fechaVencimiento":"'+fechaCobro.getValue().format("Y-m-d")+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'","cobrado":"0","fechaVencimiento":"'+fechaCobro.getValue().format("Y-m-d")+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('txtReferencia1_'+iFPago).value="";
                            gE('txtReferencia2_'+iFPago).value="";

                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelPagoReferenciado(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('txtReferencia1_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago referenciado</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">N&uacute;m. de referencia:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:15,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir N&uacute;m. de referencia:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:55,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var txtReferencia1=gE('txtReferencia1_'+iFPago);
                        var txtReferencia2=gE('txtReferencia2_'+iFPago);

                    	if(txtReferencia1.value=='')
                        {
                            function resp1()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de referencia con el cual registro la transacci&oacute;n',resp1);
                            return false;
                        }
                        if(txtReferencia1.value!=txtReferencia2.value)
                        {
                            function resp2()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Los n&uacute;meros de referencia ingresados NO coinciden',resp2);
                            return false;
                        }
                        
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"noCheque":"'+txtReferencia1.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('txtReferencia1_'+iFPago).value="";
                            gE('txtReferencia2_'+iFPago).value="";

                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}


function generarPanelPagoCombinado(iFPago)
{
	var arrPagoCombinado=[	['1','0',null,generarDetallePagoEfectivo,'','','','','','','',''],
    						['2','0',verificarNoAutorizacion,generarDetallePagoTarjeta,'No. aprobaci&oacute;n',' ','Rep. No. de<br>aprobaci&oacute;n','','Ult. 4 digitos','','',''],
                            ['3','0',verificarNoAutorizacion,generarDetallePagoTarjeta,'No. aprobaci&oacute;n',' ','Rep. No. de<br>aprobaci&oacute;n','','Ult. 4 digitos','','',''],
                            ['4','0',verificarClaveTransaccion,generarDetalleTransferencia,'Cve. Transacci&oacute;n',' ','Rep. Cve.<br>Transacci&oacute;n','','','','',''],
                            ['5','0',verificarNoCheque,generarDetalleCheque,'N&uacute;m. de cheque',' ','Rep. N&uacute;m.<br>de cheque','','','','',''],
                            ['6','0',verificarNoReferencia,generarDetallePagoReferenciado,'N&uacute;m. de referencia',' ','Rep. N&uacute;m.<br>de referencia','','','','','']
                          ];
	var altoPanel=350;
	var panel=	 new Ext.Panel	(
									{
                                        x:10,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:880,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        gEx('gGridMixto').getView().refresh();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:860,
                                                        height:altoPanel-20,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago combinado</span>',
                                                    	items:	[
                                                                    crearGridPagoCombinado(arrPagoCombinado),
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:260,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:260,
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblTotalCobroCombinado">$ 0.00</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:400,
                                                                        y:260,

                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:590,
                                                                        y:260,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroCombinado">$ 0.00</span>'
                                                                    }
                                                                   
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        var cantidadRecibidaEfectivo=normalizarValor(gE('lblTotalCobroCombinado').innerHTML);
                        if(cantidadRecibidaEfectivo=='')
                            cantidadRecibidaEfectivo.value=0;
                        var cantidadRec=parseFloat(normalizarValor(cantidadRecibidaEfectivo));
                        if(cantidadRec<total)
                        {
                            function resp()
                            {
                                
                            }
                            msgBox('La cantidad recibida no cubre el total de la venta',resp);
                            return false;
                        }
                        
                        var x;
                        var gGridMixto=gEx('gGridMixto');
                        var fila;
                        var arrDesgloce='';
                        var aux;
                        for(x=0;x<gGridMixto.getStore().getCount();x++)
                        {
                        	fila=gGridMixto.getStore().getAt(x);
                            if(parseFloat(fila.data.montoPago)>0)
                            {
                            	var continuar=true;
                            	if(fila.data.funcionValidacion!=null)
                                {
                                	continuar=fila.data.funcionValidacion(fila,x);
                                }
                            
                            	if(continuar)
                                {
                               		aux=fila.data.funcionDetallePago(fila);

                                    
                                    if(arrDesgloce=='') 	
                                    	arrDesgloce=aux;
                                    else
                                    	arrDesgloce+=','+aux;
                                
                                
                                }
                                else
                                	return false;
                            
                            
                            }
                            
                            
                        }
                        
                        
                        
                       
                       
                       var dCompra={};
                       dCompra.cadObj='{"cantidadRecibida":"'+normalizarValor(gE('lblTotalCobroCombinado').innerHTML)+'","cambio":"'+normalizarValor(gE('lblCambioCobroCombinado').innerHTML)+'","desgloceFormaPago":['+arrDesgloce+']}';
                       dCompra.desgloceFormaPago='['+arrDesgloce+']';
                       return dCompra;
                        
                        
                    }   
                    
                    
                    
	
    panel.alto=200;                    
    panel.ancho=20;                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel(panelFormaPagoActual);
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gEx('gGridMixto').getStore().loadData(arrPagoCombinado);

                            gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function crearGridPagoCombinado(arrPagoCombinado)
{

	var aFormaPago=<?php echo $arrPago?>;
	var dsDatos=arrPagoCombinado;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormaPago'},
                                                                    {name: 'montoPago'},
                                                                    {name: 'funcionValidacion'},
                                                                    {name: 'funcionDetallePago'},
                                                                    {name: 'etRef1'},
                                                                    {name: 'referencia1'},
                                                                    {name: 'etRef2'},
                                                                    {name: 'referencia2'},
                                                                    {name: 'etRef3'},
                                                                    {name: 'referencia3'},
                                                                    {name: 'etRef4'},
                                                                    {name: 'referencia4'}
                                                                    
                                                                    
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	{
															header:'Forma de pago',
															width:130,
															sortable:true,
															dataIndex:'idFormaPago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(aFormaPago,val);
                                                                    }
														},
                                                        {
															header:'Monto pago',
															width:80,
															sortable:true,
															dataIndex:'montoPago',
                                                            renderer:'usMoney',
                                                            editor:	{xtype:'numberfield'}
														},
														{
															header:'',
															width:120,
															sortable:true,
															dataIndex:'etRef1'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia1',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef2'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia2',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef3'
														},
                                                        {
															header:'',
															width:180,
															sortable:true,
															dataIndex:'referencia3',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gGridMixto',
                                                            store:alDatos,
                                                            frame:false,
                                                            y:0,
                                                            x:0,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:240,
                                                            width:820
                                                            
                                                        }
                                                    );
                                                    
	tblGrid.on('beforeedit',function(e)
    						{
                            	switch(e.field)
                                {
                                	case 'referencia1':
                                    	if(e.record.data.etRef1=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia2':
                                    	if(e.record.data.etRef2=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia3':
                                    	if(e.record.data.etRef3=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia4':
                                    	if(e.record.data.etRef4=='')
                                        	e.cancel=true;
                                    break;
                                }
                            }
    		) 
            
	tblGrid.on('afteredit',function(e)
    						{
                            	
                                switch(e.field)
                                {
                                	case 'montoPago':
                                    	var x;
                                        var total=0;
                                        var aux=0;
                                        var fila;
                                        for(x=0;x<e.grid.getStore().getCount();x++)
                                        {
                                        	fila=e.grid.getStore().getAt(x);
                                            aux=fila.data.montoPago;
                                            if(aux=='')
                                            	aux=0;
                                            aux=parseFloat(aux)    ;
                                            total+=aux;
                                        }
                                        gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(total);
                                        
                                        
                                        var montoTotal=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML))
                                        var diferencia=total-montoTotal;
                                        if(diferencia<0)
                                        	diferencia=0;
                                        gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(diferencia);
                                        
                                        
                                        
                                        
                                        
                                    break;
                                }
                            }
    		)             
                                                               
                                                    
	return 	tblGrid;
}

function verificarNoAutorizacion(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    var txtDigitosTarjeta=fila.data.referencia3.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
        return false;
    }
    
    if(noAutorizacion!=noAutorizacion2)
    {
    	
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
        return false;
    }
    if((txtDigitosTarjeta=='')||(txtDigitosTarjeta.length!=4))
    {
        function resp3()
        {
            gGridMixto.startEditing(noFila,7)  ;
        }
        msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
        return false;
    }	
    
    return true;
}

function verificarClaveTransaccion(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar la clave de la transaci&oacute;n',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Las claves de transacci&oacute;n ingresadas NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function verificarNoReferencia(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de referencia',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de referencia ingresados NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function verificarNoCheque(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de cheque',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de cheque ingresados NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function generarDetallePagoEfectivo(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'"}';	
    return cadObj;
}

function generarDetallePagoTarjeta(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noAutorizacion":"'+fila.data.referencia1.trim()+'","digitosTarjeta":"'+fila.data.referencia3.trim()+'"}';	
    return cadObj;
}

function generarDetalleTransferencia(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","cveTransaccion":"'+fila.data.referencia1.trim()+'"}';	
    return cadObj;
}

function generarDetalleCheque(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noCheque":"'+fila.data.referencia1.trim()+'"}';	
    return cadObj;
}

function generarDetallePagoReferenciado(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noEeferencia":"'+fila.data.referencia1.trim()+'"}';	
    return cadObj;
}

function generarPanelVentaCreditoV2(iPago)
{
	var iFPago=parseInt(iPago)+10;
	var altoPanel=300;
	var panel=	 new Ext.Panel	(
									{
                                        x:5,
                                        y:70,
                                        id:'panel_'+iPago,
                                        layout:'absolute',
                                        width:780,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAbonoEfectivo_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
																	
                                                                    	 $('.btnEnter_'+iFPago).ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDatosOperacion(iFPago);
                                                                                                                     }
                                                                                                        )
                                                                                                                                                                                                             
                                                                                                     
                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:760,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Venta a crédito</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                    	x:10,
                                                                        y:0,
                                                                    	xtype:'fieldset',
                                                                        layout:'absolute',
                                                                        width:720,
                                                                        height:170,
                                                                        title:'<span style="color:#000;font-size:12px; font-weight:bold">Abono en efectivo</span>',
                                                                    	items:	[
                                                                        			{
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:5,
                                                                                        id:'lblAbono_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Abono:</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:0,
                                                                                        id:'txtCantidadAbonoEfectivo_'+iFPago,
                                                                                        html:'<input type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadAbonoEfectivo_'+iFPago+'" value="$ 0.00" onkeyup="cantidadAbonoEfectivoChange(this,'+iFPago+')" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacionV2('+iFPago+')" />'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:45,
                                                                                        hidden:true,
                                                                                        id:'lblCantidadRecibidaFectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:40,
                                                                                        hidden:true,
                                                                                        id:'txtCantidadRecibidaEfectivo_'+iFPago,
                                                                                        html:'<input disabled=true type="text" class="btnEvento btnEnter_'+iFPago+'" style="width:110px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDatosOperacionV2('+iFPago+')" />'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:10,
                                                                                        y:125,
                                                                                        hidden:true,
                                                                                        id:'lblEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                                    },
                                                                                    
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:200,
                                                                                        y:125,
                                                                                        hidden:true,
                                                                                        id:'lblValorEtCambioEfectivo_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroEfectivo_'+iFPago+'">$ 0.00</span>'
                                                                                    },
                                                                                    {
                                                                                        xtype:'label',
                                                                                        x:335,
                                                                                        y:0,
                                                                                        id:'lblFechaCobro_'+iFPago,
                                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Fecha estimada de cobro:</span>'
                                                                                    },
                                                                                    {
                                                                                    	xtype:'datefield',
                                                                                        x:580,
                                                                                        y:0,
                                                                                        id:'dteFechaCobro_'+iFPago,
                                                                                        value:'<?php echo $fechaActual?>'
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                        		]
                                                                      },
                                                                       {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:210,
                                                                        id:'lblMontoTotalAbono_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto total abono:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:220,
                                                                          y:210,
                                                                          id:'lblValorMontoAbono_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoAbonoTotal_'+iFPago+'">$ 0.00</span>'
                                                                      },
                                                                    {
                                                                        xtype:'label',
                                                                        x:390,
                                                                        y:210,
                                                                        id:'lblMontoRestante_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto Restante:</span>'
                                                                    },
                                                                    {
                                                                          xtype:'label',
                                                                          x:600,
                                                                          y:210,
                                                                          id:'lblValorMontoRestante_'+iFPago,
                                                                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblMontoRestanteTotal_'+iFPago+'">$ 0.00</span>'
                                                                      }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        
                    	var montoTarjeta=0;
                        
                       
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_'+iFPago);
                        
                        if(cantidadRecibidaEfectivo.value=='')
                        	cantidadRecibidaEfectivo.value='$ 0.00';
                        
                        
                       var cantidadAbonoEfectivo=gE('cantidadAbonoEfectivo_'+iFPago);
                       var montoCantidadAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo.value));
                       
                       if(montoCantidadAbonoEfectivo>0)
                       {
                       		var montoCantidadRecibidaEfectivo=parseFloat(normalizarValor(cantidadRecibidaEfectivo.value));
                            /*if(montoCantidadRecibidaEfectivo<montoCantidadAbonoEfectivo)
                            {
                            	function resp100()
                                {
                                	cantidadRecibidaEfectivo.focus();
                                }
                                msgBox('La cantidad recibida en efectivo es menor que la cantidad por abonar',resp100);
                                return;
                            
                            }*/
                       }                       
                       
                       var totalAbono=parseFloat(normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML));
                       
                       if(totalAbono>total)
                       {
                       		function resp101()
                            {
                            	cantidadAbonoTarjeta.focus();
                            }
                            msgBox('La cantidad a abonar excede el monto total de la venta',resp101);
                            return;
                       }
                       else
                       {
                       		/*if(totalAbono==0)
                            {
                                function resp102()
                                {
                                    cantidadAbonoTarjeta.focus();
                                }
                                msgBox('La cantidad a abonar no puede ser igual a $ 0.00',resp102);
                                return;
                            }*/
                       }
                       
                       var dteFechaCobro=gEx('dteFechaCobro_'+iFPago);
                       if(dteFechaCobro.getValue()=='')
                       {
                       		function respFecha()
                            {
                            	dteFechaCobro.focus();
                            }
                            msgBox('Debe indicar la fecha estimada de cobro',respFecha);
                            return;
                       }
                       
                       
                       	var dCompra={};
                        dCompra.cadObj='{"fechaVencimiento":"'+gEx('dteFechaCobro_'+iFPago).getValue().format("Y-m-d")+'","montoAdeudo":"'+normalizarValor(gE('lblMontoRestanteTotal_'+iFPago).innerHTML)+'","montoAbono":"'+normalizarValor(gE('lblMontoAbonoTotal_'+iFPago).innerHTML)+'","pagoEfectivo":{"montoPagado":"'+normalizarValor(cantidadRecibidaEfectivo.value)+'","cambio":"'+normalizarValor(gE('lblCambioCobroEfectivo_'+iFPago).innerHTML)+'"}}';
                        dCompra.desgloceFormaPago='[{"formaPago":"1","montoPago":"'+montoCantidadAbonoEfectivo+'"}]';
                        
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                           
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                           
                            
                            gE('cantidadAbonoEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
                            gE('cantidadRecibidaEfectivo_'+iFPago).disabled=true;
                            
                            gE('lblCambioCobroEfectivo_'+iFPago).value="$ 0.00";
                            gE('lblMontoAbonoTotal_'+iFPago).value="$ 0.00";
                            gE('lblMontoRestanteTotal_'+iFPago).value="$ 0.00";
                        }    
                    
                                                    
	return panel;                                  
}

function calcularDatosOperacionV2(iFPago)
{
	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
   


	var cantidadAbonoEfectivo_3=gE('cantidadAbonoEfectivo_'+iFPago);
    if(cantidadAbonoEfectivo_3.value=='')
    	cantidadAbonoEfectivo_3.value='$ 0.00';
        
    
    var cantidadRecibidaEfectivo_3=gE('cantidadRecibidaEfectivo_'+iFPago);
    if(cantidadRecibidaEfectivo_3.value=='')
    	cantidadRecibidaEfectivo_3.value='$ 0.00';
    
    
    
    var totalAbonoEfectivo=parseFloat(normalizarValor(cantidadAbonoEfectivo_3.value));
    
        
    var cantidadRec=totalAbonoEfectivo;
    
    
    
    var diferencia=parseFloat(normalizarValor(cantidadRecibidaEfectivo_3.value))-totalAbonoEfectivo;
    if(diferencia<0)
    	diferencia=0;
    cambioUltimaVenta=diferencia;
    gE('lblCambioCobroEfectivo_'+iFPago).innerHTML=Ext.util.Format.usMoney(diferencia);		
    
    diferencia=total-cantidadRec;
    if(diferencia<0)
    	diferencia=0;

	gE('lblMontoAbonoTotal_'+iFPago).innerHTML=Ext.util.Format.usMoney(cantidadRec);		        
        
    gE('lblMontoRestanteTotal_'+iFPago).innerHTML=Ext.util.Format.usMoney(diferencia);		

}

function generarPanelPagoCombinadoV2(iFPago)
{
	var arrPagoCombinado=[	
    						['1','0',null,generarDetallePagoEfectivo,'','','','','','','',''],
    						['5','0',verificarNoChequeV2,generarDetalleChequeV2,'Banco','','N&uacute;m. de cheque','','Rep. N&uacute;m.<br>de cheque','','Fecha cobro','']
                            
                          ];
	var altoPanel=350;
	var panel=	 new Ext.Panel	(
									{
                                        x:10,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:880,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        gEx('gGridMixto').getView().refresh();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:860,
                                                        height:altoPanel-20,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago combinado</span>',
                                                    	items:	[
                                                                    crearGridPagoCombinadoV2(arrPagoCombinado,iFPago),
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:260,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:260,
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblTotalCobroCombinado">$ 0.00</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:400,
                                                                        y:260,

                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:590,
                                                                        y:260,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroCombinado">$ 0.00</span>'
                                                                    }
                                                                   
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        var cantidadRecibidaEfectivo=normalizarValor(gE('lblTotalCobroCombinado').innerHTML);
                        if(cantidadRecibidaEfectivo=='')
                            cantidadRecibidaEfectivo.value=0;
                        var cantidadRec=parseFloat(normalizarValor(cantidadRecibidaEfectivo));
                        if(cantidadRec<total)
                        {
                            function resp()
                            {
                                
                            }
                            msgBox('La cantidad recibida no cubre el total de la venta',resp);
                            return false;
                        }
                        
                        var x;
                        var gGridMixto=gEx('gGridMixto');
                        var fila;
                        var arrDesgloce='';
                        var aux;
                        var filaCheque=null;
                        for(x=0;x<gGridMixto.getStore().getCount();x++)
                        {
                        	fila=gGridMixto.getStore().getAt(x);
                            if(parseFloat(fila.data.montoPago)>0)
                            {
                            	var continuar=true;
                            	if(fila.data.funcionValidacion!=null)
                                {
                                	continuar=fila.data.funcionValidacion(fila,x);
                                }
                            
                            	if(continuar)
                                {
                               		aux=fila.data.funcionDetallePago(fila);
									
                                    console.log(fila);
                                    if((fila.data.idFormaPago=='5')&&(parseFloat(fila.data.montoPago)>0))
	                                    filaCheque=fila;
                                    
                                    if(arrDesgloce=='') 	
                                    	arrDesgloce=aux;
                                    else
                                    	arrDesgloce+=','+aux;
                                
                                
                                }
                                else
                                	return false;
                            
                            
                            }
                            
                            
                        }
                     
                       var fechaVencimiento='';
                       var montoAdeudo=normalizarValor(gE('lblCobroTotal').innerHTML);
                       if(!filaCheque)
                       {
                       		montoAdeudo=0;
                            
                       }
                       else
                       		fechaVencimiento=filaCheque.data.referencia4.format('Y-m-d');
                       
                       var dCompra={};
                       dCompra.cadObj='{"fechaVencimiento":"'+fechaVencimiento+'","montoAdeudo":"'+montoAdeudo+'","montoAbono":"1","cantidadRecibida":"'+normalizarValor(gE('lblTotalCobroCombinado').innerHTML)+'","cambio":"'+normalizarValor(gE('lblCambioCobroCombinado').innerHTML)+'","desgloceFormaPago":['+arrDesgloce+']}';
                       dCompra.desgloceFormaPago='['+arrDesgloce+']';
                       return dCompra;
                        
                        
                    }   
                    
                    
                    
	
    panel.alto=200;                    
    panel.ancho=20;                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel(panelFormaPagoActual);
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gEx('gGridMixto').getStore().loadData(arrPagoCombinado);

                            gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function crearGridPagoCombinadoV2(arrPagoCombinado,iFormaPago)
{

	var cmbBanco=crearComboExt('cmbBanco_'+iFormaPago,arrBancos);
	var aFormaPago=<?php echo $arrPago?>;
	var dsDatos=arrPagoCombinado;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormaPago'},
                                                                    {name: 'montoPago'},
                                                                    {name: 'funcionValidacion'},
                                                                    {name: 'funcionDetallePago'},
                                                                    {name: 'etRef1'},
                                                                    {name: 'referencia1'},
                                                                    {name: 'etRef2'},
                                                                    {name: 'referencia2'},
                                                                    {name: 'etRef3'},
                                                                    {name: 'referencia3'},
                                                                    {name: 'etRef4'},
                                                                    {name: 'referencia4'}
                                                                    
                                                                    
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	{
															header:'Forma de pago',
															width:130,
															sortable:true,
															dataIndex:'idFormaPago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(aFormaPago,val);
                                                                    }
														},
                                                        {
															header:'Monto pago',
															width:80,
															sortable:true,
															dataIndex:'montoPago',
                                                            renderer:'usMoney',
                                                            editor:	{xtype:'numberfield'}
														},
														{
															header:'',
															width:120,
															sortable:true,
															dataIndex:'etRef1'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia1',
                                                            editor:	cmbBanco,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrBancos,val);
                                                                    }
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef2'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia2',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef3'
														},
                                                        {
															header:'',
															width:180,
															sortable:true,
															dataIndex:'referencia3',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
                                                        {
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef4'
														},
                                                        {
															header:'',
															width:180,
															sortable:true,
															dataIndex:'referencia4',
                                                            editor:	{xtype:'datefield'},
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                    		return val.format('d/m/Y')
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gGridMixto',
                                                            store:alDatos,
                                                            frame:false,
                                                            y:0,
                                                            x:0,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:240,
                                                            width:820
                                                            
                                                        }
                                                    );
                                                    
	tblGrid.on('beforeedit',function(e)
    						{
                            	switch(e.field)
                                {
                                	case 'referencia1':
                                    	if(e.record.data.etRef1=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia2':
                                    	if(e.record.data.etRef2=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia3':
                                    	if(e.record.data.etRef3=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia4':
                                    	if(e.record.data.etRef4=='')
                                        	e.cancel=true;
                                    break;
                                }
                            }
    		) 
            
	tblGrid.on('afteredit',function(e)
    						{
                            	
                                switch(e.field)
                                {
                                	case 'montoPago':
                                    	var x;
                                        var total=0;
                                        var aux=0;
                                        var fila;
                                        for(x=0;x<e.grid.getStore().getCount();x++)
                                        {
                                        	fila=e.grid.getStore().getAt(x);
                                            aux=fila.data.montoPago;
                                            if(aux=='')
                                            	aux=0;
                                            aux=parseFloat(aux)    ;
                                            total+=aux;
                                        }
                                        gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(total);
                                        
                                        
                                        var montoTotal=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML))
                                        var diferencia=total-montoTotal;
                                        if(diferencia<0)
                                        	diferencia=0;
                                        gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(diferencia);
                                        
                                    break;
                                }
                            }
    		)             
                                                               
                                                    
	return 	tblGrid;
}


function generarDetalleChequeV2(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noCheque":"'+fila.data.referencia1.trim()+'","cobrado":"0","fechaVencimiento":"'+fila.data.referencia4.format('Y-m-d')+'"}';	
    return cadObj;
}


function verificarNoChequeV2(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia2.trim();
    var noAutorizacion2=fila.data.referencia3.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de cheque',resp1);
        return false;
    }
   
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de cheque ingresados NO coinciden',resp2);
        return false;
    }
    
    if(fila.data.referencia1=='')
    {
        function resp3()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe indicar el banco al cual pertenece el cheque',resp3);
        return false;
    }
    
    if(fila.data.referencia4=='')
    {
        function resp4()
        {
         	gGridMixto.startEditing(noFila,9)   
        }
        msgBox('Debe indicar la fecha a partir del cual se puede cobrar el cheque',resp4);
        return false;
    }
    
    return true;
}