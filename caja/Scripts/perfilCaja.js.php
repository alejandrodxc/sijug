<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");	

	$consulta="SELECT idFuncion,nombreFuncion FROM 9033_funcionesScriptsSistema WHERE idCategoria=2 ORDER BY nombreFuncion";
	$arrFuncionesAccion=$con->obtenerFilasArreglo($consulta);

	$consulta="SELECT idFuncion,nombreFuncion FROM 9033_funcionesScriptsSistema WHERE idCategoria=3 ORDER BY nombreFuncion";
	$arrFuncionesOD=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idCombinacion,etiqueta FROM 1020_combinacionesTeclas ORDER BY etiqueta";
	$arrCombinaciones=$con->obtenerFilasArreglo($consulta);
	
	$consultaSiNo="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consultaSiNo);
	
	
	$consulta="SELECT f.idFormaPago,formaPago FROM 600_formasPago f,601_aplicacionesFormaPago a WHERE a.idFormaPago=f.idFormaPago AND a.idTipoUso=4 ORDER BY formaPago";
	$arrFormaPago=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idFuncion,nombreFuncion FROM 9033_funcionesScriptsSistema WHERE idCategoria=4 ORDER BY nombreFuncion";
	$arrFuncionesScripts=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idTipoCliente,tipoCliente FROM 6941_tiposCliente  ORDER BY tipoCliente ";
	$arrTipoCliente=$con->obtenerFilasArreglo($consulta);
?>
var arrTipoCliente=<?php echo $arrTipoCliente?>;
var arrFuncionesScripts=<?php echo $arrFuncionesScripts?>;
var arrFormaPago=<?php echo $arrFormaPago?>;
var arrSiNo=<?php echo $arrSiNo?>;
var arrFuncionesAccion=<?php echo $arrFuncionesAccion?>;
var arrFuncionesOD=<?php echo $arrFuncionesOD?>;
var arrCombinaciones=<?php echo $arrCombinaciones?>;

var regFuncion=null;
Ext.onReady(inicializar);


function inicializar()
{
	var tabs = $( "#tabs" ).tabs(	
    								{
                                    	activate:function(event,ui)
                                        		{
                                                	
                                                	if(ui.newTab.context.innerHTML=='Funciones de búsqueda de producto')
                                                    {
                                                    	gEx('grid_1').getView().refresh();
                                                    }
                                                    
                                                    if(ui.newTab.context.innerHTML=='Formas de pago')
                                                    {
                                                    	gEx('grid_2').getView().refresh();
                                                    }
                                                    
                                                    if(ui.newTab.context.innerHTML=='Tipos de cliente')
                                                    {
                                                    	gEx('grid_3').getView().refresh();
                                                    }
                                                	
                                                }
                                    }
                                    
                                 );
    regFuncion=crearRegistro	(
    								[
                                    	{name: 'idFuncion'},
                                        {name: 'etiqueta'},
                                        {name: 'idFuncionOperacion'},
                                        {name: 'teclaAcceso'},
                                        {name: 'valorComplementario1'},
                                        {name: 'valorComplementario2'},
                                        {name: 'valorComplementario3'},
                                        {name: 'valorComplementario4'}
                                    ]
    							)
    crearGridConfiguracionFuncionBusqueda();
    crearGridConfiguracionFormaPago();
    crearGridTiposCliente();
    
    
    
        
    
}

var registroDimension;

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	if((gE('_idFuncionCatalogoProductoint').value=='')||(gE('_idFuncionCatalogoProductoint').value=='-1'))
        {
        	msgBox('Debe indicar la funci&oacute;n que ser&aacute; utilizado como origen de datos');
            return;
        }
        
        var grid_1=gEx('grid_1');
        var x;
        var fila;
        var arrFuncionBusqueda='';
        var obj='';
        for(x=0;x<grid_1.getStore().getCount();x++)
        {
        	fila=grid_1.getStore().getAt(x);
            obj='{"idFuncion":"'+fila.data.idFuncion+'","etiqueta":"'+fila.data.etiqueta+'","idFuncionOperacion":"'+fila.data.idFuncionOperacion+'","teclaAcceso":"'+
            		fila.data.teclaAcceso+'","valorComplementario1":"'+fila.data.valorComplementario1+'","valorComplementario2":"'+fila.data.valorComplementario2+'","default":"'+fila.data.default+'"}';
			if(arrFuncionBusqueda=='')
            	arrFuncionBusqueda=obj;
            else            
            	arrFuncionBusqueda+=','+obj;
            
        }
        
        
        var grid_2=gEx('grid_2');
        var x;
        var fila;
        var arrFuncionCierre='';
        var obj='';
        for(x=0;x<grid_2.getStore().getCount();x++)
        {
        	fila=grid_2.getStore().getAt(x);
            obj='{"idFuncion":"'+fila.data.idFuncion+'","etiqueta":"'+fila.data.etiqueta+'","idFuncionOperacion":"'+fila.data.idFormaPago+'","teclaAcceso":"'+fila.data.teclaAcceso+
            		'","valorComplementario1":"'+fila.data.funcionInvocacion+'","ordenAplicacion":"'+fila.data.ordenAplicacion+'"}';
			if(arrFuncionCierre=='')
            	arrFuncionCierre=obj;
            else            
            	arrFuncionCierre+=','+obj;
            
        }
        
       
        var arrTiposClientes='';
        var grid_3=gEx('grid_3');
        var x;
        var fila;
        
        var obj='';
        for(x=0;x<grid_3.getStore().getCount();x++)
        {
        	fila=grid_3.getStore().getAt(x);
            
            
            var ctAux=0;
            var cadFormaPago='';
            for(ctAux=0;ctAux<fila.data.formasPago.length;ctAux++)
            {
            	if(cadFormaPago=='')
                	cadFormaPago=fila.data.formasPago[ctAux];
                else
                	cadFormaPago+=','+fila.data.formasPago[ctAux];
            }
            
            obj='{"idTipoCliente":"'+fila.data.idTipoCliente+'","formasPago":"'+cadFormaPago+'"}';
			if(arrTiposClientes=='')
            	arrTiposClientes=obj;
            else            
            	arrTiposClientes+=','+obj;
            
        }
        var  arrFunciones;
        arrFunciones='{"arrTiposClientes":['+arrTiposClientes+'],"arrFuncionBusqueda":['+arrFuncionBusqueda+'],"arrFuncionCierre":['+arrFuncionCierre+']}';
        var id=gE('id').value;
        if(id=='-1')
        {
            gE('funcPHPEjecutarNuevo').value=bE('asociarFuncionesCaja(idRegPadre,\''+bE(arrFunciones)+'\',1)');
        }
        else
        {
            gE('funcPHPEjecutarModif').value=bE('asociarFuncionesCaja(idRegPadre,\''+bE(arrFunciones)+'\',0)');
        }
        
        var cEmpresas='';
        var aEmpresa='';
        var arrEmpresas=gEN('chkEmpresas');
        for(x=0;x<arrEmpresas.length;x++)
        {
        	if(arrEmpresas[x].checked)
            {
            	aEmpresa=arrEmpresas[x].id.split('_');
            	if(cEmpresas=='')
                	cEmpresas=aEmpresa[1];
                else
                	cEmpresas+=','+aEmpresa[1];
            }
        }
        gE('_empresasEmisorasComprobantesvch').value=cEmpresas;
    	gE('frmEnvio').submit();
    }
}

function crearGridConfiguracionFuncionBusqueda()
{
	var cmbCombinaciones=crearComboExt('cmbCombinaciones',arrCombinaciones);
	var cmbFuncionOrigen=crearComboExt('cmbFuncionOrigen',arrFuncionesOD);
    var cmbSiNo=crearComboExt('cmbSiNo',arrSiNo);
	var dsDatos=eval(bD(gE('arrFuncionBusqueda').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFuncion'},
                                                                    {name: 'etiqueta'},
                                                                    {name: 'idFuncionOperacion'},
                                                                    {name: 'teclaAcceso'},
                                                                    {name: 'valorComplementario1'},
	                                        						{name: 'valorComplementario2'},
                                                                    {name: 'default'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editorGrid_1',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
                                              
    editorFila.on('beforeedit',funcEditorFilaBeforeEditGrid)
    editorFila.on('validateedit',funcEditorValidaGrid);
    editorFila.on('canceledit',funcEditorCancelEditGrid);
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
														chkRow,
														{
															header:'Etiqueta <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
															dataIndex:'etiqueta',
                                                            editor: {
                                                            			xtype:'textfield'
                                                                        
                                                            		}
														},
														{
															header:'Funci&oacute;n de acci&oacute;n <span style="color:#F00">*</span>',
															width:220,
															sortable:true,
															dataIndex:'idFuncionOperacion',
                                                            editor:cmbFuncionOrigen,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFuncionesOD,val);
                                                                    }
														},
														{
															header:'Teclas de acci&oacute;n <span style="color:#F00">*</span>',
															width:120,
															sortable:true,
															dataIndex:'teclaAcceso',
                                                            editor:	cmbCombinaciones,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCombinaciones,val);
                                                                    }
														},
                                                        {
															header:'Etiqueta Campo',
															width:100,
															sortable:true,
															dataIndex:'valorComplementario1',
                                                            editor: {
                                                            			xtype:'textfield'
                                                                        
                                                            		}
														},
                                                        {
															header:'Valor comp',
															width:100,
															sortable:true,
															dataIndex:'valorComplementario2',
                                                            editor: {
                                                            			xtype:'textfield'
                                                                        
                                                            		}
														},
                                                        {
															header:'Default <span style="color:#F00">*</span>',
															width:80,
															sortable:true,
															dataIndex:'default',
                                                            editor:	cmbSiNo,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrSiNo,val);
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'grid_1',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            renderTo:'tblFuncionBusqueda',
                                                            cm: cModelo,
                                                            plugins:[editorFila],
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:880,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAdd_1',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar funci&oacute;n de b&uacute;squeda',
                                                                            handler:function()
                                                                            		{
                                                                                    	  var editorFila=gEx('editorGrid_1');
                                                                                           
                                                                                          
                                                                                          var nReg=new regFuncion	(
                                                                                                                    	{
                                                                                                                          	idFuncion:-1,
                                                                                                                            etiqueta:'',
                                                                                                                            idFuncionOperacion:'',
                                                                                                                            teclaAcceso:'',
                                                                                                                            valorComplementario1:'',
                                                                                                                            valorComplementario2:''
                                                                                                                        }
                                                                                                                     )
                                                                                          
                                                                                          editorFila.stopEditing();
                                                                                          tblGrid.getStore().add(nReg);
                                                                                          tblGrid.nuevoRegistro=true;
                                                                                          editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                          Ext.getCmp('btnAdd_1').disable();
                                                                                          Ext.getCmp('btnDel_1').disable();	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnDel_1',
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover funci&oacute;n de b&uacute;squeda',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                        var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                            msgBox('Primero debe seleccionar el elemento que desea eliminar');
                                                                                            return;	
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);	
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer eliminar el elemento seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	                                                 	
	return 	tblGrid;	

}

function funcAntesEditGrid(e)
{
	if((e.grid.soloLectura)&&(!e.grid.nuevoRegistro))
		e.cancel=true;
}

function funcEditorFilaBeforeEditGrid(rowEdit,fila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaGrid(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   	var c;
    
    for(x=0;x<nColumnas;x++)
    {
        c=cm.config[x];
        if((c.editor)&&(c.editor.attValor)&&(c.editor.attValor!=''))
        {
            dataIndex=cm.getDataIndex(x);
            eval('obj.'+dataIndex+'=obj.'+dataIndex+'+\'|'+c.editor.attValor+'\';')
        }
    }


    if(Ext.getCmp('btnDel_'+datosEditor[1])!=null)
        Ext.getCmp('btnDel_'+datosEditor[1]).enable();	
    if(Ext.getCmp('btnAdd_'+datosEditor[1])!=null)            
        Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
    grid.nuevoRegistro=false;
    return true;
   
   
	
}

function funcEditorCancelEditGrid(rowEdit,cancelado)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnDel_'+datosEditor[1]).enable();
    Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}

function crearGridConfiguracionFormaPago()
{
	var cmbCombinaciones=crearComboExt('cmbCombinaciones2',arrCombinaciones);
	var cmbFuncionOrigen=crearComboExt('cmbFuncionOrigen2',arrFormaPago);
    var cmbFuncionScripts=crearComboExt('cmbFuncionScripts',arrFuncionesScripts);
	var dsDatos=eval(bD(gE('arrFuncionCierre').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFuncion'},
                                                                    {name: 'etiqueta'},
                                                                    {name: 'idFormaPago'},
                                                                    {name: 'teclaAcceso'},
                                                                    {name: 'funcionInvocacion'},
                                                                    {name: 'ordenAplicacion',type:'int'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    
    var editorFila2=new Ext.ux.grid.RowEditor	(
    												{
														id:'editorGrid_2',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
                                              
    editorFila2.on('beforeedit',funcEditorFilaBeforeEditGrid2)
    editorFila2.on('validateedit',funcEditorValidaGrid2);
    editorFila2.on('canceledit',funcEditorCancelEditGrid2);

	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
														chkRow,
														{
															header:'Etiqueta <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
															dataIndex:'etiqueta',
                                                            editor: {
                                                            			xtype:'textfield'
                                                                        
                                                            		}
														},
														{
															header:'Forma de pago <span style="color:#F00">*</span>',
															width:170,
															sortable:true,
															dataIndex:'idFormaPago',
                                                            editor:cmbFuncionOrigen,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFormaPago,val);
                                                                    }
														},
														{
															header:'Teclas de acci&oacute;n <span style="color:#F00">*</span>',
															width:130,
															sortable:true,
															dataIndex:'teclaAcceso',
                                                            editor:	cmbCombinaciones,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCombinaciones,val);
                                                                    }
														},
														{
															header:'Funci&oacute;n de invocaci&oacute;n <span style="color:#F00">*</span>',
															width:200,
															sortable:true,
															dataIndex:'funcionInvocacion',
                                                            editor:	cmbFuncionScripts,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFuncionesScripts,val);
                                                                    }
														},
                                                        {
															header:'Prioridad',
															width:70,
															sortable:true,
															dataIndex:'ordenAplicacion'
                                                            
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'grid_2',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            renderTo:'tblFuncionCierre',
                                                            cm: cModelo,
                                                            plugins:[editorFila2],
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:850,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAdd_2',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar forma de pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	  var editorFila2=gEx('editorGrid_2');
                                                                                           
                                                                                          
                                                                                          var nReg=new regFuncion	(
                                                                                                                    	{
                                                                                                                          	idFuncion:-1,
                                                                                                                            etiqueta:'',
                                                                                                                            idFuncionOperacion:'',
                                                                                                                            teclaAcceso:'',
                                                                                                                            funcionInvocacion:'',
                                                                                                                            ordenAplicacion:tblGrid.getStore().getCount()+1
                                                                                                                        }
                                                                                                                     )
                                                                                          
                                                                                          editorFila2.stopEditing();
                                                                                          tblGrid.getStore().add(nReg);
                                                                                          tblGrid.nuevoRegistro=true;
                                                                                          editorFila2.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                          Ext.getCmp('btnAdd_2').disable();
                                                                                          Ext.getCmp('btnDel_2').disable();	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnDel_2',
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover forma de pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                        var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                            msgBox('Primero debe seleccionar el elemento que desea eliminar');
                                                                                            return;	
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);	
																								var x; 
                                                                                                
                                                                                                for(x=0;x<tblGrid.getStore().getCount();x++)                                                                                               
                                                                                                {
                                                                                                	fila=tblGrid.getStore().getAt(x);
                                                                                                    fila.set('ordenAplicacion',(x+1));
                                                                                                }
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer eliminar el elemento seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;<b>Modificar prioridad:&nbsp;&nbsp;</b>'
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignUp.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la forma de pago cuya prioridad desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.ordenAplicacion>1)
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.ordenAplicacion-2);
                                                                                            fila.data.ordenAplicacion-=1;
                                                                                            filaAux.data.ordenAplicacion+=1;
                                                                                            tblGrid.getStore().sort('ordenAplicacion','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignDown.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la forma de pago cuya prioridad desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.ordenAplicacion<tblGrid.getStore().getCount())
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.ordenAplicacion);
                                                                                            fila.data.ordenAplicacion+=1;
                                                                                            filaAux.data.ordenAplicacion-=1;
                                                                                            tblGrid.getStore().sort('ordenAplicacion','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	

}

function funcEditorFilaBeforeEditGrid2(rowEdit,fila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaGrid2(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   	var c;
    
    for(x=0;x<nColumnas;x++)
    {
        c=cm.config[x];
        if((c.editor)&&(c.editor.attValor)&&(c.editor.attValor!=''))
        {
            dataIndex=cm.getDataIndex(x);
            eval('obj.'+dataIndex+'=obj.'+dataIndex+'+\'|'+c.editor.attValor+'\';')
        }
    }


    if(Ext.getCmp('btnDel_'+idGrid)!=null)
        Ext.getCmp('btnDel_'+idGrid).enable();	
    if(Ext.getCmp('btnAdd_'+idGrid)!=null)            
        Ext.getCmp('btnAdd_'+idGrid).enable();
    grid.nuevoRegistro=false;
    return true;
   
   
	
}

function funcEditorCancelEditGrid2(rowEdit,cancelado)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnDel_'+datosEditor[1]).enable();
    Ext.getCmp('btnAdd_'+datosEditor[1]).enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
    }
	grid.nuevoRegistro=false;
}

function crearGridTiposCliente()
{
	
	var dsDatos=eval(bD(gE('arrTiposCliente').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idTipoCliente'},
                                                                    {name: 'formasPago'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Tipo de cliente',
															width:180,
															sortable:true,
															dataIndex:'idTipoCliente',
                                                            
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoCliente,val) ;
                                                                    }
														},
                                                        {
															header:'Forma de pago',
															width:470,
															sortable:true,
															dataIndex:'formasPago',
                                                            
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                        {
                                                                        	var cad='';
                                                                            var x;
                                                                            for(x=0;x<val.length;x++)
                                                                            {
                                                                            	if(cad=='')
                                                                                	cad=formatearValorRenderer(arrFormaPago,val[x]);
                                                                                else
                                                                                	cad+=', '+formatearValorRenderer(arrFormaPago,val[x]);
                                                                            }
                                                                            
                                                                            return mostrarValorDescripcion(cad);
                                                                        }
                                                                        
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'grid_3',
                                                            store:alDatos,
                                                            frame:false,
                                                            renderTo:'tblClientes',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:750,
                                                            sm:chkRow,
                                                            clicksToEdit:2,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar tipo de cliente',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarTipoCliente();
                                                                                    	
                                                                                    
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar tipo de cliente',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el tipo de cliente cuya configuraci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaAgregarTipoCliente(fila);
                                                                                    	
                                                                                    
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover tipo de cliente',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                            msgBox('Debe seleccionar el elemento que desea remover');
                                                                                            return;	
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);	
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
		                                        
	
}


function mostrarVentanaAgregarTipoCliente(fila)
{
	var gridFormaPago=crearGridFormaPago();
	var cmbTipoCliente=crearComboExt('cmbTipoCliente',arrTipoCliente,120,5,250);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Tipo de cliente:'
                                                        },
                                                        cmbTipoCliente,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Especifique las formas de pago disponibles para el tipo de cliente:'
                                                        },
                                                        gridFormaPago

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Tipo de cliente',
										width: 600,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbTipoCliente.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe especificar el tipo de cliente');
                                                                        	return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(gridFormaPago.getStore().getCount()==0)
                                                                        {
                                                                        	msgBox('Debe asociar el tipo de cliente con almenos una forma de pago');
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        var arrFormaPagoSel=[];
                                                                        var x;
                                                                        var f;
                                                                        for(x=0;x<gridFormaPago.getStore().getCount();x++)
                                                                        {
                                                                        	f=gridFormaPago.getStore().getAt(x);
                                                                            if(f.data.idFormaPago=='')
                                                                            {
                                                                            	function respAux()
                                                                                {
                                                                                	gridFormaPago.startEditing(x,2);
                                                                                }
                                                                                msgBox('Debe especificar la forma de pago a asociar',respAux);
                                                                            	return;
                                                                            }
                                                                            arrFormaPagoSel.push(f.data.idFormaPago);
                                                                        }
                                                                        
                                                                        if(fila)
                                                                        {
                                                                        	fila.set('idTipoCliente',cmbTipoCliente.getValue());
                                                                            fila.set('formasPago',arrFormaPagoSel);
                                                                            
                                                                       	}
                                                                        else
                                                                        {
                                                                        
                                                                        	var pos=obtenerPosFila(gEx('grid_3').getStore(),'idTipoCliente',cmbTipoCliente.getValue());
                                                                        	if(pos!=-1)
                                                                            {
                                                                            	msgBox('El tipo de cliente seleccionado ya ha sido agregado previamente');
                                                                                return;
                                                                            }
                                                                        	var reg=crearRegistro([{name:'idTipoCliente'},{name:'formasPago'}]);
                                                                            var r=new reg	(
                                                                                                {
                                                                                                    idTipoCliente:cmbTipoCliente.getValue(),
                                                                                                    formasPago:arrFormaPagoSel
                                                                                                }
                                                                                            )
                                                                            gEx('grid_3').getStore().add(r);
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
    
    if(fila)
    {
    	cmbTipoCliente.setValue(fila.data.idTipoCliente);
        gridFormaPago.getStore().loadData(fila.data.formasPago);
    }
    
}

function crearGridFormaPago()
{
	var cmbFormaPago=crearComboExt('cmbFormaPago',arrFormaPago);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormaPago'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Forma de pago',
															width:250,
															sortable:true,
															dataIndex:'idFormaPago',
                                                            editor:cmbFormaPago,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrFormaPago,val) ;
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gFormaPago',
                                                            store:alDatos,
                                                            frame:false,
                                                            x:10,
                                                            y:70,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:190,
                                                            width:550,
                                                            sm:chkRow,
                                                            clicksToEdit:2,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar forma de pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                    	var reg=crearRegistro([{name:'idFormaPago'}]);
                                                                                        var r=new reg	(
                                                                                        					{
                                                                                                            	idFormaPago:''
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                    	tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover forma de pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                            msgBox('Debe seleccionar la forma de pago que desea remover');
                                                                                            return;	
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);	
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la forma de pago seleccionada?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
		tblGrid.on('afteredit',function(e)
        						{
                                	if(e.field=='idFormaPago')
                                    {
                                    	var x;
                                        var pos=obtenerPosFila(e.grid.getStore(),'idFormaPago',e.value);
                                        if(pos!=e.row)
                                        {
                                        	function resp()
                                            {
                                            	e.record.set('idFormaPago',e.originalValue);
                                                e.grid.startEditing(e.row,e.column);
                                            }
                                            msgBox('La forma de pago seleccioanda ha sido agregada previamente',resp);
                                        }
                                    	
                                    }
                                	
                                }
        
        		)  
	return tblGrid;                
}