<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago";
	$arrPago=$con->obtenerFilasArreglo($consulta);
?>

function generarPanelEfectivoParcial(iFPago)
{
	var panel=	 new Ext.Panel	(
                                	{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:695,
                                        border:false,
                                        baseCls: 'x-plain',
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadRecibidaEfectivo_'+iFPago).focus();
                                                                        gE('cantidadRecibidaEfectivo_'+iFPago).select();
                                                                        
                                                                         $('#cantidadRecibidaEfectivo_'+iFPago).ClassyKeyboard('enter', function(event)
                                                                                                                     {
                                                                                                                           calcularDiferenciasEfectivo(gE('cantidadRecibidaEfectivo_'+iFPago));
                                                                                                                     }
                                                                                                        )
                                                                        
                                                                        
                                                                        registrarEventosCobroHTML();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {   
                                                                                                        	
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        
                                                                    }
                                                                }
                                                    },
                                        height:240,
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:675,
                                                        height:210,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago en efectivo</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        id:'lblCantidadRecibida_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        id:'txtCantidadRecibidaEfectivo_'+iFPago,
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadRecibidaEfectivo_'+iFPago+'" value="$ 0.00" onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasEfectivo(this,'+iFPago+')" />'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        id:'lblCantidadRecibidaPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad a pagar:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        id:'txtCantidadAPagarEfectivo_iFPago',
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAPagarEfectivo_'+iFPago+'"  onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasEfectivo(this,'+iFPago+')" />'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        id:'lblEtCambio',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:100,
                                                                        id:'lblValorEtCambio',
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobro">$ 0.00</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:140,
                                                                        id:'lblEtDiferencia',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Por pagar:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:140,
                                                                        id:'lblValorEtPorPagar',
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblPorPagar_'+iFPago+'">$ 0.00</span>'
                                                                    }
                                                            	]           
                                                    }    
                                                ]	
                                    }
								)
	                         
	panel.getDatosVenta=function()
    				{
                    	var total=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));
                        var cantidadRecibidaEfectivo=gE('cantidadRecibidaEfectivo_'+iFPago);
                        if(cantidadRecibidaEfectivo.value=='')
                            cantidadRecibidaEfectivo.value=0;
                        var cantidadRec=parseFloat(normalizarValor(gE('cantidadRecibidaEfectivo_'+iFPago).value));
                        if(cantidadRec<total)
                        {
                            function resp()
                            {
                                cantidadRecibidaEfectivo.focus();
                            }
                            msgBox('La cantidad recibida no cubre la cantidad a pagar',resp);
                            return false;
                        }
                        var lblCobroTotal=gE('lblCobroTotal');
                        var pCobroTotal=parseFloat(normalizarValor(lblCobroTotal.innerHTML));
                        var diferencia=pCobroTotal-total;
                        
                        
                        
                        if(diferencia<0)
                        {
                           	function resp2()
                            {
                                gE('cantidadAPagarEfectivo_'+iFPago).focus();
                            }
                            msgBox('La cantidad a pagar excede el monto total de la venta',resp2);
                            return false; 
                        }
                        
			            var dCompra={};
                        dCompra.cadObj='{"cantidadRecibida":"'+cantidadRecibidaEfectivo.value+'","cambio":"'+normalizarValor(gE('lblCambioCobro').innerHTML)+'","cantidadAPagar":"'+
                        				normalizarValor(total)+'","porPagar":"'+normalizarValor(gE('lblPorPagar_'+iFPago).innerHTML)+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                            gE('cantidadAPagarEfectivo_'+iFPago).value=gE('lblCobroTotal').innerHTML;
                            
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('cantidadRecibidaEfectivo_'+iFPago).value="$ 0.00";
                            gE('lblCambioCobro').innerHTML="$ 0.00";
                            
                        	
                        }                        


	panel.alto=50;                    
    panel.ancho=20;  
                                                                                        
	return panel;                                
}

function calcularDiferenciasEfectivo(ctrl,iF)
{
	var ctrl=gE('cantidadRecibidaEfectivo_'+iF);
    if(ctrl.value=='')
    	ctrl.value=0;
	ctrl.value=Ext.util.Format.usMoney(normalizarValor(ctrl.value));
    
        
    var aPagar=gE('cantidadAPagarEfectivo_'+iF);
    if(aPagar.value=='')
    	aPagar.value=0;
    aPagar.value=Ext.util.Format.usMoney(normalizarValor(aPagar.value));
    
	var total=parseFloat(normalizarValor(aPagar.value));
    
        
    var cantidadRec=parseFloat(normalizarValor(ctrl.value));
    var diferencia;
    
    diferencia=cantidadRec-total;
    if(diferencia<0)
    	diferencia=0;
    cambioUltimaVenta=diferencia;
    gE('lblCambioCobro').innerHTML=Ext.util.Format.usMoney(diferencia);	
    
    
    var totalAdeudo=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
    var diferencia=totalAdeudo-total;
    
    
    gE('lblPorPagar_'+iF).innerHTML=Ext.util.Format.usMoney(diferencia);	
    
}


///

function generarPanelTarjetaDebitoParcial(iFPago)
{
	var altoPanel=280;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:260,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAPagarEfectivo_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
																		gE('cantidadAPagarEfectivo_'+iFPago).select();				                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:240,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Tarjeta de Débito</span>',
                                                    	items:	[
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        id:'lblCantidadRecibidaPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad a pagar:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAPagarEfectivo_'+iFPago+'"  onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasTarjetaCreditoDebito(this,'+iFPago+')" />'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        
                                                                        
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        
                                                                       
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:95,
                                                                        
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:140,
                                                                        
                                                                        id:'lblDigitos',
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:135,
                                                                        html:'<input class="btnEvento" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_'+iFPago+'" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:180,
                                                                        id:'lblEtDiferencia_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Por pagar:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:180,
                                                                        id:'lblValorEtPorPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblPorPagar_'+iFPago+'">$ 0.00</span>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var noAutorizacion=gE('noAutorizacion_'+iFPago);
                        var noAutorizacion2=gE('noAutorizacion_2_'+iFPago);
                        var txtDigitosTarjeta=gE('digitosTarjeta_'+iFPago);
                        
                        
                        
                        var lblCobroTotal=gE('lblCobroTotal');
                        var pCobroTotal=parseFloat(normalizarValor(lblCobroTotal.innerHTML));
                        var total=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));
                        var diferencia=pCobroTotal-total;
                        
                        
                        
                        if(diferencia<0)
                        {
                           	function resp2()
                            {
                                gE('cantidadAPagarEfectivo_'+iFPago).focus();
                            }
                            msgBox('La cantidad a pagar excede el monto total de la venta',resp2);
                            return false; 
                        }
                        
                        
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        
                       	var dCompra={};
                        
                       
                        
                        dCompra.cadObj='{ "cantidadAPagar":"'+normalizarValor(total)+'","porPagar":"'+normalizarValor(gE('lblPorPagar_'+iFPago).innerHTML)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                            gE('cantidadAPagarEfectivo_'+iFPago).value=gE('lblCobroTotal').innerHTML;
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('noAutorizacion_'+iFPago).value="";
                            gE('noAutorizacion_2_'+iFPago).value="";
                            gE('digitosTarjeta_'+iFPago).value="";
                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function calcularDiferenciasTarjetaCreditoDebito(ctrl,iF)
{
	var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
    gE('cantidadAPagarEfectivo_'+iF).value=Ext.util.Format.usMoney(gE('cantidadAPagarEfectivo_'+iF).value);
    var totalPagar=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iF).value));
    var diferencia=total-totalPagar;
    gE('lblPorPagar_'+iF).innerHTML=Ext.util.Format.usMoney(diferencia);
    
    
    
}

function generarPanelTarjetaCreditoParcial(iFPago)
{
	var altoPanel=260;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:280,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('cantidadAPagarEfectivo_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
																		gE('cantidadAPagarEfectivo_'+iFPago).select();		                                                                                                     
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:240,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Tarjeta de Crédito</span>',
                                                    	items:	[
                                                                 	{
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        id:'lblCantidadRecibidaPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad a pagar:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        id:'txtCantidadAPagarEfectivo_'+iFPago,
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAPagarEfectivo_'+iFPago+'"  onkeypress="return soloNumero(event,true,false,this)" onblur="calcularDiferenciasTarjetaCreditoDebito(this,'+iFPago+')" />'
                                                                    },   
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:55,
                                                                        

                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:100,
                                                                        

                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir No. Aprob.:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:95,
                                                                        
                                                                        html:'<input type="text" class="btnEvento" style="width:170px; height:26px; font-size:18px; text-align:right" id="noAutorizacion_2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:140,
                                                                        
                                                                        
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">&Uacute;lt. 4 digitos tarjeta:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:135,
                                                                        
                                                                        
                                                                        html:'<input class="btnEvento" type="text" style="width:70px; height:26px; font-size:18px; text-align:right" id="digitosTarjeta_'+iFPago+'" value="" maxlength="4" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:180,
                                                                        id:'lblEtDiferencia_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Por pagar:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:180,
                                                                        id:'lblValorEtPorPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblPorPagar_'+iFPago+'">$ 0.00</span>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var noAutorizacion=gE('noAutorizacion_'+iFPago);
                        var noAutorizacion2=gE('noAutorizacion_2_'+iFPago);
                        var txtDigitosTarjeta=gE('digitosTarjeta_'+iFPago);
                        
                        
                        
                        var lblCobroTotal=gE('lblCobroTotal');
                        var pCobroTotal=parseFloat(normalizarValor(lblCobroTotal.innerHTML));
                        var total=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));
                        var diferencia=pCobroTotal-total;
                        
                        
                        
                        if(diferencia<0)
                        {
                           	function resp2()
                            {
                                gE('cantidadAPagarEfectivo_'+iFPago).focus();
                            }
                            msgBox('La cantidad a pagar excede el monto total de la venta',resp2);
                            return false; 
                        }
                        
                    	if(noAutorizacion.value=='')
                        {
                            function resp1()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
                            return false;
                        }
                        if(noAutorizacion.value!=noAutorizacion2.value)
                        {
                            function resp2()
                            {
                                noAutorizacion.focus();
                            }
                            msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
                            return false;
                        }
                        if((txtDigitosTarjeta.value=='')||(txtDigitosTarjeta.value.length!=4))
                        {
                            function resp3()
                            {
                                txtDigitosTarjeta.focus();
                            }
                            msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
                            return false;
                        }
                        
                        
                       	var dCompra={};
                        dCompra.cadObj='{ "cantidadAPagar":"'+normalizarValor(total)+'","porPagar":"'+normalizarValor(gE('lblPorPagar_'+iFPago).innerHTML)+'","noAutorizacion":"'+noAutorizacion.value+'","digitosTarjeta":"'+txtDigitosTarjeta.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                            gE('cantidadAPagarEfectivo_'+iFPago).value=gE('lblCobroTotal').innerHTML;
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('noAutorizacion_'+iFPago).value="";
                            gE('noAutorizacion_2_'+iFPago).value="";
                            gE('digitosTarjeta_'+iFPago).value="";
                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelTransferenciaParcial(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('txtReferencia1_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Transferencia bancaria</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cve. Transacci&oacute;n:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:15,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir Cve. Transacci&oacute;n:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:55,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var txtReferencia1=gE('txtReferencia1_'+iFPago);
                        var txtReferencia2=gE('txtReferencia2_'+iFPago);

                    	if(txtReferencia1.value=='')
                        {
                            function resp1()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Debe ingresar la clave de transacci&oacute;n',resp1);
                            return false;
                        }
                        if(txtReferencia1.value!=txtReferencia2.value)
                        {
                            function resp2()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Las claves de transaci&oacute;n ingresadas NO coinciden',resp2);
                            return false;
                        }
                        
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"cveTransaccion":"'+txtReferencia1.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('txtReferencia1_'+iFPago).value="";
                            gE('txtReferencia2_'+iFPago).value="";

                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelChequeParcial(iFPago)
{
	var altoPanel=200;
	var panel=	 new Ext.Panel	(
									{
                                        x:50,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:650,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:240,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	gE('txtReferencia1_'+iFPago).focus();
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                        registrarEventosCobroHTML();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:630,
                                                        height:altoPanel-30,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Cheque</span>',
                                                    	items:	[
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">N&uacute;m. de cheque:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:15,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia1_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:60,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Repetir N&uacute;m. de cheque:</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:280,
                                                                        y:55,
                                                                        html:'<input type="text" class="btnEvento" style="width:230px; height:26px; font-size:18px; text-align:right" id="txtReferencia2_'+iFPago+'" value="" onkeypress="return soloNumero(event,false,false,this)"/>'
                                                                    }
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	var txtReferencia1=gE('txtReferencia1_'+iFPago);
                        var txtReferencia2=gE('txtReferencia2_'+iFPago);

                    	if(txtReferencia1.value=='')
                        {
                            function resp1()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Debe ingresar el n&uacute;mero de cheque con el cual registro la transacci&oacute;n',resp1);
                            return false;
                        }
                        if(txtReferencia1.value!=txtReferencia2.value)
                        {
                            function resp2()
                            {
                                txtReferencia1.focus();
                            }
                            msgBox('Los n&uacute;meros de cheques ingresados NO coinciden',resp2);
                            return false;
                        }
                        
                        
                        var total=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                       	var dCompra={};
                        dCompra.cadObj='{"noCheque":"'+txtReferencia1.value+'"}';
                        dCompra.desgloceFormaPago='[{"formaPago":"'+iFPago+'","montoPago":"'+total+'"}]';
                        return dCompra;
                        
                        
                    }   
                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel();
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gE('txtReferencia1_'+iFPago).value="";
                            gE('txtReferencia2_'+iFPago).value="";

                            
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function generarPanelPagoCombinadoParcial(iFPago)
{
	var arrPagoCombinado=[	['1','0',null,generarDetallePagoEfectivo,'','','','','','','',''],
    						['2','0',verificarNoAutorizacion,generarDetallePagoTarjeta,'No. aprobaci&oacute;n',' ','Rep. No. de<br>aprobaci&oacute;n','','Ult. 4 digitos','','',''],
                            ['3','0',verificarNoAutorizacion,generarDetallePagoTarjeta,'No. aprobaci&oacute;n',' ','Rep. No. de<br>aprobaci&oacute;n','','Ult. 4 digitos','','','']
//                            ['4','0',verificarClaveTransaccion,generarDetalleTransferencia,'Cve. Transacci&oacute;n',' ','Rep. Cve.<br>Transacci&oacute;n','','','','',''],
//                            ['5','0',verificarNoCheque,generarDetalleCheque,'N&uacute;m. de cheque',' ','Rep. N&uacute;m.<br>de cheque','','','','','']
                            
                          ];
	var altoPanel=350;
	var panel=	 new Ext.Panel	(
									{
                                        x:10,
                                        y:70,
                                        id:'panel_'+iFPago,
                                        layout:'absolute',
                                        width:880,
                                        border:false,
                                        baseCls: 'x-plain',
                                        height:altoPanel,
                                        listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    	
                                                                        $("input[type=text]").focus(	function()
                                                                                                        {    
                                                                                                            this.select();
                                                                                                         }
                                                                                                     );
                                                                                                     
                                                                                                     
                                                                        gE('cantidadAPagarEfectivo_'+iFPago).focus();                             
                                                                        registrarEventosCobroHTML();
                                                                        gEx('gGridMixto').getView().refresh();
                                                                        
                                                                    }
                                                                }
                                                    },
                                        items:	[
                                                    
                                                    {
                                                    	xtype:'fieldset',
                                                        width:860,
                                                        height:altoPanel-20,
                                                        layout:'absolute',
                                                        title:'<span style="color:#900;font-size:16px; font-weight:bold">Pago combinado</span>',
                                                    	items:	[
                                                        			{
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:20,
                                                                        id:'lblCantidadRecibidaPagar_'+iFPago,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad a pagar:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:200,
                                                                        y:15,
                                                                        id:'txtCantidadAPagarEfectivo_iFPago',
                                                                        html:'<input type="text" class="btnEvento" style="width:150px; height:26px; font-size:18px; text-align:right" id="cantidadAPagarEfectivo_'+iFPago+'"  onkeypress="return soloNumero(event,true,false,this)" onblur="calcularTotalPagoCombinado('+iFPago+')" />'
                                                                    },
                                                                    crearGridPagoCombinadoParcial(arrPagoCombinado,iFPago),
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:280,
                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cantidad Recibida:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:220,
                                                                        y:280,
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblTotalCobroCombinado">$ 0.00</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:350,
                                                                        y:280,

                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Cambio:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:460,
                                                                        y:280,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCambioCobroCombinado">$ 0.00</span>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:590,
                                                                        y:280,

                                                                        html:'<span style="font-size:18px; color:#000; font-weight:bold">Por pagar:</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        xtype:'label',
                                                                        x:700,
                                                                        y:280,
                                                                        
                                                                        html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblPorPagar_'+iFPago+'">$ 0.00</span>'
                                                                    }
                                                                   
                                                				]
                                                    }
                                                ]
                                    }
								)                                    
                                
	panel.getDatosVenta=function()
    				{
                    	
                        var total=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));
                        var cantidadRecibidaEfectivo=normalizarValor(gE('lblTotalCobroCombinado').innerHTML);
                        if(cantidadRecibidaEfectivo=='')
                            cantidadRecibidaEfectivo.value=0;
                        var cantidadRec=parseFloat(normalizarValor(cantidadRecibidaEfectivo));
                        if(cantidadRec<total)
                        {
                            function resp()
                            {
                                gE('cantidadAPagarEfectivo_'+iFPago).focus();
                            }
                            msgBox('La cantidad recibida no cubre la cantidad total a pagar',resp);
                            return false;
                        }
                        
                        var ventaTotal=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML));
                        if(ventaTotal<total)
                        {
                            function resp2()
                            {
                                gE('cantidadAPagarEfectivo_'+iFPago).focus();
                            }
                            msgBox('La cantidad recibida excede el monto total de la venta',resp2);
                            return false;
                        }
                        
                        var x;
                        var gGridMixto=gEx('gGridMixto');
                        var fila;
                        var arrDesgloce='';
                        var aux;
                        for(x=0;x<gGridMixto.getStore().getCount();x++)
                        {
                        	fila=gGridMixto.getStore().getAt(x);
                            if(parseFloat(fila.data.montoPago)>0)
                            {
                            	var continuar=true;
                            	if(fila.data.funcionValidacion!=null)
                                {
                                	continuar=fila.data.funcionValidacion(fila,x);
                                }
                            
                            	if(continuar)
                                {
                               		aux=fila.data.funcionDetallePago(fila);

                                    
                                    if(arrDesgloce=='') 	
                                    	arrDesgloce=aux;
                                    else
                                    	arrDesgloce+=','+aux;
                                
                                
                                }
                                else
                                	return false;
                            
                            
                            }
                            
                            
                        }
                        
                        
                        
                       
                       
                       var dCompra={};
                       dCompra.cadObj='{"cantidadAPagar":"'+normalizarValor(total)+'","porPagar":"'+normalizarValor(gE('lblPorPagar_'+iFPago).innerHTML)+'","cantidadRecibida":"'+normalizarValor(gE('lblTotalCobroCombinado').innerHTML)+'","cambio":"'+normalizarValor(gE('lblCambioCobroCombinado').innerHTML)+'","desgloceFormaPago":['+arrDesgloce+']}';
                       dCompra.desgloceFormaPago='['+arrDesgloce+']';
                       return dCompra;
                        
                        
                    }   
                    
                    
                    
	
    panel.alto=200;                    
    panel.ancho=20;                    
                    
	panel.mostrarPanel=function()
    					{
                            panelFormaPagoActual=this;
                        	this.show();
                            ajustarVentanaPanel(panelFormaPagoActual);
                            gE('cantidadAPagarEfectivo_'+iFPago).value=gE('lblCobroTotal').innerHTML;
                        }  
                        
	panel.ocultarPanel=function()
    					{
                        	this.hide();
                        	gEx('gGridMixto').getStore().loadData(arrPagoCombinado);

                            gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(0);
                            
                        	
                        }    
                    
                                                    
	return panel;                                  
}

function crearGridPagoCombinadoParcial(arrPagoCombinado,iFPago)
{

	var aFormaPago=<?php echo $arrPago?>;
	var dsDatos=arrPagoCombinado;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormaPago'},
                                                                    {name: 'montoPago'},
                                                                    {name: 'funcionValidacion'},
                                                                    {name: 'funcionDetallePago'},
                                                                    {name: 'etRef1'},
                                                                    {name: 'referencia1'},
                                                                    {name: 'etRef2'},
                                                                    {name: 'referencia2'},
                                                                    {name: 'etRef3'},
                                                                    {name: 'referencia3'},
                                                                    {name: 'etRef4'},
                                                                    {name: 'referencia4'}
                                                                    
                                                                    
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	{
															header:'Forma de pago',
															width:130,
															sortable:true,
															dataIndex:'idFormaPago',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(aFormaPago,val);
                                                                    }
														},
                                                        {
															header:'Monto pago',
															width:80,
															sortable:true,
															dataIndex:'montoPago',
                                                            renderer:'usMoney',
                                                            editor:	{xtype:'numberfield'}
														},
														{
															header:'',
															width:120,
															sortable:true,
															dataIndex:'etRef1'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia1',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef2'
														},
                                                        {
															header:'',
															width:130,
															sortable:true,
															dataIndex:'referencia2',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														},
														{
															header:'',
															width:90,
															sortable:true,
															dataIndex:'etRef3'
														},
                                                        {
															header:'',
															width:180,
															sortable:true,
															dataIndex:'referencia3',
                                                            editor:	{xtype:'textfield',maskRe:/^[0-9]$/,enableKeyEvents:true}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gGridMixto',
                                                            store:alDatos,
                                                            frame:false,
                                                            y:70,
                                                            x:0,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:190,
                                                            width:820
                                                            
                                                        }
                                                    );
                                                    
	tblGrid.on('beforeedit',function(e)
    						{
                            	switch(e.field)
                                {
                                	case 'referencia1':
                                    	if(e.record.data.etRef1=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia2':
                                    	if(e.record.data.etRef2=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia3':
                                    	if(e.record.data.etRef3=='')
                                        	e.cancel=true;
                                    break;
                                    case 'referencia4':
                                    	if(e.record.data.etRef4=='')
                                        	e.cancel=true;
                                    break;
                                }
                            }
    		) 
            
	tblGrid.on('afteredit',function(e)
    						{
                            	
                                switch(e.field)
                                {
                                	case 'montoPago':
                                    	calcularTotalPagoCombinado(iFPago);
                                        
                                        
                                        
                                        
                                        
                                    break;
                                }
                            }
    		)             
                                                               
                                                    
	return 	tblGrid;
}


function calcularTotalPagoCombinado(iFPago)
{
	var x;
    var total=0;
    var aux=0;
    var fila;
    var grid=gEx('gGridMixto');
    for(x=0;x<grid.getStore().getCount();x++)
    {
        fila=grid.getStore().getAt(x);
        aux=fila.data.montoPago;
        if(aux=='')
            aux=0;
        aux=parseFloat(aux)    ;
        total+=aux;
    }
    gE('lblTotalCobroCombinado').innerHTML=Ext.util.Format.usMoney(total);
    gE('cantidadAPagarEfectivo_'+iFPago).value=Ext.util.Format.usMoney(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));
    var montoTotal=parseFloat(normalizarValor(gE('cantidadAPagarEfectivo_'+iFPago).value));

    var diferencia=total-montoTotal;
    if(diferencia<0)
        diferencia=0;
    gE('lblCambioCobroCombinado').innerHTML=Ext.util.Format.usMoney(diferencia);
    diferencia=parseFloat(normalizarValor(gE('lblCobroTotal').innerHTML))-montoTotal;
    gE('lblPorPagar_'+iFPago).innerHTML=Ext.util.Format.usMoney(diferencia);
    
    
}

function verificarNoAutorizacion(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    var txtDigitosTarjeta=fila.data.referencia3.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n',resp1);
        return false;
    }
    
    if(noAutorizacion!=noAutorizacion2)
    {
    	
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de aprobaci&oacute;n ingresados NO coinciden',resp2);
        return false;
    }
    if((txtDigitosTarjeta=='')||(txtDigitosTarjeta.length!=4))
    {
        function resp3()
        {
            gGridMixto.startEditing(noFila,7)  ;
        }
        msgBox('El valor de los &uacute;ltimos 4 d&iacute;gito de la tarjeta no es v&aacute;lido',resp3);
        return false;
    }	
    
    return true;
}

function verificarClaveTransaccion(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar la clave de la transaci&oacute;n',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Las claves de transacci&oacute;n ingresadas NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function verificarNoReferencia(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de referencia',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de referencia ingresados NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function verificarNoCheque(fila,noFila)
{
	var gGridMixto=gEx('gGridMixto');
	var noAutorizacion=fila.data.referencia1.trim();
    var noAutorizacion2=fila.data.referencia2.trim();
    
    if((noAutorizacion=='')||(noAutorizacion==0))
    {
        function resp1()
        {
         	gGridMixto.startEditing(noFila,3)   
        }
        msgBox('Debe ingresar el n&uacute;mero de cheque',resp1);
        return false;
    }
    if(noAutorizacion!=noAutorizacion2)
    {
        function resp2()
        {
            gGridMixto.startEditing(noFila,5)   
        }
        msgBox('Los n&uacute;meros de cheque ingresados NO coinciden',resp2);
        return false;
    }
    	
    
    return true;
}

function generarDetallePagoEfectivo(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'"}';	
    return cadObj;
}

function generarDetallePagoTarjeta(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noAutorizacion":"'+fila.data.referencia1.trim()+'","digitosTarjeta":"'+fila.data.referencia3.trim()+'"}';	
    return cadObj;
}

function generarDetalleTransferencia(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","cveTransaccion":"'+fila.data.referencia1.trim()+'"}';	
    return cadObj;
}

function generarDetalleCheque(fila)
{
	var cadObj='{"formaPago":"'+fila.data.idFormaPago+'","montoPago":"'+fila.data.montoPago+'","noCheque":"'+fila.data.referencia1.trim()+'"}';	
    return cadObj;
}

