<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


var tipoCliente=1;
Ext.onReady(inicializar);

function inicializar()
{
	
    
    var oConf=	{
    					idCombo:'cmbCliente',
                        posX:570,
                        posY:45,
                        anchoCombo:350,
                        campoDesplegar:'nombreCliente',
                        campoID:'idCliente',
                        campoHDestino:'idCliente',
                        funcionBusqueda:104,
                        paginaProcesamiento:'../paginasFunciones/funcionesContabilidad.php',
                        confVista:'<tpl for="."><div class="search-item"><b>{nombreCliente}</b></div></tpl>',
                        campos:	[
                                    {name:'idCliente'},
                                    {name:'nombreCliente'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	
                                    	gE('idCliente').value='-1';
                                    	var aValor=combo.getRawValue();
										dSet.baseParams.criterio=aValor;
                                        dSet.baseParams.tipoCliente=tipoCliente;
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gEx('grid').getStore().removeAll();
                                    	gE('idCliente').value=registro.get('idCliente');
                                        buscarAdeudosCliente();
                                        calcularTotal();
                                    	
                                    }  
    				};
    var cmbCliente=crearComboExtAutocompletar(oConf);
    
	var gridCaja=crearGridCaja();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            title:'<span class="letraRojaSubrayada8" style="font-size:14px">Caja 1</span>&nbsp;&nbsp;<span style="font-size:14px; color:#000">[Usuario Rool Gral.]</span>',
                                            items:	[
                                           				gridCaja,
                                                        {
                                                        	region:'south',
                                                        	xtype:'panel',
                                                            height:250,
                                                            layout:'absolute',
                                                           
                                                            border:true,
                                                            frame:true,
                                                            items:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<img width="170" height="200" id="imgCliente" src="../Usuarios/verFoto.php?Id=-1" />'
                                                                        },
                                                                        {
                                                                        	x:210,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"> <b>Orden de cobro: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:335,
                                                                            y:15,
                                                                            xtype:'textfield',
                                                                            id:'txtOrdenCobro',
                                                                            width:120,
                                                                            enableKeyEvents:true,
                                                                            listeners:	{
                                                                            				keyup:function(textField,e)
                                                                                            		{
                                                                                                    	if(e.getKey()==13)
                                                                                                        {
                                                                                                            if(textField.getValue()!='')
                                                                                                            {
                                                                                                                buscarOrdenCobro();
                                                                                                            }
                                                                                                        }
                                                                                                    	
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	xtype:'fieldset',
                                                                            x:480,
                                                                            y:0,
                                                                            height:35,
                                                                            width:440,
                                                                            layout:'absolute',
                                                                            items:	[
                                                                            			{
                                                                                        	x:10,
                                                                                            y:0,
                                                                                            xtype:'label',
                                                                                            html:'<font color="#000000"><b>Tipo de cliente:</b></font>'
                                                                                        },
                                                                                        {
                                                                                        	xtype:'radio',
                                                                                            x:120,
                                                                                            y:-5,
                                                                                            name:'tCliente',
                                                                                            checked:true,
                                                                                            id:'rdoEmpleado',
                                                                                            value:1,
                                                                                            boxLabel:'Empleado',
                                                                                            listeners:	{
                                                                                            				check:function(rdo,valor)
                                                                                                            		{
                                                                                                                    	
                                                                                                                    	if(valor)
                                                                                                                        {
                                                                                                                        	limpiarBase();
                                                                                                                        	tipoCliente=1;
                                                                                                                            gE('lblCliente').innerHTML='<b>Clave empleado: </b>';
                                                                                                                        }
                                                                                                                    }
                                                                                            			}
                                                                                        },
                                                                                        {
                                                                                        	xtype:'radio',
                                                                                            x:270,
                                                                                            y:-5,
                                                                                            name:'tCliente',
                                                                                            id:'rdoCliente',
                                                                                            boxLabel:'Cliente',
                                                                                            value:2,
                                                                                            listeners:	{
                                                                                            				check:function(rdo,valor)
                                                                                                            		{
                                                                                                                    	
                                                                                                                    	if(valor)
                                                                                                                        {
                                                                                                                        	limpiarBase();
                                                                                                                        	tipoCliente=2;
                                                                                                                            gE('lblCliente').innerHTML='<b>Clave cliente: </b>';
                                                                                                                        }
                                                                                                                    }
                                                                                            			}
                                                                                        }
                                                                            		]
                                                                        },
                                                            			{
                                                                        	x:210,
                                                                            y:50,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000" id="lblCliente"> <b>Clave empleado: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:335,
                                                                            y:45,
                                                                            xtype:'textfield',
                                                                            width:120,
                                                                            enableKeyEvents:true,
                                                                            id:'txtClaveEmpleado',
                                                                            listeners:	{
                                                                            				keyup:function(textField,e)
                                                                                            		{
																										if(e.getKey()==13)
                                                                                                        {
                                                                                                            if(textField.getValue()!='')
                                                                                                            {
                                                                                                            	gE('idCliente').value=textField.getValue();
                                                                                                                buscarAdeudosCliente();
                                                                                                            }
                                                                                                         }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	x:490,
                                                                            y:50,
                                                                            xtype:'label',
                                                                             html:'<span style="color:#000"> <b>Empleado: </b></span>'
                                                                        },
                                                                        cmbCliente,
                                                                        {
                                                                        	xtype:'fieldset',
                                                                            width:350,
                                                                            height:120,
                                                                            x:570,
                                                                            y:80,
                                                                            layout:'absolute',
                                                                            items:	[
                                                                            			 {
                                                                                            x:10,
                                                                                            y:10,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#003; font-size:16px"> <b>Subtotal: </b></span>'
                                                                                        },
                                                                                        {
                                                                                            x:150,
                                                                                            y:10,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#900; font-size:16px"><b><span id="lblSubtotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:40,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#003; font-size:16px"> <b>IVA: </b></span>'
                                                                                        },
                                                                                        {
                                                                                            x:150,
                                                                                            y:40,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblIVA">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                                        },
                                                                                        {
                                                                                            x:10,
                                                                                            y:70,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#003; font-size:16px"> <b>Total: </b></span>'
                                                                                        },
                                                                                        {
                                                                                            x:150,
                                                                                            y:70,
                                                                                            xtype:'label',
                                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblTotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                                        }	
                                                                            		]
                                                                        
                                                                        },
                                                                        
                                                                        
                                                                        {
                                                                        	x:210,
                                                                            y:150,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            id:'btnPagar',
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            icon:'../images/icon_big_tick.gif',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            disabled:true,
                                                                                            text:'Registrar pago',
                                                                                            handler:function()
                                                                                                    {
                                                                                                    	obtenerConceptosCobros(); 
                                                                                                       
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        },
                                                                        {
                                                                        	x:360,
                                                                            y:150,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            id:'btnCancelar',
                                                                                            icon:'../images/cross.png',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            disabled:true,
                                                                                            text:'Cancelar pago',
                                                                                            handler:function()
                                                                                                    {
                                                                                                    	function resp(btn)
                                                                                                        {
                                                                                                        	if(btn=='yes')
                                                                                                            {
                                                                                                           		limpiarCaja();
                                                                                                        	}
                                                                                                    	}
                                                                                                        msgConfirm('Est&aacute; seguro de querer cancelar el pago de los conceptos seleccionados?',resp);
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                            		]
                                                        }
	                                           		]
                                        }
                                     ]
						}
                    )  
	gEx('txtOrdenCobro').focus();                      
	calcularTotal();                    
}

function crearGridCaja()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                        			{name: 'idAdeudo'},
                                                                    {name: 'cveProducto'},
                                                                    {name: 'concepto'},
                                                                    {name: 'costoUnitario', type:'float'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'subtotal', type:'float'},                                                                
                                                                    {name: 'iva', type:'float'},
                                                                    {name: 'total', type:'float'},
                                                                    {name: 'montoAbono', type:'float'},
                                                                    {name: 'porcentajeIVA', type:'float'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
                                                       
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:false});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Clave',
															width:110,
															sortable:true,
                                                            hidden:false,
															dataIndex:'cveProducto'
														},
                                                        {
															header:'Concepto',
															width:350,
															sortable:true,
															dataIndex:'concepto'
														},
                                                        
														{
															header:'Costo unitario',
															width:100,
															sortable:true,
                                                            hidden:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney'
														},
                                                        
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
                                                            hidden:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'SubTotal',
															width:100,
															sortable:true,
                                                            hidden:true,
															dataIndex:'subtotal',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:100,
															sortable:true,
															dataIndex:'iva',
                                                            hidden:true,
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Adeudo',
															width:100,
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Monto abono',
															width:100,
															sortable:true,
															dataIndex:'montoAbono',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                            		}
														}
                                                        
													]
												);
                                                
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'grid',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            clicksToEdit:1,
                                                            tbar:	[
                                                            			{
                                                                            xtype:'button',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Agregar concepto',
                                                                            handler:function()
                                                                                    {
                                                                                        
                                                                                        gE('idCliente').value=gEx('txtClaveEmpleado').getValue();
                                                                                        buscarAdeudosCliente();
                                                                                       
                                                                                    }
                                                                        },
                                                                        {
                                                                            xtype:'button',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Remover concepto',
                                                                            handler:function()
                                                                                    {
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas==null)    
                                                                                        {
                                                                                        	msgBox('Debe selccionar el concepto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        tblGrid.getStore().remove(filas);
                                                                                        calcularTotal();
                                                                                        if(tblGrid.getStore().getCount()==0)
                                                                                        {
                                                                                        	gEx('btnPagar').disable();
	                                                                                        gEx('btnCancelar').disable();
                                                                                        }
                                                                                       
                                                                                    }
                                                                        }
                                                            		]
                                                        }
                                                    );
	
   
	tblGrid.on('afteredit',function(e)
    						{
                            	if(e.value>e.record.get('total'))
                                {
                                	msgBox('La cantidad que desea abonar es mayor que el adeudo');
                                	e.record.set('montoAbono',e.record.get('total'));
                                    return;
                                }
                            	calcularTotal();
                            }
    			)                                                                                                                           
	return 	tblGrid;		
}

function calcularTotal()
{
	var x;
    var grid=gEx('grid');
    var ivaTotal=0;
    var subtotal=0;
    var fila;
    var porcIVA;
    var subT;
    var ivaT;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
		subT=fila.get('montoAbono')/((fila.get('porcentajeIVA')/100)+1);
        ivaTotal+=fila.get('montoAbono')-subT;
        subtotal+=subT;
    }
    
    gE('lblSubtotal').innerHTML=Ext.util.Format.usMoney(subtotal);    
    gE('lblIVA').innerHTML=Ext.util.Format.usMoney(ivaTotal);    
    gE('lblTotal').innerHTML=Ext.util.Format.usMoney(subtotal+ivaTotal);    
    
}

function buscarOrdenCobro()
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	if(arrResp[1]=='-1')
            {
            	msgBox('El n&uacute;mero de &oacute;rden de pago ingresado no existe');
            	return;
            }
            var cadObj=arrResp[1];
            var obj=eval('['+cadObj+']')[0];
            gE('idCliente').value=obj.idCliente;
            if(obj.tipoCliente=='1')
            {
            	gEx('rdoEmpleado').setValue(true);
                gE('imgCliente').src='../Usuarios/verFoto.php?Id='+obj.idCliente
            }
            else
            {
            	gEx('rdoCliente').setValue(true);
            }
            gEx('txtClaveEmpleado').setValue(obj.idCliente);
            gEx('cmbCliente').setValue(obj.nombreCliente);
            gEx('grid').getStore().loadData(obj.arrAdeudos);
            calcularTotal();
            gEx('btnPagar').enable();
            gEx('btnCancelar').enable();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=102&noOrdenPago='+gEx('txtOrdenCobro').getValue(),true);
}

function crearGridConceptosAdeudo()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                        			{name: 'idAdeudo'},
                                                                    {name: 'cveProducto'},
                                                                    {name: 'concepto'},
                                                                    {name: 'costoUnitario', type:'float'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'subtotal', type:'float'},                                                                
                                                                    {name: 'iva', type:'float'},
                                                                    {name: 'total', type:'float'},
                                                                    {name: 'montoAbono', type:'float'},
                                                                    {name: 'porcentajeIVA', type:'float'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
                                                       
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Clave',
															width:110,
															sortable:true,
                                                            hidden:false,
															dataIndex:'cveProducto'
														},
                                                        {
															header:'Concepto',
															width:350,
															sortable:true,
															dataIndex:'concepto'
														},
                                                        {
															header:'Adeudo',
															width:100,
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney'
														}
                                                        
													]
												);
                                                
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'gridAdeudos',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            title:'Selecciones los conceptos que desea abonar',
                                                            cm: cModelo,
                                                            sm:chkRow,
                                                            x:10,
                                                            y:40,
                                                            width:700,
                                                            height:350,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            tbar:	[
                                                            			{
                                                                            xtype:'button',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Agregar concepto',
                                                                            handler:function()
                                                                                    {
                                                                                        var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe selccionar el concepto que desea agregar');
                                                                                        	return;
                                                                                        }
                                                                                        var x;
                                                                                        var grid=gEx('grid');
                                                                                        for(x=0;x<filas.length;x++)
                                                                                        {
                                                                                        	if(obtenerPosFila(grid.getStore(),'idAdeudo',filas[x].get('idAdeudo'))==-1)
	                                                                                        	grid.getStore().add(filas[x].copy());
                                                                                        }
                                                                                        calcularTotal();
                                                                                        gEx('btnPagar').enable();
                                                                                        gEx('btnCancelar').enable();
                                                                                        gEx('ventanaAdeudos').close();
                                                                                       
                                                                                    }
                                                                        }
                                                            		]
                                                        }
                                                    );
	 tblGrid.nuevoRegistro=false;   
                                                                                                                           
	return 	tblGrid;		
}

function buscarAdeudosCliente()
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	if(arrResp[1]=='-1')
            {
            	msgBox('El n&uacute;mero de empleado/cliente ingresado no tiene adeudos vigentes');
            	return;
            }
            var cadObj=arrResp[1];
            var obj=eval('['+cadObj+']')[0];
            gE('idCliente').value=obj.idCliente;
            if(obj.tipoCliente=='1')
            {
            	gEx('rdoEmpleado').setValue(true);
                gE('imgCliente').src='../Usuarios/verFoto.php?Id='+obj.idCliente
            }
            else
            {
            	gEx('rdoCliente').setValue(true);
            }
            gEx('txtClaveEmpleado').setValue(obj.idCliente);
            gEx('cmbCliente').setValue(obj.nombreCliente);
            mostrarVentanaAdeudos(obj.arrAdeudos);
            gEx('btnCancelar').enable();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=103&idCliente='+gE('idCliente').value+'&tipoCliente='+tipoCliente,true);
}

function mostrarVentanaAdeudos(arrAdeudos)
{
	var gridAdeudos=crearGridConceptosAdeudo();
    gridAdeudos.getStore().loadData(arrAdeudos);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            
                                                            html:'<b><span id="lblNomCliente" style="color:#000000"></span></b>'
                                                        },
														gridAdeudos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'ventanaAdeudos',
										title: 'Adeudos pendientes',
										width: 750,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	
	ventanaAM.show();
    gE('lblNomCliente').innerHTML='['+gEx('txtClaveEmpleado').getValue()+'] '+gEx('cmbCliente').getRawValue();                                
}

function limpiarCaja()
{
	limpiarBase();
    gEx('rdoEmpleado').setValue(true);
    tipoCliente=1;
    
}

function limpiarBase()
{
	gE('imgCliente').src='../Usuarios/verFoto.php?Id=-1';
	gEx('grid').getStore().removeAll();
    calcularTotal();
    gEx('txtClaveEmpleado').setValue('');
    gEx('txtOrdenCobro').setValue('');
    gEx('cmbCliente').setValue();
    gE('idCliente').value=-1;
    gEx('txtOrdenCobro').focus();
    gEx('btnPagar').disable();
    gEx('btnCancelar').disable();
}

function obtenerConceptosCobros()
{
	var grid=gEx('grid');
    var x;
    var fila;
    var arrCobro='';
    var obj='';
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        obj='{"idAdeudo":"'+fila.get('idAdeudo')+'","montoAbono":"'+fila.get('montoAbono')+'","confirmado":"1"}';
        if(arrCobro=='')
        	arrCobro=obj;
        else
        	arrCobro+=','+obj;
        
    }
    var cadObj='{"idCaja":"1","arrCobros":['+arrCobro+']}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            limpiarCaja();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=105&cadObj='+cadObj,true);
    
}
