<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idPlanPagos,nombrePlanPago,descripcion FROM 6020_planesPago ORDER BY nombrePlanPago";
	$arrPlanPagos="";
	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$consulta="SELECT idPlanPagos,noPago,etiquetaPago,'' FROM 6021_desglocePlanPagos WHERE  idPlanPagos=".$fila[0]." ORDER BY noPago";
		$arrPagos=$con->obtenerFilasArreglo($consulta);
		$obj="['".$fila[0]."','".cv($fila[1])."','".cv($fila[2])."',".$arrPagos."]";
		if($arrPlanPagos=="")
			$arrPlanPagos=$obj;
		else
			$arrPlanPagos.=",".$obj;
	}
	$arrPlanPagos="[".$arrPlanPagos."]";
	$consulta="SELECT i.idConcepto,concat('[',cveConcepto,'] ',nombreConcepto) FROM 561_conceptosIngreso i,564_conceptosVSCategorias c
				WHERE c.idConcepto=i.idConcepto AND c.idCategoria=10 ORDER BY cveConcepto,nombreConcepto ";
	$arrConceptosIngreso=$con->obtenerFilasArreglo($consulta);
?>
var arrValor=[['1','Decimal'],['2','Entero'],['3','Fecha'],['4','Fecha (Fecha inicial de aplicaci\xF3n)'],['5','Fecha (Fecha final de aplicaci\xF3n)']];
var arrSiNo=[['0','No'],['1','S\xED']];
var arrConceptosIngreso=<?php echo $arrConceptosIngreso?>;
var arrPlanPagos=<?php echo $arrPlanPagos?>;
var registroDimension;
var registroPlanPago;
var arrTipoOperacion=[['1','Cargo'],['-1','Descuento']];
var arrOrigenValor=[['1','Definido por funci\xF3n de sistema'],['2','Definido por porcentaje'],['3','Definido por valor absoluto']];
var arrTipoDescuentos=[['1','Porcentaje'],['2','Monto']];
var arrConfiguracionPlanPagos=[];
var regConfAvanzada=null;


Ext.onReady(inicializar);

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	var x;
    	var id=gE('id').value;
        var gridCategoriasConcepto=gEx('gridCategoriasConcepto');
        var fila;
        var cadCategoria='';
        var obj='';
        for(x=0;x<gridCategoriasConcepto.getStore().getCount();x++)
        {
        	fila=gridCategoriasConcepto.getStore().getAt(x);
            obj='{"idCategoria":"'+fila.get('idCategoria')+'"}';
            if(cadCategoria=='')
            	cadCategoria=obj;
            else
            	cadCategoria+=','+obj;
        }
        var cadPlanPagos='';
        var valor=getValorCombo(gE('_vincularPlanPagoint'));

        if(valor=='1')
        {
            var gridPlanPagosConcepto=gEx('gridPlanPagosConcepto');
            for(x=0;x<gridPlanPagosConcepto.getStore().getCount();x++)
            {
                fila=gridPlanPagosConcepto.getStore().getAt(x);
                obj='{"idPlanPagos":"'+fila.get('idPlanPagos')+'","noPago":"'+fila.get('noPago')+'","conceptoAsociado":"'+fila.get('conceptoAsociado')+'"}';
                if(fila.get('conceptoAsociado')=='')
                {
                	function resp10()
                    {
                    	gridPlanPagosConcepto.startEditing(x,2);
                    }
                    msgBox('Debe ingresar el concepto de pago con el cual se asociar&aacute; el n&uacute;mero de pago',resp10);
                    return;
                }
                if(cadPlanPagos=='')
                    cadPlanPagos=obj;
                else
                    cadPlanPagos+=','+obj;
            }
            if(cadPlanPagos=='')
            {
            	msgBox('Debe vincular el concepto con al menos un Plan de Pagos');
            	return;
            }
        }
        
        var cadOperaciones='';
        var gridCargosDescuentos=gEx('gridCargosDescuentos');
        for(x=0;x<gridCargosDescuentos.getStore().getCount();x++)
        {
            fila=gridCargosDescuentos.getStore().getAt(x);

            obj='{"idOperacion":"'+fila.get('idOperacion')+'","ordenAplicacion":"'+fila.get('ordenAplicacion')+'","idTipoOperacion":"'+fila.get('idTipoOperacion')+
            		'","etiquetaOperacion":"'+fila.get('etiquetaOperacion')+'","origenValor":"'+fila.get('origenValor')+'","valor":"'+fila.get('valor')+'","funcionAplicacion":"'+fila.get('funcionAplicacion')+
                    '","configuracionComp":"'+bE(fila.get('configuracionComp'))+'"}';
            
            if(cadOperaciones=='')
                cadOperaciones=obj;
            else
                cadOperaciones+=','+obj;
        }

        var cadConfiguracionPlan='';
        if(id=='-1')
        {
            for(x=0;x<arrConfiguracionPlanPagos.length;x++)
            {
                if(cadConfiguracionPlan=='')
                    cadConfiguracionPlan=arrConfiguracionPlanPagos[x][1];
                else
                    cadConfiguracionPlan+=','+arrConfiguracionPlanPagos[x][1];
            }
        }
        var objArr='{"arrCategorias":['+cadCategoria+'],"arrPlanPagos":['+cadPlanPagos+'],"arrConfiguracionPlan":['+cadConfiguracionPlan+'],"arrOperaciones":['+cadOperaciones+']}';
        
        if(id=='-1')
        {
        	gE('funcPHPEjecutarNuevo').value=bE('asociarCategoriasConcepto(@idRegPadre,\''+bE(objArr)+'\')');
        }
        else
        {
        	gE('funcPHPEjecutarModif').value=bE('asociarCategoriasConcepto('+id+',\''+bE(objArr)+'\')');
        }
    	
    	gE('frmEnvio').submit();
    }
}

function inicializar()
{
	registroDimension=crearRegistro([{name: 'idCategoria'},{name: 'categoria'}]);
    registroPlanPago=crearRegistro([{name: 'idPlanPagos'},{name: 'noPago'},{name: 'etPago'},{name: 'conceptoAsociado'}]);
	crearGridCategorias();
    crearGridPlanPagos();
    crearGridDescuentosCargos();
    regConfAvanzada=crearRegistro(
    								[
                                    	{name:'plantel'},
                                        {name: 'nombrePlantel'},
                                        {name:'aplicaPlantel'},
                                        {name: 'valorReferencia'},
                                        {name: 'tipoValorReferencia'}
                                    ]
    							);
}

function crearGridCategorias()
{
	
	var dsDatos=eval(bD(gE('arrCategorias').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idCategoria'},
                                                                    {name: 'categoria'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Categor&iacute;a',
															width:250,
															sortable:true,
															dataIndex:'categoria'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            id:'gridCategoriasConcepto',

                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            renderTo:'tblCategorias',
                                                            height:220,
                                                            width:450,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar categor&iacute;a',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarCategoria();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Eliminar categoria',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar almenos una categor&iacute;a a remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                           
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la catagor&iacute;a seleccionada?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function crearGridPlanPagos()
{
	var dsDatos=eval(bD(gE('arrPlanPagos').value));
    
   	var cmbConceptoIngreso=crearComboExt('cmbConceptoIngreso',arrConceptosIngreso);
	
    var lector= new Ext.data.ArrayReader({
                                            fields: [
                                               			{name: 'idPlanPagos'},
                                                        {name: 'noPago'},
                                                        {name: 'etPago'},
                                                        {name: 'conceptoAsociado'}
                                            		]
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
															data:dsDatos,					
                                                            groupField: 'idPlanPagos',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
    
    
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														{
															header:'Plan de pagos',
															width:250,
															sortable:true,
															dataIndex:'idPlanPagos',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrPlanPagos,val);
                                                                    }
														},
                                                        {
															header:'No. pago',
															width:250,
															sortable:true,
															dataIndex:'etPago'
														},
                                                        {
															header:'Asociado a concepto',
															width:450,
															sortable:true,
															dataIndex:'conceptoAsociado',
                                                            editor:cmbConceptoIngreso,
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrConceptosIngreso,val));
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            id:'gridPlanPagosConcepto',

                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            renderTo:'tblPlanesPago',
                                                            height:320,
                                                            width:770,
                                                            clicksToEdit:1,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar plan de pagos',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarPlanPagos();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover plan de pagos',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaRemoverPlan();
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Configuraci&oacute;n avanzada de plan de pagos',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaModificarPlan();
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		],
                                                            view:new Ext.grid.GroupingView({
                                                                                            forceFit:false,
                                                                                            showGroupName: true,
                                                                                            enableGrouping :true,
                                                                                            enableNoGroups:false,
                                                                                            enableGroupingMenu:false,
                                                                                            hideGroupedColumn: true,
                                                                                            startCollapsed:false
                                                                                        })
                                                        }
                                                    );
	return 	tblGrid;
}

function mostrarVentanaRemoverPlan()
{
	var gridPlanPagos=crearGridPlanPagosRemover();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Indique los planes de pago que desea remover:'
                                                        },
														gridPlanPagos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Remover Plan de Pagos',
										width: 410,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var gridPlanPagosConcepto=gEx('gridPlanPagosConcepto');
                                                                    	var filas=gridPlanPagos.getSelectionModel().getSelections();
                                                                        if(filas.length=='0')
                                                                        {
                                                                        	msgBox('Al menos debe seleccionar un Plan de Pagos a remover');
                                                                            return;
                                                                        }
                                                                        var x;
                                                                        var z;
                                                                        var f;
                                                                        var fAux;
                                                                        var pos;
                                                                        var arrFilas=new Array();
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	f=filas[x];
                                                                            for(z=0;z<gridPlanPagosConcepto.getStore().getCount();z++)
                                                                            {
                                                                            	fAux=gridPlanPagosConcepto.getStore().getAt(z);
                                                                                if(fAux.get('idPlanPagos')==f.get('idPlanPagos'))
                                                                                	arrFilas.push(fAux);
                                                                            }
                                                                            
                                                                            
                                                                        }
																		function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                gridPlanPagosConcepto.getStore().remove(arrFilas);
                                                                                ventanaAM.close();
                                                                            }
                                                                           
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer remover los planes de pago seleccionados?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridPlanPagosRemover()
{
	var dsDatos=[];
    var gridPlanPagosConcepto=gEx('gridPlanPagosConcepto');
    var x;
    var fila;
    for(x=0;x<gridPlanPagosConcepto.getStore().getCount();x++)
    {
    	fila=gridPlanPagosConcepto.getStore().getAt(x);
        if(existeValorMatriz(dsDatos,fila.get('idPlanPagos'))==-1)
        	dsDatos.push([fila.get('idPlanPagos'),formatearValorRenderer(arrPlanPagos,fila.get('idPlanPagos'))]);
    }

    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idPlanPagos'},
                                                                {name: 'planPagos'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Plan de pagos',
															width:300,
															sortable:true,
															dataIndex:'planPagos'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            y:40,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:160,
                                                            width:380,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;
}

function mostrarVentanaAgregarCategoria()
{
	var gridConcepto=crearGridConcepto();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Indique las categor&iacute;as que desea agregar al concepto:'
                                                        },
                                                        gridConcepto
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar categor&iacute;a',
										width: 700,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var  filas=gridConcepto.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos una categor&iacute;a para asociarla al concepto');
                                                                            return;
                                                                        }
                                                                     	var x;
                                                                        var f;
                                                                        var pos;
                                                                        var gridCategorias=gEx('gridCategoriasConcepto');
                                                                        for(x=0;x<filas.length;x++)   
                                                                        {
                                                                        	
                                                                        	f=filas[x];
                                                                            pos=obtenerPosFila(gridCategorias.getStore(),'idCategoria',f.get('idCategoria'));
                                                                            if(pos==-1)
                                                                            {
                                                                            	var r=new registroDimension({idCategoria:f.get('idCategoria'),categoria:f.get('nombreCategoria')});
                                                                                gridCategorias.getStore().add(r);
                                                                            }
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}

function crearGridConcepto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCategoria'},
		                                                {name: 'nombreCategoria'},
		                                                {name:'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesContabilidad.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreCategoria', direction: 'ASC'},
                                                            groupField: 'nombreCategoria',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='110';

                                    }
                        )   
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Categor&iacute;a',
                                                                width:230,
                                                                sortable:true,
                                                                dataIndex:'nombreCategoria',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                renderer:mostrarValorDescripcion
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridCategorias',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:360,
                                                                sm:chkRow,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function mostrarVentanaAgregarPlanPagos()
{
	var gridPlanPago=crearGridCatalogoPlanPago();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Indique los planes de pago que desea vincular al concepto:'
                                                        },
                                                        gridPlanPago
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar Plan de Pagos',
										width: 700,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var  filas=gridPlanPago.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos un Plan de Pagos a con el concepto');
                                                                            return;
                                                                        }
                                                                     	var x;
                                                                        var z;
                                                                        var f;
                                                                        var pos;
                                                                        var gridPlanPagosConcepto=gEx('gridPlanPagosConcepto');
                                                                        for(x=0;x<filas.length;x++)   
                                                                        {
                                                                        	
                                                                        	f=filas[x];
                                                                            pos=obtenerPosFila(gridPlanPagosConcepto.getStore(),'idPlanPagos',f.get('idPlanPago'));
                                                                            if(pos==-1)
                                                                            {
                                                                            	var arrPagos=f.get('arrPagos');
                                                                                for(z=0;z<arrPagos.length;z++)
                                                                                {
                                                                                    var r=new registroPlanPago({idPlanPagos:arrPagos[z][0],noPago:arrPagos[z][1],etPago:arrPagos[z][2],conceptoAsociado:arrPagos[z][3]});
                                                                                    gridPlanPagosConcepto.getStore().add(r);
                                                                                }
                                                                            }
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}

function crearGridCatalogoPlanPago()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idPlanPago'},
                                                                {name: 'nombrePlanPago'},
                                                                {name: 'descripcion'},
                                                                {name: 'arrPagos'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(arrPlanPagos);
                                                   
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Plan de pagos',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'nombrePlanPago'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcion'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gPlanPago',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                y:40,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:320,
                                                                sm:chkRow
                                                            }
                                                        );
        return 	tblGrid;
}

function vinculoPlanPagoChange(cmb)
{
	var valor=getValorCombo(cmb);
    if(valor=='1')
    	mE('filaPlanPago');
    else
    	oE('filaPlanPago');
}

function crearGridDescuentosCargos()
{
	var dsDatos=eval(bD(gE('arrOperaciones').value));
    
   	
	
    var lector= new Ext.data.ArrayReader({
                                            fields: [
		                                            	{name: 'idOperacion'},
                                                        {name: 'ordenAplicacion', type:'int'},
                                               			{name: 'idTipoOperacion'},
                                                        {name: 'etiquetaOperacion'},
                                                        {name: 'origenValor'},
                                                        {name: 'valor'},
                                                        {name:'lblValor'},
                                                        {name: 'funcionAplicacion'},
                                                        {name:'lblFuncionAplicacion'},
                                                        {name:'configuracionComp'}
                                            		]
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
															data:dsDatos,					
                                                            groupField: 'ordenAplicacion',
                                                            
                                                            
                                                        })     
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	{
															header:'Orden de aplicaci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'ordenAplicacion'
                                                            
														},
														{
															header:'Tipo de operaci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'idTipoOperacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoOperacion,val);
                                                                    }
														},
                                                        {
															header:'Etiqueta operaci&oacute;n',
															width:160,
															sortable:true,
															dataIndex:'etiquetaOperacion'
                                                            
														},
                                                        {
															header:'Origen del valor',
															width:180,
															sortable:true,
															dataIndex:'origenValor',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrOrigenValor,val);
                                                                    }
														},
                                                        {
															header:'Valor',
															width:220,
															sortable:true,
															dataIndex:'lblValor',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},

                                                        {
															header:'Funci&oacute;n de aplicaci&oacute;n',
															width:250,
															sortable:true,
															dataIndex:'lblFuncionAplicacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            id:'gridCargosDescuentos',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            renderTo:'tblCargos',
                                                            height:270,
                                                            width:770,
                                                            clicksToEdit:1,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar cargo/descuento',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaCargoDescuento();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar cargo/descuento',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la operaci&oacute;n que desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaCargoDescuento(fila);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover cargo/descuento',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el elemento que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                                var x;
                                                                                                for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                                {
                                                                                                	tblGrid.getStore().getAt(x).set('ordenAplicacion',(x+1));
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;<b>Modificar orden de apliaci&oacute;n:&nbsp;&nbsp;</b>'
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignUp.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la operaci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.ordenAplicacion>1)
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.ordenAplicacion-2);
                                                                                            fila.data.ordenAplicacion-=1;
                                                                                            filaAux.data.ordenAplicacion+=1;
                                                                                            tblGrid.getStore().sort('ordenAplicacion','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignDown.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la operaci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.ordenAplicacion<tblGrid.getStore().getCount())
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.ordenAplicacion);
                                                                                            fila.data.ordenAplicacion+=1;
                                                                                            filaAux.data.ordenAplicacion-=1;
                                                                                            tblGrid.getStore().sort('ordenAplicacion','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-'
                                                                        
                                                            		],
                                                            view:new Ext.grid.GroupingView({
                                                                                            forceFit:false,
                                                                                            showGroupName: true,
                                                                                            enableGrouping :false,
                                                                                            enableNoGroups:false,
                                                                                            enableGroupingMenu:false,
                                                                                            hideGroupedColumn: false,
                                                                                            startCollapsed:false
                                                                                        })
                                                        }
                                                    );
	return 	tblGrid;
    
}

function mostrarVentanaCargoDescuento(fConf)
{
	var cmbTipoOperacion=crearComboExt('cmbTipoOperacion',arrTipoOperacion,185,35,250);
    var cmbOrigenValorOperacion=crearComboExt('cmbOrigenValorOperacion',arrOrigenValor,185,65,250);
    cmbOrigenValorOperacion.setValue('1');
    cmbOrigenValorOperacion.on('select',function(cmb,registro)
    									{
                                        	gEx('txtValorOperacion').hide();
                                            gEx('txtValorOperacion').setValue('');
                                            gEx('txtFuncionValorOperacion').hide();
                                            gEx('txtFuncionValorOperacion').idConsulta=-1;
                                            gEx('txtFuncionValorOperacion').setValue('');
                                            gEx('linkPencil1').hide();
                                        	switch(registro.data.id)
                                            {
                                                case '1':
                                                    gEx('txtFuncionValorOperacion').show();
                                                    gEx('linkPencil1').show();
                                                    gE('lblValorOperacion').innerHTML='Funci&oacute;n del sistema'
                                                break;
                                                case '2':
                                                	gEx('txtValorOperacion').show();
                                                    gEx('txtValorOperacion').focus(false,500);
                                                	gE('lblValorOperacion').innerHTML='Porcentaje a aplicar'
                                               break;
                                                case '3':
                                                    gEx('txtValorOperacion').show();
                                                    gEx('txtValorOperacion').focus(false,500);
                                                    gE('lblValorOperacion').innerHTML='Monto a aplicar'
                                                break;
                                            }
                                        }
    							)
    var cmbInterface=crearComboExt('cmbInterface',arrSiNo,185,155,115);
    cmbInterface.setValue('0');
    
                    
   	var cmbReferencia=crearComboExt('cmbReferencia',arrSiNo,185,185,115);
    cmbReferencia.setValue('0');
    cmbReferencia.on('select',function(cmb,registro)
    						{
                            	gEx('frameConf').disable();
                                if(registro.data.id=='1')
                                	gEx('frameConf').enable();
                            }
    				)
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                            x:10,
                                                            y:10,
                                                            html:'Etiqueta de operaci&oacute;n:<span class="letraRoja"><b>*</b></span>'
                                                        },
                                                        {
                                                        	x:185,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:280,
                                                            id:'txtEtiqueta',
                                                        },
														{
                                                            x:10,
                                                            y:40,
                                                            html:'Tipo de operaci&oacute;n:<span class="letraRoja"><b>*</b></span>'
                                                        },

														cmbTipoOperacion,
                                                        {
                                                            x:10,
                                                            y:70,
                                                            html:'Origen del valor de la operaci&oacute;n:<span class="letraRoja"><b>*</b></span>'
                                                        },
                                                        cmbOrigenValorOperacion,
                                                        
                                                        {
                                                            x:10,
                                                            y:100,
                                                            html:'<span id="lblValorOperacion">Funci&oacute;n del sistema</span>:<span class="letraRoja"><b>*</b></span>'
                                                        },
                                                        {
                                                        	x:185,
                                                            y:95,
                                                            hidden:true,
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'txtValorOperacion'
                                                            
                                                        },
                                                        {
                                                        	x:185,
                                                            y:95,
                                                            xtype:'textfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            width:280,
                                                            readOnly:true,
                                                            id:'txtFuncionValorOperacion'
                                                            
                                                        },
                                                        {
                                                        	x:480,
                                                            y:95,
                                                            id:'linkPencil1',
                                                            html:'<a href="javascript:mostrarVentanaFuncion(1)"><img src="../images/pencil.png"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:removerFuncion(1)"><img src="../images/cross.png"></a>'
                                                        },
                                                        {
                                                            x:10,
                                                            y:130,
                                                            html:'Funci&oacute;n de aplicaci&oacute;n:'
                                                        },
                                                         {
                                                        	x:185,
                                                            y:125,
                                                            xtype:'textfield',
                                                            width:280,
                                                            readOnly:true,
                                                            id:'txtFuncionAplicacion'
                                                            
                                                        },
                                                        {
                                                        	x:480,
                                                            y:125,
                                                            html:'<a href="javascript:mostrarVentanaFuncion(2)"><img src="../images/pencil.png"></a>&nbsp;&nbsp;&nbsp;<a href="javascript:removerFuncion(2)"><img src="../images/cross.png"></a>'
                                                        },
                                                         {
                                                            x:10,
                                                            y:160,
                                                            html:'Calcular en interface de costeo:<span class="letraRoja"><b>*</b></span>'
                                                        },
                                                        cmbInterface,
                                                        {
                                                            x:10,
                                                            y:190,
                                                            html:'Requiere valores de referencia:<span class="letraRoja"><b>*</b></span>'
                                                        },
                                                        cmbReferencia,
                                                        {
                                                        	x:10,
                                                            y:220,
                                                            xtype:'fieldset',
                                                            title:'Configuraci&oacute;n de valores de referencia',
                                                            width:580,
                                                            height:220,
                                                            id:'frameConf',
                                                            disabled:true,
                                                            items:	[crearGridColumnasReferencia()]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar cargo/descuento',
										width: 630,
										height:520,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtEtiqueta').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var txtEtiqueta=gEx('txtEtiqueta');
                                                                        if(txtEtiqueta.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtEtiqueta.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la etiqueta de la operaci&oacute;n',resp1);
                                                                            return;
                                                                        }
                                                                        var tipoOperacion=cmbTipoOperacion.getValue();
                                                                        if(tipoOperacion=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbTipoOperacion.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar el tipo de operaci&oacute;n operaci&oacute;n a agregar',resp2);
                                                                            return;
                                                                        }
                                                                        var valor='';
                                                                        var lblValor='';
                                                                        var txtValorOperacion=gEx('txtValorOperacion');
                                                                        var txtFuncionValorOperacion=gEx('txtFuncionValorOperacion');
                                                                        var txtFuncionAplicacion=gEx('txtFuncionAplicacion');
                                                                        switch(cmbOrigenValorOperacion.getValue())
                                                                        {
                                                                        	case '1':
                                                                            	if(txtFuncionValorOperacion.getValue()=='')
                                                                                {
                                                                                	msgBox('Debe indicar la funci&oacute;n del sistema que fungir&aacute; como origen del valor de la operaci&oacute;n');
                                                                                    return;
                                                                                }
                                                                                valor=txtFuncionValorOperacion.idConsulta;
                                                                                
                                                                                lblValor=txtFuncionValorOperacion.getValue();
                                                                            break;
                                                                            case '2':
                                                                            case '3':
                                                                            	if(txtValorOperacion.getValue()=='')
                                                                                {
                                                                                	msgBox('El valor ingresado como origen del valor de la operaci&oacute;n no es v&aacute;lido');
                                                                                    return;
                                                                                }
                                                                                valor=txtValorOperacion.getValue();
                                                                                lblValor=txtValorOperacion.getValue();
                                                                            break;
                                                                            
                                                                        }
                                                                        
                                                                        var idFuncionAplicacion=-1;
                                                                        var lblFuncionAplicacion='';
                                                                        if(txtFuncionAplicacion.getValue()!='')
                                                                        {
                                                                        	idFuncionAplicacion=txtFuncionAplicacion.idConsulta;
                                                                            lblFuncionAplicacion=txtFuncionAplicacion.getValue();
                                                                        }                
                                                                        
                                                                        var v=valor+'';
                                                                        var l=lblValor+'';
                                                                        var lF=lblFuncionAplicacion;
                                                                        var arrColumnas='';
                                                                        var x;
                                                                        var gridColumnas=gEx('gridColumnas');
                                                                        var fila;
                                                                        var o;
                                                                        for(x=0;x<gridColumnas.getStore().getCount();x++)
                                                                        {
                                                                        	
                                                                        	fila=gridColumnas.getStore().getAt(x);
                                                                            if(fila.get('etiqueta')=='')
                                                                            {
                                                                            	function resp10()
                                                                                {
                                                                                	gridColumnas.startEditing(x,1);
                                                                                }
                                                                                msgBox('Debe ingresar la etiqueta de la columna',resp10);
                                                                            	return;
                                                                            }
                                                                            if(fila.get('anchoColumna')=='')
                                                                            {
                                                                            	function resp11()
                                                                                {
                                                                                	gridColumnas.startEditing(x,2);
                                                                                }
                                                                                msgBox('Debe ingresar el ancho de la columna',resp11);
                                                                            	return;
                                                                            }
                                                                            if(fila.get('tipoValor')=='')
                                                                            {
                                                                            	function resp12()
                                                                                {
                                                                                	gridColumnas.startEditing(x,3);
                                                                                }
                                                                                msgBox('Debe ingresar el tipo de valor de la columna',resp12);
                                                                            	return;
                                                                            }
                                                                            o='{"idColumna":"'+fila.get('idColumna')+'","etiqueta":"'+fila.get('etiqueta')+'","anchoColumna":"'+fila.get('anchoColumna')+'","tipoValor":"'+fila.get('tipoValor')+'"}';
                                                                            if(arrColumnas=='')
                                                                            	arrColumnas=o;
                                                                            else
                                                                            	arrColumnas+=','+o;
                                                                        }
                                                                        
                                                                        
                                                                        var objConf='{"calcularInterfaceCosto":"'+cmbInterface.getValue()+'","valorReferencia":"'+cmbReferencia.getValue()+'","arrColumnas":['+arrColumnas+']}';
                                                                        if(!fConf)                                                        
                                                                        {
                                                                            var rOperacion=crearRegistro(	[
                                                                                                                {name: 'idOperacion'},
                                                                                                                {name: 'ordenAplicacion'},
                                                                                                                {name: 'idTipoOperacion'},
                                                                                                                {name: 'etiquetaOperacion'},
                                                                                                                {name: 'origenValor'},
                                                                                                                {name: 'valor'},
                                                                                                                {name:'lblValor'},
                                                                                                                {name: 'funcionAplicacion'},
                                                                                                                {name:'lblFuncionAplicacion'},
                                                                                                                {name:'configuracionComp'}
                                                                                                            ]);
                                                                            var r=new rOperacion	(
                                                                                                        {
                                                                                                            idOperacion:-1,
                                                                                                            ordenAplicacion: gEx('gridCargosDescuentos').getStore().getCount()+1,
                                                                                                            idTipoOperacion:tipoOperacion,
                                                                                                            etiquetaOperacion:txtEtiqueta.getValue(),
                                                                                                            origenValor:cmbOrigenValorOperacion.getValue(),
                                                                                                            valor:v,
                                                                                                            lblValor:l,
                                                                                                            funcionAplicacion:idFuncionAplicacion,
                                                                                                            lblFuncionAplicacion:lF,
                                                                                                            configuracionComp:objConf
                                                                                                            
                                                                                                        }
                                                                                                    )   ;    
                                                                             gEx('gridCargosDescuentos').getStore().add(r); 
                                                                         }
                                                                         else
                                                                         {
                                                                         	fConf.set('idTipoOperacion',tipoOperacion);
                                                                            fConf.set('etiquetaOperacion',txtEtiqueta.getValue());
                                                                            fConf.set('origenValor',cmbOrigenValorOperacion.getValue());
                                                                            fConf.set('valor',v);
                                                                            fConf.set('lblValor',l);
                                                                            fConf.set('funcionAplicacion',idFuncionAplicacion);
                                                                            fConf.set('lblFuncionAplicacion',lF);
                                                                            fConf.set('configuracionComp',objConf);
                                                                         }
                                                                         ventanaAM.close();                       
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();		
	if(fConf)
    {
    	var txtEtiqueta=gEx('txtEtiqueta');
        txtEtiqueta.setValue(fConf.get('etiquetaOperacion'));
        cmbTipoOperacion.setValue(fConf.get('idTipoOperacion'));
        cmbOrigenValorOperacion.setValue(fConf.get('origenValor'));
        dispararEventoSelectCombo('cmbOrigenValorOperacion');
		if(cmbOrigenValorOperacion.getValue()=='1')
        {
        	var txtFuncionValorOperacion=gEx('txtFuncionValorOperacion');
            txtFuncionValorOperacion.idConsulta=fConf.get('valor');
            txtFuncionValorOperacion.setValue(fConf.get('lblValor'));
        }
        else
        {
            var txtValorOperacion=gEx('txtValorOperacion');
            txtValorOperacion.setValue(fConf.get('valor'));
        }
        
        
        var txtFuncionAplicacion=gEx('txtFuncionAplicacion');
        
        
        if(fConf.get('idFuncionAplicacion')!='')
        {
            txtFuncionAplicacion.idConsulta=fConf.get('idFuncionAplicacion');
            txtFuncionAplicacion.setValue(fConf.get('lblFuncionAplicacion'));
        }
        var objConf=eval('['+fConf.get('configuracionComp')+']')[0];
        cmbInterface.setValue(objConf.calcularInterfaceCosto);
        cmbReferencia.setValue(objConf.valorReferencia);
        dispararEventoSelectCombo('cmbReferencia');
        var gridColumnas=gEx('gridColumnas');
        var arrColumnas=new Array();
        var x;
        var fAux;
        
        for(x=0;x<objConf.arrColumnas.length;x++)
        {
        	fAux=objConf.arrColumnas[x];
           	arrColumnas.push([fAux.idColumna,fAux.etiqueta,fAux.anchoColumna,fAux.tipoValor]); 
        }
        gridColumnas.getStore().loadData(arrColumnas);
    }
}

function mostrarVentanaFuncion(tipoFuncion)
{
	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                            {
                                            	var txtDestino='';
                                                if(tipoFuncion==1)
                                                	txtDestino='txtFuncionValorOperacion';
                                                else
                                                	txtDestino='txtFuncionAplicacion';
                                                gEx(txtDestino).setValue(nombre);
                                                gEx(txtDestino).idConsulta=idConsulta;
                                                gEx('vAgregarExp').close();
                                                
                                            }
	mostrarVentanaExpresion(function(fila,ventana)
    						{
                            	var txtDestino='';
                                if(tipoFuncion==1)
                                    txtDestino='txtFuncionValorOperacion';
                                else
                                    txtDestino='txtFuncionAplicacion';
                            	gEx(txtDestino).setValue(fila.get('nombreConsulta'));
                                gEx(txtDestino).idConsulta=fila.get('idConsulta');

                                ventana.close();
                            }
    						,
    						true);
}

function removerFuncion(tipoFuncion)
{
	var txtDestino='';
    if(tipoFuncion==1)
        txtDestino='txtFuncionValorOperacion';
    else
        txtDestino='txtFuncionAplicacion';
    gEx(txtDestino).setValue();
    gEx(txtDestino).idFuncion=-1;
}

function crearGridColumnasReferencia()
{
	var cmbTipoValor=crearComboExt('cmbTipoValor',arrValor);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idColumna'},
                                                                {name: 'etiqueta'},
                                                                {name: 'anchoColumna'},
                                                                {name: 'tipoValor'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Etiqueta<span class="letraRoja">*</span>',
															width:150,
															sortable:true,
															dataIndex:'etiqueta',
                                                            editor:{xtype:'textfield'},
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Ancho columna<span class="letraRoja">*</span>',
															width:110,
															sortable:true,
															dataIndex:'anchoColumna',
                                                            editor:{xtype:'numberfield'}
														},
														{
															header:'Tipo de valor<span class="letraRoja">*</span>',
															width:220,
															sortable:true,
															dataIndex:'tipoValor',
                                                            editor:cmbTipoValor,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrValor,val);
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridColumnas',
                                                            store:alDatos,
                                                            frame:true,
                                                            y:10,
                                                            clicksToEdit:1,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:180,
                                                            width:560,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar columna de referencia',
                                                                            handler:function()
                                                                            		{
                                                                                    	var regColumna=crearRegistro([	{name: 'idColumna'},
                                                                                                                        {name: 'etiqueta'},
                                                                                                                        {name: 'anchoColumna'},
                                                                                                                        {name: 'tipoValor'}
                                                                                                                      ]);
                                                                                        var r=new  regColumna(	{
                                                                                        							idColumna:-1,
                                                                                                                    etiqueta:'',
                                                                                                                    anchoColumna:110,
                                                                                                                    tipoValor:''
                                                                                                                    
                                                                                        						}
                                                                                                              )                           
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover columna de referencia',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccioanar las columnas de referencia que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp2(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover las columnas de referencias seleccionadas?',resp2);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;		
}


function mostrarVentanaModificarPlan()
{
	var arrPlanPlagos=[];
    var gridPlanPagosConcepto=gEx('gridPlanPagosConcepto');
    var x;
    var fila;
    for(x=0;x<gridPlanPagosConcepto.getStore().getCount();x++)
    {
    	fila=gridPlanPagosConcepto.getStore().getAt(x);
        if(existeValorMatriz(arrPlanPlagos,fila.get('idPlanPagos'))==-1)
        	arrPlanPlagos.push([fila.get('idPlanPagos'),formatearValorRenderer(arrPlanPagos,fila.get('idPlanPagos'))]);
    }
	var cmbPlanPagosConf=crearComboExt('cmbPlanPagosConf',arrPlanPlagos,110,5,300);
    cmbPlanPagosConf.on('select',function(cmb,registro)
    							{
                                	var pos=existeValorMatriz(arrConfiguracionPlanPagos,registro.data.id,0,true);
                                	if((gE('id').value!='-1')||(pos==-1))
                                    {
                                        gEx('gridConfiguracionPlanEstudio').getStore().load	(
                                                                                                {
                                                                                                    url:'../paginasFunciones/funcionesTesoreria.php',
                                                                                                    params:	{
                                                                                                                funcion:24,
                                                                                                                idPlanPagos:registro.data.id,
                                                                                                                idConcepto:gE('id').value
                                                                                                            }
                                                                                                }
                                                                                            );
                                	}
                                    else
                                    {
                                    	
                                        var cadObj=arrConfiguracionPlanPagos[pos][1];
                                        cadObj=cadObj.replace('"aplicaPlantel":"1"','"aplicaPlantel":true');
                                        cadObj=cadObj.replace('"aplicaPlantel":"0"','"aplicaPlantel":false');
                                        
                                        var obj=eval('['+cadObj+']');
                                        var o;
                                        gEx('gridConfiguracionPlanEstudio').getStore().removeAll();
                                        for(x=0;x<obj[0].planteles.length;x++)
                                        {
                                        	o=obj[0].planteles[x];
                                            r=new regConfAvanzada(	
                                            						{
                                                                    	plantel:o.plantel,
                                                                        nombrePlantel:o.nombrePlantel,
                                                                        aplicaPlantel:o.aplicaPlantel,
                                                                        valorReferencia:o.valorReferencia,
                                                                        tipoValorReferencia:o.tipoValorReferencia
                                            						}
                                            					);
                                            gEx('gridConfiguracionPlanEstudio').getStore().add(r);
                                        }
                                        
										                                        
                                        
                                    }                                                            
                                                                                            
                                }
    					)
    var gridPlantelesConfiguracion=crearGridPlantelesConfiguracion();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Plan de pagos:'
                                                        },
                                                        cmbPlanPagosConf,
                                                        gridPlantelesConfiguracion

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Configuraci&oacute;n avanzada de Plan de Pagos',
										width: 760,
										height:370,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var x;
                                                                        var fila;
                                                                        var o;
                                                                        var arrPlanteles='';
                                                                        var aplicaPlantel=0;
                                                                        var aplicaDescuento=0;
                                                                        for(x=0;x<gridPlantelesConfiguracion.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gridPlantelesConfiguracion.getStore().getAt(x);
                                                                            aplicaPlantel=0;
                                                                            if(fila.get('aplicaPlantel')==true)
                                                                            {
                                                                            	aplicaPlantel=1;
                                                                                   
                                                                               
                                                                             }
                                                                             
                                                                           
                                                                             
                                                                           
                                                                            if(((fila.data.valorReferencia=='')||(fila.data.valorReferencia=='0'))&&(fila.data.tipoValorReferencia!=''))
                                                                            {
                                                                                function resp()
                                                                                {
                                                                                    gridPlantelesConfiguracion.startEditing(x,3);
                                                                                }
                                                                                msgBox('Debe especificar el valor de referencia a aplicar',resp);
                                                                                return;
                                                                                
                                                                            }
                                                                            if((fila.data.tipoValorReferencia=='')&&(fila.data.valorReferencia!='')&&(fila.data.valorReferencia!='0'))
                                                                            {
                                                                                function resp2()
                                                                                {
                                                                                    gridPlantelesConfiguracion.startEditing(x,4);
                                                                                }
                                                                                msgBox('Debe especificar el tipo del valor de referencia a aplicar',resp2);
                                                                                return;
                                                                            }
                                                                            
                                                                             
                                                                             
                                                                            o='{"plantel":"'+fila.data.plantel+'","nombrePlantel":"'+fila.data.nombrePlantel+'","aplicaPlantel":"'+aplicaPlantel+
                                                                            '","valorReferencia":"'+fila.data.valorReferencia+'","tipoValorReferencia":"'+fila.data.tipoValorReferencia+'"}';
                                                                            if(arrPlanteles=='')
                                                                                arrPlanteles=o;
                                                                            else
                                                                                arrPlanteles+=','+o;
                                                                        	
                                                                        }
                                                                        var cadObj='{"idConcepto":"'+gE('id').value+'","idPlanPagos":"'+cmbPlanPagosConf.getValue()+'","planteles":['+arrPlanteles+']}';
                                                                        
                                                                        
                                                                        if(gE('id').value!='-1')
                                                                        {
                                                                        	function funcAjax()
                                                                            {
                                                                                var resp=peticion_http.responseText;
                                                                                arrResp=resp.split('|');
                                                                                if(arrResp[0]=='1')
                                                                                {
                                                                                    ventanaAM.close();
                                                                                }
                                                                                else
                                                                                {
                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                }
                                                                            }
                                                                            obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=25&cadObj='+cadObj,true);

                                                                        }
                                                                        else
                                                                        {
                                                                            var oPlanPagos=[cmbPlanPagosConf.getValue(),cadObj];
                                                                            
                                                                            var pos=existeValorMatriz(arrConfiguracionPlanPagos,arrConfiguracionPlanPagos,0,true);
                                                                            if(pos==-1)
                                                                                arrConfiguracionPlanPagos.push(oPlanPagos);
                                                                            else
                                                                                arrConfiguracionPlanPagos[pos][1]=cadObj;
                                                                            ventanaAM.close();
                                                                        }
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}

function crearGridPlantelesConfiguracion()
{
	var cmbTipoDescuentos=crearComboExt('cmbTipoDescuentos',arrTipoDescuentos);
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'plantel'},
		                                                {name: 'nombrePlantel'},
		                                                {name:'aplicaPlantel'},
		                                                {name: 'valorReferencia'},
                                                        {name: 'tipoValorReferencia'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombrePlantel', direction: 'ASC'},
                                                            groupField: 'nombrePlantel',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='24';
                                        
                                    }
                        )   
       
       
	var checkColumnAP = new Ext.grid.CheckColumn	(
                                                        {
                                                           header: 'Aplica en plantel',
                                                           dataIndex: 'aplicaPlantel',
                                                           width: 100
                                                        }
                                                    );
	                                                
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Plantel',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'nombrePlantel'
                                                            },
                                                            checkColumnAP,
                                                            
                                                            {
                                                                header:'Valor referencia',
                                                                width:100,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'valorReferencia',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(val=='')
                                                                            	return '';
                                                                        	if(registro.data.tipoValorReferencia=='1')
                                                                            {
                                                                            	return Ext.util.Format.number(val,'0.00')+' %';
                                                                            }
                                                                            else
                                                                            	return Ext.util.Format.usMoney(val);
                                                                        },
                                                                editor:	{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:true,
                                                                            allowNegative:false
                                                                		}
                                                            },
                                                            {
                                                                header:'Tipo valor de referencia',
                                                                width:140,
                                                                sortable:true,
                                                                dataIndex:'tipoValorReferencia',
                                                                editor:cmbTipoDescuentos,
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return formatearValorRenderer(arrTipoDescuentos,val);
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridConfiguracionPlanEstudio',
                                                                store:alDatos,
                                                                x:10,
                                                                y:40,
                                                                height:240,
                                                                width:720,
                                                                frame:true,
                                                                cm: cModelo,
                                                                clicksToEdit:1,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:	[checkColumnAP],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		
        
        tblGrid.on('beforeedit',function(e)
        						{
                                	if(e.field=='aplicaPlantel')
                                    {
                                    	e.grid.stopEditing(); 
                                    }
                                	
                                    if((e.field=='valorReferencia')||(e.field=='tipoValorReferencia'))
                                    {
                                    	if(e.record.data.aplicaPlantel!=true)
                                        {
                                        	e.cancel=true;
                                        }
                                    }
                                }
        				
        			)  
        
        tblGrid.on('afteredit',function(e)
        						{
                                	if(e.field=='aplicaPlantel')
                                    {
                                    	if(!e.value)
                                        {
                                        	e.record.set('valorReferencia','');
                                            e.record.set('tipoValorReferencia','');

                                        }
                                    }
                                }
        				
        			)                                                        
        return 	tblGrid;	
}