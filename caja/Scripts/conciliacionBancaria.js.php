<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idConcepto,nombreConcepto FROM 598_perfilesMovimientos";
	$arrMovimientos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago";
	$arrTipoPago=$con->obtenerFilasArreglo($consulta);
	
	$arrTipoPresupuesto="[]";
	
	$consulta="SELECT idBanco,nombreBanco  FROM 6000_bancos WHERE idBanco IN (SELECT DISTINCT s.idBanco FROM 6004_cuentasBancarias c,6001_sucursales s WHERE s.idSucursal=c.idSucursal)";
	$arrBancos=$con->obtenerFilasArreglo($consulta);
	
?>

var arrMovimientos=<?php echo $arrMovimientos?>;
var arrTipoPago=<?php echo $arrTipoPago?>;
var arrTipoPresupuesto=<?php echo $arrTipoPresupuesto?>;
var arrBancos=<?php echo $arrBancos?>;
var tipoEdicion=1;
Ext.onReady(inicializar);

function inicializar()
{
	var cmbBanco=crearComboExt('cmbBanco',arrBancos,0,0,200);
    cmbBanco.on('select',function(cmb,registro)
    					{
                        	function funcAjax()
                            {
                                var resp=peticion_http.responseText;
                                arrResp=resp.split('|');
                                if(arrResp[0]=='1')
                                {
                                    gEx('cmbCuenta').getStore().loadData(eval(arrResp[1]));
                                }
                                else
                                {
                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=106&idBanco='+registro.get('id'),true);
                        }
    			);

	var objConf={};
    objConf.campoValor='idCuenta';
    objConf.campoEtiqueta='nombreEtiqueta';
    objConf.arrCampos=	[
                            {"name":"idCuenta"},
                            {"name":"noCuenta"},
                            {"name":"descripcionCuenta"},
                            {"name":"clabeInterbancaria"},
                            {"name":"nombreSucursal"},
                            {"name":"nombreEtiqueta"}
                        ]
    objConf.confVista='<tpl for="."><div class="search-item"><b>[{noCuenta}]</b> - {descripcionCuenta} <br />(<b>CLABE:</b> {clabeInterbancaria}, <b>Sucursal:</b> {nombreSucursal})</div></tpl>';                
    var cmbCuenta=crearComboExt('cmbCuenta',[],0,0,500,objConf);
    cmbCuenta.on('select',function(cmb,registro)
    					{
                        	function funcAjax()
                            {
                                var resp=peticion_http.responseText;
                                arrResp=resp.split('|');
                                if(arrResp[0]=='1')
                                {
                                	gEx('btnAgregar').enable();
                                    gEx('btnAgregar2').enable();
                                    gEx('grid_1').getStore().loadData(eval(arrResp[1]));
                                }
                                else
                                {
                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=107&idCuenta='+registro.get('idCuenta'),true);
                        }
    			);
	var gridCaja=crearGridCaja();

	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            title:'<span class="letraRojaSubrayada8" style="font-size:14px"><b>Conciliación bancaria</b></span>',
                                            tbar:	[
                                                        {
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8"><b>Banco:</b></span>&nbsp;&nbsp;'
                                                           
                                                        },
                                                        cmbBanco,'-',
                                                        {
                                                            x:10,
                                                            y:10,
                                                            html:'<span class="letraRojaSubrayada8"><b>Cuenta:</b></span>&nbsp;&nbsp;'
                                                           
                                                        },
                                                        cmbCuenta
                                                        
                                                    ],
                                            items:	[
                                           				gridCaja
	                                           		]
                                        }
                                     ]
						}
                    )  
}

function crearGridCaja()
{
	dsDatos=[];
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'filaEditor',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:1
                                                    }
                                                );
	editorFila.on('beforeedit',funcEditorFilaBeforeEditGrid)
	editorFila.on('afteredit',funcEditorFilaAfterEditGrid);                                                
    editorFila.on('validateedit',funcEditorValidaGrid);
    editorFila.on('canceledit',funcEditorCancelEditGrid);
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                   	{name: 'idMovimiento'},
                                                                    {name: 'fechaMovimiento',dateFormat:'Y-m-d',type:'date'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'deposito'},
                                                                    {name: 'retiro'},
                                                                    {name: 'conciliado'},
                                                                    {name: 'referencia'},
                                                                    {name: 'editable'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	

	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				
                                                                        {type: 'date', dataIndex: 'fechaMovimiento'}
                                                                        
                                                                        
                                                                    ]
                                                    }
                                                );   
                                                
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=96;
                                    }
                        ) 
                                                        
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 100,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Fecha de movimiento<span class="letraRoja">*</span>',
															width:150,
															sortable:true,
                                                            editor:{xtype:'datefield'},
															dataIndex:'fechaMovimiento',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(val)
	                                                                    	return val.format("d/m/Y");
                                                                    }
														},
                                                        {
															header:'Concepto<span class="letraRoja">*</span>',
															width:300,
															sortable:true,
															dataIndex:'descripcion',
                                                            editor:{xtype:'textfield'},
                                                            renderer:mostrarValorDescripcion
														},
                                                         {
															header:'No. Referencia',
															width:180,
															sortable:true,
                                                            css:'text-align:right !important;',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false},
															dataIndex:'referencia'
														},
                                                        {
															header:'Dep&oacute;sito',
															width:130,
															sortable:true,
															dataIndex:'deposito',
                                                            renderer:'usMoney',
                                                            editor:{id:"txtDeposito",xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            css:'text-align:right !important;',
                                                            summaryType:'sum'
														},
														{
															header:'Retiro',
															width:130,
                                                            dataIndex:'retiro',
															sortable:true,
                                                            editor:{id:"txtRetiro",xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            css:'text-align:right !important;',
															renderer:'usMoney',
                                                            summaryType:'sum'
														},
                                                        {
															header:'Conciliado',
															width:120,
															sortable:true,
															dataIndex:'conciliado',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return 'Sí';
                                                                    }
														}
													]
												);
                                                
	var summary = new Ext.ux.grid.GridSummary();
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'grid_1',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                          
                                                            stripeRows :true,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAgregar',
                                                                            disabled:true,
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar movimiento de Dep&oacute;sito',
                                                                            handler:function()
                                                                            		{
                                                                                    	tipoEdicion=1;
                                                                                        var arrCampos=	[
                                                                                                           	{name: 'idMovimiento'},
                                                                                                            {name: 'fechaMovimiento'},
                                                                                                            {name: 'descripcion'},
                                                                                                            {name: 'deposito'},
                                                                                                            {name: 'retiro'},
                                                                                                            {name: 'conciliado'},
                                                                                                            {name: 'referencia'},
                                                                                                            {name: 'editable'}
                                                                                                        ]
                                                                                                        
                                                                                        var registroGrid=crearRegistro(arrCampos);
                                                                                        var r=new registroGrid	(
                                                                                        							{
                                                                                                                    	idMovimiento:'-1',
                                                                                                                        fechaMovimiento:'',
                                                                                                                        descripcion:'',
                                                                                                                        deposito:'',
                                                                                                                        retiro:'',
                                                                                                                        conciliado:'1',
                                                                                                                        referencia:'',
                                                                                                                        editable:'1'
                                                                                                                    }
                                                                                        						)
                                                                                        
                                                                                       
                                                                                        
                                                                                        editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAgregar').disable();
                                                                                        Ext.getCmp('btnAgregar2').disable();	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnAgregar2',
                                                                            disabled:true,
                                                                        	icon:'../images/cancel_round.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar movimiento de Retiro',
                                                                            handler:function()
                                                                            		{
                                                                                    	tipoEdicion=2;
                                                                                        var arrCampos=	[
                                                                                                           	{name: 'idMovimiento'},
                                                                                                            {name: 'fechaMovimiento'},
                                                                                                            {name: 'descripcion'},
                                                                                                            {name: 'deposito'},
                                                                                                            {name: 'retiro'},
                                                                                                            {name: 'conciliado'},
                                                                                                            {name: 'referencia'}
                                                                                                        ]
                                                                                                        
                                                                                        var registroGrid=crearRegistro(arrCampos);
                                                                                        var r=new registroGrid	(
                                                                                        							{
                                                                                                                    	idMovimiento:'-1',
                                                                                                                        fechaMovimiento:'',
                                                                                                                        descripcion:'',
                                                                                                                        deposito:'',
                                                                                                                        retiro:'',
                                                                                                                        conciliado:'1',
                                                                                                                        referencia:''
                                                                                                                    }
                                                                                        						)
                                                                                        
                                                                                       
                                                                                        
                                                                                        editorFila.stopEditing();
                                                                                        tblGrid.getStore().add(r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                        Ext.getCmp('btnAgregar').disable();
                                                                                        Ext.getCmp('btnAgregar2').disable();	
                                                                                    }
                                                                            
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	icon:'../images/table_row_insert.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            text:'Importar LayOut de dep&oacute;sito',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            bbar:[paginador,'-',
                                                            			{
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Marcar movimiento como Conciliado',
                                                                            handler:function()
                                                                            		{
                                                                                    	function resp()
                                                                                        {
                                                                                        	
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer marcar los movimientos seleccionados como conciliados?',resp);
                                                                                        return;
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/cross.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Marcar movimiento como No Conciliado',
                                                                            handler:function()
                                                                            		{
                                                                                    	function resp2()
                                                                                        {
                                                                                        
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer marcar los movimientos seleccionados como NO conciliados?',resp2);
                                                                                        return;
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	x:10,
                                                                            y:10,
                                                                            hidden:true,
                                                                            html:'<span class="letraRojaSubrayada8"><b>Saldo Actual Cuenta:</b></span>&nbsp;&nbsp;<span><b>$ 0.00</b></span>'
                                                                           
                                                                        }
                                                            	],
                                                            plugins:[filters,summary,editorFila],
                                                            columnLines : true
                                                           
                                                        }
                                                    );
	 tblGrid.nuevoRegistro=false;   
	tblGrid.on('beforeedit',function(e)
    						{
                            	if(e.record.data.editable=='0')
                                	e.cancel=true;
                            }
    			)                                                                                                                            
	return 	tblGrid;		
}


function funcEditorFilaBeforeEditGrid(rowEdit,fila)
{
	var registro=gEx('grid_1').getStore().getAt(fila);
    
    if(tipoEdicion==1)
    {
    	gEx('txtDeposito').setReadOnly(false);
        gEx('txtRetiro').setReadOnly(true);
        gEx('txtRetiro').setValue(0);
    }
    else
    {
		gEx('txtDeposito').setReadOnly(true);
        gEx('txtRetiro').setReadOnly(false);
        gEx('txtDeposito').setValue(0);
    }
    
    if(registro.get('editable')=='0')
    	return false;
}

function funcEditorFilaAfterEditGrid(rowEdit,obj,registro,nFila)
{
	
}



function funcEditorValidaGrid(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_1';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
    var objGrid;
    var atributo;
	for(x=0;x<nColumnas;x++)
	{
    	dataIndex=cm.getDataIndex(x);
		if((dataIndex!='')&&(dataIndex!=undefined))
        {
            valor=(eval('obj.'+dataIndex));
            if(cm.getColumnHeader(x).indexOf('*')!=-1)
            {
                if(valor=='')
                {
                    function funcResp()
                    {
                        var ctrl=gEx('editor_'+dataIndex);
                        ctrl.focus();
                    }
                    msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
                    return false;
                }
            }	
		}
	}
    var deposito=0;
    var retiro=0;

    if(tipoEdicion==1)
    {
    	if((obj.deposito=='')||(obj.deposito=='0'))
        {
        	function resp10()
            {
            	gEx('txtDeposito').focus(true,200);
            }
            msgBox('Debe indicar el monto del dep&oacute;sito a registrar',resp10);
            return false;
        }
        deposito=obj.deposito;
    }
    else
    {
		if((obj.retiro=='')||(obj.retiro=='0'))
        {
        	function resp11()
            {
            	gEx('txtRetiro').focus(true,200);
            }
            msgBox('Debe indicar el monto del retiro a registrar',resp11);
            return false;
        }
        retiro=obj.retiro;
    }
    var cmbCuenta=gEx('cmbCuenta');
    objGrid='{"idCuentaBancaria":"'+cmbCuenta.getValue()+'","referencia":"'+obj.referencia+'","tipoMovimiento":"'+tipoEdicion+'","descripcion":"'+cv(obj.descripcion)+'","fechaMovimiento":"'+obj.fechaMovimiento.format('Y-m-d')+
    		'","deposito":"'+deposito+'","retiro":"'+retiro+'"}';
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            registro.data.editable=0;
            registro.data.idMovimiento=arrResp[1];
            Ext.getCmp('btnAgregar').enable();
            Ext.getCmp('btnAgregar2').enable();
            grid.nuevoRegistro=false;
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=88&cadObj='+objGrid,true);    
        
	
	return true;
}

function funcEditorCancelEditGrid(rowEdit,cancelado)
{
	
	var idGrid='grid_1';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnAgregar').enable();
    Ext.getCmp('btnAgregar2').enable();
    var copiaRegistro=grid.copiaRegistro;
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;
    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
    }
	grid.nuevoRegistro=false;
}



function cancelarMovimiento()
{
	var grid_1=gEx('grid_1');
    grid_1.getStore().removeAt(grid_1.getStore().getCount()-1);
	cerrarVentana();
}

function asignarFolioMovimimiento(folio,idMovimiento)
{
	var grid=gEx('grid_1');
   	var fila= grid.getStore().getAt(grid.getStore().getCount()-1);
   	fila.set('movimiento',folio);
    fila.set('idMovimiento',idMovimiento);
    cerrarVentana();
}

function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='86';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                    	return Ext.util.Format.usMoney(subTotal)
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                        var total=subTotal+parseFloat(registro.get('iva'));
                                                                    	return Ext.util.Format.usMoney(total)
                                                                    },
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Contenedor',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            
                                                            dataIndex:'contenedor',
                                                            hideable:true
														},
                                                        {
															header:'Unidad de medida',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'unidadMedida',
                                                            hideable:true
														},
                                                        {
															header:'Presentacion',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'presentacion',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:230,
                                                            collapsible:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                           
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}

function mostrarVentanaRegistroPago(fila)
{
	var cmbFormaPago=crearComboExt('cmbFormaPago',arrTipoPago,300,85,200);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Proveedor: '
                                                        },
                                                        {
                                                        	x:120,
                                                            y:5,
                                                            width:400,
                                                            xtype:'textfield',
                                                            value:fila.get('txtRazonSocial2'),
                                                            readOnly:true
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Concepto de pago:'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:35,
                                                            xtype:'textarea',
                                                            id:'txtConcepto',
                                                            readOnly:true,
                                                            value:fila.get('concepto'),
                                                            width:400,
                                                            height:40
                                                        },
                                                        {
                                                        	x:10,
                                                            y:90,
                                                            html:'Monto de operaci&oacute;n:'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:120,
                                                            y:90,
                                                            html:'<span class="letraRojaSubrayada8">'+Ext.util.Format.usMoney(fila.get('totalFactura'))+'</span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:90,
                                                            html:'Forma de pago:'
                                                        },
                                                        cmbFormaPago

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Pago a proveedor',
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		abrirVentanaIFrame(727,650,250,'Pago  a proveedor por cheque');
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}
