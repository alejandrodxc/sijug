<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


var arrCtrlRegistradosCombEnter=[];

var panelFormaPagoActual=null;
var arrPanelesFormaPago;
var arrFormaPagoCliente;
var cambioUltimaVenta=0;

function mostrarVentanaPago()
{

	var lblAcceso='';
    arrPanelesFormaPago=[];
    arrFormaPagoCliente=[];
    var posFormaPago;
    var posCliente=existeValorMatriz(arrTipoCliente,tipoCliente,0,true);
    var fCliente=arrTipoCliente[posCliente];
    var fPerfil;
    var x;
    var f;
    var panel;
    for(x=0;x<fCliente[4].length;x++)
    {
    	f=fCliente[4][x][0];
        posFormaPago=existeValorMatriz(arrFormasPagoPerfil,f,0,true);
        if(posFormaPago!=-1)
        {
            fPerfil=arrFormasPagoPerfil[posFormaPago];
            lblAcceso+='['+fPerfil[3]+'] '+fPerfil[1]+'&nbsp;&nbsp;&nbsp;';
            eval("panel="+fPerfil[2]+"("+fPerfil[0]+");");
            arrPanelesFormaPago.push(panel);
            arrFormaPagoCliente.push(fPerfil);
		}
    }
    
	var total=parseFloat(normalizarValor(gE('lblTotal').innerHTML));
    if(total==0)
    {
    	function respTmp()
        {
        	gEx('txtClave').focus();
        }
    	msgBox('Debe indicar almenos un producto para cerrar la operaci&oacute;n',respTmp);
        return;
        
    }
	formaPago=1;
	ignorarGlobal=true;
    
    var arrItems=	[
                      {
                          x:10,
                          y:0,
                          xtype:'label',
                          html:'<span style="color:#063"><b>'+lblAcceso+'</b></span>'
                      },
                      {
                          xtype:'label',
                          x:10,
                          y:45,
                          html:'<span style="font-size:18px; color:#000; font-weight:bold">Monto total:</span>'
                      },
                      {
                          xtype:'label',
                          x:200,
                          y:45,
                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblCobroTotal"></span>'
                      },
                      {
                          xtype:'label',
                          x:435,
                          y:45,
                          html:'<span style="font-size:18px; color:#000; font-weight:bold">Saldo a favor:</span>'
                      },
                      {
                          xtype:'label',
                          x:595,
                          y:45,
                          html:'<span style="font-size:18px; color:#900; font-weight:bold" id="lblSaldoFavor"></span>'
                      }

                  ];
    
    
    for(x=0;x<arrPanelesFormaPago.length;x++)
    {
    	arrPanelesFormaPago[x].hide();
    	arrItems.push(arrPanelesFormaPago[x]);
    }
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	arrItems
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vCierreOperaciones',
										title: 'Registrar operaci&oacute;n',
										width: 800,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 100,
																fn : function() 
																{
                                                                	
                                                                    
																}
															}
												},
										buttons:	[
														{
                                                        	id:'btnCerrarOperacion',
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															listeners: 	{
                                                            				click:	function()
                                                                                    {
                                                                                        var arrProductos='';
                                                                                        var obj='';
                                                                                        var cadObj='';
                                                                                        var grid=gEx('grid');
                                                                                        var x;
                                                                                        var fila;
                                                                                        var totalDescuento=0;
                                                                                        var descuento;
                                                                                        
                                                                                        for(x=0;x<grid.getStore().getCount();x++)
                                                                                        {
                                                                                            fila=grid.getStore().getAt(x);
                                                                                            descuento=(parseFloat(fila.data.descuento)*parseFloat(fila.get('cantidad')));
                                                                                            obj='{"descripcion":"'+cv(fila.data.descripcion)+'","metaData":"'+cv((typeof(fila.data.metaData)=='undefined')?'':fila.data.metaData)+
                                                                                            	'","descuentoUnitario":"'+fila.data.descuento+'","descuento":"'+descuento+'","idRegistro":"'+fila.get('idRegistro')+'","cveProducto":"'+
                                                                                                fila.get('cveProducto')+'","costoUnitario":"'+fila.get('costoUnitario')+'","cantidad":"'+fila.get('cantidad')+
                                                                                                '","subtotal":"'+fila.get('subtotal')+'","iva":"'+fila.get('iva')+'","total":"'+fila.get('total')+'","tipoConcepto":"'+
                                                                                                fila.get('tipoConcepto')+'","idProducto":"'+fila.get('idProducto')+
                                                                                                '","dimensiones":"'+fila.get('dimensiones')+'","tipoMovimiento":"'+fila.get('tipoMovimiento')+'","porcentajeIVA":"'+
                                                                                                fila.get('porcentajeIVA')+'","llave":"'+fila.get('llave')+'","unidadMedida":"'+fila.get('unidadMedida')+'","costoUnitarioOriginal":"'+
                                                                                                fila.get('precioUnitarioOriginal')+'","productoConExistencia":"'+fila.get('productoConExistencia')+'"}';
                                                                                            if(arrProductos=='')
                                                                                                arrProductos=obj;
                                                                                            else
                                                                                                arrProductos+=','+obj;
                                                                                            totalDescuento+=descuento;
                                                                                        }
                                                                                        
                                                                                        var dCompra='';
                                                                                    
                                                                                        var cadNotaCredito='';
                                                                                        var f;
                                                                                        var o;
                                                                                        for(x=0;x<notaCredito.length;x++)
                                                                                        {
                                                                                            f=notaCredito[x];
                                                                                            o='{"idNota":"'+f.data.idNotaCredito+'"}';
                                                                                            if(cadNotaCredito=='')
                                                                                                cadNotaCredito=o;
                                                                                            else
                                                                                                cadNotaCredito+=','+o;
                                                                                        }
                                                                                     	var montoNota=normalizarValor(gE('lblSaldoFavor').innerHTML);
                                                                                        
                                                                                        var aFormaPago=panelFormaPagoActual.id.split('_');
                                                                                        var tipoOperacion=1;
                                                                                        var datos=panelFormaPagoActual.getDatosVenta();
                                                                                        
                                                                                        if(datos)
                                                                                        {
                                                                                            cadObj='{"totalDescuento":"'+totalDescuento+'","montoNota":"'+montoNota+'","idPedido":"'+idPedidoActivo+'","arrNotasCredito":['+cadNotaCredito+
                                                                                            		'],"tipoCliente":"'+tipoCliente+'","idCliente":"'+idCliente+'","formaPago":"'+aFormaPago[1]+'","datosCompra":"'+cv(datos.cadObj)+
                                                                                                    '","desgloceFormaPago":'+datos.desgloceFormaPago+',"total":"'+normalizarValor(gE('lblTotal').innerHTML)+'","subtotal":"'+normalizarValor(gE('lblSubtotal').innerHTML)+
                                                                                                    '","iva":"'+normalizarValor(gE('lblIVA').innerHTML)+'","idCaja":"'+gE('idCaja').value+'","arrProductos":['+arrProductos+']}';   
                                                                                           
                                                                                           if(typeof(panelFormaPagoActual.cerrarOperacionCaja)=='undefined')
                                                                                           		cerrarOperacionCaja(tipoOperacion,aFormaPago[1],cadObj,ventanaAM);
                                                                                           else
	                                                                                           	panelFormaPagoActual.cerrarOperacionCaja(tipoOperacion,aFormaPago[1],cadObj,ventanaAM);
                                                                                           
                                                                                            
                                                                                      	}  
                                                                                        
                                                                                    }
                                                                        }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
                                                                        ignorarGlobal=false;
                                                                        gEx('txtClave').focus();
																	}
														}
													]
									}
								);
	ventanaAM.show();  
                                     
	var saldoFavor=0;
    var lblTotalAdeudo=gE('lblTotalAdeudo').innerHTML;
    var mTotal=parseFloat(normalizarValor(lblTotalAdeudo));
    if(mTotal<0)
    {
    	saldoFavor=mTotal*-1;
        mTotal=0;
    }
    else
    {
		saldoFavor=0;

    }
    gE('lblCobroTotal').innerHTML=Ext.util.Format.usMoney(mTotal);
	gE('lblSaldoFavor').innerHTML=Ext.util.Format.usMoney(saldoFavor);
    
    if(arrPanelesFormaPago.length>0)
    	arrPanelesFormaPago[0].mostrarPanel();
}



function ajustarVentanaPanel(panel)
{
	var vCierreOperaciones=gEx('vCierreOperaciones');
    if(panel)
    {
    	vCierreOperaciones.setHeight(panelFormaPagoActual.getHeight()+panel.alto);
        if(panel.ancho)
        {
        	vCierreOperaciones.setWidth(panelFormaPagoActual.getWidth()+panel.ancho);
        }
    }
    else
	    vCierreOperaciones.setHeight(panelFormaPagoActual.getHeight()+170);
        
   vCierreOperaciones.show();    
        
}

function ocultarPanelesFormaPago()
{
	var x;
    for(x=0;x<arrPanelesFormaPago.length;x++)
    {
    	arrPanelesFormaPago[x].ocultarPanel();
    }
    
}

function mostrarPanelFormaPago(iForma)
{
	var x;
   
    ocultarPanelesFormaPago();
    
    for(x=0;x<arrPanelesFormaPago.length;x++)
    {
    	if(arrPanelesFormaPago[x].id=='panel_'+iForma)
        {
	    	arrPanelesFormaPago[x].mostrarPanel();
        	return;
        }
    }
}

function registrarEventosCobroHTML()
{
	var x;
    var fila;
    var cadAux;
    for(x=0;x<arrFormaPagoCliente.length;x++)	
    {
    	fila=arrFormaPagoCliente[x];
        
        cadAux="$('.btnEvento').ClassyKeyboard('"+fila[4]+"', function(event) "+
                                                            "{"+
                                                                "event.originalEvent.keyCode=0;"+
                                                                "event.originalEvent.returnValue=false;"+
                                                                "if(event.originalEvent.preventDefault)"+
																		"event.originalEvent.preventDefault();"+
                                                                "mostrarPanelFormaPago("+fila[0]+");"+
                                                            "}"+
                                				")";
		
        eval(cadAux);
    }
   	var arrCtrlAux=$('.btnEvento');
    for(x=0;x<arrCtrlAux.length;x++)
    {
    	if(existeValorArreglo(arrCtrlRegistradosCombEnter,arrCtrlAux[x].id)==-1)
        {
        	 $('#'+arrCtrlAux[x].id).ClassyKeyboard('ctrl+enter', function(event)
                                                 {
                                                		event.originalEvent.keyCode=0;
                                                    	event.originalEvent.returnValue=false;
                                                        if(event.originalEvent.preventDefault)
															event.originalEvent.preventDefault();
                                                        gEx('btnCerrarOperacion').fireEvent('click',gEx('btnCerrarOperacion'));
                                                 }
                                	)
        	arrCtrlRegistradosCombEnter.push(arrCtrlAux[x].id);
        }
    }
    
   
    
}