<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var conceptosCobro=[['0','Productos de Farmacia'],['1','Comprobaci&oacute;n de gastos'],['2','Servicios'],['3','Otros ingresos']];
var arrTipoComprobante=[['1','Factura'],['2','Nota'],['3','Boletos']];	
Ext.onReady(inicializar);

function inicializar()
{
	var gridCaja=crearGridCaja();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            title:'<span class="letraRojaSubrayada8" style="font-size:14px">Caja 1 Comprobación de gastos</span>&nbsp;&nbsp;<span style="font-size:14px; color:#000">[Usuario Rool Gral.]</span>',
                                            items:	[
                                           				gridCaja,
                                                        {
                                                        	region:'south',
                                                        	xtype:'panel',
                                                            height:250,
                                                            layout:'absolute',
                                                           
                                                            border:true,
                                                            frame:true,
                                                            items:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<img width="170" height="200" src="../Usuarios/verFoto.php?Id=-1" />'
                                                                        },
                                                            			{
                                                                        	x:210,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"> <b>Clave empleado: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:305,
                                                                            y:25,
                                                                            xtype:'textfield',
                                                                            width:120
                                                                        },
                                                                        {
                                                                        	x:210,
                                                                            y:60,
                                                                            xtype:'label',
                                                                             html:'<span style="color:#000"> <b>Empleado: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:305,
                                                                            y:55,
                                                                            xtype:'textfield',
                                                                            width:300
                                                                        },
                                                                         {
                                                                        	x:650,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>Subtotal: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:830,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"><b><span id="lblSubtotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:650,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>IVA: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:830,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblIVA">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:650,
                                                                            y:90,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>Total comprobaci&oacute;n: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:830,
                                                                            y:90,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblTotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:210,
                                                                            y:100,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            icon:'../images/icon_big_tick.gif',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            text:'Registrar comprobaci&oacute;n',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        
                                                                                                       
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        },
                                                                        {
                                                                        	x:210,
                                                                            y:150,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            icon:'../images/cross.png',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            text:'Cancelar cmprobaci&oacute;n',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        
                                                                                                       
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                            		]
                                                        }
	                                           		]
                                        }
                                     ]
						}
                    )  
		calcularTotal();                    
}

function crearGridCaja()
{
	//var dsDatos=[['00001','Hospedaje hotel','850','3','2550','408','2958','1'],['00002','Transportes','250','1','250','0','250','3']];
    var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'cveProducto'},
                                                                {name: 'concepto'},
                                                                {name: 'costoUnitario', type:'float'},
                                                                {name: 'cantidad'},
																{name: 'subtotal', type:'float'},                                                                
                                                                {name: 'iva', type:'float'},
                                                                {name: 'total', type:'float'},
                                                                {name: 'tipoComprobante'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
                                                       
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Clave',
															width:110,
															sortable:true,
                                                            hidden:false,
															dataIndex:'cveProducto'
														},
                                                        {
															header:'Concepto a comprobar',
															width:350,
															sortable:true,
															dataIndex:'concepto'
														},
                                                        
														{
															header:'Costo unitario',
															width:100,
															sortable:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney'
														},
                                                        
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'SubTotal',
															width:100,
															sortable:true,
															dataIndex:'subtotal',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:100,
															sortable:true,
															dataIndex:'iva',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Total',
															width:100,
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Tipo comprobante',
															width:100,
															sortable:true,
															dataIndex:'tipoComprobante',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoComprobante,val);
                                                                    }
                                                            
														},
                                                         {
															header:'Comprobante',
															width:100,
															sortable:true,
															dataIndex:'tipoComprobante',
                                                            renderer:function(val)
                                                            		{
                                                                    	return '<img src="../images/download.png" />';
                                                                    }
                                                            
														}
                                                        
													]
												);
                                                
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'grid',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            tbar:	[
                                                            			{
                                                                            xtype:'button',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Agregar concepto a comprobar',
                                                                            handler:function()
                                                                                    {
                                                                                        
                                                                                       
                                                                                    }
                                                                        },
                                                                        {
                                                                            xtype:'button',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Remover concepto a comprobar',
                                                                            handler:function()
                                                                                    {
                                                                                        
                                                                                       
                                                                                    }
                                                                        }
                                                            		]
                                                        }
                                                    );
	 tblGrid.nuevoRegistro=false;   
                                                                                                                             
	return 	tblGrid;		
}

function calcularTotal()
{
	var x;
    var grid=gEx('grid');
    var ivaTotal=0;
    var subtotal=0;
    var fila;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        ivaTotal+=fila.get('iva');
        subtotal+=fila.get('total');
    }
    
    gE('lblSubtotal').innerHTML=Ext.util.Format.usMoney(subtotal);    
    gE('lblIVA').innerHTML=Ext.util.Format.usMoney(ivaTotal);    
    gE('lblTotal').innerHTML=Ext.util.Format.usMoney(subtotal+ivaTotal);    
    
}
