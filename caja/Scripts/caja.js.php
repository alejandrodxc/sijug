<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__649_tablaDinamica,txtNomMovimiento FROM _649_tablaDinamica";
	$arrMovimientos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__651_tablaDinamica,txtTipopago FROM _651_tablaDinamica";
	$arrTipoPago=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoPresupuesto,tituloTipoP FROM 508_tiposPresupuesto ORDER BY tituloTipoP";
	$arrTipoPresupuesto=$con->obtenerFilasArreglo($consulta);
?>

var arrMovimientos=<?php echo $arrMovimientos?>;
var arrTipoPago=<?php echo $arrTipoPago?>;
var arrTipoPresupuesto=<?php echo $arrTipoPresupuesto?>;

Ext.onReady(inicializar);

function inicializar()
{
	var gridCaja=crearGridCaja();
    var gridDetalle=crearGridDetalle();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            title:'<span class="letraRojaSubrayada8">Pagos programados</span>',
                                            items:	[
                                           				gridCaja,
                                                        gridDetalle
	                                           		]
                                        }
                                     ]
						}
                    )  
                    
                    
}

function crearGridCaja()
{
	var dsDatos=[];
    
    
	var cmbTipoPago=crearComboExt('cmbTipoPago',arrTipoPago); 
   var cmbTipoPresupuesto=crearComboExt(cmbTipoPresupuesto,arrTipoPresupuesto); 
   
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idPresupuestoAutorizado',
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idContrarecibo'},
                                                        {name: 'noContraRecibo'},
                                                        {name: 'tipoMovimiento'},
                                                        {name: 'concepto'},
                                                        {name: 'noFactura', type :'int'},
                                                        {name: 'fechaRecepcion', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'txtRazonSocial2'},
                                                        {name: 'montoFactura'},
                                                        {name: 'ivaFactura'},
                                                        {name: 'totalFactura'},
                                                        {name: 'retencion'},
                                                        {name: 'fechaPago', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'descuento'},
                                                        {name: 'noPedido', type :'int'}
                                            ],
                                            root:'registros'
                                            
                                        }
                                      );
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesContabilidad.php'
                                                                                                  
				

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idContrarecibo', direction: 'ASC'},
                                                            groupField: 'tipoMovimiento',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        })     


	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'concepto'},
                                                                        {type: 'string', dataIndex: 'noFactura'},
                                                                        {type: 'string', dataIndex: 'txtRazonSocial2'},
                                                                        {type: 'date', dataIndex: 'fechaRecepcion'},
                                                                        {type: 'string', dataIndex: 'noPedido'},
                                                                        {type: 'date', dataIndex: 'fechaPago'},
                                                                        {type: 'list', dataIndex: 'tipoMovimiento', options:arrMovimientos, phpMode:true}
                                                                        
                                                                    ]
                                                    }
                                                );   
                                                
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                    	proxy.baseParams.funcion=96;
                                        
                                        
                                    }
                        ) 
                                                        
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: 100,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Tipo de movimiento',
															width:150,
															sortable:true,
                                                            hidden:true,
															dataIndex:'tipoMovimiento',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return formatearValorRenderer(arrMovimientos,val)
                                                                    }
														},
                                                        {
															header:'No. contrarecibo',
															width:100,
															sortable:true,
															dataIndex:'noContraRecibo'
														},
                                                        {
															header:'No. pedido',
															width:100,
															sortable:true,
															dataIndex:'noPedido'
														},
														{
															header:'No. factura',
															width:100,
															sortable:true,
															dataIndex:'noFactura'
														},
                                                        {
															header:'Fecha de recepci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'fechaRecepcion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if((val!='')&&(val!=null))
                                                                        {
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                                    }
														},
                                                        {
															header:'Concepto',
															width:250,
															sortable:true,
															dataIndex:'concepto'
														},
                                                        {
															header:'Proveedor',
															width:250,
															sortable:true,
															dataIndex:'txtRazonSocial2'
														},
                                                        {
															header:'Monto',
															width:120,
															sortable:true,
															dataIndex:'montoFactura',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:120,
															sortable:true,
															dataIndex:'ivaFactura',
                                                            renderer:'usMoney'
														},
                                                         {
															header:'Retenci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'retencion',
                                                            renderer:'usMoney'
														},
                                                         {
															header:'Descuento',
															width:120,
															sortable:true,
															dataIndex:'descuento',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Total',
															width:120,
															sortable:true,
															dataIndex:'totalFactura',
                                                            renderer:'usMoney'
														},
                                                       
                                                        {
															header:'Fecha de pago',
															width:120,
															sortable:true,
															dataIndex:'fechaPago',
                                                            renderer:function (val)
                                                            		{
                                                                    	if((val!='')&&(val!=null))
                                                                    		return val.format('d/m/Y');
                                                                    }
														}
													]
												);
                                                
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'grid_1',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            columnLines : true,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAdd_1',
                                                                        	icon:'../images/icon_big_tick.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:false,
                                                                            text:'Registrar pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                    	mostrarVentanaRegistroPago(fila);
                                                                                       
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	 tblGrid.nuevoRegistro=false;   
     tblGrid.getStore().load({url:'../paginasFunciones/funcionesContabilidad.php',
     							params:	{
                                              funcion:96,
                                              limit:100,
                                              start:0
                                          }})          
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	if(tblGrid.getSelectionModel().getSelections().length==1)
	                                                	gEx('gridDetallePedido').getStore().load({ url: '../paginasFunciones/funcionesAlmacen.php',params:{idPedido:registro.get('noPedido'),funcion:86}})
                                                }
    							)                                                                                                                           
	return 	tblGrid;		
}

function funcEditorFilaAfterEditGrid(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
    var registro=grid.getStore().getAt(nFila);
    var cmbSituacion=gEx('cmbSituacion');
    var valor=cmbSituacion.getRawValue();
    registro.set('status',valor);
}

function funcEditorFilaBeforeEditGrid(rowEdit,fila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
}

function funcEditorValidaGrid(rowEdit,obj,registro,nFila)
{
	var datosEditor=rowEdit.getId().split('_')	
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
    var objGrid;
    var atributo;
	for(x=0;x<nColumnas;x++)
	{
    	dataIndex=cm.getDataIndex(x);
		if((dataIndex!='')&&(dataIndex!=undefined))
        {
            valor=(eval('obj.'+dataIndex));
            if(cm.getColumnHeader(x).indexOf('*')!=-1)
            {
                if(valor=='')
                {
                    function funcResp()
                    {
                        var ctrl=gEx('editor_'+dataIndex);
                        ctrl.focus();
                    }
                    msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
                    return false;
                }
            }	
		}
	}
    objGrid='{"tipoMovimiento":"'+obj.tipoMovimiento+'","movimiento":"'+obj.movimiento+'","fechaMovimiento":"'+obj.fechaMovimiento.format('Y-m-d')+
    		'","tipoPago":"'+obj.tipoPago+'","monto":"'+obj.monto+'","recurso":"'+obj.recurso+'","situacion":"'+obj.idStatus+'"}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            if(arrResp[1]!='-1')
                abrirVentanaIFrame(arrResp[1],arrResp[2],arrResp[3],arrResp[4],objGrid);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesContabilidad.php',funcAjax, 'POST','funcion=88&tipoPago='+obj.tipoPago,true);        
	Ext.getCmp('btnDel_1').enable();
    Ext.getCmp('btnAdd_1').enable();
   	grid.nuevoRegistro=false;
	return true;
}

function funcEditorCancelEditGrid(rowEdit,cancelado)
{
	var datosEditor=rowEdit.getId().split('_')
	var idGrid='grid_'+datosEditor[1];
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnDel_1').enable();
    Ext.getCmp('btnAdd_1').enable();
    var copiaRegistro=grid.copiaRegistro;
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;
    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));
    }
	grid.nuevoRegistro=false;
}

function abrirVentanaIFrame(idFormulario,ancho,alto,titulo,objGrid)
{
    var width=parseInt(ancho)+60;
    var height=parseInt(alto)+120;
    var funcionGuardar=bE('guardarMovimiento(idRegPadre,'+idFormulario+',\''+bE(objGrid)+'\')');
	var url='../modeloPerfiles/registroFormulario.php?confReferencia=1&idRegistro=-1&idFormulario='+idFormulario+'&cPagina=sFrm=true&funcPHPEjecutarNuevo='+funcionGuardar+'&accionCancelar=window.parent.cancelarMovimiento()';
	
    $.fancybox({
    			'href'				: url,
				'title'    			: titulo,			
				'width'				: width,
				'height'			: height,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
                'modal':true
			});	
	$("#btnPrueba").click();
}

function cerrarVentana()
{
	$.fancybox.close();
}

function cancelarMovimiento()
{
	var grid_1=gEx('grid_1');
    grid_1.getStore().removeAt(grid_1.getStore().getCount()-1);
	cerrarVentana();
}

function asignarFolioMovimimiento(folio,idMovimiento)
{
	var grid=gEx('grid_1');
   	var fila= grid.getStore().getAt(grid.getStore().getCount()-1);
   	fila.set('movimiento',folio);
    fila.set('idMovimiento',idMovimiento);
    cerrarVentana();
}

function crearGridDetalle()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
		                                                {name:'marca'},
		                                                {name:'modelo'},
                                                        {name: 'cantidad',type:'int'},
                                                        {name: 'costoUnitario'},
                                                        {name: 'iva'},
                                                        {name: 'contenedor'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'presentacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	  
                                                                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='86';
                                    }
                        )   
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});              
	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
                                                    	new  Ext.grid.RowNumberer(),
                                                    	chkRow,
                                                        
													 	{
															header:'Producto',
															width:280,
															sortable:true,
															dataIndex:'nombreProducto',
                                                            hideable:true
                                                            
														},
														{
															header:'Marca',
															width:150,
                                                            align:'right',
															sortable:true,
															dataIndex:'marca',
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Modelo',
															width:180,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'modelo',
                                                           
                                                            hideable:true
														},
														{
															header:'Cantidad',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'cantidad',
                                                           
                                                            hideable:true
														},
                                                         {
															header:'Costo unitario',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'costoUnitario',
                                                            hideable:true
														},
                                                        {
															header:'Sub total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                    	return Ext.util.Format.usMoney(subTotal)
                                                                    },
                                                            hideable:true
														},
                                                        {
															header:'IVA',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:'usMoney',
                                                            dataIndex:'iva',
                                                            hideable:true
														},
                                                        
                                                        {
															header:'Total',
															width:110,
                                                            align :'right',
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var subTotal=parseFloat(registro.get('costoUnitario'))*parseFloat(registro.get('cantidad'));
                                                                        var total=subTotal+parseFloat(registro.get('iva'));
                                                                    	return Ext.util.Format.usMoney(total)
                                                                    },
                                                           
                                                            hideable:true
														},
                                                        {
															header:'Contenedor',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            
                                                            dataIndex:'contenedor',
                                                            hideable:true
														},
                                                        {
															header:'Unidad de medida',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'unidadMedida',
                                                            hideable:true
														},
                                                        {
															header:'Presentacion',
															width:160,
                                                            align :'right',
															sortable:true,
                                                            dataIndex:'presentacion',
                                                            hideable:true
														}
													]
												);
	
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridDetallePedido',
                                                            store:dsTablaRegistros2,
                                                            frame:true,
                                                            cm: cModelo,
                                                            columnLines:true,
                                                            region: 'south',
                                                            height:230,
                                                            collapsible:true,
                                                            sm:chkRow,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                           
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            )
                                                        }
												)   
	
	return tblGrid;   
}

function mostrarVentanaRegistroPago(fila)
{
	var cmbFormaPago=crearComboExt('cmbFormaPago',arrTipoPago,300,85,200);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Proveedor: '
                                                        },
                                                        {
                                                        	x:120,
                                                            y:5,
                                                            width:400,
                                                            xtype:'textfield',
                                                            value:fila.get('txtRazonSocial2'),
                                                            readOnly:true
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Concepto de pago:'
                                                        },
                                                        {
                                                        	x:120,
                                                            y:35,
                                                            xtype:'textarea',
                                                            id:'txtConcepto',
                                                            readOnly:true,
                                                            value:fila.get('concepto'),
                                                            width:400,
                                                            height:40
                                                        },
                                                        {
                                                        	x:10,
                                                            y:90,
                                                            html:'Monto de operaci&oacute;n:'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:120,
                                                            y:90,
                                                            html:'<span class="letraRojaSubrayada8">'+Ext.util.Format.usMoney(fila.get('totalFactura'))+'</span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:90,
                                                            html:'Forma de pago:'
                                                        },
                                                        cmbFormaPago

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Pago a proveedor',
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		abrirVentanaIFrame(727,650,250,'Pago  a proveedor por cheque');
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}
