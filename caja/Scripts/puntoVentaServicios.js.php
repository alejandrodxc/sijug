<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var conceptosCobro=[['0','Productos de Farmacia'],['1','Comprobaci&oacute;n de gastos'],['2','Servicios'],['3','Otros ingresos']];
	
Ext.onReady(inicializar);

function inicializar()
{
	var gridCaja=crearGridCaja();
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            title:'<span class="letraRojaSubrayada8" style="font-size:14px">Caja 1 Cobro de servicios</span>&nbsp;&nbsp;<span style="font-size:14px; color:#000">[Usuario Rool Gral.]</span>',
                                            items:	[
                                           				gridCaja,
                                                        {
                                                        	region:'south',
                                                        	xtype:'panel',
                                                            height:200,
                                                            layout:'absolute',
                                                           
                                                            border:true,
                                                            frame:true,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"> <b>Clave: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:65,
                                                                            y:25,
                                                                            xtype:'textfield',
                                                                            width:120
                                                                        },
                                                                        {
                                                                        	x:220,
                                                                            y:30,
                                                                            xtype:'label',
                                                                             html:'<span style="color:#000"> <b>Cantidad: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:285,
                                                                            y:25,
                                                                            xtype:'textfield',
                                                                            width:80
                                                                        },
                                                                         {
                                                                        	x:600,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>Subtotal: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:780,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"><b><span id="lblSubtotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:600,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>IVA: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:780,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblIVA">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:600,
                                                                            y:90,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#003; font-size:16px"> <b>Total: </b></span>'
                                                                        },
                                                                        {
                                                                        	x:780,
                                                                            y:90,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-size:16px"> <b><span id="lblTotal">'+Ext.util.Format.usMoney(0)+'</span></b></span>'
                                                                        },
                                                                        {
                                                                        	x:90,
                                                                            y:80,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            icon:'../images/icon_big_tick.gif',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            text:'Realizar venta',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        
                                                                                                       
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        },
                                                                        {
                                                                        	x:260,
                                                                            y:80,
                                                                            xtype:'panel',
                                                                            items:	[
                                                                            			{
                                                                                            x:0,
                                                                                            y:0,
                                                                                            width:140,
                                                                                            height:40,
                                                                                            xtype:'button',
                                                                                            icon:'../images/cross.png',
                                                                                            cls:'x-btn-text-icon',
                                                                                            hidden:false,
                                                                                            text:'Cancelar venta',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        
                                                                                                       
                                                                                                    }
                                                                                        }
                                                                                        
                                                                                        
                                                                            		]
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                            		]
                                                        }
	                                           		]
                                        }
                                     ]
						}
                    )  
		calcularTotal();                    
}

function crearGridCaja()
{
	var dsDatos=[['01-0210','Enteroscopia','1995','6',1,'1995',(1995*0.16),1995+(1995*0.16)]];

    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'cveProducto'},
                                                                {name: 'concepto'},
                                                                {name: 'costoUnitario', type:'float'},
                                                                {name: 'nivel'},
                                                                {name: 'cantidad'},
																{name: 'subtotal', type:'float'},                                                                
                                                                {name: 'iva', type:'float'},
                                                                {name: 'total', type:'float'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
                                                       
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Clave',
															width:110,
															sortable:true,
                                                            hidden:false,
															dataIndex:'cveProducto'
														},
                                                        {
															header:'Concepto',
															width:450,
															sortable:true,
															dataIndex:'concepto'
														},
                                                        
														{
															header:'Costo unitario',
															width:100,
															sortable:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Nivel de cobro',
															width:100,
															sortable:true,
															dataIndex:'nivel'
														},
                                                        
                                                        {
															header:'Cantidad',
															width:100,
															sortable:true,
															dataIndex:'cantidad'
														},
                                                        {
															header:'SubTotal',
															width:100,
															sortable:true,
															dataIndex:'subtotal',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:100,
															sortable:true,
															dataIndex:'iva',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Total',
															width:100,
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'grid',
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            cm: cModelo,
                                                            region:'center',
                                                            sm:chkRow,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true
                                                        }
                                                    );
	 tblGrid.nuevoRegistro=false;   
                                                                                                                             
	return 	tblGrid;		
}

function calcularTotal()
{
	var x;
    var grid=gEx('grid');
    var ivaTotal=0;
    var subtotal=0;
    var fila;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        ivaTotal+=fila.get('iva');
        subtotal+=fila.get('total');
    }
    
    gE('lblSubtotal').innerHTML=Ext.util.Format.usMoney(subtotal);    
    gE('lblIVA').innerHTML=Ext.util.Format.usMoney(ivaTotal);    
    gE('lblTotal').innerHTML=Ext.util.Format.usMoney(subtotal+ivaTotal);    
    
}
