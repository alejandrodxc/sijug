<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />

<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	
?>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/checkColumn.js"></script>

<script type="text/javascript" src="../caja/Scripts/catalogoConceptos.js.php"></script>
<script type="text/javascript" src="../Scripts/ux/menu/EditableItem.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/RangeMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/ListMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GridFilters.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/Filter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/StringFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/DateFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/ListFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/NumericFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/BooleanFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/dataConceptosAPI.js.php"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>



</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog())
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas" id="nav2" style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					$consulta="select distinct(pO.idOpcion),tM.textoMenu,tM.colorFondo from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM,808_titulosMenu tM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2 and tM.idMenu=pO.idOpcion order by tM.textoMenu";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						echo '<li><a href="#" style="z-index:190 !important">'.$filaM[1].'</a><ul>';
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino,oP.nombreBullet,oP.idOpciones from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							$comp="<img src='../images/s.gif' width='16' height='16'> ";
							if($fila[2]!="")
							{
								$comp="<img src='../media/verBullet.php?id=".$fila[3]."' width='16' height='16'> ";
							}
							echo '<li  z-index:200000 !important"><a href="'.$fila[1].'">'.$fila[0].'</a></li>';
						}
						echo "</ul></li>";
					}
					
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" id="nav3">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion),tM.textoMenu,tM.colorFondo from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM,808_titulosMenu tM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1 and tM.idMenu=pO.idOpcion order by tM.textoMenu";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
						
						echo '<li><a href="#" style="z-index:190 !important">'.$filaM[1].'</a><ul>';
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino,oP.nombreBullet,oP.idOpciones from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							$comp="";
							if($fila[2]!="")
							{
								$comp="<img src='../media/verBullet.php?id=".$fila[3]."' width='16' height='16'> ";
							}
							echo '<li z-index:190000 !important"><a href="'.$fila[1].'">'.$fila[0].'</a></li>';
						}
						echo "</ul></li>";
					}
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        		<ul id="nav">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu,tm.colorFondo 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									$tabla="";
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$opciones=generarOpciones($fila[1],"","",$unico[5]);
										$menuComp="";
										if($opciones!="")
										{
											$menuComp="<ul>".$opciones."</ul>";
										}
										$obj="
												<li class='current' >
													<a href='#'>&nbsp;&nbsp;".$fila[0]."</a>".$menuComp."
												</li>
												";
										
										if($tabla=="")
											$tabla=$obj;
										else
											$tabla.="".$obj;
										
									}
									echo $tabla;
								?>
                                </ul>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
							$idConcepto=-1;
							if(isset($objParametros->idConcepto))
								$idConcepto=$objParametros->idConcepto;
								
							$consulta="SELECT * FROM 561_conceptosIngreso WHERE idConcepto=".$idConcepto;
							$fConcepto=$con->obtenerPrimeraFila($consulta);
							if(!$fConcepto)
							{
								for($x=0;$x<14;$x++)
									$fConcepto[$x]="";
							}
							$arrCategorias="[]";
							$lblFuncion="(No especificada)";
							if($fConcepto[9]=="")
								$fConcepto[9]=-1;
							if($fConcepto[9]!="-1")
							{
								$consulta="SELECT nombreConsulta FROM 991_consultasSql WHERE idConsulta=".$fConcepto[9];
								$lblFuncion=$con->obtenerValor($consulta);
								
							}
							$consulta="SELECT idCategoriaConceptosIngreso,nombreCategoria from 562_categoriasConceptosIngreso WHERE idCategoriaConceptosIngreso
										IN(select idCategoria FROM 564_conceptosVSCategorias WHERE idConcepto=".$idConcepto.")";
							$arrCategorias=$con->obtenerFilasArreglo($consulta);
							$mostrarPlanPago="none";
							$consulta="SELECT c.idPlanPago,c.noPago,d.etiquetaPago,idConceptoAsociado FROM 564_conceptosVSPlanPago c,6021_desglocePlanPagos d WHERE idConcepto=".$idConcepto."
										AND d.idPlanPagos=c.idPlanPago AND d.noPago=c.noPago ORDER BY c.idPlanPago,c.noPago";
							$arrPlanPagos=$con->obtenerFilasArreglo($consulta);
							
							$arrOperaciones="";
							$consulta="select idOperacion,ordenAplicacion,idTipoOperacion,etiquetaOperacion,origenValor,valor,funcionAplicacion,calcularInterfaceCosto,valorReferencia,idConcepto 
									from 564_conceptosVSOperacionesCargosDescuentos WHERE idConcepto=".$idConcepto." order by ordenAplicacion";
							$res=$con->obtenerFilas($consulta);
							while($fOperacion=mysql_fetch_row($res))
							{
								$valor=$fOperacion[5];
								if($fOperacion[4]==1)
								{
									$consulta="SELECT nombreConsulta FROM 991_consultasSql WHERE idConsulta=".$valor;
									$valor=$con->obtenerValor($consulta);
								}
								$consulta="SELECT nombreConsulta FROM 991_consultasSql WHERE idConsulta=".$fOperacion[6];
								$lblFuncionAplicacion=$con->obtenerValor($consulta);
								$consulta="SELECT idColumna,etiqueta,anchoColumna,tipoValor FROM 565_configuracionColumnaOperacion WHERE idOperacion=".$fOperacion[0];
								$arrColumnas=utf8_encode($con->obtenerFilasJSON($consulta));
								$configuracionComp='{"calcularInterfaceCosto":"'.$fOperacion[7].'","valorReferencia":"'.$fOperacion[8].'","arrColumnas":'.$arrColumnas.'}';
								$o="['".$fOperacion[0]."','".$fOperacion[1]."','".$fOperacion[2]."','".$fOperacion[3]."','".$fOperacion[4]."','".$fOperacion[5]."','".$valor."','".$fOperacion[6]."','".$lblFuncionAplicacion."','".$configuracionComp."']";
								if($arrOperaciones=="")
									$arrOperaciones=$o;
								else
									$arrOperaciones.=",".$o;
							}
							$arrOperaciones="[".$arrOperaciones."]";
						?>
                        	<span class="tituloPaginas"></span>
                            <table width="850">
                            	<tr>
                                	<td align="left">
                                    	<fieldset class="frameHijo"><legend>Concepto de ingreso</legend>
                                    	<table>
                                        <tr>
                                        	<td align="left">
                                        		<form method="post" action="../paginasFunciones/guardarDatos.php" id="frmEnvio">
                                                <table>
                                                    <tr height="21">
                                                        <td width="210"><span class="corpo8_bold">Clave del concepto:</span><span style="color:#F00">*</span></td>
                                                        <td width="600"><input type="text" name="_cveConceptovch" id="_cveConceptovch" value="<?php echo $fConcepto[1]?>" size="20" val="obl" campo="Clave del concepto" /></td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td ><span class="corpo8_bold">Nombre del concepto:</span><span style="color:#F00">*</span></td>
                                                        <td ><input type="text" value="<?php echo $fConcepto[2]?>" size="70" val="obl" name="_nombreConceptovch" id="_nombreConceptovch" campo="Nombre del concepto" /></td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td  valign="top"><span class="corpo8_bold">Descripci&oacute;n:</span></td>
                                                        <td ><textarea cols="80" rows="8" name="_descripcionvch"><?php echo $fConcepto[3]?></textarea></td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td  valign="top"><span class="corpo8_bold">Clasificar el concepto como:</span></td>
                                                        <td  >
                                                        	<span id="tblCategorias"></span>
                                                        </td>
                                                    </tr>
                                                     <tr height="10">
                                                    	<td colspan="2"></td>
                                                    </tr>
                                                    
                                                    <tr>
                                                    	<td colspan="2"><br />
                                                        <fieldset class="frameHijo"><legend>Configuración de costeo</legend>
                                                        <table >
                                                        <tr height="21">
                                                            <td  valign="top" width="240"><span class="corpo8_bold">Ingresar costo a nivel de:</span><span style="color:#F00">*</span></td>
                                                            <td  >
                                                                <select name="_nivelCosteoint" id="_nivelCosteoint" campo="Costo a nivel de" val="obl" >
                                                                    <option value="-1">Seleccione</option>
                                                                    <?php
																		
																		$arrCosto[0][0]=1;
																		$arrCosto[0][1]="Plantel";
																		$arrCosto[1][0]=2;
																		$arrCosto[1][1]="Programa Educativo";
																		$arrCosto[2][0]=3;
																		$arrCosto[2][1]="Plan de Estudios";
																		$arrCosto[3][0]=4;
																		$arrCosto[3][1]="Grado";
																		$arrCosto[4][0]=5;
																		$arrCosto[4][1]="No aplica";
																		
                                                                        
                                                                        $con->generarOpcionesSelectMatriz($arrCosto,$fConcepto[12]);
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td  valign="top" width="240"><span class="corpo8_bold">Interface de ingreso de costo:</span><span style="color:#F00">*</span></td>
                                                            <td  >
                                                                <select name="_perfilCosteoint" id="_perfilCosteoint" campo="Perfil de costeo" val="obl" >
                                                                    <option value="-1">Seleccione</option>
                                                                    <?php
																		$consulta="SELECT idPerfilCosteo,nombrePerfil FROM 6022_perfilesCosteo ORDER BY nombrePerfil";
																		$con->generarOpcionesSelect($consulta,$fConcepto[13]);
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td  valign="top"><span class="corpo8_bold">Considera fecha de vencimiento?:</span><span style="color:#F00">*</span></td>
                                                            <td  >
                                                                <select name="_consideraFechaVencimientoint" id="_consideraFechaVencimientoint" campo="Considera fecha de vencimiento" val="obl" >
                                                                    <option value="-1">Seleccione</option>
                                                                    <?php
                                                                        $consulta="SELECT valor,texto FROM  1004_siNo WHERE idIdioma=1";
                                                                        $con->generarOpcionesSelect($consulta,$fConcepto[11]);
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td  valign="top"><span class="corpo8_bold">Vincular con plan de pago:</span><span style="color:#F00">*</span></td>
                                                            <td  >
                                                                <select name="_vincularPlanPagoint" id="_vincularPlanPagoint" campo="Vincular con plan de pago" val="obl" onchange="vinculoPlanPagoChange(this)" >
                                                                    <option value="-1">Seleccione</option>
                                                                    <?php
                                                                        $consulta="SELECT valor,texto FROM  1004_siNo WHERE idIdioma=1";
                                                                        $con->generarOpcionesSelect($consulta,$fConcepto[10]);
                                                                    ?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        
                                                        </table>
                                                        </fieldset><br />
                                                        </td>
                                                    </tr>
                                                    <?php
													if($fConcepto[10]==1)
														$mostrarPlanPago="";
													?>
                                                    <tr id="filaPlanPago" style="display:<?php  echo $mostrarPlanPago?>">
                                                    	<td colspan="2">
                                                        	<fieldset class="frameHijo"><legend>Configuración de Plan de pagos</legend>
                                                        	<table width="800" >
																
                                                                <tr height="21" >
                                                                    <td  valign="top" ><span class="corpo8_bold">Planes de pagos con los cuales se vincular&aacute; el concepto:</span></td>
                                                                </tr>
                                                                <tr height="21" >
                                                                    <td  ><br />
                                                                        <span id="tblPlanesPago"></span><br /><br />
                                                                    </td>
                                                                </tr>
                                                                
                                                            </table><br /><br />
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr  >
                                                    	<td colspan="2">
                                                        <br /><br />
                                                        	<fieldset class="frameHijo"><legend>Configuración de Cargos y descuentos</legend>
                                                        	<table width="800" >
																
                                                                
                                                                <tr height="21" >
                                                                    <td  valign="top" ><span class="corpo8_bold">Cargos y descuentos aplicables al concepto:</span></td>
                                                                </tr>
                                                                <tr height="21" >
                                                                    <td  ><br />
                                                                        <span id="tblCargos"></span>
                                                                    </td>
                                                                </tr>
                                                            </table><br /><br />
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr height="10">
                                                    	<td colspan="2"></td>
                                                    </tr>
                                                     <tr height="21" style="display:none">
                                                        <td  valign="top"><span class="corpo8_bold">Funci&oacute;n de costeo:</span></td>
                                                        <td valign="top"><b><span class="letraExt" id="lbl_funcionCosteoint"><?php echo $lblFuncion?></span></b>&nbsp;&nbsp;
                                                        	<a href="javascript:mostrarVentanaExpresion('<?php echo bE("_funcionCosteoint")?>',1)"><img src="../images/add.png" width="13" height="13" title='Agregar función de costeo' alt='Agregar función de costeo' /></a>&nbsp;
                                                        	<a href="javascript:removerConcepto('<?php echo bE("_funcionCosteoint")?>')"><img src="../images/delete.png" width="13" height="13" title='Remover función de costeo' alt='Remover función de costeo' /></a>
                                                            <input type="hidden" id="_funcionCosteoint" name="_funcionCosteoint" value="<?php echo $fConcepto[9]?>" />
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr height="21">
                                                        <td  valign="top"><span class="corpo8_bold">Situaci&oacute;n del concepto:</span><span style="color:#F00">*</span></td>
                                                        <td >
                                                        	<select id="_situacionint" name="_situacionint">
                                                        <?php
                                                        	$consulta="SELECT claveElemento,nombreElemento FROM 1018_catalogoVarios WHERE tipoElemento=1";
															echo $con->generarOpcionesSelect($consulta,$fConcepto[8]);
														?>
                                                        	</select>
                                                        </td>
                                                    </tr>
                                                    <tr height="21">
                                                    </tr>
                                                    
                                                    <tr>
                                                    	<td align="center" colspan="2">	
                                                        <input type="button" class="btnAceptar" value="Aceptar" onclick="validarFrm()" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                

                                                <input type="hidden" id="arrCategorias" value="<?php echo bE($arrCategorias)?>" />
                                                <input type="hidden" id="arrPlanPagos" value="<?php echo bE($arrPlanPagos)?>" />
                                                <input type="hidden" id="arrOperaciones" value="<?php echo bE($arrOperaciones)?>" />
                                                
                                                <input type="hidden" name="tabla" value="561_conceptosIngreso" />
                                                <input type="hidden" name="post"  value="" />
                                                <input type="hidden" name="id" id="id" value="<?php echo $idConcepto?>" />
                                                <input type="hidden" name="campoId" value="idConcepto" />
                                                <input type="hidden" name="pagRedireccion" value="../caja/tblCatalogoConceptos.php"/>	
                                                
                                                <?php
                                                    if($idConcepto=="-1")
                                                    {
                                                ?>
                                                	<input name="_responsableCreacionint" type="hidden" id="_responsableCreacionint" value="<?php echo $_SESSION["idUsr"]?>"  />
	                                                <input name="_fechaCreaciondta" type="hidden" id="_fechaCreaciondta" value=""  />
                                                    <input name="funcPHPEjecutarNuevo" id="funcPHPEjecutarNuevo" type="hidden"  value=""  />
                                                    
                                                <?php
                                                    }
													else
													{
												?>
                                                	<input name="_responsableModificacionint" type="hidden" id="_responsableModificacionint" value="<?php echo $_SESSION["idUsr"]?>"  />
                                                    <input name="_fechaModificaciondta" type="hidden" id="_fechaModificaciondta" value=""  />
                                                    <input name="funcPHPEjecutarModif" id="funcPHPEjecutarModif" type="hidden"  value=""  />
                                                <?php
													}
                                                    
                                                ?>	
                                                </form>
                                            </td>
                                       	</tr>
                                        </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
