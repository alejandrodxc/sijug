<?php 
include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
?>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="Scripts/recursos.js.php"></script>
<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
                                  ?>
                                  
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <?php
				   $idRecurso="-1";
				   if(isset($objParametros->idRecurso))
							$idRecurso=$objParametros->idRecurso;
							
				   $consulta="select * from  4009_recursos where idRecurso=".$idRecurso;
				   $fila=$con->obtenerPrimeraFila($consulta);
				   ?>
                   <tr>
                        <td>
                          <form id="frmEnvio" method="post" action="guardarRecursos.php"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="center">
                                  <table width="650">
                                    <tr>
                                      <td align="left">
                                          <fieldset class="frameHijo" ><legend><b>Recurso</b></legend>
                                            <table width="600" >
                                              <tr height="23">
                                                <td width="182"  align="right"><label>Recurso: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                                <td><input type="text" name="_titulovch" id="_titulovch" maxlength="150" size="53" val="obl" campo="titulo" value="<?php echo $fila[1]?>" /></td>
                                              </tr>
                                              <tr height="23">
                                                <td valign="top" align="right"><label>Descripción: <font color="#FF0000"></font></label>&nbsp;&nbsp;</td>
                                                <td width="477"><textarea rows="4" cols="50" id="_descripcionvch" name="_descripcionvch" campo="Descripcion" ><?php echo $fila[2]?></textarea></td>
                                              </tr>
                                              <tr height="23">
                                                <td align="right"><label>Tipo: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                                <td>
                                                  <select name="_idTipoRecursoint" id="_idTipoRecursoint" val="obl" campo="Tipo" onchange="tipoRecurso()">
                                                       <option value="-1">Seleccione </option>
                                                       <?php 
                                                         $consulta2="select idTipoRecurso,nombre from 4055_tipoRecurso";
                                                         $con->generarOpcionesSelect($consulta2,$fila[7]);
                                                       ?>
                                                  </select>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td  align="right"><label>Apartable: &nbsp;&nbsp;</label></td>
                                                <td>
                                                  <select name="apartable" id="apartable"   val="obl" campo="Apartable" onchange="ocultarResponsable()" >
                                                       <option value="0">Si </option>
                                                       <option value="1">No </option>
                                                  </select>
                                                </td>
                                              </tr>
                                              <tr height="23">
                                                <td align="right"><label>Contenido en: &nbsp;&nbsp;</label></td>
                                                <td>
                                                    <label>Categoría:&nbsp;</label>
                                                    <select id="cmbContenidoEn" onchange="cargarRecursos(this)" >
                                                         <option value="0">Nada</option>
                                                         <?php 
                                                           $consulta2="select idTipoRecurso,nombre from 4055_tipoRecurso";
                                                           $con->generarOpcionesSelect($consulta2,$fila[7]);
                                                         ?>
                                                    </select>&nbsp;&nbsp;
                                                    <select name="_idInmuebleint" id="_idInmuebleint" >
                                                    </select>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td colspan="2" >
                                                    <table>
                                                         <tr>
                                                             <td valign="top" width="170px" >
                                                             </td>
                                                             <td height="30" style="border:0px">
                                                                    <input value="1" type="radio" name="radio1" checked="checked" onclick="ponerCBusqueda('1')" /><label>&nbsp;Ap. Paterno&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                                    <input value="2" type="radio" name="radio1" onclick="ponerCBusqueda('2')"  /><label>&nbsp;Ap. Materno&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                                    <input value="3" type="radio" name="radio1" onclick="ponerCBusqueda('3')"   /><label>&nbsp;Nombre</label>
                                                                    <input type="hidden" name="cBusqueda" id="cBusqueda"  value="1"/>		
                                                                    <input type="hidden" name="idUsuario" id='idUsuario' value="-1">								
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="right" valign="top"><label>Responsable:&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td colspan="2" style="border:0px" ><input type="text" name="Nombre" id="Nombre" /> &nbsp;</td>
                                                         </tr>
                                                    </table>
                                                </td>
                                              </tr>
                                              <tr id="filaResponsable">
                                                    <td colspan="2">
                                                      <table>
                                                            <tr>
                                                                <td width="170" valign="top" >
                                                                    <table>
                                                                      <tr>
                                                                          <td style="border:0px" ><a href="javascript:agregarRol()"><img src="../images/accept_green.png" alt='Agregar Coordinador por Rol' title='Agregar Coordinador por Rol'/></a></td>
                                                                          <td class="letraFicha" style="border:0px" >&nbsp;&nbsp;<a href="javascript:agregarRol()">Agregar Coordinador</a></td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td style="border:0px" ><a href="javascript:removerRol()"><img src="../images/cancel_round.png" alt='Remover Participantes' title='Remover Participantes'/></a></td>
                                                                          <td class="letraFicha" style="border:0px" >&nbsp;&nbsp;<a href="javascript:removerParticipante()">Remover Coordinador</a></td>
                                                                      </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="left" style="border:0px" >
                                                                    <select size="5" style="width:350px" id="listaParticipantes" name="listaParticipantes" >
                                                                    </select>
                                                                    <input type="hidden" name="listadoRolesP" id="listadoRolesP" value="" />
                                                                    <input type="hidden" id="listadoUsuariosP" value="" />
                                                                </td>
                                                            </tr>
                                                      </table>
                                                    </td>
                                              </tr>
                                              <tr id="areas" style="display:none" height="23">
                                                <td align="right" ><label>Capacidad(personas):&nbsp;&nbsp;</label></td>
                                                <td><input type="text" name="_capacidadint" id="_capacidadint"  campo="Capacidad" style="width:112px" onKeyPress="return soloNumero(event,false,false)" value="<?php echo $fila[8]?>" /></td>
                                              </tr>
                                              <tr id="otrosTipos" style="display:none">
                                                <td colspan="2">
                                                  <table>
                                                    <tr>
                                                      <td>
                                                        <br/>
                                                      </td>
                                                    </tr>
                                                    <tr height="23">
                                                      <td width="182" align="right" valign="top"><label>Marca:&nbsp;</label></td>
                                                      <td width="487"><input type="text" name="_marcavch" id="_marcavch" value="<?php echo $fila[12]?>" size="30" /></td>
                                                    </tr>
                                                    <tr height="23">
                                                      <td align="right" valign="top"><label>Modelo:&nbsp;&nbsp;&nbsp;</label></td>
                                                      <td><input type="text" name="_modelovch" id="_modelovch" value="<?php echo $fila[11]?>" size="50" /></td>
                                                    </tr>    
                                                    <tr height="23">
                                                      <td align="right" valign="top"><label>No. de serie:&nbsp;&nbsp;&nbsp;</label></td>
                                                      <td><input type="text" name="_noSerievch" id="_noSerievch" value="<?php echo $fila[12]?>" size="30" /></td>
                                                    </tr>
                                                    <tr height="23">
                                                      <td align="right" valign="top"><label>Observaciones:&nbsp;&nbsp;&nbsp;</label></td>
                                                      <td><textarea rows="5" cols="70" id="_observacionesvch" name="_observacionesvch" campo="Observaciones" ><?php echo $fila[13]?></textarea></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </fieldset>
                                      </td>
                                    </tr>
                                  </table>
                              </td>
                            </tr>
                            <tr height="23">
                              <td colspan="2" align="center">
                              <br />
                              <input type="hidden" name="_fechaCreaciondta" value="<?php if(isset($_POST["idRecurso"]))
                              {
                                  echo $fila[3];
                              }
                              else
                              {
                              echo date("d/m/y H:i:s"); 
                                  }?>" />
                               <?php 	
                                      if($idRecurso=="-1")
                                      {
                              ?>
                              <input type="button" class="tituloTabla" name="btnGuardar" value="Guardar" onclick="validar('frmEnvio')"/>
                              <?php
                                      }
                                      else
                                      {
                              ?>
                              <input type="button" class="tituloTabla" name="btnGuardar" value="Actualizar" onclick="validar('frmEnvio')"/>
                              <input type="hidden" name="_fechaModifdta" value="<?php echo date("d/m/y H:i:s");  ?> " />
                              <input type="hidden" name="_respModifint" value="<?php echo $_SESSION["idUsr"]?>" />
                              <?php 
                                      }
                              ?>
                              </td>
                            </tr>
                          </table>
						    <input type="hidden" name="idRecurso" value="<?php echo $idRecurso?>" />
						    <input type="hidden" name="pagRedireccion" value="../Administracion/tblRecursos.php"/>
                            <input type="hidden" name="paramPost" value='[{"nombreP":"configuracion","valorP":"<?php echo $nConfRegresar?>"}]' />		
					      </form>
						  <br />
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
