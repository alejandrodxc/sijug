<?php	session_start(); 
	include("latis/conexionBD.php");							  
    $bloques=$_POST["_bloquesint"];
	if($bloques=="-1")
		$bloques=0;
	$idCiclo=$_POST["_cicloint"];
	$idmat= $_POST["id"];
	$idprog= $_POST["_idProgramaint"];
	$clave= $_POST["_cve_materiavch"];
	$titulo= $_POST["_titulovch"];
	$obj=$_POST["_objetivovch"];
	$objEsp=$_POST["_objetivos_especificosvch"];
	$prop=$_POST["_propositovch"];
	$key=$_POST["_keywordsvch"];
	$status=$_POST["_statusint"];
	$abreviatura=$_POST["_abreviaturavch"];
    $horasTotal=$_POST["_horasTotalint"];
	$horasSemana=$_POST["_horasSemanaint"];
	$idMapaCurricular=$_POST["idMapaCurricular"];
	$descripcion=$_POST["_descripcionvch"];
	$tipoTemario=$_POST["_tipoTemarioint"];
	$esquemaEvaluacion=$_POST["_esquemaEvaluacionint"];
	$compartida=$_POST["_compartidaint"];
	
	$horasTeoricas=$_POST["_horasTeoricasint"];
	if($horasTeoricas=="-1")
		$horasTeoricas=0;
		
	$horasPracticas=$_POST["_horasPracticasint"];
	if($horasPracticas=="-1")
		$horasPracticas=0;
		
	if(isset($_POST["_horasTeoricasLibresint"]))
		$horasTeoricasLibres=$_POST["_horasTeoricasLibresint"];
	else
		$horasTeoricasLibres=0;
		
	if(isset($_POST["_horasPracticasLibresint"]))	
		$horasPracticasLibres=$_POST["_horasPracticasLibresint"];
	else
		$horasPracticasLibres=0;
		
	if(isset($_POST["_nCreditosint"]))
		$creditos=$_POST["_nCreditosint"];
	else
		$creditos=0;
	
	if(isset($_POST["minGrupo"]))
		$minGrupo=$_POST["minGrupo"];
	else
		$minGrupo=0;
		
	if(isset($_POST["maxGrupo"]))
		$maxGrupo=$_POST["maxGrupo"];
	else
		$maxGrupo=0;
		
	if(isset($_POST["noGrupos"]))
		$noGrupos=$_POST["noGrupos"];
	else
		$noGrupos=0;		
	
	if(isset($_POST["cadenaCompartida"]))
		$cadenaCompartir=$_POST["cadenaCompartida"];
	else
		$cadenaCompartir="";
	
	$confReferencia=$_POST["confReferencia"];
	if(isset($_POST["_mostrarSesionesint"]))
	{
		$mostrarSesiones=$_POST["_mostrarSesionesint"];
	}
	else
	{
		$mostrarSesiones=1;
	}
	
	$conCreditos="select creditosHoraPractica,creditosHoraTeorica,manejaCreditos,modalidadC,horasLibres,considerarHorasL from 4029_mapaCurricular where idMapaCurricular=".$idMapaCurricular ;
	//echo $conCreditos;
	$fila=$con->obtenerPrimeraFila($conCreditos);
	
	if($fila[2]==1)
	{
		if($fila[3]==1)
		{
			if($fila[5]==1)
			{
				$horasPracticasCreditos=$horasPracticas+$horasPracticasLibres;
				$horasTeoricasCreditos=$horasTeoricas+$horasTeoricasLibres;
				$creditos=($horasPracticasCreditos*$fila[0]) + ($horasTeoricasCreditos*$fila[1]);
			}
			else
			{
				$creditos=($horasPracticas*$fila[0]) + ($horasTeoricas*$fila[1]);
			}
		}
	}
	try
	{
		if($idmat==-1)
		{
			$fcreacion= $_POST["_fechaCreacion"];
			$resp= $_POST["_responsableint"];
			
			$guardar="insert into 4013_materia(idPrograma,cve_materia,titulo,objetivo,objetivos_especificos,proposito,fechaCreacion,responsable,status,keywords,bloques,ciclo,abreviatura,horasTotal,horasSemana,descripcion,tipoTemario,horasTeoricas,horasPracticas,esquemaEvaluacion,compartida,nCreditos,mostrarSesiones,minGrupo,maxGrupo,noGrupos,idMapaCurricular)values('".$idprog."','".$clave."','".$titulo."','".$obj."','".$objEsp."','".$prop."','".$fcreacion."','".$resp."','".$status."','".$key."','".$bloques."','".$idCiclo."','".$abreviatura."','".$horasTotal."','".$horasSemana."','".$descripcion."','".$tipoTemario."','".$horasTeoricas."','".$horasPracticas."','".$esquemaEvaluacion."','".$compartida."','".$creditos."','".$mostrarSesiones."',".$minGrupo.",".$maxGrupo.",".$noGrupos.",".$idMapaCurricular.")";
			$resultado=$con->ejecutarConsulta($guardar);
			$imateria=$con->obtenerUltimoID();
			
			if($resultado)
			{
				if(isset($_POST["_campoint"]))
					$codCampo=$_POST["_campoint"];
				else
					$codCampo=-1;
					
				if(isset($_POST["_disciplinaint"]))
					$codDisciplina=$_POST["_disciplinaint"];
				else
					$codDisciplina=-1;
				
				if(isset($_POST["_subDisciplinaint"]))
					$codSubD=$_POST["_subDisciplinaint"];
				else
					$codSubD=-1;
					
				$consultaT="begin";
				if($con->ejecutarConsulta($consultaT))
				{
					$ct=0;
					$query[$ct]="INSERT INTO 4214_clasificacionMateriaUnesco(idMateria,codCampo,codDisciplina,codSubDisciplina) VALUES('".$imateria."','".$codCampo."','".$codDisciplina."','".$codSubD."')";
					$ct++;
					
					$conDatosMapa="SELECT generarGrupos,modoPrefijo,incremento FROM 4029_mapaCurricular WHERE idMapaCurricular=".$idMapaCurricular;
					$filaD=$con->obtenerPrimeraFila($conDatosMapa);
					if($filaD[0]==2)//genera grupos automaticamente
					{
						$conExisteIncremento="SELECT idIncreGrupoMateria FROM 4237_incrementableGrupoMateria WHERE idMateria=".$imateria." AND idMapaCurricular=".$idMapaCurricular;
						$existeInc=$con->obtenerFilas($conExisteIncremento);
						$nFilasIncre=$con->filasAfectadas;
						if($nFilasIncre==0)
						{
							$conSedes="SELECT sede FROM 4020_programasVSSede WHERE idPrograma=".$idprog;
							$resSedes=$con->obtenerFilas($conSedes);
							
							while($filaS=mysql_fetch_row($resSedes))
							{
								$query[$ct]="INSERT INTO 4237_incrementableGrupoMateria (idMapaCurricular,idMateria,sede,noIncrementable,incremento)
										VALUES(".$idMapaCurricular.",".$imateria.",'".$filaS[0]."',0,".$filaD[2].")";
								$ct++;		
							}
						}
					}
					
					if($cadenaCompartir!="")
					{
						$arregloPr=explode(",",$cadenaCompartir);
						$tamanoP=sizeof($arregloPr);
						for($z=0;$z<$tamanoP;$z++)
						{
							$query[$ct]="INSERT INTO 4239_materiaCompVSPrograma (idMateria,idPrograma)
										VALUES(".$imateria.",".$arregloPr[$z].")";
								$ct++;	
						}
					}
					$query[$ct]="commit";
					$con->ejecutarBloque($query);
				}
			}
			$nueva=1;
		}
		else
		{
			$fmodif= $_POST["_fechaModif"];
			$rmodif= $_POST["_respModifint"];
			$modificar="update 4013_materia set cve_materia='".$clave."',titulo='".$titulo."',objetivo='".$obj."',objetivos_especificos='".$objEsp."',proposito='".$prop."',status='".$status."',keywords='".$key."',fechaModif='".$fmodif."',respModif='".$rmodif."',bloques='".$bloques."',ciclo='".$idCiclo."',abreviatura='".$abreviatura."',horasTotal='".$horasTotal."',horasSemana='".$horasSemana."',descripcion='".$descripcion."',tipoTemario='".$tipoTemario."',horasTeoricas='".$horasTeoricas."',horasPracticas='".$horasPracticas."',esquemaEvaluacion='".$esquemaEvaluacion."',compartida='".$compartida."',nCreditos='".$creditos."',mostrarSesiones='".$mostrarSesiones."',minGrupo=".$minGrupo.",maxGrupo=".$maxGrupo.",noGrupos=".$noGrupos." where idMateria='".$idmat."'";
			$resultado=$con->ejecutarConsulta($modificar);
			$imateria=$idmat;
			$nueva=0;
			
			if($resultado)
			{
				if(isset($_POST["_campoint"]))
					$codCampo=$_POST["_campoint"];
				else
					$codCampo=-1;
					
				if(isset($_POST["_disciplinaint"]))
					$codDisciplina=$_POST["_disciplinaint"];
				else
					$codDisciplina=-1;
				
				if(isset($_POST["_subDisciplinaint"]))
					$codSubD=$_POST["_subDisciplinaint"];
				else
					$codSubD=-1;
					
				
				
				$consultaTran="begin";
				if($con->ejecutarconsulta($consultaTran))
				{
					$ct=0;
					$conExiste="SELECT idMateriaVSClasificacioU FROM 4214_clasificacionMateriaUnesco WHERE idMateria=".$imateria;
					$existe=$con->obtenerValor($conExiste);
					if($existe=="")
					{
						$query[$ct]="INSERT INTO 4214_clasificacionMateriaUnesco(idMateria,codCampo,codDisciplina,codSubDisciplina) VALUES('".$imateria."','".$codCampo."','".$codDisciplina."','".$codSubD."')";
						$ct++;
					}
					else
					{
						$query[$ct]="UPDATE 4214_clasificacionMateriaUnesco SET codCampo='".$codCampo."',codDisciplina='".$codDisciplina."',codSubDisciplina='".$codSubD."' where idMateriaVSClasificacioU=".$existe;
						$ct++;
					}
					
					$query[$ct]="DELETE FROM 4239_materiaCompVSPrograma WHERE idMateria=".$imateria;
					$ct++;
					
					if($cadenaCompartir!="")
					{
						$arregloPr=explode(",",$cadenaCompartir);
						$tamanoP=sizeof($arregloPr);
						for($z=0;$z<$tamanoP;$z++)
						{
							$query[$ct]="INSERT INTO 4239_materiaCompVSPrograma (idMateria,idPrograma)
										VALUES(".$imateria.",".$arregloPr[$z].")";
							$ct++;	
						}
					}
					
					$query[$ct]="commit";
					$con->ejecutarBloque($query);
				}
			}
		}
		$res=1;
	}
	
	catch(Exception $e)
	{
		$res=-1;
		echo $e->getMessage();
	}
	
	if($idmat!="-1")
	{
		$objSesion=json_decode($_SESSION["configuracionesPag"][$confReferencia]["parametros"]);
		$pagRedireccion=$objSesion->paginaConf;
	}
	else
	{
		$pagRedireccion="../Administracion/fichaMateria.php";
	}
	//echo $pagRedireccion;
 ?>
 <title>
 </title>
 <body>
 
 	<?php
		if($idmat!="-1")
		{
	?>
 		<form method="post" action="<?php echo $pagRedireccion?>" id="frmEnvio">
        	<input type="hidden" name="cPagina" value="sFrm=true">
            <input type="hidden" name="configuracion" value="<?php echo $confReferencia ?>"/>
        </form>
	<?php
		}
		else
		{
	?>
    	<form method="post" action="<?php echo $pagRedireccion?>" id="frmEnvio">
            <input type="hidden" name="confReferencia" value="<?php echo $confReferencia ?>"/>
            <input type="hidden" name="idCiclo" id="idCiclo" value="<?php echo $idCiclo ?>"/>
            <input type="hidden" name="idMapaCurricular" id="idMapaCurricular" value="<?php echo $idMapaCurricular ?>"/>
            <input type="hidden" name="idMateria" id="idMateria" value="<?php echo $imateria ?>"/>
            <input type="hidden" name="idSeccion" id="idSeccion" value="<?php echo $idprog ?>"/>
            <input type="hidden" name="panel" id="panel" value="0"/>
        </form>
    <?php
		}
	?>
        <script language="javascript">
           document.getElementById('frmEnvio').submit();
        </script>
    
 </body>