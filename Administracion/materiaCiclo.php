<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php 
	$guardarConfSession=true;
	$mostrarMenuIzq=false;
?>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="Scripts/materiaCiclo.js.php"></script>
<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
										echo $txMenuIncluye;
                                  
										?>
                                       
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <?php 
				    $idMapaCurricular="-1";
					 if(isset($objParametros->idMapaCurricular))
						$idMapaCurricular=$objParametros->idMapaCurricular;
					
					$idGrado="-1";
					if(isset($objParametros->idGrado))
						$idGrado=$objParametros->idGrado;
						
					$idElemento="-1";
					if(isset($objParametros->idElemento))
						$idElemento=$objParametros->idElemento;	
					
					$conTipoH="SELECT idTipoHorario,ciclo,idPrograma FROM 4029_mapaCurricular WHERE idMapaCurricular=".$idMapaCurricular ;
					$filaMapa=$con->obtenerPrimeraFila($conTipoH);
					$idTipoHorario=$filaMapa[0];
					$idPrograma=$filaMapa[2];
					$idCiclo=$filaMapa[1];
					//consulta para etiquetas 
					$etiquetaMateria="SELECT etiqueta,etiquetaPlural FROM 4140_etiquetasMapa WHERE idTipoElemento=1 AND idMapaCurricular=".$idMapaCurricular;
				    $etiquetaM=$con->obtenerPrimeraFila($etiquetaMateria);
				    $etiquetaFilasM=$con->obtenerFilas($etiquetaMateria);
				    $filasM=$con->filasAfectadas;
				    if($filasM==0)
				    {
					 	 $etiquetaM[0]="Materia";
						 $etiquetaM[1]="Materias";
				    }
				   
					
					//consulta para saber el numero de materias
					$conNumeroMaterias="SELECT idMateria FROM 4013_materia WHERE idMapaCurricular=".$idMapaCurricular." and idMateria not in (select idMateria from 4031_elementosMapa where idMapaCurricular=".$idMapaCurricular.")";
					//echo $conNumeroMaterias;
					$materias=$con->obtenerFilas($conNumeroMaterias);
					$filasMaterias=$con->filasAfectadas;
					
					//programa
					
					$conNombreP="SELECT nombrePrograma FROM 4004_programa WHERE idPrograma=".$idPrograma ;
					$nombreP=$con->obtenerValor($conNombreP);
					
					
					
					$conMaterias="SELECT idMateria,titulo,cve_materia,descripcion,idPrograma FROM 4013_materia WHERE  compartida=1 and (((idMateria IN (SELECT idMateria FROM 4031_elementosMapa)) and (idMateria in (SELECT idMateria FROM 4239_materiaCompVSPrograma WHERE idPrograma=".$idPrograma."))) OR idPrograma=".$idPrograma.")  order by titulo,idPrograma";
					 //idMateria IN (SELECT idMateria FROM 4031_elementosMapa)
					//echo $conMaterias;
					$res=$con->obtenerFilas($conMaterias);
					$filas=$con->filasAfectadas;
					
					$conMaterias2="SELECT idMateria,titulo,cve_materia,descripcion,idPrograma FROM 4013_materia WHERE compartida=0 AND idMapaCurricular=".$idMapaCurricular." AND idPrograma=".$idPrograma." AND idMateria NOT IN (SELECT idMateria FROM 4031_elementosMapa WHERE idMapaCurricular=".$idMapaCurricular.") order by titulo,idPrograma";
					//echo $conMaterias2;
					$res2=$con->obtenerFilas($conMaterias2);
					$filas2=$con->filasAfectadas;
					?>
                    <input type="hidden" name="panel" id="panel" value="<?php echo $filasMaterias ?>" />
                    <input type="hidden" name="filas" id="filas" value="<?php echo $filas ?>" />
                    <input type="hidden" name="filas2" id="filas2" value="<?php echo $filas2 ?>" />
                    <input type="hidden" name="etiqueta" id="etiqueta" value="<?php echo $etiquetaM[1] ?>" />
                    <tr>
                        <td  align="center">
                        <span class="tituloPaginas"><?php echo $etiquetaM[1] ?>&nbsp;Disponibles para:&nbsp;<?php echo $nombreP ?></span> <br /><br /><br /><br />
                        <table width="100%">
                        <tr>
                        <td align="center">
                            <table >
                            	<?php 
								if(($filas>0) || ($filas2 > 0))
								{
								?>
                                
                                <tr>
                                	<td align="left">
                                        <div id="my-tabs" align="center" style="width:900px;"></div>
                        			</td>
                            	</tr>
                                <?php 
								}
								else
								{
								?>
                                <tr>
                                	<td class="letraFicha"  align="center">
                                    	No Hay&nbsp;<?php echo $etiquetaM[1] ?>&nbsp;Disponibles
                        			</td>
                            	</tr>
								<?php 
								}
								?>
                            </table>
                       
                    <form name="guardarMateriaPrograma" id="guardarMateriaPrograma" method="post" action="../programaAcademico/configuracionMateria.php" >
                    <table id='tabla1' align="center"  class="x-hide-display">
                        <?php 
                        if($filas==0)
                        {
                        ?>
                        <tr>
                        	<td>
                            	<br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="letraFicha" width="300" align="center">
                                <b>No hay Materias Compartidas Disponibles</b>
                            </td>
                        </tr>
                        <?php 
                        }
                        else
                        {
                        ?>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td width="20" align="center" class="tituloTabla">
                            </td>
                            <td width="30" align="center" class="tituloTabla" >
                                Clave
                            </td>
                            <td  class="tituloTabla" width="250" align="left">
                                <?php echo $etiquetaM[0] ?>
                            </td>
                            <td class="tituloTabla" width="320" align="left">
                                Descripci&oacute;n
                            </td>
                            <td class="tituloTabla" width="180" align="center">
                            	Programa
                            </td>
                        </tr>
                        <?php 
							$clase="filaBlanca10";
                            while($fila=mysql_fetch_row($res))
                            {
								
                        ?>
                        <tr>
                            <td align="center" class="<?php echo $clase?>" valign="top">
                                <input type="radio" name="idMateriaC"  value="<?php echo $fila[0] ?>" onchange="radioCheck(this)" />
                            </td>
                            <td align="left" class="<?php echo $clase?>" valign="top">
                                <?php echo $fila[2] ?>
                            </td>
                             <td align="left" class="<?php echo $clase?>" valign="top">
                                <?php echo $fila[1] ?>
                            </td>
                            <td width="150" align="left" class="<?php echo $clase?>" valign="top">
                                <?php echo $fila[3] ?>
                            </td>
                            <td width="150" align="left" class="<?php echo $clase?>" valign="top">
                            	<?php 
								$conNombrePmat="SELECT nombrePrograma FROM 4004_programa WHERE idPrograma=".$fila[4] ;
								$nombrePmat=$con->obtenerValor($conNombrePmat);
								echo $nombrePmat;
								?>
                            </td>
                        </tr>
                        <?php
								if($clase=='filaRosa10')
									$clase="filaBlanca10";
								else
									$clase="filaRosa10";
								
                            }
                        ?>
                        <tr>
                            <td colspan="5" align="center"><br /><br />
                                <input  type="button" value="Guardar" class="tituloTabla" onclick="validarCompartida('guardarMateriaPrograma')" />  <br /><br /><br /><br />
                            </td>
                        </tr>
                        <?php 
                        }
                        ?>
                    </table>
                    
                    
                    </fieldset>
                    </form>
                    <?php 
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					// materias compartidas
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////77
					?>
                    <form name="guardarMateriaPrograma" id="guardarMateriaPrograma" method="post" action="../programaAcademico/configuracionMateria.php" >
                    <table id='tabla2' align="center" class="x-hide-display">
                        <?php 
                        if($filas2==0)
                        {
                        ?>
                        <tr>
                        	<td>
                            	<br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="letraFicha" width="300" align="center">
                                <b>No Existen Materias Disponibles</b>
                            </td>
                        </tr>
                        <?php 
                        }
                        else
                        {
                        ?>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td width="20" align="center" class="tituloTabla">
                                
                            </td>
                            <td width="30" align="center" class="tituloTabla">
                                Clave
                            </td>
                            <td  class="tituloTabla" width="300" align="left">
                                Materia
                            </td>
                            <td class="tituloTabla" width="350" align="left">
                                Descripci&oacute;n
                            </td>
                        </tr>
                        <?php 
							$clase="filaBlanca10";
                            while($fila2=mysql_fetch_row($res2))
                            {
                        ?>
                        <tr>
                            <td align="center" class="<?php echo $clase?>">
                                <input type="radio" name="idMateria"  value="<?php echo $fila2[0] ?>" onchange="radioCheck(this)" />
                            </td>
                            <td align="left" valign="top" class="<?php echo $clase?>">
                                <?php echo $fila2[2] ?>
                            </td>
                             <td align="left" valign="top" class="<?php echo $clase?>">
                                <?php echo $fila2[1] ?>
                            </td>
                            <td width="150" align="left" valign="top" class="<?php echo $clase?>">
                                <?php echo $fila2[3] ?>
                            </td>
                        </tr>
                        <?php
								if($clase=='filaRosa10')
									$clase="filaBlanca10";
								else
									$clase="filaRosa10";
                            }
                        ?>
                        <tr>
                            <td colspan="4" align="center"><br /><br />
                                <input  type="button" value="Guardar" class="tituloTabla" onclick="validarFrm('guardarMateriaPrograma')" /> <br /><br /> 
                            </td>
                        </tr>
                        <?php 
                        }
                        ?>
                    </table>
                    </form>
					
                    <input type="hidden" name="idMapaCurricular" id="idMapaCurricular" value="<?php echo $idMapaCurricular ?>" />
                    <input type="hidden" name="idGrado" id="idGrado" value="<?php echo $idGrado ?>" />
                    <input type="hidden" name="idPadre" id="idPadre" value="<?php echo $idElemento ?>" />
                    <input type="hidden" name="idTipoHorario" id="idTipoHorario" value="<?php echo $idTipoHorario ?>" />
                    <input type="hidden" name="idMateria" id="idMateria" value="0" />
                    <input type="hidden" name="idMateria" id="idMateriaC" value="0" />
                    <input type="hidden" name="confMapa" id="confMapa" value="<?php echo $nConfRegresar?>" />
                    	</td>
                        </tr>
                       </table>
                  	</td>
                  </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
