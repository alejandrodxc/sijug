<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="select  idCategoria, categoria, tipoCategoria, tablaCategoria,campoId,columnaUnidad,condicion from 4085_categoriasRol  order by categoria";
	$arrCiclo=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrCiclo?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idCategoria'},
                                                                    {name: 'categoria'},
                                                                    {name: 'tipoCategoria'},
                                                                     {name: 'tablaCategoria'},
                                                                     {name: 'campoId'},
                                                                     {name: 'columnaUnidad'},
                                                                     {name: 'condicion'}                                                            ]
                                                    	}
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Categoria',
															width:150,
															sortable:true,
															dataIndex:'categoria'
														},
                                                        {
															header:'Tipo categoria',
															width:50,
															sortable:true,
															dataIndex:'tipoCategoria'
                                                         },
                                                        {
															header:'Tabla categoria',
															width:150,
															sortable:true,
															dataIndex:'tablaCategoria'
														},
                                                        {
															header:'campo relacion',
															width:150,
															sortable:true,
															dataIndex:'campoId'
														},
                                                        {
															header:'Columna Unidad',
															width:200,
															sortable:true,
															dataIndex:'columnaUnidad'
														},
                                                        {
															header:'Query',
															width:200,
															sortable:true,
															dataIndex:'condicion'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:350,
                                                            width:900,
                                                            renderTo:'tblRolCategoria',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nuevo rol',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idCategoria','-1']];
                                                                                    	enviarFormularioDatos('catalogoRolCategoria.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar rol',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar el rol a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idCategoria=fila.get('idCategoria');
                                                                                       
                                                                                       var arrParam=[['idCategoria',idCategoria]];
                                                                                    	enviarFormularioDatos('catalogoRolCategoria.php',arrParam);
                                                                                    }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
}

