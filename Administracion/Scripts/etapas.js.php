Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_nombreEtapavch').focus();
}

function validarfrm(formulario)
{
	if(validarFormularios(formulario))
	{
		gE(formulario).submit();
	}
}
