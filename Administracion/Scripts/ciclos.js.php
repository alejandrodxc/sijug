Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	try
    {
		gE('_anioint').focus();
    }
    catch(e)
    {
    }
}

function validar(formulario)
{
	if(validarFormularios(formulario))
	{
    	
		gE(formulario).submit();
	}
}

function establecerCicloAnt()
{
    var hfuncion=gE('idProcedimiento');
    var idCiclo=gE('id');
    var comboCicloAnt=gE('_idCicloAntint');
    var idCicloAnt=comboCicloAnt.options[comboCicloAnt.selectedIndex].value;	
	if(idCiclo=='-1')
    {
    	idCiclo='idRegPadre';
        
    }

    hfuncion.value='activarCiclo(idRegPadre)|asignaCicloAnterior('+idCiclo.value+','+idCicloAnt+')';
}
