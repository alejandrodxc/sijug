Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	prueba();
    gE('_cve_materiavch').focus();
    mBloques();
    horas();
    mostrarDatosC();
    var idMateria=gE('id').value;
    var oculto=true;
    if(idMateria=='-1')
    {
    	oculto=true;
    }
    else
    {
    	var compartida=gE('esCompartida').value;
        if(compartida=='1')
      		oculto=false;
        else
        	oculto=true;    
    }
   if(oculto==true)
   	  gEx('idTab').hideTabStripItem(2);
   //gEx('idTab').unhideTabStripItem(1);
   crearGridProgramas();
   validarGeneraGrupos();
}

function validarFrm(formulario)
{
	var almacen=Ext.getCmp('gridProgramas').getStore();
    var tamanoAlm=almacen.getCount();
    if(tamanoAlm==0)
    {
    	gE('cadenaCompartida').value='';
    }
    else
    {
    	var x;
        var cadenaProg='';
        for(x=0;x< tamanoAlm;x++)
        {
        	var idPrograma=almacen.getAt(x).get('idPrograma');
            if(cadenaProg=='')
            	cadenaProg=idPrograma;
            else
            	cadenaProg+=','+idPrograma;    
        }
        gE('cadenaCompartida').value='';
        gE('cadenaCompartida').value=cadenaProg;
    }
    var cmbTotal=gE('_horasTotalint');
    var horasTotal=parseInt(cmbTotal.options[cmbTotal.selectedIndex].value);
    
    var idMateria=gE('id').value;
    var idPrograma=gE('idPrograma').value;
    var claveM=gE('_cve_materiavch').value;
    function funcAjaxM()
    {
    	var arrResp=peticion_http.responseText.split('|');
        if(arrResp[0]=='1')
        {
        	
            if(horasTotal!=0 && horasTotal!=-1)
            {
                var tipoHoras=gE('considerarLibres').value;
                
                var cmbPracticas=gE('_horasPracticasint');
                var practicas=parseInt(cmbPracticas.options[cmbPracticas.selectedIndex].value);
                if(practicas==-1)
                    practicas=0;
                
               if(tipoHoras=='1')
               {
                    var cmbPracticasLibres=gE('_horasPracticasLibresint');
                    var practicasLibres=parseInt(cmbPracticasLibres.options[cmbPracticasLibres.selectedIndex].value);
                    if(practicasLibres==-1)
                        practicasLibres=0;
                
                    practicas=practicas+practicasLibres;
                }   
                
                var cmbTeoricas=gE('_horasTeoricasint');
                var teoricas=parseInt(cmbTeoricas.options[cmbTeoricas.selectedIndex].value);
                if(teoricas==-1)
                    teoricas=0;
                
               if(tipoHoras=='1')
               {
                    var cmbTeoricasLibres=gE('_horasTeoricasLibresint');
                    var teoricasLibres=parseInt(cmbTeoricasLibres.options[cmbTeoricasLibres.selectedIndex].value);
                    if(teoricasLibres==-1)
                        teoricasLibres=0;
                
                teoricas=teoricas+teoricasLibres;
               } 
                
                
                var totalDeHoras=practicas+teoricas;
                if(totalDeHoras==horasTotal)
                {
                     if(validarFormularios(formulario))
                     {
                          gE(formulario).submit();
                     }
                }
                else
                {
                    if(totalDeHoras < horasTotal)
                    {
                         Ext.MessageBox.alert(lblAplicacion,'La suma de Horas Te&oacute;ricas y Horas Pr&aacute;cticas es menor al Total de Horas');
                         return;
                    }
                    else
                    {
                         Ext.MessageBox.alert(lblAplicacion,'La suma de Horas Te&oacute;ricas y Horas Pr&aacute;cticas es mayor al Total de Horas');
                         return;
                    
                    }
                }
            }
            else
            {
                if(validarFormularios(formulario))
                 {
                      gE(formulario).submit();
                 }
            }
        }
        else
    	{
        	if(arrResp[0]=='2')
            {
            	Ext.MessageBox.alert(lblAplicacion,'La clave de la materia ya ha sido registrada');
                return;
            }
            else
            {
            	Ext.MessageBox.alert(lblAplicacion,lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br/>'+arrResp[0]);
            }    
        }
    }
	obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjaxM,'POST','funcion=105&claveM='+claveM+'&idMateria='+idMateria+'&idPrograma='+idPrograma,true);
    
    	
}

function regresar(idMateria,idCiclo)
{
	var arrP=[['idMateria',idMateria],['idCiclo',idCiclo]];
	enviarFormularioDatos('../Administracion/fichaMateria.php',arrP);										
}


function mostrarElementos()
{
	var idMapaCurricular=gE('idMapaCurricuar').value;
    if(idMapaCurricular==-1)
    {
    	mE('tipoHorario');
	}
    else
    {
    	var tipo=gE('tipoHorario');
        combo.removeAttribute('val');
        oE('tipoHorario');
    
    }
}

function horas()
{
   
    var cmbTotal=gE('_horasTotalint');
    var horasTotal=parseInt(cmbTotal.options[cmbTotal.selectedIndex].value);
    
    var cmbSemana=gE('_horasSemanaint');
    var horasSemana=parseInt(cmbSemana.options[cmbSemana.selectedIndex].value);
    mostrar();
    
    if(horasTotal!=0 && horasTotal!=-1)
    {
        if(horasSemana!=0 && horasSemana!=-1)
        {
        	if(horasSemana> horasTotal)
            {
            	Ext.MessageBox.alert(lblAplicacion,'El numero de horas a la semana no puede ser mayor al total de horas');
                selElemCombo(cmbSemana,'-1');
                return;
            }
        }
    }

}

function mostrar()
{
//	var cmbTotal=gE('_horasTotalint');
//    var horasTotal=parseInt(cmbTotal.options[cmbTotal.selectedIndex].value);
//    
//    var tipoHoras=gE('considerarLibres').value;
//    
//   // if(tipoHoras=='1')
////    {
////        if(horasTotal!=0 && horasTotal!=-1 )
////        {
////            var cmbPracticas=gE('_horasPracticasint');
////            var cmbTeoricas=gE('_horasTeoricasint');
////            cmbPracticas.setAttribute('val','obl');
////            cmbTeoricas.setAttribute('val','obl');
////            mE('teoricaslibres');
////            mE('practicasLibres');
////        }
////        else
////        {
////            var cmbPracticas=gE('_horasPracticasint');
////            var cmbTeoricas=gE('_horasTeoricasint');
////            cmbPracticas.removeAttribute('val');
////            cmbTeoricas.removeAttribute('val');
////            selElemCombo(cmbPracticas,'-1');
////            selElemCombo(cmbTeoricas,'-1');
////            oE('teoricaslibres');
////            oE('practicasLibres');
////        }
////    }
////    else
////    {
////    	if(horasTotal!=0 && horasTotal!=-1 )
////        {
////            var cmbPracticas=gE('_horasPracticasint');
////            var cmbTeoricas=gE('_horasTeoricasint');
////            cmbPracticas.setAttribute('val','obl');
////            cmbTeoricas.setAttribute('val','obl');
////            mE('teoricasNormal');
////            mE('practicasNormal');
////        }
////        else
////        {
////        	var cmbPracticas=gE('_horasPracticasint');
////            var cmbTeoricas=gE('_horasTeoricasint');
////            cmbPracticas.removeAttribute('val');
////            cmbTeoricas.removeAttribute('val');
////            selElemCombo(cmbPracticas,'-1');
////            selElemCombo(cmbTeoricas,'-1');
////            oE('teoricasNormal');
////            oE('practicasNormal');
////        }    
////    }
//    mostrarCreditos(); 
}


function suma(combo)
{
    var tipoHoras=gE('considerarLibres').value;
    
    var cmbTotal=gE('_horasTotalint');
    var horasTotal=parseInt(cmbTotal.options[cmbTotal.selectedIndex].value);
    
    var cmbPracticas=gE('_horasPracticasint');
    var horasPracticas=parseInt(cmbPracticas.options[cmbPracticas.selectedIndex].value);
    if(horasPracticas==-1)
    	horasPracticas=0;
        
    if(tipoHoras=='1')
    {
        var cmbPracticasLibres=gE('_horasPracticasLibresint');
        var horasPracticasLibres=parseInt(cmbPracticasLibres.options[cmbPracticasLibres.selectedIndex].value);
        if(horasPracticasLibres==-1)
            horasPracticasLibres=0;
        
        horasPracticas=horasPracticas+horasPracticasLibres;
   }
        
    var cmbTeoricas=gE('_horasTeoricasint');
    var horasTeoricas=parseInt(cmbTeoricas.options[cmbTeoricas.selectedIndex].value);
    if(horasTeoricas==-1)
    	horasTeoricas=0;
    
    if(tipoHoras=='1')
    {
        var cmbTeoricasLibres=gE('_horasTeoricasLibresint');
        var horasTeoricasLibres=parseInt(cmbTeoricasLibres.options[cmbTeoricasLibres.selectedIndex].value);
        if(horasTeoricasLibres==-1)
            horasTeoricasLibres=0;
        
        horasTeoricas=horasTeoricas+horasTeoricasLibres;
    }
    
    var totalHoras=horasTeoricas+horasPracticas;
    if(totalHoras> horasTotal)
    {
    	Ext.MessageBox.alert(lblAplicacion,'La suma de horas Te&oacute;ricas y Pr&aacute;cticas  no puede ser mayor al total de horas');
        selElemCombo(combo,'-1');
        //selElemCombo(c,'-1');
        return;
    }

}

function mBloques()
{
	var banderaValidar=gE('validarEsquema').value;
    //alert(banderaValidar);
    if(banderaValidar=='1')
    {
        var combo=gE('_esquemaEvaluacionint');
        var opcion=combo.options[combo.selectedIndex].value;
        if(opcion=='0')
        {
            var comboBloques=gE('_bloquesint');
            combo.setAttribute('val','obl');
            mE('bloques');
        }
        else
        {
            var comboBloques=gE('_bloquesint');
            comboBloques.removeAttribute('val');
            selElemCombo(comboBloques,'-1');
            oE('bloques');
        }
    }
}

function mostrarCreditos()
{
	var manejaCreditos=gE('manejaCreditos').value;
    var tipoC=gE('tipoCreditos').value;
    
    if(manejaCreditos=='1')
    {
       // var cmbTotal=gE('_horasTotalint');
//        var horasTotal=parseInt(cmbTotal.options[cmbTotal.selectedIndex].value);
//        if(horasTotal!='-1')
//        {
           if(tipoC!='1')
           {
           	   var cmbCreditos=gE('_nCreditosint');
           	   cmbCreditos.setAttribute('val','obl');
           }
           mE('creditos');
       // }
//        else
//        {
           //var cmbCreditos=gE('_nCreditosint');
//           cmbCreditos.removeAttribute('val');
//           selElemCombo(cmbCreditos,'-1');
//           oE('creditos');
       // }
    }  
    else
    {
    	if(tipoC!='1')
        {
            //var cmbCreditos=gE('_nCreditosint');
        	//cmbCreditos.removeAttribute('val');
        	//selElemCombo(cmbCreditos,'-1');
         }   
        oE('creditos');
    }
}


function validarCompartida()
{
	var idMateria=gE('id').value;
    var combo=gE('_compartidaint');
    var opcion=combo.options[combo.selectedIndex].value; 
    if(idMateria!=-1)
    {
        var idCiclo=gE('_cicloint').value;
        if(opcion==0 || opcion==-1)
        {
            var almacen=Ext.getCmp('gridProgramas').getStore();
            var tamano=almacen.getCount();
            var mensaje='';
            if(tamano>0)
            {
                var x;
                for(x=0;x< tamano;x++)
                {
                	var registro=almacen.getAt(x);
                    var nAlumn=registro.get('noAlumnos');
                    var nombreProg=registro.get('nombrePrograma');
                    if(nAlumn>0)
                    {
                    	if(mensaje=='')
                        	mensaje='Programa : '+nombreProg+' No. Alumnos: '+nAlumn;
                        else
                        	mensaje+='<br />Programa : '+nombreProg+' No. Alumnos: '+nAlumn;    
                    }   
                }
                
                if(mensaje!='')
                {
                    msgBox('No puede cambiar la configuracion de esta materia ya que cuenta<br/> con alumnos inscritos en los siguientes programas:<br />'+mensaje);
                    selElemCombo(combo,'1');
                    return;
                }
                else
                {
                	gEx('idTab').hideTabStripItem(2);
                }
            }
            else
            {
            	 gEx('idTab').unhideTabStripItem(2);
            }
            //function funcAjax2()
//            {
//                var arrResp=peticion_http.responseText.split('|');
//                
//                if(arrResp[0]==1)
//                {
//                
//                }
//                else
//                {
//                    if(arrResp[0]==2)
//                    {
//                    	function respPregunta(btn2)
//                        {
//                            if(btn2=='yes')
//                            {
//                                function funcAjax3()
//                                {
//                                    var arrResp=peticion_http.responseText.split('|');
//                                    
//                                    if(arrResp[0]==1)
//                                    {
//                                    
//                                    }
//                                    else
//                                    {
//                                    	msgBox(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br/>'+arrResp[0]);
//                                    }
//                                }
//                                obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax3,'POST','funcion=101&idMateria='+idMateria+'&idCiclo='+idCiclo,true);
//                            }
//                            else
//                            {
//                            	selElemCombo(combo,'1');
//                            }
//                        }
//                        Ext.MessageBox.confirm(lblAplicacion,'<table><tr><td>'+arrResp[1]+'<br /></td></tr><tr><td><br/>Si cambia la configuraci&oacute;;n de esta materia compartida eliminara esta informacion</td></tr><tr><td>Desea eliminarla</td></tr></table>',respPregunta);
//                    }   
//                    else
//                    {
//                    	msgBox(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br/>'+arrResp[0]);
//                    }
//                }
//            }
//            obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax2,'POST','funcion=100&idMateria='+idMateria+'&idCiclo='+idCiclo,true);
        }
        else
        {
            if(opcion==1)
            {
                gEx('idTab').unhideTabStripItem(2);
            }
        }
    }
    else
    {
        if(opcion==0 || opcion==-1)
        {
            var almacen=Ext.getCmp('gridProgramas').getStore();
            var tamano=almacen.getCount();
            if(tamano>0)
            {
                function respPregunta(btn2)
                {
                    if(btn2=='yes')
                    {
                    	almacen.removeAll();
                        gEx('idTab').hideTabStripItem(2);
                    }
                    else
                    {
                        selElemCombo(combo,'1');
                    }
                }
                Ext.MessageBox.confirm(lblAplicacion,'Si cambia la configuraci&oacute;;n de esta materia compartida eliminara la informacion <br />de programas con los que esta compartiendo<br />Desea eliminar esta informaci&oacute;n',respPregunta);
            }
            else
            {
                gEx('idTab').hideTabStripItem(2);
            }
        }
        else
        {
        	if(opcion==1)
            {
            	gEx('idTab').unhideTabStripItem(2);
            }    
        }
    }
}


function prueba() 
{
    var tabs = new Ext.TabPanel	(
									{
                                    	id:'idTab',
										renderTo: 'my-tabs',
										activeTab: 0,
										width:750,
										height:1250,
										items:	[	
                                                     {
                                                        contentEl:'tabla1', 
                                                        title:'Datos generales'
                                                     },
                                                     {
                                                        contentEl:'tabla2', 
                                                        title:'ConF. Avanzadas'
                                                     }
                                                     ,
                                                     {
                                                        contentEl:'tabla3', 
                                                        title:'Programas compatiendo',
                                                        id:'tabCompartida'
                                                     }
												]
									}
								);
                               
	//tabs.setActiveTab(parseInt(tab));	
}


function obtenerDisciplina(bandera)
{
	var combo=gE('_campoint');
    var codCampo=combo.options[combo.selectedIndex].value;
    
    function funcAjax3()
    {
    	var arrResp=peticion_http.responseText.split('|');
        if(arrResp[0]=='1')
        {
        	var cadCmb=arrResp[1];
            var arrCmb=eval(cadCmb);
            var combo=gE('_disciplinaint');
			llenarCombo(combo,arrCmb,false);
            if(bandera==1)
            {
            	var disciplina=gE('diciplinaBase').value;
        		disciplina=parseInt(disciplina);
                selElemCombo(combo,disciplina);
                obtenerSubD(1);
            }
        }
        else
    	{
        	Ext.MessageBox.alert(lblAplicacion,lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br/>'+arrResp[0]);
        }
    }
	obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax3,'POST','funcion=102&codCampo='+codCampo,true);
}

function obtenerSubD(bandera)
{
	var combo=gE('_disciplinaint');
    var codDisciplina=combo.options[combo.selectedIndex].value;
    function funcAjax4()
    {
    	var arrResp=peticion_http.responseText.split('|');
        if(arrResp[0]=='1')
        {
        	var cadCmb=arrResp[1];
            var arrCmb=eval(cadCmb);
            var combo=gE('_subDisciplinaint');
			llenarCombo(combo,arrCmb,false);
            if(bandera==1)
            {
                var subD=gE('subDbase').value;
                subD=parseInt(subD);
                selElemCombo(combo,subD);
            }
        }
        else
    	{
        	Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br/>'+arrResp[0]);
        }
    }
	obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax4,'POST','funcion=103&codDisciplina='+codDisciplina,true);
}

function mostrarDatosC()
{
    var idMateria=gE('id').value;
	if(idMateria!='-1')
    {
        var campo=gE('campoBase').value;
        campo=parseInt(campo);
    	var disciplina=gE('diciplinaBase').value;
        disciplina=parseInt(disciplina);
        var subD=gE('subDbase').value;
        subD=parseInt(subD);
        
        var cmbCampo=gE('_campoint');
        selElemCombo(cmbCampo,campo);
        
        var codCampo=cmbCampo.options[cmbCampo.selectedIndex].value;
        
        obtenerDisciplina(1);
        //var cmbDisciplina=gE('_disciplinaint');
//        selElemCombo(cmbDisciplina,disciplina);
//        var cmbSubD=gE('_subDisciplinaint');
//        selElemCombo(cmbSubD,subD);
        
    }
}

function crearGridProgramas()
{
    var dsRegistrosP=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idMatComVSPrograma'},
                                                                  {name: 'idPrograma'},
                                                                  {name: 'nombrePrograma'},
                                                                  {name: 'noAlumnos'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAdministracion.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosP.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=41;
                                        proxy.baseParams.idMateria=gE('id').value;
                                        proxy.baseParams.idPrograma=gE('idPrograma').value;
                                    }
                        );
   
    
  // var filters = new Ext.ux.grid.GridFilters	(
//                                                  {
//                                                      filters:	[
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'nombrePrograma' 
//                                                                      },
//                                                                      {
//                                                                          type:'string',
//                                                                          dataIndex:'folioPedido' 
//                                                                      }
//                                                                  ]
//                                                  }
//                                              ); 
    
    var columnaCheck=new Ext.grid.CheckboxSelectionModel();	 
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        columnaCheck,
														{
															header:'Programa',
															width:350,
															sortable:true,
															dataIndex:'nombrePrograma',
                                                            align:'left'
														},
                                                        {
															header:'Alumnos inscritos',
															width:150,
															sortable:true,
															dataIndex:'noAlumnos',
                                                            align:'right'
														}
													]
												);
                                         
	var tblGridP=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridProgramas',
                                                            title:'Programas con los que se esta compartiendo la materia',
                                                            store:dsRegistrosP,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm: columnaCheck,
                                                            renderTo:'divProgramas',
                                                            height:450,
                                                            width:580,
                                                            tbar:[
                                                            	  {
                                                                      text:'Agregar Programa',
                                                                      icon:'../images/add.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              mostrarVentanaAgregar();
                                                                          }
                                                                  }
                                                                  ,
                                                                  {
                                                                      text:'Quitar programa',
                                                                      id:'removerB',
                                                                      icon:'../images/cancel_round.png',
                                                                      cls:'x-btn-text-icon',
                                                                      handler:function()
                                                                          {
                                                                              quitarPrograma();
                                                                          }
                                                                  }
                                                                 ]
                                                        }
                                                    );
		
    dsRegistrosP.load()  ;
    //return tblGridP;     
}

function mostrarVentanaAgregar()
{
	var idMateria=gE('id').value;
    var idPrograma=gE('idPrograma').value;
    function funcAjax()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
             var arreglo=eval(resp[1]);
             var tamano=arreglo.length;
             if(tamano==0)
             {
             	msgBox(lblAplicacion,'No existen programas disponibles');
                return;
             }
             mostrarVentanaGridProgramas(arreglo,tamano);
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax, 'POST','funcion=42&idMateria='+idMateria+'&idPrograma='+idPrograma,true)
}


function mostrarVentanaGridProgramas(arreglo,tamano)
{
	var almacenDestino=Ext.getCmp('gridProgramas').getStore();
    var arregloValidos=new Array();
    var x;
    
    for(x=0;x< tamano;x++)
    {
    	var nomP=arreglo[x][1];
        var idProg=arreglo[x][0];
		var pos=obtenerPosFila(almacenDestino,'idPrograma',idProg);    
        if(pos==-1)
        {
        	var obj=[-1,idProg,nomP,0];
            arregloValidos.push(obj);
        }
        
    }
    
    var gridProg=crearGridNuevosP(arregloValidos);
    var formA = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                      {
                                                         x:0,
                                                         y:10,
                                                         xtype:'panel',
                                                         items:[
                                                                    gridProg
                                                               ]
                                                      }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Programas no asociados',
                                        //closable : false,
										width: 450,
										height:400,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: formA,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var almacenDestino=Ext.getCmp('gridProgramas').getStore();
                                                                                    var cadena='';
                                                                                    var filas=Ext.getCmp('gridProgramasNoAsociados').getSelectionModel().getSelections();
                                                                                    var tamano=filas.length;
                                                                                    if(tamano==0)
                                                                                    {
                                                                                     	msgBox('Debe seleccionar al menos un programa');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    var x;
                                                                                    for(x=0;x< tamano;x++)
                                                                                    {
                                                                                    	var idP=filas[x].get('idPrograma');
                                                                                        var nP=filas[x].get('nombrePrograma');
                                                                                        var nA='0';
                                                                                        var nRegistro=new nuevoRegistroPrograma(
                                                                                                                                    {
                                                                                                                                          idMatComVSPrograma:-1,
                                                                                                                                          idPrograma:idP,
                                                                                                                                          nombrePrograma:nP,
                                                                                                                                          noAlumnos:nA
                                                                                                                                    }
                                                                                                                                );
                                                                                        almacenDestino.add(nRegistro);                      
                                                                                    } 
                                                                                    
                                                                                     ventana.close();
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]
									}
								);
     ventana.show();                           
}

function crearGridNuevosP(arregloValidos)
{
    var dsNuevos= new Ext.data.SimpleStore	(
													{
														fields:	[
																  {name: 'idMatComVSPrograma'},
                                                                  {name: 'idPrograma'},
                                                                  {name: 'nombrePrograma'},
                                                                  {name: 'noAlumnos'}
																]
													}
												);

	dsNuevos.loadData(arregloValidos);
	
	var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
	var cmUnidades= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            columnaCheck,
                                                            {
                                                                header:'Programa',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'nombrePrograma'
                                                            }
                                                        ]
                                                    );
	var tblNuevos=new Ext.grid.GridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'gridProgramasNoAsociados',
                                                        store:dsNuevos,
                                                        frame:true,
                                                        cm: cmUnidades,
                                                        sm:columnaCheck,
                                                        height:350,
                                                        width:420
													}
    											);
   return tblNuevos;                                             
                                                
}

var nuevoRegistroPrograma=Ext.data.Record.create(
													[
                                                        {name: 'idMatComVSPrograma'},
                                                        {name: 'idPrograma'},
                                                        {name: 'nombrePrograma'},
                                                        {name: 'noAlumnos'}
													]
                                                );


function quitarPrograma()
{
	var filas=Ext.getCmp('gridProgramas').getSelectionModel().getSelections();
    var tamano=filas.length;
    var x;
    
    if(tamano==0)
    {
    	msgBox(lblAplicacion,'Debe seleccionar al menos un registro');
        return;
    }
    
    var arregloBorrar=new Array ();
    var mensaje='';
    for(x=0;x<tamano;x++)
    {
    	var noAlumnos=filas[x].get('noAlumnos');
        if(noAlumnos==0)
        {
        	arregloBorrar.push(filas[x]);
        }
        else
        {
        	if(mensaje=='')
            	mensaje='Programa:'+filas[x].get('nombrePrograma')+'  No.Alumnos'+noAlumnos;
            else
            	mensaje+='<br/>Programa:'+filas[x].get('nombrePrograma')+'  No.Alumnos'+noAlumnos;   
        }
    
    }
    Ext.getCmp('gridProgramas').getStore().remove(arregloBorrar);
    
    if(mensaje!='')
    {
    	msgBox('Los siguientes registros no se pueden borrar porque tiene alumnos inscritos<br/>'+mensaje);
        return;
    }
}

function validarGeneraGrupos()
{
	var genera=gE('generaGrupos').value;
    if(genera!=2)
    {
    	var min=gE('minGrupo');
        min.removeAttribute('val');
        var max=gE('maxGrupo');
        max.removeAttribute('val');
        var nGrupos=gE('noGrupos');
        nGrupos.removeAttribute('val');
    }
}
