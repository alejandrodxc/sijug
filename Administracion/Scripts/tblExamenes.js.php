<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>



function eliminarExamen(idExamen)
{
	function resp(btn)
    {
        function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                var fila=gE('fila_'+idExamen);
                fila.parentNode.removeChild(fila);
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax, 'POST','funcion=106&idExamen='+idExamen,true);
    }
    Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer eliminar este registro?',resp);
}

function modificar(id)
{
	//gE('idExamen').value=idExamen;
    var arrDatos=[['idExamen',id]];
	enviarFormularioDatos('../Administracion/examenes.php',arrDatos);
    
    //gE('frmEnvio').submit();
}

function nuevo(id)
{
    var arrDatos=[['idExamen',id]];
	enviarFormularioDatos('../Administracion/examenes.php',arrDatos);
}