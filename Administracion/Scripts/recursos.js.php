<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


Ext.onReady(inicializar);
var nUsuario;
function inicializar()
{
   oE('areas');
   oE('otrosTipos');
   tipoRecurso();
	inicializarCombos();   
}
function ocultarResponsable()
{
 if(gE('apartable').options[gE('apartable').selectedIndex].value==1)
 {
  oE('filaResponsable');
}
 else
 {
    mE('filaResponsable');
 }
}

function tipoRecurso()
{
	var tipo=gE('_idTipoRecursoint');
	tipo.value=tipo.options[tipo.selectedIndex].value;
    if(tipo.value==1)
    {
    	 oE('otrosTipos');
		 mE('areas');
    }
    else
    {
    	if(tipo.value>1)
		{
		oE('areas');
        mE('otrosTipos');
		}
    }
	
}

function funValidar()
{
	validar('frmEnvio');
}

  function validar(f, campo) 
{		if(valor!=1)
		{
			var valida = false;
			if (f[campo].length == undefined) valida = f[campo].checked
			else for (var i = 0, total = f[campo].length; i < total; i ++)
			valida = valida || f[campo][i].checked;
			if (!valida) alert('Debe seleccionar al menos una opcion');
			return valida;
		}
}

function validar(formulario)
{
	if(validarFormularios(formulario))
	{
		var listaParticipantes=gE('listaParticipantes');
    var x;
    var listadoRolesP=gE('listadoRolesP');
    var listadoUsuariosP=gE('listadoUsuariosP');
   // var listadoParticipantesTxt=gE('listadoParticipantesTxt');
    var cadRolesP='';
    var cadUsuariosP='';
    var cadParticipantesTxt='';
    var valor;
    var cadena;
    var arrValor;
    for(x=0;x<listaParticipantes.options.length;x++)
    {
    	 valor=listaParticipantes.options[x].value;
        arrValor=valor.split('|');
        if(arrValor[1]=='1')
        {
        	if(cadUsuariosP=='')
            	cadUsuariosP=arrValor[0];
            else
           		cadUsuariosP+=','+arrValor[0];
        }
        else
        {
        	if(cadRolesP=='')
            	cadRolesP="'"+arrValor[0]+"'";
            else
           		cadRolesP+=",'"+arrValor[0]+"'";
        }
        
        if(cadParticipantesTxt=='')
        	cadParticipantesTxt=listaParticipantes.options[x].text;
        else
        	cadParticipantesTxt+='<br>'+listaParticipantes.options[x].text;
        
    }
	  listadoRolesP.value=cadRolesP;
    listadoUsuariosP.value=cadUsuariosP;
    listadoParticipantesTxt=cadParticipantesTxt;
    //gE('listadoParticipantesTxt').value=cadParticipantesTxt;  
		gE(formulario).submit();
	}
}
 function ponerCBusqueda(cBusqueda)
{
	gE('cBusqueda').value=cBusqueda;
	gE('listaParticipantes').value="";
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
											]
										);
	var parametros=	{
						funcion:'1',
						criterio:''
					};
	inicializarCmbNombre(pPagina,lector,parametros);
}

function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
		gE('idUsuario').value='-1';
		var aNombre=Ext.getCmp('cmbNombre').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=gE('cBusqueda').value;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														applyTo:'Nombre',
														itemSelector:'div.search-item',
														listWidth :500
													}
												 );
	function funcElemSeleccionado(combo,registro)
	{	
		nUsuario=registro.get('Nombre');
		gE('idUsuario').value=registro.get('idUsuario');
	}
	
	comboNombre.on('select',funcElemSeleccionado);	
}
function removerParticipante()
{
	var listaParticipantes=gE('listaParticipantes');
	if(listaParticipantes.selectedIndex==-1)
	{
		msgBox('Debe seleccionar el participante a remover');
		return;
	}

	function resp(btn)
	{
		if(btn=='yes')
		{
			listaParticipantes.options[listaParticipantes.selectedIndex]=null;
		}
	}
	msgConfirm('Est&aacute; seguro de querer remover al participante seleccionado?',resp);
}

var nav4 = window.Event ? true : false;

//function acceptNum(evt)
//{   
//
//   var key = nav4 ? evt.which : evt.keyCode;   
//  return (key <= 13 || (key>= 48 && key <= 57));
//
//}

function agregarRol()
{

	<?php
		$consulta="select concat(idRol,'_',extensionRol),nombreGrupo from 8001_roles where vistosAdmin=1 and idIdioma=".$_SESSION["leng"]." order by nombreGrupo";
		$arrRoles=uEJ($con->obtenerFilasArreglo($consulta));
	?>
    var arrRoles=<?php echo $arrRoles;?>;
    var cmbExtensiones=crearComboExt('cmbExtensiones',[],100,35,250);
	cmbExtensiones.hide();
	var cmbRoles=crearComboExt('cmbRoles',arrRoles,100,5,250);
    function rolSeleccionado(combo,registro,indice)
    {
    	cmbExtensiones.reset();
    	var idRegistro=registro.get('id');
        var arrId=idRegistro.split('_');
        if(arrId[1]!=0)
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
					var arrExtensiones=eval(arrResp[1]);
                    cmbExtensiones.getStore().loadData(arrExtensiones);                
                	cmbExtensiones.show();
		            Ext.getCmp('lblExtension').show();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=20&extension='+arrId[1],true);
        
        	
        }
        else
        {
        	cmbExtensiones.hide();
            Ext.getCmp('lblExtension').hide();
        }
        
    }
    
    cmbRoles.on('select',rolSeleccionado);
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
													 	{
                                                        	id:'lblRol',
                                                            x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Rol:'
                                                        },
                                                        cmbRoles,
                                                        {
                                                        	id:'lblExtension',
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'Extensi&oacute;n:',
                                                            hidden:true
                                                        },
                                                        cmbExtensiones
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar rol',
										width:380,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var rol=cmbRoles.getValue();
                                                                var arrId=rol.split('_');
                                                                var extension='0';
                                                                if(arrId[1]!=0)
                                                                	extension=cmbExtensiones.getValue();	
                                                                if(extension=='')
                                                                {
                                                                	function resp()
                                                                    {
                                                                    	cmbExtensiones.focus();
                                                                    }
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe seleccionar una extensi&oacute;n del rol',resp);
                                                                    return;
                                                                }
                                                                var listRoles=gE('listaParticipantes');
                                                                var codigoRol=arrId[0]+'_'+extension;
                                                                var rolExiste=existeRol('listaParticipantes',codigoRol);
                                                                
                                                                if(!rolExiste)
                                                                {
                                                                	
                                                                	var option=document.createElement('option');
                                                                    option.value=codigoRol+'|2';
                                                                    var nExtension=cmbExtensiones.getValue();
                                                                    var txtExtension='';
                                                                    if(nExtension!='')
                                                                    {
                                                                    	txtExtension=' ('+cmbExtensiones.getRawValue()+')';
                                                                    }
                                                                    option.text=cmbRoles.getRawValue()+txtExtension;
                                                                    listRoles.options[listRoles.options.length]=option;
                                                                }
                                                                else
                                                                {
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','El rol seleccionado ya se encuentra agregado a la slista de participantes');
                                                                    return;
                                                                }
                                                                
                                                                ventana.close();
																
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();

}

function existeRol(idCombo,valor)
{
	var combo=gE(idCombo);
    var x;
    for(x=0;x<combo.options.length;x++)
    {
    	if(combo.options[x].value==valor)
        	return true;
    }
    return false;

}

function cargarRecursos(combo)
{
	var cmbApartable=gE('apartable');
	var idCategoria=combo.options[combo.selectedIndex].value;
    var apartable=cmbApartable.options[cmbApartable.selectedIndex].value;
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	
        	var _idInmuebleint=gE('_idInmuebleint');
            llenarCombo(_idInmuebleint,eval(arrResp[1]),true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax, 'POST','funcion=33&idCategoria='+idCategoria+'&apartable='+apartable,true);
    
    
    

}