<?php 
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
Ext.onReady(inicializar);

function inicializar()
{
    var valida1=parseInt(gE('filas').value);
    var valida2=parseInt(gE('filas2').value);
    if((valida1 > 0) || (valida2 > 0))
    {
    	prueba();
    }    
}


function prueba() 
{
 
	var etiqueta=gE('etiqueta').value;
    var cadenaTabla='';
    var filas=parseInt(gE('filas').value);
    var filas2=parseInt(gE('filas2').value);
	
   
    
    if(Ext.isIE7)
    {
    
    
    var tabs = new Ext.TabPanel	(
									{
										renderTo: 'my-tabs',
										activeTab: 0,
										width:900,
										height:12000,
										items:	[	 
													
                                                    {
                                                    	id:'tab2',
														contentEl:'tabla2', 
														title:etiqueta+ '&nbsp;del Mapa Curricular'
													},
													
													{
                                                    	id:'tab1',
														contentEl:'tabla1',
														title:etiqueta+ '&nbsp;Compartida(o)s'
													}
												]
									}
								);
	
    }
    else
    {
    
     var tabs = new Ext.TabPanel	(
									{
										renderTo: 'my-tabs',
										activeTab: 0,
										width:900,
                                        height:9000,
										autoScroll:true,
										items:	[	 
													
                                                    {
                                                    	id:'tab2',
														contentEl:'tabla2', 
														title:etiqueta+ '&nbsp;del Mapa Curricular'
													},
													
													{
                                                    	id:'tab1',
														contentEl:'tabla1',
														title:etiqueta+ '&nbsp;Compartida(o)s'
													}
												]
									}
								);
     if(tabs.getInnerHeight()< 4200)
        {
                tabs.setHeight(4200);
        }                           
    
    }
    
	if(filas==0)
    {
    	tabs.hideTabStripItem(1);
        tabs.setActiveTab(0);
        
    }
    if(filas2==0)
    {
    	tabs.hideTabStripItem(0);
        tabs.setActiveTab(1);
    }
    
   
	
}


function validarFrm(form)
{
	if(validarFormularios(form))
    {
        var numero=0;
        var arreglo=document.getElementsByName('idMateria');
        var tamano=arreglo.length;
        for(x=0;x< tamano;x++)
        {
            if(arreglo[x].checked)
            {
                numero=numero+1
            }   
        }
        
        if(numero>0)
        {
        	var idMateria=gE('idMateria').value;
            function funcAjax5()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    mostrarVentanaTipoHorario(1,arrResp[1]);
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax5, 'POST','funcion=47&idMateria='+idMateria,true);
            //mostrarVentanaTipoHorario(1);
        }
        else
        {
        	msgBox('Debe seleccionar  al menos una  materia ');
        }
    }
 
}

function radioCheck(radio)
{
	if(radio.name=='idMateria')
    {
    	gE('idMateria').value=radio.value;
    }
    else
    {
    	gE('idMateriaC').value=radio.value;
    }    
}

function validarCompartida()
{
     var numero=0;
     var idMapaCurricular=gE('idMapaCurricular').value;
     var numero=0;
     var arreglo=document.getElementsByName('idMateriaC');
     var tamano=arreglo.length;
     for(x=0;x< tamano;x++)
     {
         if(arreglo[x].checked)
         {
             numero=numero+1
         }   
     } 
      
    if(numero> 0)
    {
    	var idMateria=gE('idMateriaC').value;
        var idGrado=gE('idGrado').value;
        var idPadre=gE('idPadre').value;
        
        function funcAjax1()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                function funcAjax3()
                {
                    var resp=peticion_http.responseText;
                    arrResp=resp.split('|');
                    if(arrResp[0]=='1')
                    {
                        mostrarVentanaTipoHorario(0,arrResp[1]);
                    }
                    else
                    {
                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax3, 'POST','funcion=47&idMateria='+idMateria,true);
                
            }
            else
            {
                if(arrResp[0]=='2')
                {
                	function funcAjax()
                    {
                        var resp=peticion_http.responseText;
                        arrResp=resp.split('|');
                        if(arrResp[0]=='1')
                        {
                            regresarPagina();
                        }
                        else
                        {
                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                        }
                    }
                    obtenerDatosWeb('../paginasFunciones/funcionesHorarios.php',funcAjax, 'POST','funcion=24&idMateria='+idMateria+'&idPadre='+idPadre+'&idGrado='+idGrado+'&idMapaCurricular='+idMapaCurricular,true);
                
                }
                else
                {
                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                } 
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesHorarios.php',funcAjax1, 'POST','funcion=25&idMateria='+idMateria+'&idMapaCurricular='+idMapaCurricular,true);
    }
    else
    {
        msgBox('Debe seleccionar al menos una materia');
    }
}

function mostrarVentanaTipoHorario(pertenece,clave)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[
                                                        {
                                                             x:10,
                                                             y:10,
                                                             xtype:'label',
                                                             align:'center',
                                                             html:'Seleccione el tipo de Horario de la Materia:'
                                                         },
                                 new Ext.form.Radio	(
                                                         {
                                                              x:10,
                                                              y:45,
                                                              id:'chksi',
                                                              name:'chk',
                                                              boxLabel:'Horario Cerrado',
                                                              checked:true,
                                                              value:1
                                                         }
                                                     ),
                                   new Ext.form.Radio(
                                                         {
                                                              x:140,
                                                              y:45,
                                                              id:'chkno',
                                                              name:'chk',
                                                              boxLabel:'Horario Abierto',
                                                              value:2
                                                          }
                                                      ),
                                                     
                                                      {
                                                         x:10,
                                                         y:105,
                                                         hidden:true,
                                                         xtype:'textfield',
                                                         id:'claveTxt',
                                                         value:clave
                                                      }
													]
										}
									);

	var ventana = new Ext.Window(
									{
										title: 'Tipo de Horario',
										width: 300,
										height:180,
										minWidth: 200,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptar',
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					
                                                                                    var check=Ext.getCmp('chkno').getValue();
                                                                                    if(check==false)
                                                                                    	var idTipoHorario=1;
                                                                                    else
                                                                                    	var idTipoHorario=2;
                                                                                        
                                                                                    
                                                                                    var clave=Ext.getCmp('claveTxt').getValue();
                                                                                    if(clave==='')
                                                                                    {
                                                                                    	msgBox('Debe indicar la clave de la materia');
                                                                                        return;
                                                                                    }
                                                                                    var paginaEnvio;
                                                                                    var idMapaCurricular=gE('idMapaCurricular').value;
                                                                                    var idGrado=gE('idGrado').value;
                                                                                    var idPadre=gE('idPadre').value;
                                                                                    if(pertenece==1)
                                                                                    {
                                                                                    	var idMateria=gE('idMateria').value;
                                                                                    }   
                                                                                    else
                                                                                    {
                                                                                    	var idMateria=gE('idMateriaC').value;
                                                                                    }
                                                                                    var arrParam=[['confMapa',gE('confMapa').value],['idMapaCurricular',idMapaCurricular],['idGrado',idGrado],['idPadre',idPadre],['idTipoHorario',idTipoHorario],['idMateria',idMateria],['clave',clave]];
                                                                                    
                                                                                    switch(idTipoHorario)
                                                                                    {
                                                                                    
                                                                                        case 1:
                                                                                            paginaEnvio='../programaAcademico/configuracionMateriaCerrado.php';
                                                                                            
                                                                                        break;
                                                                                        case 2:
                                                                                            paginaEnvio='../programaAcademico/configuracionMateriaAbierto.php';
                                                                                            
                                                                                        break;
                                                                                    }
                                                                                    
                                                                                    function funcAjax4()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            enviarFormularioDatos(paginaEnvio,arrParam);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if(arrResp[0]=='2')
                                                                                            {
                                                                                            	msgBox('La clave ingresada ya se en cuentra registrada');
                                                                                                return;
                                                                                            }	
                                                                                            else
                                                                                            {
                                                                                            	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);    
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax4, 'POST','funcion=48&clave='+bE(clave)+'&idMapaCurricular='+idMapaCurricular,true);
                                                                                    
                                                                                    
																				}
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
									}
								);
		ventana.show();
}


//function obtenerGrados()
//{
//    var cmbPrograma=gE('idPrograma');
//    var idPrograma=cmbPrograma.options[cmbPrograma.selectedIndex].value;
//    
//    var idCiclo=gE('idCiclo').value;
//  
//    
//    function funcAjax()
//    {
//      
//        var resp=peticion_http.responseText;
//        arrResp=resp.split('|');
//        if(arrResp[0]=='1')
//        {
//        	var cadCmb=arrResp[1];
//            var arrCmb=eval(cadCmb);
//            var combo=gE('idGrado');
//            rellenarCombo(combo,arrCmb,1);
//        }
//        else
//        {
//            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
//        }
//    }
//    obtenerDatosWeb('../paginasFunciones/funcionesHorarios.php',funcAjax, 'POST','funcion=7&idPrograma='+idPrograma+'&idCiclo='+idCiclo,true);
//}

//function guardar()
//{
//	
//        var numero=gE('checados').value;
//       
//        if(numero>0)
//        {
//              var arreglo=document.getElementsByName('idMateria');
//              var tamano=arreglo.length;
//              for(x=0;x< tamano;x++)
//              {
//                  if(arreglo[x].checked)
//                      var idMateria=arreglo[x].value;
//                      //alert(idMateria);
//              }
//              
//              
//              var idCiclo=gE('idCiclo').value;
//              var idPrograma=gE('idPrograma').value;
//              
//              var idGrado=gE('idGrado').value;
//              var formulario=gE('guardarMateriaPrograma');
//              function funcAjax()
//              {
//             
//              
//              var resp=peticion_http.responseText;
//              arrResp=resp.split('|');
//                  if(arrResp[0]=='1')
//                  {
//                       formulario.submit();
//                  }
//                  else
//                  {
//                      msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
//                  }
//              }
//              obtenerDatosWeb('../paginasFunciones/funcionesMapaCurricular.php',funcAjax, 'POST','funcion=7&idMateria='+idMateria+'&idCiclo='+idCiclo+'&idPrograma='+idPrograma+'&idGrado='+idGrado,true);
//              
//             
//        }
//        else
//        {
//        	alert('no hay checados');
//        }
//    
// 
//}