<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$idPrograma=$_GET["idPrograma"];
	$consulta="select idDocumento from 4020_programaVsDocumento where idPrograma=".$idPrograma;
	$arrDocumentos=$con->obtenerListaValores($consulta);
	if($arrDocumentos=="")
		$arrDocumentos="-1";
	$consulta="select idDocumento,tipo from 4016_documentos where idDocumento not in (".$arrDocumentos.") order by tipo ";
	$arrDocumentos=$con->obtenerFilasArreglo($consulta);
	
	$consulta="select idElementoPerfilAutor,descParticipacion from 953_elementosPerfilesParticipacionAutor where idPerfilAutor=7 order by descParticipacion";
	$arrPar=$con->obtenerFilasArreglo($consulta);
	
	$conAdscip="SELECT Institucion  FROM 801_adscripcion WHERE idUsuario=".$_SESSION["idUsr"];
	$adscU=$con->obtenerValor($conAdscip);
	if($adscU=="")
		$adscU="-1";
	$arrUsuarios="[]";
	
	
	$conSedes="SELECT o.codigoUnidad,unidad FROM 817_organigrama o, 4020_programasVSSede c WHERE o.codigoUnidad=c.sede AND idPrograma=".$idPrograma;
	$arrSedes=uEJ($con->obtenerFilasArreglo($conSedes));
	
	$conUnidades="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion=1 AND unidadPadre='0001' AND codigoUnidad NOT IN (SELECT sede FROM 4020_programasVSSede WHERE idPrograma=".$idPrograma.")";
	$arrUnidades=uEJ($con->obtenerFilasArreglo($conUnidades));
	
	
?>

Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function periodicidadChange(combo)
{
	 var _idCicloint=gE('_idCicloint');
	if(combo.options[combo.selectedIndex].value=='1')
    {
    	oE('filaCiclo');
       
        _idCicloint.setAttribute('val','');
        _idCicloint.selectedIndex=0;
    }
    else
    {
    	mE('filaCiclo');
         _idCicloint.setAttribute('val','obl');
    }
}



function inicializar()
{
    var instUsr=<?php echo $adscU?>;
    if(instUsr=='-1')
    {
    	function funRedir(btn1)
        {
                regresarPagina();
        }	
        msgBox('No se encuentra adscripto a ninguna institucion por lo tanto no puede realizar operaciones sobre esta pagina.<br />',funRedir);
        
    }
    
    mE('trGrid')    ;
    
	crearGridSedes();
    //crearGridUsuarios();
}

function validarFrm(formulario)
{
    var idPrograma=<?php echo $idPrograma?>;
   
    if(idPrograma=='-1')
    {
    	
       	var opcion=gE('global').value;
        if(opcion=='0')
        {
            var codigo=gE('codUn').value;
            if(codigo=='')
            {
                Ext.MessageBox.alert(lblAplicacion,'No cuenta con datos de adscripci&oacute;n<br /> Por lo tanto no puede guardar este programa');
                return;
            }
        }
    }
   
   
    if(validarFormularios(formulario))
	{
    	var x;
        prepararDocumento();
        var cmbTipoPrograma=gE('_idTipoProgramaint');
        try
        {
            var tipo=cmbTipoPrograma.options[cmbTipoPrograma.selectedIndex].value;
        }
        catch(e)
        {
        	var tipo=cmbTipoPrograma.value;
        }
    	if(tipo!=1)
        {
        	var filaDoc=gE('filaDocumentos');
            if(filaDoc!=null)
	            filaDoc.parentNode.removeChild(filaDoc);
        }	
        gE('_idProgramaVinculadoint').name='_idProgramaVinculadoint';
        var validarSedes=prepararSedes();
        if(validarSedes==2)
		{
        	msgBox('Debe indicar las sedes para este programa');
            return;
        }
        
        gE(formulario).submit();
	}
}

function pregunta()
{
	var tipo=gE('_idTipoProgramaint');
    var _idProgramaVinculadoint=gE('_idProgramaVinculadoint');
	tipo.value=tipo.options[tipo.selectedIndex].value;
    if(tipo.value=='2')//Especial
    {
    	mE('valor');
		_idProgramaVinculadoint.setAttribute('val','obl');
        gE('_idProgramaVinculadoint').name='_idProgramaVinculadoint';
        oE('filaDocumentos')
        oE('filaProgramaPre');
         oE('filaProgramaPre2');
        gE('_idProgramaAntint').selectedIndex=0;
    }
    else
    {
    	oE('valor');
        _idProgramaVinculadoint.setAttribute('val','');
        gE('_idProgramaVinculadoint').name='';
        mE('filaDocumentos');
        mE('filaProgramaPre');
         mE('filaProgramaPre2');
    }
}
function removerDoc()
{
	var cmbDoc=gE('listDocumentos');
	var doc=cmbDoc.selectedIndex;
	if(doc==-1)
	{
		Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe elegir el documento a remover');
        return;
	}
    
    function resp(btn)
    {
    	if(btn=='yes')
        	cmbDoc[cmbDoc.selectedIndex]=null;
    }
    Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer remover este documento',resp);
}

function removerRol()
{
	var cmbRoles=gE('listRoles');
	var rol=cmbRoles.selectedIndex;
	if(rol==-1)
	{
		Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe elegir el rol a remover');
        return;
	}
    
    function resp(btn)
    {
    	if(btn=='yes')
        	cmbRoles[cmbRoles.selectedIndex]=null;
    }
    Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer remover este rol',resp);
}

function agregarRol()
{
	<?php
		$consulta="select concat(idRol,'_',extensionRol),nombreGrupo from 8001_roles where idIdioma=".$_SESSION["leng"]." order by nombreGrupo";
		$arrRoles=uEJ($con->obtenerFilasArreglo($consulta));
	?>
    var arrRoles=<?php echo $arrRoles;?>;
    var cmbExtensiones=crearComboExt('cmbExtensiones',[],100,35,250);
	cmbExtensiones.hide();
	var cmbRoles=crearComboExt('cmbRoles',arrRoles,100,5,250);
    function rolSeleccionado(combo,registro,indice)
    {
    	cmbExtensiones.reset();
    	var idRegistro=registro.get('id');
        var arrId=idRegistro.split('_');
        if(arrId[1]!=0)
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
					var arrExtensiones=eval(arrResp[1]);
                    cmbExtensiones.getStore().loadData(arrExtensiones);                
                	cmbExtensiones.show();
		            Ext.getCmp('lblExtension').show();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesUsuarios.php',funcAjax, 'POST','funcion=20&extension='+arrId[1],true);
        
        	
        }
        else
        {
        	cmbExtensiones.hide();
            Ext.getCmp('lblExtension').hide();
        }
        
    }
    
    cmbRoles.on('select',rolSeleccionado);
    
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
													 	{
                                                        	id:'lblRol',
                                                            x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Rol:'
                                                        },
                                                        cmbRoles,
                                                        {
                                                        	id:'lblExtension',
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'Extensi&oacute;n:',
                                                            hidden:true
                                                        },
                                                        cmbExtensiones
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar rol',
										width:380,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var rol=cmbRoles.getValue();
                                                                var arrId=rol.split('_');
                                                                var extension='0';
                                                                if(arrId[1]!=0)
                                                                	extension=cmbExtensiones.getValue();	
                                                                if(extension=='')
                                                                {
                                                                	function resp()
                                                                    {
                                                                    	cmbExtensiones.focus();
                                                                    }
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','Debe seleccionar una extensi&oacute;n del rol',resp);
                                                                    return;
                                                                }
                                                                var listRoles=gE('listRoles');
                                                                var codigoRol=arrId[0]+'_'+extension;
                                                                var rolExiste=existeRol('listRoles',codigoRol);
                                                                
                                                                if(!rolExiste)
                                                                {
                                                                	
                                                                	var option=document.createElement('option');
                                                                    option.value=codigoRol;
                                                                    var nExtension=cmbExtensiones.getValue();
                                                                    var txtExtension='';
                                                                    if(nExtension!='')
                                                                    {
                                                                    	txtExtension=' ('+cmbExtensiones.getRawValue()+')';
                                                                    }
                                                                    option.text=cmbRoles.getRawValue()+txtExtension;
                                                                    listRoles.options[listRoles.options.length]=option;
                                                                }
                                                                else
                                                                {
                                                                	Ext.MessageBox.alert('<?php echo $etj["lblAplicacion"]?>','El rol seleccioando ya se encuentra asignado')
                                                                    return;
                                                                }
                                                                
                                                                ventana.close();
																
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function existeRol(idCombo,valor)
{
	var combo=gE(idCombo);
    var x;
    
    for(x=0;x<combo.options.length;x++)
    {
    	if(combo.options[x].value==valor)
        	return true;
    }
    return false;
}

function agregarDoc()
{
	var tblDocumentos=crearGridDocumentos();
	var form=new Ext.form.FormPanel(
										{
											baseCls: 'x-plain',
											layout:'absolute',
											disabled:false,
											items:
													[
													 	{
                                                        	id:'lblDocumentos',
                                                            x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Elija los documentos a agregar:'
                                                        },
                                                        tblDocumentos
													]
										}
									)
	var ventana=new Ext.Window(
							   		{
										title:'Agregar Documento',
										width:450,
										height:350,
										layout:'fit',
										buttonAlign:'center',
										items:[form],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
																
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                            	var filas=tblDocumentos.getSelectionModel().getSelections();
                                                                if(filas.length==0)
                                                                {
                                                                	msgBox('Debe seleccionar al menos un documento');
                                                                    return;
                                                                }
                                                                
                                                                var x=0;
                                                                var idDocumento;
                                                                var option;
                                                                var combo=gE('listDocumentos');
                                                                for (x=0;x<filas.length;x++)
                                                                {
                                                                	idDocumento=filas[x].get('idDocumento');
                                                                    if(!existeDocumento('listDocumentos',idDocumento))
                                                                    {
                                                                    	option=document.createElement('option');
                                                                        option.value=idDocumento;
                                                                        option.text=filas[x].get('documento');
                                                                        combo.options[combo.length]=option;
                                                                    }
                                                                }
                                                                ventana.close();
                                                                
																
															}
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

var alDocumentos= new Ext.data.SimpleStore	(
											 	{
													fields:	[
															 	{name:'idDocumento'},
																{name:'documento'}
																
															]
												}
                                             )

function crearGridDocumentos()
{
    var arrDocumentos=<?php echo $arrDocumentos?>;
    alDocumentos.loadData(arrDocumentos);	
    var colCheck=new Ext.grid.CheckboxSelectionModel();	
	var cmDocumentos= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            colCheck,
                                                            {
                                                                header:'Documento',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'documento'
    
                                                            }
                                                        ]
                                                    );
											
												
	tblDocumentos=	new Ext.grid.GridPanel	(
                                                {
                                                    x:40,
                                                    y:45,
                                                    id:'tblDocumentos',
                                                    store:alDocumentos,
                                                    frame:true,
                                                    cm: cmDocumentos,
                                                    sm:colCheck,
                                                    height:190,
                                                    width:360
                                                }
                
                                            );
	return tblDocumentos;
}

function existeDocumento(idCombo,valor)
{
	var combo=gE(idCombo);
    var x;
    
    for(x=0;x<combo.options.length;x++)
    {
    	if(combo.options[x].value==valor)
        	return true;
    }
    return false;
}

function prepararDocumento()
{
	var combo=gE('listDocumentos');
    if(combo!=null)
    {
        var x;
        var listadoDoc='';
        for(x=0;x<combo.length;x++)
        {
            if(listadoDoc=='')
                listadoDoc=combo.options[x].value;
            else
                listadoDoc+=','+combo.options[x].value;
        }
        gE('listadoDocumentos').value=listadoDoc;
    }
}

function prepararResponsables()
{
	var muestraC=gE('mCmbA').value;
    if(muestraC=='1')
    {
        var cmbM=gE('global');
        var opcion=cmbM.options[cmbM.selectedIndex].value;
    }
    else
    {
        var opcion=gE('global').value;
    }
    
    var y;
    var responsables=Ext.getCmp('responsables');
    var listadoUsuarios='';
    for(y=0;y<responsables.getStore().getCount();y++)
    {
        var fila=responsables.getStore().getAt(y);
        var acciones=fila.get('acciones');
        if(acciones=='')
        {
            acciones='00'
        }
        
        var sedes=fila.get('sedes');
        if(sedes=='')
        {
        	if(opcion==0)
            {
            	sedes='00'
            }
            else
            {
            	msgBox('El usuario <b>'+fila.get('nombre')+'</b> no cuenta con sedes registradas por lo tanto no puede ser guardado')
                return 2;
            }       
        }
        else
        {
        	var cadenaSedes='';
            var almacenSedes=Ext.getCmp('sedesG').getStore();
            var arregloS=sedes.split('|');
            var tamanoS=arregloS.length;
            var x;
            for(x=0;x< tamanoS;x++)
            {
               
                var arreReg=arregloS[x].split('!');
                var codU=arreReg[0];
                var pos=obtenerPosFila(almacenSedes,'codigoUnidad',codU);
                if(pos!=-1)
                {
                    if(cadenaSedes=='')
                    	cadenaSedes=arreReg[0]+'!'+arreReg[1]+'!'+arreReg[2];
                    else
                    	cadenaSedes+='|'+arreReg[0]+'!'+arreReg[1]+'!'+arreReg[2];    
                }
            }
        }
        var manG=fila.get('manejoG');
        if(manG=='')
        {
        	manG='00'
        }
    	var usuario=fila.get('idUsuario')+'_'+fila.get('participacion')+'_'+fila.get('codigoUnidad')+'_'+acciones+'_'+sedes+'_'+manG;
    	if(listadoUsuarios=='')
        	listadoUsuarios=usuario;
        else
        	listadoUsuarios+=','+usuario;
        
    	//alert(listadoUsuarios);
    }
    //alert(listadoUsuarios);
    gE('listadoUsuarios').value=listadoUsuarios;
    //alert(gE('listadoUsuarios').value);
    var listaFinal=gE('listadoUsuarios').value;
    if(listaFinal=='')
    {
    	msgBox('Debe indicar los responsables para este programa');
        return 2;
    }
    
    return 1;
}

var arrParticipacion=<?php echo $arrPar?>;
var arrManejoG=[['1','Si'],['0','No']];

var registroUsuario=Ext.data.Record.create	(
                                                [
                                                    {name: 'idUsuario'},
                                                    {name: 'nombre'},
                                                    {name: 'participacion'},
                                                    {name: 'codigoUnidad'},
                                                    {name: 'acciones'},
                                                    {name: 'sedes'},
                                                    {name: 'manejoG'}
                                                ]
                                            );

function crearGridUsuarios()
{
    var muestraC=gE('mCmbA').value;
   
    var opcion=gE('global').value;
       
    var arregloB=new Array();
    var mostrarB=gE('permisoM').value;
    if(mostrarB==1)
    {
    	arregloB.push(
        				{
                          text:'Agregar Usuario',
                          icon:'../images/add.png',
                          cls:'x-btn-text-icon',
                          handler:function()
                                  {
                                      mostrarVentanaAgregar();
                                  }
                        }
                     );
                      
        arregloB.push(
        			    {
                          text:'Remover Usuario',
                          icon:'../images/cancel_round.png',
                          cls:'x-btn-text-icon',
                          handler:function()
                                  {
                                      var responsables=Ext.getCmp('responsables');
                                      
                                      var filas=responsables.getSelectionModel().getSelections();
                                      
                                      if(filas.length>0)
                                      {
                                          
                                          function funcConfirmDel(btn)
                                          {
                                              if(btn=="yes")
                                              {
                                                  responsables.getStore().remove(filas);
                                              }
                                          }
                                          Ext.Msg.confirm('<?php echo $etj["lblAplicacion"] ?>','Est&aacute; seguro de querer remover a los usuarios seleccionados?',funcConfirmDel);
                                          
                                      }
                                      else
                                      {
                                          msgBox('Primero debe seleccionar al usuario a remover');
                                      }
                                  }
                      }
                     );     
        if(opcion==0)
        {
            arregloB.push(
                            {
                              text:'Modificar permisos',
                              icon:'../images/update_nw.gif',
                              cls:'x-btn-text-icon',
                              handler:function()
                                      {
                                          var responsables=Ext.getCmp('responsables');
                                          var filas=responsables.getSelectionModel().getSelections();
                                          var x;
                                          
                                          if(filas.length>0)
                                          {
                                                if(filas.length>1)
                                                {
                                                    msgBox('Debe seleccionar solo un elemento');
                                                    return;
                                                }
                                                
                                                var idUsr='';
                                                for(x=0;x< filas.length;x++)
                                                {
                                                    var id=filas[x].get('idUsuario');
                                                    if(idUsr=='')
                                                        idUsr=id;
                                                    else    
                                                        idUsr+=','+id;
                                                }
                                                
                                                var pos=obtenerPosFila(responsables.getStore(),'idUsuario',idUsr);
                                                var cadenaP=responsables.getStore().getAt(pos).get('acciones');
                                                
                                                var aG=cadenaP.indexOf('A');
                                                if(aG==-1)
                                                    aG=false;
                                                else
                                                    aG=true;    
                                                
                                                var mD=cadenaP.indexOf('M');
                                                if(mD==-1)
                                                    mD=false;
                                                else
                                                    mD=true; 
                                                    
                                                var eL=cadenaP.indexOf('E');
                                                if(eL==-1)
                                                    eL=false;
                                                else
                                                    eL=true;
                                                    
                                                var mG=cadenaP.indexOf('G');
                                                if(mG==-1)
                                                    mG=false;
                                                else
                                                    mG=true;              
                                                
                                                ventanaPermisos(aG,mD,eL,pos,mG)
                                          }
                                          else
                                          {
                                              msgBox('Primero debe seleccionar el registro a modificar');
                                          }
                                      }
                          }
                         ); 
        }                
                     
         
    }
    
    var cmbParticipacion=crearComboExt('cmbParticipacion',arrParticipacion);
    var comboGrupos=crearComboExt('comboGrupos',arrManejoG);
    var x;
    var dsUsuarios=<?php echo $arrUsuarios ?>;
    
	var alUsuariosResp=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'idUsuario'},
                                                                    {name: 'nombre'},
                                                                    {name: 'participacion'},
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'acciones'},
                                                                    {name: 'sedes'},
                                                                    {name: 'manejoG'}
                                                                ]
                                                    }
                                                );
	alUsuariosResp.loadData(dsUsuarios);
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var muestraC=gE('mCmbA').value;
    var cadena='';
   
    if(opcion!='-1')
    {
       if(opcion=='0')//una sede
       {   
          var cmResponsables= new Ext.grid.ColumnModel   	(
                                                          [
                                                              new  Ext.grid.RowNumberer(),
                                                              chkRow,
                                                              {
                                                                  header:'Usuario',
                                                                  width:240,
                                                                  sortable:true,
                                                                  dataIndex:'nombre'
                                                              },
                                                              //{
//                                                                  header:'Participaci&oacute;n',
//                                                                  width:100,
//                                                                  sortable:true,
//                                                                  dataIndex:'participacion',
//                                                                  editor:cmbParticipacion,
//                                                                  renderer:formatearParticipacion
//                                                              },
                                                              {
                                                                  header:'Permisos',
                                                                  width:100,
                                                                  sortable:true,
                                                                  dataIndex:'acciones'
                                                              }
                                                             // ,
//                                                              {
//                                                                  header:'Permisos',
//                                                                  width:110,
//                                                                  sortable:true,
//                                                                  dataIndex:'manejoG',
//                                                                  editor:comboGrupos,
//                                                                  renderer:formatearManejoG
//                                                              }
                                                          ]
                                                      );
       }
       else
       {
            if(opcion==1)//muchas sedes
            {
                    var cmResponsables= new Ext.grid.ColumnModel   	(
                                                          [
                                                              new  Ext.grid.RowNumberer(),
                                                              chkRow,
                                                              {
                                                                  header:'Usuario',
                                                                  width:240,
                                                                  sortable:true,
                                                                  dataIndex:'nombre'
                                                              },
                                                             // {
//                                                                  header:'Participaci&oacute;n',
//                                                                  width:100,
//                                                                  sortable:true,
//                                                                  dataIndex:'participacion',
//                                                                  editor:cmbParticipacion,
//                                                                  renderer:formatearParticipacion
//                                                              },
//                                                              {
//                                                                  header:'Permisos',
//                                                                  width:60,
//                                                                  sortable:true,
//                                                                  dataIndex:'acciones'
//                                                              },
                                                              {
                                                                  header:'Sedes',
                                                                  width:100,
                                                                  sortable:true,
                                                                  dataIndex:'sedes',
                                                                  renderer:function(val,meta,registro,pos)
                                                            				  {
                                                                                	return '<a href="javascript:modificarVentanaSedes('+pos+',\''+bE(registro.get('nombre'))+'\')"><img height="13" width="13" src="../images/update_nw.gif" alt="Ver Parametros" title="Ver Parametros" /></a>';
                                                                              }
                                                              }
                                                          ]
                                                      );
            }
       }     
    
    var existeGridR=Ext.getCmp('responsables');
    if(existeGridR==undefined)
    {
    	mE('trResponsables');
    }
    else
    {
    	existeGridR.destroy();
        mE('trResponsables');
    }
    var responsables=	new Ext.grid.EditorGridPanel	(
													{
														id:'responsables',
                                                        store:alUsuariosResp,
                                                        frame:true,
                                                        cm: cmResponsables,
                                                        sm:chkRow,
                                                        height:200,
                                                        width:400,
												        clicksToEdit:1,
														renderTo:'tblUsuariosResp',
														tbar:arregloB
													}
					
    											);
    }
    else
    {
    	oE('trResponsables');
    }                                                  
														
	  //responsables.on();

//    var gridResp=Ext.getCmp('responsables');
//    var pos=obtenerPosFila(gridResp.getStore(),'idUsuario',idUsr);
//    if(pos==-1)
//    {
//        var nRegistro=new registroUsuario	(
//                                                {
//                                                     idUsuario:idUsr,
//                                                     nombre:comboPapa.getRawValue(),
//                                                     participacion:part,
//                                                     codigoUnidad: depto
//                                                }
//                                            )
//        gridResp.getStore().add(nRegistro);
//}                                             
}

function formatearParticipacion(val)
{
	var pos=existeValorMatriz(arrParticipacion,val);
	if(pos!=-1)
    	return arrParticipacion[pos][1];
    else
    	return val;
}


function formatearManejoG(val)
{
	var pos=existeValorMatriz(arrManejoG,val);
	if(pos!=-1)
    	return arrManejoG[pos][1];
    else
    	return val;
}

function mostrarVentanaAgregar(tblPersonas)
{
	var idPrograma=<?php echo $idPrograma?>;
    var muestraC=gE('mCmbA').value;
    var cadena='';
    
    var opcion=gE('global').value;
    var gridS=Ext.getCmp('sedesG');
    var tamano=gridS.getStore().getCount();
    for(x=0;x< tamano;x++)
    {
        var codDepto=gridS.getStore().getAt(x).get('codigoUnidad');
            
            if(cadena!='')
               cadena+=",'"+codDepto+"'";
            else
               cadena="'"+codDepto+"'";
    }

    if(cadena=="")
    {
        Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar las sedes en las que se aplicara este programa');
        return;
    }
           
   
    gE('cBusquedaP').value='1';

	
	
	var parametros2=	{
							funcion:'123',
							criterio:'',
                            cadena:cadena
						};
	
	var comboPapa=inicializarCmbPadre(parametros2);

	

	var arregloE=new Array();
    if(opcion==0)
    {
        arregloE.push(new Ext.form.Label	(
                                                        {
                                                            x:5,
                                                            y:70,
                                                            html:'Permisos: '
                                                        }
                                                    )
                          );                          
                  arregloE.push({
                                    x:20,
                                    y:90,
                                    fieldLabel: '',
                                    boxLabel: 'Consultar',
                                    name: 'publico',
                                    xtype:'checkbox',
                                    id:'chkConsultar',
                                    checked:true,
                                    disabled:true
                                    
                                });				
                 arregloE.push({
                                    x:100,
                                    y:90,
                                    fieldLabel: '',
                                    boxLabel: 'Agregar',
                                    name: 'publico',
                                    xtype:'checkbox',
                                    id:'chkAgregar'
                                });
                 arregloE.push({
                                    x:180,
                                    y:90,
                                    fieldLabel: '',
                                    boxLabel: 'Modificar',
                                    name: 'publico',
                                    xtype:'checkbox',
                                    id:'chkModificar'
                                });
                 arregloE.push({
                                    x:260,
                                    y:90,
                                    labelSeparator: '',
                                    boxLabel: 'Eliminar',
                                    name: 'privado',
                                    xtype:'checkbox',
                                    id:'chkElminar'
                                });
                 arregloE.push({
                                    x:340,
                                    y:90,
                                    labelSeparator: '',
                                    boxLabel: 'Manejo de grupos',
                                    name: 'privado',
                                    xtype:'checkbox',
                                    id:'chkManejoG'
                                });               
    }
    var alto=220;
    if(opcion==1)
    {
    	var gridSedesA=obtenerSedesAgregadas();
        var alto=500;
        arregloE.push(gridSedesA);
    }
   
    var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
														 	new Ext.form.Label	(
																				 	{
																						x:5,
																						y:40,
																						text:'Persona: '
																					}
																				)
															,
															comboPapa,
															

															
															
															new Ext.form.Radio	(
																					{
																						x:5,
																						y:5,
																						id:'rdoPaterno',
																						boxLabel:'Ap. Paterno',
																						checked:true,
																						value:1
																						
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:135,
																						y:5,
																						id:'rdoMaterno',
																						boxLabel:'Ap. Materno',
																						value:2
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:265,
																						y:5,
																						id:'rdoNombre',
																						boxLabel:'Nombre',
																						value:3
																					}
																				),
                                                            arregloE
														]
											}
										);
	
	ventana = new Ext.Window	(
									{
										title: lblAplicacion,
										width: 500,
										height:alto,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										listeners : {
														show : {
																	buffer : 500,
																	fn : function() 
																	{
																		comboPapa.focus();
																	}
																}
													},
										buttons:	[
														{
															text: 'Aceptar',
															handler:function()
																	{
																		var idUsr=gE('idUsuario').value;
                                                                        if(idUsr==-1)
                                                                        {
                                                                        	function respUsr()
                                                                            {
                                                                            	comboPapa.focus();
                                                                            }
                                                                        	msgBox('Debe seleccionar el usuario a agregar',respUsr);
                                                                            return;
                                                                        }
                                                                        var part=-1;
                                                                       
                                                                        
                                                                         
                                                                         var cadPermisos='C';
                                                                         if(opcion==0)
                                                                         {   
                                                                             var agreg=Ext.getCmp ('chkAgregar');
                                                                             var modif=Ext.getCmp ('chkModificar');
                                                                             var elim=Ext.getCmp ('chkElminar');
                                                                             var mGrup=Ext.getCmp ('chkManejoG');
                                                                             
                                                                             if(agreg.getValue()==true)
                                                                             {
                                                                                  cadPermisos=cadPermisos+'A';
                                                                             }
                                                                             if(modif.getValue()==true)
                                                                             {
                                                                                  cadPermisos=cadPermisos+'M';
                                                                             }
                                                                             if(elim.getValue()==true)
                                                                             {
                                                                                  cadPermisos=cadPermisos+'E';
                                                                             }
                                                                             if(mGrup.getValue()==true)
                                                                             {
                                                                                  cadPermisos=cadPermisos+'G';
                                                                             }     
                                                                         }  
                                                                        
                                                                        if(opcion=='0')// una sede
                                                                        {
                                                                            var cmbGrupo='';
                                                                            
                                                                            var combo=Ext.getCmp('cmbNombrePadre');
                                                                            var pos=obtenerPosFila(combo.getStore(),'idUsuario',idUsr);
                                                                            var registro=combo.getStore().getAt(pos);
                                                                            var depto=registro.get('codigoUnidad');
                                                                            var sdes='';
                                                                            var gridResp=Ext.getCmp('responsables');
                                                                            var pos=obtenerPosFila(gridResp.getStore(),'idUsuario',idUsr);
                                                                            if(pos==-1)
                                                                            {
                                                                                var nRegistro=new registroUsuario	(
                                                                                                                        {
                                                                                                                             idUsuario:idUsr,
                                                                                                                             nombre:comboPapa.getRawValue(),
                                                                                                                             participacion:part,
                                                                                                                             codigoUnidad: depto,
                                                                                                                             acciones: cadPermisos,
                                                                                                                             sedes:sdes,
                                                                                                                             manejoG:cmbGrupo
                                                                                                                        }
                                                                                                                    )
                                                                                gridResp.getStore().add(nRegistro);
                                                                            }
                                                                        }    
                                                                        else
                                                                        {
                                                                        	if(opcion=='1')
                                                                        	{
                                                                            	var cmbGrupo='';
                                                                                var combo=Ext.getCmp('cmbNombrePadre');
                                                                                var pos=obtenerPosFila(combo.getStore(),'idUsuario',idUsr);
                                                                                var registro=combo.getStore().getAt(pos);
                                                                                var depto=registro.get('codigoUnidad');
                                                                                
                                                                                var almacen=Ext.getCmp('sedesUsuarioP').getStore();
                                                                                var tamano=almacen.getCount();
                                                                                var x;
                                                                                var cadenaSedesPer='';
                                                                                for(x=0;x< tamano;x++)
                                                                                {
                                                                                	var registro=almacen.getAt(x);
                                                                                    if(cadenaSedesPer=='')
                                                                                    	cadenaSedesPer=registro.get('codigoUnidad')+'!'+registro.get('unidad')+'!'+registro.get('permisos');
                                                                                    else
                                                                                    	cadenaSedesPer+='|'+registro.get('codigoUnidad')+'!'+registro.get('unidad')+'!'+registro.get('permisos');   
                                                                                }
                                                                                
                                                                                var gridResp=Ext.getCmp('responsables');
                                                                                var pos=obtenerPosFila(gridResp.getStore(),'idUsuario',idUsr);
                                                                                if(pos==-1)
                                                                                {
                                                                                    var nRegistro=new registroUsuario	(
                                                                                                                            {
                                                                                                                                 idUsuario:idUsr,
                                                                                                                                 nombre:comboPapa.getRawValue(),
                                                                                                                                 participacion:part,
                                                                                                                                 codigoUnidad: depto,
                                                                                                                                 acciones: cadPermisos,
                                                                                                                                 sedes:cadenaSedesPer,
                                                                                                                                 manejoG:cmbGrupo
                                                                                                                            }
                                                                                                                        )
                                                                                    gridResp.getStore().add(nRegistro);
                                                                                } 
                                                                            }
                                                                        }
                                                                        ventana.close();
                                                                    }
                                                                    
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);

	var rdoPaterno=Ext.getCmp('rdoPaterno');
	var rdoMaterno=Ext.getCmp('rdoMaterno');
	var rdoNombre=Ext.getCmp('rdoNombre');
	rdoPaterno.on('check',cambiarRadioSel);									
	rdoMaterno.on('check',cambiarRadioSel);									
	rdoNombre.on('check',cambiarRadioSel);							
    ventana.show();
}

function cambiarRadioSel(chk, valor)
{
	if(valor==true)
	{
		var rdoPaterno=Ext.getCmp('rdoPaterno');
		var rdoMaterno=Ext.getCmp('rdoMaterno');
		var rdoNom=Ext.getCmp('rdoNombre');
		if(rdoPaterno.id!=chk.id)
			rdoPaterno.setValue(false);
		if(rdoMaterno.id!=chk.id)
			rdoMaterno.setValue(false);
		if(rdoNom.id!=chk.id)
			rdoNom.setValue(false);
		gE('cBusquedaP').value=chk.value;
	}
}

function inicializarCmbPadre(parametros2)
{

	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesProgAcademico.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'idUsuario',mapping:'idUsuario'},
                                                {name:'codigoUnidad',mapping:'codigoUnidad'}
											]
										);

	var ds=new Ext.data.Store	(
								 	{
										proxy:pPagina,
										reader:lector,
										baseParams:parametros2
									}
								 );
	
	function cargarDatos(dSet)
	{
		gE('idUsuario').value='-1';
		var aNombre=Ext.getCmp('cmbNombrePadre').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=gE('cBusquedaP').value;
       
	}
	
	ds.on('beforeload',cargarDatos);

	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br>',
										'</div></tpl>'
									 );
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:90,
														y:35,
														id:'cmbNombrePadre',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:320,
                                                        listWidth :320,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item'
														
													}
												 );
	
    function funcElemSeleccionado(combo,registro)
	{	
		var idUsuario=registro.get('idUsuario');
		gE('idUsuario').value=idUsuario;
    }
	comboNombre.on('select',funcElemSeleccionado);	
	return comboNombre;
}

function programaPrecedeChange(combo)
{
	var idPrograma=combo.selectedIndex;
    if(idPrograma==0)
    {
    	oE('lblIncluido');
        oE('_programaAntContenidoint');
        gE('_programaAntContenidoint').selectedIndex=0;
    }
    else
    {
       	mE('lblIncluido');
        mE('_programaAntContenidoint');
    }
}

function crearGridSedes()
{
	var arregloB2=new Array();
    var mostrarB2=1;
    if(mostrarB2==1)
    {
    	arregloB2.push(
        				{
                          text:'Agregar Sede',
                          icon:'../images/add.png',
                          cls:'x-btn-text-icon',
                          handler:function()
                                  {
                                      mostrarVentanaAgregarSede();
                                  }
                        }
                     );
                      
        arregloB2.push(
        			    {
                          text:'Remover Sede',
                          icon:'../images/cancel_round.png',
                          cls:'x-btn-text-icon',
                          handler:function()
                                  {
                                      var sedes=Ext.getCmp('sedesG');
                                      var filas=sedes.getSelectionModel().getSelections();
                                      var tamanoS=filas.length;
                                      
                                      var usuarios=Ext.getCmp('responsables');
                                      var tamano=usuarios.getStore().getCount();
                                      
                                      if(filas.length>0)
                                      {
                                          function funcConfirmDel(btn)
                                          {
                                              if(btn=="yes")
                                              {
                                                  var x;
                                                  var y;
                                                  for(y=0;y< tamanoS;y++)
                                                  {
                                                      var deptoS=filas[y].get('codigoUnidad');
                                                      
                                                      for(x=0;x <usuarios.getStore().getCount();x++)
                                                      {
                                                        var registro=usuarios.getStore().getAt(x);
                                                        var cadenaSedes=usuarios.getStore().getAt(x).get('sedes');
                                                        if(cadenaSedes==='')
                                                        {
                                                        	
                                                        }
                                                        else
                                                        {
                                                        	var nuevaCadenaSedesUsuario='';
                                                            var arregloSedes=cadenaSedes.split('|');
                                                            var tamanoSedes=arregloSedes.length;
                                                            if(tamanoSedes>0)
                                                            {
                                                            	var w;
                                                                for(w=0;w<tamanoSedes;w++)
                                                                {
                                                                      var arreglo2=arregloSedes[w].split('!');
                                                                      var unidadArreglo=arreglo2[0];
                                                                      var nombreUnidad=arreglo2[1];
                                                                      var permisosUnidad=arreglo2[2];
                                                                      if(deptoS!=unidadArreglo)
                                                                      {
                                                                          if(nuevaCadenaSedesUsuario=='')
                                                                          	nuevaCadenaSedesUsuario=unidadArreglo+'!'+nombreUnidad+'!'+permisosUnidad;
                                                                          else
                                                                          	nuevaCadenaSedesUsuario+='|'+unidadArreglo+'!'+nombreUnidad+'!'+permisosUnidad;    
                                                                      }			
                                                                }
                                                                
                                                                registro.set('sedes',nuevaCadenaSedesUsuario);
                                                            }    
                                                        }
                                                      }
                                                  }
                                                  sedes.getStore().remove(filas);
                                              }
                                          }
                                          Ext.Msg.confirm('<?php echo $etj["lblAplicacion"] ?>','Est&aacute; seguro de querer remover las sedes seleccionadas?',funcConfirmDel);
                                          
                                      }
                                      else
                                      {
                                          msgBox('Primero debe seleccionar las sedes a remover');
                                      }
                                  }
                      }
                     );      
    }
    
    var dsSedes=<?php echo $arrSedes ?>;
    
	var uSedes=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'unidad'}
                                                                ]
                                                    }
                                                );
	uSedes.loadData(dsSedes);
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var cmSedes= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Sede',
															width:320,
															sortable:true,
															dataIndex:'unidad'
														}
												
													]
												);
														
	sedes=	new Ext.grid.EditorGridPanel	(
													{
														id:'sedesG',
                                                        store:uSedes,
                                                        frame:true,
                                                        cm: cmSedes,
                                                        sm:chkRow,
                                                        height:200,
                                                        width:430,
												        clicksToEdit:1,
														renderTo:'tblCedes',
														tbar:arregloB2
													}
					
    											);
	
}


var nuevoRegistroGridSede=Ext.data.Record.create(
													[
                                                        {name:'codigoUnidad'},
                                                        {name:'unidad'}
													]
                                                );

function mostrarVentanaAgregarSede()
{
	var arregloUnidades=<?php echo $arrUnidades?>;
    var dsUnidades= new Ext.data.SimpleStore	(
													{
														fields:	[
																	{name:'codigoUnidad'},
																	{name:'unidad'}
																]
													}
												);

	dsUnidades.loadData(arregloUnidades);
	
	var columnaCheck=new Ext.grid.CheckboxSelectionModel();	
	var cmUnidades= new Ext.grid.ColumnModel   	(
                                                        [
                                                            //new  Ext.grid.RowNumberer(),
                                                            columnaCheck,
                                                            {
                                                                header:'Sede',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'unidad'
                                                            }
                                                        ]
                                                    );
											
												
	tblUnidades=new Ext.grid.GridPanel	(
                                                    {
                                                    	x:10,
                                                        y:10,
														id:'tblUnidades',
                                                        store:dsUnidades,
                                                        frame:true,
                                                        cm: cmUnidades,
                                                        sm:columnaCheck,
                                                        height:350,
                                                        width:550
															
													}
					
    											);
    
    	
	ventana = new Ext.Window	(
									{
										title: lblAplicacion,
										width: 450,
										height:450,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: tblUnidades,
										modal:true,
										listeners : {
														show : {
																	buffer : 500,
																	fn : function() 
																	{
																	}
																}
													},
										buttons:	[
														{
															text: 'Aceptar',
															handler:function()
																	{
																		
                                                                        var gridU=Ext.getCmp('tblUnidades');
                                                                        var modeloSel=gridU.getSelectionModel();
                                                                        var fila=modeloSel.getSelections();
                                                                        var tamano=fila.length;
                                                                        
                                                                        if(tamano==0)
                                                                        {
                                                                            Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar una sede');
                                                                            return;
                                                                        }
                                                                        
                                                                        var cadena='';
                                                                        var gridS=Ext.getCmp('sedesG');
                                                                        for(x=0;x< tamano;x++)
                                                                        {
                                                                            var codigoU=fila[x].get('codigoUnidad');
                                                                            var nombre=fila[x].get('unidad');
                                                                            var nRegistro=new nuevoRegistroGridSede	(
                                                                                                                    {
                                                                                                                          codigoUnidad:codigoU,
                                                                                                                          unidad:nombre
                                                                                                                    }
                                                                                                                );
                                                                       		
                                                                            var existe=obtenerPosFila(gridS.getStore(),'codigoUnidad',codigoU);
                                                                            if(existe==-1)
                                                                        	{
                                                                            	gridS.getStore().add(nRegistro);
                                                                              	
                                                                                if(cadena!='')
                                                                                   cadena+=','+codigoU;
                                                                                else
                                                                                   cadena=codigoU;
                                                                            }
                                                                        }
                                                                       
                                                                       if(cadena!='')
                                                                       {
                                                                       	var cadUnidades=gE('cadUnidades');
                                                                       	cadUnidades.value=cadena;
                                                                       }
                                                                       
                                                                       ventana.close();
                                                                    }
                                                                    
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);

    ventana.show();
}

function existeSede(codigoU)
{
	var gridS=Ext.getCmp('sedesG');
    
}

function mostrarGrid()
{
	var cmb=gE('global');
    var opcion=cmb.options[cmb.selectedIndex].value;
    if(opcion=='1')
    {
        mE('trGrid');
    }
    else
    {
    	oE('trGrid');
        gE('cadUnidades').value='';
        gE('listadoUsuarios').value='';
        var existeGridS=Ext.getCmp('sedesG');
        if(existeGridS!=undefined)
        {
        	Ext.getCmp('sedesG').getStore().removeAll();
        }
        
        var existeGridR=Ext.getCmp('responsables');
        if(existeGridR!=undefined)
        {
        	Ext.getCmp('responsables').getStore().removeAll();
        }    
    }
    
    if(opcion!='-1')
    {
    	crearGridUsuarios();
    }    
}


function prepararSedes()
{
    var opcion=gE('global').value;
    if(opcion==1)
    {
        var x;
        var sedes=Ext.getCmp('sedesG');
        var listado='';
        for(x=0;x<sedes.getStore().getCount();x++)
        {
            var fila=sedes.getStore().getAt(x);
            var sede=fila.get('codigoUnidad');
            if(listado=='')
                listado=sede;
            else
                listado+=','+sede;
            
        }
        gE('cadUnidades').value=listado;
        var listaFinal=gE('cadUnidades').value;
        if(listaFinal=='')
        {
            
            return 2;
        }
    }  
}


function ventanaPermisos(aG,mD,eL,pos,mG)
{
    var panelModPer=new Ext.form.FieldSet (
                                              {
                                                  x:10,
                                                  y:70,
                                                  id:'permisosP',
                                                  width:340,
                                                  title:'Permisos asignados al rol:',
                                                  layout:'absolute',
                                                  height:80,
                                                  items: [		
                                                                  {
                                                                      x:10,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Consultar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      id:'chkConsultar',
                                                                      checked:true,
                                                                      disabled:true
                                                                      
                                                                  },				
                                                                  {
                                                                      x:90,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Agregar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:aG,
                                                                      id:'chkAgregarM'
                                                                  },
                                                                  {
                                                                      x:170,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Modificar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:mD,
                                                                      id:'chkModificarM'
                                                                  },
                                                                  {
                                                                      x:250,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Eliminar',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:eL,
                                                                      id:'chkElminarM'
                                                                  },
                                                                  {
                                                                      x:330,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Manejo de grupos',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:mG,
                                                                      id:'chkManejoGrupoM'
                                                                  }
                                                          ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Modificar Permisos',
										width:500,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[panelModPer],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var cadPermisos='C';
                                                                    var agreg=Ext.getCmp ('chkAgregarM');
                                                                    var modif=Ext.getCmp ('chkModificarM');
                                                                    var elim=Ext.getCmp ('chkElminarM');
                                                                    var manejoG=Ext.getCmp ('chkManejoGrupoM');
                                                                    if(agreg.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'A';
                                                                    }
                                                                    if(modif.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'M';
                                                                    }
                                                                    if(elim.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'E';
                                                                    }
                                                                    if(manejoG.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'G';
                                                                    }        
                                                                   
                                                                    var almacen=Ext.getCmp('responsables').getStore();
                                                                    var registro=almacen.getAt(pos);
                                                                    registro.set('acciones',cadPermisos);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function obtenerSedesAgregadas()
{
	var almacen=Ext.getCmp('sedesG').getStore();
    var arreglo=new Array();
    var x;
    
    var tamano=almacen.getCount();
    for(x=0;x< tamano;x++)
    {
    	var obj=[almacen.getAt(x).get('codigoUnidad'),almacen.getAt(x).get('unidad'),'C'];
        arreglo.push(obj);
    }
    
   return generarGridPermisos(arreglo);
}


function generarGridPermisos(arreglo)
{
	var uSedesPer=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'unidad'},
                                                                    {name: 'permisos'}
                                                                ]
                                                    }
                                                );
	uSedesPer.loadData(arreglo);
    
	//var comboGrupos2=crearComboExt('comboGrupos2',arrManejoG);
    var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var cmSedesP= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Sede',
															width:250,
															sortable:true,
															dataIndex:'unidad'
														},
                                                        {
															header:'Permisos',
															width:110,
															sortable:true,
															dataIndex:'permisos',
                                                            //,
                                                            //editor:comboGrupos2,
                                                            //renderer:formatearManejoG
                                                            renderer:function(val,meta,registro,pos)
                                                            				  {
                                                                                	return val+'<a href="javascript:modificarPermisosPorSedeAgregar('+registro.get('codigoUnidad')+','+pos+',\''+registro.get('unidad')+'\')"><img height="13" width="13" src="../images/key_go.png" alt="Ver Parametros" title="Ver Parametros" /></a>';
                                                                              }
                                                            
														}
												
													]
												);
														
	var sedesP=	new Ext.grid.EditorGridPanel	(
													{
														id:'sedesUsuarioP',
                                                        x:0,
                                                        y:120,
                                                        store:uSedesPer,
                                                        frame:true,
                                                        cm: cmSedesP,
                                                        sm:chkRow,
                                                        height:250,
                                                        width:425,
												        clicksToEdit:1,
                                                        tbar:
                                                        [
                                                            {
                                                                text:'Agregar sede',
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                        {
                                                                            agregarSedeAgregarU();
                                                                        }
                                                            },
                                                            {
                                                                text:'Remover sede',
                                                                icon:'../images/cancel_round.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                        {
                                                                            var sPer=Ext.getCmp('sedesUsuarioP');
                                                                            var filas=sPer.getSelectionModel().getSelections();
                                                                            if(filas.length>0)
                                                                            {
                                                                                function funcConfirmDelSede(btn)
                                                                                {
                                                                                    if(btn=="yes")
                                                                                    {
                                                                                        sPer.getStore().remove(filas);
                                                                                    }
                                                                                }
                                                                                Ext.Msg.confirm('<?php echo $etj["lblAplicacion"] ?>','Est&aacute; seguro de querer remover a las sedes seleccionadas?',funcConfirmDelSede);
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('Primero debe seleccionar la sede a remover');
                                                                            }
                                                                        }
                                                            }
                                                        ]
													}
					
    											);
   return  sedesP  ;                                          
}

function modificarPermisosPorSede(codigoUnidad,pos,nombreU)
{
    var almc=Ext.getCmp('sedesUsuarioMod').getStore();
    var reg=almc.getAt(pos);
    var cad=reg.get('permisos');
    var cad=cad+'';
    var aG=cad.indexOf('A');
    if(aG==-1)
        aG=false;
    else
        aG=true;    
    
    var mD=cad.indexOf('M');
    if(mD==-1)
        mD=false;
    else
        mD=true; 
        
    var eL=cad.indexOf('E');
    if(eL==-1)
        eL=false;
    else
        eL=true;
    
    var mG=cad.indexOf('G');
    if(mG==-1)
        mG=false;
    else
        mG=true;                    
    var panelModPerSede=new Ext.form.FieldSet (
                                              {
                                                  x:10,
                                                  y:70,
                                                  id:'permisosPSede',
                                                  width:490,
                                                  title:'Permisos por sede:',
                                                  layout:'absolute',
                                                  height:80,
                                                  items: [		
                                                                  {
                                                                      x:10,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Consultar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      id:'chkConsultarSede',
                                                                      checked:true,
                                                                      disabled:true
                                                                      
                                                                  },				
                                                                  {
                                                                      x:90,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Agregar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:aG,
                                                                      id:'chkAgregarSede'
                                                                  },
                                                                  {
                                                                      x:170,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Modificar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:mD,
                                                                      id:'chkModificarSede'
                                                                  },
                                                                  {
                                                                      x:250,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Eliminar',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:eL,
                                                                      id:'chkElminarSede'
                                                                  },
                                                                  {
                                                                      x:330,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Manejo de grupos',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:mG,
                                                                      id:'chkManejoGrupoSede'
                                                                  }
                                                          ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Modificar Permisos en sede:&nbsp;'+nombreU,
										width:490,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[panelModPerSede],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var cadPermisos='C';
                                                                    var agreg=Ext.getCmp ('chkAgregarSede');
                                                                    var modif=Ext.getCmp ('chkModificarSede');
                                                                    var elim=Ext.getCmp ('chkElminarSede');
                                                                    var manejoG=Ext.getCmp ('chkManejoGrupoSede');
                                                                    if(agreg.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'A';
                                                                    }
                                                                    if(modif.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'M';
                                                                    }
                                                                    if(elim.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'E';
                                                                    }
                                                                    if(manejoG.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'G';
                                                                    }   
                                                                        
                                                                    var almacen=Ext.getCmp('sedesUsuarioMod').getStore();
                                                                    var registro=almacen.getAt(pos);
                                                                    registro.set('permisos',cadPermisos);
                                                                    
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function modificarVentanaSedes(posicion,nombre)
{
   var gridModif=crearGridModificaciones(posicion);
   var ventana=new Ext.Window(
							   		{
										title:'Modificar sedes usuario:&nbsp;'+bD(nombre),
										width:450,
										height:380,
										layout:'absolute',
										buttonAlign:'center',
										items:[gridModif],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var almacenResponsables=Ext.getCmp('responsables').getStore();
                                                                    var almacen=Ext.getCmp('sedesUsuarioMod').getStore();
                                                                    var tamano=almacen.getCount();
                                                                    var x;
                                                                    var cadenaFinal='';
                                                                    if(tamano==0)
                                                                    {
                                                                    	var registro=almacenResponsables.getAt(posicion);
                                                                        registro.set('sedes',cadenaFinal);
                                                                        ventana.close();
                                                                    }
                                                                    else
                                                                    {
                                                                        for(x=0;x< tamano;x++)
                                                                        {
                                                                        	var registroInt=almacen.getAt(x);
                                                                            //alert(registroInt.get('permisos'));
                                                                            if(cadenaFinal=='')
                                                                            	cadenaFinal=registroInt.get('codigoUnidad')+'!'+registroInt.get('unidad')+'!'+registroInt.get('permisos');
                                                                            else
                                                                                cadenaFinal+='|'+registroInt.get('codigoUnidad')+'!'+registroInt.get('unidad')+'!'+registroInt.get('permisos');
                                                                        }
                                                                        
                                                                        var registro=almacenResponsables.getAt(posicion);
                                                                        registro.set('sedes',cadenaFinal);
                                                                        ventana.close();
                                                                    }
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}


function crearGridModificaciones(pos) //modificar permisos en sedes 
{
	var almacenSedes=Ext.getCmp('sedesG').getStore();
    var arregloGrid=new Array();
    var almacen=Ext.getCmp('responsables').getStore();
    var registro=almacen.getAt(pos);
    var cadenaRegistro=registro.get('sedes');
    var arregloIni=cadenaRegistro.split('|');
    var x;
    if(arregloIni.length>0)
    {
    	for(x=0;x< arregloIni.length;x++ )
        {
        	var arregloFin=arregloIni[x];
            var datos=arregloFin.split('!');
            var obj=[datos[0],datos[1],datos[2]];
            var pos=obtenerPosFila(almacenSedes,'codigoUnidad',datos[0]);
            if(pos!=-1)
            {
            	arregloGrid.push(obj);
            }    
        }
    }
    var uSedesPer=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'unidad'},
                                                                    {name: 'permisos'}
                                                                ]
                                                    }
                                                );
	uSedesPer.loadData(arregloGrid);
    
	var comboGrupos3=crearComboExt('comboGrupos3',arrManejoG);
    var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var cmSedesP= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Sede',
															width:260,
															sortable:true,
															dataIndex:'unidad'
														},
                                                        {
															header:'Permisos',
															width:115,
															sortable:true,
															dataIndex:'permisos',
                                                            //editor:comboGrupos3,
                                                            //renderer:formatearManejoG
                                                            renderer:function(val,meta,registro,pos)
                                                            				  {
                                                                                	return val+'<a href="javascript:modificarPermisosPorSede('+registro.get('codigoUnidad')+','+pos+',\''+registro.get('unidad')+'\')"><img height="13" width="13" src="../images/key_go.png" alt="Modificar Permisos" title="Modificar Permisos" /></a>';
                                                                              }
														}
													]
												);
														
	var sedesP=	new Ext.grid.EditorGridPanel	(
													{
														id:'sedesUsuarioMod',
                                                        x:0,
                                                        y:0,
                                                        store:uSedesPer,
                                                        frame:true,
                                                        cm: cmSedesP,
                                                        sm:chkRow,
                                                        height:300,
                                                        width:435,
												        clicksToEdit:1,
                                                        tbar:
                                                        [
                                                            {
                                                                text:'Agregar sede',
                                                                icon:'../images/add.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                        {
                                                                            agregarSedeModificacion();
                                                                        }
                                                            },
                                                            {
                                                                text:'Remover sede',
                                                                icon:'../images/cancel_round.png',
                                                                cls:'x-btn-text-icon',
                                                                handler:function()
                                                                        {
                                                                            var sPer=Ext.getCmp('sedesUsuarioMod');
                                                                            var filas=sPer.getSelectionModel().getSelections();
                                                                            if(filas.length>0)
                                                                            {
                                                                                function funcConfirmDelSede(btn)
                                                                                {
                                                                                    if(btn=="yes")
                                                                                    {
                                                                                        sPer.getStore().remove(filas);
                                                                                    }
                                                                                }
                                                                                Ext.Msg.confirm('<?php echo $etj["lblAplicacion"] ?>','Est&aacute; seguro de querer remover las sedes seleccionadas?',funcConfirmDelSede);
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('Primero debe seleccionar la sede a remover');
                                                                            }
                                                                        }
                                                            }
                                                        ]
													}
					
    											);
   return  sedesP  ;              
}

var regNuevaSede=Ext.data.Record.create	(
                                                [
                                                    {name: 'codigoUnidad'},
                                                    {name: 'unidad'},
                                                    {name: 'permisos'}
                                                ]
                                            );

function agregarSedeModificacion()//cuando entras a un modificar las sedes de un usuario que ya esta como responsable
{
	
    var almacenSedes=Ext.getCmp('sedesG').getStore();
    var tamano=almacenSedes.getCount();
    var almacenExistente=Ext.getCmp('sedesUsuarioMod').getStore();
    var x;
    var arreglo=new Array();
    for(x=0;x< tamano;x++)
    {
    	var registro=almacenSedes.getAt(x);
        var codU=registro.get('codigoUnidad');
        var nomU=registro.get('unidad');
        var per='';
        var pos=obtenerPosFila(almacenExistente,'codigoUnidad',codU);
        if(pos==-1)
        {
        	var obj=[codU,nomU,per];
            arreglo.push(obj);
        }
    }
    
    if(arreglo.length==0)
    {
    	msgBox('No hay sedes disponibles');
        return;
    }
    
    var gridSedesNuevas=crearGridSedesNuevasModif(arreglo);
    var ventana=new Ext.Window(
							   		{
										title:'Agregar sede',
										width:450,
										height:380,
										layout:'absolute',
										buttonAlign:'center',
										items:[gridSedesNuevas],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var almacenDest=Ext.getCmp('sedesUsuarioMod').getStore();
                                                                    var filas=Ext.getCmp('sedModNueva').getSelectionModel().getSelections();
                                                                    var tamano=filas.length;
                                                                    var almacenOrigen=Ext.getCmp('sedModNueva').getStore();
                                                                    var y;
                                                                    if(tamano==0)
                                                                    {
                                                                    	msgBox('Debe seleccionar al menos una sede');
                                                                        return;
                                                                    }
                                                                   
                                                                    for(y=0;y< tamano;y++)
                                                                    {
                                                                        //var registro=filas[y];
                                                                        var codU=filas[y].get('codigoUnidad');
                                                                        var nom=filas[y].get('unidad');
                                                                        var per='C';
                                                                        var pos=obtenerPosFila(almacenDest,'codigoUnidad',codU);
                                                                        if(pos==-1)
                                                                        {
                                                                            var nRegistro=new regNuevaSede(
                                                                                                                {
                                                                                                                      codigoUnidad:codU,
                                                                                                                      unidad:nom,
                                                                                                                      permisos:per
                                                                                                                }
                                                                                                             );
                                                                            almacenDest.add(nRegistro); 
                                                                        }                                
                                                                    }
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}

function crearGridSedesNuevasModif(arreglo)
{
	var uSedesModNueva=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'unidad'},
                                                                    {name: 'permisos'}
                                                                ]
                                                    }
                                                );
	uSedesModNueva.loadData(arreglo);
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var cmSedesP= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Sede',
															width:260,
															sortable:true,
															dataIndex:'unidad'
														}
													]
												);
														
	var sedesModN=	new Ext.grid.EditorGridPanel	(
													{
														id:'sedModNueva',
                                                        x:0,
                                                        y:0,
                                                        store:uSedesModNueva,
                                                        frame:true,
                                                        cm: cmSedesP,
                                                        sm:chkRow,
                                                        height:300,
                                                        width:435,
												        clicksToEdit:1
													}
					
    											);
   return  sedesModN  ;              
}

function agregarSedeAgregarU()
{
	var almacenSedes=Ext.getCmp('sedesG').getStore();
    var tamano=almacenSedes.getCount();
    var almacenExistente=Ext.getCmp('sedesUsuarioP').getStore();
    var x;
    var arreglo=new Array();
    for(x=0;x< tamano;x++)
    {
    	var registro=almacenSedes.getAt(x);
        var codU=registro.get('codigoUnidad');
        var nomU=registro.get('unidad');
        var per='';
        var pos=obtenerPosFila(almacenExistente,'codigoUnidad',codU);
        if(pos==-1)
        {
        	var obj=[codU,nomU,per];
            arreglo.push(obj);
        }
    }
    
    if(arreglo.length==0)
    {
    	msgBox('No hay sedes disponibles');
        return;
    }
    
    var gridSedesNuevasAgregarU=crearGridSedesNuevasAgergarU(arreglo);
    var ventana=new Ext.Window(
							   		{
										title:'Agregar sede',
										width:450,
										height:380,
										layout:'absolute',
										buttonAlign:'center',
										items:[gridSedesNuevasAgregarU],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var almacenDest=Ext.getCmp('sedesUsuarioP').getStore();
                                                                    var filas=Ext.getCmp('sedAgregarNueva').getSelectionModel().getSelections();
                                                                    var tamano=filas.length;
                                                                    var almacenOrigen=Ext.getCmp('sedAgregarNueva').getStore();
                                                                    if(tamano==0)
                                                                    {
                                                                    	msgBox('Debe seleccionar al menos una sede');
                                                                        return;
                                                                    }
                                                                   
                                                                    for(x=0;x< tamano;x++)
                                                                    {
                                                                        var registro=almacenOrigen.getAt(x);
                                                                        var codU=registro.get('codigoUnidad');
                                                                        var nom=registro.get('unidad');
                                                                        var per=registro.get('permisos');
                                                                        var nRegistro=new regNuevaSede(
                                                                                                            {
                                                                                                                  codigoUnidad:codU,
                                                                                                                  unidad:nom,
                                                                                                                  permisos:per
                                                                                                            }
                                                                                                         );
                                                                        almacenDest.add(nRegistro);                                 
                                                                    }
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}


function crearGridSedesNuevasAgergarU(arreglo)
{
	var uSedesAgreNueva=	new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                    {name: 'codigoUnidad'},
                                                                    {name: 'unidad'},
                                                                    {name: 'permisos'}
                                                                ]
                                                    }
                                                );
	uSedesAgreNueva.loadData(arreglo);
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
                                          
	var cmSedesP= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Sede',
															width:260,
															sortable:true,
															dataIndex:'unidad'
														}
													]
												);
														
	var sedesAgN=	new Ext.grid.EditorGridPanel	(
													{
														id:'sedAgregarNueva',
                                                        x:0,
                                                        y:0,
                                                        store:uSedesAgreNueva,
                                                        frame:true,
                                                        cm: cmSedesP,
                                                        sm:chkRow,
                                                        height:300,
                                                        width:435,
												        clicksToEdit:1
													}
					
    											);
   return  sedesAgN  ;              
}

function modificarPermisosPorSedeAgregar(codigoUnidad,pos,nombreU)
{
	var almc=Ext.getCmp('sedesUsuarioP').getStore();
    var reg=almc.getAt(pos);
    var cad=reg.get('permisos');
    var aG=cad.indexOf('A');
    if(aG==-1)
        aG=false;
    else
        aG=true;    
    
    var mD=cad.indexOf('M');
    if(mD==-1)
        mD=false;
    else
        mD=true; 
        
    var eL=cad.indexOf('E');
    if(eL==-1)
        eL=false;
    else
        eL=true;
        
    var mG=cad.indexOf('G');
    if(mG==-1)
        mG=false;
    else
        mG=true;
                          
    var panelModPerSede=new Ext.form.FieldSet (
                                              {
                                                  x:10,
                                                  y:70,
                                                  id:'permisosPSede',
                                                  width:340,
                                                  title:'Permisos por sede:',
                                                  layout:'absolute',
                                                  height:80,
                                                  items: [		
                                                                  {
                                                                      x:10,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Consultar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      id:'chkConsultarSedeNu',
                                                                      checked:true,
                                                                      disabled:true
                                                                      
                                                                  },				
                                                                  {
                                                                      x:90,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Agregar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:aG,
                                                                      id:'chkAgregarSedeNu'
                                                                  },
                                                                  {
                                                                      x:170,
                                                                      y:10,
                                                                      fieldLabel: '',
                                                                      boxLabel: 'Modificar',
                                                                      name: 'publico',
                                                                      xtype:'checkbox',
                                                                      checked:mD,
                                                                      id:'chkModificarSedeNu'
                                                                  },
                                                                  {
                                                                      x:250,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Eliminar',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:eL,
                                                                      id:'chkElminarSedeNu'
                                                                  },
                                                                  {
                                                                      x:330,
                                                                      y:10,
                                                                      labelSeparator: '',
                                                                      boxLabel: 'Manejo de grupos',
                                                                      name: 'privado',
                                                                      xtype:'checkbox',
                                                                      checked:mG,
                                                                      id:'chkManejoGrupoNu'
                                                                  }
                                                          ]
                                              }
                                          )
   var ventana=new Ext.Window(
							   		{
										title:'Modificar Permisos en sede:&nbsp;'+nombreU,
										width:490,
										height:150,
										layout:'fit',
										buttonAlign:'center',
										items:[panelModPerSede],
										modal:true,
										plain:true,
										listeners:
											{
												show:
												{
													buffer:10,fn:function()
															{
															}
												}
											},
										buttons:
												[
												 	{
														text:'Aceptar',
														handler:function ()
															{
                                                                    var cadPermisos='C';
                                                                    var agreg=Ext.getCmp ('chkAgregarSedeNu');
                                                                    var modif=Ext.getCmp ('chkModificarSedeNu');
                                                                    var elim=Ext.getCmp ('chkElminarSedeNu');
                                                                    var manejoG=Ext.getCmp ('chkManejoGrupoNu');
                                                                    
                                                                    if(agreg.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'A';
                                                                    }
                                                                    if(modif.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'M';
                                                                    }
                                                                    if(elim.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'E';
                                                                    }    
                                                                    if(manejoG.getValue()==true)
                                                                    {
                                                                        cadPermisos=cadPermisos+'G';
                                                                    }   
                                                                    
                                                                    var almacen=Ext.getCmp('sedesUsuarioP').getStore();
                                                                    var registro=almacen.getAt(pos);
                                                                    registro.set('permisos',cadPermisos);
                                                                    ventana.close();
                                                            }
													},
													{
														text:'Cancelar',
														handler:function ()
															{
																ventana.close();
															}
													}
												 ]
									}
							   )
	ventana.show();
}