<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$conCalculos="SELECT idConsulta,nombreConsulta FROM 991_consultasSql WHERE idTipoConcepto=5";
	$arregloCalc=$con->obtenerFilasArreglo($conCalculos);
?>
Ext.onReady(Inicializar);
function Inicializar()
{
	prueba();
	var validacion=gE('tipoTemario').value;
	
	if(validacion!='1')
	{
		nuevoDTD();
	}
}

function prueba() 
{
	var tab=gE('panel').value;
	ocultar=true;
    <?php
		$cadTabs="";
		if($considerarActitudes)
		{
			$cadTabs="	{
							contentEl:'tabla1', 
							title:'Actitudes'
						}";
		}
		
		if($considerarCompetencias)
		{
			if($cadTabs=="")
			{
				$cadTabs="{
							  contentEl:'tabla2',
							  title:'Competencias'
						  }";
			}
			else
			{
				$cadTabs.=",{
							  contentEl:'tabla2',
							  title:'Competencias'
						  }";
			}
		}
		
		if($considerarProductos)
		{
			if($cadTabs=="")
			{
				$cadTabs="{
							  contentEl:'tabla3',
							  title:'Productos'
						  }";
			}
			else
			{
				$cadTabs.=",{
							  contentEl:'tabla3',
							  title:'Productos'
						  }";
			}
		}
		
		if($considerarCriteriosE)
		{
			if($cadTabs=="")
			{
				$cadTabs="{
							  contentEl:'tabla4',
							  title:'Habilidades'
						  }";
			}
			else
			{
				$cadTabs.=",{
							  contentEl:'tabla4',
							  title:'Habilidades'
						  }";
			}

		}
		
	?>
	var tabs = new Ext.TabPanel	(
									{
										renderTo: 'my-tabs',
										activeTab: tab,
										width:650,
										height:400,
										items:	[	
												 <?php 
												 	echo $cadTabs;
												 ?>
												]
									
									
										
									}
								);
                               
	tabs.setActiveTab(parseInt(tab));	
}

var panelArbol;
function nuevoDTD()
{
	 
  	 var idMat=gE('idMateria3').value;
	 var nom=gE('nombre').value;
     var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
      
      var cargadorArbol=new Ext.tree.TreeLoader(
												   {
												     baseParams:{
																
																funcion:'28',
																idMateria:idMat
															},
												   
												   
														dataUrl:'../paginasFunciones/funcionesAdministracion.php'
													}	

		                                         )	
                                               
		panelArbol=new Ext.tree.TreePanel	(
                                              {
												  id:'tblArbol',
                                                  useArrows:true,
                                                  autoScroll:true,
                                                  animate:false,
                                                  enableDD:true,
                                                  containerScroll:true,
                                                  root:nom,
                                                  loader:cargadorArbol,
                                                  height:300,
												  width:600,
												  collapsible: true,
                                                  draggable:false,
												  rootVisible:true,
												  el:'divArbol'
												  
											  }
                                                  
                                          );                                                 	  
      
      
	 
      panelArbol.render();
      panelArbol.expandAll();
}

function regresar(form)
{
	var form;
	form=gE(form);
	form.action="../Administracion/tblMateria.php";
	form.submit();
}

function agregarActitud()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
    var idMapaCurricular=gE('idMapaCurricular').value;
	var arrParam=[['panel',0],['idMateria',idMateria],['idCiclo',idCiclo],['idMapaCurricular',idMapaCurricular]];
	enviarFormularioDatos('../Administracion/elegirActitud.php',arrParam);
}

function agregarCompetencia()
{
	var idMateria=gE('idMateria2').value;
    var idMapaCurricular=gE('idMapaCurricular').value;
	var idCiclo=gE('idCiclo').value;
	var arrParam=[['panel',1],['idMateria',idMateria],['idCiclo',idCiclo],['idMapaCurricular',idMapaCurricular]];
	enviarFormularioDatos('../Administracion/elegirCompetencia.php',arrParam);

}

function agregarProducto()
{
	
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
     var idMapaCurricular=gE('idMapaCurricular').value;
	var arrParam=[['panel',2],['idMateria',idMateria],['idCiclo',idCiclo],['idMapaCurricular',idMapaCurricular]];
	enviarFormularioDatos('../Administracion/elegirProducto.php',arrParam);
}

function agregarHabilidad()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
    var idMapaCurricular=gE('idMapaCurricular').value;
	var arrParam=[['panel',3],['idMateria',idMateria],['idCiclo',idCiclo],['idMapaCurricular',idMapaCurricular]];
	enviarFormularioDatos('../Administracion/elegirHabilidad.php',arrParam);
}

function agregarRecurso()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
	var arrParam=[['panel',4],['idMateria',idMateria],['idCiclo',idCiclo]];
	enviarFormularioDatos('../Administracion/elegirRecurso.php',arrParam);
}

function agregarTecnica()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
	var arrParam=[['panel',5],['idMateria',idMateria],['idCiclo',idCiclo]];
	enviarFormularioDatos('../Administracion/elegirTecnica.php',arrParam);
	
}

function modificarMateria()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
	var idSeccion=gE('idSeccion').value;
	var idMapaCurricular=gE('idMapaCurricular').value;
	var arrParam=[['idSeccion',idSeccion],['idMateria',idMateria],['idCiclo',idCiclo],['idMapaCurricular',idMapaCurricular]];
	enviarFormularioDatos('../Administracion/materia.php',arrParam);
}

function modificarEstructuraTematica()
{
	var idMateria=gE('idMateria2').value;
	var idCiclo=gE('idCiclo').value;
	var idSeccion=gE('idSeccion').value;
	var nombre=gE('nombre').value;
	var validacion=gE('validacion').value;
	var arrParam=[['idMateria',idMateria],['idCiclo',idCiclo],['idSeccion',idSeccion],['nombre',nombre],['validacion',validacion]];
	enviarFormularioDatos('../Administracion/estructuraTematica.php',arrParam);       
}

function criteriosEval()
{
	var idMapaCurricular=gE('idMapaCurricular').value;
	var idMateria=gE('idMateria2').value;
	var arrParam=[['idMateria',idMateria],['ig','<?php echo base64_encode("-1")?>'],['iP','-1'],['confFicha','1'],['idMapaCurricular',idMapaCurricular]];
    enviarFormularioDatos('../Profesores/criteriosDeEvaluacion.php',arrParam);
}

function reportePlaneacion()
{
	var idGrupo=0;
	var idMateria=gE('idMateria2').value;
	var arrParam=[['idMateria',idMateria],['idGrupo',idGrupo]];
    enviarFormularioDatos('../Profesores/planeacion.php',arrParam);
}

var arreglotipoH=[['1','Profesor se apega a materia'],['2','Materia se apega a profesor']];

function perfilMateria()
{
    //var comboAplHorario=crearComboExt('comboAplHorario',arreglotipoH,130,25,200);
    //comboAplHorario.on('select',guardaHorario);
    var gC=crearGridCriterios();
    var form = new Ext.form.FormPanel(	
										{
                                        	id:'formulario1',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 
                                                        {
                                                            xtype:'label',
                                                            x:25,
                                                            y:5,
                                                            html:'<span><b>Agregue los criterios que desea vincular a esta materia</b></span>'
                                                        },
                                                        //{
//                                                            xtype:'label',
//                                                            x:10,
//                                                            y:30,
//                                                            html:'<span>Aplicaci&oacute;n de horario:</span>'
//                                                        },
//                                                        comboAplHorario,
                                                        {
                                                        	xtype:'panel',
                                                            x:0,
                                                            y:60,
                                                            items:[
                                                                        gC
                                                                   ]
                                                         }
													]
										}
									);

    var ventana = new Ext.Window(
									{
										title: '',
										width: 500,
										height:400,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
                                        id:'ventanaD',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												}
									}
								);
        
        ventana.show();       
}

function crearGridCriterios()
{
    var idMateria=gE('idMateria3').value;
    var dsRegistrosC=new Ext.data.JsonStore({
                                                        root: 'registros',
                                                        totalProperty: 'numReg',
                                                        fields: [
                                                                  {name: 'idMateriaVSConf'},
                                                                  {name: 'idCriterio'},
                                                                  {name: 'nombreConsulta'},
                                                                  {name: 'tieneParam'},
                                                                  {name: 'arregloParam'},
                                                                  {name: 'valor'}
                                                              ],         
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesAdministracion.php'
                                                                                          }
                                                                                      )                             
                                                    })
	dsRegistrosC.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=37;
                                        proxy.baseParams.idMateria=idMateria;
                                    }
                        );
    var columnaCheck=new Ext.grid.CheckboxSelectionModel();
    var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        columnaCheck,
														{
															header:'Criterio',
															width:230,
															sortable:true,
															dataIndex:'nombreConsulta',
                                                            align:'left'
														},
                                                        {
															header:'Puntos',
															width:100,
															sortable:true,
															dataIndex:'valor',
                                                            align:'right',
                                                            renderer:function(val,meta,registro){
                                                            						return Ext.util.Format.number(val,'0,0.00')+'&nbsp;<a href="javascript:modificarPuntos('+registro.get('idPerfilVSPuesto')+','+val+')"><img height="13" width="13" src="../images/pencil.png" alt="Modificar Puntos" title="Modificar Puntos" /></a>';
                                                            				   }
														},
                                                        {
															header:'Parametros',
															width:100,
                                                            align:'center',
															sortable:true,
															dataIndex:'tieneParam',
                                                            renderer:function(val,meta,registro)
                                                            				  {
                                                                                if(val==1)
                                                                                	return '<a href="javascript:verParametros('+registro.get('idMateriaVSConf')+','+registro.get('idCriterio')+')"><img height="13" width="13" src="../images/magnifier.png" alt="Ver Parametros" title="Ver Parametros" /></a>';
                                                                                else
                                                                                	return '';
                                                                              }
														}
													]
												);
	var tblGridC=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridPerfil',
                                                            title:'Pefil de materia',
                                                            store:dsRegistrosC,
                                                            frame:true,
                                                            cm: cModelo,
                                                            sm:columnaCheck,
                                                            height:295,
                                                            width:475,
                                                            tbar:[
                                                            		{
                                                                    	text:'Agregar Criterio',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                        		{
                                                                                	agregarCalculo(idMateria);
                                                                                }
                                                                    },
                                                                    {
                                                                    	text:'Eliminar Criterio',
                                                                        icon:'../images/cancel_round.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                        		{
                                                                                	eliminarCriterio();
                                                                                }
                                                                    }
                                                            	 ]
                                                            
                                                        }
                                                    );
		
    dsRegistrosC.load()  ;
    return tblGridC;     
}

function agregarCalculo(idMateria)
{
    var arregloC=<?php echo $arregloCalc ?>;
    var comboCalculos=crearComboExt('comboCalculos',arregloC,70,10);
    var form2 = new Ext.form.FormPanel(	
										{
                                        	id:'formulario2',
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'textfield',
											items: 	[ 

                                                        {
                                                            xtype:'label',
                                                            x:15,
                                                            y:15,
                                                            html:'<span><b>Criterio:</b></span>'
                                                        },
                                                        comboCalculos
													]
										}
									);

    
    var ventana = new Ext.Window(
									{
										title: 'Agregar Criterio',
										width: 300,
										height:150,
										minWidth: 300,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form2,
                                        id:'ventana2',
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																	
																}
															}
												},
                                        buttons:	[
														{
															text: 'Aceptar',
															listeners:	{
																			click:function()
																				{
																					var idCalculo=Ext.getCmp('comboCalculos').getValue();
                                                                                    if(idCalculo=='')
                                                                                    {
                                                                                     	msgBox('Debe indicar el criterio ');
                                                                                        return;
                                                                                    }
																					
                                                                                    var almacenP=Ext.getCmp('gridPerfil').getStore();
                                                                                    var pos=obtenerPosFila(almacenP,'idCriterio',idCalculo);
                                                                                    if(pos==-1)
                                                                                    {
                                                                                          function funcAjax()
                                                                                          {
                                                                                              var resp=peticion_http.responseText;
                                                                                              arrResp=resp.split('|');
                                                                                              if(arrResp[0]=='1')
                                                                                              {
                                                                                                  var arrDatos=eval(arrResp[1]);
                                                                                                  if(arrDatos.length>0)
                                                                                                  {
                                                                                                  
                                                                                                      ventana.close();
                                                                                                      gEx('ventanaD').hide();
                                                                                                      tb_show(lblAplicacion,'../Administracion/parametrosCalculo.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idCalculo='+idCalculo+'&TB_iframe=true&height=420&width=650',"","scrolling=yes");
                                                                                                  }
                                                                                                  else
                                                                                                  {
                                                                                                      var cadena='';
                                                                                                      var puntos=0;
                                                                                                      function funcAjax2()
                                                                                                      {
                                                                                                          var resp=peticion_http.responseText.split('|');
                                                                                                          if(resp[0]==1)
                                                                                                          {
                                                                                                              
                                                                                                              //entraPagina(idPuesto);
                                                                                                              //recargarPagina();
                                                                                                              Ext.getCmp('gridPerfil').getStore().reload();
                                                                                                              ventana.close();
                                                                                                          }
                                                                                                          else
                                                                                                          {
                                                                                                                Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                                                                                                          }
                                                                                                      }
                                                                                                      obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax2, 'POST','funcion=38&idCalculo='+bE(idCalculo)+'&idMateria='+bE(idMateria)+'&valor='+puntos+'&cadena='+cadena,true)
                                                                                                  }
                                                                                              }
                                                                                              else
                                                                                              {
                                                                                                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                              }
                                                                                          }
                                                                                          obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax, 'POST','funcion=135&idCalculo='+idCalculo,true);
                                                                                    }     
                                                                                }	
																		}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
                                                                        ventana.close();
																	}
														}
													]        
									}
								);
        ventana.show();
}

var regCriterio=crearRegistro([
                                                                    
                                  {name:'idCalculo'},
                                  {name:'nombreCalculo'},
                                  {name:'variables'}
                              ]);

function guardarCalculo(cadena,idCalculo)
{
    var idMateria=gE('idMateria3').value;
    var puntos=0;
    function funcAjax2()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
            Ext.getCmp('gridPerfil').getStore().reload();
            gEx('ventanaD').show();
            
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax2, 'POST','funcion=38&idCalculo='+bE(idCalculo)+'&idMateria='+bE(idMateria)+'&valor='+puntos+'&cadena='+cadena,true)
}

function verParametros(idTabla)
{
	Ext.getCmp('ventanaD').hide();
    tb_show(lblAplicacion,'../Administracion/modificarParametros.php?cPagina='+cv('mPie=false|mI=false|b=false|mR1=false')+'&idTabla='+idTabla+'&TB_iframe=true&height=420&width=650',"","scrolling=yes");
}

function recargarAlmacen()
{
    Ext.getCmp('ventanaD').show();
    //var almacen=Ext.getCmp('gridPerfil');
    
    Ext.getCmp('gridPerfil').getStore().reload();
    
}

function guardaHorario(combo,fila,pos)
{
	var valor=combo.value;
    var idMateria=gE('idMateria3').value;
    var idCalculo=-100;
    var cadena='';
    function funcAjax2()
    {
        var resp=peticion_http.responseText.split('|');
        if(resp[0]==1)
        {
            
        }
        else
        {
              Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax2, 'POST','funcion=38&idCalculo='+bE(idCalculo)+'&idMateria='+bE(idMateria)+'&valor='+valor+'&cadena='+cadena,true)
}

function eliminarCriterio()
{
	var filas=Ext.getCmp('gridPerfil').getSelectionModel().getSelections();
    if(filas=='')
    {
    	msgBox('Debe seleccionar al menos un elemento');
    	return;
    }
    var tamano=filas.length;
    var cadena="";
    var x;
    
    for(x=0;x< tamano;x++)
    {
    	var id=filas[x].get('idMateriaVSConf');
        if(cadena=='')
        	cadena=id;
        else
        	cadena+=','+id;    
    }
    
    function resElim(btnE)
    {
        if(btnE=='yes')
        {
            function funcAjax2()
            {
                var resp=peticion_http.responseText.split('|');
                if(resp[0]==1)
                {
                    var almacen=Ext.getCmp('gridPerfil').getStore();
                    almacen.remove(filas);
                }
                else
                {
                      Ext.MessageBox.alert(lblAplicacion,'No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: <br>'+resp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax2, 'POST','funcion=40&cadena='+cadena,true);
        }
    } 
    msgConfirm('Est&aacute; seguro de eliminar los registros seleccionados?',resElim);        
}