Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_gradoint').focus();
    porcentajeL();
    porcentajeOc();
    mostrarPonderacionGrado();
}

function validarFrm(formulario)
{
	var comboControl=gE('_esquemaEvaluacionint');
    var opcion=comboControl.options[comboControl.selectedIndex].value;
    if(opcion=='0')
    {
        var comboLibre=gE('_materiasLibresint');
        var opcionLibre=comboLibre.options[comboLibre.selectedIndex].value;
        if(opcionLibre==1)
        {
            var porcentajeL=parseInt(gE('_porcentajeLibresflo').value);
            var bandera1=true;
        }
        else
        {
            var porcentajeL=0;
            var bandera1=false;
        }
        var comboOpC=gE('_materiasOptativasCint');
        var opcionOpC=comboOpC.options[comboOpC.selectedIndex].value;
        if(opcionOpC==1)
        {
            var porcentajeOc=parseInt(gE('_porcentajeOptativaCflo').value);
            var bandera2=true;
        }
        else
        {
            var porcentajeOc=0;
            var bandera2=false;
        }
        
        var porcentajeFinal=porcentajeL+porcentajeOc;
        if(bandera1==true || bandera2==true )
        {
            if(porcentajeFinal==100)
            {
                if(validarFormularios(formulario))
                {
                    gE(formulario).submit();
                }
             }
             else
             {
                if(porcentajeFinal> 100)
                {
                    Ext.MessageBox.alert(lblAplicacion,'El porcentaje rebasa el 100%');
                    return;
                }
                else
                {
                    Ext.MessageBox.alert(lblAplicacion,'El porcentaje no alcanza el 100%');
                    return;
                }
             }   
        }
        else
        {
            if(validarFormularios(formulario))
                {
                    gE(formulario).submit();
                }
        }
    }
    else
    {
    	if(validarFormularios(formulario))
        {
            gE(formulario).submit();
        }
    }     
}

function regresar()
{
	var ciclo=gE('_cicloint').value;
    var programa=gE('_idProgramaint').value;
	var arrParam=[['idCiclo',ciclo],['idPrograma',programa]];
    enviarFormularioDatos('tblGrados.php',arrParam);
}

function establecerGradoAnt()
{
    var hfuncion=gE('idProcedimiento');
    var idGrado=gE('id');
    var comboGradoAnt=gE('idGradoAntint');
    var idGradoAnt=comboGradoAnt.options[comboGradoAnt.selectedIndex].value;	
    if(idGrado=='-1')
    {
        idGrado='idRegPadre';
        
    }

    hfuncion.value='asignaGradoAnterior('+idGrado.value+','+idGradoAnt+')';
}

function porcentajeL()
{
    var comboEsquema=gE('_esquemaEvaluacionint');
    var opcionEsquema=comboEsquema.options[comboEsquema.selectedIndex].value;
    
    var combo=gE('_materiasLibresint');
    var opcion=combo.options[combo.selectedIndex].value;
    
    if(opcion=='1' && opcionEsquema=='0')
    {
        mE('ponderaL');
    	var control=gE('_porcentajeLibresflo');
        control.setAttribute('val','obl');
        
        var combo2=gE('_materiasOptativasCint');
		var opcion2=combo2.options[combo2.selectedIndex].value;
        if(opcion2=='0')
        {
        	control.value=100;
        }
       
    }
    else
    {
    	oE('ponderaL');
    	var control=gE('_porcentajeLibresflo');
        control.removeAttribute('val');
        control.value=0;
        
        var control2=gE('_porcentajeOptativaCflo');
        control2.value=100;
	}
}

function  porcentajeOc()
{
    var comboEsquema=gE('_esquemaEvaluacionint');
    var opcionEsquema=comboEsquema.options[comboEsquema.selectedIndex].value;
    
    var combo=gE('_materiasOptativasCint');
	var opcion=combo.options[combo.selectedIndex].value;
    
    if(opcion=='1')
    {
    	mE('numero');
        mE('numeroMax');
        
        if(opcionEsquema=='0')
        {	
            mE('ponderaOc');
        	var control=gE('_porcentajeOptativaCflo');
        	control.setAttribute('val','obl');
        }
        else
        {
        	oE('ponderaOc');
            var control=gE('_porcentajeOptativaCflo');
        	control.removeAttribute('val');
        	control.value=0;
        }
        var control2=gE('_noMateriaOpCint');
        control2.setAttribute('val','obl');
        
        var combo2=gE('_materiasLibresint');
        var opcion2=combo.options[combo2.selectedIndex].value;
        if(opcion2=='0')
        {
           control.value=100;
        }
    }
    else
    {
    	oE('numero');
        oE('ponderaOc');
        oE('numeroMax');
        var control=gE('_porcentajeOptativaCflo');
        control.removeAttribute('val');
        control.value=0;
        var control2=gE('_noMateriaOpCint');
        control2.removeAttribute('val');
        selElemCombo(control2,'-1');
        
        var combo2=gE('_porcentajeLibresflo');
        combo2.value=100;
        
        var control3=gE('_noMaxMateriaOpCint');
        control3.removeAttribute('val');
        selElemCombo(control3,'-1');
    }
}

function muestraP()
{
     porcentajeL();
     porcentajeOc();
}

function mostrarPonderacionGrado()
{
	var esquemaMapa=gE('esquema');
    var ctrPonGrado=gE('_ponderacionflo');
    
    if(esquemaMapa=='0')
    {
        ctrPonGrado.setAttribute('val','obl');
        mE('ponderaGrado');
    }
	else
    {
        ctrPonGrado.removeAttribute('val');
    	oE('ponderaGrado');
    }
}
