Ext.onReady(inicializar);

function inicializar()
{
	gE('area').focus();
}


var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	var accion=gE('Nuevo').value;
	Guardar('tabla','Unidades',accion);
}

function Guardar(f,form,accion)
{
	if (!validarFormularios(f))
	{
		return;
	}
	if(accion=="Guardar")
	{
		var area=gE('area');
		var cod=gE('codigo');
		
		
		
		if(cod.value=='00')
		{
			Ext.MessageBox.alert('CLH','El Código especificado no es válido para una Unidad, por favor verifique.');
			cod.focus();
			return;
		}
		
		var lon=cod.value;
		if(lon.length < 2)
		{
			Ext.MessageBox.alert('CLH','Una Unidad consta de 2 números en su código, por favor verifique.');
			cod.focus();
			return;
		}
			
		function funcTratarRespuesta()
		{
			var resp=peticion_http.responseText;
			if(resp==0 || resp=='0')
			{
				function OK()
				{
					var formulario=gE(form);
					formulario.submit();
				}
				OK();
			}
			else
			{
				Ext.MessageBox.alert('CLH','El Código especificado ya existe, por favor verifique.');
				cod.focus();
			}
		}
		obtenerDatosWeb('validarExisten.php',funcTratarRespuesta, 'POST','funcion=3&codigo='+cod.value+'&area='+area.value);
	}
	else
	{
		var area=gE('area');
		var cod=gE('codigo');
		
		
		
		if(cod.value=='00')
		{
			Ext.MessageBox.alert('CLH','El Código especificado no es válido para una Unidad, por favor verifique.');
			cod.focus();
			return;
		}
		
		var lon=cod.value;
		if(lon.length < 2)
		{
			Ext.MessageBox.alert('CLH','Una Unidad consta de 2 números en su código, por favor verifique.');
			cod.focus();
			return;
		}
			
		var formulario=gE(form);
		formulario.submit();
	}
}

function Enviar(form)
{
	var formulario=gE(form);
	formulario.submit();
}