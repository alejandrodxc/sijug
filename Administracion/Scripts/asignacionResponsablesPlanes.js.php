<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idPrograma,nombrePrograma FROM 4004_programa WHERE idPrograma IN (select idPrograma FROM 4020_programasVSSede WHERE sede ='".$_SESSION["codigoInstitucion"]."')";

	$arrProgramas=$con->obtenerFilasArreglo($consulta);
?>

Ext.onReady(inicializar);
var arrProgramas=<?php echo $arrProgramas ?>;
function inicializar()
{
	crearGridAsignacion();
}

function crearGridAsignacion()
{
	
    
	var lector= new Ext.data.JsonReader({
                                            idProperty:'idAsignacion',
                                            fields: [
                                               			{name:'idResponsable'},
                                                        {name: 'idPrograma'},
                                                        {name: 'programa'},
                                                        {name:'idUsuario'},
                                                        {name:'nombre'}
                                            		],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: false
                                        }
                                      );
                                      
	var dsTablaRegistros2=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesProgramaAcademicoV2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombre', direction: 'ASC'},
                                                            groupField: 'programa',
                                                            autoLoad:true
                                                        })                                      
     
	dsTablaRegistros2.on('beforeload',function(proxy)
    								{
                                    	
                                    	proxy.baseParams.funcion=4;
                                        proxy.baseParams.codigoInstitucion='<?php echo $_SESSION["codigoInstitucion"]?>';
                                    }
                        )
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});                    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer({width:30}),
														chkRow,
                                                        
                                                        {
                                                        	header:'Plan de estudio',
															width:400,
															sortable:true,
															dataIndex:'programa'
                                                        },
                                                         {
                                                        	header:'Responsable',
															width:300,
															sortable:true,
															dataIndex:'nombre'
                                                        }
													]
												);
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:dsTablaRegistros2,
                                                            title:'Responsables de administraci&oacute;n de planes de estudio',
                                                            frame:true,
                                                            renderTo:'tblAsignacionResp',
                                                            cm: cModelo,
                                                            height:700,
                                                            width:950,
                                                            sm:chkRow,
                                                            id:'gridResponsables',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Agregar responsable',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	mostrarVentanaResponsable(null);	
                                                                                }
                                                                        },
                                                                       
                                                                        {
                                                                        	text:'Remover responsable',
                                                                            icon:'../images/cancel_round.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            	{
                                                                                	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                	if(fila==null)
                                                                                    {
                                                                                    	msgBox('Debe seleccionar la asignaci&oacute;n a remover');
                                                                                    	return;
                                                                                    }
                                                                                    
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    tblGrid.getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=7&idResponsable='+fila.get('idResponsable'),true);
                                                                                       }
                                                                                   }
                                                                                   msgConfirm('Est&aacute; seguro de querer remover la asignaci&oacute;n seleccionada?',resp);
                                                                                }
                                                                        }
                                                            		],
                                                         	view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true
                                                                                            	}   
                                                                                            )       
                                                        }
                                                    );            
}

var campoBusqueda;

function mostrarVentanaResponsable(fila)
{
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
    var cmbPrograma=crearComboExt('cmbPrograma',arrProgramas,195,70,400);
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Usuario:'
                                                        },
                                                        cmbComboBusqueda,
                                                        
                                                        {
                                                        	x:10,
                                                            y:75,
                                                            html:'Responsable en el plan de estudios:'
                                                           
                                                        },
                                                        cmbPrograma

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar Empleado',
										width: 800,
										height:220,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbComboBusqueda.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbComboBusqueda.focus();
                                                                            }
                                                                        	msgBox('Debe seleccionar el usuario a agregar como responsable',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if((cmbPrograma.getValue()==''))
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbPrograma.focus();
                                                                            }
                                                                        	msgBox('Debe seleccionar el plan de estudios sobre el cual ser&aacute; responsable el usuario seleccionado',resp3);
                                                                            return;
                                                                        }
                                                                        var idPrograma=cmbPrograma.getValue();
                                                                        
                                                                        if(existeAsignacion(idPrograma,cmbComboBusqueda.getValue()))
                                                                        {
                                                                        	 ventanaAM.close();
                                                                        	return;
                                                                        }
                                                                        var obj='{"sede":"<?php echo $_SESSION["codigoInstitucion"]?>","idUsuario":"'+cmbComboBusqueda.getValue()+'","idPrograma":"'+idPrograma+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridResponsables').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=6&obj='+obj,true);
                                                                       
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}
function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../paginasFunciones/funcionesProgramaAcademicoV2.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'departamento'}
											]
										);
	var parametros=	{
						funcion:'5',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;

	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br><span class="letraRojaSubrayada8">Departamento:</span> {departamento}',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:85,
                                                        y:35
													}
												 );
	
   	return comboNombre;
}

function existeAsignacion(idPrograma,idUsuario)
{
	var gridResponsables=gEx('gridResponsables').getStore();
    var x;
    var fila;
    for(x=0;x<gridResponsables.getCount();x++)
    {
		fila=gridResponsables.getAt(x);
        if((fila.get('idUsuario')==idUsuario)&&(fila.get('idPrograma')==idPrograma))
        	return true;    
    }
    return false;
}
