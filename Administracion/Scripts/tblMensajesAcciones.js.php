<?php
session_start();
include("latis/configurarIdiomaJS.php");
include("latis/conexionBD.php");

$consulta="select idAccion,accionEnvio from 2003_accionesEnvio where idIdioma=".$_SESSION["leng"]." and idAccion not in (select idAccionEnvio from 2004_mensajesAcciones)";
$arrAcciones=$con->obtenerFilasArreglo($consulta);
?>


Ext.onReady(inicializar);

function inicializar()
{
	return;
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            items:	[
                                           				
	                                           		]
                                        }
                                     ]
						}
                    )
}

function nuevoMensajeAccion()
{
	var accionesMg=<?php echo $arrAcciones ?>;
	var comboAcciones=crearComboExt('comboAcciones',accionesMg,215,5);
    comboAcciones.setWidth(250);
	var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
														 	new Ext.form.Label	(
																				 	{
																						x:5,
																						y:10,
																						html:'Acci&oacute;n que disparar&aacute; el mensaje:'
																					}
																				)
															,
															comboAcciones
														]
											}
										);
	
	var ventanaPar = new Ext.Window	(
									{
										title: 'Agregar mensaje de acci&oacute;n',
										width: 500,
										height:130,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										buttons:	[
														{
															text: 'Aceptar',
															handler:function()
																	{
																		var idAccion=comboAcciones.getValue();
                                                                        if(idAccion=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	comboAcciones.focus();
                                                                                
                                                                            }
                                                                        	msgBox('Debe seleccionar la acci&oacute;n que disparar&aacute; el evento',resp);
                                                                        	return;
                                                                        }
                                                                         var arrParam=[['idAccion',idAccion],['idCircular','-1']];
	                                                                    enviarFormularioDatos('../Administracion/mensajesAcciones.php',arrParam);
																	}
                                                                    
                                                                   
                                                                    
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventanaPar.close();
																	}
														}
													]
    								}
								);
                                
                                
                                
    ventanaPar.show();
}

function ejecutarAccion(accion,idCircular)
{
	switch(accion)
	{
		case '-1':
			eliminarCircular(idCircular);
		break;
		case '1':
			var form=gE('frmEnvio');
			gE('idCircular').value=idCircular;
			form.action='mensajesAcciones.php';
			form.submit();
		break;
		case '2':
			var form=gE('frmEnvio');
			gE('idCircular').value='-1';
			form.action='mensajesAcciones.php';
			form.submit();
		break;
	}
}



function eliminarCircular(idCircular)
{
	function funcRespuesta(btn)
	{
		if(btn=='yes')
		{
			function funcAjax()
			{
				var resp=peticion_http.responseText.split('|')[0];
				if(resp=='1')
				{
					var fila=gE('fila_'+idCircular);
                    fila.parentNode.removeChild(fila);
				}
				else
				{
					msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: '+' <br />'+resp);
				}
			}
			obtenerDatosWeb("../paginasFunciones/funcionesPortal.php",funcAjax,'POST','funcion=3&idCircular='+idCircular,true);
			
			
		}
	}
	
	Ext.MessageBox.confirm('<?php echo $etj["lblAplicacion"]?>','Est&aacute; seguro de querer eliminar este registro?',funcRespuesta);
}
