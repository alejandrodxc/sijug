Ext.onReady(inicializar);


function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_titulovch').focus();
}

function validarFrm(formulario)
{
	if(validarFormularios(formulario))
	{
		gE(formulario).submit();
	}
}

function funcTipoCriterioChange(cmb)
{
	if(cmb.selectedIndex==1)
    {
        oE('filaEvidencias2');
        gE('_funcionEvaluacionint').value='';
        gE('lbl_funcionEvaluacionint').innerHTML='(No especificado)';
        gE('_funcionEvaluacionint').setAttribute('val','');
        
    }
    else
    {
       	if(cmb.selectedIndex==0)
        {
        	oE('filaEvidencias2');
            gE('_funcionEvaluacionint').value='';
            gE('lbl_funcionEvaluacionint').innerHTML='(No especificado)';
            gE('_funcionEvaluacionint').setAttribute('val','');
        }
        else
        {
        	mE('filaEvidencias2');
            gE('_funcionEvaluacionint').setAttribute('val','obl');
        }
    }
}

function funcMaximoCriterio(cmb)
{
	var opcion=cmb.options[cmb.selectedIndex].value;
    switch(opcion)
    {
    	case '2':
        	mE('filaValorCriterio1');
            oE('filaValorCriterio2');
            gE('_funcionSistemaValMaxint').setAttribute('val','');
            gE('_funcionSistemaValMaxint').value='';
            gE('lbl_funcionSistemaValMaxint').innerHTML='(No especificado)';
            gE('_valorMaximoint').setAttribute('val','obl');
            
        break;
        case '3':
        	mE('filaValorCriterio2');
            oE('filaValorCriterio1');
            gE('_funcionSistemaValMaxint').setAttribute('val','obl');
            gE('_valorMaximoint').setAttribute('val','');
            gE('_valorMaximoint').value='';
        
        break;
        default:
        	oE('filaValorCriterio2');
            oE('filaValorCriterio1');
            gE('_funcionSistemaValMaxint').setAttribute('val','');
	        gE('_funcionSistemaValMaxint').value='';
            gE('lbl_funcionSistemaValMaxint').innerHTML='(No especificado)';
            gE('_valorMaximoint').setAttribute('val','');
            gE('_valorMaximoint').value='';
        break;
    }
	
}
