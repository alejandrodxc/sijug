<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	var gridRango=gEx('gridRango');
    
    var x;
    var fila;
    var cadObj='';
    for(x=0;x<gridRango.getStore().getCount();x++) 
    {
    	fila=gridRango.getStore().getAt(x);
        obj='{"leyendaRango":"'+fila.get('leyendaRango')+'","condValorMinimo":"'+fila.get('condValorMinimo')+'","valorMinimo":"'+fila.get('valorMinimo')+'","condValorMaximo":"'+fila.get('condValorMaximo')+
        	'","valorMaximo":"'+fila.get('valorMaximo')+'","valorEquivalencia":"'+fila.get('valorEquivalencia')+'","situacion":"'+fila.get('situacion')+'"}';
       	if(cadObj=='')
        	cadObj=obj;
        else
        	cadObj+=','+obj;
    }
     var objArr='{"arrRango":['+cadObj+']}';
     var id=gE('idEscala').value;

   
    gE('objEscalas').value=bE(objArr);

}

function inicializar()
{
	crearGridEscala();
	gE('_nombreEscalavch').focus();
}

function validar(formulario)
{
	if(validarFormularios(formulario))
	{
    	funValidar();
		gE(formulario).submit();
	}
}

function agregarNuevoElemento()
{
	var idEscalaCalificacion=gE('idEscala').value;
    var obj={};
    obj.ancho=800;
    obj.alto=350;
    obj.titulo='Agregar escala de calificación';
    obj.params=[['idEscalaCalificacion',idEscalaCalificacion],['cPagina','sFrm=true']];
    obj.url='../Administracion/configuracionEscala.php';
    abrirVentanaFancy(obj);
}

function modificarEscala(iE)
{
    var obj={};
    obj.ancho=800;
    obj.alto=350;
    obj.titulo='Modificar escala de calificación';
    obj.params=[['idElementoEscala',Base64.decode(iE)],['cPagina','sFrm=true']];
    obj.url='../Administracion/configuracionEscala.php';
    abrirVentanaFancy(obj);
}

function removerEscala(iE)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    var fila=gE('fila_'+bD(iE));
                    fila.parentNode.removeChild(fila);
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=49&idEscala='+bD(iE),true);
    	}
            
	}
    msgConfirm('Est&aacute; seguro de querer eliminar el intervalo de escala seccionado?',resp);
}


var registroGrid=crearRegistro([
                                                                    {name: 'idRango'},
                                                                    {name: 'leyendaRango'},
                                                                    {name: 'condValorMinimo'},
                                                                    {name: 'valorMinimo'},
                                                                    {name: 'condValorMaximo'},
                                                                    {name: 'valorMaximo'},
                                                                    {name: 'valorEquivalencia'},
                                                                    {name: 'situacion'}
                                                                    
                                                                ]);

function crearGridEscala()
{

	var dsDatos=eval(bD(gE('arrEscalas').value));
    var arrSituacion=[['2','No aprobado'],['1','Aprobado']];
    var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion);
    var arrCondicionMayor=[['>','Mayor que'],['>=','Mayor o igual que'],['=','Igual que']];
    var cmbCondMayor=crearComboExt('cmbCondMayor',arrCondicionMayor);
    
    var arrCondicionMenor=[['<','Menor que'],['<=','Menor o igual que'],['=','Igual que']];
    var cmbCondMenor=crearComboExt('cmbCondMenor',arrCondicionMenor);
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idRango'},
                                                                    {name: 'leyendaRango'},
                                                                    {name: 'condValorMinimo'},
                                                                    {name: 'valorMinimo'},
                                                                    {name: 'condValorMaximo'},
                                                                    {name: 'valorMaximo'},
                                                                    {name: 'valorEquivalencia'},
                                                                    {name: 'situacion'}
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editor',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
	editorFila.on('afteredit',fEditorFilaAfterEditCampoGrid)                                                
    editorFila.on('beforeedit',fEditorFilaBeforeEditCampoGrid)
    editorFila.on('validateedit',fEditorValidaCampoGrid);
    editorFila.on('canceledit',fEditorCancelEditCampoGrid);
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Leyenda <span class="letraRoja">*</span>',
															width:150,
															sortable:true,
															dataIndex:'leyendaRango',
                                                            editor:{xtype:'textfield'}
														},
                                                        {
															header:'Condici&oacute;n de<br>Valor minimo <span class="letraRoja">*</span>',
															width:100,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'condValorMinimo',
                                                            editor:cmbCondMayor,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCondicionMayor,val);
                                                                    }
														},
														{
															header:'Valor m&iacute;nimo <span class="letraRoja">*</span>',
															width:100,
															sortable:true,
															dataIndex:'valorMinimo',
                                                            editor:{xtype:'numberfield',allowDecimals:true}
														},
                                                        {
															header:'Condici&oacute;n de<br>Valor m&aacute;ximo <span class="letraRoja">*</span>',
															width:100,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'condValorMaximo',
                                                            editor:cmbCondMenor,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCondicionMenor,val);
                                                                    }
														},
														{
															header:'Valor m&aacute;ximo <span class="letraRoja">*</span>',
															width:100,
															sortable:true,
															dataIndex:'valorMaximo',
                                                            editor:{xtype:'numberfield',allowDecimals:true}
														},
														{
															header:'Valor a asentar',
															width:110,
                                                            hidden:true,
															sortable:true,
															dataIndex:'valorEquivalencia',
                                                            editor:{xtype:'numberfield',allowDecimals:true}
														},
														{
															header:'Situaci&oacute;n',
															width:90,
                                                            hidden:true,
															sortable:true,
															dataIndex:'situacion',
                                                            editor:cmbSituacion,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrSituacion,val);
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridRango',
                                                            store:alDatos,
                                                            frame:false,
                                                            renderTo:'tblGridRango',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:670,
                                                            sm:chkRow,
                                                            plugins:[editorFila],
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAgregarRango',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar rango',
                                                                            handler:function()
                                                                            		{
                                                                                    	var nReg=new registroGrid	(
                                                                                        								{
                                                                                                                        	idRango:-1,
                                                                                                                            leyendaRango:'',
                                                                                                                            condValorMinimo:'',
                                                                                                                            valorMinimo:'0',
                                                                                                                            condValorMaximo:'',
                                                                                                                            valorMaximo:'0',
                                                                                                                            valorEquivalencia:'0',
                                                                                                                            situacion:'0'
                                                                                                                        }
                                                                                                                      )
                                                                                          
                                                                                          editorFila.stopEditing();
                                                                                          tblGrid.getStore().add(nReg);
                                                                                          tblGrid.nuevoRegistro=true;
                                                                                          editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                          Ext.getCmp('btnAgregarRango').disable();
                                                                                          Ext.getCmp('btnRemoverRango').disable();		
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnRemoverRango',
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover rango',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos un elemento a remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover los elementos seleccionados?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;		

}


function fEditorFilaAfterEditCampoGrid(rowEdit,obj,registro,nFila)
{
}


function fEditorFilaBeforeEditCampoGrid(rowEdit,fila)
{
	var idGrid='gridRango';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function fEditorValidaCampoGrid(rowEdit,obj,registro,nFila)
{
	
	var idGrid='gridRango';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if((valor=='')&&(valor!='0'))
			{
            	
				function funcResp()
				{
					
				}
				msgBox('La columna <b>'+cm.getColumnHeader(x).replace('*','')+'</b> no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   
    var resValidacion=true;
    if(resValidacion)
    {
        Ext.getCmp('btnAgregarRango').enable();	
		Ext.getCmp('btnRemoverRango').enable();
        grid.nuevoRegistro=false;
		return true;
   }
   else
   {
   		return false;
  	}
	
}

function fEditorCancelEditCampoGrid(rowEdit,cancelado)
{

	var idGrid='gridRango';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnRemoverRango').enable();
    Ext.getCmp('btnAgregarRango').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}