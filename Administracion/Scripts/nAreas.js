Ext.onReady(inicializar);

function inicializar()
{
	gE('codigo').focus();
}

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	var accion=gE('Nuevo').value;
	Guardar('tabla','Areas',accion);
}

function Guardar(f,form,accion)
{
	if (!validarFormularios(f))
	{
		return;
	}
		
	if(accion=="Guardar")
	{
		var cod=gE('codigo');
		var lon=cod.value;
		if(lon.length < 3)
		{
			Ext.MessageBox.alert('CLH','Un Área consta de 3 números en su código, por favor verifique.');
			cod.focus();
			return;
		}
		
		function funcTratarRespuesta()
		{
			var resp=peticion_http.responseText;
			if(resp==0 || resp=='0')
			{
				function OK()
				{
					var formulario=gE(form);
					formulario.submit();
				}
				OK();
			}
			else
			{
				Ext.MessageBox.alert('CLH','El Código especificado ya existe, por favor verifique.');
				cod.focus();
			}
		}
		obtenerDatosWeb('validarExisten.php',funcTratarRespuesta, 'POST','funcion=1&codigo='+cod.value);
	}
	else
	{
		var cod=gE('codigo');
		var lon=cod.value;
		if(lon.length < 3)
		{
			Ext.MessageBox.alert('CLH','Un Área consta de 3 números en su código, por favor verifique.');
			cod.focus();
			return;
		}
		var formulario=gE(form);
		formulario.submit();
	}
}

function Enviar(form)
{
	var formulario=gE(form);
	formulario.submit();
}