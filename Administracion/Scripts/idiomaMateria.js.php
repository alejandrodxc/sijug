Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_nombreIdiomavch').focus();
}

function validarFrm(formulario)
{
	if(validarFormularios(formulario))
	{
		gE(formulario).submit();
	}
}
