<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			crearGridPlanesEstudio(),
                                        crearGridProgramaSedes()
                                        
                                     ]
						}
                    )   
}


function crearGridPlanesEstudio()
{
	var lector= new Ext.data.JsonReader({

                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'idPrograma'},
                                                        {name: 'nombrePrograma'},
                                                        {name: 'codRvoe'},
                                                        {name: 'descripcion'},
                                                        {name: 'nombreStatus'},
                                                        {name: 'idMapaCurricular'}
                                                        
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesProgramaAcademicoV2.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'nombrePrograma', direction: 'ASC'},
                                                groupField: 'nombrePrograma',
                                                autoLoad:true,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('nombrePrograma', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=1;
                                    }
				)	
                
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'codRvoe'},
                                                                        {type: 'string', dataIndex: 'nombrePrograma'}
                                                                    ]
                                                    }
                                                );                   
                
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'RVOE',
															width:150,
															sortable:true,
															dataIndex:'codRvoe'
														},
														{
															header:'<?php echo $tituloPrograma?>',
															width:300,
															sortable:true,
															dataIndex:'nombrePrograma'
														},
														{
															header:'Descripci&oacute;n',
															width:300,
															sortable:true,
															dataIndex:'descripcion'
														},
                                                        {
															header:'Situaci&oacute;n',
															width:120,
															sortable:true,
															dataIndex:'nombreStatus'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            store:alDatos,
                                                            title: '<span class="letraRojaSubrayada8"><?php echo $tituloProgramaPlural?></span>',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loaMask:true,
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            plugins:[filters],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Crear <?php echo $tituloPrograma?>',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrDatos=[['idPrograma','-1']];
                                                                                        enviarFormularioDatos('../Administracion/programa.php',arrDatos);
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar <?php echo $tituloPrograma?>',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el <?php echo $tituloPrograma?> que desea modificar')
                                                                                        	return;
                                                                                        }
                                                                                    	var arrDatos=[['idPrograma',fila.get('idPrograma')]];
                                                                                        enviarFormularioDatos('../Administracion/programa.php',arrDatos);
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover <?php echo $tituloPrograma?>',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el <?php echo $tituloPrograma?> que desea remover')
                                                                                        	return;
                                                                                        }	
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                     	tblGrid.getStore().remove(fila);   
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=2&idPrograma='+fila.get('idPrograma'),true);

                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el <?php echo $tituloPrograma?> seleccionado?',resp)
                                                                                    }
                                                                            
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Modificar estructura curricular del <?php echo $tituloPrograma?>',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(fila==null)
                                                                                            {
                                                                                                msgBox('Debe seleccionar el <?php echo $tituloPrograma?> cuya estructura curricular desea modificar')
                                                                                                return;
                                                                                            }
                                                                                            if(fila.get('idMapaCurricular')=='')
                                                                                            {
                                                                                            	var arrParam=[['iPrograma',fila.get('idPrograma')]];
                                                                                            	var obj={
                                                                                                			titulo:'Configurar estructura curricular',
                                                                                                            url:'../programaAcademico/nuevoMapaCurricular.php',
                                                                                                            params:arrParam,
                                                                                                            width:800,
                                                                                                            height:450
                                                                                                		}
                                                                                            	abrirVentanaFancy(obj);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                var arrDatos=[['idMapaCurricular',fila.get('idMapaCurricular')]];
                                                                                                enviarFormularioDatos('../programaAcademico/mapaCurricularAcademico.php',arrDatos);
                                                                                            }
                                                                                        }
                                                                                
                                                                        	
                                                                        }
                                                            		]
                                                        }
                                                    );
	tblGrid.getSelectionModel().on('rowselect',function (sm,nFila,registro)
    											{
                                                	gEx('gridSedes').getStore().load({url:'../paginasFunciones/funcionesProgramaAcademicoV2.php',params:{funcion:3,idPrograma:registro.get('idPrograma')}});
                                                }
    									)                                                    
	return 	tblGrid;
}

function crearGridProgramaSedes()
{
	var lector= new Ext.data.JsonReader({

                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'codigoUnidad'},
                                                        {name: 'unidad'}
                                                        
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesProgramaAcademicoV2.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'unidad', direction: 'ASC'},
                                                groupField: 'unidad',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('unidad', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=3;
                                    }
				)	
                
	                 
                

	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),

														{
															header:'Sede',
															width:400,
															sortable:true,
															dataIndex:'unidad'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'gridSedes',
                                                            collapsible:true,
                                                            store:alDatos,
                                                            title: '<span class="letraRojaSubrayada8">Sedes con las cuales se comparte el <?php echo $tituloPrograma?></span>',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            height:200,
                                                            region:'south',
                                                            frame:false,
                                                            border:false

                                                            
                                                        }
                                                    );
	return 	tblGrid;
}

