<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
Ext.onReady(inicializar);

function inicializar()
{

	nuevoDTD();
}

var panelArbol;
function nuevoDTD()
{
	 
  	 var idMat=gE('idMateria').value;
	 var nom=gE('nombre').value;
     var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'-1',
                                                          text:'Raiz',
                                                          draggable:false,
                                                          expanded :true
                                                      }
                                              	)
      
      var cargadorArbol=new Ext.tree.TreeLoader(
												   {
												     baseParams:{
																
																funcion:'29',
																idMateria:idMat
															},
												   
												   
														dataUrl:'../paginasFunciones/funcionesAdministracion.php'
													}	

		                                         )	
		//function preparar(cargador)
//		{
//			var idMateria=gE('idMateria').value;
//			cargador.baseParams.funcion=3;
//			cargador.baseParams.idMateria=idMateria;
//		
//		}                            
                                                 
                                                 
		//cargadorArbol.on('beforeload')//,preparar)                                                 
		panelArbol=new Ext.tree.TreePanel	(
                                              {
												  id:'tblArbol',
                                                  useArrows:true,
                                                  autoScroll:true,
                                                  animate:false,
                                                  enableDD:true,
                                                  containerScroll:true,
                                                  root:nom,
                                                  loader:cargadorArbol,
                                                  height:500,
												  width:600,
												  collapsible: true,
                                                  draggable:false,
												  rootVisible:true,
												  el:'divArbol'
											  }
                                                  
                                          );                                                 	  
      
      
	 
      panelArbol.render();
      panelArbol.expandAll();

}


function guardar(form)
{
	gE(form).action="../Administracion/fichaMateria.php";
    gE(form).submit();			
}

function regresar(form)
{
	var arrP=[];
	enviarFormularioDatos('../Administracion/fichaMateria.php',arrP);				
}


function agregarTema(idMateria1,idMateria,idCiclo)
{
	 var idMateria1='-'+gE('idMateria').value;
     var idMateria=gE('idMateria').value;
      var idCiclo=gE('idCiclo').value;
     
    var arrP=[['idPadre',idMateria1],['idMateria',idMateria],['idCiclo',idCiclo]];
	enviarFormularioDatos('../Administracion/temas.php',arrP);										
}

function agregarSubtemas(idMateria,idCiclo)
{
	var selNodes = panelArbol.getChecked();
    var idTema=selNodes[0].id;
    var idMateria=gE('idMateria').value;
    var idCiclo=gE('idCiclo').value;
    
            var arrP=[['idPadre',idTema],['idMateria',idMateria],['idCiclo',idCiclo]];
            enviarFormularioDatos('../Administracion/temas.php',arrP);										


}


function modificarTema(idMateria,idCiclo)
{
	var selNodes = panelArbol.getChecked();
    var idTema=selNodes[0].id;
    var idMateria=gE('idMateria').value;
    var idCiclo=gE('idCiclo').value;
   
    var arrP=[['idTema',idTema],['idMateria',idMateria],['idCiclo',idCiclo]];
	enviarFormularioDatos('../Administracion/temas.php',arrP);										
   
}

function eliminarTema()
{
  var selNodes = panelArbol.getChecked();
  
  var idTema='';
	
	for(x=0;x<selNodes.length;x++)
	
	{	
		if(idTema=="")
			idTema=selNodes[x].id;
		else
			idTema+=","+selNodes[x].id;
			
	}
        function respPregunta(btn)
        {
            if(btn=='yes')
            {
                    function funcAjax()
                    {
                        var resp=peticion_http.responseText;
                        arrResp=resp.split('|');
                        if(arrResp[0]=='1')
                        {
                            var raiz=panelArbol.getRootNode() ;
                            raiz.reload();
							panelArbol.expandAll();
                           
                        }
                    	else
                    	{
                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                    	}
            		}
                obtenerDatosWeb('../paginasFunciones/funcionesAdministracion.php',funcAjax, 'POST','funcion=27&idTema='+idTema,true);
            }
                  
        }
      Ext.MessageBox.confirm(lblAplicacion,'Si elimina este tema se borraran los temas asociados.<br />Est&aacute; seguro de querer eliminar este registro',respPregunta);   
    
    									
}

function asigna(radio)
{
	var valor=radio.value;
    gE('idTema').value=valor;
}

function eliminarTabla()
{
	 var valor=gE('idTema').value;
     var tr=gE('tr_'+valor);
     var padre=tr.parentNode;
     padre.removeChild(tr);
     
}

function agregarSubtema()
{

      var selNodes = panelArbol.getChecked();
      if (selNodes==0)
          Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
      if(selNodes.length>1)
          Ext.MessageBox.alert(lblAplicacion,'Seleccione solo un tema')
      if(selNodes.length==1)
      {
          
         agregarSubtemas();
         selNodes=null;
      }
}

function modificar()
{
    var selNodes = panelArbol.getChecked();
    
    if (selNodes==0)
    
        Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
    if(selNodes.length>1)
        Ext.MessageBox.alert(lblAplicacion,'Seleccione solo un tema para modificar')
    if(selNodes.length==1)
    {
        
       modificarTema();
       selNodes=null;
        
    }
}

function eliminar()
{
	var selNodes = panelArbol.getChecked();
     if (selNodes==0)
        
            Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar al menos un tema ')
     else
     
            eliminarTema();
}