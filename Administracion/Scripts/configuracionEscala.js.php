<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_etiquetavch').focus();
}

function validar(formulario)
{
	if(validarFormularios(formulario))
	{
    	var idElemento=gE('_idElementoEscalaint');
        if(idElemento!=null)
        	idElemento=idElemento.value;
        else
        	idElemento=-1;
        var idEscala=gE('_idEscalaCalificacionint');
        if(idEscala!=null)
        	idEscala=idEscala.value;
        else
        	idEscala=-1;
        
    	var cadObj='{"idElemento":"'+idElemento+'","minimo":"'+gE('_valorMinimoflo').value+'","maximo":"'+gE('_valorMaximoflo').value+'","idEscala":"'+idEscala+'"}';

    	function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
            	if(arrResp[1]=='0')
	                gE(formulario).submit();
                else
                	msgBox('Algunos valores de intervalo comprendido entre el valor m&iacute;nimo y el valor m&aacute;ximo, ya se encuentran considerados dentro de otra configuraci&oacute;n de escala');
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesProgramaAcademicoV2.php',funcAjax, 'POST','funcion=48&cadObj='+cadObj,true);
	}
}
