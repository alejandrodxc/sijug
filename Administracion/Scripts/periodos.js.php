Ext.onReady(inicializar);

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: funValidar,
										scope: this
									}
							);

function funValidar()
{
	validar('frmEnvio');
}

function inicializar()
{
	gE('_nombrePeriodovch').focus();
}

function validar(formulario)
{
	if(validarFormularios(formulario))
	{
		gE(formulario).submit();
	}
}
