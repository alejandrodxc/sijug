<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
?>

<!-- InstanceBeginEditable name="EditRegion5" -->


<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<?php 
	//$pagRegresar="../Administracion/tblPrograma.php";
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
?>
<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
                                  ?>
                                  
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
                      
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <?php
					$codUnidad=$_SESSION["codigoInstitucion"];
					//echo $_SESSION["codigoInstitucion"];
					if(isset($objParametros->idPrograma))
						$idPrograma=$objParametros->idPrograma;
					else
						$idPrograma="-1";
					
					?>
					<script type="text/javascript" src="../Administracion/Scripts/programa.js.php?idPrograma=<?php echo $idPrograma?>"></script>
					<?php
					$consulta="select * from 4004_programa where idPrograma=".$idPrograma;
					$fila=$con->obtenerPrimeraFila($consulta);
					$tipoProg=$fila[23];
					
					
					$atributo="";
					?>
                   <tr>
                        <td align="center">
                           <br />
						   <form id="frmEnvio" method="post" action="../Administracion/guardarPrograma.php"> 
                           <table>
                              
                              <tr>
                                <td align="center" width="850">
                                <fieldset class="frameHijo" ><legend><b><?php echo $tituloPrograma?></b></legend>
                                    <br />
                                <table>
                                	<tr height="23">
                                        <td width="250" align="right"><label>Nombre de <?php echo $tituloPrograma?>:<font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                        <td width="500" align="left"><input type="text" name="_nombreProgramavch" id="_nombreProgramavch" maxlength="150" size="69" val="obl" campo="Nombre de <?php $tituloPrograma?>" value="<?php echo $fila[4]?>" <?php echo $atributo?>/></td>
                                      </tr>
                                       <tr height="23">
                                        <td  align="right"><label>Clave Rvoe:<font color="#FF0000"></font></label>&nbsp;&nbsp;</td>
                                        <td  align="left"><input type="text" name="_codRvoevch" id="_codRvoevch" maxlength="150" size="69"  value="<?php echo $fila[22]?>"/></td>
                                      </tr>
                                      
                                      <tr height="23">
                                        <td align="right" valign="top"><label>Descripción:</label>&nbsp;&nbsp;</td>
                                        <td align="left"><textarea rows="4" cols="70" id="_descripcionvch" name="_descripcionvch" <?php echo $atributo?>><?php echo $fila[5]?></textarea>							
                                        </td>
                                      </tr>
                                	<?php
                                    if($preguntarNivel)
                                    {
                                	?>
                                     <tr height="23">
                                          <td align="right"><label>Nivel: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                          <td align="left">
                                              <select name="_idNivelint" id="_idNivelint" val="obl" campo="Nivel" <?php echo $atributo?>>
                                              <option value="-1">Seleccione</option>
                                              <?php
                                                  $consulta="Select idNivel,nombreNivel from 4003_nivel where status='1'";
                                                  $con->generarOpcionesSelect($consulta,$fila[1]);
                                              ?>
                                              </select>
                                          </td>
                                     </tr>
                                 <?php
                                    }
                                    else
                                    {
                                ?>
                                    <input type="hidden" name="_idNivelint" id="_idNivelint" value="-1">
                                <?php
                                    }
                                 ?>
                                     <tr height="23">
                                     	<td align="right" ><label>Periodo: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                    	<td align="left">
                                            <select name="_idPeriodoint" id="_idPeriodoint" val="obl" campo="Periodo"  <?php echo $atributo?>>
                                            <option value="-1">Seleccione</option>
                                            <?php
                                                $consulta="Select idPeriodo,nombrePeriodo from 4019_periodos";
                                                $con->generarOpcionesSelect($consulta,$fila[2]);
                                            ?>
                                            </select>
                                        </td>
                                     </tr>
                                     <tr height="23">
                                     	<td align="right"><label>Perfil de per&iacute;odo: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                    	<td align="left" >
                                            <select name="perfilPeriodo" id="perfilPeriodo" val="obl" campo="Perfil de per&iacute;odo"  <?php echo $atributo?>>
                                            <option value="-1">Seleccione</option>
                                            <?php
                                                $consulta="SELECT id__340_tablaDinamica,txtNomPerfil FROM _340_tablaDinamica ORDER BY txtNomPerfil";
                                                $con->generarOpcionesSelect($consulta,$fila[24]);
                                            ?>
                                            </select>
                                        </td>
                                     </tr>
                                  <?php 
                                  if($preguntarTipoPrograma)
                                  {
                                      if ($idPrograma!="-1")
                                      {
                                      ?>
                                     <tr height="23">
                                        <td  align="right"><label>Tipo de <?php echo $tituloPrograma?>:<font color="#FF0000">&nbsp;</font></label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td  align="left" class="letraFicha"><b>
                                        <?php
                                            $consulta4="Select nombreTipoPrograma from 4036_tipoPrograma where idTipoPrograma=".$fila[3];
                                            $tipo=$con->obtenerValor($consulta4);
                                            echo $tipo;
                                        ?>
                                        </b>
                                        <input type="hidden" name="_idTipoProgramaint" value="<?php echo $fila[3] ?>" id="_idTipoProgramaint" />
                                        </td>
                                     </tr>
                                     <?php 
                                      }
                                      else
                                      {
                                      ?>
                                     <tr height="23">
                                        <td  align="right"><label>Tipo de <?php echo $tituloPrograma?>: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                        <td align="left">
                                            <select name="_idTipoProgramaint" id="_idTipoProgramaint" val="obl" campo="Tipo de <?php $tituloPrograma?>" onchange="pregunta()">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                                $consulta="Select idTipoPrograma,nombreTipoPrograma from 4036_tipoPrograma";
                                                $con->generarOpcionesSelect($consulta,$fila[3]);
                                            ?>
                                            </select>
                                        </td>
                                     </tr>
                                       <?php 
                                      }
                                  
                                      if(($idPrograma!="-1")&& ($fila[3]!=1))//Especial
                                      {
                                       ?>
                                     <tr id="valor2" height="23">
                                      	<td  align="right"><label>Vincular con <?php $tituloPrograma?>:<font color="#FF0000">&nbsp;</font></label>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td  align="left" class="letraFicha"><b> 
                                        
                                        <?php
                                            $consulta3="select nombrePrograma from 4004_programa where idPrograma=".$fila[12];
                                            $programa=$con->obtenerValor($consulta3);
                                            echo $programa;
                                        ?>
                                        </b>
                                        <input type="hidden" name="_idProgramaVinculadoint" value="<?php echo $fila[12] ?>" id="_idProgramaVinculadoint" />
                                        </td>
                                      </tr>
                                      <?php 
                                      }
                                      else
                                      {
                                          if($idPrograma=="-1")
                                          {
                                      ?>
                                          <tr id="valor" height="23" style="display:none">
                                            <td  align="right"><label>Vincular con <?php $tituloPrograma?>: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                            <td  align="left">
                                                <select name="_idProgramaVinculadoint" id="_idProgramaVinculadoint" val=""  campo="Vincular con <?php $tituloPrograma?>" onchange="pregunta()">
                                                <option value="-1">Seleccione</option>
                                                <?php
                                                    $consulta3="select idPrograma,nombrePrograma from 4004_programa where idTipoPrograma=1";
                                                    $con->generarOpcionesSelect($consulta3,$fila[12]);
                                                ?>
                                                </select>
                                            </td>
                                          </tr>
                                      <?php
                                          }
                                          else
                                          {
                                        ?>
                                        <input type="hidden" value="-1" name="_idProgramaVinculadoint" id="_idProgramaVinculadoint"/>
                                        <?php
                                          }
                                      }
                                  }
                                  else
                                  {
                                    ?>
                                    <input type="hidden" value="-1" name="_idProgramaVinculadoint" id="_idProgramaVinculadoint"/>
                                    <input type="hidden" name="_idTipoProgramaint" value="1" id="_idTipoProgramaint" />
                                    <?php							 
                                     
                                  }
                                  ?>
                                  <input type="hidden" name="_idEtiquetaNivelint" value="1" id="_idEtiquetaNivelint" />
                                  <input type="hidden" name="_modalidadvch" id="_modalidadvch" value="-1" />
                                  <input type="hidden" name="global" id="global" value="1" />
                                       <tr>
                                        <td  align="right"><label>Modelo Educativo: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td> 
                                        <td  align="left">
                                        <select name="_modeloEint" id="_modeloEint" val="obl" campo="Modelo Educativo" <?php echo $atributo?>>
                                        <option value="-1">Seleccione</option>
                                        <?php
                                        $consulta="SELECT idModeloEducativo,nombre FROM 4215_modalidadPrograma order by nombre";
                                        $con->generarOpcionesSelect($consulta,$fila[19]);
                                        ?>
                                        </select>
                                        </td>
                                      </tr>
                                      
                                      <?php
                                        $oFilaProgramaP="";
                                        if($preguntarTipoPrograma)
                                        {
                                            $tipoPrograma=$fila[3];
                                            if(($tipoPrograma==2)||($idPrograma=="-1")) //especial
                                                $oFilaProgramaP='none';
                                            else
                                                $oFilaProgramaP='';
                                        }
                                  ?>
                                      <tr height="23" id='filaProgramaPre' style="display:<?php echo $oFilaProgramaP?>">
                                      	<td  align="right" valign="top"><label><?php echo $tituloPrograma?> que le precede: </label>&nbsp;&nbsp;</td>
                                        <td  align="left">
                                           
                                                    <select name="_idProgramaAntint" id="_idProgramaAntint" onchange="programaPrecedeChange(this)" <?php echo $atributo?>>
                                                    <option value="-1">Seleccione</option>
                                                    <?php
                                                        $consulta="SELECT idPrograma,nombrePrograma FROM 4004_programa WHERE idPrograma<>".$idPrograma."  ORDER BY ciclo desc,nombrePrograma ";
                                                        $con->generarOpcionesSelect($consulta,$fila[11]);
                                                        
                                                        $ocultoProgramaIncl='style="display:"';
                                                        if($fila[11]=="-1")
                                                            $ocultoProgramaIncl='style="display:none"';
                                                    ?>
                                                    </select>
                                                   
                                        </td>
                                      </tr>  
                                      <tr height="23" id='filaProgramaPre2' style="display:<?php echo $oFilaProgramaP?>">
                                        <td  align="left" >
                                        
                                        <label id="lblIncluido"  <?php echo $ocultoProgramaIncl ?> >Inclu&iacute;do como elemento del <?php echo $tituloPrograma?>?:</label></td>
                                        <td width="">
                                        <select name="_programaAntContenidoint" id="_programaAntContenidoint" <?php echo $ocultoProgramaIncl ?> <?php echo $atributo?>>
                                        <?php
                                            $consulta="select valor,texto from 1004_siNo where idIdioma=".$_SESSION["leng"];
                                            $con->generarOpcionesSelect($consulta,$fila[18]);
                                        ?>
                                        </select>
                                        </td>
                                    </tr>
                                      
                                      <tr height="40">
                                        <td  align="right"><label>Situación: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                        <td  align="left" class="corpo8">
                                        <select name="_statusint" id="_statusint" val="obl" campo="Situaci&oacute;n" <?php echo $atributo?>>
                                        <option value="-1">Seleccione</option>
                                        <?php
                                            $consulta="Select idStatus,nombreStatus from 4005_status where idStatus<3";
                                            $con->generarOpcionesSelect($consulta,$fila[8]);
                                        ?>
                                        </select>
                                        </td>
                                      </tr>
									  <?php
                                        if(($idPrograma=="-1")||($fila[3]==1))
                                        {
                                            $oculto="";
                                            if($idPrograma=="-1")
                                                //$oculto='style="display:none"';
                                      ?>
                                      <tr id="filaDocumentos" <?php echo $oculto ?>>
                                      	<td height="23"  align="left" colspan="2"><label>Documentos solicitados alumnos de nuevo ingreso: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                      </tr>
                                      <tr>
                                      	<td></td>
                                        <td  align="left">
                                           <table >
                                               <tr>
                                                  <td>
                                                      <select size="10" style="width:350px" id="listDocumentos" >
                                                      <?php
                                                          $consulta="select d.idDocumento,d.tipo from 4016_documentos d,4020_programaVsDocumento pd where d.idDocumento=pd.idDocumento and pd.idPrograma=".$idPrograma;
                                                          $con->generarOpcionesSelect($consulta);
                                                      
                                                      ?>
                                                      </select>
                                                      <input type="hidden" name="listadoDocumentos" id="listadoDocumentos" value="" />
                                                  </td>
                                                  <td width="10">
                                                  </td>
                                                  <td valign="top">
                                                     
                                                      <table>
                                                        <tr>
                                                            <td><a href="javascript:agregarDoc()"><img src="../images/accept_green.png" alt='Agregar Rol' title='Agregar Documento'/></a></td>
                                                            <td class="letraFicha">&nbsp;&nbsp;<a href="javascript:agregarDoc()">Agregar Documento</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="javascript:removerDoc()"><img src="../images/cancel_round.png" alt='Remover Rol' title='Remover Documento'/></a></td>
                                                            <td class="letraFicha">&nbsp;&nbsp;<a href="javascript:removerDoc()">Remover Documento</a></td>
                                                        </tr>
                                                      </table>
                                                     
                                                  </td>
                                               </tr>
                                           </table>
                                           <br /><br />
                                        </td>
                                      </tr>
                                    <?php
                                    }
                                    //AQUI ESTOY MODIFICANDO LOS USUARIOS PERMITIDOS
                                    ?>
                                    <!--<tr id="trResponsables">
                                         <td width="200" height="50" valign="top" align="right"><label>Usuarios responsables: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td>
                                         <td width="600" align="left">
                                              <span id="tblUsuariosResp"></span>
                                         </td>
                                    </tr>  -->  
                                   
                                  
                                       <tr id="trGrid">
                                        <td  align="left" valign="top" colspan="2"><label>Sedes que pueden hacer uso de este <?php echo $tituloPrograma?>: <font color="#FF0000">*</font></label>&nbsp;&nbsp;</td> 
                                       </tr>
                                       <tr>
                                       	<td>
                                        </td>
                                        <td  align="left" colspan="1">
                                              <br />
                                              <span id="tblCedes"></span>
                                              <br />
                                         </td>      
                                </table>
                                </fieldset>
                                </td>
                              </tr>
                              <tr height="23">
                                <td colspan="2" align="center">
                                <br />
                                 <?php 	
                                        if($idPrograma=="-1")
                                        {
                                ?>
                                <input type="button" class="btnAceptar"  value="Guardar" onclick="validarFrm('frmEnvio')"/>
                                <?php $fechaCreacion=date('Y-m-d');?>
                                <input type="hidden" name="_fechaCreacion" value="<?php echo $fechaCreacion?>" />
                                <input type="hidden" name="_responsableint" value="<?php echo $_SESSION["idUsr"]?>" />
                                
                                <?php
                                        }
                                        else
                                        {
											
                                ?>			
                                <input type="button" class="btnAceptar" name="btnGuardar" value="Actualizar" onclick="validarFrm('frmEnvio')"/>
                                 <?php $fechaModif=date('Y-m-d');?>
                                <input type="hidden" name="_fechaModif" value="<?php echo $fechaModif?>"/>
                                <input type="hidden" name="_respModifint" value="<?php echo $_SESSION["idUsr"]?>" />
                                
                                <?php 
										
										}
                                ?>
                                
                                </td>
                              </tr>
                            </table>
                            <input type="hidden" name="tabla" value="4004_programa" />
                            <input type="hidden" name="id" value="<?php echo $idPrograma?>" />
                            <input type="hidden" name="campoId" value="idPrograma" />
                            <input type="hidden" name="pagRedireccion" value="../Administracion/tblPrograma.php"/>
                            <input type="hidden" name="listadoUsuarios" id="listadoUsuarios" value="" />
                            <input type="hidden" name="cadUnidades" id="cadUnidades" value="" />
                           </form> 
                            <br />
                            <input type="hidden" name="cBusquedaP" id="cBusquedaP" value="1" />
                            <input type="hidden" name="idUsuario" id="idUsuario" value="-1" />
                           
						</td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
