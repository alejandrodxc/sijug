<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->


<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="Scripts/materia.js.php"></script>
<?php 
$mostrarMenuIzq=false;
$guardarConfSession=true;
$soloContenido=true;
	$mostrarRegresar=true;
?>
<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
										echo $txMenuIncluye;
                                  
										?>
                                       
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					 
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                    <?php
					  	$idMateria="-1";
						if(isset($objParametros->idMateria))
							$idMateria=$objParametros->idMateria;
						
						$idCiclo="-1";
						if(isset($objParametros->idCiclo))
							$idCiclo=$objParametros->idCiclo;
						
						$idSeccion="0";
						if(isset($objParametros->idSeccion))
							$idSeccion=$objParametros->idSeccion;
							
						$idMapaCurricular="-1";
						if(isset($objParametros->idMapaCurricular))
							$idMapaCurricular=$objParametros->idMapaCurricular;		
						
						$conTipoBloques="SELECT obligarBloques,noBloques,horasLibres,manejaCreditos,modalidadC FROM 4029_mapaCurricular WHERE idMapaCurricular=".$idMapaCurricular;
						$filaBloque=$con->obtenerPrimeraFila($conTipoBloques);
						
						$conTipoGrupos="SELECT generarGrupos FROM 4029_mapaCurricular WHERE idMapaCurricular=".$idMapaCurricular;
						$fGrupos=$con->obtenerPrimeraFila($conTipoGrupos);
						$generaGrupos=$fGrupos[0];
						
						if($idMapaCurricular!="-1")
						{
							$etiquetaMateria="SELECT etiqueta,etiquetaPlural FROM 4140_etiquetasMapa WHERE idTipoElemento=1 AND idMapaCurricular=".$idMapaCurricular;
							$etiquetaM=$con->obtenerPrimeraFila($etiquetaMateria);
							$etiquetaFilasM=$con->obtenerFilas($etiquetaMateria);
							$filasM=$con->filasAfectadas;
							if($filasM==0)
							{
						  		$etiquetaM[1]="Materias";
						  		$etiquetaM[0]="Materia";
							}
							
							$conPrograma="SELECT idPrograma FROM 4029_mapaCurricular WHERE idMapaCurricular=".$idMapaCurricular;
							$idPrograma=$con->obtenerValor($conPrograma);
							if($idPrograma=="")
								$idPrograma="-1";
						}
						else
						{
							$etiquetaM[1]="Materias";
						  	$etiquetaM[0]="Materia";
						}
						
						$consulta="select * from 4013_materia where idMateria=".$idMateria;
						$fila=$con->obtenerPrimeraFila($consulta);
						
						if($fila[0]!="")
							$idPrograma=$fila[1];
						
						$conDatosCampo="SELECT *FROM 4214_clasificacionMateriaUnesco WHERE idMateria=".$idMateria;
						$datosC=$con->obtenerPrimeraFila($conDatosCampo);
						
					  ?>
                   <tr>
                      <td align="center">
                          <!--<fieldset class="frameHijo" ><legend><b><?php //echo $etiquetaM[0] ?></b></legend>-->
                                    <!--</fieldset>-->
                          <form id="frmEnvio" method="post" action="../Administracion/generarMateria.php">
                          <table>
                             <tr>
                             	<td class="tituloPaginas" align="center">
                                	Materia
                                    <br />
                                    <br />
                                </td>
                             </tr>
                             <tr>
                                <td width="800" align="center">
                                   <div id="my-tabs" name"my-tabs"></div>
                                </td>
                             </tr>
                             <tr>
                             	<td>
                                	<table id="tabla1" width="100%" align="center" class="x-hide-display">
                                    <tr>
                                    	<td colspan="2">
                                        	<br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr height="23">
                                        <td width="250" valign="top" align="right">
                                            <label>Clave: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="100" align="left"> 
                                        <input type="text" name="_cve_materiavch" id="_cve_materiavch" maxlength="150" size="11" val="obl" campo="Clave de Materia" value="<?php echo $fila[2]?>" />	
                                        <?php 
                                        if($idMateria==-1)
                                            $idSeccion=$idSeccion;
                                        else
                                            $idSeccion=$fila[1];
                                        ?>
                                        <input type="hidden" name="_idProgramaint" value="<?php echo $idSeccion?>" />
                                    </tr>
                                    <?php 
                                    if($filaBloque[0]==1)
                                    {
                                    ?>
                                        <input type="hidden" name="_bloquesint" id="_bloquesint" value="<?php echo $filaBloque[1] ?>" />
                                        <input type="hidden" name="_esquemaEvaluacionint" id="_esquemaEvaluacionint" value="0" />
                                        <input type="hidden" name="validarEsquema" id="validarEsquema" value="0" />
                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <input type="hidden" name="validarEsquema" id="validarEsquema" value="1" />
                                    <tr height="23" >
                                        <td align="right" width="230">
                                            <label>Esquema de Calificaci&oacute;n: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td align="left" >
                                            <select name="_esquemaEvaluacionint" id="_esquemaEvaluacionint" val="obl" campo="Esquema de Calificaci&oacute;n:" onchange="mBloques()">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                                $arrOpciones[0][0]="0";
                                                $arrOpciones[0][1]="Parciales";
                                                $arrOpciones[1][0]="1";
                                                $arrOpciones[1][1]="Temario";
                                                $con->generarOpcionesSelectArreglo($arrOpciones,$fila[22]);
                                            ?>
                                            </select> 
                                        </td>                         
                                    </tr>
                                    <tr height="23" id="bloques">
                                        <td align="right">
                                            <label>Parciales: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td align="left" >
                                            <select name="_bloquesint" id="_bloquesint" val="obl" campo="Bloques" >
                                            <option value="-1">Seleccione</option>
                                             <?php 
                                             if(($fila[13]==1000) && ($fila[13]!=""))
                                             {
                                             ?>	
                                                <option value="1000" selected="selected">Definido por el Profesor</option>
                                             <?php 
                                             	$con->generarNumeracionSelect(1,10,1000,1);
											 }
                                             else
                                             {
                                             ?>
                                                <option value="1000">Definido por el Profesor</option>
                                             <?php
                                             }
                                                $con->generarNumeracionSelect(1,10,$fila[13],1);
                                            ?>
                                            </select> 
                                        </td>                         
                                    </tr>
                                    <?php 
                                    }
                                    ?>
                                    <!--<tr height="23" align="right">
                                        <td>
                                            <label>Mostrar Reporte:<font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                            <select id="_mostrarSesionesint" name="_mostrarSesionesint" val="obl" campo="Mostrar Reporte"/>
                                                <option value="-1">Seleccione</option>
                                                <?php
                                                //$arregloM[0][0]="0";
                                                //$arregloM[0][1]="Hasta Planeacion";
                                                //$arregloM[1][0]="1";
                                                //$arregloM[1][1]="Hasta Sesiones";
                                                //$con->generarOpcionesSelectArreglo($arregloM,$fila[29]);
                                                ?>
                                            </select>    
                                        </td>
                                    </tr>-->
                                    <input type="hidden" name="_mostrarSesionesint" id="_mostrarSesionesint" value="0"/>
                                    <tr height="23">
                                        <td  valign="top" align="right">
                                            <label>Nombre: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                            <input type="text" name="_titulovch" id="_titulovch" maxlength="150" size="53" val="obl" campo="Nombre" value="<?php echo $fila[3]?>" />	
                                        </td>    
                                    </tr>
                                    <tr height="23">
                                        <td  valign="top" align="right">
                                            <label>Abreviatura: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                            <input type="text" name="_abreviaturavch" id="_abreviaturavch" maxlength="150" size="53" val="obl" campo="Abreviatura" value="<?php echo $fila[15]?>" />
                                        </td>    	
                                    </tr>
                                    <tr>
                                        <td valign="top" align="right">
                                            <label>Prof. Define Temario: <font color="#FF0000">*</font></label>&nbsp;&nbsp;	
                                        </td>
                                        <td align="left">
                                            <select name="_tipoTemarioint" id="_tipoTemarioint" val="obl" campo="Prof. Define Temario">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                                $consulta="SELECT valor,texto FROM 1004_siNo WHERE idIdioma=".$_SESSION["leng"] ;
                                                $con->generarOpcionesSelect($consulta,$fila[21]);
                                            ?>
                                            </select> 
                                        </td>
                                    </tr>
                                    <!--<tr height="23" id="tipoHorario">
                                        <td width="150" align="right">
                                            <label>Tipo de Horario : <font color="#FF0000">*</font></label></td>
                                        <td>
                                            <select name="idTipoHorario" id="idTipoHorario" val="obl" campo="Tipo de Horario" >
                                            <option value="-1">Seleccione</option>
                                            <?php
                                          //$consultaH="Select idTipoHorario,nombre from 4154_tipoHorario where idTipoHorario<>3";
        //                                  $con->generarOpcionesSelect($consultaH,$fila[4]);
                                            ?>
                                            </select>
                                        </td>
                                    </tr>-->
                                    <tr height="23">
                                        <td valign="top" align="right">
                                            <label >Descripción: &nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left"><textarea rows="4" cols="50" id="_descripcionvch" name="_descripcionvch"  campo="Descripcion" ><?php echo uE($fila[18])?></textarea>							
                                        </td>
                                    </tr>
                                    <tr height="23">
                                        <td  valign="top" align="right">
                                            <label>Total de horas: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                            <select name="_horasTotalint" id="_horasTotalint" val="obl" campo="Total de horas" onchange="horas()" >
                                             <option value="-1">Seleccione</option>
                                             <?php 
                                             if(($fila[17]==0) && ($fila[17]!=""))
                                             {
                                             ?>	
                                                <option value="0" selected="selected">Libre</option>
                                             <?php 
                                             }
                                             else
                                             {
                                             ?>
                                                <option value="0">Libre</option>
                                             <?php
                                             }
                                                $con->generarNumeracionSelect(1,400,$fila[17],1);
                                             ?>
                                             </select>
                                        </td>      	
                                    </tr>
                                    <tr height="23">
                                        <td  valign="top" align="right">
                                            <label>Horas/Semana: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                             <select name="_horasSemanaint" id="_horasSemanaint" val="obl" campo="Horas/Semana" onchange="horas()">
                                             <option value="-1">Seleccione</option>
                                             <?php 
                                             if(($fila[16]==0) && ($fila[16]!=""))
                                             {
                                             ?>	
                                                <option value="0" selected="selected">Definido por el Profesor</option>
                                             <?php 
                                             }
                                             else
                                             {
                                             ?>
                                                <option value="0">Definido por el Profesor</option>
                                             <?php
                                             }
                                                $con->generarNumeracionSelect(1,168,$fila[16],1);
                                             ?>
                                             </select>
                                        </td>      	
                                    </tr>
                                    <?php 
                                    if($filaBloque[3]==1)
                                    {
                                    ?>
                                    <tr height="23" id="creditos">
                                        <td valign="top" align="right">
                                            <label>No. Creditos: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                        </td>
                                        <td align="left">
                                        <?php
                                        if($filaBloque[4]==2)
                                        {
                                        ?>
                                            <input type="text" name="_nCreditosint" id="_nCreditosint" maxlength="10" size="10" campo="No. Creditos" value="<?php echo $fila[26]?>" onkeypress="return soloNumero(event,true,false,this)"/>
                                            <!--<select name="_nCreditosint" id="_nCreditosint" campo="No. Creditos" >
                                                <option value="-1">Seleccione</option>
                                                <?php 
                                                //$con->generarNumeracionSelect(1,100,$fila[26])
                                                ?>
                                            </select>    -->
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <label>&nbsp;&nbsp;<?php echo $fila[26]?></label>
                                        <?php
                                        }
                                        ?>
                                        </td>
                                    </tr>
                                    <?php 
                                    }
                                    ?>
                                    <?php 
                                    if($filaBloque[2]==1)
                                    {		
                                    ?>
                                    <tr height="23" id="teoricaslibres">
                                        <td  valign="top" align="right">
                                            <label>Horas Te&oacute;ricas:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td  valign="top" align="right">
                                                        <label>Con Profesor:<font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <select name="_horasTeoricasint" id="_horasTeoricasint" val="obl" campo="Horas Te&oacute;ricas con profesor" onchange="suma(this)">
                                                        <option value="-1">Seleccione</option>
                                                        <?php
                                                        $con->generarNumeracionSelect(0,400,$fila[19],1);
                                                        ?>
                                                        </select>
                                                    </td>
                                                    <td width="20">
                                                    </td>
                                                    <td  valign="top" align="right">
                                                        <label>Libres: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <select name="_horasTeoricasLibresint" id="_horasTeoricasLibresint" val="obl" campo="Horas Te&oacute;ricas libres" onchange="suma(this)">
                                                        <option value="-1">Seleccione</option>
                                                        <?php
                                                        $con->generarNumeracionSelect(0,400,$fila[30],1);
                                                        ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>         	
                                        </td>    
                                    </tr>
                                     <tr height="23" id="practicasLibres">
                                        <td valign="top" align="right">
                                            <label>Horas Pr&aacute;cticas:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td valign="top" align="right">
                                                        <label>Con profesor:<font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <select name="_horasPracticasint" id="_horasPracticasint" val="obl" campo="Horas Pr&aacute;cticas con profesor" onchange="suma(this)">
                                                        <option value="-1">Seleccione</option>
                                                        <?php
                                                        $con->generarNumeracionSelect(0,400,$fila[20],1);
                                                        ?>
                                                        </select>
                                                    </td>
                                                    <td width="20">
                                                    </td>
                                                    <td valign="top" align="right">
                                                        <label>Libres: <font color="#FF0000">*</font></label>&nbsp;&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <select name="_horasPracticasLibresint" id="_horasPracticasLibresint" val="obl" campo="Horas Pr&aacute;cticas libres" onchange="suma(this)">
                                                        <option value="-1">Seleccione</option>
                                                        <?php
                                                        $con->generarNumeracionSelect(0,400,$fila[31],1);
                                                        ?>
                                                        </select>
                                                    </td> 	
                                                </tr>
                                            </table>
                                        </td>    
                                    </tr>
                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <tr height="23" id="teoricasNormal">
                                        <td  valign="top" align="right">
                                            <label>Horas Te&oacute;ricas:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        </td>
                                        <td align="left">
                                            <select name="_horasTeoricasint" id="_horasTeoricasint" val="obl" campo="Horas Te&oacute;ricas con profesor" onchange="suma(this)">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                            $con->generarNumeracionSelect(0,400,$fila[19],1);
                                            ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr height="23" id="practicasNormal">
                                        <td valign="top" align="right">
                                            <label>Horas Pr&aacute;cticas:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        </td>
                                        <td align="left">
                                            <select name="_horasPracticasint" id="_horasPracticasint" val="obl" campo="Horas Pr&aacute;cticas con profesor" onchange="suma(this)">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                            $con->generarNumeracionSelect(0,400,$fila[20],1);
                                            ?>
                                            </select>
                                        </td>
                                    </tr>       
                                    <?php 
                                    }
                                    ?>
                                    <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Compartir con Programas: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left">
                                             <?php
                                             $numeroAlumnos=0;
                                             if($idMateria!="-1")
                                             {
                                                if($fila[23]==1)
                                                {
                                                    $mensajeAlt="";
                                                    $conGrupos="SELECT nombreGrupo,idGrupo FROM 4048_grupos WHERE idMateria=".$idMateria;
                                                    $resGrupos=$con->obtenerFilas($conGrupos);
                                                    
                                                    $consulta="SELECT idMatComVSPrograma,m.idPrograma,p.nombrePrograma FROM 4239_materiaCompVSPrograma m,4004_programa p WHERE m.idMateria=".$idMateria." AND m.idPrograma=p.idPrograma order by p.nombrePrograma";
													$resProg=$con->obtenerFilas($consulta);
													$noFilas=$con->filasAfectadas;
													while($fProg=mysql_fetch_row($resProg))
													{
														  while($filaGrupos=mysql_fetch_row($resGrupos))
														  {
															  $conAlumnos="SELECT idAlumnosElementoMapa FROM 4120_alumnosVSElementosMapa WHERE idMateria=".$idMateria." AND idGrupo=".$filaGrupos[1]." and idPrograma=".$fProg[0];
															  $resAlumnos=$con->obtenerFilas($conAlumnos);
															  $numeroAlumnosC=$con->filasAfectadas;
															  
															  $datosGrupo="Grupo:&nbsp;".$filaGrupos[0]."&nbsp;No.Alumnos:&nbsp;".$numeroAlumnosC;
														  
															  if($mensajeAlt=="")
																  $mensajeAlt=$datosGrupo;
															  else
																  $mensajeAlt.=",".$datosGrupo;
															  
															  $numeroAlumnos=$numeroAlumnos+$numeroAlumnosC;
														  }
													}
                                                    if($numeroAlumnos>0)
                                                    {
                                                    ?>
                                                        <label>Si&nbsp;&nbsp;<img src="../images/exclamation.png" alt="No puede cambiar la configuracion de esta materia porque tiene grupos con alumnos,&nbsp; <br/><?php echo $mensajeAlt ?>" title="No puede cambiar la configuracion de esta materia porque tiene grupos con alumnos,&nbsp;<?php echo $mensajeAlt ?>" /></label>
                                                        <input type="hidden" name="_compartidaint" id="_compartidaint" value="<?php echo $fila[23] ?>" />
                                                    <?php
                                                    }
                                                    else
                                                    {
                                                    ?>
                                                         <select name="_compartidaint" id="_compartidaint" val="obl" campo="Compartir con Programas" onchange="validarCompartida()">
                                                          <option value="-1">Seleccione</option>
                                                           <?php
                                                              $consultaC="SELECT valor,texto FROM 1004_siNo WHERE idIdioma=".$_SESSION["leng"];
                                                              $con->generarOpcionesSelect($consultaC,$fila[23]);
                                                           ?>
                                                         </select> 
                                                    <?php
                                                    }
                                                }
                                                else
                                                {
                                                ?>
                                                      <select name="_compartidaint" id="_compartidaint" val="obl" campo="Compartir con Programas" onchange="validarCompartida()">
                                                          <option value="-1">Seleccione</option>
                                                     <?php
                                                        $consultaC="SELECT valor,texto FROM 1004_siNo WHERE idIdioma=".$_SESSION["leng"];
                                                        $con->generarOpcionesSelect($consultaC,$fila[23]);
                                                     ?>
                                                     </select> 	
                                                <?php
                                                }
                                             }
                                             else
                                             {
                                             ?>
                                             <select name="_compartidaint" id="_compartidaint" val="obl" campo="Compartir con Programas" onchange="validarCompartida()">
                                             <option value="-1">Seleccione</option>
                                             <?php
                                                $consultaC="SELECT valor,texto FROM 1004_siNo WHERE idIdioma=".$_SESSION["leng"];
                                                $con->generarOpcionesSelect($consultaC,$fila[23]);
                                             ?>
                                             </select> 							
                                             <?php
                                             }
                                             ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if($generaGrupos==2)
									{
								    ?>
                                    <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Cupo Minimo de grupo: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left">
                                            <input type="text" name="minGrupo" id="minGrupo" maxlength="10" size="10" campo="Cupo Minimo de grupo" value="<?php echo $fila[32]?>" onkeypress="return soloNumero(event,false,false,this)"/>						
                                        </td>
                                    </tr>
                                    <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Cupo Maximo de grupo: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left">
                                            <input type="text" name="maxGrupo" id="maxGrupo" maxlength="10" size="10" campo="Cupo Maximo de grupo" value="<?php echo $fila[33]?>" onkeypress="return soloNumero(event,false,false,this)"/>						
                                        </td>
                                    </tr>
                                     <tr height="23">
                                        <td valign="top" align="right">
                                            <label>No. de grupos: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left">
                                            <input type="text" name="noGrupos" id="noGrupos" maxlength="10" size="10" campo="No. de grupos" value="<?php echo $fila[34]?>" onkeypress="return soloNumero(event,false,false,this)"/>						
                                        </td>
                                    </tr>
                                    <?php
									}
									?>
                                    <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Objetivo:&nbsp;&nbsp;</label>
                                        </td>
                                        <td width="487" align="left">
                                            <textarea rows="8" cols="85" id="_objetivovch" name="_objetivovch"  ><?php echo $fila[4]?></textarea>							
                                        </td>
                                    </tr>
                                   <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Objetivos Específicos:</label>&nbsp;&nbsp;
                                        </td>
                                        <td width="487" align="left">
                                            <textarea rows="8" cols="85" id="_objetivos_especificosvch" name="_objetivos_especificosvch"  ><?php echo $fila[5]?></textarea>							
                                        </td>
                                   </tr>
                                   <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Propósito: </label>&nbsp;&nbsp;
                                        </td>
                                        <td width="487" align="left">
                                            <textarea rows="8" cols="85" id="_propositovch" name="_propositovch"  ><?php echo $fila[6]?></textarea>							
                                        </td>
                                   </tr>
                                   <tr height="23">
                                        <td valign="top" align="right">
                                            <label>Palabras Clave: </label>&nbsp;&nbsp;
                                        </td>
                                        <td width="487" align="left">
                                            <textarea rows="8" cols="85" id="_keywordsvch" name="_keywordsvch"  ><?php echo $fila[10]?></textarea>							
                                        </td>
                                   </tr>
                                   <tr height="23">
                                        <td align="right">
                                            <label>Situación: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                        </td>
                                        <td align="left">
                                            <select name="_statusint" id="_idStatusint" val="obl" campo="Status">
                                            <option value="-1">Seleccione</option>
                                            <?php
                                               $consulta="Select idStatus,nombreStatus from 4005_status";
                                               $con->generarOpcionesSelect($consulta,$fila[9]);
                                            ?>
                                            </select>
                                        </td>
                                   </tr>
                                   <tr height="23">
                                      <td colspan="2" align="center">
                                        <br />
                                        <?php 	
                                        if($idMateria=="-1")
                                        {
                                        ?>
                                        <input type="button" class="tituloTabla" name="btnGuardar" value="Guardar" onclick="validarFrm('frmEnvio')"/>
                                        <?php $fechaCreacion=date('Y-m-d');?>
                                        <input type="hidden" name="_fechaCreacion" value="<?php echo $fechaCreacion?>" />
                                        <input type="hidden" name="_responsableint" value="<?php echo $_SESSION["idUsr"]?>" />
                                        <input type="hidden" name="id" id="id" value="<?php echo $idMateria ?>" />
                                        <input type="hidden" name="_cicloint" id="_cicloint" value="<?php echo $idCiclo ?>" />
                                        <input type="hidden" name="idMapaCurricular" id="idMapaCurricular" value="<?php echo $idMapaCurricular ?>" />
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                        <input type="button" class="tituloTabla" name="btnGuardar" value="Actualizar" onclick="validarFrm('frmEnvio')"/>
                                        <?php $fechaModif=date('Y-m-d');?>
                                        <input type="hidden" name="_fechaModif" value="<?php echo $fechaModif?>"/>
                                        <input type="hidden" name="_respModifint" value="<?php echo $_SESSION["idUsr"]?>" />
                                        <input type="hidden" name="id" id="id" value="<?php echo $idMateria ?>" />
                                        <input type="hidden" name="_cicloint" id="_cicloint" value="<?php echo $idCiclo ?>" />
                                        <input type="hidden" name="idMapaCurricular" id="idMapaCurricular" value="<?php echo $idMapaCurricular ?>" />
                                        <?php
                                        }
                                        ?>
                                      </td>
                                    </tr>
                                    </table>
                                </td>
                             </tr>
                             <tr>
                             	<td align="center">
                                	<table>
                                    	<tr>
                                        	<td width="400" align="center">
                                              <table id="tabla2" class="x-hide-display" align="left" width="60%">
                                                <tr>
                                                  <td colspan="2">
                                                      <br />
                                                      <br />
                                                  </td>
                                                </tr>
                                                <tr height="23">
                                                    <td width="200" align="right">
                                                      <label>Campo:&nbsp;&nbsp;&nbsp;</label> 
                                                    </td>
                                                    <td width="200" align="left">
                                                        <select name="_campoint" id="_campoint" onchange="obtenerDisciplina()">
                                                            <option value="-1">Seleccione</option>
                                                            <?php
                                                                $consulta="SELECT codigo,nombre FROM 4211_camposUnesco";
                                                                $con->generarOpcionesSelect($consulta,-1);
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr height="23">
                                                    <td width="200" align="right">
                                                      <label>Disciplina:&nbsp;&nbsp;&nbsp;</label>
                                                    </td>
                                                    <td width="200" align="left">
                                                        <select name="_disciplinaint" id="_disciplinaint" onchange="obtenerSubD()">
                                                            <option value="-1">Seleccione</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr height="23">
                                                    <td width="200" align="right">
                                                        <label>SubDisciplina:&nbsp;&nbsp;&nbsp;</label>
                                                    </td>
                                                    <td width="200" align="left">
                                                        <select name="_subDisciplinaint" id="_subDisciplinaint">
                                                            <option value="-1">Seleccione</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td colspan="2">
                                                    	<br />
                                                        <br />
                                                        <span class="letraRojaSubrayada8">Para guardar los cambios realizados, debe guardar en la pesta&ntilde;a de datos generales  </span>
                                                    </td>
                                                </tr>
                                              </table>
                                            </td>
                                        </tr>
                                    </table>          
                                </td>
                             </tr>
                              <tr>
                             	<td align="center">
                                	<table>
                                    	<tr>
                                        	<?php
											?>
                                            <td width="400" align="center">
                                              <table id="tabla3" class="x-hide-display" align="center" width="60%">
                                                <tr>
                                                	<td><br /><br /><br /></td>
                                                </tr>
                                                <tr>
                                                  <td align="center"> 
                                                  	 <span id="divProgramas" style="width:650"></span>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                        </tr>
                                    </table>          
                                </td>
                             </tr>
                          <input type="hidden" name="confReferencia" value="<?php echo $nConfRegresar ?>" />
                          <input type="hidden" name="cadenaCompartida" id="cadenaCompartida" value="" />
                          </form>
                          <input type="hidden" name="considerarLibres" id="considerarLibres" value="<?php echo $filaBloque[2] ?>" />
                          <input type="hidden" name="manejaCreditos" id="manejaCreditos" value="<?php echo $filaBloque[3] ?>" />
                          <input type="hidden" name="tipoCreditos" id="tipoCreditos" value="<?php echo $filaBloque[4] ?>" />
                          
                          <input type="hidden" name="campoBase" id="campoBase" value="<?php echo $datosC[2] ?>" />
                          <input type="hidden" name="diciplinaBase" id="diciplinaBase" value="<?php echo $datosC[3] ?>" />
                          <input type="hidden" name="subDbase" id="subDbase" value="<?php echo $datosC[4] ?>" />
                          <input type="hidden" name="esCompartida" id="esCompartida" value="<?php echo $fila[23] ?>" />
                          <input type="hidden" name="idPrograma" id="idPrograma" value="<?php echo $idPrograma ?>" />
                          <input type="hidden" name="generaGrupos" id="generaGrupos" value="<?php echo $generaGrupos ?>" />
                          </table>
                      </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
