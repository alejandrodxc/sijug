<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
?>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<script type="text/javascript" src="Scripts/funcionesTablasGrid.js.php"></script>
<script type="text/javascript" src="Scripts/fichaMateria.js.php"></script>
<script type="text/javascript" src="Scripts/funcionesTablasGrid.js.php"></script>
<script type="text/javascript" src="../Scripts/thickbox/jquery.js"></script>
<script type="text/javascript" src="../Scripts/thickbox/thickbox.js"></script>
<link rel="stylesheet" href="../Scripts/thickbox/thickbox.css" type="text/css" />
<?php 
$guardarConfSession=true;
$pagRegresar="javascript:regresar('frmEnvioC')";
?>
<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
										echo $txMenuIncluye;
                                  
										?>
                                       
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                     <td align="center">
                        	<?php
					$idPrograma="-1";		
					if(isset($objParametros->idPrograma))
						$idPrograma=$objParametros->idPrograma;		 
					
					$idCiclo="-1";
					if(isset($objParametros->idCiclo))
						$idCiclo=$objParametros->idCiclo;
					
					$idMateria="-1";
					if(isset($objParametros->idMateria))
						$idMateria=$objParametros->idMateria;
					
					$idMapaCurricular="-1";
					if(isset($objParametros->idMapaCurricular))
						$idMapaCurricular=$objParametros->idMapaCurricular;	
					
					if($idMapaCurricular!="-1")
					{
					$etiquetaMateria="SELECT etiqueta,etiquetaPlural FROM 4140_etiquetasMapa WHERE idTipoElemento=1 AND idMapaCurricular=".$idMapaCurricular;
				    $etiquetaM=$con->obtenerPrimeraFila($etiquetaMateria);
					
					$conReporte="select defineReporte,horasLibres from 4029_mapaCurricular where idMapaCurricular=".$idMapaCurricular;
					$reporte=$con->obtenerPrimeraFila($conReporte);
					}
					
					$consulta="select * from 4013_materia where idMateria=".$idMateria;
					$fila=$con->obtenerPrimeraFila($consulta);
					$validacion=$fila[21];
					$clase="filaRosa10";
					$clase1="filaBlanca10";
					
					$panel="0";
					if(isset($objParametros->panel))
						$panel=$objParametros->panel;
					else
						$panel=0;
					
				    $filaCiclo="0";
					
					$conDatosCampo="SELECT *FROM 4214_clasificacionMateriaUnesco WHERE idMateria=".$idMateria;
					$datosC=$con->obtenerPrimeraFila($conDatosCampo);
					if($datosC)
					{
						$conCampo="SELECT nombre FROM 4211_camposUnesco WHERE codigo=".$datosC[2];
						$nombreCampo=$con->obtenerValor($conCampo);
						
						$conDisciplina="SELECT nombre FROM 4212_disciplinasUnesco WHERE codigoDisciplina=".$datosC[3];
						$nombreDisc=$con->obtenerValor($conDisciplina);
						
						$conSubD="SELECT nombre FROM 4213_subDisciplinasUnesco WHERE codigoSubDisciplina=".$datosC[4];
						$nombreSubD=$con->obtenerValor($conSubD);
					
					}
					
					?>
                  
					<table width="750" border="0" cellspacing="0" cellpadding="0" >
                      <tr>
                        <td>
						<fieldset class="frameHijo" ><legend><b><?php echo $tituloDefMateriaSingular ?></b></legend>
                          
                                  
                          	<form id="frmEnvio" action="materia.php" method="post">
                                <input type="hidden" name="idMateria" id="idMateria" value="" />
                                <input type="hidden" name="idCiclo" id="idCiclo" value="<?php echo $idCiclo ?>" />
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfRegresar ?>" />                        
						    </form>
                            <table  border="0">
                             <?php 
							  if ($filaCiclo!=1 )
							  {
							 ?>
								<tr>
								<td colspan="" align="center">
                                    <table class="tablaMenu">
                                    <tr>
                                        <td style="padding:4px 4px 4px 4px">
                                            <a href="javascript:modificarMateria()"><img src='../images/pencil.png' border=0  title='Modificar' alt='Modificar' >&nbsp;<span class="letraExt">Modificar</span> <br/></a>
                                        </td>
                                        <td width="20">
                                        </td>
                                        <?php 
										if($reporte[0]==1000)
										{
										?>
                                        <td style="padding:4px 4px 1px 4px">
                                        	<a href="javascript:reportePlaneacion()">
                                            <span class="letraExt"><img src='../images/lock.png' border=0  title='Reporte de Planeaci&oacute;n' alt='Reporte de Planeaci&oacute;n' >&nbsp;Reporte de Planeaci&oacute;n</span>
                                            </a>
                                        </td>
                                        <?php
										}
										?>
                                         <td width="20">
                                        </td>
                                        <td style="padding:4px 4px 1px 4px">
                                        	<a href="javascript:criteriosEval()">
	                                        	<span class="letraExt"><img src='../images/folder_table.png' border=0  title='Configuración de criterios de evaluación' alt='Configuración de criterios de evaluación' >&nbsp;Configuración de criterios de evaluación</span>
                                            </a>
                                        </td>
                                    </tr>
                                    </table>
                                    <br />
                                    <br />
							   
								</td>
								</tr>
								<?php 
							   }
	
							   ?> 
							   <tr>
								 <td>
								   <table width="100%">
									 <tr height="23"> <!--style="border-color: #000000; border-width:1px; border-style: solid">-->
									   <td  >
											 <table width="100%">
												<tr height="23">  
													<td colspan="4" >
														<table width="100%">
														<tr height="25">
															<?php 
															if($fila[22]==0)
															{
															?>
                                                            <td width="125"  class="letraFicha" align="left">
															 Esquema de Evaluaci&oacute;n:
                                                             </td>
															 <td width="10"></td>
															 <td align="left" width="160" class="copyrigth">
															  <?php
															  if($fila[22]==0)
															  {
															  echo "Parciales";
															  }
															  else
															  {
															  echo "Temario";
															  }
															  ?>
															 </td>
                                                             <?php 
															 }
															 else
															 {
															  ?>
                                                             <td width="125"  class="letraFicha" align="left">
                                                             </td>
															 <td width="10"></td>
															 <td align="left" width="120" class="copyrigth">
                                                             </td>	 
															 <?php
															 }
															 ?>
															 <td width="80"></td>
															 <td width="140"  class="letraFicha" align="left" >
															  Clave: 
															 </td>
															 <td width="10"></td>
															 <td align="left"  class="copyrigth" width="120">
															<?php echo $fila[2]?>	
															 </td>
															 <td>
															 </td>
														</tr>
														<tr height="25">
													   
															<?php 
															if($fila[22]==1)
															{
															?>
                                                            <td width="125"  class="letraFicha" align="left">
															 Esquema de Evaluaci&oacute;n:
                                                             </td>
															 <td width="10"></td>
															 <td align="left" width="120" class="copyrigth">
															  <?php
															  if($fila[22]==0)
															  {
															  echo "Bloques";
															  }
															  else
															  {
															  echo "Temario";
															  }
															  ?>
															 </td>
                                                            <?php
															}
															else
															{
															?>
                                                            <td width="70"  class="letraFicha"  align="left">
																Núm. Parciales:
															 </td>
															 <td width="10"></td>
															 <td align="left" class="copyrigth">
															  <?php 
															 if($fila[13]==1000)
															 {
															 	echo "Def. por el Profesor";
															 }
															 else
															 {
															 	echo $fila[13]; 
															 }
															 ?>
															  
															 </td>
															 <td width="50"></td>
															 <td width="90"  class="letraFicha" align="left" >
															  Situaci&oacute;n:
															 </td>
															 <td ></td>
															 <td align="left" class="copyrigth" >
															 <?php
															  $consulta3="Select e.nombreStatus from 4005_status e, 4013_materia m
																  where m.idMateria=".$idMateria." and m.status=e.idStatus";
															  $res2=$con->obtenerFilas($consulta3);
															  $fila2=mysql_fetch_row($res2);
															  echo $fila2[0];
															  ?>
															 </td>
															 <td>
															 </td>
                                                             <?php 
															 }
															 ?>
														</tr>
														 <tr height="25">
															<td width="70"  class="letraFicha"  align="left">
																Total de Horas:
															 </td>
															 <td width="10"></td>
															 <td align="left" class="copyrigth">
															  <?php
															  if($fila[17]==0)
															  {
															  	echo "Libre";
															  }
															  else
															  {
															  	echo $fila[17];
															  }
															  ?>
															  
															 </td>
															 <td width="50"></td>
															 <td width="90"  class="letraFicha" align="left" >
															  Horas/Semana:
															 </td>
															 <td ></td>
															 <td align="left" class="copyrigth" >
															 <?php 
															 if($fila[16]==0)
															 {
															 	echo "Def. por el Profesor";
															 }
															 else
															 {
															 	echo $fila[16]; 
															 }
															 ?>
															 </td>
															 <td>
															 </td>
														</tr>
														<?php 
														 if($fila[17]!="0")
														 {
														 ?>
                                                         <tr height="25">
                                                         <td width="70"  class="letraFicha"  align="left" valign="top">
																Horas Te&oacute;ricas:
															 </td>
                                                             <td width="10"></td>
															 <?php 
															 if($reporte[1]==1)
															 {
															 ?>
                                                             <td>
                                                             	<table>
                                                                	<tr>
                                                                    	<td width="80" class="letraFicha">
                                                                        	Con Prof.:
                                                                        </td>
                                                                        <td width="80" class="copyrigth">
                                                                        	 <?php echo $fila[19];?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    	<td class="letraFicha">
                                                                        	Libres:
                                                                        </td>
                                                                        <td class="copyrigth">
                                                                        	<?php echo $fila[30];?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                             </td>
                                                             <?php
															 }
															 else
															 {
															 ?>
															 <td align="left" class="copyrigth">
															  <?php echo $fila[19];?>
															 </td>
                                                             <?php
															 }
															 ?>
                                                             <td width="50"></td>
															 <td width="90"  class="letraFicha" align="left" valign="top">
															  Horas Pr&aacute;cticas:
															 </td>
															 <td ></td>
                                                             <?php 
															 if($reporte[1]==1)
															 {
															 ?>
                                                             <td>
                                                             	<table>
                                                                	<tr>
                                                                    	<td width="80" class="letraFicha">
                                                                        	Con Prof.:
                                                                        </td>
                                                                        <td width="80" class="copyrigth">
                                                                        	 <?php echo $fila[20];?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    	<td class="letraFicha">
                                                                        	Libres:
                                                                        </td>
                                                                        <td class="copyrigth">
                                                                        	<?php echo $fila[31];?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                             </td>
                                                             <?php 
															 }
															 else
															 {
															 ?>
															 <td align="left" class="copyrigth" >
															 <?php echo $fila[20]; ?>
															 </td>
                                                             <?php
															 }
															 ?>
															 <td>
															 </td>
														 </tr>
														 <?php 
														 }
														 ?>
														<tr height="25"> 
															 <td  align="left" class="letraFicha" >
															  Título:
															 </td>
															 <td width="10"></td>
															 <td colspan="7" class="copyrigth" align="left">
															  <?php echo $fila[3];?>  
															 </td>
													   </tr>
                                                       <tr height="25">
													   
															<td width="70"  class="letraFicha"  align="left">
																Abreviatura:
															 </td>
															 <td width="10"></td>
															 <td align="left" class="copyrigth">
															  <?php echo $fila[15];?>
															  
															 </td>
															 <td width="50"></td>
															 <td width="120"  class="letraFicha" align="left" >
															  Prof. Define Temario:
															 </td>
															 <td ></td>
															 <td align="left" class="copyrigth" >
															 <?php
															 $conTipoTemario="SELECT texto FROM 1004_siNo WHERE valor=".$fila[21]." AND idIdioma=".$_SESSION["leng"] ;
															 $textoTipo=$con->obtenerValor($conTipoTemario);
															 //echo $conTipoTemario;
															 echo $textoTipo ?>
															 </td>
															 <td>
															 </td>
														</tr>
														<!--<tr height="25"> 
															 <td  align="left" class="letraFicha" >
															  Abreviatura:
															 </td>
															 <td width="10"></td>
															 <td colspan="7" class="copyrigth" align="left">
															  <?php echo $fila[15];?>  
															 </td>
													   </tr>-->
													    <tr height="25"> 
															 <td  align="left" class="letraFicha" >
															  Descripci&oacute;n:
															 </td>
															 <td width="10"></td>
															 <td colspan="7" class="copyrigth" align="left">
															  <?php echo $fila[18];?>  
															 </td>
													   </tr>
													   
														<tr height="25"> 
															 <td  align="left" class="letraFicha" valign="top" >
															  Palabras Clave:
															 </td>
															 <td width="10"></td>
															 <td colspan="7" class="copyrigth" align="left">
															 <?php echo $fila[10]?>  
															 </td>
													   </tr>
														<tr height="25"> 
															 <td  align="left" class="letraFicha" valign="top" >
															  Propósito:
															 </td>
															 <td width="10"></td>
															 <td colspan="7" class="copyrigth" style="padding: 15px 15px 15px 15px; text-align:justify !important" align="left">
															 <?php echo $fila[6]?>
															 </td>
													   </tr>
														<tr height="25"> 
															 <td  align="left" class="letraFicha" valign="top" >
															  Objetivo:
															 </td>
															 <td width="10"></td>
															 <td colspan="6" class="copyrigth" style="padding: 15px 15px 15px 15px; text-align:justify !important" align="left">
															 <?php echo $fila[4]?>	
															 </td>
													   </tr>
														<tr height="25"> 
															 <td  align="left" class="letraFicha" valign="top" >
															  Objetivos Específicos:
															 </td>
															 <td width="10"></td>
															 <td colspan="6" class="copyrigth" style="padding: 15px 15px 15px 15px; text-align:justify !important" align="left">
															 <?php echo $fila[5]?>
															 </td>
													   </tr>
                                                       <?php 
                                                       if($datosC)
													   {
													   ?>
                                                       <tr>
                                                             <td align="center" class="letraFicha" valign="top" >
                                                               Clasificaci&oacute;n :
                                                             </td>
                                                             <td>
                                                             </td>       
                                                             <td colspan="5">  
                                                               <table>
                                                                   <tr>
                                                                       <td width="80" align="left" class="letraFicha">
                                                                            Campo:
                                                                        </td>
                                                                        <td width="450" align="left" class="copyrigth">
                                                                            <?php echo $nombreCampo?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="80" align="left" class="letraFicha">
                                                                            Disciplina:
                                                                        </td>
                                                                        <td width="200" align="left" class="copyrigth">
                                                                            <?php echo $nombreDisc?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="80" align="left" class="letraFicha">
                                                                            SubDisciplina:
                                                                        </td>
                                                                        <td width="200" align="left" class="copyrigth">
                                                                            <?php echo $nombreSubD?>
                                                                        </td>
                                                                    </tr>
                                                               </table>
                                                             </td>
                                                       </tr>
                                                       <?php
													   }
													   ?>
													</table>
													</td>
												</tr>
											 </table>
                                       </td>
                                       </tr>
                                       </table>
                                       </td>
                                       </tr>
                                  </table>
                        </fieldset>
                         </td>
                       </tr>
					 </table>
                     
                       <br/>
                       <br/>
                       <br/>
                        
                          <table width="750">
                            
                            
                            <tr> 
                                <td width="700" align="left" > 
                                <fieldset class="frameHijo"><legend><b>Base Conceptual</b></legend>
                                    <table width="100%">
                                    <tr>
                                    <td align="center">
                                    	<br />
                                        <div id="my-tabs" align="center" style="width:600px;"></div>
                                     </td>
                    				</tr>
                                    </table>
                                </fieldset>
                                </td>
                            </tr>
                            <form id="frmEnvioC" action="estructuraConceptual.php" method="post">
						  	<input type="hidden" name="idMateria" id="idMateria2" value="<?php echo $idMateria ?>" />
                            <input type="hidden" name="idCiclo" id="idCiclo" value="<?php echo $idCiclo ?>" />
                            <input type="hidden" name="idPrograma" value="<?php echo $idPrograma ?>" />
                            <input type="hidden" name="panel" id="panel" value="<?php echo $panel ?>" />
                            <input type="hidden" name="idMapaCurricular" id="idMapaCurricular" value="<?php echo $idMapaCurricular ?>" />
						    </form>
                          </table>
                       
                                                            
                                                  
      							<table id='tabla1' align="center" width="90%" class="x-hide-display" >
                                <?php 
								if($filaCiclo!=1)
								{
								?>
                                <tr height="23">
                                    <td colspan="3" align="right">
                                    <br />
                                    <span class="letraFichaRespuesta">Para agregar una nueva actitud de click </span> <a href="javascript:agregarActitud()"><span class="letraRoja">AQUÍ</span></a>
                                    <br /><br />
                                    </td>
                                 </tr>
                  	            <?php 
								}
								//aqui dibuja las Actitudes de la materia									
                                $consulta="SELECT o.idMateriaVSActitud,n.titulo, descripcion,o.idActitud FROM 4008_actitudes n,4043_materiaVsActitudes o WHERE n.idActitud=o.idActitud AND  o.idMateria=".$idMateria." order by n.titulo";
								$resA=$con->obtenerFilas($consulta);
								$reg=$con->filasAfectadas;
								if($reg=="0")
								{
								?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con actitudes relacionadas</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fA=mysql_fetch_row($resA))
								{
								?>
                                    <tr height="23"  id='fila_<?php echo $fA[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"> <?php echo $x?>.- &nbsp;<?php echo $fA[1] ?> </td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fA[3] ?>','4008_actitudes','idActitud')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                       <?php 
									   $x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                       
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fA[0] ?>','-1','A')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                    <?php 
									}
									?>
                                    
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
									   
									}
									?>
                                    <tr height="23">
                                    </tr>
                             
                                    
                                
                    </table>
                    <table align="center" id="tabla2" width="90%" class="x-hide-display">
                     <?php 
					  if($filaCiclo!=1)
					  {
					  ?>
                    <tr height="23">
                        <td colspan="3" align="right">
                        <br />
                        <span class="letraFichaRespuesta">Para agregar una nueva competencia de click </span> <a href="javascript:agregarCompetencia()"><span class="letraRoja">AQUÍ</span></a>
                        <br /><br />
                        </td>
                     </tr>
                                <?php
					  }
									//aqui dibuja las competencias de la materia
								$consultaC="SELECT o.idMateriaVsCompetencias,n.titulo,descripcion,o.idCompetencia FROM 4007_competencias n,4041_materiaVsCompetencias o WHERE n.idCompetencia=o.idCompetencia AND  o.idMateria=".$idMateria." order by n.titulo";
								$resC=$con->obtenerFilas($consultaC);
								$reg1=$con->filasAfectadas;
								if($reg1=="0")
									
									{
								?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con competencias relacionadas</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
									
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fC=mysql_fetch_row($resC))
								{
								?>
                                    <tr height="23" id='fila_<?php echo $fC[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"><?php echo $x ?>.-&nbsp; <?php echo $fC[1] ?></td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fC[3] ?>','4007_competencias','idCompetencia')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                        <?php 
										$x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fC[0] ?>','-1','C')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                         <?php 
										 }
										 ?>
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
									
									}
									
									?>
                                    <tr height="23">
                                    </tr>
                    </table>
                    <table align="center" id="tabla3" width="90%" class="x-hide-display">
                     <?php 
					  if($filaCiclo!=1)
					  {
					  ?>
                    <tr height="23">
                        <td colspan="3" align="right">
                        <br />
                        <span class="letraFichaRespuesta">Para agregar un nuevo Producto de click </span> <a href="javascript:agregarProducto()"><span class="letraRoja">AQUÍ</span></a>
                        <br /><br />
                        </td>
                     </tr>
                                <?php	
					  }
								//aqui dibuja los Productos
								$consultaE="SELECT idMateriaVSProducto,titulo,descripcion FROM  4012_productos p,4185_materiaVSProducto m WHERE idMateria=".$idMateria." AND p.idProducto=m.idProducto order by titulo";
								$resE=$con->obtenerFilas($consultaE);
								$reg2=$con->filasAfectadas;
								if($reg2=="0")
									{
									?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con Productos relacionados</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
								
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fE=mysql_fetch_row($resE))
								{
								?>
                                    <tr height="23" id='fila_<?php echo $fE[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"><?php echo $x ?>.-&nbsp; <?php echo $fE[1] ?></td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fE[3] ?>','4010_evaluaciones','idEvaluacion')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                       <?php 
									   $x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fE[0] ?>','-1','E')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                         <?php 
										 }
										 ?>
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
								
									}
									?>
                                    <tr height="23">
                                    </tr>
                    </table>
                    
                    <table align="center" id="tabla4" width="90%" class="x-hide-display">
                     <?php 
					  if($filaCiclo!=1)
					  {
					  ?>
                    <tr height="23">
                        <td colspan="3" align="right">
                        <br />
                        <span class="letraFichaRespuesta">Para agregar una nueva Habilidad de click </span> <a href="javascript:agregarHabilidad()"><span class="letraRoja">AQUÍ</span></a>
                        <br /><br />
                        </td>
                     </tr>
                                <?php
					  }
                                //aqui dibuja las Habilidades de la materia
								$consultaH="SELECT o.idMateriaVsHabilidad,n.titulo, descripcion,o.idHabilidad FROM 4006_habilidades n,4042_materiaVsHabilidades o WHERE n.idHabilidad=o.idHabilidad AND  o.idMateria=".$idMateria." order by n.titulo";
								$resH=$con->obtenerFilas($consultaH);
								$reg3=$con->filasAfectadas;
								if($reg3=="0")
									{
									?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con Habilidades relacionadas</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fH=mysql_fetch_row($resH))
								{
								?>
                                    <tr height="23" id='fila_<?php echo $fH[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"><?php echo $x ?>.-&nbsp; <?php echo $fH[1] ?></td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fH[3] ?>','4006_habilidades','idHabilidad')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                       <?php 
									   $x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fH[0] ?>','-1','H')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                        <?php }?>
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
									}
									

									?>
                                    <tr height="23">
                                    </tr>
                    </table>
                    
                    <table align="center" id="tabla5" width="90%" class="x-hide-display">
                    <tr height="23">
                        <td colspan="3" align="right">
                        <br />
                        <span class="letraFichaRespuesta">Para agregar un nuevo recurso de click </span> <a href="javascript:agregarRecurso()"><span class="letraRoja">AQUÍ</span></a>
                        <br /><br />
                        </td>
                     </tr>
                                <?php	
                                //aqui dibuja los Recursos de la materia
								$consultaR="SELECT o.idMateriaVsRecurso,n.titulo,descripcion,o.idRecurso FROM 4009_recursos n,4045_materiaVsRecursos o WHERE n.idRecurso=o.idRecurso AND  o.idMateria=".$idMateria." order by n.titulo";
								$resR=$con->obtenerFilas($consultaR);
								$reg4=$con->filasAfectadas;
								if($reg4=="0")
									{
									?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con Recursos relacionadas</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fR=mysql_fetch_row($resR))
								{
								?>
                                    <tr height="23" id='fila_<?php echo $fR[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"><?php echo $x ?>.-&nbsp;<?php echo $fR[1] ?></td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fR[3] ?>','4009_recursos','idRecurso')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                        <?php 
										$x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fR[0] ?>','-1','R')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                        <?php }?>
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
									
									}
									?>
                                    <tr height="23">
                                    </tr>
                    </table> 
                    
                    <table align="center" id="tabla6" width="90%" class="x-hide-display">
                    <tr height="23">
                        <td colspan="3" align="right">
                        <br />
                        <span class="letraFichaRespuesta">Para agregar una nueva técnica colaborativa de click </span> <a href="javascript:agregarTecnica()"><span class="letraRoja">AQUÍ</span></a>
                        <br /><br />
                        </td>
                     </tr>
                                <?php	
                                //aqui dibuja las Tecnicas de la materia
								$consultaT="SELECT o.idMateriaVsTecnica,n.titulo,descripcion,o.idTecnicaC FROM  4011_tecnicasColaborativas n,4046_materiaVsTecnicas o WHERE n.idTecnicaC=o.idTecnicaC AND  o.idMateria=".$idMateria." order by n.titulo";
								$resT=$con->obtenerFilas($consultaT);
								$reg5=$con->filasAfectadas;
								if($reg5=="0")
									{
									?>
								         <tr height="35" align="center">
										  <td align="center"><font class="letraAzul">Esta materia no cuenta con técnicas colaborativas  relacionadas</font></td>
										 </tr>
										 	
								<?php 
								}
								else
								{
								?>
										
										<tr height="23">
										  <td bgcolor="#0099ff" align="center"  width="300" class="letraFicha"><font color="#000099" ><b>Titulo</b></font></td>
										  <td bgcolor="#0099FF" align="center" width="100" class="letraFicha"><font color="#000099"><b> Descripcion </b></font></td>
										</tr>
								<?php
								}
								$clase="filaRosa10";
								$x=1;
								while($fT=mysql_fetch_row($resT))
								{
								?>
                                    <tr height="23" id='fila_<?php echo $fT[0] ?>'>
                                        <td class="<?php echo $clase?>" width='200' align="left"> <?php echo $x ?>.-&nbsp;<?php echo $fT[1] ?></td>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:descripcion('<?php echo $fT[3] ?>','4011_tecnicasColaborativas','idTecnicaC')"><img src='../images/icon_code.gif' border=0 width='15' height='15' title='Ver descripcion' ></a> </td>
                                        <?php 
										$x++;
									   if ($filaCiclo!=1 )
					                         {
									   ?>
                                        <td class="<?php echo $clase?>" align='center'><a href="javascript:ejecutarAccionActitudMateria('<?php echo $fT[0] ?>','-1','T')"><img src='../images/cancel_round.png' border=0 width='15' height='15' title='Eliminar' ></a> </td>
                                         <?php }?>
                                    </tr>
                                    <?php
									        if($clase=="filaRosa10")
											   $clase="filaBlanca10";
											 else
											   $clase="filaRosa10";	
									
									}
									?>
                                    <tr height="23">
                                    </tr>
                    </table>

                       </td>
                     </tr>
                     <tr>
                       <td align="center">
                       <br/>
                       <br/>
                       <br/>
                       <?php
                        
                         
							$consulta="select * from 4013_materia where idMateria=".$idMateria;
					        $fila=$con->obtenerPrimeraFila($consulta);
							
							$idMateria1="-$idMateria";
						
						if($validacion!="1")
						{
						?>
                         
                         <table width="750">
                         <tr>
                         <td align="left">
                         <fieldset class="frameHijo"><legend><b>Base Temática</b></legend>
                          <table width="100%">
                            <tr height="30" >
                             <td align="right" colspan="2" class="letraFicha">
                               <?php 
						       if ($filaCiclo!=1 )
					           {
						       ?>
                               <a href="javascript:modificarEstructuraTematica()">Modificar <img src='../images/edit_f2.png' border=0 width='15' height='15' title='Modificar' ></a>
                             <?php 
						   }

						   ?>
                              </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td colspan="3" align="center" >
                               <table width="">
                                 <tr>
                                   <td align="left">
                                     <div id="divArbol" style="width:180px;"/>
                                   </td>
                                 </tr>
                           
                               </table>
                              </td>
                            </tr>
                          </table>
                          </fieldset>
                          </td>
                          </tr>
                          </table>
                          <?php 
						  }
						  ?>
                        		<form id="frmEnvioT" action="estructuraTematica.php" method="post">
						  		<input type="hidden" name="idMateria" id="idMateria3" value="<?php echo $fila[0] ?>" />
                                <input type="hidden" name="idCiclo" id="idCiclo" value="<?php echo $idCiclo ?>" />
                                <input type="hidden" name="nombre" id="nombre" value="<?php echo $fila[3] ?>" />
						    	</form>
                                <input type="hidden" name="validacion" id="validacion" value="<?php echo $filaCiclo ?>" />
							 	<input type="hidden" id="idSeccion" name="idSeccion" value="<?php echo $idPrograma?>" />
                               </table>
                              </td>
                             </tr>
                           </table>
                    </td>
                    <input type="hidden" id="tipoTemario" name="tipoTemario" value="<?php echo $validacion?>" />
                    <input type="hidden" id="idMapaCurricular" name="idMapaCurricular" value="<?php echo $idMapaCurricular?>" />
                    
                   </tr>  
                     
                        
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
