<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$tituloModulo="Criterio de evaluación";
?>
<script type="text/javascript" src="../Scripts/jquery.min.js"></script>
<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/EditableItem.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/RangeMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/ListMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GridFilters.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/Filter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/StringFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/DateFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/ListFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/NumericFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/BooleanFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/dataConceptosAPI.js.php"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="../Administracion/Scripts/catalogos.js.php"></script>

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
						
							
								
						
					if(isset($objParametros->idEvaluacion))
						$idEvaluacion=$objParametros->idEvaluacion;
					else
				  		$idEvaluacion="-1";
					$idMapaCurricular="-1";
					if(isset($objParametros->idMapaCurricular))
						$idMapaCurricular=$objParametros->idMapaCurricular;
				  	$consulta="select * from 4010_evaluaciones where idEvaluacion=".$idEvaluacion;
					$fila=$con->obtenerPrimeraFila($consulta);
					

					$arrTipoEvaluacion=array();
					$arrTipoEvaluacion[0][0]="1";
					$arrTipoEvaluacion[0][1]="Ingreso manual";
					$arrTipoEvaluacion[1][0]="2";
					$arrTipoEvaluacion[1][1]="Automática (Mediante función de sistema)";
					echo formatearTituloPagina($tituloModulo,true,$idEvaluacion);
					
					
				  ?>
                  	<table width="650">
                      <tr>
                      <td align="left">
                            <form id="frmEnvio" method="post" action="../paginasFunciones/guardarDatos.php"> 
                           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          
                              <tr>
                                <td>
                               
                                <table width="800" border="0">
                                  <tr height="23">
                                    <td width="260" valign="top" align="left">
                                    <label>Criterio de evaluaci&oacute;n: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                    </td>
                                    <td>
                                    <input type="text" name="_titulovch" id="_titulovch" maxlength="150" size="80" val="obl" campo="Criterio" value="<?php echo $fila[1]?>" />	
                                  </tr>
                                  <tr height="23">
                                    <td  valign="top" align="left">
                                    <label>Nombre corto: <font color="#FF0000">*</font>&nbsp;&nbsp;</label>
                                    </td>
                                    <td>
                                    <input type="text" name="_nombreCortovch" id="_nombreCortovch" maxlength="50" size="50" val="obl" campo="Nombre corto" value="<?php echo $fila[17]?>" />	
                                  </tr>
                                  <tr height="23">
                                    <td valign="top" align="left"><label>Descripción: <font color="#FF0000">&nbsp;&nbsp;</font></label></td>
                                    <td width="500"><textarea rows="8" cols="80" id="_descripcionvch" name="_descripcionvch" campo="Descripción" ><?php echo $fila[2]?></textarea>							
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaEscala" >
                                    <td valign="top" align="left"><label>Escala de evaluaci&oacute;n : <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select id="_idEscalaCalificacionint" name="_idEscalaCalificacionint" val="obl" campo="Escala de Calificaci&oacute;n">
                                        <option value="-1">Seleccione</option>
                                        <?php
											$consulta="SELECT idEscalaCalificacion,nombreEscala FROM 4032_escalasCalificacion order by nombreEscala";
											$con->generarOpcionesSelect($consulta,$fila[9]);
										?>
                                        </select>
                                        
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaEscala" >
                                    <td valign="top" align="left"><label># de decimales (Precisión) a utilizar en resultado: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select id="_precisionDecimalesint" name="_precisionDecimalesint" val="obl" campo="# de decimales (Precisión) a utilizar en resultado">
                                        <?php
											echo $con->generarNumeracionSelectNoImp(0,4,$fila[12]);
										?>
                                        
                                        </select>
                                    	
                                        
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaEscala" >
                                    <td valign="top" align="left"><label>Acción a aplicar sobre excedente de decimales: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select id="_accionPrecisionint" name="_accionPrecisionint" val="obl" campo="Acción a aplicar sobre excedente de decimales">
                                        <option value="-1">Seleccione</option>
                                        <?php
											$arrAccion[0][0]=1;
											$arrAccion[0][1]="Truncar";
											$arrAccion[1][0]=2;
											$arrAccion[1][1]="Redondear";
											
											$con->generarOpcionesSelectArreglo($arrAccion,$fila[13]);
										?>
                                        </select>
                                        
                                    </td>
                                  </tr>
                                  
                                  
                                   <tr height="23">
                                    <td valign="top" align="left"><label>Forma de evaluación: <font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select id="_idTipoEvaluacionint" name="_idTipoEvaluacionint" val="obl" campo="Forma de evaluación" onchange="funcTipoCriterioChange(this)">
                                        <option value="-1">Seleccione</option>
                                        <?php
											$con->generarOpcionesSelectArreglo($arrTipoEvaluacion,$fila[7]);
											$none="none";
											$obl="";
											$oblCriterio="";
											$noneEvidencias="none";
											if($fila[7]==1)
											{
												$none="";
												$obl="obl";
											}
											if($fila[7]==2)
											{
												$noneEvidencias="";
												$oblCriterio="obl";
											}
										?>
                                        </select>
                                    </td>
                                  </tr>
                                   <tr height="23" id="filaEvidencias2" style="display:<?php echo $noneEvidencias?>">
                                    <td valign="top" align="left"><label>Función evaluación:<font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    <?php
										$lblFuncion="(No especificado)";
										
										if(($fila[10]=="")||($fila[10]=="-1"))
										{
											$fila[10]="";
										}
										else
										{
											$consulta="SELECT nombreConsulta FROM 991_consultasSql WHERE idConsulta=".$fila[10];
											$lblFuncion=$con->obtenerValor($consulta);
										}
										
									?>
                                    	<b><span class="letraExt" id="lbl_funcionEvaluacionint"><?php echo $lblFuncion?></span></b>&nbsp;&nbsp;
                                        <a href="javascript:mostrarVentanaExpresion('<?php echo bE("_funcionEvaluacionint")?>',1)"><img src="../images/add.png" width="13" height="13" title='Asignar función' alt='Asignar función' /></a>&nbsp;
                                        <a href="javascript:removerConcepto('<?php echo bE("_funcionEvaluacionint")?>')"><img src="../images/delete.png" width="13" height="13" title='Remover función' alt='Remover función' /></a>
                                        <input type="hidden" id="_funcionEvaluacionint" name="_funcionEvaluacionint" value="<?php echo $fila[10]?>" val="<?php echo $oblCriterio?>" campo="Función evaluación"  />
                                   
                                    </td>
                                  </tr>
                                  
                                  <tr height="23">
                                    <td valign="top" align="left"><label>Determinar valor máximo del criterio mediante:<font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select name="_formaValorMaximoCriterioint" id="_formaValorMaximoCriterioint" val="obl" campo="Determinar valor máximo del criterio" onchange="funcMaximoCriterio(this)">
                                         <option value="-1">Seleccione</option>
                                        	<?php
												$arrDeteminacionMaxima[0][0]=1;
												$arrDeteminacionMaxima[0][1]="Valor máximo de escala de evaluación";
												$arrDeteminacionMaxima[1][0]=2;
												$arrDeteminacionMaxima[1][1]="Valor constante";
												$arrDeteminacionMaxima[2][0]=3;
												$arrDeteminacionMaxima[2][1]="Automático (Mediante función de sistema)";
												$idFormaValor=-1;
												if(isset($fila[14]))
													$idFormaValor=$fila[14];
												echo $con->generarOpcionesSelectArreglo($arrDeteminacionMaxima,$idFormaValor);
												
												
												$mValorMaximoC="none";
												$oblValMaxC="";
												
												
												$mFuncionMaximoC="none";
												$oblFuncMaxC="";
												if(isset($fila[14]))
												{
													switch($fila[14])
													{
														case 2:
															$mValorMaximoC="";
															$oblValMaxC="obl";
														break;
														case 3:
															$mFuncionMaximoC="";
															$oblFuncMaxC="obl";
														break;
													}
												}
												
											?>
                                        </select>
                                        
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaValorCriterio1" style="display:<?php echo $mValorMaximoC?>">
                                    <td valign="top" align="left"><label>Valor máximo del criterio:<font color="#FF0000">*</font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    <?php
										if(!isset($fila[15]))
											$fila[15]="";
									?>
                                    	<input type="text" value="<?php echo $fila[15]?>" id="_valorMaximoint" name="_valorMaximoint" val="<?php echo $oblValMaxC ?>" size="3" campo="Valor máximo del criterio" onkeypress="return soloNumero(event,false,false,this)" />
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaValorCriterio2" style="display:<?php echo $mFuncionMaximoC?>">
                                    <td valign="top" align="left"><label>Función valor máximo del criterio:<font color="#FF0000"></font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<?php
										$lblFuncion="No especificado";
										$idValor=-1;
												
										if(isset($fila[11]))
										{
											if(($fila[11]=="")||($fila[11]==-1))
											{
												$fila[11]="";
											}
											else
											{
												$idValor=$fila[11];
												$consulta="SELECT nombreConsulta FROM 991_consultasSql WHERE idConsulta=".$fila[11];
												$lblFuncion=$con->obtenerValor($consulta);
											}
										}
										
										
									?>
                                    	<b><span class="letraExt" id="lbl_funcionSistemaValMaxint"><?php echo $lblFuncion?></span></b>&nbsp;&nbsp;
                                        <a href="javascript:mostrarVentanaExpresion('<?php echo bE("_funcionSistemaValMaxint")?>',1)"><img src="../images/add.png" width="13" height="13" title='Asignar función' alt='Asignar función' /></a>&nbsp;
                                        <a href="javascript:removerConcepto('<?php echo bE("_funcionSistemaValMaxint")?>')"><img src="../images/delete.png" width="13" height="13" title='Remover función' alt='Remover función' /></a>
                                        <input type="hidden" id="_funcionSistemaValMaxint" name="_funcionSistemaValMaxint" value="<?php echo $idValor?>" val="<?php echo $oblFuncMaxC ?>" campo="Función valor máximo del criterio" />
                                        	
                                       
                                        
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaValorCriterio2">
                                    <td valign="top" align="left"><label>Permitir ajuste de valor máximo?:<font color="#FF0000"></font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select name="_ajusteValMaxint" id="_ajusteValMaxint"  campo="Permitir ajuste de valor máximo" val="obl">
                                         <option value="-1">Seleccione</option>
                                        	<?php
												$consulta="SELECT valor,texto FROM 1004_siNo";
												$idValor=-1;
												if(isset($fila[16]))
													$idValor=$fila[16];
												echo $con->generarOpcionesSelect($consulta,$idValor);
											?>
                                        </select>
                                        
                                    </td>
                                  </tr>
                                  <tr height="23" id="filaValorCriterio2" style="display:none">
                                    <td valign="top" align="left"><label>Mostrar valor máximo?:<font color="#FF0000"></font>&nbsp;&nbsp;</label></td>
                                    <td width="">
                                    	<select name="_mostrarValMaximoint" id="_mostrarValMaximoint"  campo="Mostrar valor máximo" val="obl">
                                         <option value="-1">Seleccione</option>
                                        	<?php
												$consulta="SELECT valor,texto FROM 1004_siNo";
												//echo $con->generarOpcionesSelect($consulta,$fila[18]);
												echo $con->generarOpcionesSelect($consulta,0);
											?>
                                        </select>
                                        
                                    </td>
                                  </tr>
                                </table>
                                
                                </td>
                              </tr>
                              <tr height="23">
                                    <td colspan="2" align="center">
                                    <br />
                                     <?php 	
                                            if($idEvaluacion=="-1")
                                            {
                                    ?>
                                    <input type="button" class="btnAceptar" name="btnGuardar" value="Guardar" onclick="validarFrm('frmEnvio')"/>
                                    <input type="hidden" name="_fechaCreaciondta" value=" " />
                                    <input type="hidden" name="_responsableint" value="<?php echo $_SESSION["idUsr"]?>" />
                                    
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <input type="button" class="btnAceptar" name="btnGuardar" value="Actualizar" onclick="validarFrm('frmEnvio')"/>
                                    <input type="hidden" name="_fechaModifdta" value=" " />
                                    <input type="hidden" name="_respModifint" value="<?php echo $_SESSION["idUsr"]?>" />
                                    
                                    <?php 
                                            }
                                    ?>
                                    
                                    </td>
                                  </tr>
                            </table>
                            
                            <input type="hidden" name="tabla" value="4010_evaluaciones" />
                                <input type="hidden" name="id" value="<?php echo $idEvaluacion?>" />
                                <input type="hidden" name="campoId" value="idEvaluacion" />
                                <input type="hidden" name="pagRedireccion" value="../Administracion/tblEvaluaciones.php"/>
                                <input type="hidden" name="_idMapaCurricularint" value="<?php echo $idMapaCurricular?>" />	
                                <input type="hidden" name="paramPost" value='[{"nombreP":"configuracion","valorP":"<?php echo $nConfRegresar?>"}]' />	
                                
                            </form>
                            <br />
                      </td>
                      </tr>
                      </table>          
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
