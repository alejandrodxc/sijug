<?php
	include_once('latis/tcpdf/tcpdf.inc.php');
	include_once('latis/conexionBD.php');
	//include_once('../reportes/funcionesReportes.php');
	include_once('latis/numeroToLetra.php');
	include_once('latis/cAlmacen.php');
	//include_once('../../sigloxxi/ventas/funcionesVentaCaja.php');
	
	global $con;
	$fechaReporte=date('d-m-Y');
	$fechaR=date('Y-m-d');
	$idTipoOperacion=1;
	$idVenta=2513;
	$idFormaPago=-1;

	if(isset($_POST['idTipoOperacion']))// 1=Venta, 2=Pedido
		$idTipoOperacion=$_POST['idTipoOperacion'];
	else
		if(isset($_GET['idTipoOperacion']))
			$idTipoOperacion=$_GET['idTipoOperacion'];
	
	if(isset($_POST['idMovimiento']))
		$idVenta=$_POST['idMovimiento'];
	else
		if(isset($_GET['idMovimiento']))
			$idVenta=$_GET['idMovimiento'];
		else
			if(isset($_POST['idOperacion']))
				$idVenta=$_POST['idOperacion'];
			else
				if(isset($_GET['idOperacion']))
					$idVenta=$_GET['idOperacion'];
					
					
	if(isset($_POST['idFormaPago']))
		$idFormaPago=$_POST['idFormaPago'];
	else
		if(isset($_GET['idFormaPago']))
			$idFormaPago=$_GET['idFormaPago'];

	$esPrueba=false;
	if(isset($_POST['esPrueba']))
		$esPrueba=true;
	else
		if(isset($_GET['esPrueba']))
			$esPrueba=true;


if($idTipoOperacion==1)
{
	//$leyendaNota="Nota de venta:";
	if($idFormaPago==-1)
	{
		$consulta="SELECT formaPago FROM 6008_ventasCaja WHERE idVenta=".$idVenta;	
		$idFormaPago=$con->obtenerValor($consulta);
	}
}
else
{
	$leyendaNota="Nota de pedido:";
}

if($idFormaPago!=-1)
{
	$leyendaTipoPago=obtenerTipoPagoIEP($idFormaPago);	
}
else
{
	$leyendaTipoPago="";
}

$detalle=null;
$subTotal=0;
$iva=0;
$total=0;
$totalG=0;
$fechaMov="";
$libro=new cExcel("../modulosEspeciales/ticketChiles.xlsx",true,"Excel2007");
switch($idTipoOperacion)
{
	case 1:
			$consulta="SELECT subtotal,iva,total,tipoCliente,idCliente,totalDescuento,datosCompra,formaPago,folioVenta,fechaVenta
					 FROM 6008_ventasCaja WHERE idVenta='".$idVenta."'";
			
			$totales=$con->obtenerPrimeraFila($consulta);
					 
			$consulArticulos="SELECT p.idProductoVenta,p.idProducto,a.descripcionProducto,cantidad,costoUnitario,total,descuentoUnitario,
				 0,p.llave,metaData,(SELECT IF(abreviatura IS NULL OR abreviatura='',unidadMedida,abreviatura)AS unidadMedida FROM 6923_unidadesMedida WHERE idUnidadMedida=p.unidadMedida) as uM 
				 FROM 6009_productosVentaCaja p,6932_descripcionProducto a WHERE p.idProducto=a.idProducto and p.llave=a.llave
				 AND idVenta=".$idVenta." ORDER BY idProductoVenta";
			$detalle=$con->obtenerFilas($consulArticulos);
			
			
			$nFilas=$con->filasAfectadas;
			
			$nFilas--;
			
			if($nFilas>0)
				$libro->insertarFila(9,$nFilas);
			
			
			
			
			$folioVenta=$totales[8];
			$subTotal=$totales[0]+$totales[5];
			$iva=$totales[1];
			$totalG=$totales[2];
			
			$descuento=$totales[5];
			$fechaMov=formatearFecha($totales[9]);
			
			
			
			
			
	break;
	
}


	
	$libro->setValor("B4",$folioVenta);
	$libro->setValor("B5",$fechaMov);
	
	
	
	$linea=8;
	switch($idTipoOperacion)
	{
		case 1:
			while($fila=mysql_fetch_row($detalle))
			{
				
				
				$descripcion= $fila[2];
				$producto=$fila[1];		
				$cantidad=number_format($fila[3],2)." (".$fila[10].")";
				$pu=$fila[4];
				$total=$fila[4]*$fila[3];
				$libro->setValor("B".$linea,$cantidad);
				$libro->setValor("A".$linea,$descripcion);
				$libro->setValor("C".$linea,number_format($total,2));
				$linea++;
			}
		break;
	}

	$linea=10+$nFilas;
		
	$libro->setValor("C".$linea,"$ ".number_format($subTotal,2));
	$linea++;
	$libro->setValor("C".$linea,"$ ".number_format($iva,2));
	$linea++;
	$libro->setValor("C".$linea,"$ ".number_format($totalG,2));
	$linea++;



	
	$libro->generarArchivoServidor("PDF",$folioVenta.".pdf");
	header('Content-Type: application/pdf');
	header("Content-length: ".filesize($folioVenta.".pdf"));
	header("Content-Disposition: inline; filename=".$folioVenta.".pdf");
	readfile($folioVenta.".pdf");
	unlink($folioVenta.".pdf");
	
		
		

function formatearFecha($fecha)
{
	return date("d/m/Y H:i:s",strtotime($fecha))	;
}




function obtenerTipoPagoIEP($idTipo)
{
	global $con;
	$consulta="SELECT formaPago FROM 600_formasPago WHERE idFormaPago='".$idTipo."'";
	$res=$con->obtenerValor($consulta);				
	return $res;
}




?>