<?php	include("latis/conexionBD.php"); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body >
	<?php
		$idFalta=$_POST["idFalta"];
		$consulta="SELECT idGrupo,idUsuario,fechafalta,horaInicial,horaFinal,idRegistroJustificacion FROM 4559_controlDeFalta WHERE idFalta=".$idFalta;
	
		$falta=$con->obtenerPrimeraFila($consulta);

		
		$consulta="select m.nombreMateria,g.nombreGrupo,idInstanciaPlanEstudio,g.fechaFin,g.Plantel FROM 4502_Materias m,4520_grupos g WHERE g.idGrupos=".$falta[0]." AND m.idMateria=g.idMateria";

		$fGrupo=$con->obtenerPrimeraFila($consulta);
		$consulta="";
		$aReponer=0;
		$nTiempo=0;
		if($falta[5]=="")
			$falta[5]=-1;
		$duracionHora=obtenenerDuracionHoraGrupo($falta[0]);
		
		$consulta="SELECT  cmbFormaReposicion, txtHorasReponer FROM _481_tablaDinamica WHERE id__481_tablaDinamica= ".$falta[5];

		$fReg=$con->obtenerPrimeraFila($consulta);
		if(($fReg)&&($fReg[0]!=1))
		{
			if($fReg[0]==3)
				$nTiempo=$fReg[1];
			else
			{
				
				
				$arrRecesos=obtenerArregloRecesos();
					
				 $nTiempo=obtenerNumeroHorasBloque($falta[0],$falta[3],$falta[4],$fGrupo[4],$arrRecesos);
			}
		}
		$aReponer=$nTiempo;
		$nProfesor=obtenerNombreUsuarioPaterno($falta[1]);
	?>
    
	<table width="100%" style="background-color:#FFF">
   		<tr height="21">
        	<td>
            </td>
        </tr>
    	<tr height="21">
        	<td valign="top" width="100" >
            	&nbsp;&nbsp;<label style="color:#000"><b>Grupo:</b></label>
            </td>
            <td valign="top">
            	<label class="letraExt"><?php echo $fGrupo[1]?></label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Materia:</b></label>
            </td>
            <td valign="top">
            	<label class="letraExt"><?php echo $fGrupo[0]?></label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Profesor:</b></label>
            </td>
            <td valign="top">
            	<label class="letraExt"><?php echo $nProfesor?></label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Fecha falta:</b></label>
            </td>
            <td valign="top">
            	<label class="letraExt"><?php echo date("d/m/Y",strtotime($falta[2]))?></label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Horario falta:</b></label>
            </td>
            <td valign="top">
            	<label class="letraExt"><?php echo date("H:i",strtotime($falta[3]))." - ".date("H:i",strtotime($falta[4]))?></label>
            </td>
        </tr>
        <tr height="21">
        	<td colspan="2" valign="top">
            	&nbsp;&nbsp;<label  class="letraRojaSubrayada8">Desgloce de horas:</label>
            </td>
        </tr>
         <tr height="15">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>A reponer:</b></label>
            </td>
            <td valign="top">
            	<label id="lblAReponer" class="letraExt"><?php echo $aReponer?></label>
            </td>
        </tr>
         <tr height="15">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Agendadas:</b></label>
            </td>
            <td valign="top">
            <label id="lblAgendadas" class="letraExt">0</label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top">
            	&nbsp;&nbsp;<label style="color:#000"><b>Por agendar:</b></label>
            </td>
            <td valign="top">
            	<label id="lblPorReponer" class="letraExt"><?php echo $aReponer?></label>
            </td>
        </tr>
        <tr height="21">
        	<td valign="top" colspan="2"><br />
				<table id="lblReposicionAgendada">
                	<tr>
                    	<td width="80" align="center">
                        	<span class="letraExt"><b>Fecha sesión</b></span>
                        </td>
                        <td width="80" align="center">
                        	<span class="letraExt"><b>Horario</b></span>
                        </td>
                        <td width="80" align="center">
                        	<span class="letraExt"><b>#Horas repone</b></span>
                        </td>
                    </tr>
                    <tr height="1">
                    	<td style="background-color:#900" colspan="3"></td>
                    </tr>
                </table>
                <br /><br><br /><br /><br /><br /><br /><br /><br />
            </td>
            
        </tr>
        
    </table>
	<input type="hidden" id="duracionHora" value="<?php echo $duracionHora?>" />
    <input type="hidden" id="idGrupo" value="<?php echo $falta[0]?>" />
   
    
</body>
</html>