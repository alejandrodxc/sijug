<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT id__742_tablaDinamica,agendarDiaInhabil,accionDiaNoHabil FROM _742_tablaDinamica";
	$fConfiguracion=$con->obtenerPrimeraFila($consulta);
	
	$agendarDiaInhabil=1;
	
	$accionDiaNoHabil=1;
	$listDias="";
	if($fConfiguracion)
	{
		$agendarDiaInhabil=$fConfiguracion[1];
		$accionDiaNoHabil=$fConfiguracion[2];
		$consulta="SELECT dia FROM _742_diasValidosFechaPago WHERE idReferencia=".$fConfiguracion[0];
		$listDias=$con->obtenerListaValores($consulta);
	}
	if($listDias=="")
		$listDias=-1;
	$fechasNoHabiles="";
	if($fConfiguracion&&($fConfiguracion[1]==0))
	{
		$consulta="SELECT fechaInicio,fechaFin FROM 4525_fechaCalendarioDiaHabil WHERE afectaPago=1 ORDER BY fechaInicio";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			$obj="[Date.parseDate('".$fila[0]."','Y-m-d'),Date.parseDate('".$fila[1]."','Y-m-d')]";
			if($fechasNoHabiles=="")
				$fechasNoHabiles=$obj;
			else
				$fechasNoHabiles.=",".$obj;
		}
	}
	
?>

var arrDiasPermitidos=[<?php echo $listDias?>];
var agendarDiaInhabil=<?php echo $agendarDiaInhabil?>;
var accionDiaNoHabil=<?php echo $accionDiaNoHabil?>;
var fechasNoHabiles=[<?php echo $fechasNoHabiles?>];

var ignorarRecarga=false;
var inicioCheck=0;
Ext.onReady(inicializar);

function inicializar()
{
	crearGridAdeudos();
    crearGridReestructuracion();
    
    var objEstructura=bD(gE('objEstructura').value);
	if(objEstructura!='')
    {
    	objEstructura=eval(objEstructura)[0];
        
        
    	gE('lblMontoReestructura').innerHTML=Ext.util.Format.usMoney(objEstructura.montoReestructura);
        gEx('numPagos').setValue(objEstructura.noPagos);
        if(objEstructura.diferenciaCentavos=='1')
        {
        	gEx('chkDiferencia').setValue(true);
            if(objEstructura.tipoDescuento!='')
            {
            	inicioCheck=1;
            	gEx('chkDescuento').setValue(true);
                gEx('cmbTipoDescuento').setValue(objEstructura.tipoDescuento);
                gEx('cmbTipoDescuento').enable();
                gEx('txtDescuento').setValue(objEstructura.cantidadDescuento);
                gEx('txtDescuento').enable();
            }
        }
       
                                                                    
        var reg=crearRegistro	(
                                    [
                                        {name: 'concepto'},
                                        {name: 'monto'},
                                        {name: 'fechaVencimiento'},
                                        {name: 'montoDescuento'},
                                        {name: 'fechaVencimientoDescuento'}
                                    ]
                                )
        var r;
        var x;
        
        
        for(x=0;x<objEstructura.arrPagos.length;x++)
        {
        
            r=new reg	(
                            {
                                concepto:objEstructura.arrPagos[x][0],
                                monto:objEstructura.arrPagos[x][1],
                                fechaVencimiento:Date.parseDate(objEstructura.arrPagos[x][2],'Y-m-d'),
                                montoDescuento:objEstructura.arrPagos[x][3],
                                fechaVencimientoDescuento:Date.parseDate(objEstructura.arrPagos[x][4],'Y-m-d')
                            }
                        )
           
                        
            gEx('tblReestructura').getStore().add(r);   
        }
        
        
    }
    
    if(gE('sL').value=='1')
    {
    	oE('lblFechaVencimiento');
        oE('btnFDescuento');
    }
}

function crearGridAdeudos()
{
	var dsDatos=eval(bD(gE('arrRegistros').value));
    var maxNumPago=parseInt(gE('maxNumPago').value);
    var arrCampos=	[
                        {name: 'idMovimiento'},
                        {name:'idReferencia'}, 
                        {name:'idConcepto'},
                        {name:'situacion'},
                        {name: 'fechaPago', type:'date', dateFormat:'Y-m-d'},
                        {name: 'fechaVencimiento', type:'date', dateFormat:'Y-m-d'},
                        {name:'nombreConcepto'},
                        {name:'montoPagado'}
                    ]
                    
    var arrColumnas= [
                        
                        {
                            header:'Concepto',
                            width:280,
                            sortable:true,
                            dataIndex:'nombreConcepto',
                            renderer:function(val,meta,registro)
                            		{
                                    	
                                         return val;
                                    }
                        },
                        {
                            header:'Fecha de Vencimiento',
                            width:120,
                            sortable:true,
                            css:'text-align:right !important;',
                            dataIndex:'fechaVencimiento',
                            renderer:function(val,meta,registro)
                                    {
                                    	var comp='';
                                      
                                        if(val)
                                            return comp+val.format('d/m/Y');
                                    }
                        },
                        {
                            header:'Situaci&oacute;n',
                            width:70,
                            sortable:true,
                            dataIndex:'situacion',
                            renderer:function(val,meta,registro)
                            	{
                                	var  comp='';
                                    
                                	switch(val)
                                    {
                                    	case '0':
                                        	return '<img src="../images/control_pause.png">'+comp;
                                        break;
                                        case '1':
                                        	return '<img src="../images/icon_big_tick.gif">'+comp;
                                        break;
                                        case '2':
                                        	return '<img src="../images/cancel_round.png">'+comp;
                                        break;
                                    }
                                }
                        }
                        
                    ]
	var x;
    var etPago='';
    for(x=0;x<maxNumPago;x++)
    {
        etPago='Pago '+(x+1);
    	arrCampos.push({name: 'pago_'+x});
        arrCampos.push({name: 'et_'+x});
        columna=	{
        				header:etPago,
                        width:90,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'pago_'+x,
                        summaryType:'sum',
                        css:'text-align:right !important;',
                        renderer:function(val,meta,registro,nFila,colIndex )
                                    {
                                    	var cadFinal=Ext.util.Format.usMoney(val);
                                        
                                        if(gE('sL').value=='1')
                                        	return '<span style="color:#030; ">'+cadFinal+'</span>';
                                    	var tblAdeudos=gEx('tblAdeudos');
                                        if(colIndex)
                                        {
                                            var col=tblAdeudos.getColumnModel().config[colIndex];
                                            var arrDatos=col.dataIndex.split('_');
                                            if(val=='')
                                                return '';
                                                
                                        	var checado='';
                                            if(registro.data.noPagoConsiderar==arrDatos[1])        
                                            	checado='checked=checked';
                                                
                                            
                                            if(registro.data.idMovimiento)
                                                return '<input type=\'checkbox\'  '+checado+' id=\'chk_'+nFila+'_'+registro.data.idMovimiento+'_'+arrDatos[1] +'\' onclick=\'javascript:checkClick(this)\'>&nbsp;<span style="color:#030; ">'+cadFinal+'</span>';
                                        }
                                        return '<span style="color:#030; ">'+cadFinal+'</span>';
                                    }
        			};
        arrColumnas.push(columna);
        columna=	{
        				header:'Vigencia Pago',
                        width:170,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'et_'+x,
                        css:'text-align:left !important;'
        			};
        arrColumnas.push(columna);
    }   
    arrCampos.push({name: 'situacionSolExtension'});
    arrCampos.push({name: 'idRegSolExtension'});
                         
    arrCampos.push({name: 'noPagoConsiderar'});                 
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	arrCampos
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	arrColumnas
												);
    var summary = new Ext.ux.grid.GridSummary();                                              
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            renderTo:'tblPagos',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:850,
                                                            id:'tblAdeudos',
                                                            plugins:[summary],
                                                            sm:chkRow
                                                            
                                                        }
                                                    );
	                                                 
	return 	tblGrid;	
}

function mostrarSolicitudPago(iM)
{
	var arrDatos=[["idFormulario",900],["idRegistro",-1],["idReferencia",bD(iM)],["actor",bE(182)],['dComp',bE('agregar')]];
    
    
    
    var ventanaAbierta=window.open('',"vAuxiliar2", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
    enviarFormularioDatosV("../modeloPerfiles/vistaDTD.php",arrDatos,'POST','vAuxiliar2');	


}

function modificarSolicitudPago(iR)
{
	verRegistroProyecto(iR,bE(713),bE(900));
}

function verSolicitudPago(iR)
{
	verRegistroProyecto(iR,bE(0),bE(900));
}

function regresar1Pagina()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresar2Pagina()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function recargarContenedorCentral()
{
	if(!ignorarRecarga)
		recargarPagina();
    
}

function regresar1PaginaContenedor()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresarPagina2Contenedor()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresarContenedorCentral()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function crearGridReestructuracion()
{
	var arrTipoDescuento=[['1','Monto'],['2','Porcentaje']];
	var cmbTipoDescuento=crearComboExt('cmbTipoDescuento',arrTipoDescuento,0,0,110);
    cmbTipoDescuento.setValue('2');
    cmbTipoDescuento.on('select',function()
    							{
                                	calcularReestructura();
                                }
    					)
	cmbTipoDescuento.disable();                        
	var summary = new Ext.ux.grid.GridSummary();                                              
	var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                        			{name: 'concepto'},
                                                                    {name: 'monto'},
                                                                    {name: 'fechaVencimiento', type:'date',dateFormat:'Y-m-d'},
                                                                    {name: 'montoDescuento'},
                                                                    {name: 'fechaVencimientoDescuento', type:'date',dateFormat:'Y-m-d'}
                                                        		]
                                                    }
                                                );


	
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														
														{
															header:'Concepto',
															width:260,
															sortable:true,
                                                            editor:{xtype:'textfield'},
															dataIndex:'concepto'
														},
                                                        {
                                                            header:'Monto pago',
                                                            width:100,
                                                            sortable:false,
                                                            
                                                            dataIndex:'monto',
                                                            summaryType:'sum',
                                                            css:'text-align:right !important;',
                                                            renderer:function(val,meta,registro)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var cadFinal=Ext.util.Format.usMoney(val);
                                                                            return '<span style="color:#030; ">'+cadFinal+'</span>';
                                                                        }
                                                        },
														{
															header:'Fecha de Vencimiento&nbsp;<span id="lblFechaVencimiento"><a href="javascript:mostrarVentanaAsignarFechaPlan(\''+bE(1)+'\')"><img src="../images/pencil.png" width="13" height="13" /></a></span>',
															width:150,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'fechaVencimiento',
                                                            editor:	{
                                                            			xtype:'datefield'
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                        	return val.format('d/m/Y')
                                                                    
                                                                    }
														},
                                                        {
                                                            header:'Monto pago <br>con descuento',
                                                            width:100,
                                                            sortable:false,
                                                            dataIndex:'montoDescuento',
                                                            summaryType:'sum',
                                                            css:'text-align:right !important;',
                                                            renderer:function(val,meta,registro)
                                                                        {
                                                                        	if(!registro.data.concepto)
                                                                            {
                                                                            	var cadFinal=Ext.util.Format.usMoney(val);
	                                                                            return '<span style="color:#030; ">'+cadFinal+'</span>';
                                                                            }
                                                                        
                                                                            if(val=='')
                                                                                return '';
                                                                            if(registro.data.montoDescuento==registro.data.monto)
                                                                            {
                                                                            	oE('btnFDescuento');
                                                                            	return '(Sin descuento)';
                                                                            }
                                                                            mE('btnFDescuento');
                                                                            var cadFinal=Ext.util.Format.usMoney(val);
                                                                            return '<span style="color:#030; ">'+cadFinal+'</span>';
                                                                        }
                                                        },
                                                        {
															header:'Fecha de Vencimiento<br />(Descuento)&nbsp;<span id="btnFDescuento"><a href="javascript:mostrarVentanaAsignarFechaPlan(\''+bE(2)+'\')"><img src="../images/pencil.png" width="13" height="13" /></a>'+
                                                            		'&nbsp;&nbsp;&nbsp;<a href="javascript:ajustarFechaVencimientoDescuento()"><img src="../images/calendar_edit.jpg" width="15" height="15" title="Ajustar a dia anterior de fecha de vencimiento" alt="Ajustar a dia anterior de fecha de vencimiento" /></a></span>',
															width:160,
															sortable:true,
                                                            css:'text-align:right !important;',
															dataIndex:'fechaVencimientoDescuento',
                                                            editor:	{
                                                            			xtype:'datefield'
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                        	return val.format('d/m/Y')
                                                                    
                                                                    }
														}
													]
												);

	

	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            clicksToEdit:1,
                                                            border:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:850,
                                                            id:'tblReestructura',
                                                            plugins:[summary],
                                                            tbar:	[
                                                            			{
                                                                            xtype:'checkbox',
                                                                            id:'chkDescuento',
                                                                            disabled:(gE('sL').value=='1'),
                                                                            listeners:	{
                                                                                            check:function(chk,valor)
                                                                                                    {
                                                                                                    	if(inicioCheck==1)
                                                                                                        {
                                                                                                        	inicioCheck=0;
                                                                                                            return;
                                                                                                        }
                                                                                                        calcularReestructura();
                                                                                                        if(valor)
                                                                                                        {
                                                                                                            gEx('cmbTipoDescuento').enable();
                                                                                                            gEx('txtDescuento').enable();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            gEx('cmbTipoDescuento').disable();
                                                                                                            gEx('txtDescuento').disable();
                                                                                                        }
                                                                                                    }
                                                                                        },
                                                                            boxLabel:'<span style="color:#000"><b>Permitir descuento</b></span>'
                                                                        },'-',
                                                            			{
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Tipo de descuento:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbTipoDescuento,
                                                                        '-',
                                                                        {
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000"><b>Cantidad descuento:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        {
                                                                        	xtype:'numberfield',
                                                                            allowDecimals:true,
                                                                            allowNegative:false,
                                                                            width:80,
                                                                            disabled:true,
                                                                            id:'txtDescuento',
                                                                            value:0,
                                                                            listeners:	{
                                                                                            change:function(ctrl,nValor)
                                                                                                    {
                                                                                                        if(nValor=='')
                                                                                                        {
                                                                                                            nValor=0;
                                                                                                            ctrl.setValue(0);   
                                                                                                         }
                                                                                                        calcularReestructura();
                                                                                                    }
                                                                                        }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
    
    tblGrid.on('beforeedit',function(e)
    						{
                            	if(gE('sL').value=='1')
                                {
                                	e.cancel=true;
                                    return;
                            	}
                            	if((e.field=='fechaVencimientoDescuento')||(e.field=='montoDescuento'))
                                {
                                	if((e.record.data.montoDescuento==e.record.data.monto)||(e.record.fechaVencimiento==''))
                                    	e.cancel=true;
                                }
                            
                            }
    			)
	
                                
    var panel=	new Ext.Panel (
    								{
                                        renderTo:'tReestructura',
                                        tbar:	[
                                                    {
                                                        xtype:'label',
                                                        html:'<span style="color:#000"><b>Monto a reestructurar:&nbsp;&nbsp;</b></span>'
                                                        
                                                    },
                                                    {
                                                        xtype:'label',
                                                        html:'&nbsp;&nbsp;<span style="color:#900; font-weight:bold" id="lblMontoReestructura">$ 0.00</span>'
                                                        
                                                    }
                                                    
                                                    
                                                    ,'-',
                                                    {
                                                        xtype:'label',
                                                        html:'<span style="color:#000"><b>Diferir adeudo en:&nbsp;&nbsp;</b></span>'
                                                        
                                                    },
                                                    {
                                                        xtype:'numberfield',
                                                        allowDecimals:false,
                                                        allowNegative:false,
                                                        width:25,
                                                        id:'numPagos',
                                                        value:1,
                                                        disabled:(gE('sL').value=='1'),
                                                        listeners:	{
                                                                        change:function(ctrl,nValor)
                                                                                {
                                                                                    if(nValor=='')
                                                                                    {
                                                                                        nValor=1;
                                                                                        ctrl.setValue(1);   
                                                                                     }
                                                                                    calcularReestructura();
                                                                                }
                                                                    }
                                                        
                                                    },
                                                    {
                                                        xtype:'label',
                                                        html:'<span style="color:#000"><b>&nbsp;&nbsp;pagos</b></span>'
                                                        
                                                    },'-',
                                                    {
                                                        xtype:'checkbox',
                                                        id:'chkDiferencia',
                                                        checked:true,
                                                        disabled:(gE('sL').value=='1'),
                                                        listeners:	{
                                                                        check:function(chk,valor)
                                                                                {
                                                                                    calcularReestructura();
                                                                                }
                                                                    },
                                                        boxLabel:'<span style="color:#000"><b>Aplicar diferencia de centavos al &uacute;ltimo pago</b></span>'
                                                    }
                                                    
                                                ],
                                        items:	[
                                                    tblGrid
                                                ]	
                            		}
    							)
                                                     
	return 	tblGrid;
}

function calcularReestructura()
{
	
	var arrFilasTMP=[];
	var tblReestructura=gEx('tblReestructura');
    var x;
    var fila;
    for(x=0;x<tblReestructura.getStore().getCount();x++)
    {
    	fila=tblReestructura.getStore().getAt(x);
        arrFilasTMP.push(fila);
    }
    tblReestructura.getStore().removeAll();
    var reg=crearRegistro	(
    							[
                                	{name: 'concepto'},
                                    {name: 'monto'},
                                    {name: 'montoDescuento'},
                                    {name: 'fechaVencimiento'},
                                    {name: 'fechaVencimientoDescuento'}
                            	]
    						)
	var r;

    var numPagos=gEx('numPagos').getValue();
    var montoPago=parseFloat(normalizarValor(gE('lblMontoReestructura').innerHTML));
    
    if(montoPago==0)
    	return;
    
    var parcialidad=montoPago/numPagos
    
    if(gEx('chkDiferencia').getValue())
    {
    	parcialidad+='';
    	var aPago=parcialidad.split('.');
        parcialidad=parseFloat(aPago[0]);
    }
    
    var totalAcumulado=0;
    var fVencimiento='';
    var fDescuento='';
    for(x=1;x<=numPagos;x++)
    {
    	if(x!=numPagos)
        	totalAcumulado+=parcialidad;
       	else
        	parcialidad=montoPago-totalAcumulado;
        parcialidadDescuento=parcialidad;    
        if(gEx('chkDescuento').getValue())
        {
        	switch(gEx('cmbTipoDescuento').getValue())
            {
            	case '1':
                	parcialidadDescuento-=parseFloat(gEx('txtDescuento').getValue());
                break;
                case '2':
                	var descuento=Ext.util.Format.number((parcialidadDescuento*(parseFloat(gEx('txtDescuento').getValue())/100)),'0.00');
                    
                    
                	parcialidadDescuento=Ext.util.Format.number(parcialidadDescuento-descuento,'0.00');
                    
                    
                break;
            }
        }    
        fVencimiento='';
        
        fDescuento='';
        if(arrFilasTMP.length>=x)
        {
        	fVencimiento=arrFilasTMP[x-1].data.fechaVencimiento;
            if(parcialidad!=parcialidadDescuento)
	            fDescuento=arrFilasTMP[x-1].data.fechaVencimientoDescuento;
        }
        

        
    	r=new reg	(
        				{
                        	concepto:'Pago: '+x+ ' (Reestructura)',
                            monto:parcialidad,
                            montoDescuento:parcialidadDescuento,
                            fechaVencimiento:fVencimiento,
                            fechaVencimientoDescuento:fDescuento
                        }
        			)
		tblReestructura.getStore().add(r);                    
    }
    
}

function mostrarVentanaAsignarFechaPlan(tCol)
{
	var arrCriterio=[['1','Primer d\xEDa de cada mes'],['2','\xDAltimo d\xEDa de cada mes'],['3','Intervalo de tiempo'],['4','Mismo d\xEDa de cada mes']];
	var cmbCriterio=crearComboExt('cmbCriterio',arrCriterio,250,5,250);
    cmbCriterio.on('select',function(cmb,registro)
    								{
                                    	var lblIndique=gEx('lblIndique');
                                        var txtIntervalo=gEx('txtIntervalo');
                                        var cmbPeriodoTiempo=gEx('cmbPeriodoTiempo');
                                    	lblIndique.hide();
                                        txtIntervalo.hide();
                                        cmbPeriodoTiempo.hide();
                                        txtIntervalo.setValue('');
                                        if(registro.get('id')=='3')
                                        {
                                        	lblIndique.show();
                                            txtIntervalo.show();
                                            cmbPeriodoTiempo.show();
                                            txtIntervalo.focus(false,500);
                                        }
                                    }
    					)
    
    
    var arrPeriodo=[['1','Dias'],['2','Meses'],['3','A\xF1os']];
    var cmbPeriodoTiempo=crearComboExt('cmbPeriodoTiempo',arrPeriodo,310,35,150);
    cmbPeriodoTiempo.setValue('1');
    cmbPeriodoTiempo.hide();
   
	var gridTabuladorPlanPago=gEx('tblReestructura');
	
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha base a asignar:'
                                                        },
                                                        {
                                                        	x:190,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaDescuento'
                                                        },
                                                        
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Criterio de c&aacute;lculo de siguientes pagos',
                                                            width:650,
                                                            height:150,
                                                            id:'fCriterio',
                                                            
                                                            items:	[	
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Elija un criterio de generaci&oacute;n de fechas:'
                                                                        },
                                                                        cmbCriterio,
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            id:'lblIndique',
                                                                            xtype:'label',
                                                                            hidden:true,
                                                                            html:'Indique el intervalo de tiempo de entre pagos:'
                                                                        },
                                                                        {
                                                                        	xtype:'numberfield',
                                                                            id:'txtIntervalo',
                                                                            width:50,
                                                                            hidden:true,
                                                                            x:250,
                                                                            y:35
                                                                        },
                                                                        cmbPeriodoTiempo,
                                                                        {
                                                                        	xtype:'label',
                                                                            x:35,
                                                                            y:70,
                                                                            html:'<span class="letraRoja">*</span> <span style="color: #000">Las fechas de los pagos se calcular&aacute;n a partir del primer elemento seleccionado de cada plan de pago</span>'
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            x:35,
                                                                            y:90,
                                                                            html:'<span class="letraRoja">**</span> <span style="color: #000">Si la fecha del pago excede al &uacute;ltimoltimo d&iacute;a del mes, se asignar&aacute; como fecha de pago el ultimo d&iacute;a del mismo</span>'
                                                                        }
                                                            		]
                                                            
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Asignaci&oacute;n de fechas para descuesto por Pronto Pago',
										width: 700,
										height:280,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var x;
                                                                        var dteFechaDescuento=gEx('dteFechaDescuento');
                                                                        if(dteFechaDescuento.getValue()=='')	
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaDescuento.focus();
                                                                            }
                                                                            msgBox('Debe indicar la Fecha base a asignar',resp);
                                                                            return;
                                                                        }
                                                                        ultimaFecha=dteFechaDescuento.getValue();
                                                                        var fCriterio=gEx('fCriterio');
                                                                        
                                                                        var cmbCriterio=gEx('cmbCriterio');
                                                                        var cmbPeriodoTiempo=gEx('cmbPeriodoTiempo');
                                                                        var txtIntervalo=gEx('txtIntervalo');
                                                                        
                                                                        
                                                                        var ultimoIDPlan=-1;
                                                                        var ultimaFecha;
                                                                        var fecha;
                                                                        var txtFecha;
                                                                        var dia;
                                                                        var mes;
                                                                        var anio;
                                                                        var nFecha;
                                                                        var cadFechaAux;
                                                                        var nFechaAux;
                                                                        var intervalo;
                                                                        var fila;
                                                                        for(x=0;x<gridTabuladorPlanPago.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gridTabuladorPlanPago.getStore().getAt(x);
                                                                            fecha=ultimaFecha;
                                                                            dia=parseInt(fecha.format('d'));
                                                                            mes=parseInt(fecha.format('m'));
                                                                            anio=parseInt(fecha.format('Y'));
                                                                            switch(cmbCriterio.getValue())
                                                                            {
                                                                                case '1'://Primer dia de cada mes
                                                                                    mes++;
                                                                                    if(mes>12)
                                                                                    {
                                                                                        mes=1;
                                                                                        anio++;
                                                                                    }
                                                                                    
                                                                                    nFecha=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                    fecha=Date.parseDate(nFecha,'Y-m-d');
                                                                                break;
                                                                                case '2'://Ultimo dia de cada mes
                                                                                    mes++;
                                                                                    if(mes>12)
                                                                                    {
                                                                                        mes=1;
                                                                                        anio++;
                                                                                    }
                                                                                    
                                                                                    nFecha=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                    fecha=Date.parseDate(nFecha,'Y-m-d');
                                                                                    fecha=fecha.getLastDateOfMonth();
                                                                                break;
                                                                                case '3'://Intervalo de tiempo
                                                                                    
                                                                                    switch(cmbPeriodoTiempo.getValue())
                                                                                    {
                                                                                        case '1':
                                                                                            intervalo=	Date.DAY;
                                                                                        break;
                                                                                        case '2':
                                                                                            intervalo=	Date.MONTH;
                                                                                        break;
                                                                                        case '3':
                                                                                            intervalo=	Date.YEAR;
                                                                                        break;
                                                                                    }
                                                                                    
                                                                                    fecha=fecha.add(intervalo,txtIntervalo.getValue());
                                                                                    
                                                                                    
                                                                                break;
                                                                                case '4':  //Mismo dia de cada mes
                                                                                    mes++;
                                                                                    if(mes>12)
                                                                                    {
                                                                                        mes=1;
                                                                                        anio++;
                                                                                    }
                                                                                    cadFechaAux=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                    fecha=Date.parseDate(cadFechaAux,'Y-m-d');
                                                                                    fecha=fecha.getLastDateOfMonth();
                                                                                    cadFechaAux=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-'+rellenarCadena(dia,2,'0',-1);
                                                                                    nFechaAux=Date.parseDate(cadFechaAux,'Y-m-d');
                                                                                    
                                                                                    if(fecha.format('m')==nFechaAux.format('m'))
                                                                                    {
                                                                                        fecha=nFechaAux;
                                                                                    }
                                                                                break;
                                                                            }
                                                                            fecha=obtenerDiaHabil(fecha,accionDiaNoHabil);
                                                                            if(bD(tCol)=='1')
	                                                                            fila.set('fechaVencimiento',fecha);
                                                                            else
                                                                            	fila.set('fechaVencimientoDescuento',fecha);
                                                                            ultimaFecha=fecha;
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function obtenerDiaHabil(fecha,accion)
{
	var dia=fecha.getDay();
    
    return fecha;

    if(dia==0)
	   	dia=7;
    if(existeValorArreglo(arrDiasPermitidos,dia)!=-1)
    {
    	if(!esDiaInhabil(fecha))
        	return fecha;
        
    }
    
    if(accion=='0')
        fecha=fecha.add(Date.DAY,1);
   	else
        fecha=fecha.add(Date.DAY,-1);
   	
    return obtenerDiaHabil(fecha,accion);
    
    
    
}

function esDiaInhabil(fecha)
{
	var x;
    for(x=0;x<fechasNoHabiles.length;x++)
    {
    	if((fecha>=fechasNoHabiles[x][0])&&(fecha<=fechasNoHabiles[x][1]))
        	return true;
        if(fechasNoHabiles[x][0]>fecha)
        	return false
    }
    return false;
}

function ajustarFechaVencimientoDescuento()
{
	
	var gridTabuladorPlanPago=gEx('tblReestructura');
    var x=0;
    var fila;	
    for(x=0;x<gridTabuladorPlanPago.getStore().getCount();x++)
    {
        fila=gridTabuladorPlanPago.getStore().getAt(x);
        if((fila.data.montoDescuento!=fila.data.monto)&&(fila.data.fechaVencimiento!=''))
        {
        	fila.set('fechaVencimientoDescuento',fila.data.fechaVencimiento.add(Date.DAY,-1));
        }
        
        
        
    }
}

function checkClick(chk)
{
	var arrDatos=chk.id.split('_');
    
    var tblAdeudos=gEx('tblAdeudos');
    var fila=tblAdeudos.getStore().getAt(parseInt(arrDatos[1]));
    if(chk.checked)
    {
    	fila.set('noPagoConsiderar',arrDatos[3]);
        
    }
    else
    {
    	fila.set('noPagoConsiderar','');
    }
    calcularAdeudosReestructura();
}

function calcularAdeudosReestructura()
{
	var total=0;
    var tblAdeudos=gEx('tblAdeudos');
    var x;
    var fila;
    for(x=0;x<tblAdeudos.getStore().getCount();x++)
    {
    	fila=tblAdeudos.getStore().getAt(x);
        if(fila.data.noPagoConsiderar!='')
        {
        	var pago=parseFloat(fila.get('pago_'+fila.data.noPagoConsiderar));
            total+=pago;
        
        }
    }
    gE('lblMontoReestructura').innerHTML=Ext.util.Format.usMoney(total);
    calcularReestructura();
}

function guardarReestructura()
{
	var cadObj='';
    var tblAdeudos=gEx('tblAdeudos');
    var x;
    var fila;
    var obj;
    var cadMovimientosConsiderar='';
    for(x=0;x<tblAdeudos.getStore().getCount();x++)
    {
    	fila=tblAdeudos.getStore().getAt(x);
        if(fila.data.noPagoConsiderar!='')
        {
        	obj='{"idMovimiento":"'+fila.data.idMovimiento+'","noPago":"'+fila.data.noPagoConsiderar+'"}';
        	if(cadMovimientosConsiderar=='')
            	cadMovimientosConsiderar=obj;
            else
            	cadMovimientosConsiderar+=','+obj;
        
        }
    }
    
    var cadReestructura='';
    var tblReestructura=gEx('tblReestructura');
    
    if(tblReestructura.getStore().getCount()==0)
    {
    	msgBox('No ha indicado la forma de reestructuraci&oacute;n del pago');
    	return;
    }
    
	for(x=0;x<tblReestructura.getStore().getCount();x++)
    {
    	fila=tblReestructura.getStore().getAt(x);
        
        montoDescuento=fila.data.montoDescuento;
        if(fila.data.monto==montoDescuento)
        	montoDescuento='';
            
        if(fila.data.fechaVencimiento=='')
        {
        	function resp1()
            {
            	tblReestructura.startEditing(x,2);
            }
        	msgBox('Debe ingresar la fecha de vencimiento del pago',resp1);
        	return;
        } 
        
        if((fila.data.fechaVencimientoDescuento=='')&&(montoDescuento!=''))
        {
        	function resp2()
            {
            	tblReestructura.startEditing(x,4);
            }
        	msgBox('Debe ingresar la fecha de vencimiento del descuento de pago',resp2);
        	return;
        }    
            
        obj='{"concepto":"'+cv(fila.data.concepto)+'","monto":"'+Ext.util.Format.number(fila.data.monto,'0.00')+'","montoDescuento":"'+Ext.util.Format.number(montoDescuento,'0.00')+'","fechaVencimiento":"'+fila.data.fechaVencimiento.format('Y-m-d')+'","fechaVencimientoDescuento":"'+
        		fila.data.fechaVencimientoDescuento.format('Y-m-d')+'"}';
        if(cadReestructura=='')
            cadReestructura=obj;
        else
            cadReestructura+=','+obj;
    }
    var diferenciaCentavosFinal=0;
    var tipoDescuento='';
    var porcentajeDescuento='0';
    if(gEx('chkDiferencia').getValue())
    {
    	diferenciaCentavosFinal=1;
        tipoDescuento=gEx('cmbTipoDescuento').getValue();
        porcentajeDescuento=gEx('txtDescuento').getValue();
    }
    cadObj='{"montoReestructurar":"'+normalizarValor(gE('lblMontoReestructura').innerHTML)+'","noPagos":"'+gEx('numPagos').getValue()+'","diferenciaCentavosFinal":"'+diferenciaCentavosFinal+'","tipoDescuento":"'+tipoDescuento+'","porcentajeDescuento":"'+porcentajeDescuento+'","idFormulario":"'+gE('idFormulario').value+
    		'","idRegistro":"'+gE('idRegistro').value+'","arrMovimientos":['+cadMovimientosConsiderar+'],"arrReestructura":['+cadReestructura+']}';
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	function respuesta()
            {
            	window.parent.mostrarMenuDTD();
            }
            msgBox('La informaci&oacute;n ha sido guardada satisfactoriamente',respuesta);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=40&cadObj='+cadObj,true);
}

