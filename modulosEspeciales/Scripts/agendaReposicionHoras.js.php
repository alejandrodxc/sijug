<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="	SELECT codigoUnidad, unidad,(SELECT tituloCentroC FROM 506_centrosCosto WHERE codigoUnidad=o.codigoUnidad ) AS centroCosto,ciudad,estado,idPais,municipio
				FROM 817_organigrama o,247_instituciones i WHERE i.idOrganigrama=o.idOrganigrama and institucion=1 and status=1 ORDER BY centroCosto,unidad";
	$resSedes=$con->obtenerFilas($consulta);
	$arrSedes="";
	while($fila=mysql_fetch_row($resSedes))
	{
		$idPais=$fila[5];
		$localidad=$fila[3];
		$estado=$fila[4];
		$municipio=$fila[6];
		$ubicacion="";
		if($idPais==146)
		{
			$consulta="select Localidad from 822_localidades where cveLocalidad='".$localidad."'";
			$localidad=$con->obtenerValor($consulta);
			$consulta="select municipio from 821_municipios where cveMunicipio='".$municipio."'";
			$municipio=$con->obtenerValor($consulta);
			$consulta="select estado from 820_estados where cveEstado='".$estado."'";
			$estado=$con->obtenerValor($consulta);
			
		}
		
		if($localidad!="")
			$ubicacion=$localidad;
		if($municipio!="")
		{
			if($ubicacion=="")
				$ubicacion=$municipio;
			else	
				$ubicacion.=", ".$municipio;
		}
		if($estado!="")
		{
			if($ubicacion=="")
				$ubicacion=$estado;
			else	
				$ubicacion.=", ".$estado;
		}
		if($ubicacion=="")
			$ubicacion="Sin informaci&oacute;n";
		$zona=$fila[2];
		if($zona=="")
			$zona="Sin informaci&oacute;n";
		$obj="['".$fila[0]."','".$fila[1]."','".$zona."','".$ubicacion."']";
		if($arrSedes=="")
			$arrSedes=$obj;
		else
			$arrSedes.=",".$obj;
	}
	
	$arrSedes="[".$arrSedes."]";
	
	$consulta="SELECT idAreaFisica,nombreArea,descripcion FROM 9309_ubicacionesFisicas where codigoInstitucion='".$_SESSION["codigoInstitucion"]."' and asignableCurso=1 ORDER BY nombreArea";
	$arrAulas=$con->obtenerFilasArreglo($consulta);
	
?>
var fechaMinima=convertirCadenaFecha('<?php echo date("Y-m-d")?>');
var fechaMaxima=null;
var arrAulas=<?php echo $arrAulas?>;
var arrSedes=<?php echo $arrSedes?>;
var today = new Date().clearTime();
var campoBusqueda;
var arrSesiones=new Array();    
var nFila=1;   
var arrDiasNoHabiles=null;         
                
App = function() {
    				return 	{
        						init : 	function() 
                                		{
                                        	fechaMaxima=convertirCadenaFecha(gE('fFin').value);
                                            arrDiasNoHabiles=eval(gE('arrDiasNoHabiles').value);
            								Ext.BLANK_IMAGE_URL = '../images/s.gif';
                                            var idUsuario=gE('idUsuario').value;
                                            // This is an example calendar store that enables the events to have
                                            // different colors based on CalendarId. This is not a fully-implmented
                                            // multi-calendar implementation, which is beyond the scope of this sample app
                                            this.calendarStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                storeId: 'calendarStore',
                                                                                                root: 'calendarios',
                                                                                                idProperty: 'id',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                autoLoad: true,
                                                                                                fields: [
                                                                                                            {name:'CalendarId', mapping: 'id', type: 'int'},
                                                                                                            {name:'Title', mapping: 'title', type: 'string'}
                                                                                                        ],
                                                                                                sortInfo: 	{
                                                                                                                field: 'CalendarId',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=3;
                                                                                                                                
                                
                                                                                                                            }
                                                                                                			}
                                                                                            }
                                                                                         );
                                            // A sample event store that loads static JSON from a local file. Obviously a real
                                            // implementation would likely be loading remote data via an HttpProxy, but the
                                            // underlying store functionality is the same.  Note that if you would like to 
                                            // provide custom data mappings for events, see EventRecord.js.
                                            this.eventStore = new Ext.data.JsonStore	(
                                            												{
                                                                                                id: 'eventStore',
                                                                                                root: 'evts',
                                                                                                proxy: new Ext.data.HttpProxy	(
                                                                                                                                    {
                                                                                                                                        url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                                                    }
                                      
                                                                                                                                ),
                                                                                                fields: Ext.calendar.EventRecord.prototype.fields.getRange(),
                                                                                                sortInfo: 	{
                                                                                                                field: 'StartDate',
                                                                                                                direction: 'ASC'
                                                                                                            },
                                                                                                listeners: {
                                                                                                				beforeLoad:function(proxy)
                                                                                                                			{
                                    																							proxy.baseParams.funcion=30;
                                                                                                                                proxy.baseParams.idUsuario=idUsuario;
                                                                                                                                proxy.baseParams.institucion='<?php echo $_SESSION["codigoInstitucion"]?>';
                                	
                                                                                                                            }	
                                                                                                			}
                                                                                            }
                                                                                       );
            
                                            // This is the app UI layout code.  All of the calendar views are subcomponents of
                                            // CalendarPanel, but the app title bar and sidebar/navigation calendar are separate
                                            // pieces that are composed in app-specific layout code since they could be ommitted
                                            // or placed elsewhere within the application.
                                            new Ext.Viewport	(
                                                                    {
                                                                        layout: 'border',
                                                                        renderTo: 'calendar-ct',
                                                                        items: 	[
                                                                                    
                                                                                    {
                                                                                        id: 'app-center',
                                                                                        title: '...', // will be updated to view date range
                                                                                        region: 'center',
                                                                                        layout: 'border',
                                                                                        items: 	[
                                                                                                    {
                                                                                                        id:'app-west',
                                                                                                        region: 'west',
                                                                                                        width: 250,
                                                                                                        
                                                                                                        border: false,
                                                                                                        layout:'absolute',
                                                                                                        items:	[
                                                                                                                    {
                                                                                                                        xtype: 'datepicker',
                                                                                                                        id: 'app-nav-picker',
                                                                                                                        x:30,
                                                                                                                        showToday:false,
                                                                                                                        cls: 'ext-cal-nav-picker',
                                                                                                                        minDate:fechaMinima,
                                                                                                                        maxDate:fechaMaxima,
                                                                                                                        disabledDates :arrDiasNoHabiles,
                                                                                                                        format:'Y-m-d',
                                                                                                                        listeners:	{
                                                                                                                                        'select':	{
                                                                                                                                                        fn: function(dp, dt)
                                                                                                                                                            {
                                                                                                                                                            	
                                                                                                                                                                App.calendarPanel.setStartDate(dt);
                                                                                                                                                            },
                                                                                                                                                        scope: this
                                                                                                                                                    }
                                                                                                                                    }
                                                                                                                    },
                                                                                                                    {
                                                                                                                    	y:170,
                                                                                                                    	xtype:'panel',
                                                                                                                        tbar:	[
                                                                                                                        
                                                                                                                        			{
                                                                                                                                        icon:'../images/icon_big_tick.gif',
                                                                                                                                        cls:'x-btn-text-icon',
                                                                                                                                        text:'Registrar reposici&oacute;n de horas',
                                                                                                                                        handler:function()
                                                                                                                                                {
                                                                                                                                                 	registrarReposicionHoras();   
                                                                                                                                                }
                                                                                                                                        
                                                                                                                                    }
                                                                                                                        
                                                                                                                        			
                                                                                                                        		],
                                                                                                                        autoLoad:	{
                                                                                                                                        url:'../modulosEspeciales/datosFaltaReposicion.php',
                                                                                                                                        scripts:false,
                                                                                                                                        params:	{
                                                                                                                                        			idFalta:gE('idFalta').value
                                                                                                                                        		}
                                                                                                                                       
                                                                                                                                    }
                                                                                                                    }
                                                                                                                ]
                                                                                                    },
                                                                                                    {
                                                                                                        xtype: 'calendarpanel',
                                                                                                        eventStore: this.eventStore,
                                                                                                        calendarStore: this.calendarStore,
                                                                                                        border: false,
                                                                                                        id:'app-calendar',
                                                                                                        region: 'center',
                                                                                                        activeItem: 0, // month view
                                                                                                        
                                                                                                        monthViewCfg:	{
                                                                                                                            showHeader: true,
                                                                                                                            showWeekLinks: true,
                                                                                                                            showWeekNumbers: false
                                                                                                                        },
                                                        
                                                                                                        
                                                                                                        showDayView: false,
                                                                                                        
                                                                                                        showMonthView: false,
                                                                                                        
                                                                                                        initComponent: 	function()
                                                                                                                        {
                                                                                                                            App.calendarPanel = this;
                                                                                                                            this.constructor.prototype.initComponent.apply(this, arguments);
                                                                                                                        },
                                                        
                                                                                                        listeners: 	{
                                                                                                                        
                                                                                                                        'viewchange': 	{
                                                                                                                                            fn: function(p, vw, dateInfo)
                                                                                                                                                {
                                                                                                                                                    if(this.editWin)
                                                                                                                                                    {
                                                                                                                                                        this.editWin.hide();
                                                                                                                                                    };
                                                                                                                                                    if(dateInfo !== null)
                                                                                                                                                    {
                                                                                                                                                        
                                                                                                                                                        Ext.getCmp('app-nav-picker').setValue(dateInfo.activeDate);
                                                                                                                                                        this.updateTitle(dateInfo.viewStart, dateInfo.viewEnd);
                                                                                                                                                    }
                                                                                                                                                },
                                                                                                                                                scope: this
                                                                                                                                        },
                                                                                                                        'dayclick': 	{
                                                                                                                                            fn: function(vw, dt, ad, el)
                                                                                                                                                {
                                                                                                                                                	if(dt<fechaMinima)
                                                                                                                                                    {
                                                                                                                                                    	msgBox('No puede asignar una reposici&oacute;n en fechas pasadas');
                                                                                                                                                    	return;
                                                                                                                                                    }
                                                                                                                                                    
                                                                                                                                                    if(dt>=fechaMaxima.add(Date.DAY, 1))
                                                                                                                                                    {
                                                                                                                                                    	msgBox('No puede asignar una reposici&oacute;n en fechas posteriores al t&eacute;rmino del curso');
                                                                                                                                                    	return;
                                                                                                                                                    }
                                                                                                                                                    
                                                                                                                                                    if(existeValorArreglo(arrDiasNoHabiles,dt.format('Y-m-d'))!=-1)
                                                                                                                                                    {
                                                                                                                                                    	msgBox('No puede asignar una reposici&oacute;n en un d&iacute;a marcado como NO h&aacute;bil');
                                                                                                                                                    	return;
                                                                                                                                                    }
                                                                                                                                                
                                                                                                                                                	var eventStore=gEx('app-calendar').eventStore;
                                                                                                                                                    var registro;
                                                                                                                                                    for(x=0;x<eventStore.getCount();x++)
                                                                                                                                                    {
                                                                                                                                                        registro=eventStore.getAt(x);
                                                                                                                                                        
                                                                                                                                                        if((dt>=registro.data.StartDate)&&(dt<registro.data.EndDate))
                                                                                                                                                        {
                                                                                                                                                        	msgBox('No puede agendar una sesi&oacute;n en un bloque hora ocupado por otro evento')
                                                                                                                                                        	return;
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    
                                                                                                                                                	mostrarVentanaAgregarHora(dt,ad);
                                                                                                                                                    this.clearMsg();
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'rangeselect': {
                                                                                                                                            fn: function(win, dates, onComplete)
                                                                                                                                                {

                                                                                                                                                    onComplete();
                                                                                                                                                },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventmove': 	{
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	rec.reject();
                                                                                                                                               	return;
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventresize': {
                                                                                                                                            fn: function(vw, rec)
                                                                                                                                            {
                                                                                                                                            	rec.reject();
                                                                                                                                               	return;
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'eventdelete': {
                                                                                                                                            fn: function(win, rec)
                                                                                                                                            {
                                                                                                                                                this.eventStore.remove(rec);
                                                                                                                                                
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        },
                                                                                                                        'initdrag': 	{
                                                                                                                                            fn: function(vw)
                                                                                                                                            {
                                                                                                                                                if(this.editWin && this.editWin.isVisible())
                                                                                                                                                {
                                                                                                                                                    this.editWin.hide();
                                                                                                                                                }
                                                                                                                                            },
                                                                                                                                            scope: this
                                                                                                                                        }
                                                                                                                    }
                                                                                                    }
                                                                                                ]
                                                                                    }
                                                                                ]
                                                                    }
                                                               )
                                            if(gE('fechaInicial').value!='')
                                            {                   
                                                var fechaInicial=new Date(gE('fechaInicial').value);
                                               gEx('app-calendar').setStartDate(fechaInicial);                                                               
                                            }
                                        },
        
                                
                                showEditWindow : function(rec, animateTarget)
                                				{
                                    				if(!this.editWin)
                                                    {
                                       					this.editWin = new Ext.calendar.EventEditWindow	(
                                                        													{
                                            																	calendarStore: this.calendarStore,
                                                                                                                listeners: {
                                                                                                                                'eventadd':	{
                                                                                                                                                fn: function(win, rec)
                                                                                                                                                	{
                                                                                                                                                    	
                                                                                                                                                    	
                                                                                                                                                        
                                                                                                                                                    },
                                                                                                                                                    scope: this
                                                                                                                                            },
                                                                                                                                'eventupdate': {
                                                                                                                                                    fn: function(win, rec)
                                                                                                                                                    	{
                                                                                                                                                           
                                                                                                                                                            
                                                                                                                                                        },
                                                                                                                                                    scope: this
                                                                                                                                                },
                                                                                                                                'eventdelete': {
                                                                                                                                                    fn: function(win, rec)
                                                                                                                                                        {
                                                                                                                                                           
                                                                                                                                                        },
                                                                                                                                                    scope: this
                                                                                                                                                },
                                                                                                                                'editdetails': {
                                                                                                                                                    fn: function(win, rec)
                                                                                                                                                    {
                                                                                                                                                    	
                                                                                                                                                    }
                                                                                                                                                }
                                            																			}
                                        																	}
                                                                                                        );
                                    				}
                                                    var ventana=this.editWin.show(rec, animateTarget);
                                                    if(rec.data!=undefined)
                                                    {
                                                    	
                                                    }
                                                    else
                                                    {
                                                        ventana.show(rec, animateTarget);
                                                        gE('arrUsuarios').value='';
                                                        gEx('gridUsuarios').getStore().loadData(eval('['+gE('arrUsuarios').value+']'));
                                                    }
                                				},
                                
                                
                                updateTitle: function(startDt, endDt)
                                			{
                                                var p = Ext.getCmp('app-center');
                                                
                                                if(startDt.clearTime().getTime() == endDt.clearTime().getTime())
                                                {
                                                    p.setTitle(startDt.format('F j, Y'));
                                                }
                                                else
                                                	if(startDt.getFullYear() == endDt.getFullYear())
                                                    {
                                                        if(startDt.getMonth() == endDt.getMonth())
                                                        {
                                                            p.setTitle(startDt.format('F j') + ' - ' + endDt.format('j, Y'));
                                                        }
                                                    	else
                                                        {
                                                        	p.setTitle(startDt.format('F j') + ' - ' + endDt.format('F j, Y'));
                                                    	}
                                                	}
                                                	else
                                                    {
                                                    	p.setTitle(startDt.format('F j, Y') + ' - ' + endDt.format('F j, Y'));
                                                	}
                                            },
                                
                                
                                showMsg: function(msg)
                                		{
                                            Ext.fly('app-msg').update(msg).removeClass('x-hidden');
                                        },
                                        
                                clearMsg: function()
                                        {
                                            Ext.fly('app-msg').update('').addClass('x-hidden');
                                        }
    						}
				}();

Ext.onReady(App.init, App);                                   

var registroGrid=crearRegistro([{"name":"idUsuario"},{"name":"nombreUsuario"},{"name":"tipoRelacion"}]);
var arrTipoRelacion=[['1','Asistente'],['2','Supervisor']];

function crearGridUsuariosInvitados(idGrid)
{

	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idUsuario'},
                                                                {name: 'nombreUsuario'},
                                                                {name: 'tipoRelacion'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	var cmbTipoRelacion=crearComboExt('cmbTipoRelacion_'+idGrid,arrTipoRelacion);
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Usuario',
															width:300,
															sortable:true,
															dataIndex:'nombreUsuario'
														},
														{
															header:'Participaci&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'tipoRelacion',
                                                            editor:cmbTipoRelacion,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoRelacion,val);
                                                                    }
                                                                    
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:idGrid,
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:550,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	agregarUsuario(tblGrid,idGrid);
                                                                                        
                                                                                        
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover usuario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al usuario que desea remover del evento');
                                                                                            return;
                                                                                        }
                                                                                    	function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                                generarArregloUsuarios(tblGrid);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover del evento al usuario seleccionado',resp)
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}


function agregarUsuario(grid,id)
{
	campoBusqueda=1;
	var cmbComboBusqueda=inicializarCombos();
    var cmbParticipacion=crearComboExt('cmbParticipacion_'+id,arrTipoRelacion,100,65);
    cmbParticipacion.setValue('1');
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Paterno',
                                                            checked:true,
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(1);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Ap. Materno',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(2);
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:250,
                                                            y:5,
                                                            xtype:'radio',
                                                            name:'vBusqueda',
                                                            boxLabel:'Nombre',
                                                            listeners:	{
                                                            				'check':function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
                                                                                        	ponerCBusqueda(3);
                                                                                    }
                                                            			}
                                                        },
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Usuario:'
                                                        },
                                                        cmbComboBusqueda,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Participaci&oacute;n:',
                                                            xtype:'label'
                                                        },
                                                        cmbParticipacion
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar usuario',
										width: 500,
										height:185,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbComboBusqueda.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbComboBusqueda.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe seleccionar el usuario a agregar como destinatario');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var pos=obtenerPosFila(grid.getStore(),'idUsuario',cmbComboBusqueda.getValue());
                                                                        if(pos!=-1)
																		{
                                                                        	msgBox('El usuario seleccionado ya ha sido agregado anteriormente en el evento');
                                                                        	return;
                                                                        }                                                                        
                                                                        var r=new registroGrid({
                                                                        							idUsuario:cmbComboBusqueda.getValue(),
                                                                                                    nombreUsuario:cmbComboBusqueda.getRawValue(),
                                                                                                    tipoRelacion:cmbParticipacion.getValue()
                                                                        						});
                                                                        grid.getStore().add(r);
                                                                        generarArregloUsuarios(grid);
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	 gE('arrUsuarios').value='';
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function ponerCBusqueda(cBusqueda)
{
	var cmbNombre=gEx('cmbNombre');
    cmbNombre.reset();
    cmbNombre.focus();
	campoBusqueda=cBusqueda;
	
}
function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
											]
										);
	var parametros=	{
						funcion:'1',
						criterio:''
					};
	return inicializarCmbNombre(pPagina,lector,parametros);
}
function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=campoBusqueda;
	}
	ds.on('beforeload',cargarDatos);
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>---<br>',
										'</div></tpl>'
									 )
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :500,
                                                        x:100,
                                                        y:35
													}
												 );
	
   	return comboNombre;
}

function generarArregloUsuarios(grid)
{
	var x;
    var arrUsuarios='';
    var obj='';
    var fila;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
    	obj="['"+fila.get('idUsuario')+"','"+fila.get('nombreUsuario')+"','"+fila.get('tipoRelacion')+"']";
    	if(arrUsuarios=='')
        	arrUsuarios=obj;
        else
        	arrUsuarios+=','+obj;
    }
    gE('arrUsuarios').value=arrUsuarios
    
}

function generarArregloUsuariosID(grid)
{
	var x;
    var arrUsuarios='';
    var obj='';
    var fila;
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        obj='{"idUsuario":"'+fila.get('idUsuario')+'","tipoRelacion":"'+fila.get('tipoRelacion')+'"}';
    	if(arrUsuarios=='')
        	arrUsuarios=obj;
        else
        	arrUsuarios+=','+obj;
    }
    return arrUsuarios;
}


function mostrarVentanaAgregarHora(dt,ad,rec)
{
	var M = Ext.calendar.EventMappings;
	var lblTitle='Agregar horario de materia';
    var duracionHora=parseInt(gE('duracionHora').value);

    var et= dt.add(Date.MINUTE, duracionHora);
   
    var valorMin=gE('horaInicial').value+':00';
    var valorMax=gE('horaFinal').value+':00';
   
    /*if(horaFinal!=null)
    {
    	et=horaFinal;
    }
    horaFinal=null;*/
    if(rec!=undefined)
    {
    	lblTitle='Modificar horario de materia';
        dt=rec.data[M.StartDate.name];
        et=rec.data[M.EndDate.name];
        
    }
	
	
   
    var almacen=new Ext.data.SimpleStore	(
											 	{
													fields:	[
															 	{name:'id'},
																{name:'nombre'},
																{name:'valorComp'}
																
															]
												}
											)
	almacen.loadData(arrAulas);                                            
	var comboTmp=document.createElement('select');
    var cmbAula =new Ext.form.ComboBox	(
													{
														
														id:'cmbAula',
														x:155,
														y:65,
														mode:'local',
														emptyText:'Elija una opci\u00f3n',
														store:almacen,
														displayField:'nombre',
														valueField:'id',
														transform:comboTmp,
														editable:false,
														typeAhead: true,
														triggerAction: 'all',
														lazyRender:true,
                                                        width:300,
                                                        tpl:'<tpl for="."><div class="x-combo-list-item"><b><span class="corpo8_bold">{nombre}</span></b><br><span class="letraRojaSubrayada8">Descripci&oacute;n:</span> <span class="copyrigthSinPaddingNegro">{valorComp}</span></div></tpl>'
													}
												)
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Hora de inicio de la sesi&oacute;n:'
                                                        },
                                                        {
                                                        	id:'hIni',
                                                        	x:155,
                                                            y:5,
                                                            xtype:'timefield',
                                                            increment:10,
                                                            minValue:valorMin,
                                                            maxValue:valorMax,
                                                            disabled:false,
                                                            width:140,
                                                            value:dt
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Hora de t&eacute;rmino de la sesi&oacute;n:'
                                                        },
                                                         {
                                                         	id:'hFin',
                                                        	x:155,
                                                            y:35,
                                                            xtype:'timefield',
                                                            increment:10,
                                                            minValue:valorMin,
                                                            maxValue:valorMax,
                                                            disabled:false,
                                                            width:140,
                                                            value:et
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'&Aacute;rea/Aula:'
                                                        },
                                                        cmbAula,
                                                        {
                                                        	x:465,
                                                            y:70,
                                                            html:'<a href="javascript:verDisponibilidadAula()"><img src="../images/magnifier.png" title="Ver disponibilidad del &aacute;rea/aula" alt="Ver disponibilidad del &aacute;rea/aula"></a>'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vHorario',
										title: lblTitle,
										width: 530,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var horaIni=gEx('hIni');
                                                                        var horaFin=gEx('hFin');
                                                                        
                                                                        var hInicio;
                                                                        var hFin;
                                                                        
                                                                        hInicio=new Date(dt.format('m/d/Y')+' '+horaIni.getValue());
                                                                        hFin=new Date(dt.format('m/d/Y')+' '+horaFin.getValue());	

                                                                        if(hInicio>hFin)
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	horaIni.focus();
                                                                            }
                                                                        	msgBox('La hora de inicio de la sesi&oacute;n no puede ser mayor que la hora de t&eacute;rmino',resp);
                                                                        	return;
                                                                        }
                                                                        if(hInicio==hFin)
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	horaIni.focus();
                                                                            }
                                                                        	msgBox('La hora de inicio de la sesi&oacute;n no puede ser igual que la hora de t&eacute;rmino',resp3);
                                                                        	return;
                                                                        }
                                                                        if(gE('validarHorasCompletas').value=='1')
                                                                        {
                                                                            var diferencia=(hFin-hInicio)/60000;
                                                                            if((diferencia%duracionHora)!=0)
                                                                            {
                                                                                function resp2()
                                                                                {
                                                                                    horaIni.focus();
                                                                                }
                                                                                msgBox('S&oacute;lo se pueden asignar horas completas  por sesi&oacute;n el cual tiene una duraci&oacute;n de: '+duracionHora+' minutos',resp2);
                                                                                return;
                                                                            }
                                                                    	}
                                                                        
                                                                        
                                                                        var idAula=cmbAula.getValue();
                                                                        if(idAula=='')
                                                                        {
                                                                        	msgBox("Debe indicar el aula en la cual ser&aacute; impartida la sesi&oacute;n");
                                                                        	return;
                                                                        }
                                                                        
                                                                        var obj={};
                                                                        
                                                                        obj.hInicio=hInicio;
                                                                        obj.hFin=hFin;
                                                                        obj.fecha=dt.format('Y-m-d');
                                                                        obj.idAula=idAula;
                                                                         var diferencia=hFin-hInicio;
                                                                         var minutos=diferencia/60000;

                                                                         var horas=minutos/duracionHora;
                                                                         
                                                                        obj.nHoras=horas; 
                                                                         var lblPorReponer=gE('lblPorReponer');
                                                                         var hPAgendar=parseFloat(lblPorReponer.innerHTML);
                                                                         
																		if(horas>hPAgendar)
                                                                        {
                                                                        	msgBox('El n&uacute;mero de horas a agendar (<b>'+horas+'</b>), es mayor que el requerido por cubrir (<b>'+hPAgendar+'</b>)')
                                                                        	return;
                                                                        }
                                                                        
                                                                        var objAula='{"idAula":"'+idAula+'","fecha":"'+obj.fecha+'","horaInicio":"'+obj.hInicio.format("H:i")+'","horaFin":"'+obj.hFin.format("H:i")+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            switch(arrResp[0])
                                                                            {
                                                                            	case '1':
                                                                                    
                                                                                    var lblReposicionAgendada=gE('lblReposicionAgendada');
                                                                                    var fila=lblReposicionAgendada.insertRow(-1);
                                                                                    fila.id='fila_'+nFila;
            
                                                                                    var pos=existeValorMatriz(arrAulas,idAula);
                                                                                    
                                                                                    var celda=fila.insertCell(-1);
                                                                                    celda.align='center';
                                                                                    celda.innerHTML='<a href="javascript:removerAgenda(\''+bE(nFila)+'\')"><img src="../images/delete.png" width="13" height="13" title="Remover agenda de sesión" alt="Remover agenda de sesión" /></a>&nbsp;<span alt="Aula: '+arrAulas[pos][1]+'" title="Aula: '+arrAulas[pos][1]+'">'+dt.format('d/m/Y')+'</span>';
                                                                                    
                                                                                    celda=fila.insertCell(-1);
                                                                                    celda.align='center';
                                                                                    celda.innerHTML='<span alt="Aula: '+arrAulas[pos][1]+'" title="Aula: '+arrAulas[pos][1]+'">'+hInicio.format('H:i')+'-'+hFin.format('H:i')+'</span>';
                                                                                    celda=fila.insertCell(-1);
                                                                                    celda.align='center';
                                                                                    
                                                                                    celda.innerHTML='<span alt="Aula: '+arrAulas[pos][1]+'" title="Aula: '+arrAulas[pos][1]+'">'+horas+'</span>';
                                                                                    lblPorReponer.innerHTML=hPAgendar-horas;
                                                                                    lblAgendadas.innerHTML=(parseFloat( lblAgendadas.innerHTML)+horas);
                                                                                    obj.id=nFila;
                                                                                    arrSesiones.push(obj);
                                                                                    nFila++;
                                                                                    var M = Ext.calendar.EventMappings;
                                                                                    var rec;
                                                                                    rec = new Ext.calendar.EventRecord();
                                                                                    rec.data[M.Title.name] = 'Reposici&oacute;n de clase';
                                                                                    rec.data[M.CalendarId.name] = '18';
                                                                                    rec.data[M.EventId.name] = 'evt_'+obj.id;
                                                                                    rec.data[M.StartDate.name] =hInicio;
                                                                                    rec.data[M.EndDate.name] = hFin;
                                                                                    rec.data[M.IsAllDay.name] = 0;
                                                                                    rec.data[M.IsNew.name] = true;
                                                                                    rec.data[M.Notes.name] = '';
                                                                                    rec.data[M.Location.name] = '';
                                                                                    rec.data[M.RO.name] = '0';
                                                                                    
                                                                                    gEx('app-calendar').eventStore.add(rec);
                                                                                    ventanaAM.close();
                                                                                break;
                                                                                case 2:
                                                                                	messageBox('El aula se encuentra ocupado en el d&iacute; y horario ingresado');
                                                                                    return;
                                                                                break;
                                                                                default:
                                                                                	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                break;
                                                                            }
                                                                           
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=31&cadObj='+objAula,true);

                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	if(rec!=undefined)
    {
        if(rec.data[M.Notes.name]!='-1')
        {
	        cmbAula.setValue(rec.data[M.Notes.name]);
        }
    }                                
	ventanaAM.show();	
}

function verDisponibilidadAula()
{
	var cmbAula=gEx('cmbAula');
    if(cmbAula.getValue()=='')
    {
    	msgBox('Debe seleccionar el aula cuya disponibilida de horario desea observar');
    	return;
    }

    var arrDatos=[['idAula',cmbAula.getValue()],['cPagina','mR1=false'],['idGrupo',gE('idGrupo').value]];
	window.open('',"vAuxiliar2", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
    enviarFormularioDatos('../planteles/horarioAreaFisica.php',arrDatos,'POST','vAuxiliar2');
    
}

function removerAgenda(i)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var id=parseInt(bD(i));
        	var x;
            
            var obj;
            for(x=0;x<arrSesiones.length;x++)
            {
            	obj=arrSesiones[x];
            	if(obj.id==id)
                {
                	arrSesiones.splice(x,1);
                	break;
                }
            }
        	
            
            var eventStore=gEx('app-calendar').eventStore;
            var registro;
            for(x=0;x<eventStore.getCount();x++)
            {
            	registro=eventStore.getAt(x);
				if(registro.data.EventId=='evt_'+id)
                {
                    eventStore.remove(registro);
                    break;
                }
            }
            
            
            var fila=gE('fila_'+id);
            fila.parentNode.removeChild(fila);
            var lblAgendadas=gE('lblAgendadas');
            lblAgendadas.innerHTML=(parseFloat( lblAgendadas.innerHTML)-obj.nHoras);
            var lblPorReponer=gE('lblPorReponer');
            var hPAgendar=parseInt(lblPorReponer.innerHTML);
            lblPorReponer.innerHTML=hPAgendar+obj.nHoras;
            
            
            
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover la fecha de sesi&oacute;n agendada?',resp)

}

function registrarReposicionHoras()
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
            var lblPorReponer=gE('lblPorReponer');
            var nHorasFaltantes=parseFloat(lblPorReponer.innerHTML);
            if(nHorasFaltantes>0)	
            {
                msgBox('A&uacute;n debe agendar la reposici&oacute;n de <b>'+nHorasFaltantes+'</b> horas')
                return;
            }
            var x;
            var obj;
            var o;
            var cadObj='';
            for(x=0;x<arrSesiones.length;x++)
            {
                obj=arrSesiones[x];
                o='{"idAula":"'+obj.idAula+'","fecha":"'+obj.fecha+'","horaInicio":"'+obj.hInicio.format("H:i")+'","horaFin":"'+obj.hFin.format("H:i")+'","nHoras":"'+obj.nHoras+'"}';
                if(cadObj=='')
                    cadObj=o;
                else
                    cadObj+=','+o
            }
            var cObj='{"idFalta":"'+gE('idFalta').value+'","arrReposicion":['+cadObj+']}';
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	function resp2()
                    {
                    	if(window.parent.registroExitoso!=undefined)
                        {
                        	window.parent.registroExitoso();
                        }
                    }
					msgBox('El registro de reposici&oacute;n de horas ha sido llevado a cabo con &eacute;xito',resp2);
                    return;
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=14&cadObj='+cObj,true);
            
            
            
		}
	}
    msgConfirm('Est&aacute; seguro de querer registrar la reposici&oacute;n de horas?',resp);

    
}