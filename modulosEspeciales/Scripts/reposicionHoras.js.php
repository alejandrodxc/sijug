<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT DISTINCT idUsuario FROM 4520_grupos g,4519_asignacionProfesorGrupo a WHERE a.idGrupo=g.idGrupos AND 
				a.situacion=1 AND participacionPrincipal=1 AND g.Plantel='".$_SESSION["codigoInstitucion"]."'";

	$listUsuarios=$con->obtenerListaValores($consulta);
	if($listUsuarios=="")
		$listUsuarios=-1;
	$query=bE(" idUsuario in (".$listUsuarios.")");
?>
var tipoBusqueda=1;
Ext.onReady(inicializar);
function inicializar()
{

	var  gridFaltasReposicion=crearGridReposicion();
    var oConf=	{
                    idCombo:'cmbProfesor',
                    posX:0,
                    posY:0,
                    raiz:'personas',
                    nRegistros:'num',
                    renderTo:'cmbNombreProfesor',
                    anchoCombo:400,
                    campoDesplegar:'Nombre',
                    campoID:'idUsuario',
                    campoHDestino:'idUsuario',
                    funcionBusqueda:13,
                    paginaProcesamiento:'../paginasFunciones/funcionesAuxiliares.php',
                    confVista:'<tpl for="."><div class="search-item">[{idUsuario}] {Paterno} {Materno} {Nom}</div></tpl>',
                    campos:	[
                                {name:'idUsuario'},
                                {name:'Nombre'},
                                {name: 'Paterno'},
                                {name: 'Materno'},
                                {name: 'Nom'}
                            ],
                    funcAntesCarga:function(dSet,combo)
                                {
                                    
                                    gE('idUsuario').value='-1';
                                    var aValor=combo.getRawValue();
                                    dSet.baseParams.criterio=aValor;
                                    dSet.baseParams.campoBusqueda=tipoBusqueda;
                                    dSet.baseParams.rol=bE("'5_0'");
                                    dSet.baseParams.cond='<?php echo $query?>';
                                },
                    funcElementoSel:function(combo,registro)
                                {
                                    gEx('gridFaltas').getStore().reload();
                                    gE('idUsuario').value=registro.get('idUsuario');
                                   
                                }  
                };
	crearComboExtAutocompletar(oConf);                
	
}

function crearGridReposicion()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idFalta'},
		                                                {name: 'grupo'},
                                                        {name: 'idGrupo'},
		                                                {name:'fechaFalta', type:'date', dateFormat:'Y-m-d'},
		                                                {name:'horaFalta'},
                                                        {name: 'tiempoReponer'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosProcesos.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaFalta', direction: 'ASC'},
                                                            groupField: 'fechaFalta',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='13';
                                        proxy.baseParams.idUsuario=gE('idUsuario').value;
                                    }
                        )   
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
                                                            header:'Fecha de falta',
                                                            width:100,
                                                            sortable:true,
                                                            dataIndex:'fechaFalta',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y');
                                                                    }
                                                        },
                                                        {
                                                            header:'Grupo',
                                                            width:320,
                                                            sortable:true,
                                                            dataIndex:'grupo'
                                                        },
                                                        {
                                                            header:'Horario',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'horaFalta'
                                                        },
                                                        {
                                                            header:'# horas a reponer',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'tiempoReponer'
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridFaltas',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            renderTo:'tblReposicion',
                                                            columnLines : true,
                                                            width:740,
                                                            height:330,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/calendar_edit.jpg',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agendar reposici&oacute;n de horas',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la falta cuyas horas desea agendar para reposici&oacute;n');
                                                                                        	return;
                                                                                        }
                                                                                    	var obj={};
                                                                                        obj.titulo='Agendar reposici&oacute;n de horas';
                                                                                        obj.ancho='98%';
                                                                                        obj.alto='100%';
                                                                                        obj.url='../modulosEspeciales/agendaReposicionHoras.php';
                                                                                        obj.params=[['idFalta',fila.get('idFalta')],['cPagina','sFrm=true']];
                                                                                        abrirVentanaFancy(obj);
                                                                                    }
                                                                            
                                                                        },
                                                            		],
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit: false,
                                                                                                enableGrouping :false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}

function criterioChange(rdo)
{
	tipoBusqueda=rdo.value;	
}

function registroExitoso()
{
	gEx('gridFaltas').getStore().reload();
    cerrarVentanaFancy();	
}