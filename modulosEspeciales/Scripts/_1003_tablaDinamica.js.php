<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT id__1010_tablaDinamica,estudiogabinete FROM _1010_tablaDinamica ORDER BY estudiogabinete";
	$arrEstudioGabinete=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__1004_tablaDinamica,disciplinaLaboratorio FROM _1004_tablaDinamica ORDER BY disciplinaLaboratorio";
	$arrDisciplina=$con->obtenerFilasArreglo($consulta);
	
?>

var registro;

var arrEstudioGabinete=<?php echo $arrEstudioGabinete?>;
var arrDisciplina=<?php echo $arrDisciplina?>;


function inyeccionCodigo()
{
	
   
    var grid_7875=gEx('grid_7875');
    grid_7875.getColumnModel().setRenderer(2,fichaEspecialidad);
    
}


function agregarEstudio()
{
	mostrarVentanaEstudio();
}

function mostrarVentanaEstudio()
{
	registro=crearRegistro	(
    							[
                                	{name: 'idRegistro'},
                                    {name: 'idReferencia'},
                                    {name:'tipoEstudioLaboratorio'},
                                    {name:'estudio'},
                                    {name:'preparacionPaciente'},
                                    {name:'idEstudio'}
                                ]
    						);
                            
                            
	var cmbTipoEstudio=crearComboExt('cmbTipoEstudio',[['1','Laboratorio'],['2','Gabinete']],110,5,220);
    cmbTipoEstudio.on('select',function(cmb,registro)
    							{
                                	switch(registro.data.id)
                                    {
                                    	case '1':
                                        	gEx('cmbDisciplinaEstudio').reset();
                                            gEx('cmbEstudio').reset();
                                            gEx('cmbDisciplinaEstudio').show();
                                            gEx('lblDisciplina').show();
                                        break;
                                        case '2':
                                        	gEx('cmbDisciplinaEstudio').reset();
                                            gEx('cmbDisciplinaEstudio').hide();
                                            gEx('lblDisciplina').hide();
                                            gEx('cmbEstudio').reset();
                                            cmbEstudio.getStore().loadData(arrEstudioGabinete);
                                        break;
                                    }
                                }
    				)
    var cmbDisciplinaEstudio=crearComboExt('cmbDisciplinaEstudio',arrDisciplina,110,35,300);
    cmbDisciplinaEstudio.hide();
    cmbDisciplinaEstudio.on('select',function(cmb,registro)
    								{
                                    	function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                gEx('cmbEstudio').getStore().loadData(eval(arrResp[1]));
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesClinicas.php',funcAjax, 'POST','funcion=8&idDiciplina='+registro.data.id,true);
                                    }
    						)
    var cmbEstudio=crearComboExt('cmbEstudio',[],110,65,350);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Tipo de estudio:'
                                                        },
                                                        cmbTipoEstudio,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            id:'lblDisciplina',
                                                            hidden:true,
                                                            html:'Disciplina estudio:'
                                                        },
                                                        cmbDisciplinaEstudio,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Estudio:'
                                                        },
                                                        cmbEstudio

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar estudio de laboratorio/gabinete',
										width: 500,
										height:	200,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbEstudio.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe seleccionar el estudio que desea agregar');
                                                                            return;
                                                                        }
                                                                        var datosPreparacion='';
                                                                        
                                                                        
                                                                        var posFila=obtenerPosFila(cmbEstudio.getStore(),'id',cmbEstudio.getValue());
                                                                        
                                                                        var r=new registro(
                                                                        						{
                                                                                                	idRegistro:-1,
                                                                                                    idReferencia:-1,
                                                                                                    tipoEstudioLaboratorio:cmbTipoEstudio.getRawValue(),
                                                                                                    estudio:cmbEstudio.getRawValue(),
                                                                                                    preparacionPaciente:cmbEstudio.getStore().getAt(posFila).data.valorComp,
                                                                                                    idEstudio:cmbEstudio.getValue()
                                                                                                }
                                                                        				);
                                                                     	var grid_7833=gEx('grid_7833');
                                                                        grid_7833.getStore().add(r);
                                                                        ventanaAM.close();
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function abrirFicha(iF,iR,titulo)
{
	var arrDatos=[['idFormulario',iF],['idRegistro',iR],['cPagina','sFrm=true'],['eliminarEspacios',1]];	
    
    var obj={};
    obj.params=arrDatos;
    obj.titulo=bD(titulo);
    obj.ancho='90%';
    obj.alto=400;
    obj.url='../modeloPerfiles/verFichaFormulario.php';
    
	abrirVentanaFancy(obj);
}

function fichaEstudio(val,meta,registro)
{
	if(registro.data.tipoEstudioLaboratorio=='Laboratorio')
		return '<a href=javascript:abrirFicha(1008,'+val+',"'+bE(registro.data.estudio)+'")><img src="../images/book_open.png" title="Abrir ficha del estudio" alt="Abrir ficha del estudio"></a>';
    return '<a href=javascript:abrirFicha(1010,'+val+',"'+bE(registro.data.estudio)+'")><img src="../images/book_open.png" title="Abrir ficha del estudio" alt="Abrir ficha del estudio"></a>';
}

var idEnfermedad=-1;

function agregarDiagnostico()
{
	registro=crearRegistro	(
    							[
                                	{name: 'idRegistro'},
                                    {name: 'idReferencia'},
                                    {name:'enfermedadCIE'},
                                    {name:'detalle'}
                                ]
    						);
                            
	var oConf=	{
                        idCombo:'cmbDiagnostico',
                        anchoCombo:325,
                        campoDesplegar:'enfermedad',
                        campoID:'idEnfermedad',
                        funcionBusqueda:9,
                        posX:120,
                        posY:5,
                        raiz:'registros',
                        nRegistros:'numReg',
                        paginaProcesamiento:'../paginasFunciones/funcionesClinicas.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{enfermedad}</td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEnfermedad'},
                                    {name:'enfermedad'}
                                ],
                        funcAntesCarga:function(dSet,combo)
                                    {
                                        
                                        idEnfermedad=-1;
                                        
                                    },
                        funcElementoSel:function(combo,registro)
                                    {
                                        idEnfermedad=registro.get('idEnfermedad')
                                        
                                        
                                    }  
                    };

    
    var cmbDiagnostico=crearComboExtAutocompletar(oConf);

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Diagn&oacute;stico:'
                                                        },
                                                        cmbDiagnostico,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:500,
                                                            height:80,
                                                            id:'detalle'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar diagn&oacute;stico',
										width: 550,
										height:210,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(idEnfermedad==-1)
                                                                        {
                                                                        	msgBox('Debe especificar el diagn&oacute;stico del paciente');
                                                                            return;
                                                                        }	
                                                                        var r=new registro(
                                                                        						{
                                                                                                	idRegistro:-1,
                                                                                                    idReferencia:-1,
                                                                                                    enfermedadCIE:cmbDiagnostico.getRawValue(),
                                                                                                    idEnfermedad:idEnfermedad,
                                                                                                    detalle:gEx('detalle').getValue()
                                                                                                }
                                                                        				);
                                                                     	var grid_7875=gEx('grid_7875');
                                                                        grid_7875.getStore().add(r);
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function fichaEspecialidad(val,meta,registro)
{
	
    return '<a href=javascript:abrirFicha2(1016,-1)><img src="../images/book_open.png" title="Ficha avanzada de diagn&oacute;stico" alt="Ficha avanzada de diagn&oacute;stico"></a>';
}


function abrirFicha2(iF,iR)
{
	var arrDatos=[['idFormulario',iF],['idRegistro',iR],['cPagina','sFrm=true'],['eliminarEspacios',1],['eJs',bE('window.parent.cerrarVentanaFancy()')]];	
    
    var obj={};
    obj.params=arrDatos;
    
    obj.ancho='90%';
    obj.alto=400;
    obj.url='../modeloPerfiles/registroFormulario.php';
	abrirVentanaFancy(obj);
}