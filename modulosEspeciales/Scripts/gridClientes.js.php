<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__821_tablaDinamica,STATUS FROM _821_tablaDinamica ORDER BY status";
	$arrSituaciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT DISTINCT txtNombre,txtFuncionConstruccion FROM _842_relacionGrids g,_842_tablaDinamica r,_841_tablaDinamica cr WHERE cr.id__841_tablaDinamica=g.IdGrid AND g.idReferencia=r.cmbRol AND cmbRol IN (".$_SESSION["idRol"].")";
	$resGrid=$con->obtenerFilas($consulta);
	
?>

Ext.onReady(inicializar);

var tamPaginaAccion=10;
var arrGrid=new Array();
function inicializar()
{
	Ext.QuickTips.init();
    var grid;
    <?php 
		while($fGrid=mysql_fetch_row($resGrid))
		{
			echo "grid=".$fGrid[1]."('".$fGrid[0]."');
				arrGrid.push(grid);";
		}
	?>
}      

function crearGridClientes(t)
{
	var tamPagina=100;
	var dsDatos=[];
   	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCliente',
                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'id__813_tablaDinamica'},
                                                        {name: 'apellidoPaterno'},
                                                        {name: 'apellidoMaterno'},
                                                        {name: 'nombres'}
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'apellidoPaterno', direction: 'ASC'},
                                                groupField: 'nombres',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('apellidoPaterno', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnRegistroLlamada').disable();
                                    	proxy.baseParams.funcion=1;
                                        proxy.baseParams.idEstado=0;
                                    }
				)	          
                        
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'apellidoPaterno'},
                                                                        {type: 'string', dataIndex: 'apellidoMaterno'},
                                                                        {type: 'string', dataIndex: 'nombres'}
                                                                        /*,
                                                                        {
                                                                            type: 'list',
                                                                            dataIndex: 'status',
                                                                            options: <?php echo $arrSituaciones ?>,
                                                                            phpMode: true
                                                                        }*/
                                                                    ]
                                                    }
                                                );                              
                                                 
    
 	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Paterno',
															width:120,
															sortable:true,
															dataIndex:'apellidoPaterno'
														},
														{
															header:'Materno',
															width:120,
															sortable:true,
															dataIndex:'apellidoMaterno'
														},
														{
															header:'Nombre',
															width:180,
															sortable:true,
															dataIndex:'nombres'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:t,
                                                            id:'gridClientes',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows:true, 
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            loadMask:true,
                                                            width:700,
                                                            height:350,
                                                            renderTo:'tblClientesSinContacto',
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            ),
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnRegistroLlamada',
                                                                        	icon:'../images/telephone_go.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar llamada',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientes').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Registrar llamada';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','830'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        window.parent.abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load({
                    url: '../paginasFunciones/funcionesAccion111.php',
                    params:{
                                start:0,
                                limit:tamPagina,
                                funcion:1
                           }
                });
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnRegistroLlamada').enable();
                                                	/*
                                                	gEx('btnVerFicha').disable();
                                                	gEx('gridAcciones').getStore().load(
                                                    							{
                                                                                	url: '../paginasFunciones/funcionesAccion111.php',
                                                                                   	params:{
                                                                                    			funcion:2,
                                                                                    			idCliente:registro.get('id__813_tablaDinamica'),
                                                                                                limit:tamPaginaAccion,
                                                                                                idEstado:0,
                                                                                                start:0
	                                                                                   		}
                                                                                }
                                                    						)*/
                                                }
                                                		
								)
	return 	tblGrid;
}

function crearGridClientesVolverLlamar(t)
{
	var tamPagina=100;
	var dsDatos=[];
   	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCliente',
                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'id__813_tablaDinamica'},
                                                        {name: 'apellidoPaterno'},
                                                        {name: 'apellidoMaterno'},
                                                        {name: 'nombres'},
                                                        {name: 'fechaLlamada', type:'date'}
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'apellidoPaterno', direction: 'ASC'},
                                                groupField: 'nombres',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('fechaLlamada', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnRegistroLlamada2').disable();
                                    	proxy.baseParams.funcion=3;

                                    }
				)	          
                        
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'apellidoPaterno'},
                                                                        {type: 'string', dataIndex: 'apellidoMaterno'},
                                                                        {type: 'string', dataIndex: 'nombres'}
                                                                        /*,
                                                                        {
                                                                            type: 'list',
                                                                            dataIndex: 'status',
                                                                            options: <?php echo $arrSituaciones ?>,
                                                                            phpMode: true
                                                                        }*/
                                                                    ]
                                                    }
                                                );                              
                                                 
    
 	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Paterno',
															width:120,
															sortable:true,
															dataIndex:'apellidoPaterno'
														},
														{
															header:'Materno',
															width:120,
															sortable:true,
															dataIndex:'apellidoMaterno'
														},
														{
															header:'Nombre',
															width:180,
															sortable:true,
															dataIndex:'nombres'
														},
                                                        {
															header:'Fecha programada de llamada',
															width:180,
															sortable:true,
															dataIndex:'fechaLlamada',
                                                            renderer:formatearfecha
														},

                                                        
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:t,
                                                            id:'gridClientes2',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows:true, 
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            loadMask:true,
                                                            width:700,
                                                            height:350,
                                                            renderTo:'tblClientesVolverLlamar',
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            ),
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnRegistroLlamada2',
                                                                        	icon:'../images/telephone_go.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar llamada',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientes2').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Registrar llamada';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','830'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        window.parent.abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load({
                    url: '../paginasFunciones/funcionesAccion111.php',
                    params:{
                                start:0,
                                limit:tamPagina,
                                funcion:3
                           }
                });
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnRegistroLlamada2').enable();
                                                	/*
                                                	gEx('btnVerFicha').disable();
                                                	gEx('gridAcciones').getStore().load(
                                                    							{
                                                                                	url: '../paginasFunciones/funcionesAccion111.php',
                                                                                   	params:{
                                                                                    			funcion:2,
                                                                                    			idCliente:registro.get('id__813_tablaDinamica'),
                                                                                                limit:tamPaginaAccion,
                                                                                                idEstado:0,
                                                                                                start:0
	                                                                                   		}
                                                                                }
                                                    						)*/
                                                }
                                                		
								)
	return 	tblGrid;
}

function crearGridSeguimiento()
{
    var alDatos= new Ext.data.JsonStore({
                                            idProperty:'idAccion',
                                            totalProperty :'numReg',
                                            fields: [
                                            			{name: 'idAccion'},
                                               			{name: 'accion'},
                                                        {name: 'fechaAccion', type:'date'},
                                                        {name: 'responsable'},
                                                        {name: 'comentarios'},
                                                        {name: 'idFormulario'},
                                                        {name: 'idRegistro'}
                                            		],
                                             proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                            sortInfo: {field: 'apellidoPaterno', direction: 'ASC'},
                                            autoLoad:false,
                                            root:'registros',
                                            remoteSort: true
                                        }
                                      );
    alDatos.setDefaultSort('fechaAccion', 'DES');
    
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion=2;
                                    }
                        )
	
    var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPaginaAccion,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	{
															header:'Acci&oacute;n',
															width:700,
															sortable:true,
															dataIndex:'accion',
                                                            renderer:formatearAccion
														},
														{
															header:'Fecha',
															width:170,
															sortable:true,
															dataIndex:'fechaAccion',
                                                            renderer:formatearfecha
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	
                                                            id:'gridAcciones',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:230,
                                                            region:'south',
                                                            collapsible:true,
                                                            bbar:[paginador],
                                                            viewConfig: {
                                                                            forceFit:false,
                                                                            enableRowBody:true,
                                                                            getRowClass : formatearFila
                                                                        },
                                                            loadMask:true,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnVerFicha',
                                                                        	icon:'../images/magnifier.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Ver ficha',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=650;
                                                                                        conf.modal=true;
                                                                                        conf.alto=400;
                                                                                        conf.titulo='';
                                                                                        conf.url='../modeloPerfiles/verFichaFormulario.php';
                                                                                        var arrParam=[['idRegistro',fila.get('idRegistro')],['idFormulario',fila.get('idFormulario')],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        },
                                                                        {
                                                                        	id:'btnRegistroLlamada',
                                                                        	icon:'../images/telephone_go.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar llamada',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientes').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Registrar llamada';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','830'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                            		]

                                                        }
                                                    );

	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnVerFicha').enable();

                                                }
									);                                                
	                                                 
	return 	tblGrid;
}

function formatearfecha(date) 
{
    if (!date) 
    {
        return '';
    }
    var now = new Date();
    var d = now.clearTime(true);
    var notime = date.clearTime(true).getTime();
    if (notime == d.getTime()) 
    {
        return 'Hoy ' + date.dateFormat('g:i a');
    }
   
   	
    return date.dateFormat('d/m/Y g:i a');
}

function formatearAccion(value, p, record) 
{
	return String.format(
                '<img src="../images/icon_comment.gif">&nbsp;<span class="corpo8_bold">{0}</span> (<span class="copyrigthSinPadding">{1}</span>)',
                	value, record.data.responsable
                );
}

function formatearFila(record, rowIndex, p, ds) 
{
	var xf = Ext.util.Format;
    p.body = '<p style="margin-left: 3em"><span class="letraExt">&nbsp;' + xf.ellipsis(xf.stripTags(record.data.comentarios), 200) + '</span></p>';
    return 'x-grid3-row-expanded';
        /*if (this.showPreview) {
            var xf = Ext.util.Format;
            p.body = '<p>' + xf.ellipsis(xf.stripTags(record.data.description), 200) + '</p>';
            return 'x-grid3-row-expanded';
        }
        return 'x-grid3-row-collapsed';*/
}

function recargarGridAccion()
{
	var registro=gEx('gridClientes').getSelectionModel().getSelected();
	gEx('btnVerFicha').disable();
    gEx('gridAcciones').getStore().load(
                                {
                                    url: '../paginasFunciones/funcionesAccion111.php',
                                    params:{
                                                funcion:2,
                                                idCliente:registro.get('id__813_tablaDinamica'),
                                                limit:tamPaginaAccion,
                                                start:0
                                            }
                                }
                            )	
}

function recargarGridClientes()
{
	gEx('gridClientes').getStore().reload();
}

function recargarGrids()
{
	var x;
    for(x=0;x<arrGrid.length;x++)
    {
    	arrGrid[x].getStore().reload();
    }
}

function crearGridClientesEsperaPromotor(t)
{
	var tamPagina=100;
	var dsDatos=[];
   	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCliente',
                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'id__813_tablaDinamica'},
                                                        {name: 'apellidoPaterno'},
                                                        {name: 'apellidoMaterno'},
                                                        {name: 'nombres'},
                                                        {name: 'fechaCita', type:'date'}
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'apellidoPaterno', direction: 'ASC'},
                                                groupField: 'nombres',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('apellidoPaterno', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnRegistroPromotor').disable();
                                    	proxy.baseParams.funcion=5;
                                        proxy.baseParams.idEstado=6;
                                    }
				)	          
                        
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'apellidoPaterno'},
                                                                        {type: 'string', dataIndex: 'apellidoMaterno'},
                                                                        {type: 'string', dataIndex: 'nombres'}
                                                                        /*,
                                                                        {
                                                                            type: 'list',
                                                                            dataIndex: 'status',
                                                                            options: <?php echo $arrSituaciones ?>,
                                                                            phpMode: true
                                                                        }*/
                                                                    ]
                                                    }
                                                );                              
                                                 
    
 	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Paterno',
															width:120,
															sortable:true,
															dataIndex:'apellidoPaterno'
														},
														{
															header:'Materno',
															width:120,
															sortable:true,
															dataIndex:'apellidoMaterno'
														},
														{
															header:'Nombre',
															width:180,
															sortable:true,
															dataIndex:'nombres'
														},
                                                        {
															header:'Fecha programada de cita',
															width:180,
															sortable:true,
															dataIndex:'fechaCita',
                                                            renderer:formatearfecha
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:t,
                                                            id:'gridClientesPromotor',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows:true, 
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            loadMask:true,
                                                            width:700,
                                                            height:350,
                                                            renderTo:'tblClientesSinPromotor',
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            ),
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnRegistroPromotor',
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Asignar Ejecutivo',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientesPromotor').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Asignar Ejecutivo';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','833'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        window.parent.abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load({
                    url: '../paginasFunciones/funcionesAccion111.php',
                    params:{
                                start:0,
                                limit:tamPagina,
                                idEstado:6,
                                funcion:5
                           }
                });
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnRegistroPromotor').enable();
                                                	/*
                                                	gEx('btnVerFicha').disable();
                                                	gEx('gridAcciones').getStore().load(
                                                    							{
                                                                                	url: '../paginasFunciones/funcionesAccion111.php',
                                                                                   	params:{
                                                                                    			funcion:2,
                                                                                    			idCliente:registro.get('id__813_tablaDinamica'),
                                                                                                limit:tamPaginaAccion,
                                                                                                idEstado:0,
                                                                                                start:0
	                                                                                   		}
                                                                                }
                                                    						)*/
                                                }
                                                		
								)
	return 	tblGrid;
}

function crearGridClientesEsperaCita(t)
{
	var tamPagina=100;
	var dsDatos=[];
   	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCliente',
                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'id__813_tablaDinamica'},
                                                        {name: 'apellidoPaterno'},
                                                        {name: 'apellidoMaterno'},
                                                        {name: 'nombres'},
                                                        {name: 'status'}
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'status', direction: 'ASC'},
                                                groupField: 'nombres',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('apellidoPaterno', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnRegistroCita').disable();
                                    	proxy.baseParams.funcion=4;
                                       
                                    }
				)	          
                        
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'apellidoPaterno'},
                                                                        {type: 'string', dataIndex: 'apellidoMaterno'},
                                                                        {type: 'string', dataIndex: 'nombres'}
                                                                        ,
                                                                        {
                                                                            type: 'list',
                                                                            dataIndex: 'status',
                                                                            options: <?php echo $arrSituaciones ?>,
                                                                            phpMode: true
                                                                        }
                                                                    ]
                                                    }
                                                );                              
                                                 
    
 	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Paterno',
															width:120,
															sortable:true,
															dataIndex:'apellidoPaterno'
														},
														{
															header:'Materno',
															width:120,
															sortable:true,
															dataIndex:'apellidoMaterno'
														},
														{
															header:'Nombre',
															width:180,
															sortable:true,
															dataIndex:'nombres'
														},
                                                        {
															header:'Situaci&oacute;n',
															width:220,
															sortable:true,
															dataIndex:'status'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:t,
                                                            id:'gridClientesEsperaCita',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows:true, 
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            loadMask:true,
                                                            width:700,
                                                            height:350,
                                                            renderTo:'tblClientesSinCita',
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            ),
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnRegistroCita',
                                                                        	icon:'../images/report.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Asignar cita',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientesEsperaCita').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Registrar cita';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','834'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        window.parent.abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load({
                    url: '../paginasFunciones/funcionesAccion111.php',
                    params:{
                                start:0,
                                limit:tamPagina,
                                funcion:4
                           }
                });
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnRegistroCita').enable();
                                                	
                                                }
                                                		
								)
	return 	tblGrid;
}

function crearGridClientesCita(t)
{
	var tamPagina=100;
	var dsDatos=[];
   	var lector= new Ext.data.JsonReader({
                                            idProperty:'idCliente',
                                            totalProperty :'numReg',
                                            fields: [
                                               			{name: 'id__813_tablaDinamica'},
                                                        {name: 'apellidoPaterno'},
                                                        {name: 'apellidoMaterno'},
                                                        {name: 'nombres'},
                                                        {name: 'fechaCita', type:'date'},
                                                        {name: 'ejecutivo'}
                                                        
                                            ],
                                            root:'registros',
                                            remoteGroup:false,
                                            remoteSort: true
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesAccion111.php'
                                                                                      
                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'apellidoPaterno', direction: 'ASC'},
                                                groupField: 'nombres',
                                                autoLoad:false,
                                                remoteSort: true
                                            })     

	alDatos.setDefaultSort('fechaCita', 'ASC');

	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnRegistroResultadoCita').disable();
                                    	proxy.baseParams.funcion=6;

                                    }
				)	          
                        
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'string', dataIndex: 'apellidoPaterno'},
                                                                        {type: 'string', dataIndex: 'apellidoMaterno'},
                                                                        {type: 'string', dataIndex: 'nombres'}
                                                                        /*,
                                                                        {
                                                                            type: 'list',
                                                                            dataIndex: 'status',
                                                                            options: <?php echo $arrSituaciones ?>,
                                                                            phpMode: true
                                                                        }*/
                                                                    ]
                                                    }
                                                );                              
                                                 
    
 	var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               ) 
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Paterno',
															width:120,
															sortable:true,
															dataIndex:'apellidoPaterno'
														},
														{
															header:'Materno',
															width:120,
															sortable:true,
															dataIndex:'apellidoMaterno'
														},
														{
															header:'Nombre',
															width:180,
															sortable:true,
															dataIndex:'nombres'
														},
                                                        {
															header:'Fecha programada de cita',
															width:180,
															sortable:true,
															dataIndex:'fechaCita',
                                                            renderer:formatearfecha
														},
                                                        {
															header:'Ejecutivo',
															width:250,
															sortable:true,
															dataIndex:'ejecutivo'
														}

                                                        
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                        	title:t,
                                                            id:'gridClientesCita',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows:true, 
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            loadMask:true,
                                                            width:700,
                                                            height:350,
                                                            renderTo:'tblClientesConCita',
                                                            view: new Ext.grid.GroupingView(	{
                                                                                                    forceFit:false,
                                                                                                    showGroupName: true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    enableGrouping:false,
                                                                                                    hideGroupedColumn: false
                                                                                            	}   
                                                                                            ),
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnRegistroResultadoCita',
                                                                        	icon:'../images/page_edit2.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Registrar resultado de cita',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=gEx('gridClientesCita').getSelectionModel().getSelected();
                                                                                    	var conf={};
                                                                                    	conf.ancho=900;
                                                                                        conf.modal=true;
                                                                                        conf.alto=480;
                                                                                        conf.titulo='Registrar resultado de cita';
                                                                                        conf.url='../modeloPerfiles/registroFormulario.php';
                                                                                        var arrParam=[['idReferencia',fila.get('id__813_tablaDinamica')],['ignoraPermisos','1'],['idRegistro','-1'],['idFormulario','832'],['cPagina','sFrm=true'],['accionCancelar','window.parent.cerrarVentanaFancy()']];
                                                                                        conf.params=arrParam;
                                                                                        window.parent.abrirVentanaFancy(conf);
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	alDatos.load({
                    url: '../paginasFunciones/funcionesAccion111.php',
                    params:{
                                start:0,
                                limit:tamPagina,
                                funcion:6
                           }
                });
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
    											{
                                                	gEx('btnRegistroResultadoCita').enable();
                                                	
                                                }
                                                		
								)
	return 	tblGrid;
}


