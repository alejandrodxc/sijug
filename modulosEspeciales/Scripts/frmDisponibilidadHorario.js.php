<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$iR=bD($_GET["iR"]);
	
	$consulta="SELECT id__1026_tablaDinamica,nombreCiclo,c.descripcion,i.idEstado,c.id__1025_tablaDinamica,c.cicloEscolar,i.idConvocatoria FROM _1026_tablaDinamica i,_1025_tablaDinamica c,4526_ciclosEscolares ce 
				WHERE id__1026_tablaDinamica=".$iR." AND c.id__1025_tablaDinamica=i.idConvocatoria and ce.idCiclo=c.cicloEscolar";
	$fConvocatoria=$con->obtenerPrimeraFila($consulta);
	
	
	$consulta="SELECT idOpcion FROM _1025_periodoEscolar WHERE idPadre=".$fConvocatoria[4];

	$lPeriodos=$con->obtenerListaValores($consulta);
	if($lPeriodos=="")
		$lPeriodos-1;
	
	
	$arrPlanesDisponibles=array();
	
	$consulta="SELECT i.idInstanciaPlanEstudio,i.nombrePlanEstudios,n.txtNivelEstudio,pe.nombreProgramaEducativo,p.nivelPlanEstudio,p.idProgramaEducativo
				FROM 4513_instanciaPlanEstudio i,4500_planEstudio p, _401_tablaDinamica n,4500_programasEducativos pe
				WHERE p.idPlanEstudio=i.idPlanEstudio AND  i.situacion=1 AND n.id__401_tablaDinamica=p.nivelPlanEstudio 
				AND pe.idProgramaEducativo=p.idProgramaEducativo ORDER BY i.nombrePlanEstudios";


	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$arrGradosFinal=obtenerGradosAperturaCiclo($fConvocatoria[5],$lPeriodos,$fila[0]);
		if(sizeof($arrGradosFinal)>0)
		{
			if(!isset($arrPlanesDisponibles[$fila[2]."_".$fila[4]]))
				$arrPlanesDisponibles[$fila[2]."_".$fila[4]]=array();
			
			if(!isset($arrPlanesDisponibles[$fila[2]."_".$fila[4]][$fila[3]."_".$fila[5]]))
				$arrPlanesDisponibles[$fila[2]."_".$fila[4]][$fila[3]."_".$fila[5]]=array();
			
			$o["idInstancia"]=$fila[0];
			$o["lblInstancia"]=$fila[1];
			array_push($arrPlanesDisponibles[$fila[2]."_".$fila[4]][$fila[3]."_".$fila[5]],$o);
		}
	}
	
	
	$arrPlanes="";
	foreach($arrPlanesDisponibles as $nivelAcademico=>$resto)
	{
		$arrNivel=explode("_",$nivelAcademico);
		$oPlan="['".$arrNivel[1]."','".cv($arrNivel[0])."'";
		$arrFacultades="";
		foreach($resto as $facultad=>$planes)
		{
			$arrFacultad=explode("_",$facultad);
			$oFacultad="['".$arrFacultad[1]."','".cv($arrFacultad[0])."'";
			
			$arrInstanciaPlan="";
			foreach($planes as $plan)
			{
				
				$oInstancia="['".$plan["idInstancia"]."','".cv($plan["lblInstancia"])."']";
				if($arrInstanciaPlan=="")
					$arrInstanciaPlan=$oInstancia;
				else
					$arrInstanciaPlan.=",".$oInstancia;
				
				
			}	
			$oFacultad.=",[".$arrInstanciaPlan."]]";
			if($arrFacultades=="")
				$arrFacultades=$oFacultad;
			else
				$arrFacultades.=",".$oFacultad;
		}
		$oPlan.=",[".$arrFacultades."]]";
		if($arrPlanes=="")
			$arrPlanes=$oPlan;
		else
			$arrPlanes.=",".$oPlan;
	}
	
	
	
	$lblConvocatoria="";
	switch($fConvocatoria[3])
	{
		case 1:
			$lblConvocatoria="En diseño";
		break;
		case 2:
			$lblConvocatoria="Liberado";
		break;
		
	}	
?>


var urlPagina='../reportes/ives/academico/disponibilidadHorario.php';
var condicionesAceptadas=0;
var arrPlanes=[<?php echo $arrPlanes?>];
var rO=0;

Ext.onReady(inicializar);

function inicializar()
{
	
	var fechaInicial=new Date(2011,5,6);
	Ext.QuickTips.init();
	var rO=gE('rO').value;
    
    if((gE('mCondicionesUso').value=='0')||(gE('condicionesAceptadas').value=='1'))
    	condicionesAceptadas=1;
        
    var arrPaneles=	[
    					
    					{
                          xtype:'panel',
                          layout:'border',
                          border:false,
                          id:'rDisponibilidad',
                          title:'Registro de disponibilidad de horario',
                          items:	[
  
                                      {
                                          xtype:'panel',
                                          width:320,
                                          hidden:(gE('mPeriodos').value=='0'),
                                          layout:'border',
                                          region:'west',
                                          tbar:	[
                                                      {
                                                          xtype:'label',
                                                          html:'<span style="color:#000"><b>Acciones: </b></span>'
                                                          
                                                      }
                                                      ,'-',
                                                      {
                                                          icon:'../images/cancel_round.png',
                                                          cls:'x-btn-text-icon',
                                                          id:'btnNoDisponibilidad',
                                                          disabled:true,
                                                          hidden:((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0')),
                                                          tooltip:'Marcar NO disponibilidad para el periodo seleccionado',
                                                          handler:function()
                                                                  {
                                                                      function resp(btn)
                                                                      {
                                                                          if(btn=='yes')
                                                                          {
                                                                              function funcAjax()
                                                                              {
                                                                                  var resp=peticion_http.responseText;
                                                                                  arrResp=resp.split('|');
                                                                                  if(arrResp[0]=='1')
                                                                                  {
                                                                                      gEx('gPeriodosRegistro').getStore().reload();
                                                                                  }
                                                                                  else
                                                                                  {
                                                                                      msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                  }
                                                                              }
                                                                              obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=41&c='+gE('ciclo').value+'&iU='+gE('idUsuario').value+'&iP='+gE('idPeriodo').value+'&iR='+gE('idRegistro').value,true);
                                                                          }
                                                                      }
                                                                      msgConfirm('Est&aacute; seguro de querer indicar la <b>NO disponibilidad de horario</b> para el periodo seleccionado?<br />(Esto eliminar&aacute; cualquier horario registrado previamente en el periodo seleccionado)',resp);
                                                                      
                                                                  }
                                                          
                                                      }
                                                      ,'-',
                                                      {
                                                          icon:'../images/accept_green.png',
                                                          cls:'x-btn-text-icon',
                                                          id:'btnRemoverNODisponibilidad',
                                                          disabled:true,
                                                          hidden:((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0')),
                                                          tooltip:'Remover marca de NO disponibilidad para el periodo seleccionado',
                                                          handler:function()
                                                                  {
                                                                      function resp(btn)
                                                                      {
                                                                          if(btn=='yes')
                                                                          {
                                                                              function funcAjax()
                                                                              {
                                                                                  var resp=peticion_http.responseText;
                                                                                  arrResp=resp.split('|');
                                                                                  if(arrResp[0]=='1')
                                                                                  {
                                                                                      gEx('gPeriodosRegistro').getStore().reload();
                                                                                  }
                                                                                  else
                                                                                  {
                                                                                      msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                  }
                                                                              }
                                                                              obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=42&iP='+gE('idPeriodo').value+'&iR='+gE('idRegistro').value,true);
                                                                          }
                                                                      }
                                                                      msgConfirm('Est&aacute; seguro de querer remover la marca de <b>NO disponibilidad de horario</b> para el periodo seleccionado?',resp);
                                                                      
                                                                  }
                                                          
                                                      }
                                                      ,'-',
                                                      {
                                                          icon:'../images/page_white_stack.png',
                                                          cls:'x-btn-text-icon',
                                                          disabled:true,
                                                          id:'btnClonar',
                                                          tooltip:'Clonar disponibilidad sobre el periodo seleccionado',
                                                          hidden:((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0')),
                                                          handler:function()
                                                                  {
                                                                      var pos=obtenerPosFila(gEx('gPeriodosRegistro').getStore(),'idPeriodo',gE('idPeriodo').value);
                                                                      
                                                                      var fila=gEx('gPeriodosRegistro').getStore().getAt(pos);
                                                                      mostrarVentanaClonar(fila); 
                                                                  }
                                                          
                                                      }
                                                      ,'-',
                                                      {
                                                          icon:'../images/icon_big_tick.gif',
                                                          cls:'x-btn-text-icon',
                                                          id:'btnDisponibilidad',
                                                          disabled:true,
                                                          hidden:(parseFloat(gE('idEstado').value)==2),
                                                          tooltip:'Liberar disponibilidad de horario',
                                                          handler:function()
                                                                  {
                                                                      function resp(btn)
                                                                      {
                                                                          if(btn=='yes')
                                                                          {
                                                                          	  if(gEx('arbolMateriasInteres')&&(gEx('arbolMateriasInteres').getRootNode().childNodes.length==0))
                                                                              {
                                                                                  function respFinal(btn2)
                                                                                  {
                                                                                      if(btn2=='yes')
                                                                                      {
                                                                                          liberarDisponibilidad();
                                                                                      }
                                                                                  }
                                                                                  msgConfirm('No ha definido alguna materia de inter&eacute;s, est&aacute; seguro de continuar con la finalizaci&oacute;n del registro de horario?',respFinal);
                                                                                  
                                                                              }
                                                                              else
                                                                                  liberarDisponibilidad();
                                                                          }
                                                                      }
                                                                      msgConfirm('Est&aacute; seguro de querer liberar su registro de disponibilidad de horario?',resp);
                                                                  }
                                                          
                                                      }	
                                                  ],
                                          title:'<span style="color:#000">Periodos registrados</span>',
                                          collapsible:true,
                                          items:	[
                                                      	crearGridPeriodos()
                                                  	]
                                      },
                                      {
                                          xtype:'panel',
                                          region:'center',
                                          border:false,
                                          layout: 'border',
                                          items:	[
                                                      	crearPanelHorario()
                                                  	]
                                      }
                                 ]
                      }
    				];
                    
                    
                    
                    
    if(gE('mMateriasInteres').value=='1')
    {
    	arrPaneles.push	(
        					{
                              xtype:'panel',
                              layout:'border',
                              border:false,
                              title:'Selecci&oacute;n de materias de inter&eacute;s',
                              items:	[
                                          /*{
                                              xtype:'panel',
                                              region:'west',
                                              layout:'border',
                                              hidden:((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0')),
                                              title:'<span style="color:#000">Materias ofertadas</span>',
                                              width:'50%',
                                              items:	[
                                                          crearGridMaterias(1)
                                                      ]
                                          },
                                          {
                                              xtype:'panel',
                                              layout:'border',
                                              title:'<span style="color:#000">Materias de inter&eacute;s para impartir como profesor</span>',
                                              region:'center',
                                              items:	[
                                                          crearGridMaterias(2)
                                                      ]
                                          }*/
                                          
                                          crearGridMateriasInteres(),
                                          crearPanelGridSeleccionMateria()
                                          
                                      ]
                          }	
        				)
    }
    
    
    	
    
    
    if(gE('mCondicionesUso').value=='1')
    {
    
    	var arrBBar=[];
        if(condicionesAceptadas==0)
        {
        	arrPaneles=[];
        	arrBBar=	[
                            {
                                xtype:'checkbox',
                                id:'chkAceptacion',
                                listeners:	{
                                				check:function(chk,valor)
                                                		{
                                                        	if(valor)
                                                            	gEx('btnContinuar').enable();
                                                            else
                                                            	gEx('btnContinuar').disable();
                                                        }
                                			},
                                boxLabel:'<span style="color:#900; font-weight:bold">He le&iacute;do y estoy de acuerdo con las condiciones descritas en la secci&oacute;n anterior</span>&nbsp;&nbsp;'	             
                            },'-',
                            {
                                icon:'../images/icon_big_tick.gif',
                                cls:'x-btn-text-icon',
                                text:'Continuar >>',
                                id:'btnContinuar',
                                disabled:true,
                                handler:function()
                                        {
                                        	
                                        	
                                        	var frameContenido=gEx('frameContenido');

                                            var contenido=frameContenido.getFrameWindow().document.getElementById('bEContenido').value;
                                            
                                        	function funcAjax()
                                            {
                                                var resp=peticion_http.responseText;
                                                arrResp=resp.split('|');
                                                if(arrResp[0]=='1')
                                                {
                                                    recargarPagina();
                                                }
                                                else
                                                {
                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                }
                                            }
                                            obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=57&contenido='+(contenido)+'&idRegistro='+gE('idRegistro').value,true);
                                               
                                        }
                                
                            }
                        ]	
        }
    	
        
    
    	
    
    	arrPaneles.splice	(0,0,{
                                      xtype:'panel',
                                      layout:'border',
                                      border:false,
                                      id:'rCondiciones',
                                      title:'Aceptaci&oacute;n de condiciones de uso',
                                      bbar:	arrBBar,
                                      items:	[
                                                  	new Ext.ux.IFrameComponent({ 
    
                                                                                      id: 'frameContenido', 
                                                                                      anchor:'100% 100%',
                                                                                      region:'center',
                                                                                      loadFuncion:function(iFrame)
                                                                                                  {
                                                                                                    /*  window.scrollTo(0,150);
                                                                                                      autofitIframe(iFrame);*/
                                                                                                  },

                                                                                      url: '../paginasFunciones/white.php',
                                                                                      style: 'width:100%;height:100%' 
                                                                              })
                                              	]
                                  }	
                            );
    }
    
    
    
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                tbar:	[
                                                            {
                                                                icon:'../images/icon_big_tick.gif',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnDisponibilidad2',
                                                                hidden:((parseFloat(gE('idEstado').value)=='2')||(gE('rO').value=='1')||(gE('mPeriodos').value=='1')||(condicionesAceptadas==0)),
                                                                text:'Liberar disponibilidad de horario',
                                                                handler:function()
                                                                        {
                                                                            function resp(btn)
                                                                            {
                                                                                if(btn=='yes')
                                                                                {
                                                                                    
                                                                                    if(gEx('calendario').eventStore.getCount()==0)
                                                                                    {
                                                                                        function respFinal3(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                if(gEx('arbolMateriasInteres')&&(gEx('arbolMateriasInteres').getRootNode().childNodes.length==0))
                                                                                                {
                                                                                                    function respFinal(btn2)
                                                                                                    {
                                                                                                        if(btn2=='yes')
                                                                                                        {
                                                                                                            liberarDisponibilidad();
                                                                                                        }
                                                                                                    }
                                                                                                    msgConfirm('No ha definido alguna materia de inter&eacute;s, est&aacute; seguro de continuar con la finalizaci&oacute;n del registro de horario?',respFinal);
                                                                                                    
                                                                                                }
                                                                                                else
                                                                                                    liberarDisponibilidad();    
                                                                                            }
                                                                                            
                                                                                        }
                                                                                        msgConfirm('No ha ingresado alg&uacute;n bloque de disponibilidad de horario, est&aacute; seguro de continuar con la finalizaci&oacute;n del registro de horario?',respFinal3);
                                                                                        return;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    
                                                                                    
                                                                                        if(gEx('arbolMateriasInteres')&&(gEx('arbolMateriasInteres').getRootNode().childNodes.length==0))
                                                                                        {
                                                                                            function respFinal(btn2)
                                                                                            {
                                                                                                if(btn2=='yes')
                                                                                                {
                                                                                                    liberarDisponibilidad();
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('No ha definido alguna materia de inter&eacute;s, est&aacute; seguro de continuar con la finalizaci&oacute;n del registro de horario?',respFinal);
                                                                                            
                                                                                        }
                                                                                        else
                                                                                            liberarDisponibilidad();
                                                                                    }
                                                                                }
                                                                            }
                                                                            msgConfirm('Est&aacute; seguro de querer liberar su registro de disponibilidad de horario?',resp);
                                                                        }
                                                                
                                                            },
                                                            {
                                                                icon:'../images/document_go.png',
                                                                cls:'x-btn-text-icon',
                                                                id:'btnDescargarFormato',
                                                                hidden:(parseFloat(gE('idEstado').value)!='2'),
                                                                text:'Descargar formato de disponibilidad de horario',
                                                                handler:function()
                                                                        {
                                                                            window.parent.invocarEjecucionFuncionContenedor('imprimirFormato',"'"+urlPagina+"','"+gE('idRegistro').value+"'");
                                                                        }
                                                                
                                                            }
                                                        ],
                                                title: '<span class="letraRojaSubrayada8" style="font-size:12px"><b>Registro de disponibilidad de horario (<span style="color:#000"><?php echo $lblConvocatoria?></span>)</b></span> [<span style="color:#000"><b>Ciclo:</b></span> <span style="color:#900"><?php echo $fConvocatoria[1]?></span>'+
                                                		'<span style="color:#000">, <b>Convocatoria:</b> <span style="font-weight:normal" title="<?php echo cv($fConvocatoria[2])?>" alt="<?php echo cv($fConvocatoria[2])?>">'+Ext.util.Format.ellipsis('<?php echo $fConvocatoria[2]?>',115)+'</span></span>]',
                                                
                                                items:	[
                                                			{
                                                            	xtype:'tabpanel',
                                                                region:'center',
                                                                activeTab:(arrPaneles.length==1)?0:1,
                                                                border:false,
                                                                listeners:	{
                                                                				tabchange:function(t,tab)
                                                                                		{
                                                                                        	if((tab.id=='rCondiciones')&&(!tab.visto))
                                                                                            {
                                                                                            	gEx('frameContenido').load	(	
                                                                                                                                {
                                                                                                                                    url:'../modulosEspeciales/vistaConvocatoria.php',
                                                                                                                                    params:	{
                                                                                                                                                idRegistro:gE('idRegistro').value,
                                                                                                                                                idConvocatoria:gE('idConvocatoria').value
                                                                                                                                            }
                                                                                                                                            
                                                                                                                                            
                                                                                                                                }
                                                                                                                            )
                                                                                                                            
                                                                                            	tab.visto=true;                              
                                                                                            }
                                                                                        	if((tab.id=='rDisponibilidad')&&(!tab.visto))
                                                                                            {
                                                                                            	gEx('calendario').setStartDate(fechaInicial); 	
                                                                                                tab.visto=true;
                                                                                            }
                                                                                        }
                                                                			},
                                                                items:	arrPaneles
                                                            
                                                        	}
                                                        ]
                                            }
                                         ]
                            }
                        )   
                        
	
                        
}

function crearGridMateriasInteres()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'56'
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'
                                                }
                                            )		
										
											

	cargadorArbol.on('beforeload',function(proxy)
    								{
                                        proxy.baseParams.iC=<?php echo $fConvocatoria[6]?>;
                                        proxy.baseParams.iU=gE('idUsuario').value;
                                        proxy.baseParams.sL=gE('rO').value
                                    }
    				)

										
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															x:10,
															y:40,
                                                            border:false,
															id:'arbolMateriasInteres',
															region:'center',
															width:550,
															useArrows:true,
															autoScroll:true,
															animate:true,
															enableDD:true,
															containerScroll: true,
															root:raiz,
                                                            title:'<span style="color:#000">Materias seleccionadas como de inter&eacute;s para se impartidas por el profesor:</span>',
															loader: cargadorArbol,
															rootVisible:false
															
															
														}
													)
	return 	arbolOpciones;		
}

function recargarMateriasInteres()
{
	gEx('arbolMateriasOferta').getRootNode().reload();
}

function crearPanelGridSeleccionMateria()
{
	var cmbNivelEducativo=crearComboExt('cmbNivelEducativo',arrPlanes,180,15,350);
    cmbNivelEducativo.on('select',function(cmb,registro)
    								{
                                    	gEx('cmbFacultad').reset();

                                    	gEx('cmbFacultad').getStore().loadData(registro.data.valorComp);
                                        gEx('arbolMateriasOferta').getRootNode().removeAll();
                                    }
    					)
    var cmbFacultad=crearComboExt('cmbFacultad',[],180,45,450);
    cmbFacultad.on('select',function(cmb,registro)
    								{
                                    	gEx('cmbPlanEstudio').setValue('');
                                    	gEx('cmbPlanEstudio').getStore().loadData(registro.data.valorComp);
                                        gEx('arbolMateriasOferta').getRootNode().removeAll();
                                    }
    					)
    var cmbPlanEstudio=crearComboExt('cmbPlanEstudio',[],180,75,450);
    cmbPlanEstudio.on('select',function(cmb,registro)
    								{
                                    	recargarMateriasInteres();
                                    }
    					)
    
	var tabPanel=new Ext.Panel	(	
    									{
                                            region:'east',
                                            width:'50%',
                                            baseCls: 'x-plain',
                                            border:false,
                                            collapsible:true,
                                            layout:'border',
                                            hidden:(gE('rO').value=='1'),
                                            title:'<br>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#000; font-size:11px"><b>Selecci&oacute;n de materias de inter&eacute;s para se impartidas por el profesor:</b></span>',
                                            items:	[
                                            			{
                                                        	xtype:'panel',
                                                            layout:'absolute',
                                                            baseCls: 'x-plain',
                                                            region:'center',
                                                            items:	[
                                                            			{
                                                                        	x:20,
                                                                            y:20,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000">Nivel educativo:</span>'
                                                                        },
                                                                        cmbNivelEducativo,
                                                                        {
                                                                        	x:20,
                                                                            y:50,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000">Facultad/&Aacute;rea acad&eacute;mica:</span>'
                                                                        },
                                                                        cmbFacultad,
                                                                        {
                                                                        	x:20,
                                                                            y:80,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000">Plan de estudios:</span>'
                                                                        },
                                                                        cmbPlanEstudio,
                                                                        crearArbolMaterias(),
                                                                        {
                                                                        	xtype:'panel',
                                                                            x:20,
                                                                            y:180,
                                                                            baseCls: 'x-plain',
                                                                            width:100,
                                                                            height:30,
                                                                            items:	[
                                                                            			{
                                                                                            icon:'../images/right1.png',
                                                                                            xtype:'button',
                                                                                            width:80,
                                                                                            x:10,
                                                                                            cls:'x-btn-text-icon',
                                                                                            handler:function()
                                                                                                    {
                                                                                                     	var arbolMateriasInteres=gEx('arbolMateriasInteres');
                                                                                                        var arrNodos=arbolMateriasInteres.getChecked();
                                                                                                        if(arrNodos.length==0)
                                                                                                        {
                                                                                                        	msgBox('Debe seleccionar la materia que desea remover como de su inter&eacute;s');
                                                                                                        	return;
                                                                                                        }
                                                                                                        
                                                                                                        var nodo;
                                                                                                        var x;
                                                                                                        var arrMaterias='';
                                                                                                        var oMateria='';
                                                                                                        for(x=0;x<arrNodos.length;x++)
                                                                                                        {
                                                                                                        	nodo=arrNodos[x];
                                                                                                            oMateria='{"idMateria":"'+nodo.id+'","idInstancia":"'+nodo.attributes.idInstancia+'"}';
                                                                                                            if(arrMaterias=='')
                                                                                                            	arrMaterias=oMateria;
                                                                                                            else
                                                                                                            	arrMaterias+=','+oMateria;
                                                                                                            
                                                                                                        }
                                                                                                        
                                                                                                        var cadObj='{"idUsuario":"'+gE('idUsuario').value+'","arrMaterias":['+arrMaterias+
                                                                                                        			'],"idConvocatoria":"<?php echo $fConvocatoria[6]?>"}';   
                                                                                                                    
                                                                                                                    
                                                                                                        function funcAjax()
                                                                                                        {
                                                                                                            var resp=peticion_http.responseText;
                                                                                                            arrResp=resp.split('|');
                                                                                                            if(arrResp[0]=='1')
                                                                                                            {
                                                                                                                var x;
                                                                                                                var arrMaterias='';
                                                                                                                for(x=0;x<arrNodos.length;x++)
                                                                                                                {
                                                                                                                	arrNodos[x].remove();
                                                                                                                }
                                                                                                                
                                                                                                                gEx('arbolMateriasOferta').getRootNode().reload();
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            }
                                                                                                        }
                                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=55&cadObj='+cadObj,true);
                                                                                                                    
                                                                                                                    
                                                                                                    }
                                                                                            
                                                                                        }
                                                                            		]
                                                                        },
                                                                        {
                                                                        	xtype:'panel',
                                                                            x:20,
                                                                            y:215,
                                                                            baseCls: 'x-plain',
                                                                            width:100,
                                                                            height:30,
                                                                            items:	[
                                                                            			{
                                                                                            icon:'../images/left1.png',
                                                                                            xtype:'button',
                                                                                            width:80,
                                                                                            x:10,
                                                                                            cls:'x-btn-text-icon',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        var arbolMateriasOferta=gEx('arbolMateriasOferta');
                                                                                                        var arrNodos=arbolMateriasOferta.getChecked();
                                                                                                        if(arrNodos.length==0)
                                                                                                        {
                                                                                                        	msgBox('Debe seleccionar la materia que desea a gregar como de su inter&eacute;s');
                                                                                                        	return;
                                                                                                        }
                                                                                                        
                                                                                                        var nodo;
                                                                                                        var x;
                                                                                                        var arrMaterias='';
                                                                                                        for(x=0;x<arrNodos.length;x++)
                                                                                                        {
                                                                                                        	nodo=arrNodos[x];
                                                                                                            if(arrMaterias=='')
                                                                                                            	arrMaterias=nodo.id;
                                                                                                            else
                                                                                                            	arrMaterias+=','+nodo.id;
                                                                                                            
                                                                                                        }
                                                                                                        
                                                                                                        var cadObj='{"idUsuario":"'+gE('idUsuario').value+'","arrMaterias":"'+arrMaterias+
                                                                                                        			'","idConvocatoria":"<?php echo $fConvocatoria[6]?>","idInstancia":"'+cmbPlanEstudio.getValue()+'"}';   
                                                                                                                    
                                                                                                                    
                                                                                                        function funcAjax()
                                                                                                        {
                                                                                                            var resp=peticion_http.responseText;
                                                                                                            arrResp=resp.split('|');
                                                                                                            if(arrResp[0]=='1')
                                                                                                            {
                                                                                                                var x;
                                                                                                                var arrMaterias='';
                                                                                                                for(x=0;x<arrNodos.length;x++)
                                                                                                                {
                                                                                                                	arrNodos[x].remove();
                                                                                                                }
                                                                                                                
                                                                                                                gEx('arbolMateriasInteres').getRootNode().reload();
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            }
                                                                                                        }
                                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=54&cadObj='+cadObj,true);
                                                                                                    }
                                                                                            
                                                                                        }
                                                                            		]
                                                                        }
                                                            		]
                                                        }
                                            		]
                                    	
                                        }
    								)
	return tabPanel;                                    
}

function crearArbolMaterias()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1Materia',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	
										
	
    var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'53'
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'
                                                }
                                            )		
										
											

	cargadorArbol.on('beforeload',function(proxy)
    								{
                                    	
                                    	proxy.baseParams.idInstanciaPlan=((gEx('cmbPlanEstudio')!=null)?gEx('cmbPlanEstudio').getValue():'-1');
                                        proxy.baseParams.p='<?php echo $lPeriodos?>';
                                        proxy.baseParams.c=<?php echo $fConvocatoria[5]?>;
                                        proxy.baseParams.iU=gE('idUsuario').value;
                                        proxy.baseParams.iC=<?php echo $fConvocatoria[6]?>;
                                        
                                        
                                       
                                        
                                    }
    				)											
										
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															x:120,
															y:120,
                                                            width:500,
                                                            height:270,
                                                            border:true,
															id:'arbolMateriasOferta',
															useArrows:true,
															autoScroll:true,
															animate:true,
															enableDD:true,
															containerScroll: true,
															root:raiz,
															loader: cargadorArbol,
															rootVisible:false
															
															
														}
													)
	return 	arbolOpciones;		
}

function liberarDisponibilidad()
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            window.parent.recargarContenedorCentral();
            window.parent.invocarEjecucionFuncionContenedor('imprimirFormato',"'"+urlPagina+"','"+gE('idRegistro').value+"'");
            window.parent.cerrarVentanaFancy();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=40&iR='+gE('idRegistro').value,true);
}

function crearGridPeriodos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPeriodo'},
		                                                {name: 'situacion'},
		                                                {name:'periodo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'periodo', direction: 'ASC'},
                                                            groupField: 'periodo',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='36';
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
                                        gEx('btnDisponibilidad').disable();
                                        gEx('btnNoDisponibilidad').disable();
                                        gEx('btnRemoverNODisponibilidad').disable();
                                        gEx('btnClonar').disable();
                                       
                                    }
                        )   
       
       
      alDatos.on('load',function(a)
    								{
                                    	var x;
                                        var fila;
                                        var totalCumplido=0;
                                        for(x=0;x<a.getCount();x++)
                                        {
                                        	fila=a.getAt(x);
                                            if(fila.data.situacion!='0')
                                            	totalCumplido++;
                                        }
                                        
                                        if(totalCumplido==a.getCount())
                                        {
                                        	gEx('btnDisponibilidad').enable();
                                        }
                                        
                                        if(gE('idPeriodo').value!='-1')
                                        {
											var pos=obtenerPosFila(gEx('gPeriodosRegistro').getStore(),'idPeriodo',gE('idPeriodo').value);
                                            gEx('gPeriodosRegistro').getSelectionModel().selectRow(pos);
                                        }
                                        
                                        
                                    }
                        ) 
       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            {
                                                                header:'',
                                                                width:30,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val=='0')
                                                                            	return '<img src="../images/cross.png" width="13" height="13" title="Sin registro de disponibilidad" alt="Sin registro de disponibilidad">';
                                                                            else
                                                                            	if(val=='-1')
                                                                                	return '<img src="../images/cancel_round.png" width="13" height="13" title="Marcado como SIN disponibilidad de horario" alt="Marcado como SIN disponibilidad de horario">';
                                                                                else
		                                                                            return '<img src="../images/icon_big_tick.gif" width="13" height="13" title="Disponibilidad de horario registrada" alt="Disponibilidad de horario registrada">';
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'Periodo',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'periodo',
                                                                renderer:mostrarValorDescripcion
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gPeriodosRegistro',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                border:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                               
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
    											{
                                                	gEx('btnRemoverNODisponibilidad').disable();
                                                    gEx('btnNoDisponibilidad').disable();
                                                    gEx('btnClonar').disable();
                                                	gE('idPeriodo').value=registro.data.idPeriodo;
                                                    if(registro.data.situacion==-1)
                                                    {
                                                    	gEx('btnRemoverNODisponibilidad').enable();
                                                    	gEx('calendario').eventStore.removeAll();
                                                    	gEx('calendario').disable();
                                                    }
                                                    else
                                                    {
                                                        gEx('calendario').enable();
                                                        gEx('btnNoDisponibilidad').enable();
                                                        
                                                        var lastOptions = gEx('calendario').eventStore.lastOptions;
                                                        lastOptions.params.idPeriodo=gE('idPeriodo').value;
                                                        gEx('calendario').eventStore.reload(lastOptions);
                                                        
                                                        if(gEx('gPeriodosRegistro').getStore().getCount()>1)
                                                        {
                                                        
                                                        	var nRegistrosClon=0;
                                                            var x;
                                                            var fila;
                                                            for(x=0;x<gEx('gPeriodosRegistro').getStore().getCount();x++)
                                                            {
                                                            	fila=gEx('gPeriodosRegistro').getStore().getAt(x);
                                                                if((fila.data.idPeriodo!=registro.data.idPeriodo)&&(fila.data.situacion!='-1')&&(fila.data.situacion!='0'))
                                                                {
                                                                	gEx('btnClonar').enable();
                                                                    break;
                                                                }
                                                           	}
                                                        
                                                            
                                                        }
                                                        else
                                                        {
                                                            gEx('btnClonar').disable();
                                                        }
													}                                                    
                                                    
                                                }
                                                
    								) 
                                    
                                    
	tblGrid.getSelectionModel().on('rowdeselect',function(sm,nFila,registro)
    											{
                                                	gE('idPeriodo').value=-1;
                                                    gEx('calendario').eventStore.removeAll();
                                                }
                                                
    								)                                                                                            
                                                        
        return 	tblGrid;	
}

function crearPanelHorario()
{
	var calendarAlmacen = new Ext.data.JsonStore	(
                                                        {
                                                            storeId: 'calendarStore',
                                                            root: 'calendarios',
                                                            idProperty: 'id',
                                                            proxy: new Ext.data.HttpProxy	(
                                                                                                {
                                                                                                    url: '../paginasFunciones/funcionesAgenda.php'
                                                                                                }
        
                                                                                            ),
                                                            autoLoad: true,
                                                            fields: [
                                                                        {name:'CalendarId', mapping: 'id', type: 'string'},
                                                                        {name:'Title', mapping: 'title', type: 'string'}
                                                                    ],
                                                            sortInfo: 	{
                                                                            field: 'CalendarId',
                                                                            direction: 'ASC'
                                                                        }
                                                        }
                                                     );
                                            
	var eventAlmacen = new Ext.data.JsonStore	(
                                                    {
                                                        id: 'eventStore',
                                                        root: 'evts',
                                                        autoLoad: false,
                                                        proxy: new Ext.data.HttpProxy	(
                                                                                            {
                                                                                                url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'
                                                                                            }
            
                                                                                        ),
                                                        fields: Ext.calendar.EventRecord.prototype.fields.getRange(),
                                                        sortInfo: 	{
                                                                        field: 'StartDate',
                                                                        direction: 'ASC'
                                                                    },
                                                        listeners: {
                                                                        beforeLoad:function(proxy)
                                                                                    {
                                                                                        
                                                                                        proxy.baseParams.funcion=37;
                                                                                        proxy.baseParams.idFormulario=gE('idFormulario').value;
                                                                                        proxy.baseParams.idReferencia=gE('idRegistro').value;
                                                                                        proxy.baseParams.idPeriodo=((gE('mPeriodos').value==1)?gE('idPeriodo').value:'0');
                                                                                        proxy.baseParams.rO=gE('rO').value;
                                                                                        
            
                                                                                    }	
                                                                    }
                                                    }
                                               );
    
    
    
    var cPanel=new Ext.calendar.CalendarPanel	(                                   
                                                    {
                                                        
                                                        eventStore: eventAlmacen,
                                                        calendarStore: calendarAlmacen,
                                                        border: false,
                                                        disabled:(gE('mPeriodos').value=='1'),
                                                        id:'calendario',
                                                        region: 'center',
                                                        activeItem: 1, // month view
                                                        monthViewCfg:	{
                                                                            showHeader: false,
                                                                            showWeekLinks: true,
                                                                            showWeekNumbers: false
                                                                        },
                                                        weekViewCfg:	{
                                                                            showHeader: false,
                                                                            showWeekLinks: false,
                                                                            showWeekNumbers: false
                                                                        },
                                                        showDayView: false,
                                                        showWeekView: true,
                                                        showMonthView:false,
                                                        showNavBar: false,
                                                        showTodayText: false,
                                                        initComponent: 	function()
                                                                        {
                                                                            
                                                                            this.constructor.prototype.initComponent.apply(this, arguments);
                                                                            
																			
                                                                        },
                                                    
                                                        listeners: 	{

                                                                        'rangeselect': {
                                                                                            fn: function(win, dates, onComplete)
                                                                                                {
                                                    
                                                                                                    if(gE('rO').value=='1')
                                                                                                    {
                                                                                                        onComplete();
                                                                                                        return;
                                                                                                    }
                                                                                                    var et= dates.EndDate;
                                                                                                    var dt=dates.StartDate;
                                                                                                    var ciclo=gE('ciclo').value;
                                                                                                    
                                                                                                    var obj='{"idPeriodo":"'+((gE('mPeriodos').value=='1')?gE('idPeriodo').value:'0')+'","ciclo":"'+ciclo+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                    '","fechaInicio":"'+dt.format('Y-m-d H:i')+'","fechaFin":"'+et.format('Y-m-d H:i')+'"}';
                                                    
                                                                                                    function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            if((gEx('calendario').eventStore.getCount()==0)&&(gE('mPeriodos').value=='1'))
                                                                                                                gEx('gPeriodosRegistro').getStore().reload();
																											gEx('calendario').eventStore.reload();
                                                                                                            onComplete();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=15&obj='+obj,true);
                                                                                                    
                                                                                                    
                                                                                                },
                                                                                            scope: this
                                                                                        },
                                                                        'eventmove': 	{
                                                                                            fn: function(vw, rec)
                                                                                            {
                                                                                                if(gE('rO').value=='1')
                                                                                                {
                                                                                                    rec.reject();
                                                                                                    return;
                                                                                                }
                                                                                                var obj='{"idPeriodo":"'+((gE('mPeriodos').value=='1')?gE('idPeriodo').value:'0')+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                    '","idRegistro":"'+rec.data.EventId+'","fechaInicio":"'+rec.data.StartDate.format('Y-m-d H:i')+'","fechaFin":"'+rec.data.EndDate.format('Y-m-d H:i')+'"}';
                                                                                                function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            rec.commit();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            rec.reject();
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=38&obj='+obj,true);
                                                                                                
                                                                                            },
                                                                                            scope: this
                                                                                        },
                                                                        'eventresize': {
                                                                                            fn: function(vw, rec)
                                                                                            {
                                                                                                if(gE('rO').value=='1')
                                                                                                {
                                                                                                    rec.reject();
                                                                                                    return;
                                                                                                }
                                                                                                var obj='{"idPeriodo":"'+((gE('mPeriodos').value=='1')?gE('idPeriodo').value:'0')+'","idFormulario":"'+gE('idFormulario').value+'","idReferencia":"'+gE('idRegistro').value+
                                                                                                    '","idRegistro":"'+rec.data.EventId+'","fechaInicio":"'+rec.data.StartDate.format('Y-m-d H:i')+'","fechaFin":"'+rec.data.EndDate.format('Y-m-d H:i')+'"}';
                                                                                                function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            rec.commit();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                            rec.reject();
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=38&obj='+obj,true);
                                                                                                
                                                                                                
                                                                                                
                                                                                            },
                                                                                            scope: this
                                                                                        }
                                                                       
                                                                    }
                                                  }   
                                          	)  
                                            
	return cPanel;                                                                              
                                       
}

function removerBloque(iB)
{
	
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                 	var almacen=gEx('calendario').eventStore   ;
                    var pos=obtenerPosFila(almacen,'EventId',bD(iB));
                    var fila=almacen.getAt(pos);
                    almacen.remove(fila);
                    
                    if(gEx('calendario').eventStore.getCount()==0)
                   	{
                    	gEx('gPeriodosRegistro').getStore().reload();
                    }
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAgenda.php',funcAjax, 'POST','funcion=17&idRegistro='+bD(iB),true);
            
            

        }
    }
    msgConfirm('Est&aacute; seguro de querer remover el bloque seleccionado?',resp)
}

function mostrarVentanaClonar(fila)
{

	var arrPeriodos=[];
    
    var x;
    var filaAux;
    for(x=0;x<gEx('gPeriodosRegistro').getStore().getCount();x++)
    {
        filaAux=gEx('gPeriodosRegistro').getStore().getAt(x);
        if((filaAux.data.idPeriodo!=fila.data.idPeriodo)&&(filaAux.data.situacion!='-1')&&(filaAux.data.situacion!='0'))
        {
            arrPeriodos.push([filaAux.data.idPeriodo,filaAux.data.periodo]);
        }
    }
    
	var cmbPeriodosBase=crearComboExt('cmbPeriodosBase',arrPeriodos,445,5,250);
    cmbPeriodosBase.on('select',function(cmb,registro)
    							{
                                
                                	gEx('frameContenido').load	(
                                    								{
                                                                    	url:'../modulosEspeciales_UGM/frmCalendarioClonDisponibilidad.php',
                                                                        params:	{
                                                                        			idFormulario:gE('idFormulario').value,
                                                                                    idReferencia:gE('idRegistro').value,
                                                                                    idPeriodo:registro.data.id
                                                                        		}
                                                                    }
                                    							)
                                }
    					)
    
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'<span style="color:#000">Seleccione el periodo cuya disponibilidad de horario ser&aacute; clonada al periodo destino:</span>'
                                                        },
                                                        cmbPeriodosBase,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'panel',
                                                            width:690,
                                                            border:false,
                                                            layout:'border',
                                                            height:300,
                                                            items:	[
                                                            			new Ext.ux.IFrameComponent({ 
                
                                                                                                            id: 'frameContenido', 
                                                                                                            anchor:'100% 100%',
                                                                                                            region:'center',
                                                                                                            url: '../modulosEspeciales_UGM/frmCalendarioClonDisponibilidad.php',
                                                                                                            params:	{
                                                                                                                        idFormulario:gE('idFormulario').value,
                                                                                                                        idReferencia:gE('idRegistro').value,
                                                                                                                        idPeriodo:-1
                                                                                                                    },
                                                                                                            style: 'width:100%;height:100%' 
                                                                                                    }),
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Clonar disponibilidad de horario (<span style="color:#000"><b>Periodo destino:</b></span> <span style="color:#900"><b>'+fila.data.periodo+'</b></span>)',
										width: 740,
										height:420,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	var fechaInicial=new Date(2011,5,6);
																	gEx('calendarioClon').setStartDate(fechaInicial);  
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbPeriodosBase.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbPeriodosBase.focus();
                                                                            }
                                                                        	msgBox('Debe seleccionar el periodo cuya disponibilidad de horario ser&aacute; clonada al periodo destino',resp)
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        function resp2(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gPeriodosRegistro').getStore().reload();
		                                                                            	ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=43&iR='+gE('idRegistro').value+'&idOrigen='+cmbPeriodosBase.getValue()+'&idDestino='+fila.data.idPeriodo,true);
                                                                                
                                                                            	
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer clonar el horario del periodo: '+cmbPeriodosBase.getRawValue()+' al periodo destino:'+fila.data.periodo+
                                                                        		'? <br />(Esto eliminar&aacute; cualquier horario registrado previamente en el periodo destino)',resp2)
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridMaterias(t)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idMateria'},
		                                                {name: 'idInstanciaPlanEstudio'},
		                                                {name:'lblPlanEstudio'},
		                                                {name: 'nombreMateria'},
                                                        {name: 'idPeriodo'},
                                                        {name: 'lblPeriodo'},
                                                        {name: 'criterioMaterias'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreMateria', direction: 'ASC'},
                                                            groupField: 'lblPeriodo',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 

	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                      filters:	[
                                                                      {
                                                                          type:'string',
                                                                          dataIndex:'nombreMateria' 
                                                                      }
                                                                  ]
                                                  }
    											)                                       
                                                        
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='44';
                                        proxy.baseParams.idUsuario=gE('idUsuario').value;
                                        proxy.baseParams.accion=t;
                                        proxy.baseParams.idRegistro=gE('idRegistro').value;
                                    }
                        )   
       
	var anchoPlan=1;
    var anchoMateria=1;
    if((parseFloat(gE('idEstado').value)=='2')&&(t=='2')&&(gE('vModificacion').value=='0'))       
    {
    	anchoPlan++;
        anchoMateria++;
        
    }  
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	chkRow,
                                                            {
                                                                header:'Periodo',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'lblPeriodo'
                                                            },
                                                            {
                                                                header:'Plan de estudios',
                                                                width:(280*anchoPlan),
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'lblPlanEstudio'
                                                            },
                                                            {
                                                                header:'Materia',
                                                                width:(320*anchoMateria),
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'nombreMateria'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridMaterias_'+t,
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                border:false,
                                                                cm: cModelo,
                                                                
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                sm:chkRow,
                                                                plugins:[filters],
                                                                columnLines : true,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/right1.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:((t!='1')||((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0'))),
                                                                                text:'Marcar como materias de inter&eacute;s',
                                                                                handler:function()
                                                                                        {
                                                                                        	
                                                                                            var filas=tblGrid.getSelectionModel().getSelections();
                                                                                            if(filas.length==0)
                                                                                            {
                                                                                            	msgBox('Almenos debe seleccionar una materia a agregar como de inter&eacute;s')
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            var cadMaterias="";
                                                                                            var x;
                                                                                            var f;
                                                                                            var ct;
                                                                                            var aux;
                                                                                            var oMateria;
                                                                                            for(x=0;x<filas.length;x++)
                                                                                            {
                                                                                            	f=filas[x];
                                                                                                arrMaterias=eval(f.data.idMateria);
                                                                                                for(ct=0;ct<arrMaterias.length;ct++)
                                                                                                {
                                                                                                	aux=arrMaterias[ct];
                                                                                                    oMateria='{"iMateria":"'+aux[0]+'","iPlan":"'+aux[1]+'","criterio":"'+f.data.criterioMaterias+'"}';
                                                                                                    if(cadMaterias=='')
                                                                                                    	cadMaterias=oMateria;
                                                                                                    else
                                                                                                    	cadMaterias+=","+oMateria;
                                                                                                }
                                                                                            }
                                                                                            
                                                                                            
                                                                                            var cadObj='{"idUsuario":"'+gE('idUsuario').value+'","idConvocatoria":"'+gE('idRegistro').value+'","arrMaterias":['+cadMaterias+']}';
                                                                                            
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    tblGrid.getStore().remove(filas);
                                                                                                    gEx('gridMaterias_2').getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=45&cadObj='+cadObj,true);
                                                                                            
                                                                                            
                                                                                            
                                                                                        }
                                                                                
                                                                            },
                                                                            {
                                                                                icon:'../images/left1.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:((t!='2')||((parseFloat(gE('idEstado').value)==2)&&(gE('vModificacion').value=='0'))),
                                                                                text:'Quitar de materias de inter&eacute;s',
                                                                                handler:function()
                                                                                        {
                                                                                             var filas=tblGrid.getSelectionModel().getSelections();
                                                                                            if(filas.length==0)
                                                                                            {
                                                                                            	msgBox('Almenos debe seleccionar una materia a remover de materias de inter&eacute;s')
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            var cadMaterias="";
                                                                                            var x;
                                                                                            var f;
                                                                                            var ct;
                                                                                            var aux;
                                                                                            var oMateria;
                                                                                            for(x=0;x<filas.length;x++)
                                                                                            {
                                                                                            	f=filas[x];
                                                                                                arrMaterias=eval(f.data.idMateria);
                                                                                                for(ct=0;ct<arrMaterias.length;ct++)
                                                                                                {
                                                                                                	aux=arrMaterias[ct];
                                                                                                    oMateria='{"iMateria":"'+aux[0]+'","iPlan":"'+aux[1]+'","criterio":"'+f.data.criterioMaterias+'"}';
                                                                                                    if(cadMaterias=='')
                                                                                                    	cadMaterias=oMateria;
                                                                                                    else
                                                                                                    	cadMaterias+=","+oMateria;
                                                                                                }
                                                                                            }
                                                                                            
                                                                                            
                                                                                            var cadObj='{"idUsuario":"'+gE('idUsuario').value+'","idConvocatoria":"'+gE('idRegistro').value+'","arrMaterias":['+cadMaterias+']}';
                                                                                            
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    tblGrid.getStore().remove(filas);
                                                                                                    gEx('gridMaterias_1').getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=46&cadObj='+cadObj,true);
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                      	],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
	
}

