<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var capturado=false;

var arrSituacionOperacion=[['1,0','Todas las aperaciones'],['1','Activas'],['0','Canceladas']];
var arrTipoOperacion=[['-1','Egreso'],['1','Ingreso']];

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Control presupuestal</b></span>',
                                               
                                                items:	[
                                                            crearGridPresupuesto()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridPresupuesto()
{
   var cmbTipoOperacion=crearComboExt('cmbTipoOperacion',arrTipoOperacion);
   var cmbSituacionOperacion=crearComboExt('cmbSituacionOperacion',arrSituacionOperacion,0,0,240);
   cmbSituacionOperacion.setValue('1');
   cmbSituacionOperacion.on('select',function(cmb,registro)
   										{
                                        	gEx('gridPresupuesto').getStore().reload();
                                        }
   							)
   var lector= new Ext.data.JsonReader({
                                        
                                        totalProperty:'numReg',
                                        fields: [
                                                    {name:'idOperacion'},
                                                    {name: 'fechaCreacion', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                    {name: 'fechaOperacion', type:'date', dateFormat:'Y-m-d'},
                                                    {name:'montoOperacion'},
                                                    {name: 'tipoOperacion'},
                                                    {name: 'concepto'},
                                                    {name: 'comentarios'},
                                                    {name: 'fechaCancelacion', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                    {name: 'motivoCancelacion'},
                                                    {name: 'responsable'},
                                                    {name: 'situacion'},
                                                    {name: 'idFormularioRef'},
                                                    {name: 'idRegistroRef'}
                                                ],
                                        root:'registros'
                                        
                                    }
                                  );
 
                                                                                  
	var alDatos=new Ext.data.GroupingStore({
                                                        reader: lector,
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesModulosProcesos.php',
                                                                                              listeners:	{
                                                                                              					'load':function(proxy,o)
                                                                                                                		{
                                                                                                                        	var color='030';
                                                                                                                        	var objSaldos=o.reader.jsonData.objSaldos;
                                                                                                                            gE('tIngreso').innerHTML=Ext.util.Format.usMoney(objSaldos.totalIngreso);
                                                                                                                            gE('tEgreso').innerHTML=Ext.util.Format.usMoney(objSaldos.totalEgreso);
                                                                                                                            if(parseFloat(objSaldos.saldo)<0)
                                                                                                                            	color='F00';
                                                                                                                            gE('tSaldo').innerHTML='<span style="color:#'+color+';font-weight:bold">'+Ext.util.Format.usMoney(objSaldos.saldo)+'</span>';
                                                                                                                        }
                                                                                              				}

                                                                                          }

                                                                                      ),
                                                        sortInfo: {field: 'idOperacion', direction: 'DESC'},
                                                        groupField: 'fechaOperacion',
                                                        remoteGroup:false,
                                                        remoteSort: false,
                                                        autoLoad:true
                                                        
                                                    }) 
	alDatos.on('beforeload',function(proxy)
                                {
                                    proxy.baseParams.funcion='17';
                                    proxy.baseParams.idFormulario=gE('idFormulario').value;
                                    proxy.baseParams.idReferencia=gE('idRegistro').value;
                                    proxy.baseParams.situacionOperacion=cmbSituacionOperacion.getValue();
                                    
                                }
                    )   
   	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true,checkOnly:true });
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        chkRow,
                                                        {
                                                            header:'Fecha de registro',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'fechaCreacion',
                                                            renderer:function(val,meta,registro)		
                                                            		{
                                                                    	var color='000';
                                                                        if(registro.data.situacion=='0')
                                                                        	color='F00';
                                                                    	if(val)
                                                                        	return '<span style="color:#'+color+'">'+val.format('d/m/Y H:i:s')+'</span>';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Fecha de la Operaci&oacute;n <span style="color:#F00">*</span>',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'fechaOperacion',
                                                            editor:{xtype:'datefield',value:'<?php echo date("Y-m-d")?>'},
                                                            renderer:function(val,meta,registro)		
                                                            		{
                                                                    	var color='000';
                                                                        if(registro.data.situacion=='0')
                                                                        	color='F00';
                                                                    	if(val)
                                                                        	return '<span style="color:#'+color+'">'+val.format('d/m/Y')+'</span>';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Monto de la Operaci&oacute;n <span style="color:#F00">*</span>',
                                                            width:130,
                                                            sortable:true,
                                                            css:'text-align:right !important;',
                                                            editor:{xtype:'numberfield', allowDecimals:true,allowNegative:false},
                                                            dataIndex:'montoOperacion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color='000';
                                                                        if(registro.data.situacion=='0')
                                                                        	color='F00';
                                                                    	return '<span style="color:#'+color+'">'+Ext.util.Format.usMoney(val)+'</span>';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Tipo de Operaci&oacute;n <span style="color:#F00">*</span>',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'tipoOperacion',
                                                            editor:cmbTipoOperacion,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color='030';
                                                                        var comp='';
                                                                        if(val=='-1')
                                                                        	color='900';
                                                                        if(registro.data.situacion=='0')    
                                                                        {
                                                                        	color='F00';
                                                                        	var leyenda='Operación cancelada por: '+registro.data.responsable+'\r\nFecha de cancelación: '+registro.data.fechaCancelacion.format('d/m/Y H:i:s')+'\r\nMotivo de la cancelación: '+registro.data.motivoCancelacion;
                                                                        	comp='&nbsp;<img src="../images/exclamation.png" width="11" height="11" title="'+leyenda+'" alt="'+leyenda+'">';
                                                                        }
                                                                    	return '<span style="color:#'+color+'"><b>'+formatearValorRenderer(arrTipoOperacion,val)+'</b></span>'+comp;
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Concepto <span style="color:#F00">*</span>',
                                                            width:350,
                                                            sortable:true,
                                                            editor:{xtype:'textfield'},
                                                            dataIndex:'concepto',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color='000';
                                                                        var comp='';
                                                                        if(registro.data.situacion=='0')
                                                                        	color='F00';
                                                                            
                                                                    	return comp+' <span style="color:#'+color+'">'+mostrarValorDescripcion(val)+'</span>';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Comentarios adicionales',
                                                            width:450,
                                                            sortable:true,
                                                            editor:{xtype:'textfield'},
                                                            dataIndex:'comentarios',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var color='000';
                                                                        if(registro.data.situacion=='0')
                                                                        	color='F00';
                                                                    	return '<span style="color:#'+color+'">'+mostrarValorDescripcion(val)+'</span>';
                                                                    }
                                                            
                                                        }
                                                    ]
                                                );
	
    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editorFila',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
                                               
    editorFila.on('beforeedit',funcEditorFilaBeforeEdicion)
    editorFila.on('validateedit',funcEditorValidaEdicion);
    editorFila.on('canceledit',funcEditorCancelEdicion);
                                                    
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridPresupuesto',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            border:false,
                                                            sm:chkRow,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            clicksToEdit:1,
                                                            columnLines : true,
                                                            plugins:[editorFila],
                                                            bbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000"><b>Total Ingreso:&nbsp;&nbsp;&nbsp;</b></span>'
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold" id="tIngreso"></span>&nbsp;&nbsp;&nbsp;'
                                                                        }
                                                                        ,'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold" ><b>Total Egreso:&nbsp;&nbsp;&nbsp;</b></span>'
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold" id="tEgreso"></span>&nbsp;&nbsp;&nbsp;'
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold"><b>Saldo disponible:&nbsp;&nbsp;&nbsp;</b></span>'
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold" id="tSaldo"></span>&nbsp;&nbsp;&nbsp;'
                                                                        }
                                                                        
                                                            		],
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'<span class="letraRojaSubrayada8"><b>Mostrar operaciones:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                            			cmbSituacionOperacion,
                                                                        '-',
                                                            			{
                                                                        	id:'btnAgregar',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:(gE('sL').value=='1'),
                                                                            text:'Agregar operaci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrCampos=	[
                                                                                                           	{name: 'idOperacion'},
                                                                                                            {name: 'fechaOperacion'},
                                                                                                            {name: 'montoOperacion'},
                                                                                                            {name: 'tipoOperacion'},
                                                                                                            {name: 'concepto'},
                                                                                                            {name: 'comentarios'}
                                                                                                        ]
                                                                                                        
                                                                                        var registroGrid=crearRegistro(arrCampos);
                                                                                        var r=new registroGrid	(
                                                                                        							{
                                                                                                                    	idOperacion:'-1',
                                                                                                                        fechaOperacion:'',
                                                                                                                        montoOperacion:'',
                                                                                                                        tipoOperacion:'',
                                                                                                                        concepto:'',
                                                                                                                        comentarios:''
                                                                                                                    }
                                                                                        						)
                                                                                        
                                                                                       
                                                                                        
                                                                                        editorFila.stopEditing();
                                                                                        tblGrid.getStore().insert(0,r);
                                                                                        tblGrid.nuevoRegistro=true;
                                                                                        editorFila.startEditing(0);	
                                                                                        Ext.getCmp('btnAgregar').disable();
                                                                                        Ext.getCmp('btnRemover').disable();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnRemover',
                                                                        	icon:'../images/delete.png',
                                                                            hidden:(gE('sL').value=='1'),
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Cancelar operaci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar le movimiento que desea cancelar');
                                                                                            return;
                                                                                        }
                                                                                       	mostrarVentanaCancelacion(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                            		],
                                                            view: new Ext.ux.grid.BufferView({
                                                            									scrollDelay: false
                                                                                              }
                                                                                            )
                                                            /*view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })*/
                                                        }
                                                    );
	tblGrid.on('beforeedit',function(e)
    						{
                            	e.cancel=true;
                            }
    		)                                                    
	                                                  
    return 	tblGrid;	
}

function funcEditorFilaBeforeEdicion(rowEdit,fila)
{
	if(gE('sL').value=='1')
    	return false;
	var idGrid='gridPresupuesto';
	var grid=Ext.getCmp(idGrid);
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
	if((grid.soloLectura)&&(!grid.nuevoRegistro))
		return false;
}

function funcEditorValidaEdicion(rowEdit,obj,registro,nFila)
{
	var idGrid='gridPresupuesto';
	var grid=Ext.getCmp(idGrid);
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
    if(capturado)
    {
    	return;
    }
    capturado=true;
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   	var idOperacion=registro.get('idOperacion');
    var cadObj='{"idFormulario":"'+gE('idFormulario').value+'","idRegistro":"'+gE('idRegistro').value+'","idOperacion":"'+idOperacion+'","fechaOperacion":"'+
    			obj.fechaOperacion.format('Y-m-d')+'","montoOperacion":"'+obj.montoOperacion+'","tipoOperacion":"'+obj.tipoOperacion+'","concepto":"'+obj.concepto+'","comentarios":"'+obj.comentarios+'"}';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            if(Ext.getCmp('btnRemover')!=null)
                Ext.getCmp('btnRemover').enable();	
            if(Ext.getCmp('btnAgregar')!=null)            
                Ext.getCmp('btnAgregar').enable();
            grid.nuevoRegistro=false;
            capturado=false;
            registro.set('idOperacion',arrResp[1]);
            if(idOperacion=='-1')
	            registro.set('fechaCreacion',Date.parseDate(arrResp[2],'Y-m-d H:i:s'));
            window.parent.mostrarMenuDTD();
            recalcularSaldo();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=18&cadObj='+cadObj,true);
}

function funcEditorCancelEdicion(rowEdit,cancelado)
{

	var idGrid='gridPresupuesto';
	var grid=Ext.getCmp(idGrid);
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(0);
	Ext.getCmp('btnRemover').enable();
    Ext.getCmp('btnAgregar').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}

function recalcularSaldo()
{
	var grid=Ext.getCmp('gridPresupuesto');
    var x;
    var fila;
    var sumaTI=0;
    var sumaTE=0;
    var saldo=0;
    var color='030';
    for(x=0;x<grid.getStore().getCount();x++)
    {
    	fila=grid.getStore().getAt(x);
        if(fila.get('tipoOperacion')=='-1')
	        sumaTE+=parseFloat(fila.get('montoOperacion'));
        else
        	sumaTI+=parseFloat(fila.get('montoOperacion'));
        
    }
    saldo=sumaTI-sumaTE;
    gE('tIngreso').innerHTML=Ext.util.Format.usMoney(sumaTI);
    gE('tEgreso').innerHTML=Ext.util.Format.usMoney(sumaTE);
    if(saldo<0)
        color='F00';
    gE('tSaldo').innerHTML='<span style="color:#'+color+';font-weight:bold">'+Ext.util.Format.usMoney(saldo)+'</span>';
    
}


function mostrarVentanaCancelacion(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Especifique el motivo de la cancelaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:460,
                                                            height:100,
                                                            id:'txtMotivoCancelacion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar operaci&oacute;n',
										width: 500,
										height:235,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivoCancelacion').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                var txtMotivoCancelacion=gEx('txtMotivoCancelacion');
                                                                                if(txtMotivoCancelacion.getValue().trim()=='')
                                                                                {
                                                                                	function resp2()
                                                                                    {
                                                                                    	txtMotivoCancelacion.focus();
                                                                                    }
                                                                                	msgBox('Debe ingresar el motivo de la cancelaci&oacute;n',resp2);
                                                                                	return;
                                                                                }
                                                                                
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gridPresupuesto').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesModulosProcesos.php',funcAjax, 'POST','funcion=19&idRegistro='+fila.get('idOperacion')+'&motivo='+txtMotivoCancelacion.getValue().trim(),true);

                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar la operaci&oacute;n seleccionada?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
	 																					
}