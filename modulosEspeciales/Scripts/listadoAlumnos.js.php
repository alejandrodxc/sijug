<?php include("latis/conexionBD.php");
?>

function modificarInformacionAlumno()
{
	var actor=gE('actor').value;
	var grid=gEx('grid_tblTabla');
    var idFormulario=bD(gE('idFormularioBase').value);
    var fila=grid.getSelectionModel().getSelected();
    if(fila==null)
    {
    	msgBox('Debe seleccionar el alumno con el cual desea trabajar');
    	return;
    }
    var idAlumno=fila.get('idUsuario');
    function funcAjax()
    {
    	
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var arrDatos=[['idFormulario',idFormulario],['idRegistro',arrResp[1]],['actor',actor],['dComp',bE('auto')]];
            window.parent.open('',"vAuxiliar", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
		    enviarFormularioDatos('../modeloPerfiles/vistaDTD.php',arrDatos,'POST','vAuxiliar');
        }
        else
        {
            msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema: '+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesEspeciales.php',funcAjax, 'POST','funcion=1&idFormulario='+idFormulario+'&inserta=1&valor='+bE(idAlumno+'')+'&campo='+bE('alumno'),true);
    
    
}