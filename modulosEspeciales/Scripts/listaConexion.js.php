<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE STATUS=2 AND institucion=1
				AND unidadPadre='' ORDER BY unidad";
	$arrSubsistema=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__246_tablaDinamica,Curso FROM _246_tablaDinamica WHERE Activo=1 AND modalidadCurso IN (2,3) ORDER BY Curso";
	$arrCursos=$con->obtenerFilasArreglo($consulta);
?>
var arrSubsistema=<?php echo $arrSubsistema ?>;
var arrCursos=<?php echo $arrCursos?>;

Ext.onReady(inicializar);

function inicializar()
{

	var cmbSubsistema=crearComboExt('cmbSubsistema',arrSubsistema,0,0,400);
   	
    var cmbFechaSesion=crearComboExt('cmbFechaSesion',[],0,0,150);
   	cmbFechaSesion.on('select',function(combo,registro)
    							{
                                	if((gEx('cmbSubsistema').getValue()!='')&&(gEx('cmbHorario').getValue()!='')&&(gEx('cmbCursos').getValue()!=''))
	                                	gEx('gridConexion').getStore().reload();
                                }
    				)
    var cmbHorario=crearComboExt('cmbHorario',[],0,0,195);
    cmbHorario.on('select',function(combo,registro)
    							{
                                	if((gEx('cmbSubsistema').getValue()!='')&&(gEx('cmbFechaSesion').getValue()!='')&&(gEx('cmbCursos').getValue()!=''))
                                		gEx('gridConexion').getStore().reload();
                                }
    				)
                    
                    
	cmbSubsistema.on('select',function(combo,registro)
    							{
                                	gEx('gridConexion').getStore().removeAll();
                                   	cmbFechaSesion.reset();
                                   	cmbHorario.reset();
                                    gEx('cmbCursos').reset();    
                                	
                                }
    				)    
                    
	var cmbCursos=crearComboExt('cmbCursos',arrCursos,0,0,410);   
	cmbCursos.on('select',function(combo,registro)
    						{
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        gEx('gridConexion').getStore().removeAll();
                                        cmbFechaSesion.reset();
                                        cmbFechaSesion.getStore().loadData(eval(arrResp[4]));
                                        cmbHorario.reset();
                                        var arrHorario=eval(arrResp[2]);
                                        arrHorario.splice(arrHorario.length-1,1);
                                        cmbHorario.getStore().loadData(arrHorario);
                                
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesReportes.php',funcAjax, 'POST','funcion=20&idCurso='+registro.get('id')+'&codigoUnidad='+gEx('cmbSubsistema').getValue(),true);
                            }
    			)                                 
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'codigoUnidad'},
		                                                {name: 'plantel'},
		                                                {name:'conectado'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesReportesGalileo.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'plantel', direction: 'ASC'},
                                                            groupField: 'plantel',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='3';
                                        proxy.baseParams.subsistema=cmbSubsistema.getValue();
                                        proxy.baseParams.fechaSesion=cmbFechaSesion.getValue();
                                        proxy.baseParams.horario=cmbHorario.getValue();
                                        proxy.baseParams.idCurso=gEx('cmbCursos').getValue();
                                        
                                        
                                    }
                        )   
     var checkColumn = new Ext.grid.CheckColumn	(
	 												{
													   header: 'Se conect&oacute;',
													   
													   width: 120,
                                                       dataIndex:'conectado'
                                                      
													}
												);
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer({width:25}),
                                                            {
                                                                header:'Plantel',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'plantel'
                                                            },
                                                            checkColumn
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridConexion',
                                                                store:alDatos,
                                                                frame:false,
                                                                border:true,
																plugins:[checkColumn],
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                width:950,
                                                                height:400,
                                                                columnLines : true,
                                                                tbar:	[
                                                                            {
                                                                                html:'<span class="letraRojaSubrayada8"><b>Fecha sesi&oacute;n:</b> </span>&nbsp;',
                                                                                xtype:'label'
                                                                            },
                                                                            cmbFechaSesion,'-',
                                                                            {
                                                                                html:'<span class="letraRojaSubrayada8"><b>Horario:</b> </span>&nbsp;',
                                                                                xtype:'label'
                                                                            },
                                                                            cmbHorario,'-',
                                                                            {
                                                                                icon:'../images/guardar.PNG',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Guardar listado de planteles conectados',
                                                                                handler:function()
                                                                                        {
                                                                                         	var x;
                                                                                            var cadObj='';
                                                                                            var obj;
                                                                                            var fila;
                                                                                            for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                            {
                                                                                            	fila=tblGrid.getStore().getAt(x);
                                                                                            	if(fila.get('conectado'))
                                                                                                {
                                                                                                	obj='{"codigoUnidad":"'+fila.get('codigoUnidad')+'"}';
                                                                                                    if(cadObj=='')
                                                                                                    	cadObj=obj;
                                                                                                    else
                                                                                                    	cadObj+=','+obj;
                                                                                                }
                                                                                            }
                                                                                            var obj='{"subsistema":"'+cmbSubsistema.getValue()+'","fecha":"'+cmbFechaSesion.getValue()+'","horario":"'+
                                                                                            			cmbHorario.getValue()+'","arrPlanteles":['+cadObj+']}';
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    msgBox("La informaci&oacute;n ha sido guardada de manera correcta");
                                                                                                    return;
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesReportesGalileo.php',funcAjax, 'POST','funcion=4&idCurso='+gEx('cmbCursos').getValue()+'&cadObj='+obj,true);
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
                                                        
	var panet=new Ext.Panel	(
    							{
                                	renderTo:'tblTabla',
                                    border:true,
                                    frame: true,
                                    tbar:	[
                                                {
                                                    html:'<span class="letraRojaSubrayada8"><b>Subsistema:</b> </span>&nbsp;',
                                                    xtype:'label'
                                                },
                                                cmbSubsistema,
                                                '-',
                                                {
                                                	html:'&nbsp;<span class="letraRojaSubrayada8"><b>Cursos:</b> </span>&nbsp;',
                                                    xtype:'label'
                                                },
                                                cmbCursos
                                            ],
                                    items:	[
                                    			tblGrid
                                    		]
                                }
    						)
}