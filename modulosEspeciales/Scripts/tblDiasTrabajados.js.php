<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idEmpresa,
				IF(e.tipoEmpresa=1,CONCAT('[',cveEmpresa,'] ',e.razonSocial,' ',e.apPaterno,' ',e.apMaterno),concat('[',cveEmpresa,'] ',e.razonSocial)) AS nombreEmpresa
			 ,concat(rfc1,'-',rfc2,'-',rfc3) as rfc FROM 6927_empresas e WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1";
	$arrInstituciones=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT noOrdinal,nombreElemento,mesInicio,diaInicio FROM 678_elementosTipoPagoNomina WHERE idTipoPago=2 ORDER BY noOrdinal";
	$arrQuincenas=$con->obtenerFilasArreglo($consulta);
	
	$arrCampos='';
	$arrColumnas='';
	
	
	
	$consulta="SELECT conceptoBase,c.nombreConcepto FROM _1012_tablaDinamica t,713_conceptosBaseNomina c WHERE t.situacion=1 AND c.idConcepto=t.conceptoBase and id__1012_tablaDinamica
			in(SELECT idReferencia FROM _1012_gridInstitucionAutorizada WHERE Instituciones='".$_SESSION["codigoInstitucion"]."') ORDER BY nombreConcepto";
	$rConceptos=$con->obtenerFilas($consulta);
	while($fConcepto=mysql_fetch_row($rConceptos))
	{
		
		$c="{
				header:'".($fConcepto[1])."',
				width:130,
				sortable:true,
				css:'text-align:right;',
				dataIndex:'concepto_".$fConcepto[0]."',
				renderer:function(val)
						{
							if(val=='')
								return 0;
							 return removerCerosDerecha(val);
						},
				editor:	{
							xtype:'numberfield',
							allowDecimals:false,
							allowNegative:false
						}
			}";
		
		$arrColumnas.=",".$c;
		$arrCampos.=',{"name":"concepto_'.$fConcepto[0].'"}';
		
	}
?>

var arrQuincenas=<?php echo $arrQuincenas?>;
var arrInstituciones=<?php echo $arrInstituciones?>;

Ext.onReady(inicializar);

function inicializar()
{

	var cmbInstituciones=crearComboExt('cmbInstituciones',arrInstituciones,0,0,480);
	
    var cmbCentroCosto=crearComboExt('cmbCentroCosto',[],0,0,300);
    
    
    cmbCentroCosto.on('select',function(cmb,registro)
    							{
                                	if(gEx('cmbQuincena').getValue()!='')
                                    {
                                    	obtenerEmpleadosCC();
                                    }
                                }
                        )
    
    cmbInstituciones.on('select',function(cmb,registro)
    							{
                                	function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                            var arrDatos=eval(arrResp[1]);
                                            gEx('cmbCentroCosto').reset();
                                            gEx('gridEmpleados').getStore().removeAll();
                                            gEx('cmbCentroCosto').getStore().loadData(arrDatos);
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesEspeciales.php',funcAjax, 'POST','funcion=24&idEmpresa='+registro.data.id,true);
                                    
                                }
    				)


	new Ext.Panel	(
    					{
                            renderTo:'tblDias',
                            tbar:	[
                                        {
                                            xtype:'label',
                                            html:'<span class="letraAzulSimple" style="color:#000">Empresa:</span>&nbsp;&nbsp;'
                                        },
                                        cmbInstituciones
                                        ,'-',
                                        {
                                            xtype:'label',
                                            html:'<span class="letraAzulSimple" style="color:#000">Centro de Costo:</span>&nbsp;&nbsp;'
                                        },
                                        cmbCentroCosto
                                    ],
                            items:	[
                                        
                                        crearGridDiasTrabajados()
                                    ]
                         }
    				)
	

    if(arrInstituciones.length==1)
    {
    	cmbInstituciones.setValue(arrInstituciones[0][0]);
        dispararEventoSelectCombo('cmbInstituciones');
   	}
}


function crearGridDiasTrabajados()
{


	var cmbQuincena=crearComboExt('cmbQuincena',arrQuincenas,0,0,180);
    cmbQuincena.on('select',function(cmb,registro)
    							{
                                	obtenerEmpleadosCC();
                                }
                        )
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idEmpleado'},
                                                        {name: 'numEmpleado'},
		                                                {name: 'nombreEmpleado'},
		                                                {name:'rfc'},
		                                                {name:'diasTrabajados'}<?php echo $arrCampos?>
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesEspeciales.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreEmpleado', direction: 'ASC'},
                                                            groupField: 'nombreEmpleado',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	 
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'No. Empleado',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'numEmpleado'
                                                            },
                                                            {
                                                                header:'RFC',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'rfc'
                                                            },
                                                            {
                                                                header:'Empleado',
                                                                width:270,
                                                                sortable:true,
                                                                dataIndex:'nombreEmpleado',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'D&iacute;as Trabajados',
                                                                width:100,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'diasTrabajados',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val=='')
                                                                            	return 0;
                                                                             return val;
                                                                        },
                                                                editor:	{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:false,
                                                                            allowNegative:false
                                                                		}
                                                            }
                                                            <?php echo $arrColumnas?>
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridEmpleados',
                                                                store:alDatos,
                                                                border:false,
                                                                frame:false,
                                                                width:960,
                                                                clicksToEdit:1,
                                                                height:450,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                 tbar:	[
                                                                            {
                                                                                xtype:'label',
                                                                                html:'<span class="letraAzulSimple" style="color:#000">Quincena de captura:</span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbQuincena
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        
        
        tblGrid.on('afteredit',function(e)
        						{
                                
                                	
                                
                                	function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                            
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesEspeciales.php',funcAjax, 'POST','funcion=26&campo='+e.field+'&idEmpleado='+e.record.data.idEmpleado+'&idCC='+gEx('cmbCentroCosto').getValue()+'&idPeriodo='+gEx('cmbQuincena').getValue()+'&dias='+e.value,true);
                                    
                                
                                }
        			)
        
        return 	tblGrid;
        
        	
}


function obtenerEmpleadosCC()
{
	gEx('gridEmpleados').getStore().load	(
    											{
                                                	url:'../paginasFunciones/funcionesEspeciales.php',
                                                    params:	{
                                                    			funcion:25,
                                                                idCentroCosto:gEx('cmbCentroCosto').getValue(),
                                                                idQuincena:gEx('cmbQuincena').getValue()
                                                    		}
                                                }
    										)
}