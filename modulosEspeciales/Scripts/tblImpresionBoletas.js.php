<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idUsuario'},
		                                                {name: 'alumnos'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesProgAcademico.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'alumnos', direction: 'ASC'},
                                                            groupField: 'alumnos',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='140';
                                        var idGrupoMaestro=-1;
                                        var cmbGrupo=gE('cmbGrupo');
                                        if(cmbGrupo.selectedIndex!=-1)
											idGrupoMaestro=cmbGrupo.options[cmbGrupo.selectedIndex].value;
                                        proxy.baseParams.idGrupoMaestro=idGrupoMaestro;
                                        
                                       
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Alumno',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'alumnos'

                                                            },
                                                            {
                                                                header:'Espa&ntilde;ol',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'idUsuario',
                                                                renderer:function(val)
                                                                		{
                                                                        	return '<a href="javascript:obtenerDocumentoBoleta(\''+bE(0)+'\',\''+bE(val)+'\')"><img src="../images/download.png"> Descargar</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Ingl&eacute;s',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'idUsuario',
                                                                renderer:function(val)
                                                                		{
                                                                        	return '<a href="javascript:obtenerDocumentoBoleta(\''+bE(1)+'\',\''+bE(val)+'\')"><img src="../images/download.png"> Descargar</a>';
                                                                        }	
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'tblGrupoGrid',
                                                                store:alDatos,
                                                                renderTo:'tblGrupo',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                width:630,
                                                                height:350,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function cicloChange(cmb)
{
	gE('cmbPeriodo').selectedIndex=-1;
    periodoChange(gE('cmbPeriodo'));

}

function planEstudioChange(cmb)
{

	gE('cmbPeriodo').selectedIndex=-1;
    periodoChange(gE('cmbPeriodo'));
	var valor=-1;
    if(cmb.selectedIndex!=-1)
	    valor=cmb.options[cmb.selectedIndex].value;
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            llenarCombo(gE('cmbPeriodo'),arrDatos,true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax, 'POST','funcion=137&idInstancia='+valor,true);

}

function periodoChange(cmb)
{
	gE('cmbGrado').selectedIndex=-1;
    gradoChange(gE('cmbGrado'));

	var valor=-1;
    if(cmb.selectedIndex!=-1)
	    valor=cmb.options[cmb.selectedIndex].value;
    var idInstancia='-1';
    var cmbPlanEstudio=gE('cmbPlanEstudio');
    
    if(cmbPlanEstudio.selectedIndex!=-1)
	    idInstancia=cmbPlanEstudio.options[cmbPlanEstudio.selectedIndex].value;
    var idCiclo='-1';
    var cmbCiclo=gE('cmbCiclo');
    if(cmbCiclo.selectedIndex!=-1)
	    idCiclo=cmbCiclo.options[cmbCiclo.selectedIndex].value;
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            llenarCombo(gE('cmbGrado'),arrDatos,true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax, 'POST','funcion=138&idInstancia='+idInstancia+'&idCiclo='+idCiclo+'&idPeriodo='+valor,true);
}

function gradoChange(cmb)
{
	gE('cmbGrupo').selectedIndex=-1;
    grupoChange(gE('cmbGrupo'));
	var valor=-1;
    if(cmb.selectedIndex!=-1)
	    valor=cmb.options[cmb.selectedIndex].value;
    var idInstancia='-1';
    var cmbPlanEstudio=gE('cmbPlanEstudio');
    if(cmbPlanEstudio.selectedIndex!=-1)
    	idInstancia=cmbPlanEstudio.options[cmbPlanEstudio.selectedIndex].value;
    var idCiclo='-1';
    var cmbCiclo=gE('cmbCiclo');
    if(cmbCiclo.selectedIndex!=-1)
	    idCiclo=cmbCiclo.options[cmbCiclo.selectedIndex].value;
    var idPeriodo='';
    var cmbPeriodo=gE('cmbPeriodo');
    if(cmbPeriodo.selectedIndex!=-1)
	    idPeriodo=cmbPeriodo.options[cmbPeriodo.selectedIndex].value;
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            llenarCombo(gE('cmbGrupo'),arrDatos,true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProgAcademico.php',funcAjax, 'POST','funcion=139&idInstancia='+idInstancia+'&idCiclo='+idCiclo+'&idPeriodo='+idPeriodo+'&grado='+valor,true);
}

function grupoChange(cmb)
{
    gEx('tblGrupoGrid').getStore().reload();

}


function obtenerDocumentoBoleta(tDocumento,idAlumno)
{	
	var cmbGrupo=gE('cmbGrupo');
	var idGrupoMaestro=cmbGrupo.options[cmbGrupo.selectedIndex].value;
    var cmbBimestre=gE('cmbBimestre');
    var bimestre=cmbBimestre.options[cmbBimestre.selectedIndex].value;
    var arrParam=[['idUsuario',bD(idAlumno)],['idGrupoMaestro',idGrupoMaestro],['bimestre',bimestre]];
	if(bD(tDocumento)=='0')
    {
    	enviarFormularioDatos('../boletaC/reporteBoletaPrimaria.php',arrParam);
	}
    else
    {
    	enviarFormularioDatos('../boletaC/reporteBoletaPrimariaIngles.php',arrParam);
    }
}

