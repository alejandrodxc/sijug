<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo FROM 4526_ciclosEscolares WHERE situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);
	
?>
var primeraCarga=true;
var arrCiclos=<?php echo $arrCiclos?>;
var cicloActivo='<?php echo $cicloActivo?>';

    
Ext.onReady(inicializar);

function inicializar()
{
	var oConf=	{
    					idCombo:'cmbCliente',
                        anchoCombo:350,
                        campoDesplegar:'Nombre',
                        campoID:'idUsuario',
                        funcionBusqueda:6,
                        raiz:'personas',
                        nRegistros:'num',
                        paginaProcesamiento:'../Usuarios/procesarbUsuario.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br></td><td width="50"><img height="40" width="33" src="../Usuarios/verFoto.php?Id={idUsuario}"/></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idUsuario'},
                                    {name:'Paterno'},
                                    {name:'Materno'},
                                    {name:'Nombre'},
                                    {name:'Nom'},
                                    {name: 'Status'}
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	gEx('frameContenidoModificacion').load	(
                                                                                    {
                                                                                        url:'../paginasFunciones/white.php'
                                                                                    }
                                                                                 )
                                        dSet.baseParams.criterio=combo.getRawValue();
                                        dSet.baseParams.campoBusqueda=5;
                                        dSet.baseParams.listRoles="'5_0'";
                                        dSet.baseParams.oDepto=1;
                                        dSet.baseParams.oInstitucion=1;
                                        if(gE('vistaPlantel').value=='1')
                                        	dSet.baseParams.adscripcion="'<?php echo $_SESSION["codigoInstitucion"]?>'";
										gE('iU').value=-1;

                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	gE('iU').value=registro.get('idUsuario');
                                        
                                        recargarDisponibilidadHorario();
                                        
                                    }  
    				};

    
	var cmbPersonal=crearComboExtAutocompletar(oConf);
    


    new Ext.Panel (
    					{
                        	renderTo:'lblPanel',
                            width:880,
                            height:450,
                            layout:'border',
                            tbar:	[
                            			{
                                            xtype:'label',
                                            html:'<span style="color:#000; font-weight:bold">Profesor:</span>&nbsp;&nbsp;'
                                        },cmbPersonal,
                                        '-',
                                        {
                                            icon:'../images/add.png',
                                            cls:'x-btn-text-icon',
                                            text:'Registrar disponibilidad de profesor',
                                            handler:function()
                                                    {
                                                        if((gE('iU').value=='')||(gE('iU').value=='-1'))
                                                        {
                                                        	msgBox('Debe seleccionar el profesor cuya disponibilidad desea capturar');
                                                        	return;
                                                        }
                                                        
                                                       mostrarVentanaRegistroConvocatoria(); 
                                                        
                                                        
                                                    }
                                            
                                        }
                                        
                            		],
                            items:	[
                            			new Ext.ux.IFrameComponent({ 
                
                                                                          id: 'frameContenidoModificacion', 
                                                                          anchor:'100% 100%',
                                                                          region:'center',
                                                                          loadFuncion:function(iFrame)
                                                                                      {
                                                                                          /*window.scrollTo(0,150);
                                                                                          autofitIframe(iFrame);*/
                                                                                      },

                                                                          url: '../paginasFunciones/white.php',
                                                                          style: 'width:100%;height:100%' 
                                                                  })
                            		]
                            	
                            
                        }
    				)
                    
                    
	cmbCliente.focus(500,false);                    
                    
}


function recargarDisponibilidadHorario()
{
	gEx('frameContenidoModificacion').load	(
    								{
                                    	url:'../modulosEspeciales/tblConvocatoriasHorariosDisponibles.php',
                                        params:	{
                                        			cPagina:'sFrm=true',
                                                    idUsuario:gE('iU').value,
                                                    vModificacion:1
                                        		}
                                    }
    							)
}


function mostrarVentanaRegistroConvocatoria()
{
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclos,280,35,90);
    
	cmbCiclo.setValue(cicloActivo);
	cmbCiclo.on('select',function(cmb,registro)
    					{
                        	gEx('gDisponibilidadHorario').getStore().load	(
                                                                                {
                                                                                    url:'../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'
                                                                                }
                                                                            )
                        }
    			)
    
    var cmbPermitirEdicion=crearComboExt('cmbPermitirEdicion',[['1','S\xED'],['2','No']],580,35,90);
	cmbPermitirEdicion.setValue('2');                
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<span style="color:#000">Registro de horario del profesor:</span>'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:170,
                                                            y:10,
                                                            html:'<span style="color:#900">'+gEx('cmbCliente').getRawValue()+'</span>'
                                                        },
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:40,
                                                            html:'<span style="color:#000">Especifique el ciclo escolar al cual aplicar&aacute; el horario:</span>'
                                                        },
                                                        cmbCiclo,
                                                        {
                                                        	xtype:'label',
                                                            x:400,
                                                            y:40,
                                                            html:'<span style="color:#000">Permitir al profesor la edici&oacute;n?:</span>'
                                                        },
                                                        cmbPermitirEdicion,
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:70,
                                                            html:'<span style="color:#000">Seleccione los periodos escolares cuya disponibilidad de horario desea registrar:</span>'
                                                        },
                                                        crearGridPeriodosConvocatoria()
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registro de disponibilidad de horario',
										width: 700,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: 'Continuar >>',
                                                            disabled:true,
                                                            id:'btnContinuar',
															handler: function()
																	{
                                                                    	
                                                                        
                                                                        var listPeriodos='';
                                                                        var x;
                                                                        var filas=gEx('gDisponibilidadHorario').getSelectionModel().getSelections();
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(listPeriodos=='')
                                                                            	listPeriodos=filas[x].data.idPeriodo;
                                                                            else
                                                                            	listPeriodos+=','+filas[x].data.idPeriodo;
                                                                        }
                                                                        
                                                                        
                                                                       
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        var cadObj='{"idUsuario":"'+gE('iU').value+'","idCiclo":"'+cmbCiclo.getValue()+'","periodos":"'+listPeriodos+'","idEstado":"'+cmbPermitirEdicion.getValue()+'"}';
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('frameContenidoModificacion').getFrameWindow().gEx('grid_tblTabla').getStore().reload();
                                                                                gEx('frameContenidoModificacion').getFrameWindow().abrirRegistroDisponibilidad(bE(arrResp[1]));
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=49&cadObj='+cadObj,true);

                                                                        
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridPeriodosConvocatoria()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPeriodo'},
		                                                {name: 'periodo'},
                                                        {name: 'tipoPeriodicidad'},
		                                                {name:'situacion'},
                                                        {name: 'comentarios'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'periodo', direction: 'ASC'},
                                              groupField: 'tipoPeriodicidad',
                                              remoteGroup:false,
                                              remoteSort: true,
                                              autoLoad:true
                                              
                                          }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='48';
                                        proxy.baseParams.idCiclo=gEx('cmbCiclo').getValue();
                                        proxy.baseParams.idUsuario=gE('iU').value;
                                    }
                        )   
       
       
       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                    	chkRow,
                                                    	{
                                                            header:'Tipo de periodo',
                                                            width:220,
                                                            sortable:true,
                                                            dataIndex:'tipoPeriodicidad'
                                                            
                                                        },
                                                         {
                                                            header:'',
                                                            width:40,
                                                            sortable:true,
                                                            dataIndex:'situacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='1')		
                                                                        	return '<img src="../images/icon_big_tick.gif" width="13" height="13">';
                                                                         return '<img src="../images/cross.png" width="13" height="13">';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'Periodo',
                                                            width:220,
                                                            sortable:true,
                                                            dataIndex:'periodo'
                                                            
                                                        },
                                                        
                                                        {
                                                            header:'Comentarios',
                                                            width:600,
                                                            sortable:true,
                                                            dataIndex:'comentarios',
                                                            renderer:mostrarValorDescripcion
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gDisponibilidadHorario',
                                                    store:alDatos,
													x:10,
                                                    y:70,
                                                    height:180,
                                                    frame:false,
                                                    cm: cModelo,
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    columnLines : true,
                                                    sm:chkRow,
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: true,
                                                                                        enableGrouping :true,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: true,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
                                            
	chkRow.on('rowselect',function(cmb,nfila,registro)
    						{
                            	validarPeriodosSel();
                            }
    		)                                            
	
    chkRow.on('rowdeselect',function(cmb,nfila,registro)
    						{
                            	validarPeriodosSel();
                            }
    		) 
                                                
    return 	tblGrid;	
}

function validarPeriodosSel()
{
	var filas=gEx('gDisponibilidadHorario').getSelectionModel().getSelections();
    var x;
    gEx('btnContinuar').disable();

    if(filas.length==0)
    	return;
    
    var habilitar=true;
    
    for(x=0;x<filas.length;x++)
    {
    	if(filas[x].data.situacion=='0')
        {
        	habilitar=false;
        	break;
        }
    }
    
    if(habilitar)
    	gEx('btnContinuar').enable();
    
}


function recargarContenedorCentral()
{
	gEx('frameContenidoModificacion').getFrameWindow().recargarContenedorCentral();
}

function imprimirFormato(url,id)
{
    if(url)
    {
        var arrParam=[['idRegistro',id]];
        enviarFormularioDatosV(url,arrParam,'POST','iImprimir');
	}    
}

function imprimirContenido(ctrl)
{
	if(!primeraCarga)
    {
		ctrl.contentWindow.print();

    }
    primeraCarga=false;
}