<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT numEtapa,nombreEtapa FROM 4037_etapas WHERE idProceso=186";
	$arrEtapas="";
	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$o="['".$fila[0]."','".removerCerosDerecha($fila[0]).".- ".cv($fila[1])."']";
		if($arrEtapas=="")
			$arrEtapas=$o;
		else
			$arrEtapas.=",".$o;
	}
	
	
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo FROM 4526_ciclosEscolares WHERE situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);


	
?>
var primeraCarga=true;
var arrCiclos=<?php echo $arrCiclos?>;
var cicloActivo='<?php echo $cicloActivo?>';



Ext.onReady(inicializar);

function inicializar()
{
	arrCiclos.splice(0,0,['0','Cualquier ciclo']);
	if(gE('vModificacion').value=='0')
		crearGridConvocatoriasParticipadas();
    else
    {
    	new Ext.Viewport(	
        					{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                items:	[
                                                            crearGridConvocatoriasParticipadas()
                                                        ]
                                            }
                                         ]
                            }
                        )   
    }
}
	
function crearGridConvocatoriasParticipadas()
{

	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclos,0,0,140);
	cmbCiclo.setValue('0');
	cmbCiclo.on('select',function(cmb,registro)
    					{
                        	gEx('grid_tblTabla').getStore().load	(
                            											{
                                                                        	url:'../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'
                                                                        }
                            										)
                        }
    			)
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'nombreciclo'},
                                                        {name:'codigo'},
                                                        {name:'situacion'},
                                                        {name:'descripcion'},
                                                        {name:'periodosConsiderados'},
                                                        {name:'periodoDel'},
                                                        {name:'periodoAl'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreciclo', direction: 'ASC'},
                                                            groupField: 'nombreciclo',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='47';
                                        proxy.baseParams.idUsuario=gE('iU').value;
                                        proxy.baseParams.idCiclo=gEx('cmbCiclo').getValue();
                                        
                                    }
                        )  
                        
                        
	alDatos.on('load',function(proxy)
    								{
                                    	buscarConvocatoriasDisponibles();
                                    }
                        )                          
                         
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                           	
                                                            {
                                                                header:'',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'idRegistro',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        },
                                                                hidden:true,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Ciclo escolar',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'nombreciclo',
                                                                align:'left',
                                                                renderer:function (val)
                                                                        {
                                                                            return val;
                                                                        },
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Folio de<br>convocatoria',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'codigo',
                                                                align:'left',
                                                                renderer:formatearFolioConvocatoria,
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Situación',
                                                                width:210,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                align:'left',
                                                                renderer:formatearEtapasProceso,
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Descripción convocatoria',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                align:'left',
                                                                renderer:mostrarValorDescripcion,
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Periodos registrados',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'periodosConsiderados',
                                                                align:'left',
                                                                renderer:mostrarValorDescripcion,
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'Publicado del',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'periodoDel',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var arrDatosFecha=val.split(' ');
                                                                            
                                                                            var arrFecha=(arrDatosFecha[0]+'').split('-');
                                                                            return arrFecha[2]+'/'+arrFecha[1]+'/'+arrFecha[0];	
                                                                        },
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            },	
                                                            {
                                                                header:'al',
                                                                width:90,
                                                                sortable:true,
                                                                dataIndex:'periodoAl',
                                                                align:'left',
                                                                renderer:function(val)
                                                                        {
                                                                            if(val=='')
                                                                                return '';
                                                                            var arrDatosFecha=val.split(' ');
                                                                            
                                                                            var arrFecha=(arrDatosFecha[0]+'').split('-');
                                                                            return arrFecha[2]+'/'+arrFecha[1]+'/'+arrFecha[0];	
                                                                        },
                                                                hidden:false,
                                                                editor:null
                                                                
                                                            } 
                                                           
                                                        ]
                                                    );


		var objConf= {
                          id:'grid_tblTabla',
                          store:alDatos,
                          frame:false,
                          cm: cModelo,
                          stripeRows :true,
                          loadMask:true,
                          columnLines : true,
                          tbar:	[
                          			{	
                                    	xtype:'label',
                                        html:'<span style="color:#000"><b>Mostrar convocatorias del ciclo escolar:</b></span>&nbsp;&nbsp;'
                                    },
                                    cmbCiclo,'-',
                                    {
                                        icon:'../images/pencil_go.png',
                                        cls:'x-btn-text-icon',
                                        hidden:(gE('vModificacion').value=='0'),
                                        text:'Aperturar registro a profesor',
                                        handler:function()
                                                {
                                                	var fila= gEx('grid_tblTabla').getSelectionModel().getSelected();
                                                    if(!fila)	   
                                                    {
                                                    	msgBox('Debe seleccionar el registro de convocatoria a aperturar');
                                                    	return;
                                                    }
                                                    if(parseFloat(fila.data.situacion)==2)
                                                    {
                                                    	function funcAjax()
                                                        {
                                                            var resp=peticion_http.responseText;
                                                            arrResp=resp.split('|');
                                                            if(arrResp[0]=='1')
                                                            {
                                                                gEx('grid_tblTabla').getStore().reload();
                                                            }
                                                            else
                                                            {
                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                            }
                                                        }
                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=50&idRegistro='+fila.data.idRegistro,true);
                                                    }
                                                    else
                                                    {
                                                    	msgBox('S&oacute;lo se puede aperturar registros de convocatoria en situaci&oacute;n: 2.- Registro enviado');
                                                    }
                                                }
                                        
                                    }
                          		],
                          view:new Ext.grid.GroupingView({
                                                              forceFit:false,
                                                              showGroupName: true,
                                                              enableGrouping :true,
                                                              enableNoGroups:false,
                                                              enableGroupingMenu:false,
                                                              hideGroupedColumn: true,
                                                              startCollapsed:false
                                                          })
                      };

		if(gE('vModificacion').value=='0')
        {
        	objConf.height=450;
            objConf.width=870;
            objConf.renderTo='tblTabla';
            
        }
        else
        {
        	objConf.region='center';
            objConf.border=false;
        }
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                   objConf
                                                );
        return 	tblGrid;	

}


var arrEtapas=[<?php echo $arrEtapas?>];

function verConvocatorias(l)
{	

	var gConvocatoria=crearGridConvocatorias(bD(l));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gConvocatoria														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Convocatorias disponibles',
                                        id:'vConvocatoria',
										width: 700,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridConvocatorias(l)
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idConvocatoria'},
		                                                {name: 'descripcion'},
                                                        {name: 'cicloEscolar'},
		                                                {name:'fechaInicial', type:'date', dateFormat:'Y-m-d'},
		                                                {name:'fechaFinal', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'periodosConsiderados'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_UGM_2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicial', direction: 'ASC'},
                                                            groupField: 'fechaInicial',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='34';
                                        proxy.baseParams.lConvocatoria=l;
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Ciclo escolar',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'cicloEscolar',
                                                                renderer:function(val)
                                                                		{
                                                                        	return '<span style="color:#900"><b>'+val+'</b></span>';
                                                                        }
                                                                
                                                            },
                                                            {
                                                                header:'Periodo de publicaci&oacute;n',
                                                                width:155,
                                                                sortable:true,
                                                                align:'right',
                                                                dataIndex:'fechaInicial',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return 'Del '+val.format('d/m/Y')+' al '+registro.data.fechaFinal.format('d/m/Y');
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gConvocatorias',
                                                                store:alDatos,
                                                                x:5,
                                                                y:5,
                                                                height:360,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    getRowClass : formatearFila,
                                                                                                    enableRowBody:true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function formatearFila(record, rowIndex, p, ds) 
{
	var xf = Ext.util.Format;
    p.body = '<br><p style="margin-left: 3em;margin-right: 3em;text-align:left"><span class=""><i>'+record.data.descripcion+
    			'</i></span></p><br><p style="margin-left: 3em;margin-right: 3em;text-align:left"><b>Periodos considerados en la convocatoria:</b> '+record.data.periodosConsiderados+'</p><br>'+
                '<p style="margin-left: 3em;margin-right: 3em;text-align:right">Para participar en esta convocatoria d&eacute; click <b><a style="color:#F00" href="javascript:participarConvocatoria(\''+bE(record.data.idConvocatoria)+'\')">AQU&Iacute;</a></b></p><br>';
    return 'x-grid3-row-expanded';
}

function participarConvocatoria(iC)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	gEx('vConvocatoria').close();
            gEx('grid_tblTabla').getStore().reload();
            buscarConvocatoriasDisponibles();
            abrirRegistroDisponibilidad(bE(arrResp[1]))
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=35&cA=0&iU='+gE('iU').value+'&c='+bD(iC),true);
    
}

function formatearEtapasProceso(val,meta,registro)
{
	var comp='';
    switch(parseFloat(val))
    {
    	case 1:
        	comp='<a href="javascript:abrirRegistroDisponibilidad(\''+bE(registro.data.idRegistro)+'\')"><img src="../images/pencil.png" width="14" height="14"></a> ';
        break;
        case 2:
        	comp='<a href="javascript:abrirRegistroDisponibilidad(\''+bE(registro.data.idRegistro)+'\')"><img src="../images/magnifier.png" width="14" height="14"></a> ';
        break;
    }
	return comp+mostrarValorDescripcion(formatearValorRenderer(arrEtapas,val));
}

function abrirRegistroDisponibilidad(iR)
{
	var obj={};
    
    obj.titulo='Diponibilidad de horario';
    obj.ancho='100%';
    obj.alto=450;
    obj.url='../modulosEspeciales/frmDisponibilidadHorario.php';
    obj.params=[['cPagina','sFrm=true'],['iR',bD(iR)],['vModificacion',gE('vModificacion').value]];
    if(!window.parent.parent.abrirVentanaFancy)
    {
        if(!window.parent.abrirVentanaFancy)
            abrirVentanaFancy(obj);
        else
        {
            obj.alto='90%';
            window.parent.abrirVentanaFancy(obj);
        }
    }
    else
    {
    	obj.alto='90%';
    	window.parent.parent.abrirVentanaFancy(obj);
    }

}

function buscarConvocatoriasDisponibles()
{
	var lblMensaje='';
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            switch(arrResp[1])
            {
            	case '0':
                	lblMensaje='<span id="tblLeyenda" class="error" style="color:#000;background-color: #0D6;background-image: url(\'../Scripts/mensajesNotificacion/images/info.png\'); border:none;font-family:Arial, Helvetica, sans-serif; '+
                                'font-size:11px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;">'+
                                'Actualmente NO existe publicada alguna convocatoria para registro de disponibilidad de horario</span>';
                break;
                default:
                	var lTitulo=(arrResp[1]==1)?'existe 1 convocatoria publicada para registro de disponibilidad de horario':'existen '+arrResp[1]+' convocatorias publicadas para registro de disponibilidad de horario';
                	lblMensaje='<span id="tblLeyenda" class="error" style="color:#FFF;background-color: #900;background-image: url(\'../Scripts/mensajesNotificacion/images/alerta.png\'); border:none;font-family:Arial, Helvetica, sans-serif; '+
                                'font-size:11px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;"> '+
                                'Actualmente '+lTitulo+' en las cuales puede participar, para m&aacute;s detalles d&eacute; click <b><a style="color:#999" href="javascript:verConvocatorias(\''+bE(arrResp[2])+'\')">AQUÍ</a></b></span>';
                break;
            }
            
            gE('lblMensaje').innerHTML=lblMensaje;
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=39&iU='+gE('iU').value,true);
}

function formatearFolioConvocatoria(val,meta,registro)
{
	var comp='';
    var fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>','Y-m-d');
    if((Date.parseDate(registro.data.periodoAl,'Y-m-d')<fechaActual)&&(parseFloat(registro.data.situacion)==1))
    {
    	comp='<img src="../images/exclamation.png" width="14" height="14" title="El periodo de publicaci&oacute;n de la convocatoria ha finalizado y su registro de disponibilidad de horario NO ha sido liberada" alt="El periodo de publicaci&oacute;n de la convocatoria ha finalizado y su registro de disponibilidad de horario NO ha sido liberada"> ';
    }
    return comp+val;
}

function recargarContenedorCentral()
{
	gEx('grid_tblTabla').getStore().reload();
}

function imprimirFormato(url,id)
{
	
    if(url)
    {
        var arrParam=[['idRegistro',id]];
        enviarFormularioDatosV(url,arrParam,'POST','iImprimir');
	}    
}

function imprimirContenido(ctrl)
{
	if(!primeraCarga)
    {
		ctrl.contentWindow.print();

    }
    primeraCarga=false;
}