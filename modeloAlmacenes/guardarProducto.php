<?php

	include("latis/conexionBD.php"); 
	include_once("latis/cAlmacen.php"); 
	

	$_nombreProductovch=$_POST["_nombreProductovch"];
	$_descripcionvch=$_POST["_descripcionvch"];
	$_situacionint=$_POST["_situacionint"];
	$_idAlmacenint=-1;
	if(isset($_POST["_idAlmacenint"]))
		$_idAlmacenint=$_POST["_idAlmacenint"];
	$configuracion=$_POST["configuracion"];
	$idProducto=$_POST["idProducto"];
	$categoria=$_POST["_categoriavch"];
	$arrProductos=$_POST["arrProductos"];
	$idUnidadMedida=$_POST["_idUnidadMedidaint"];
	if($idUnidadMedida=="")
		$idUnidadMedida="NULL";
	$idTipoArticulo=$_POST["idTipoArticulo"];
	
	$categoriaIVA=$_POST["_categoriaIVAvch"];
	
	$consulta="";
	if($idProducto==-1)
	{
		$consulta="INSERT INTO 6901_catalogoProductos(nombreProducto,descripcion,situacion,idAlmacen,tipoArticulo,categoria,categoriaIVA,idUnidadMedida) 
					VALUES('".cv($_nombreProductovch)."','".cv($_descripcionvch)."',".$_situacionint.",".$_idAlmacenint.",".$idTipoArticulo.",'".$categoria."',".$categoriaIVA.",".$idUnidadMedida.")";
		
	}
	else
	{
		$consulta="update 6901_catalogoProductos set nombreProducto='".cv($_nombreProductovch)."',descripcion='".cv($_descripcionvch)."',situacion=".$_situacionint.",idUnidadMedida=".$idUnidadMedida.",
					tipoArticulo=".$idTipoArticulo.",categoria='".$categoria."',categoriaIVA='".$categoriaIVA."' where idProducto=".$idProducto;

	}
	if(!$con->ejecutarConsulta($consulta))
		return;

	$redireccionarBase=false;
	if($idProducto==-1)
	{
		$redireccionarBase=true;
		$idProducto=$con->obtenerUltimoID();
	}
	
	$consulta="SELECT dimensionar FROM 6906_categoriasProducto WHERE llave='".$categoria."'";
	$dimensionar=$con->obtenerValor($consulta);
	if($dimensionar==0)
	{
		$consulta="INSERT INTO 6909_atributosProductos(idProducto,idDimension,valor,llave) VALUES(".$idProducto.",17,'0','17:0')";	
		if(!$con->ejecutarConsulta($consulta))
			return;
		
	}
	else
	{
		$consulta="delete from 6909_atributosProductos where idProducto=".$idProducto." and idDimension=17 and valor='0' and llave='17:0'";	
		if(!$con->ejecutarConsulta($consulta))
			return;
	}

	//asociarCategoriasProducto($idProducto,$arrCategorias);

	asociarProductosContenedor($idProducto,$arrProductos);

	function asociarCategoriasProducto($idRegistro,$cadObj)
	{
		global $con;
		$obj=json_decode(bD($cadObj));
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="DELETE FROM 6907_productosVSCategorias WHERE idProducto=".$idRegistro;

		$x++;
		foreach($obj->arrCategorias as $o)
		{
			$consulta[$x]="INSERT INTO 6907_productosVSCategorias(idProducto,idCategoria) VALUES(".$idRegistro.",".$o->idCategoria.")";
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		return $con->ejecutarBloque($consulta);
	}
	
	function asociarProductosContenedor($idRegistro,$cadObj)
	{
		global $con;
		$obj=json_decode(bD($cadObj));
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="DELETE FROM 6926_productosContenedor WHERE idContenedor=".$idRegistro;

		$x++;
		foreach($obj->arrProductos as $o)
		{
			$consulta[$x]="INSERT INTO 6926_productosContenedor(idContenedor,idProducto,cantidad) VALUES(".$idRegistro.",".$o->idProducto.",".$o->cantidad.")";
			$x++;
		}
		$consulta[$x]="commit";
		$x++;
		return $con->ejecutarBloque($consulta);
	}

	renombrarProductosAsociados($idProducto);
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>

<body>
<?php
	$listCategorias=str_replace(".",",",$categoria);	
	 $consulta="SELECT distinct f.idFormulario,nombreFormulario,f.anchoGrid,f.altoGrid FROM 6922_seccionesCategoriasProducto s,900_formularios f 
				WHERE idCategoriaProducto in (".$listCategorias.") AND f.idFormulario=s.idFormulario ORDER BY orden";
	$res=$con->obtenerFilas($consulta);
	//if($con->filasAfectadas==0)
	if(!$redireccionarBase)
	{
		$arrParam[0][0]="configuracion";
		$arrParam[0][1]=$_POST["configuracion"];
		enviarPagina("../modeloAlmacenes/tblProductosAlmacen.php",$arrParam);
	}
	else
	{
		$arrParam[0][0]="confReferencia";
		$arrParam[0][1]=$_POST["configuracion"];
		$arrParam[1][0]="idAlmacen";
		$arrParam[1][1]=$_idAlmacenint;
		$arrParam[2][0]="idProducto";
		$arrParam[2][1]=$idProducto;
		$arrParam[3][0]="cPagina";
		$arrParam[3][1]="sFrm=true";
		enviarPagina("../modeloAlmacenes/productoAlmacen.php",$arrParam);	
	}
	
?>
</body>
</html>