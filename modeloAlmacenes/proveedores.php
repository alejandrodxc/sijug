<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
	$tituloModulo="Cliente/proveedor";
	$paramGET=true;
	
?>
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/rowExpander.js"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>


<link rel="stylesheet" href="../Scripts/jQueryUI/temas/smoothness/jquery-ui.css" />
<script src="../Scripts/jQuery/jquery-1.9.1.js"></script>
<script src="../Scripts/jQueryUI/jquery-ui.js"></script>



<link rel="stylesheet" href="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.css"  type="text/css" media="screen" />
<script type="text/javascript" src="../Scripts/fancyBox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>


<link rel="stylesheet" href="../Scripts/taskBar/taskbar.css" />
<script type="text/javascript" src="../modeloAlmacenes/Scripts/proveedores.js.php"></script>
<style type="text/css">
<!--
pre{
	width:600px;
	border:1px dashed #CCCCCC;
	overflow:auto;
}
-->
</style>

<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
							$idEmpresa=-1;
							
							
							$arrTipoTel[0]="Fijo";
							$arrTipoTel[1]="Celular";
							if(isset($objParametros->idEmpresa))
								$idEmpresa=$objParametros->idEmpresa;
								
							
							$referencia="";
							if(isset($objParametros->referencia))	
								$referencia=$objParametros->referencia;
							else
								if(isset($objParametros->referenciaFiltros))	
									$referencia=$objParametros->referenciaFiltros;
							$esEmpresaUsuario=0;
							if(isset($objParametros->esEmpresaUsuario))	
								$esEmpresaUsuario=$objParametros->esEmpresaUsuario;
							$ocultarClasificacion="";
							$cDirObl="";
							$vDirObl="";
							if($esEmpresaUsuario==1)
							{
								$tituloModulo="Empresa";
								$ocultarClasificacion="none";
								$cDirObl='<span style="color:#F00">*</span>';
								$vDirObl='obl';
							}
							echo formatearTituloPagina($tituloModulo,true,$idEmpresa);
										
							$consulta="SELECT * FROM 6927_empresas WHERE idEmpresa=".$idEmpresa;

							$fRegistro=$con->obtenerPrimeraFila($consulta);
							if(!$fRegistro)
							{
								if(isset($objParametros->tipoPersona))
									$fRegistro[1]=$objParametros->tipoPersona;
							}
							
							$esCliente="";
							
							$consulta="SELECT COUNT(*) FROM 6927_categoriaEmpresa WHERE idEmpresa=".$idEmpresa." AND idCategoria=1";
							$nReg=$con->obtenerValor($consulta);
							if(($nReg>0)||(isset($objParametros->esCliente)))
							{
								$esCliente='checked="checked"';
							}
							
							
							
							
							$esProveedor="";
							
							$consulta="SELECT COUNT(*) FROM 6927_categoriaEmpresa WHERE idEmpresa=".$idEmpresa." AND idCategoria=2";
							$nReg=$con->obtenerValor($consulta);
							if(($nReg>0)||(isset($objParametros->esProveedor)))
							{
								$esProveedor='checked="checked"';
							}
							
							
							$arrContactos="";
							$consulta="SELECT idContacto,nombreContacto,apPaterno,apMaterno,departamento,puesto FROM 6929_contactoEmpresa WHERE idEmpresa=".$idEmpresa." ORDER BY nombreContacto,apPaterno,apMaterno";
							$res=$con->obtenerFilas($consulta);
							while($fila=mysql_fetch_row($res))
							{
								$arrMail="";
								
								$tblEmail='<table><tr><td width="250"><b>E-mail</b></td></tr>';
								
								$consulta="SELECT mail FROM 6929_emailContacto WHERE idContacto=".$fila[0];
								$resMail=$con->obtenerFilas($consulta);
								while($fMail=mysql_fetch_row($resMail))
								{
									$tblEmail.='<tr height="21"><td>'.$fMail[0].'</td></tr>';
									$o='{"email":"'.$fMail[0].'"}';
									if($arrMail=="")
										$arrMail=$o;
									else
										$arrMail.=",".$o;
								}
								$tblEmail.='</table>';
								$arrTelefono="";
								$tblTel='<table><tr><td width="80"><b>Tipo</b></td><td width="80"><b>Lada</b></td><td width="80"><b>Tel&eacute;fono</b></td><td width="80"><b>Extensi&oacute;n</b></td></tr>';
								$consulta="SELECT tipo,lada,telefono,extension FROM 6929_telefonoContacto WHERE idContacto=".$fila[0];
								$resTel=$con->obtenerFilas($consulta);
								while($fTel=mysql_fetch_row($resTel))
								{
									$o='{"tipo":"'.$fTel[0].'","lada":"'.$fTel[1].'","telefono":"'.$fTel[2].'","extension":"'.$fTel[3].'"}';
									if($arrTelefono=="")
										$arrTelefono=$o;
									else
										$arrTelefono.=",".$o;
									$tblTel.='<tr height="21"><td>'.$arrTipoTel[$fTel[0]].'</td><td>'.$fTel[1].'</td><td>'.$fTel[2].'</td><td>'.$fTel[3].'</td></tr>';
								}
								$tblTel.='</table>';
								
								
								 $tblDetalle='<table><tr><td width:"145"><span style="color:#900"><b>E-mail de contacto:</b><span></td><td>'.$tblEmail.
								 			'</td></tr><tr><td ><span style="color:#900"><b>Tel&eacute;fono de contacto:</b><span></td><td>'.$tblTel.'</td></tr></table>';
								
								
								$cadObj='{"idContacto":"'.$fila[0].'","apPaterno":"'.cv($fila[2]).'","apMaterno":"'.cv($fila[3]).
										'","nombre":"'.cv($fila[1]).'","puesto":"'.cv($fila[5]).
										'","departamento":"'.cv($fila[4]).'","arrMail":['.$arrMail.'],"telefono":['.$arrTelefono.']}';

								$o="['".$fila[0]."','".cv($fila[1]." ".$fila[2]." ".$fila[3])."','".cv($fila[5])."','".cv($fila[4])."','".bE($cadObj)."','".$tblDetalle."']";	
								if($arrContactos=="")
									$arrContactos=$o;
								else
									$arrContactos.=",".$o;
							}
							$arrContactos="[".$arrContactos."]";
							
							
							$arrDestinatarios="";
							$consulta="SELECT idContacto FROM 6929_contactoEmpresa WHERE idEmpresa=".$idEmpresa." AND destinatarioFactura=1 ORDER BY nombreContacto";
							$resCon=$con->obtenerFilas($consulta);
							while($fila=mysql_fetch_row($resCon))
							{
								$o="['".$fila[0]."','','']";
								if($arrDestinatarios=="")
									$arrDestinatarios=$o;
								else
									$arrDestinatarios.=",".$o;
							}

							$arrDestinatarios="[".$arrDestinatarios."]";
							
							$arrBuzones="[]";
							
							$accionCancelar="cancelarOperacion()";
							if(isset($objParametros->accionCancelar))
								$accionCancelar=bD($objParametros->accionCancelar);
							$eJs="";
							if(isset($objParametros->eJs))
								$eJs=($objParametros->eJs);
								
							$asuntoMensajeComprobante="Comprobante CFDI";
							$cuerpoMensajeComprobante="Estimado @nombreCompleto:<br><br> Por este medio se le hace llegar un nuevo comprobante fiscal digital producto del servicio proporcionado por @empresaEmisora.";
							$asuntoMensajeNomina="Comprobante de Pago de Nómina";
							$cuerpoMensajeNomina="Estimado @nombreCompleto:<br><br> Por este medio se le hace llegar su comprobante de pago de nómina por los servicios prestados a @empresaEmisora";
							if($fRegistro[25]!="")
								$asuntoMensajeComprobante=$fRegistro[25];
							if($fRegistro[26]!="")
							$cuerpoMensajeComprobante=$fRegistro[26];
							if($fRegistro[27]!="")
								$asuntoMensajeNomina=$fRegistro[27];
							if($fRegistro[28]!="")
								$cuerpoMensajeNomina=$fRegistro[28];
								
							$situacionManifiesto=0;
							$consulta="SELECT situacionManifiesto FROM 6927_empresas WHERE idEmpresa=".$idEmpresa;
							$situacionManifiesto=$con->obtenerValor($consulta);	
								
						?>
                        	
                        
                        	
                            <table width="700">
                            	<?php
									if(($situacionManifiesto==0)&&($esEmpresaUsuario==1))
									{
								?>
                            	<tr>
                                	<td colspan="2">
                                    	
												<br />
												<span id="tblLeyenda" class="error" style="color:#000;background-color: #FFBABA;background-image: url('../Scripts/mensajesNotificacion/images/error.png'); border:none;font-family:Arial, Helvetica, sans-serif; 
												    font-size:13px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;">
                                                    
                                                   			 A&uacute;n NO ha aceptado el contrato de prestación de servicios, para proceder a la firma de click  <a href="javascript:mostrarFirmaManifiesto()"><b>AQU&Iacute;</b></a>
                                                    
													<?php 
														
													?>
												</span>
                                                <br />
                                                <br />
                                                <br />
                                    </td>
                                </tr>
                                <?php
									}
								?>
                                <?php
									if(($situacionManifiesto==1)&&($esEmpresaUsuario==1))
									{
								?>
                            	<tr>
                                	<td colspan="2">
                                    	
												<br />
												<span id="tblLeyenda" class="error" style="color: #000;background-color:#C60;background-image: url('../Scripts/mensajesNotificacion/images/info.png'); border:none;font-family:Arial, Helvetica, sans-serif; 
												    font-size:13px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;">
                                                    
                                                  	Su contrato de prestación de servicios esta en proceso de VoBo.                                                 
													<?php 
														
													?>
												</span>
                                                <br />
                                                <br />
                                                <br />
                                    </td>
                                </tr>
                                <?php
									}
								?>
                                <?php
									if(($situacionManifiesto==2)&&($esEmpresaUsuario==1))
									{
								?>
                            	<tr>
                                	<td colspan="2">
                                    	
												<br />
												<span id="tblLeyenda" class="error" style="color: #FFF;background-color:#030;background-image: url('../Scripts/mensajesNotificacion/images/exito.png'); border:none;font-family:Arial, Helvetica, sans-serif; 
												    font-size:13px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;">
                                                    
                                                  	Su contrato de prestación de servicios ha sido autorizado.      
													<?php 
														
													?>
												</span>
                                                <br />
                                                <br />
                                                <br />
                                    </td>
                                </tr>
                                <?php
									}
								?>
                            	<tr>
                                	<td align="left" >
                                    	
                                    	<form method="post" action="../paginasFunciones/guardarDatos.php" id="frmEnvio" enctype="multipart/form-data">
                                    	<fieldset class="frameHijoV3"><legend>Datos generales</legend>
                                    	<table>
                                        <tr>
                                        	<td align="left">
                                        		
                                                <table>
                                                	<tr height="21" style="display:<?php echo $ocultarClasificacion?>">
                                                        <td width="180"><span class="letraAzulSimple">Clasificaci&oacute;n:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450">
                                                        	<input type="checkbox" id="esCliente" <?php echo $esCliente?> /> <span class="letraAzulSimple" style="color:#000">Cliente</span>&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" id="esProveedor" <?php echo $esProveedor?>/> <span class="letraAzulSimple" style="color:#000">Proveedor</span>
                                                        </td>
                                                    </tr>
                                                	<tr height="21">
                                                        <td width="180"><span class="letraAzulSimple">Tipo de empresa:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450">
                                                        	<input type="radio" name="tipoEmpresa" id="tipoEmpresa1" value="1" onclick="tipoEmpresaCheck(this)" /> <span class="letraAzulSimple" style="color:#000">Persona física</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="radio" name="tipoEmpresa" id="tipoEmpresa2" value="2" onclick="tipoEmpresaCheck(this)"/> <span class="letraAzulSimple" style="color:#000">Persona moral</span>
                                                        	<input type="hidden"  name="_tipoEmpresaint" id="_tipoEmpresaint" value="<?php echo $fRegistro[1]?>" val="obl" campo="Tipo de empresa" />
                                                        </td>
                                                    </tr>
                                                    <tr height="21" >
                                                        <td ><span class="letraAzulSimple">Cve. Empresa:</span> <span style="color:#F00"></span></td>
                                                        <td width="450"><input type="text" size="15" name="_cveEmpresavch" id="_cveEmpresavch" value="<?php echo $fRegistro[24]?>" val="" campo="" /></td>
                                                    </tr>
                                                    <tr height="21" >
                                                        <td ><span class="letraAzulSimple">RFC:</span> <span style="color:#F00"></span></td>
                                                        <td width="450">
                                                        	<input type="text" size="10" maxlength="10" name="_rfc1vch" id="_rfc1vch" value="<?php echo $fRegistro[5]?>" val="" campo="RFC" /> -
                                                            <input type="text" size="10" maxlength="10" name="_rfc2vch" id="_rfc2vch" value="<?php echo $fRegistro[6]?>" val="" campo="RFC" /> - 
                                                            <input type="text" size="10" maxlength="10" name="_rfc3vch" id="_rfc3vch" value="<?php echo $fRegistro[7]?>" val="" campo="RFC" />
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr height="21" id="fPaterno">
                                                        <td ><span class="letraAzulSimple">Apellido Paterno:</span> <span style="color:#F00"></span></td>
                                                        <td width="450"><input type="text" size="30" name="_apPaternovch" id="_apPaternovch" value="<?php echo $fRegistro[3]?>" val="" campo="Apellido Paterno" /></td>
                                                    </tr>
                                                    <tr height="21" id="fMaterno">
                                                        <td ><span class="letraAzulSimple">Apellido Materno:</span> <span style="color:#F00"></span></td>
                                                        <td width="450"><input type="text" size="30" name="_apMaternovch" id="_apMaternovch" value="<?php echo $fRegistro[4]?>" val="" campo="Apellido Materno" /></td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td ><span class="letraAzulSimple" id="lblRazonSocial">Razón Social:</span> <span style="color:#F00">*</span></td>
                                                        <td width="450"><input type="text" size="70" name="_razonSocialvch" id="_razonSocialvch" value="<?php echo $fRegistro[2]?>" val="obl" campo="Nombre del proveedor" /></td>
                                                    </tr>
                                                    
                                                    
                                                    <tr height="21">
                                                        <td  valign="top"><span class="letraAzulSimple">Situaci&oacute;n:</span></td>
                                                        <td >
                                                        	<select id="_situacionint" name="_situacionint">
                                                        <?php
                                                        	$consulta="SELECT claveElemento,nombreElemento FROM 1018_catalogoVarios WHERE tipoElemento=1";
															echo $con->generarOpcionesSelect($consulta,$fRegistro[8]);
														?>
                                                        	</select>
                                                        </td>
                                                    </tr>
                                                    
                                                    <?php
														if($esEmpresaUsuario==1)
														{
													?>
                                                    <tr height="21">
                                                        <td valign="top"><span class="letraAzulSimple" id="">Logo de la empresa:</span> </td>
                                                        <td width="450">
                                                        	<table>
                                                            <tr>
                                                            	<td valign="top">
                                                                	<input type="file" name="logoEmpresa"  />
			                                                        <input type="hidden" name="_logoEmpresafil" value="<?php echo $fRegistro[23] ?>" /><br /><br />
                                                                
                                                               <?php
															   	if(($fRegistro[23]!="")&&($fRegistro[23]!=-1))
																{
																	$imagen=$baseDir."/documentosUsr/archivo_".$fRegistro[23];
																	$datosImagen=getimagesize($imagen);
																	
																	$altoBase=110;
																	
																	if($datosImagen[1]>$altoBase)
																	{
																		$porcentaje=$altoBase/$datosImagen[1];
																		$ancho=$datosImagen[0]*$porcentaje;
																	
																	}
																	else
																	{
																		
																		$porcentaje=($datosImagen[1]/110);	
																		$ancho=$datosImagen[0]*$porcentaje;
																	}
																	
															   ?>
                                                                <img src="../paginasFunciones/obtenerArchivos.php?id=<?php echo bE($fRegistro[23])?>" width="<?php echo $ancho?>" height="<?php echo $altoBase?>" />
                                                               	<?php
																}
																?>
                                                                </td>
                                                            </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    <?php
														}
													?>
                                                    
                                                    <tr height="21">
                                                    </tr>
                                                    
                                                </table>
                                                

                                                
                                                <input type="hidden" name="tabla" value="6927_empresas" />
                                                <input type="hidden" name="id" id="idEmpresa" value="<?php echo $idEmpresa?>" />
                                                <input type="hidden" name="campoId" value="idEmpresa" />
                                                
                                                <?php
													$pagRedireccion="../modeloAlmacenes/tblProveedores.php";
													$valorPost=$nConfRegresar;
													if($esEmpresaUsuario==1)
													{
														if($idEmpresa==-1)
														{
															$valorPost=$nConfiguracion;
															$pagRedireccion="../modeloAlmacenes/proveedores.php";
												?>
                                                	<input type="hidden" name="reemplazarIDSesion"  value="<?php echo $nConfiguracion?>" />
                                                    <input type="hidden" name="sentenciaReemplazo" value='"idEmpresa":"-1"' />
                                                    <input type="hidden" name="valorReemplazo" value='"idEmpresa":"idRegPadre"' />
                                                    
                                                <?php
														}
														else
														{
															
															$pagRedireccion="../modeloAlmacenes/tblEmpresas.php";
														}
													}
													
												?>
                                                
                                                <input type="hidden" name="valorPost" value='<?php echo $valorPost?>' />
                                                <input type="hidden" name="paramPost" value='[{"nombreP":"configuracion","valorP":"<?php echo $valorPost?>"}]' />
                                                <input type="hidden" name="pagRedireccion" value="<?php echo $pagRedireccion?>"/>	
                                                <input type="hidden" name="_esEmpresaUsuarioint" id="esEmpresaUsuario" value="<?php echo $esEmpresaUsuario?>" />
                                                <input type="hidden" name="_referenciavch" id="_referenciavch" value="<?php echo $referencia?>" />
                                                <input type="hidden" name="eJs" id="eJs" value="<?php echo $eJs?>" />
                                            </td>
                                       	</tr>
                                        </table>
                                        </fieldset><br /><br />
                                        <div id="tabs">
                                        <ul>
                                            <li><a href="#tabs-1">Dirección</a></li>
                                            <li><a href="#tabs-2">Contactos</a></li>
                                            <li><a href="#tabs-3">Facturación Electrónica</a></li>
                                            <?php
												if($esEmpresaUsuario==1)
												{
											?>
                                            <li><a href="#tabs-4">E-mail de Comprobantes Fiscales</a></li>
                                            <li><a href="#tabs-5">E-mail de N&oacute;mina</a></li>
                                            <?php
												}
											
												//if(existeRol("'110_0'"))
												{
											?>
                                            <li><a href="#tabs-6">Buzón de recepción de comprobantes</a></li>

                                            <?php
												}
											?>
                                        </ul>
                                        <div id="tabs-1" style="background-color:rgb(240, 240, 240);">
                                        
                                    	<table width=780>
                                        <tr>
                                        	<td align="left">
                                        		
                                                <table>
                                                	
                                                    	
                                                        <tr height="21">
                                                            <td  width="120"><span class="letraAzulSimple">Estado:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <select  id="_estadovch" name="_estadovch" onchange="estadoSel(this)" val="<?php echo $vDirObl?>" campo="Estado" >
                                                                    <option value="-1">Seleccione</option>
                                                                <?php
                                                                    $consulta="sELECT cveEstado,estado FROM 820_estados ORDER BY estado";
                                                                    $con->generarOpcionesSelect($consulta,$fRegistro[13]);
                                                                ?>	
                                                                    </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Municipio:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <select  id="_municipiovch" name="_municipiovch" val="<?php echo $vDirObl?>" campo="Municipio">
                                                                    <option value="-1">Seleccione</option>
                                                                <?php
                                                                    if($fRegistro[13]!="")
                                                                    {
                                                                        $consulta="SELECT cveMunicipio,municipio FROM 821_municipios WHERE cveEstado='".$fRegistro[13]."' ORDER BY municipio";
                                                                        $con->generarOpcionesSelect($consulta,$fRegistro[14]);
                                                                    }
                                                                ?>	
                                                                    </select>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Localidad:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_localidadvch" id="_localidadvch" value="<?php echo $fRegistro[17]?>" size="40" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td ><span class="letraAzulSimple">Calle:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <input type="text" name="_direccionvch" id="_direccionvch" value="<?php echo $fRegistro[8]?>"  size="40" val="<?php echo $vDirObl?>" campo="Calle"/>
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">No. Exterior.:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_numerovch" id="_numerovch" value="<?php echo $fRegistro[9]?>" size="30" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">No. Interior.:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_numeroIntvch" id="_numeroIntvch" value="<?php echo $fRegistro[19]?>" size="30" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">Colonia:</span> <span style="color:#F00"></span></td>
                                                            <td width="450">
                                                                <input type="text" name="_coloniavch" id="_coloniavch" value="<?php echo $fRegistro[10]?>" size="60" />
                                                            </td>
                                                        </tr>
                                                        <tr height="21">
                                                            <td ><span class="letraAzulSimple">C.P.:</span> <?php echo $cDirObl?></td>
                                                            <td width="450">
                                                                <input type="text" name="_codPostalvch" id="_codPostalvch" value="<?php echo $fRegistro[11]?>" size="10" onkeypress="return soloNumero(event,false,false,this)"  val="<?php echo $vDirObl?>" campo="C.P."/>
                                                            </td>
                                                        </tr>
                                               	</table> 
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        
                                        <br /><br />
                                        </div>
                                        <div id="tabs-2" style="background-color:rgb(240, 240, 240);">
                                    	<table width="790">
                                        <tr>
                                        	<td align="left">
                                        		
                                                <span id="contactos"></span>
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        </div>
                                        <div id="tabs-3" style="background-color:rgb(240, 240, 240);">
                                    	<table width="790">
                                        <?php
											if($esEmpresaUsuario==1)
											{
												$consulta="SELECT idRegistro,registroPatronal FROM 6927_empresaRegistroPatronal WHERE idEmpresa=".$idEmpresa;
												$aRegistroPatronal=$con->obtenerFilasArreglo($consulta);
										?>
                                        
                                        <tr>
                                        	<td>
                                            	<table>
                                                	<tr>
                                                    	<td>
                                                        <span class="letraAzulSimple" id="lblRazonSocial">Régimen fiscal:</span> <?php echo $cDirObl?>
                                                        </td>
                                                        <td width="5">
                                                        </td>
                                                        <td>
                                                        <span class="letraAzulSimple" id="">No. registro patronal:</span> 
                                                        
                                                        
                                                        
                                                        <input type="hidden" id="aRegistroPatronal" value="<?php echo bE($aRegistroPatronal)?>" />
                                                        </td>
                                                        
                                                    </tr>
                                                	<tr>
                                                    	
                                                        
                                                        <td width="450">
                                                        
                                                        
                                                        <span id="tblRegimen"></span>
                                                        <input type="hidden" size="90" name="_regimenFiscalvch" id="_regimenFiscalvch" value="<?php echo $fRegistro[21]?>" val="<?php echo $vDirObl?>" campo="Régimen fiscal" />
                                                        </td>
                                                   		<td width="5">
                                                        </td>
                                                        
                                                        <td width="450">
                                                        <span id="tblRegPatronal"></span>
                                                        
                                                        </td>
                                                    
                                                        
                                                    </tr>
                                                </table>
                                            
                                            
                                            </td>
                                        </tr>
                                        <?php
											}
										?>
                                        
                                        <tr>
                                        	<td align="left">
												<br />
                                                <span class="letraAzulSimple" >Especifique los destinatarios que recibir&aacute;n cada factura electr&oacute;nica emitida:</span>
                                                <br />
                                                <span id="destinatarios"></span>
                                                <br /><br />
                                                <?php
													if(($esEmpresaUsuario==1)&&($idEmpresa!=-1))
													{
												?>
                                                <span class="letraAzulSimple" style="color:rgb(9, 58, 2);" >Para administrar los Certificados de Sello Digital (CSD's) de la empresa d&eacute; click <a href="javascript:abrirAdmonCSD()"><span style="color:#F00"><b>AQUÍ</b></span></a></span>
                                                <?php
													}
												?>
                                            </td>
                                       	</tr>
                                        </table>
                                        <br />
                                        </div>
                                        <?php
                                        	if($esEmpresaUsuario==1)
											{
												$valComprobante=0;
												$compCompobante="";
												if(($fRegistro[32]==1)||($fRegistro[32]==""))
												{
													$compCompobante='checked="checked"';
													$valComprobante=1;
												}
												$valNomina=0;
												$compNomina="";
												if($fRegistro[33]==1)
												{
													$compNomina='checked="checked"';
													$valNomina=1;
												}
												
                                        ?>
                                        <div id="tabs-4" style="background-color:rgb(240, 240, 240);">
                                        	<table width="800">
                                            	<tr>
                                                	<td colspan="2"><span class="letraAzulSimple">
                                                    	<input   type="checkbox" <?php echo $compCompobante ?> id="_enviarMailComprobanteReceptorint" onclick="enviarMail(1,this)" /> Enviar por E-mail los archivos XML y PDF del comprobante timbrado al cliente</span>
                                                        <input   type="hidden" name="_enviarMailComprobanteReceptorint" id="enviarMailComprobanteReceptor" value="<?php echo $valComprobante?>"/> <br /><br />
                                                        
                                                        
                                                    </td>
                                                </tr>
                                            	<tr>
                                                	<td width="130"><span class="letraAzulSimple">Asunto del mensaje:</span></td>
                                                    <td><input type="text" size="70" id="_asuntoMensajeComprobantevch" id="_asuntoMensajeComprobantevch" value="<?php echo $asuntoMensajeComprobante?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td width="130"><span class="letraAzulSimple">E-mail del remitente:</span></td>
                                                    <td><input type="text" size="70" id="_mailRemitenteComprobantevch" id="_mailRemitenteComprobantevch" value="<?php echo $fRegistro[29]?>" /></td>
                                                </tr>
                                                <tr>
                                                	<td colspan="2">
                                                    <br />
                                                    	<span class="letraAzulSimple" >Especifique el cuerpo del mensaje que ser&aacute; enviado al destinatario del comprobante fiscal:</span>
                                                    	<br /><br>
                                            			<span id="tPanelComprobanteFiscal"></span>
                                                        <input type="hidden" id="_cuerpoMailComprobantevch" name="_cuerpoMailComprobantevch" value="<?php echo bE($cuerpoMensajeComprobante)?>" />       
                                                    </td>
                                                </tr>
                                            </table>
                                        	
                                        </div>
                                        <div id="tabs-5" style="background-color:rgb(240, 240, 240);">
                                        	<table width="800">
                                            	<tr>
                                                	<td colspan="2"><span class="letraAzulSimple"><input type="checkbox" <?php echo $compNomina ?> id="_enviarMailNominaReceptorint" onclick="enviarMail(2,this)" /> Enviar por E-mail los archivos XML y PDF del comprobante timbrado al empleado</span>
                                                    <input   type="hidden" name="_enviarMailNominaReceptorint" id="enviarMailNominaReceptor" value="<?php echo $valNomina?>"/> <br /><br />
                                                    <br /><br /></td>
                                                    
                                                </tr>
                                            	<tr>
                                                	<td width="130"><span class="letraAzulSimple">Asunto del mensaje:</span></td>
                                                    <td><input type="text" size="70" name="_asuntoMensajeNominavch" id="_asuntoMensajeNominavch"  value="<?php echo $asuntoMensajeNomina?>"/></td>
                                                </tr>
                                                <tr>
                                                	<td width="130"><span class="letraAzulSimple">E-mail del remitente:</span></td>
                                                    <td><input type="text" size="70" name="_mailRemitenteNominavch" id="_mailRemitenteNominavch" value="<?php echo $fRegistro[30]?>" /></td>
                                                </tr>
                                                <tr>
                                                	<td colspan="2">
                                                    	<br />
                                                    	<span class="letraAzulSimple" >Especifique el cuerpo del mensaje que ser&aacute; enviado al empleado:</span>
                                                    	<br /><br>
                                            			<span id="tPanelNomina"></span>
                                                        <input type="hidden" id="_cuerpoMailNominavch" name="_cuerpoMailNominavch" value="<?php echo bE($cuerpoMensajeNomina)?>" />       
                                                    </td>
                                                </tr>
                                            </table>
                                        	
                                        </div>
                                         <?php
											}
											
											
											
											
											
											
											
											  if($idEmpresa=="-1")
											  {
										  ?>
											  <input name="funcPHPEjecutarNuevo" id="funcPHPEjecutarNuevo" type="hidden"  value=""  />
											  
										  <?php
											  }
											  else
											  {
										  ?>
											  <input name="funcPHPEjecutarModif" id="funcPHPEjecutarModif" type="hidden"  value=""  />
										  <?php
											  }
											  
										 
                                        	if($esEmpresaUsuario==1)
											{
												$chkHabilitarBuzon="";
												if($fRegistro[36]!=0)		
													$chkHabilitarBuzon="checked='checked'";
													
												$consulta="SELECT * FROM 6927_datosBuzonRecepcionComprobantes WHERE idEmpresa=".$idEmpresa;	
												$fDatosBuzon=$con->obtenerPrimeraFila($consulta);
												$utilizarSSL="";
												if($fDatosBuzon[3]==1)
													$utilizarSSL="checked='checked'";
													
												$moverCorreos="";	
												if($fDatosBuzon[8]==1)
													$moverCorreos="checked='checked'";
													
												$aBuzon=explode(",",$fDatosBuzon[6]);
												if($fDatosBuzon[6]!="")
												{
													$arrBuzones="";
													foreach($aBuzon as $b)
													{
														if($arrBuzones=="")
															$arrBuzones="['".$b."']";
														else
															$arrBuzones.=",['".$b."']";
													}
													$arrBuzones="[".$arrBuzones."]";
												}
													
													
											?>
											<div id="tabs-6" style="background-color:rgb(240, 240, 240);">
												<table width=780>
                                                <tr>
                                                    <td align="left">
                                                        <table>
                                                        		<tr height="21">
                                                                    <td colspan="2">
                                                                        <input type="checkbox" <?php echo $chkHabilitarBuzon?> id="chkHabilitarRecepcionComprobantesBuzon" onclick="habilitadoImportacionClick(this)"/> <span class="letraAzulSimple">Habilitar la importaci&oacute;n de comprobantes fiscales desde un buz&oacute;n de correo electr&oacute;nico</span>
                                                                   		<input   type="hidden" name="_habilitarBuzonRecepcionint" id="habilitarBuzonRecepcion" value="<?php echo $fRegistro[36]?>"/>
                                                                        <BR /><BR />
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  width="135"><span class="letraAzulSimple">URL del Servidor:</span><span style="color:#F00">*</span> </td>
                                                                    <td width="450">
                                                                        <input type="text" id="urlServidorEmail" size="60" value="<?php echo $fDatosBuzon[1]?>" />
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  ><span class="letraAzulSimple">Puerto de conexión:</span><span style="color:#F00">*</span> </td>
                                                                    <td >
                                                                        <input type="text" id="puertoConexion" size="10" onkeypress="return soloNumero(event,false,false,this)" value="<?php echo $fDatosBuzon[2]?>" />
                                                                        &nbsp;&nbsp;<input type="checkbox" id="chkUtilizarSSL" <?php echo $utilizarSSL?>/> <span class="letraAzulSimple">Utilizar Capa de Conexión Segura (SSL)</span>
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  ><span class="letraAzulSimple">Usuario/E-mail:</span><span style="color:#F00">*</span> </td>
                                                                    <td >
                                                                        <input type="text" id="emailConexion" size="70" value="<?php echo $fDatosBuzon[4]?>"  />
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  ><span class="letraAzulSimple">Contraseña:</span><span style="color:#F00">*</span> </td>
                                                                    <td >
                                                                        <input type="password" id="passwdConexion" size="30" value="<?php echo $fDatosBuzon[5]?>"  />
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  ><span class="letraAzulSimple">Repita la contraseña:</span> </td>
                                                                    <td >
                                                                        <input type="password" id="passwdConexion2" size="30" value="<?php echo $fDatosBuzon[5]?>"  />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Probar conexión" id="btnProbarConexion" class="btnAceptar" onclick="realizarConexionServidorCorreo()" style="width:130px;background: url(../images/database_connect.png) no-repeat left #F3F6F9;" />
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                	<td >
                                                                    </td>
                                                                    <td >
                                                                    <input type="checkbox" id="chkMoverCorreosProcesados" <?php echo $moverCorreos?>/> <span class="letraAzulSimple">Mover correos procesados</span>
                                                                    </td>
                                                                </tr>
                                                                <tr height="21">
                                                                    <td  colspan="2" align="left"><br />
                                                                    <span class="letraAzulSimple">Indique las carpetas que ser&aacute;n analizadas para b&uacute;squeda de comprobantes fiscales (Ninguno para buscar en todos):</span><br /><br />
                                                                    <span id="tDirectorios"></span>
                                                                    </td>
                                                                </tr>
                                                        </table> 
                                                        
                                                        
                                                    </td>
                                                </tr>
                                                </table>
                                                <br />
                                                
                                                <br /><br />
											</div>
                                         <?php
											}
                                          ?>
                                        </div>
                                        </form>
                                        <br /><br /><br /><br />
                                        <table width="100%">
                                        	<tr>
                                                <td align="center" colspan="2">	
												<input type="hidden" id="arrBuzones" value="<?php echo bE($arrBuzones)?>" />
                                                <input type="hidden" id="arrDestinatarios" value="<?php echo bE($arrDestinatarios)?>" />
                                                <input type="hidden" id="arrContactos" value="<?php echo bE($arrContactos)?>" />
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                	<td colspan="2" align="center">
                                    
                                     <input type="button" class="btnAceptar" value="Aceptar" onclick="validarFormulario()" />
                                  	<input type="button" class="btnCancelar" value="Cancelar" onclick="<?php echo $accionCancelar?>" />
                                    </td>
                                </tr>
                                
                            </table>
                            
			                    


                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
