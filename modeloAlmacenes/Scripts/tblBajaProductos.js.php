<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	include_once("latis/cAlmacen.php");
	
	$arrAlmacenes=obtenerAlmacenesDisponiblesJS();
	
	
	$consulta="SELECT id__988_tablaDinamica,nombreTipo FROM _988_tablaDinamica ORDER BY nombreTipo";
	$arrMotivoBaja=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT u.idUnidadMedida,IF(abreviatura='' OR abreviatura IS NULL,u.unidadMedida,CONCAT('(',u.abreviatura,') ',u.unidadMedida)) AS unidadMedida 
				FROM 6923_unidadesMedida u";
	
	
	$arrUnidadesMedida=$con->obtenerFilasArreglo($consulta);
?>

var idProductoBaja=-1;
var llaveProductoBaja='';
var arrMotivoBaja=<?php echo $arrMotivoBaja?>;

var arrAlmacenesDisp=[];
var arrAlmacenes=[<?php echo $arrAlmacenes?>];
var arrUnidadesMedida=<?php echo $arrUnidadesMedida?>;

Ext.onReady(inicializar);

function inicializar()
{
	arrAlmacenesDisp=arrAlmacenes.slice();
    arrAlmacenesDisp.splice(arrAlmacenesDisp.length-1,1);
    
	var cmbAlmacen=crearComboExt('cmbAlmacen',arrAlmacenes,0,0,280);
    cmbAlmacen.setValue(arrAlmacenes[arrAlmacenes.length-1][0]);
    cmbAlmacen.on('select',function()
    						{
                            	var lastOptions = gEx('gridBajas').getStore().lastOptions;
                                Ext.apply(lastOptions.params, {
                                                                    idAlmacen: gEx('cmbAlmacen').getValue()
                                                                }
                                         );
                            	gEx('gridBajas').getStore().reload(lastOptions);
                            }
    			);
    new Ext.Panel(	{
                                layout: 'border',
                                border:false,
                                renderTo:'tblBajas',
                                width:960,
                                height:550,
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                
                                               	tbar:	[
                                                			{
                                                            	xtype:'label',
                                                                html:'<span style="color:#000; font-weight:bold">Ver bajas de producto del almac&eacute;n:&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbAlmacen
                                                        ],
                                                items:	[
                                                            crearGridBajas()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridBajas()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idBaja'},
		                                                {name:'fechaBaja', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'Nombre'},
                                                        {name: 'descripcionProducto'},
                                                        {name: 'cantidadBaja'},
                                                        {name: 'idMotivoBaja'},
                                                        {name: 'comentariosAdicionales'},
                                                        {name: 'unidadMedida'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaBaja', direction: 'DESC'},
                                                            groupField: 'descripcionProducto',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='179';
                                        proxy.baseParams.idAlmacen=gEx('cmbAlmacen').getValue();
                                        
                                        

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            {type: 'string', dataIndex: 'descripcionProducto'},
                                                                            {type: 'date', dataIndex: 'fechaBaja'},
                                                                            {type: 'string', dataIndex: 'Nombre'},
                                                                            {type: 'list',dataIndex:'idMotivoBaja',options:arrMotivoBaja,phpMode:true}
                                                                            
                                                                        ]
                                                        }
                                                    );    
                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Fecha de baja',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaBaja',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s');
                                                                        }
                                                            },
                                                            {
                                                                header:'Producto',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'descripcionProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            
                                                            {
                                                                header:'Registrado por',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'Nombre',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:110,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'cantidadBaja',
                                                                renderer:function(val)
                                                                		{
                                                                        	return Ext.util.Format.number(val,'0.00');
                                                                        }
                                                            },
                                                            {
                                                                header:'Unidad de medida',
                                                                width:160,
                                                                sortable:true,
                                                                
                                                                dataIndex:'unidadMedida',
                                                                renderer:function(val)
                                                                		{
                                                                        	
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrUnidadesMedida,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Motivo de la salida',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'idMotivoBaja',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrMotivoBaja,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Comentarios adicionales',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'comentariosAdicionales',
                                                                renderer:mostrarValorDescripcion,
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridBajas',
                                                                store:alDatos,
                                                                border:false,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar baja de producto',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaRegistro();
                                                                                        }
                                                                                
                                                                            },'-'
                                                                            
                                                                            
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:179,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		                                                                                
        return 	tblGrid;
}

function mostrarVentanaRegistro()
{
	var cmbAlmacenPedido=crearComboExt('cmbAlmacenPedido',arrAlmacenesDisp,130,5,350);
    cmbAlmacenPedido.setValue(arrAlmacenesDisp[0][0]);
    var cmbMotivoBaja=crearComboExt('cmbMotivoBaja',arrMotivoBaja,130,125,450);
    var cmbUnidadMedida=crearComboExt('cmbUnidadMedida',[],225,95,220);
    
    
    var oConf=	{
    					idCombo:'txtProducto',
                        anchoCombo:500,
                        campoDesplegar:'nombreProducto',
                        campoID:'idEmpresa',
                        funcionBusqueda:175,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:130,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {nombreProducto}</table></div></tpl>',
                        campos:	[
                                    {name:'unidadMedida'},
                                    {name:'arrUnidadesMedida'},
                                    {name:'codigoAlterno'},
                                    {name:'codigoBarras'},
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'nombreProducto'},
                                    {name:'precioUnitario'},
                                    {name:'tasaIVA'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProductoBaja=-1;        
                                        llaveProductoBaja='';                                
                                        dSet.baseParams.valor=gEx('txtProducto').getRawValue();
                                        dSet.baseParams.criterio=2;
                                        dSet.baseParams.idZona=0;
                                        dSet.baseParams.categorias='';
                                        cmbUnidadMedida.getStore().removeAll();
                                        cmbUnidadMedida.setValue('');
                                        gEx('txtCantidad').setValue('');
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProductoBaja=registro.get('idProducto');
                                        llaveProductoBaja=registro.get('llave');
                                       	
                                        
                                        obtenerExistenciaProducto(registro);
                                        
                                    }  
    				};

    
	var txtProducto=crearComboExtAutocompletar(oConf);
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Almac&eacute;n:'
                                                        },
                                                        cmbAlmacenPedido,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Producto:'
                                                        },
                                                        
                                                        txtProducto,
                                                        {
                                                        	xtype:'label',
                                                            x:645,
                                                            y:35,
                                                            html:'<a href="javascript:buscarPorProductoNombre()"><img src="../images/magnifier.png" title="B&uacute;squeda avanzada"></a>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Existencia actual:'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            readOnly:true,
                                                            width:550,
                                                            x:130,
                                                            y:65,
                                                            id:'txtExistencia'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Cantidad a dar de baja:'
                                                        },
                                                        {
                                                        	xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            width:90,
                                                            x:130,
                                                            y:95,
                                                            id:'txtCantidad'
                                                        },
                                                        cmbUnidadMedida,
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Motivo de la baja:'
                                                        },
                                                        cmbMotivoBaja,
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            width:800,
                                                            height:80,
                                                            id:'txtComentarios',
                                                            xtype:'textarea'
                                                        }
														


													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vBajaProducto',
										title: 'Baja de producto',
										width: 850,
										height:360,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                		gEx('txtProducto').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtProducto=gEx('txtProducto');
                                                                        var txtCantidad=gEx('txtCantidad');
                                                                        var txtComentarios=gEx('txtComentarios');
                                                                        var txtExistencia=gEx('txtExistencia');
                                                                        if(idProductoBaja==-1)
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtProducto.focus();
                                                                            }
                                                                            msgBox('Debe indicar el producto cuya baja desea registrar',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        /*if(txtCantidad.getValue()>txtExistencia.getValue())
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgBox('La cantidad de producto a dar de baja excede la cantidad existente del mismo',resp4);
                                                                            return;
                                                                        }*/
                                                                        
                                                                        if(txtCantidad.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgBox('Debe indicar la cantidad de producto cuya baja desea realizar',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbUnidadMedida.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	cmbUnidadMedida.focus();
                                                                            }
                                                                            msgBox('Debe indicar la unidad de medida del producto a dar de baja',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbMotivoBaja.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbMotivoBaja.focus();
                                                                            }
                                                                            msgBox('Debe indicar el motivo de la baja del producto',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        function respConf(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                           		var cadObj='{"idAlmacen":"'+cmbAlmacenPedido.getValue()+'","idProducto":"'+idProductoBaja+'","llave":"'+llaveProductoBaja+
                                                                                        '","cantidad":"'+txtCantidad.getValue()+'","unidadMedida":"'+cmbUnidadMedida.getValue()+'","motivoBaja":"'+cmbMotivoBaja.getValue()+
                                                                                        '","comentarios":"'+cv(txtComentarios.getValue())+'"}';
                                                                            
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    
                                                                                    switch(arrResp[0])
                                                                                    {
                                                                                    	case '1':
                                                                                        	gEx('gridBajas').getStore().reload();
                                                                                       		ventanaAM.close();
                                                                                        break;
                                                                                        case '2':
                                                                                        	function respFinal()
                                                                                            {
                                                                                            	gEx('txtCantidad').focus(false,500);
                                                                                            }
                                                                                            msgBox('La cantidad a dar de baja NO puede ser mayor a la cantidad en existencia',respFinal)
                                                                                        break;
                                                                                        default:
                                                                                        	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        break;
                                                                                        
                                                                                    }
                                                                                    
                                                                                    
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=183&cadObj='+cadObj,true);
                                                                                   
                                                                            }
																		}
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la salida del producto?',respConf);
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function buscarPorProductoNombre()
{
	idProductoBaja=-1;
    llaveProductoBaja='';
    
	var arrCriterio=[['1','Comienza con...'],['2','Contiene la palabra...'],['3','C\xF3digo de barras'],['4','C\xF3digo alterno']];
	var cmbCriterioBusqueda=crearComboExt('cmbCriterioBusqueda',arrCriterio,170,5,250);
    cmbCriterioBusqueda.setValue('2');
    cmbCriterioBusqueda.on('select',function(cmb,registro)
    								{
                                    	gEx('txtNombre').focus(false,500);
                                        gEx('txtNombre').setValue('');
                                    	cargarProductosBusqueda();
                                        
                                        switch(registro.data.id)
                                        {
                                        	case '1':
                                            case '2':
                                            	gEx('lblDescripcion').setText('<b>Descripci&oacute;n del producto:</b>',false);
                                            break;
                                            case '3':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo de barras:</b>',false);
                                            break;
                                            case '4':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo alterno:</b>',false);
                                            break;
                                        }
                                        
                                    
                                    }
    						);
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>Criterio de b&uacute;squeda:</b>'
                                                                        },
                                                                        cmbCriterioBusqueda,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            id:'lblDescripcion',
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        {
                                                                            x:170,
                                                                            y:35,
                                                                            xtype:'textfield',
                                                                            width:330,
                                                                            enableKeyEvents:true,
                                                                            id:'txtNombre',
                                                                            listeners:	{
                                                                            				keyup:cargarProductosBusqueda
                                                                            			}
                                                                        }
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:410,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombre').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var gProductosBuscados=gEx('gProductosBuscados');
                                                                        var fila=gProductosBuscados.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto baja salida desea registrar');
                                                                            return;
                                                                        }
                                                                        obtenerExistenciaProducto(fila);
                                                                        
                                                                        
                                                                    	
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
   
      
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            {
                                                                            	var checado='';
                                                                        		
                                                                                return [
                                                                                           
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda();
}

function cargarProductosBusqueda()
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:gEx('cmbCriterioBusqueda').getValue(),
                                                            valor:gEx('txtNombre').getValue(),
                                                            idZona:0
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'precioUnitario'},
                                                        {name: 'codigoBarras'},
                                                        {name: 'codigoAlterno'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'arrUnidadesMedida'},
                                                        {name: 'tasaIVA'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo de barras',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoBarras',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo alterno',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoAlterno',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,   
                                                                listeners:	{
                                                                				rowdblclick:function(g,nFila)
                                                                                			{
                                                                                            	var fila=g.getStore().getAt(nFila);
                                                                                                obtenerExistenciaProducto(fila);
                                                                                                gEx('txtProducto').setRawValue(fila.data.nombreProducto); 
                                                                                                gEx('cmbUnidadMedida').getStore().loadData(fila.data.arrUnidadesMedida);
                                                                                                gEx('cmbUnidadMedida').setValue(fila.data.unidadMedida);
                                                                                                gEx('txtCantidad').focus();
                                                                                                gEx('vBuscarDescripcion').close();
                                                                                                
                                                                                            }
                                                                			},                                                           
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}

function obtenerExistenciaProducto(fila)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            
            gEx('txtExistencia').setValue(arrResp[1]);
                    
           	gEx('txtProducto').setRawValue(fila.data.nombreProducto); 
            gEx('cmbUnidadMedida').getStore().loadData(fila.data.arrUnidadesMedida);
            gEx('cmbUnidadMedida').setValue(fila.data.unidadMedida);
            gEx('txtCantidad').focus();
            
            if(gEx('vBuscarDescripcion'))
           	 	gEx('vBuscarDescripcion').close();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=180&idAlmacen='+gEx('cmbAlmacenPedido').getValue()+'&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave,true);
}

