<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	include_once("latis/cAlmacen.php");
	
	$arrAlmacenes=obtenerAlmacenesDisponiblesJS();
	$arrEmpresas="";	
	$consulta="SELECT idEmpresa,CONCAT(rfc1,'-',rfc2,'-',rfc3) AS rfc,IF(tipoEmpresa=1,CONCAT(apPaterno,' ',apMaterno,' ',razonSocial),razonSocial) AS empresa FROM 6927_empresas ORDER BY  empresa";
	$resEmp=$con->obtenerFilas($consulta);
	while($fEmp=mysql_fetch_row($resEmp))
	{
		$o="['".$fEmp[0]."','[".$fEmp[1]."] ".cv($fEmp[2])."']";
		if($arrEmpresas=="")
			$arrEmpresas=$o;
		else
			$arrEmpresas.=",".$o;
	}
	
	$arrEmpresas="[".$arrEmpresas."]";
	$consulta="SELECT idTipoComprobante,comprobante FROM 106_tipoComprobante where situacion=1 ORDER BY comprobante";
	$arrCategoriasDisponibles=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT u.idUnidadMedida,IF(abreviatura='' OR abreviatura IS NULL,u.unidadMedida,CONCAT('(',u.abreviatura,') ',u.unidadMedida)) AS unidadMedida 
					FROM 6923_unidadesMedida u order by unidadMedida";
	
	$arrUnidadesMedida=$con->obtenerFilasArreglo($consulta);					
?>
var arrUnidadesMedida=<?php echo $arrUnidadesMedida?>;
var arrAlmacenesDisp=[];
var arrAlmacenes=[<?php echo $arrAlmacenes?>];
var arrEmpresas=<?php echo $arrEmpresas?>;
var arrCategoriasDisponibles=<?php echo $arrCategoriasDisponibles?>;
var regProductoPedido=null;
var arrSituacion=[['1','En espera de entrega por proveedor'],['2','Cancelado'],['3','Entregado por proveedor'],['1,2,3','Cualquier situaci\xF3n']];
Ext.onReady(inicializar);

var arrFormaPago=[['1','Pagado'],['2','A cr\xE9dito'],['3','Contra entrega']];

function inicializar()
{
	arrAlmacenesDisp=arrAlmacenes.slice();
    arrAlmacenesDisp.splice(arrAlmacenesDisp.length-1,1);
	regProductoPedido=crearRegistro(	[
    										{name: 'idProducto'},
                                            {name: 'llave'},
                                            {name: 'producto'},
                                            {name: 'costoUnitario'},
                                            {name: 'subtotal'},
                                            {name: 'tasaIVA'},
                                            {name: 'iva'},
                                            {name: 'cantidad'},
                                            {name: 'total'},
                                            {name: 'unidadMedida'},
                                            {name: 'arrUnidadesMedida'}
    									]);
	var cmbAlmacen=crearComboExt('cmbAlmacen',arrAlmacenes,0,0,280);
    cmbAlmacen.setValue(arrAlmacenes[arrAlmacenes.length-1][0]);
    cmbAlmacen.on('select',function()
    						{
                            	obtenerPedidos();
                            }
    			);
    var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion,0,0,300);
    cmbSituacion.on('select',function()
    						{
                            	obtenerPedidos();
                            }
    			);
    cmbSituacion.setValue('1');
    
    
    new Ext.Panel({
                    
                    renderTo:'tblPedidos',
                    width:960,
                    height:480,
                    layout:'border',
                    tbar:	[
                                {
                                    xtype:'label',
                                    html:'<span style="color:#000; font-weight:bold">Ver pedidos del almac&eacute;n:&nbsp;&nbsp;</span>'
                                },
                                cmbAlmacen,
                                '-',
                                {
                                    xtype:'label',
                                    html:'<span style="color:#000; font-weight:bold">En situaci&oacute;n:&nbsp;&nbsp;</span>'
                                },
                                cmbSituacion
                                
                            ],
                    items:	[
                                crearGridPedidos()
                            ]
                } 
               )
}

function crearGridPedidos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
                                                        {name: 'rfc'},
		                                                {name: 'proveedor'},
                                                        {name: 'idProveedor'},
		                                                {name:'fechaCreacion', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'fechaPedido', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaEstimadaEntrega', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaRealEntrega', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'situacion'},
                                                        {name: 'total'},
                                                        {name: 'comentariosAdicionales'},
                                                        {name: 'comentariosRecepcion'},
                                                        {name: 'factura'},
                                                        {name: 'formaPago'},
                                                        {name: 'montoAbonado'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idPedido', direction: 'ASC'},
                                                            groupField: 'idPedido',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='151';
                                        proxy.baseParams.idAlmacen=gEx('cmbAlmacen').getValue();
                                        proxy.baseParams.situacion=gEx('cmbSituacion').getValue();
                                        gEx('btnRecepcion').disable();
                                        gEx('btnReImprimir').disable();

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            {type: 'string', dataIndex: 'rfc'},
                                                                            {type: 'string', dataIndex: 'proveedor'},
                                                                            {type: 'int', dataIndex: 'idPedido'},
                                                                            {type: 'date', dataIndex: 'fechaPedido'},
                                                                            {type: 'date', dataIndex: 'fechaEstimadaEntrega'},
                                                                            {type: 'date', dataIndex: 'fechaRealEntrega'},
                                                                            {type: 'date', dataIndex: 'fechaCreacion'}
                                                                            
                                                                        ]
                                                        }
                                                    );    
                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Folio del pedido',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'idPedido'
                                                            },
                                                            {
                                                                header:'Fecha de captura',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaCreacion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'RFC Proveedor',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'rfc'
                                                            },
                                                            {
                                                                header:'Proveedor',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'proveedor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Fecha del pedido',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaPedido',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha estimada<br>de entrega',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'fechaEstimadaEntrega',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
	                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Monto del pedido',
                                                                width:120,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'total',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Forma de pago',
                                                                width:120,
                                                                align:'left',
                                                                sortable:true,
                                                                dataIndex:'formaPago',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrFormaPago,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Monto abonado',
                                                                width:120,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'montoAbonado',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n del pedido',
                                                                width:250,
                                                                
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSituacion,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de recepci&oacute;n<br>en almac&eacute;n',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRealEntrega',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
	                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Factura',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'factura',
                                                                renderer:function(val)
                                                                		{
                                                                        	if((val!='')&&(val!='-1'))
	                                                                        	return '<a href="javascript:descargarFactura(\''+bE(val)+'\')"><img src="../images/download.png" /> Descargar</a>'
                                                                        }
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridPedidos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                border:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar pedido',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaRegistro();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            
                                                                            {
                                                                                icon:'../images/printer.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:true,
                                                                                text:'Reimprimir pedido',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el pedido que desea reimprimir');
                                                                                            	return;
                                                                                            }
                                                                                           
                                                                                            var arrParam=[['folioPedido',fila.data.idPedido]];
                                                                                            enviarFormularioDatos('../reportes/pedido.php',arrParam,'POST','iImprimir');//
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            
                                                                            
                                                                            
                                                                            {
                                                                                icon:'../images/icon_big_tick.gif',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnRecepcion',
                                                                                text:'Registrar recepci&oacute;n de pedido',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            mostrarVentanaRecepcion(fila);
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/cross.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnCancelacion',
                                                                                text:'Cancelar pedido',
                                                                                handler:function()
                                                                                        {
                                                                                        
                                                                                        	var fila=gEx('gridPedidos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el pedido que desea cancelar');
                                                                                                return;
                                                                                            }
                                                                                            
                                                                                            mostrarVentanaCancelacionPedido(fila);
                                                                                        
                                                                                        	

                                                                                        }
                                                                                
                                                                            },
                                                                            '-',
                                                                            {
                                                                                icon:'../images/printer.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                
                                                                                id:'btnReImprimir',
                                                                                text:'Reimprimir pedido',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=gEx('gridPedidos').getSelectionModel().getSelected();
                                                                                            
                                                                                        	var arrParam=[['idPedido',fila.data.idPedido]];
           																					enviarFormularioDatos('../reportes/Almacen/impresionPedido.php',arrParam,'POST','iImprimir');


                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:151,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    situacion:gEx('cmbSituacion').getValue(),
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
        											{
                                                    
                                                    	
                                                    	gEx('btnReImprimir').enable();
                                                    	if(registro.data.situacion=='1')
                                                        {
                                                        	gEx('btnRecepcion').enable();
                                                            gEx('btnCancelacion').enable();
                                                        }
                                                        else
                                                        {
                                                        	gEx('btnRecepcion').disable();
                                                            gEx('btnCancelacion').disable();
                                                        }
                                                    }
        							)
		tblGrid.getSelectionModel().on('rowdeselect',function(sm,nFila,registro)
        											{
                                                    
                                                    	
                                                    	gEx('btnReImprimir').disable();
                                                    	
                                                    }
        							)                                                                                                                       
        return 	tblGrid;
}

function obtenerPedidos()
{
	var gridPedidos=gEx('gridPedidos');
    var lastOptions = gridPedidos.getStore().lastOptions;
    Ext.apply(lastOptions.params, {
                                        idAlmacen: gEx('cmbAlmacen').getValue()
                                    }
              );
	Ext.apply(lastOptions.params, {
                                        situacion: gEx('cmbSituacion').getValue()
                                    }
              );
    
    
    gridPedidos.getStore().reload();
    /*gridPedidos.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:151,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    situacion:gEx('cmbSituacion').getValue()
                                                    
                                        		}
                                    }
    							);*/
}

function mostrarVentanaRegistro()
{
	var cmbFormaPago=crearComboExt('cmbFormaPago',arrFormaPago,140,95,300);
    cmbFormaPago.on('select',function(cmb,registro)
    						{
                            	gEx('lblAnticipo').show();
                                gEx('txtMontoAbonado').show();
                            	gEx('lblFechaLimite').hide();
                                gEx('ctrlFechaLimite').hide();
                                gEx('lblDiasPagos').hide();
                                gEx('ctrlDiasPago').hide();
                            	switch(registro.data.id)
                                {
                                	case '1':
                                    	gEx('lblAnticipo').hide();
                                        gEx('txtMontoAbonado').hide();
                                        gEx('txtMontoAbonado').setValue(0);
                                    break;
                                    case '2':
                                    	gEx('lblFechaLimite').show();
		                                gEx('ctrlFechaLimite').show();
                                        gEx('ctrlFechaLimite').focus();
                                    break;
                                    case '3':
                                    	gEx('lblDiasPagos').show();
		                                gEx('ctrlDiasPago').show();
                                        gEx('ctrlDiasPago').focus();
                                    break;
                                }
                            }
    
    				)
	
    var idProveedor=-1;
    
    var oConf=	{
    					idCombo:'cmbProveedor',
                        anchoCombo:550,
                        campoDesplegar:'nombreProveedor',
                        campoID:'idProveedor',
                        funcionBusqueda:2,
                        raiz:'registros',
                        nRegistros:'num',
                        posX:140,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{nombreProveedor}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProveedor'},
                                    {name:'nombreProveedor'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProveedor=-1;
                                        dSet.baseParams.funcion=204;
                                        dSet.baseParams.ref='<?php echo $referenciaFiltros ?>';
                                       
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProveedor=registro.get('idProveedor');

                                    }  
    				};
    
	var cmbProveedor=crearComboExtAutocompletar(oConf);
    
	var cmbAlmacenPedido=crearComboExt('cmbAlmacenPedido',arrAlmacenesDisp,170,5,350);
    cmbAlmacenPedido.setValue(arrAlmacenesDisp[0][0]);
    var gridProductos=crearGridProductosPedido();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Almac&eacute;n destino del pedido:'
                                                        },
                                                        cmbAlmacenPedido,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del Proveedor:'
                                                        },
                                                        cmbProveedor,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Fecha del pedido:'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaPedido',
                                                            value:'<?php echo date("Y-m-d")?>'
                                                        },
                                                        {
                                                        	x:330,
                                                            y:70,
                                                            html:'Fecha aproximada de entrega:'
                                                        },
                                                        {
                                                        	x:490,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Forma de pago:'
                                                        },
                                                        cmbFormaPago,
                                                         {
                                                        	x:490,
                                                            y:100,
                                                            hidden:true,
                                                            id:'lblFechaLimite',
                                                            html:'Fecha l&iacute;mite de pago:'
                                                        },
                                                        {
                                                        	x:610,
                                                            y:95,
                                                            hidden:true,
                                                            id:'ctrlFechaLimite',
                                                            xtype:'datefield'
                                                        },
                                                        {
                                                        	x:490,
                                                            y:100,
                                                            hidden:true,
                                                            id:'lblDiasPagos',
                                                            html:'Pagar a los (d&iacute;as)'
                                                        },
                                                        {
                                                        	x:610,
                                                            y:95,
                                                            hidden:true,
                                                            width:80,
                                                            id:'ctrlDiasPago',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false
                                                        },
                                                        gridProductos,
                                                        {
                                                        	x:10,
                                                            y:335,
                                                            hidden:true,
                                                            id:'lblAnticipo',
                                                            html:'Monto anticipo:'
                                                        },
                                                        {	
                                                        	x:155,
                                                            y:330,
                                                            id:'txtMontoAbonado',
                                                            hidden:true,
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            width:100
                                                        },
                                                        {
                                                        	x:10,
                                                            y:365,
                                                            xtype:'label',
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:155,
                                                            y:360,
                                                            xtype:'textarea',
                                                            width:550,
                                                            height:50,
                                                            id:'comentarios'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar pedido',
										width: 960,
										height:500,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                        gEx('cmbProveedor').focus(false,500);
                                                                    }
                                                                }
													},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                        	handler: function()
																	{
																		var dteFechaPedido=gEx('dteFechaPedido');	
                                                                        var dteFechaEntrega=gEx('dteFechaEntrega');	
                                                                        if(cmbAlmacenPedido.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbAlmacenPedido.focus();
                                                                            }
                                                                            msgBox('Debe especificar el almac&eacute;n destino del pedido',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(idProveedor=='-1')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbProveedor.focus();
                                                                            }
                                                                            msgBox('Debe especificar el proveedor con el cual se asociar&aacute; el pedido',resp2);
                                                                            return;
                                                                        }
                                                                        if(dteFechaPedido.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaPedido.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha del pedido',resp3);
                                                                            return;
                                                                        }
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha aproximada de entrega del pedido',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteFechaPedido.getValue()>dteFechaEntrega.getValue())
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	dteFechaPedido.focus();
                                                                            }
                                                                            msgBox('La fecha de solicitud del pedido no pude ser mayor que la fecha aproximada de entrega',resp5);
                                                                            return;
                                                                        }                                                                        
                                                                        
                                                                        if(cmbFormaPago.getValue()=='')
                                                                        {
                                                                        	function respFrm()
                                                                            {	
                                                                            	cmbFormaPago.focus();
                                                                            }
                                                                            msgBox('Debe especificar la forma de pago del pedido',respFrm);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                       
                                                                        var comp='';
                                                                        switch(cmbFormaPago.getValue())
                                                                        {
                                                                        	case '1':
                                                                            	comp='"diasPago":"","fechaLimite":"",';
                                                                            break;
                                                                        	case '2'://A credito
                                                                            	var ctrlFechaLimite=gEx('ctrlFechaLimite');
                                                                                if(ctrlFechaLimite.getValue()=='')
                                                                                {
                                                                                    function respFecha()
                                                                                    {
                                                                                        ctrlFechaLimite.focus();
                                                                                    }
                                                                                    msgBox('Debe especificar la fecha l&iacute;mite de pago del cr&eacute;dito',respFecha);
                                                                                    return;
                                                                                }
                                                                            	comp='"diasPago":"","fechaLimite":"'+ctrlFechaLimite.getValue().format('Y-m-d')+'",';
                                                                            break;
                                                                            case '3'://Contra entrega
                                                                            	 var ctrlDiasPago=gEx('ctrlDiasPago');
                                                                                if(ctrlDiasPago.getValue()=='')
                                                                                {
                                                                                    function respDiasPago()
                                                                                    {
                                                                                        ctrlDiasPago.focus();
                                                                                    }
                                                                                    msgBox('Debe especificar el n&uacute;mero de d&iacute;s en el cual el pago del pedido ser&aacute; programado',respDiasPago);
                                                                                    return;
                                                                                }
                                                                            	comp='"diasPago":"'+ctrlDiasPago.getValue()+'","fechaLimite":"",';
                                                                            break;
                                                                        }
                                                                        
                                                                        var gProductosPedidos=gEx('gProductosPedidos');
                                                                        var fila;
                                                                        var x;
                                                                        var arrProductos='';
                                                                        var aux='';
                                                                        var total=0;
                                                                        var subtotal=0;
                                                                        var iva=0;
                                                                        
                                                                        for(x=0;x<gProductosPedidos.getStore().getCount();x++)
                                                                        {
                                                                        	var fila=gProductosPedidos.getStore().getAt(x);
                                                                            aux='{"idUnidadMedida":"'+fila.data.unidadMedida+'","tasaIVA":"'+fila.data.tasaIVA+'","llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+'","costoUnitario":"'+fila.data.costoUnitario+
                                                                            	'","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+'","iva":"'+fila.data.iva+'","total":"'+
                                                                                fila.data.total+'"}';
                                                                            total+=parseFloat(fila.data.total);
                                                                            subtotal+=parseFloat(fila.data.subtotal);
                                                                            iva+=parseFloat(fila.data.iva);
                                                                            if(arrProductos=='')
                                                                            	arrProductos=aux;
                                                                            else
                                                                            	arrProductos+=','+aux
                                                                            
                                                                        }
                                                                        if(arrProductos=='')
                                                                        {
                                                                        	msgBox('Almenos debe agregar un producto al pedido');
                                                                        	return;
                                                                        }
                                                                        if(gEx('txtMontoAbonado').getValue()=='')
                                                                        {
                                                                        	gEx('txtMontoAbonado').setValue(0);
                                                                        }
                                                                        
                                                                        if(parseFloat(gEx('txtMontoAbonado').getValue())>total)
                                                                        {
                                                                        	function respMontoAbonado()
                                                                            {
                                                                            	gEx('txtMontoAbonado').focus();
                                                                            }
                                                                            msgBox('El monto abonado NO puede ser mayor al monto de la compra',respMontoAbonado);
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{'+comp+'"montoAbonado":"'+gEx('txtMontoAbonado').getValue()+'","formaPago":"'+cmbFormaPago.getValue()+'","subtotal":"'+subtotal+'","iva":"'+iva+'","total":"'+total+'","comentarios":"'+cv(gEx('comentarios').getValue())+'","arrProductos":['+arrProductos+'],"idAlmacen":"'+cmbAlmacenPedido.getValue()+'","idProveedor":"'+cmbProveedor.getValue()+
                                                                        			'","fechaPedido":"'+dteFechaPedido.getValue().format('Y-m-d')+'","fechaEntrega":"'+dteFechaEntrega.getValue().format('Y-m-d')+'"}'
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridPedidos').getStore().reload();
                                                                                var arrParam=[['idPedido',arrResp[1]]];
           																		//enviarFormularioDatos('../reportes/Almacen/impresionPedido.php',arrParam,'POST','iImprimir');
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=207&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridProductosPedido()
{
	var cmbUnidadMedida=crearComboExt('cmbUnidadMedida',[]);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'tasaIVA'},
                                                                    {name: 'iva'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'unidadMedida'},
                                                                    {name: 'arrUnidadesMedida'},
                                                                    {name: 'total'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Producto',
															width:210,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:mostrarValorDescripcion
														},
														{
															header:'Costo<br>Unitario',
															width:90,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'cantidad',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000.00');
                                                                    }
                                                                   
														},
                                                        {
															header:'Unidad de medida',
															width:110,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'unidadMedida',
                                                            editor:cmbUnidadMedida,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(registro.data.arrUnidadesMedida,val));
                                                                    }
														},
                                                        {
															header:'Subtotal',
															width:85,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'subtotal',
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Tasa IVA',
															width:80,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'tasaIVA',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false}
														},
                                                        {
															header:'IVA',
															width:85,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'iva',
                                                            renderer:'usMoney'
														},
														
														{
															header:'Total',
															width:90,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
                                                            summaryType:'sum',
															dataIndex:'total',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:125,
                                                            x:10,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidos',
                                                            columnLines : true,
                                                            height:190,
                                                            width:915,
                                                            sm:chkRow,
                                                            plugins:[summary],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	if(gEx('cmbAlmacenPedido').getValue()=='')
                                                                                        {
                                                                                        	function resp()
                                                                                            {
                                                                                            	gEx('cmbAlmacenPedido').focus();
                                                                                            }
                                                                                        	msgBox('Debe indicar el almac&eacute;n al cual pertenecer&aacute; el pedido',resp);
                                                                                        	return;
                                                                                        }
                                                                                        buscarPorProductoNombre(1);

                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	tblGrid.getStore().remove(filas);
                                                                                            if(tblGrid.getStore().getCount()==0)
	                                                                                            gEx('cmbAlmacenPedido').enable();
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	tblGrid.on('beforeedit',function(e)
    						{
                            	gEx('cmbUnidadMedida').getStore().loadData(e.record.data.arrUnidadesMedida);
                            }
              )                                                    
                                                    
	tblGrid.on('afteredit',function(e)
    						{
                            
                            	if((e.field=='costoUnitario')&&(e.value==''))
                                	e.record.set('costoUnitario',0);
                                if((e.field=='tasaIVA')&&(e.value==''))
                                	e.record.set('tasaIVA',0);
                            	if((e.field=='cantidad')&&(e.value==''))
                                	e.record.set('cantidad',0);
                                    
                                
                                var  subtotal= parseFloat(e.record.data.cantidad)* parseFloat(e.record.data.costoUnitario);
                                var iva=  subtotal*(parseFloat(e.record.data.tasaIVA)/100);  
                                    
                            	e.record.set('iva',iva);
                                
                            	e.record.set('subtotal',subtotal);
                            	e.record.set('total',subtotal+iva);
                                
                            }
    			)                                                    
	return 	tblGrid;		
}

function mostrarVentanaRecepcion(fila)
{
	
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                            x:0,
                                                            y:0,
                                                            width:960,
                                                            height:180,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Datos de recepción',
                                                            items:	[
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Folio del pedido:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:10,
                                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.data.idPedido+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                         {
                                                                            x:610,
                                                                            y:10,
                                                                            html:'Fecha de recepci&oacute;n:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:740,
                                                                            y:5,
                                                                            xtype:'datefield',
                                                                            id:'dteFechaRecepcion',
                                                                            value:'<?php echo date("Y-m-d")?>'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Proveedor:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:40,
                                                                            html:'<span class="letraRojaSubrayada8"><b>['+fila.data.rfc+'] '+fila.data.proveedor+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                       
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:70,
                                                                            html:'Comentarios adicionales:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:70,
                                                                            width:700,
                                                                            height:60,
                                                                            id:'comentarios',
                                                                            xtype:'textarea'
                                                                        }
                                                                    ]
                                                        },
                                                       {
                                                            x:0,
                                                            y:180,
                                                            width:960,
                                                            height:300,
                                                            xtype:'fieldset',
                                                            title:'Datos del pedido',
                                                            layout:'absolute',
                                                            items:	[
                                                                        crearGridProductosPedidoRecepcion(),
                                                                        {
                                                                            x:10,
                                                                            y:200,
                                                                            xtype:'label',
                                                                            html:'Comentarios del pedido:'
                                                                        },
                                                                        {
                                                                            x:150,
                                                                            y:200,
                                                                            xtype:'textarea',
                                                                            
                                                                            width:700,
                                                                            height:60,
                                                                            readOnly:true,
                                                                            id:'comentariosAdicionales'
                                                                        }
                                                                    ]
                                                        }
                                                   ]
                                                                   
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vComprobacionFiscal',
										title: 'Recepci&oacute;n de pedidos',
										width: 990,
										height:560,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('comentarios').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dteFechaRecepcion=gEx('dteFechaRecepcion');
                                                                        if(dteFechaRecepcion.getValue()=='')
                                                                        {
                                                                        	function respFechaX()
                                                                            {
                                                                            	dteFechaRecepcion.focus();
                                                                            }
                                                                            msgBox('Debe indicar la fecha de recepci&oacute;n del pedido',respFechaX);
                                                                        	return;
                                                                        }
                                                                        var comentarios=gEx('dteFechaRecepcion');
                                                                        
                                                                        function respPregunta(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                guardarDocumento();
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la recepci&oacute;n del pedido?',respPregunta);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
	
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj=eval('['+arrResp[1]+']')[0];
            
            gEx('comentariosAdicionales').setValue(escaparBR(obj.comentarios));
            gEx('gProductosPedidosRecepcion').getStore().loadData(obj.arrProductos);
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=178&idPedido='+fila.data.idPedido,true);

            
}

function crearGridProductosPedidoRecepcion()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'iva'},
                                                                    
                                                                    {name: 'cantidad'},
                                                                    {name: 'total'},
                                                                    {name: 'tasaIVA'},
                                                                    {name: 'unidadMedida'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Producto',
															width:210,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:mostrarValorDescripcion
														},
														{
															header:'Costo<br>Unitario',
															width:90,
                                                            align:'right',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000.00');
                                                                    },
															sortable:true,
															dataIndex:'cantidad'
														},
                                                         {
															header:'Unidad de medida',
															width:110,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'unidadMedida',
                                                            
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrUnidadesMedida,val));
                                                                    }
                                                        },
                                                        {
															header:'Subtotal',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'subtotal',
                                                            //editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Tasa IVA',
															width:80,
                                                            align:'right',
															sortable:true,
															dataIndex:'tasaIVA',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false}
														},
                                                        {
															header:'IVA',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'iva',
                                                            //editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
														
														{
															header:'Total',
															width:90,
                                                            align:'right',
															sortable:true,
                                                            summaryType:'sum',
															dataIndex:'total',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidosRecepcion',
                                                            columnLines : true,
                                                            height:190,
                                                            width:990,
                                                           
                                                            plugins:[summary]
                                                            
                                                        }
                                                    );
	tblGrid.on('afteredit',function(e)
    						{
                            
                            	if((e.field=='costoUnitario')&&(e.value==''))
                                	e.record.set('costoUnitario',0);
                                if((e.field=='tasaIVA')&&(e.value==''))
                                	e.record.set('tasaIVA',0);
                            	if((e.field=='cantidad')&&(e.value==''))
                                	e.record.set('cantidad',0);
                                    
                                
                                var  subtotal= parseFloat(e.record.data.cantidad)* parseFloat(e.record.data.costoUnitario);
                                var iva=  subtotal*(parseFloat(e.record.data.tasaIVA)/100);  
                                    
                            	e.record.set('iva',iva);
                                
                            	e.record.set('subtotal',subtotal);
                            	e.record.set('total',subtotal+iva);
                                
                            }
    			)                                                     
	                                                  
	return 	tblGrid;
}

function subidaCorrecta(file, serverData) 
{
	try 
    {
		file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
		var progress = new FileProgress(file, this.customSettings.progress_target);
		progress.setComplete();
		progress.setStatus("Completado");
		progress.toggleCancel(false);
		var arrDatos=serverData.split('|');
		if ( arrDatos[0]!='1') 
		{
			this.customSettings.upload_successful = false;
		} 
		else 
		{
			gEx("idArchivo").setValue(arrDatos[1]);
            gEx("nombreArchivo").setValue(arrDatos[2]);
            gEx('lblAvance').hide();
            guardarDocumento();
            
			this.customSettings.upload_successful = true;
            
		}
		
	} 
    catch (e) 
	{
		alert(e);
	}
}

function setTipoComprobante(tComprobante,lCampos)
{
	tipoComprobante=tComprobante;
    gEx('datosBoleto').hide();
    
    
    gEx('txtOrigen').setValue('');
    gEx('txtDestino').setValue('');
    gEx('cmbTipoViaje').setValue(0);
    gEx('txtFechaSalida').setValue('');
    gEx('cmbHoraSalida').setValue('00');
    gEx('cmbMinutoSalida').setValue('00');
    gEx('txtFechaRegreso').setValue('');
    gEx('cmbHoraRegreso').setValue('00');
    gEx('cmbMinutoRegreso').setValue('00');
    
    
    gEx('datosFactura').show();
    
	gE('oblFolio').innerHTML='*';
	var cmbIVAConsidera=gEx('cmbIVAConsidera');
    switch(tipoComprobante)
    {
    	case '5':
        case '1':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '6':
        case '2':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '3':
        		mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '4':
	        	mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
               
                gEx('datosFactura').hide();
                gEx('datosBoleto').show();
                
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
                
        break; 
        case '7':
                ocultarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gEx('lblFolioFiscal').show();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
        break;
        case '8':
                mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='*';
                gEx('anioAprobacion').enable();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
            break;           
    }
}

function ocultarCampos()
{
	gEx('lblFolio').hide();
    gEx('noFolio').hide();
    gEx('lblSerie').hide();
    gEx('lblNumAprobacion').hide();
    gEx('lblAnioAp').hide();
    gEx('noAprobacion').hide();
    gEx('txtNoSerie').hide();
    gEx('anioAprobacion').hide();
    gEx('lblFolioFiscal').show();
    gEx('lblSep1').show();
    gEx('folio1').show();
    gEx('lblSep2').show();
    gEx('folio2').show();
    gEx('lblSep3').show();
    gEx('folio3').show();
    gEx('lblSep4').show();
    gEx('folio4').show();
    gEx('folio5').show();
}

function mostrarCampos()
{
	gEx('lblFolio').show();
    gEx('noFolio').show();
    gEx('lblSerie').show();
    gEx('lblNumAprobacion').show();
    gEx('lblAnioAp').show();
    gEx('noAprobacion').show();
    gEx('txtNoSerie').show();
    gEx('anioAprobacion').show();
    gEx('lblFolioFiscal').hide();
    
    gEx('lblSep1').hide();
    gEx('folio1').hide();
    gEx('lblSep2').hide();
    gEx('folio2').hide();
    gEx('lblSep3').hide();
    gEx('folio3').hide();
    gEx('lblSep4').hide();
    gEx('folio4').hide();
    gEx('folio5').hide();
}

function guardarDocumento()
{
	
    var x;
    var arrConceptos='';
    var grid;
    var montoComprobacion=0;
    var fila;
    var obj;
    
    var gProductosPedidos=gEx('gProductosPedidosRecepcion');
    var fila;
    var x;
    var arrProductos='';
    var aux='';
    var total=0;
    var subtotal=0;
    var iva=0;
    
    for(x=0;x<gProductosPedidos.getStore().getCount();x++)
    {
        var fila=gProductosPedidos.getStore().getAt(x);
        aux='{"idUnidadMedida":"'+fila.data.unidadMedida+'","tasaIVA":"'+fila.data.tasaIVA+'","llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+'","costoUnitario":"'+fila.data.costoUnitario+
            '","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+'","iva":"'+fila.data.iva+'","total":"'+
            fila.data.total+'"}';
        total+=parseFloat(fila.data.total);
        subtotal+=parseFloat(fila.data.subtotal);
        iva+=parseFloat(fila.data.iva);
        if(arrProductos=='')
            arrProductos=aux;
        else
            arrProductos+=','+aux
        
    }
    
    
  
	
    var fila=gEx('gridPedidos').getSelectionModel().getSelected();
    
    var fechaRecepcionPedido='';
    if(gEx('dteFechaRecepcion').getValue()!='')
	    fechaRecepcionPedido=gEx('dteFechaRecepcion').getValue().format('Y-m-d');
        
   	var cadObj='{"idPedido":"'+fila.data.idPedido+'","arrProductos":['+arrProductos+'],"fechaRecepcionPedido":"'+fechaRecepcionPedido+'","comentariosAdicionales":"'+cv(gEx('comentarios').getValue())+'"}';
	         
	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('vComprobacionFiscal').close();
            gEx('gridPedidos').getStore().reload();
            
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=154&cadObj='+cadObj,true);         
      
}

function limpiarCampos()
{
	
    gEx('noFolio').setValue('');
    gEx('noAprobacion').setValue('');
    gEx('txtNoSerie').setValue('');
    gEx('folio1').setValue('');
    gEx('folio2').setValue('');
    gEx('folio3').setValue('');
    gEx('folio4').setValue('');
    gEx('folio5').setValue('');
}

function registrarActualizacionPedido()
{
	cerrarVentanaFancy();
    gEx('gridPedidos').getStore().reload();
    gEx('btnCancelacion').disable();
}

function descargarFactura(iF)
{
	document.location.href='../paginasFunciones/obtenerArchivos.php?id='+iF;
}

function buscarPorProductoNombre(idZona)
{
	var regProducto=null;
	var arrCriterio=[['1','Comienza con...'],['2','Contiene la palabra...'],['3','C\xF3digo de barras'],['4','C\xF3digo alterno']];
	var cmbCriterioBusqueda=crearComboExt('cmbCriterioBusqueda',arrCriterio,170,5,250);
    cmbCriterioBusqueda.setValue('2');
    cmbCriterioBusqueda.on('select',function(cmb,registro)
    								{
                                    	gEx('txtNombre').focus(false,500);
                                        gEx('txtNombre').setValue('');
                                    	cargarProductosBusqueda(1);
                                        
                                        switch(registro.data.id)
                                        {
                                        	case '1':
                                            case '2':
                                            	gEx('lblDescripcion').setText('<b>Descripci&oacute;n del producto:</b>',false);
                                            break;
                                            case '3':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo de barras:</b>',false);
                                            break;
                                            case '4':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo alterno:</b>',false);
                                            break;
                                        }
                                        
                                    
                                    }
    						);
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
    
    
    
    var oConf=	{
    					idCombo:'cmbCodigoAlterno',
                        anchoCombo:200,
                        campoDesplegar:'codigoAlterno',
                        campoID:'llaveProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:5,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'},
                                    {name: 'llaveProducto'},
                                    {name: 'idUnidadMedida'},
                                    {name: 'arrUnidadesMedida'}
                                    
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    
                                    	regProducto=null;
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbCodigoAlterno').getRawValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        dSet.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        
                                        
                                        
                                        gEx('cmbDescripcion').setValue(''); 
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	regProducto=registro;
                                       	gEx('cmbDescripcion').setRawValue(registro.data.descripcion); 
                                        
                                        
                                        
                                        var r=new  regProductoPedido	(
                                                                            {
                                                                                idProducto:regProducto.data.idProducto,
                                                                                llave:regProducto.data.llave,
                                                                                producto:regProducto.data.descripcion,
                                                                                costoUnitario:0,
                                                                                subtotal:0,
                                                                                tasaIVA:regProducto.data.tasaIVA,
                                                                                iva:0,
                                                                                cantidad:1,
                                                                                total:0,
                                                                                unidadMedida:registro.data.idUnidadMedida,
                                                                                arrUnidadesMedida:registro.data.arrUnidadesMedida
                                                                            }
                                                                        )
                                        gEx('gProductosPedidos').getStore().add(r);
                                        gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                       	gEx('vBuscarDescripcion').close();
                                        
                                    }  
    				};

    
	var cmbCodigoAlterno=crearComboExtAutocompletar(oConf);
    
     var oConf=	{
    					idCombo:'cmbDescripcion',
                        anchoCombo:330,
                        campoDesplegar:'descripcion',
                        campoID:'llaveProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'},
                                    {name: 'llaveProducto'},
                                    {name: 'idUnidadMedida'},
                                    {name: 'arrUnidadesMedida'}
                                    
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    	regProducto=null;
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbDescripcion').getRawValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        dSet.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        gEx('cmbCodigoAlterno').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    

                                    	regProducto=registro;
                                        gEx('cmbCodigoAlterno').setRawValue(registro.data.codigoAlterno); 
                                        
                                        
                                         var r=new  regProductoPedido	(
                                                                            {
                                                                                idProducto:regProducto.data.idProducto,
                                                                                llave:regProducto.data.llave,
                                                                                producto:regProducto.data.descripcion,
                                                                                costoUnitario:0,
                                                                                subtotal:0,
                                                                                tasaIVA:regProducto.data.tasaIVA,
                                                                                iva:0,
                                                                                cantidad:1,
                                                                                total:0,
                                                                                unidadMedida:registro.data.idUnidadMedida,
                                                                                arrUnidadesMedida:registro.data.arrUnidadesMedida
                                                                            }
                                                                        )
                                                                       
                                        gEx('gProductosPedidos').getStore().add(r);
                                        
                                        gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                       	
                                        gEx('vBuscarDescripcion').close();
                                       
                                        
                                       	
                                    }  
    				};

    
	var cmbDescripcion=crearComboExtAutocompletar(oConf);
    
    
    
    
 
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>C&oacute;digo alterno:</b>'
                                                                        },
                                                                        cmbCodigoAlterno,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            id:'lblDescripcion',
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        cmbDescripcion
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:440,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbCodigoAlterno').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var gProductosBuscados=gEx('gProductosBuscados');
                                                                        var fila=gProductosBuscados.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto que desea agregar');
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                    
                                                                        
                                                                        var r=new  regProductoPedido	(
                                                                        									{
                                                                                                            	idProducto:fila.data.idProducto,
                                                                                                                llave:fila.data.llave,
                                                                                                                producto:fila.data.nombreProducto,
                                                                                                                costoUnitario:0,
                                                                                                                subtotal:0,
                                                                                                                tasaIVA:fila.data.tasaIVA,
                                                                                                                iva:0,
                                                                                                                cantidad:1,
                                                                                                                total:0,
                                                                                                                unidadMedida:fila.data.idUnidadMedida,
                                                                                								arrUnidadesMedida:fila.data.arrUnidadesMedida
                                                                                                            }
                                                                        								)
                                                                        gEx('gProductosPedidos').getStore().add(r);
                                                                        gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                                                       
                                                                        funcionEjecucionBusqueda=function()
                                                                        						{
                                                                                                	gEx('txtClave').setValue('');
                                                                                                }
                                                                       
                                                                        
                                                                         
                                                                         ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    //cargarProductosBusqueda();
      
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            {
                                                                            	var checado='';
                                                                        		
                                                                                return [
                                                                                           
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda(1);
}

function cargarProductosBusqueda(idZona)
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:gEx('cmbCriterioBusqueda').getValue(),
                                                            valor:'',
                                                            idZona:idZona
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'tasaIVA'},
                                                        {name: 'precioUnitario'},
                                                        {name: 'codigoBarras'},
                                                        {name: 'codigoAlterno'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'arrUnidadesMedida'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo de barras',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoBarras',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo alterno',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoAlterno',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true, 
                                                                listeners:	{
                                                                				rowdblclick:function(grid,nFila)
                                                                                            {
                                                                                                var fila=grid.getStore().getAt(nFila);
                                                                                                console.log(fila);
                                                                                                var r=new  regProductoPedido	(
                                                                                                                                    {
                                                                                                                                        idProducto:fila.data.idProducto,
                                                                                                                                        llave:fila.data.llave,
                                                                                                                                        producto:fila.data.nombreProducto,
                                                                                                                                        costoUnitario:0,
                                                                                                                                        subtotal:0,
                                                                                                                                        tasaIVA:fila.data.tasaIVA,
                                                                                                                                        iva:0,
                                                                                                                                        cantidad:1,
                                                                                                                                        total:0,
                                                                                                                                        unidadMedida:fila.data.unidadMedida,
                                                                                                                                        arrUnidadesMedida:fila.data.arrUnidadesMedida
                                                                                                                                    }
                                                                                                                                )
                                                                                                gEx('gProductosPedidos').getStore().add(r);
                                                                                                gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                                                                               gEx('vBuscarDescripcion').close();
                                                                                            }
                                                                			} ,                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}


function mostrarVentanaCancelacionPedido(fila)
{

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el motivo de la cancelaci&oacute;n del pedido:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            id:'txtMotivo',
                                                            xtype:'textarea',
                                                            width:580,
                                                            height:80
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar pedido',
										width: 630,
										height:230,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    
                                                                    	if(gEx('txtMotivo').getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	gEx('txtMotivo').focus();
                                                                            }
                                                                        	msgBox('Debe ingresar el motivo de la cancelaci&oacute;n del pedido',resp);
                                                                        	return;
                                                                        }
                                                                    
                                                                    
																		function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridPedidos').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=208&cA='+cv(gEx('txtMotivo').getValue())+'&idPedido='+fila.data.idPedido,true);
                                                                            }
                                                                            
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar el pedido seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	


	
}