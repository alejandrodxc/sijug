<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago ";
	$arrFormaPago=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago WHERE idFormaPago IN (1,7)";
	$arrFormasPagoAbono=$con->obtenerFilasArreglo($consulta);
?>
var uploadControl;
var arrFormasPagoAbono=<?php echo $arrFormasPagoAbono?>;
var arrFormaPagoBD=<?php echo $arrFormaPago?>;
Ext.onReady(inicializar);



function inicializar()
{
	
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Adeudos a proveedor</b></span>',
                                               	
                                                items:	[
                                                            crearGridAdeudos()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearGridAdeudos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAdeudo'},
                                                        {name: 'rfc'},
		                                                {name: 'proveedor'},
                                                        {name: 'idProveedor'},
		                                                {name:'fechaCreacion', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'fechaPedido', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'fechaLimitePago', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'folioPedido'},
                                                        {name: 'montoTotal'},
                                                        {name: 'totalAbonos'},
                                                        {name: 'adeudoTotal'}
                                                        
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaCreacion', direction: 'ASC'},
                                                            groupField: 'fechaCreacion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='38';
                                       

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            {type: 'string', dataIndex: 'rfc'},
                                                                            {type: 'string', dataIndex: 'proveedor'},
                                                                            {type: 'date', dataIndex: 'fechaPedido'},
                                                                            {type: 'date', dataIndex: 'fechaEstimadaEntrega'},
                                                                            {type: 'date', dataIndex: 'fechaLimitePago'},
                                                                            {type: 'date', dataIndex: 'fechaLimitePago'},
                                                                            {type: 'int', dataIndex: 'folioPedido'}
                                                                            
                                                                        ]
                                                        }
                                                    );    
                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Folio del pedido',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'folioPedido'
                                                            },
                                                            {
                                                                header:'Fecha del pedido',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaPedido',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'RFC Proveedor',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'rfc'
                                                            },
                                                            {
                                                                header:'Proveedor',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'proveedor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Fecha L&iacute;mite de pago',
                                                                width:140,
                                                                sortable:true,
                                                                dataIndex:'fechaLimitePago',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'Monto total adeudo',
                                                                width:120,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'montoTotal',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Total abonos',
                                                                width:120,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'totalAbonos',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Total adeudo',
                                                                width:120,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'adeudoTotal',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	
                                                                        	registro.data.adeudoTotal=parseFloat(registro.data.montoTotal)-parseFloat(registro.data.totalAbonos);
                                                                            return Ext.util.Format.usMoney(registro.data.adeudoTotal);
                                                                        }
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAdeudos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/magnifier.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Ver historial de abonos',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el adeudo cuyos abonos desea observar');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            mostrarAbonosAdeudo(fila);
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesTesoreria.php',
                                        params:	{
                                        			funcion:38,
                                                    
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
        											{
                                                    	
                                                    }
        							)                                                                                   
        return 	tblGrid;
}

function obtenerPedidos()
{
	var gridPedidos=gEx('gridPedidos');
    var lastOptions = gridPedidos.getStore().lastOptions;
    Ext.apply(lastOptions.params, {
                                        idAlmacen: gEx('cmbAlmacen').getValue()
                                    }
              );
	Ext.apply(lastOptions.params, {
                                        situacion: gEx('cmbSituacion').getValue()
                                    }
              );
    
    
    gridPedidos.getStore().reload();
    /*gridPedidos.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:151,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    situacion:gEx('cmbSituacion').getValue()
                                                    
                                        		}
                                    }
    							);*/
}


function mostrarVentanaRegistro()
{

	var cmbFormaPago=crearComboExt('cmbFormaPago',arrFormaPago,140,95,170);
    cmbFormaPago.on('select',function(cmb,registro)
    						{
                            	gEx('lblFechaLimite').hide();
                                gEx('ctrlFechaLimite').hide();
                                gEx('lblDiasPagos').hide();
                                gEx('ctrlDiasPago').hide();
                            	switch(registro.data.id)
                                {
                                	case '1':
                                    break;
                                    case '2':
                                    	gEx('lblFechaLimite').show();
		                                gEx('ctrlFechaLimite').show();
                                        gEx('ctrlFechaLimite').focus();
                                    break;
                                    case '3':
                                    	gEx('lblDiasPagos').show();
		                                gEx('ctrlDiasPago').show();
                                        gEx('ctrlDiasPago').focus();
                                    break;
                                }
                            }
    
    				)
	var idProveedor=-1;
    
    var oConf=	{
    					idCombo:'cmbProveedor',
                        anchoCombo:330,
                        campoDesplegar:'nombreProveedor',
                        campoID:'idProveedor',
                        funcionBusqueda:2,
                        raiz:'registros',
                        nRegistros:'num',
                        posX:140,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesModulosEspeciales_Sigloxxi.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{nombreProveedor}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProveedor'},
                                    {name:'nombreProveedor'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProveedor=-1;
                                        dSet.baseParams.funcion=3;
                                       
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProveedor=registro.get('idProveedor');

                                    }  
    				};

    
	var cmbProveedor=crearComboExtAutocompletar(oConf);
    
	var cmbAlmacenPedido=crearComboExt('cmbAlmacenPedido',arrAlmacenesDisp,170,5,350);
    cmbAlmacenPedido.setValue(arrAlmacenesDisp[0][0]);
    var gridProductos=crearGridProductosPedido();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Almac&eacute;n destino del pedido:'
                                                        },
                                                        cmbAlmacenPedido,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del Proveedor:'
                                                        },
                                                        cmbProveedor,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Fecha del pedido:'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaPedido'
                                                        },
                                                        {
                                                        	x:330,
                                                            y:70,
                                                            html:'Fecha aproximada de entrega:'
                                                        },
                                                        {
                                                        	x:490,
                                                            y:65,
                                                            xtype:'datefield',
                                                            id:'dteFechaEntrega'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Forma de pago:'
                                                        },
                                                        cmbFormaPago,
                                                         {
                                                        	x:330,
                                                            y:100,
                                                            hidden:true,
                                                            id:'lblFechaLimite',
                                                            html:'Fecha l&iacute;mite de pago:'
                                                        },
                                                        {
                                                        	x:450,
                                                            y:95,
                                                            hidden:true,
                                                            id:'ctrlFechaLimite',
                                                            xtype:'datefield'
                                                        },
                                                        {
                                                        	x:330,
                                                            y:100,
                                                            hidden:true,
                                                            id:'lblDiasPagos',
                                                            html:'Pagar a los (d&iacute;as)'
                                                        },
                                                        {
                                                        	x:450,
                                                            y:95,
                                                            hidden:true,
                                                            width:80,
                                                            id:'ctrlDiasPago',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false
                                                        },
                                                        gridProductos,
                                                        {
                                                        	x:10,
                                                            y:335,
                                                            
                                                            
                                                            html:'Monto anticipo:'
                                                        },
                                                        {	
                                                        	x:155,
                                                            y:330,
                                                            id:'txtMontoAbonado',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            width:100
                                                        },
                                                        {
                                                        	x:10,
                                                            y:365,
                                                            xtype:'label',
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:155,
                                                            y:360,
                                                            xtype:'textarea',
                                                            width:550,
                                                            height:50,
                                                            id:'comentarios'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar pedido',
										width: 870,
										height:500,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                        	handler: function()
																	{
																		var dteFechaPedido=gEx('dteFechaPedido');	
                                                                        var dteFechaEntrega=gEx('dteFechaEntrega');	
                                                                        if(cmbAlmacenPedido.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbAlmacenPedido.focus();
                                                                            }
                                                                            msgBox('Debe especificar el almac&eacute;n destino del pedido',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(idProveedor=='-1')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbProveedor.focus();
                                                                            }
                                                                            msgBox('Debe especificar el proveedor con el cual se asociar&aacute; el pedido',resp2);
                                                                            return;
                                                                        }
                                                                        if(dteFechaPedido.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaPedido.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha del pedido',resp3);
                                                                            return;
                                                                        }
                                                                        if(dteFechaEntrega.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	dteFechaEntrega.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha aproximada de entrega del pedido',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(dteFechaPedido.getValue()>dteFechaEntrega.getValue())
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	dteFechaPedido.focus();
                                                                            }
                                                                            msgBox('La fecha de solicitud del pedido no pude ser mayor que la fecha aproximada de entrega',resp5);
                                                                            return;
                                                                        }                                                                        
                                                                        
                                                                        if(cmbFormaPago.getValue()=='')
                                                                        {
                                                                        	function respFrm()
                                                                            {	
                                                                            	cmbFormaPago.focus();
                                                                            }
                                                                            msgBox('Debe especificar la forma de pago del pedido',respFrm);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                       
                                                                        var comp='';
                                                                        switch(cmbFormaPago.getValue())
                                                                        {
                                                                        	case '1':
                                                                            	comp='"diasPago":"","fechaLimite":"",';
                                                                            break;
                                                                        	case '2'://A credito
                                                                            	var ctrlFechaLimite=gEx('ctrlFechaLimite');
                                                                                if(ctrlFechaLimite.getValue()=='')
                                                                                {
                                                                                    function respFecha()
                                                                                    {
                                                                                        ctrlFechaLimite.focus();
                                                                                    }
                                                                                    msgBox('Debe especificar la fecha l&iacute;mite de pago del cr&eacute;dito',respFecha);
                                                                                    return;
                                                                                }
                                                                            	comp='"diasPago":"","fechaLimite":"'+ctrlFechaLimite.getValue().format('Y-m-d')+'",';
                                                                            break;
                                                                            case '3'://Contra entrega
                                                                            	 var ctrlDiasPago=gEx('ctrlDiasPago');
                                                                                if(ctrlDiasPago.getValue()=='')
                                                                                {
                                                                                    function respDiasPago()
                                                                                    {
                                                                                        ctrlDiasPago.focus();
                                                                                    }
                                                                                    msgBox('Debe especificar el n&uacute;mero de d&iacute;s en el cual el pago del pedido ser&aacute; programado',respDiasPago);
                                                                                    return;
                                                                                }
                                                                            	comp='"diasPago":"'+ctrlDiasPago.getValue()+'","fechaLimite":"",';
                                                                            break;
                                                                        }
                                                                        
                                                                        var gProductosPedidos=gEx('gProductosPedidos');
                                                                        var fila;
                                                                        var x;
                                                                        var arrProductos='';
                                                                        var aux='';
                                                                        var total=0;
                                                                        var subtotal=0;
                                                                        var iva=0;
                                                                        
                                                                        for(x=0;x<gProductosPedidos.getStore().getCount();x++)
                                                                        {
                                                                        	var fila=gProductosPedidos.getStore().getAt(x);
                                                                            aux='{"llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+'","costoUnitario":"'+fila.data.costoUnitario+
                                                                            	'","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+'","iva":"'+fila.data.iva+'","total":"'+
                                                                                fila.data.total+'"}';
                                                                            total+=parseFloat(fila.data.total);
                                                                            subtotal+=parseFloat(fila.data.subtotal);
                                                                            iva+=parseFloat(fila.data.iva);
                                                                            if(arrProductos=='')
                                                                            	arrProductos=aux;
                                                                            else
                                                                            	arrProductos+=','+aux
                                                                            
                                                                        }
                                                                        if(arrProductos=='')
                                                                        {
                                                                        	msgBox('Almenos debe agregar un producto al pedido');
                                                                        	return;
                                                                        }
                                                                        if(gEx('txtMontoAbonado').getValue()=='')
                                                                        {
                                                                        	gEx('txtMontoAbonado').setValue(0);
                                                                        }
                                                                        var cadObj='{'+comp+'"montoAbonado":"'+gEx('txtMontoAbonado').getValue()+'","formaPago":"'+cmbFormaPago.getValue()+'","subtotal":"'+subtotal+'","iva":"'+iva+'","total":"'+total+'","comentarios":"'+cv(gEx('comentarios').getValue())+'","arrProductos":['+arrProductos+'],"idAlmacen":"'+cmbAlmacenPedido.getValue()+'","idProveedor":"'+cmbProveedor.getValue()+
                                                                        			'","fechaPedido":"'+dteFechaPedido.getValue().format('Y-m-d')+'","fechaEntrega":"'+dteFechaEntrega.getValue().format('Y-m-d')+'"}'
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('gridPedidos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=152&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridProductosPedido()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'iva'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'total'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Producto',
															width:250,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:mostrarValorDescripcion
														},
														{
															header:'Costo Unitario',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Cantidad',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidad',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false},
                                                            renderer:'cantidad'
														},
                                                        {
															header:'Subtotal',
															width:90,
                                                            align:'right',
															sortable:true,
															dataIndex:'subtotal',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'IVA',
															width:90,
                                                            align:'right',
															sortable:true,
															dataIndex:'iva',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
														
														{
															header:'Total',
															width:100,
                                                            align:'right',
															sortable:true,
                                                            summaryType:'sum',
															dataIndex:'total',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:125,
                                                            x:10,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidos',
                                                            columnLines : true,
                                                            height:190,
                                                            width:810,
                                                            sm:chkRow,
                                                            plugins:[summary],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	if(gEx('cmbAlmacenPedido').getValue()=='')
                                                                                        {
                                                                                        	function resp()
                                                                                            {
                                                                                            	gEx('cmbAlmacenPedido').focus();
                                                                                            }
                                                                                        	msgBox('Debe indicar el almac&eacute;n al cual pertenecer&aacute; el pedido',resp);
                                                                                        	return;
                                                                                        }
                                                                                        buscarPorProductoNombre();
                                                                                        return;
                                                                                    	mostrarVentanaProductoAlmacen(gEx('cmbAlmacenPedido').getValue());
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	tblGrid.getStore().remove(filas);
                                                                                            if(tblGrid.getStore().getCount()==0)
	                                                                                            gEx('cmbAlmacenPedido').enable();
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	tblGrid.on('afteredit',function(e)
    						{
                            	e.record.set('subtotal',parseFloat(e.record.data.cantidad)*parseFloat(e.record.data.costoUnitario));
                            	e.record.set('total',parseFloat(e.record.get('subtotal'))+parseFloat(e.record.data.iva));
                                
                            }
    			)                                                    
	return 	tblGrid;		
}

function mostrarVentanaProductoAlmacen(iA)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'Especifique el producto que desea agregar al pedido:'
                                                        },
                                                        crearGridProductosAgregar(iA),
                                                        {
                                                        	x:10,
                                                            y:240,
                                                            html:'Costo unitario de compra:'
                                                            
                                                        },
                                                        {
                                                        	x:150,
                                                            y:235,
                                                            xtype:'numberfield',
                                                            width:100,
                                                            id:'costoUnitario',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                        },
                                                        {
                                                        	x:290,
                                                            y:240,
                                                            html:'Cantidad adquirida:'
                                                            
                                                        },
                                                        {
                                                        	x:410,
                                                            y:235,
                                                            xtype:'numberfield',
                                                            width:100,
                                                            id:'cantidad',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                        }
                                                        
                                                        
	
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar producto a pedido',
										width: 650,
										height:340,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var costoUnitario=gEx('costoUnitario');
                                                                        var cantidad=gEx('cantidad');
                                                                        if(costoUnitario.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	costoUnitario.focus();
                                                                            }
                                                                            msgBox('Debe especificar el costo unitario del producto',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cantidad.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cantidad.focus();
                                                                            }
                                                                            msgBox('Debe especificar la cantidad de producto a incluir en el pedido',resp2);
                                                                            return;
                                                                        }
                                                                 		var gridProductosAdd=gEx('gridProductosAdd');
                                                                        var fila=gridProductosAdd.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto que desea agregar al pedido');
                                                                        	return;
                                                                        }
                                                                        var r=new regProductoPedido	(
                                                                        								{
                                                                                                        	idProducto:fila.data.idProducto,
                                                                                                            producto:'['+fila.data.cveProducto+'] '+fila.data.nombreProducto,
                                                                                                            costoUnitario:costoUnitario.getValue(),
                                                                                                            cantidad:cantidad.getValue(),
                                                                                                            total:parseFloat(costoUnitario.getValue())*parseFloat(cantidad.getValue())
                                                                                                            
                                                                                                        }
                                                                                                        	
                                                                        							)
                                                                                                    
                                                                                                    
                                                                     	var gProductosPedidos=gEx('gProductosPedidos')  ;
                                                                        gProductosPedidos.getStore().add(r); 
                                                                        gEx('cmbAlmacenPedido').disable();
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    gEx('gridProductosAdd').getStore().load({url:'../paginasFunciones/funcionesAlmacen.php',params:{funcion:146,idAlmacen:iA,start:0,limit:20}});
}

function crearGridProductosAgregar(iA)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idProducto'},
                                                        {name: 'cveProducto'},
                                                        {name: 'nombreProducto'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='146';
                                        proxy.baseParams.idAlmacen=iA;
                                    }
                        )   
       
       
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'cveProducto'},
                                                                        {type: 'string', dataIndex: 'nombreProducto'}
                                                                        
                                                                    ]
                                                    }
                                                );        
       var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                           {
                                                                header:'Cve. Producto',
                                                                width:130,
                                                                sortable:true,
                                                                dataIndex:'cveProducto'
                                                            },
                                                            {
                                                                header:'Nombre del Producto',
                                                                width:380,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto'
                                                            }
                                                        ]
                                                    );
         
        var tamPagina=20;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )   
         
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridProductosAdd',
                                                                store:alDatos,
                                                                x:10,
                                                                y:40,
                                                                sm:chkRow,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                width:600,
                                                                height:180,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
        
}

function mostrarVentanaRecepcion(fila)
{
	iva='16';
    var arrTipoViaje=[['0','Sencillo'],['1','Redondo']];
    var arrMinutos=new Array();
    var arrHoras=new Array();
    var x;
    var aux;
    for(x=0;x<24;x++)
    {
    	aux=rellenarCadena((x+''),2,'0',-1);
    	arrHoras.push([aux,aux]);
    }
    
    for(x=0;x<60;x++)
    {
    	aux=rellenarCadena((x+''),2,'0',-1);
    	arrMinutos.push([aux,aux]);
    }
    
    
    var cmbTipoViaje=crearComboExt('cmbTipoViaje',arrTipoViaje,120,50,200);
    cmbTipoViaje.setValue('0');
    cmbTipoViaje.on('select',function(cmb,registro)
    							{
                                	if(registro.get('id')=='0')
                                    {
                                    	//gEx('cmbHoraRegreso').hide();
                                        //gEx('cmbHoraRegreso').setValue(0);
                                    	//gEx('cmbMinutoRegreso').hide();
                                        //gEx('cmbMinutoRegreso').setValue(0);
                                        gEx('lblFechaRegreso').hide();
                                        gEx('txtFechaRegreso').hide();
                                        gEx('txtFechaRegreso').setValue('');
                                        //gEx('lblDotHRegreso').hide();
                                        
                                    }
                                    else
                                    {
                                    	//gEx('cmbHoraRegreso').show();
                                    	//gEx('cmbMinutoRegreso').show();
                                        gEx('lblFechaRegreso').show();
                                        gEx('txtFechaRegreso').show();
                                        //gEx('lblDotHRegreso').show();
                                    }
                                	
                                }
    				)
    
    
    var cmbHoraSalida=crearComboExt('cmbHoraSalida',arrHoras,240,75,45);
    cmbHoraSalida.hide();
    cmbHoraSalida.setValue('00');
    var cmbMinutoSalida=crearComboExt('cmbMinutoSalida',arrMinutos,295,75,45);
    cmbMinutoSalida.setValue('00');
    cmbMinutoSalida.hide();
    var cmbHoraRegreso=crearComboExt('cmbHoraRegreso',arrHoras,240,100,45);
    cmbHoraRegreso.setValue('00');
    cmbHoraRegreso.hide();
    var cmbMinutoRegreso=crearComboExt('cmbMinutoRegreso',arrMinutos,295,100,45);
    cmbMinutoRegreso.setValue('00');
    cmbMinutoRegreso.hide();
    var cmbTipoComprobante=crearComboExt('cmbTipoComprobante',arrCategoriasDisponibles,140,160,350);
    cmbTipoComprobante.on('select',function(cmb,registro)
    								{
                                    	
                                    	setTipoComprobante(registro.get('id'),true);
                                    }
    						)
	//cmbTipoComprobante.setValue('5');
	       					
   
    var arrIVA=[['11','11'],['16','16']];
    var cmbIVAConsidera=crearComboExt('cmbIVAConsidera',arrIVA,150,5,70);
    cmbIVAConsidera.on('select',function(cmb,registro)
    							{
                                	iva=registro.get('id');
                                    recalcularValoresIVA();
                                }
    				)
    
    cmbIVAConsidera.setValue(iva)
	var tabla='<div><input type="text" id="txtFileName" disabled="true" style="border: solid 1px; background-color: #FFFFFF; width: 50px" /></div><div class="flash" id="fsUploadProgress">'+ 
					'</div><input type="hidden" name="hidFileID" id="hidFileID" value="" /> ';       
	tipoComprobante=1;
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	xtype:'fieldset',
                                                            title:'',
                                                            layout:'absolute',
                                                            x:10,
                                                            y:0,
                                                            width:950,
                                                            height:220,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            html:'Folio del pedido:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:140,
                                                                            y:10,
                                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.data.idPedido+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            html:'Proveedor:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:140,
                                                                            y:40,
                                                                            html:'<span class="letraRojaSubrayada8"><b>['+fila.data.rfc+'] '+fila.data.proveedor+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:70,
                                                                            html:'Fecha de recepci&oacute;n:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:140,
                                                                            y:65,
                                                                            xtype:'datefield',
                                                                            id:'dteFechaRecepcion',
                                                                            value:'<?php echo date("Y-m-d")?>'
                                                                        },
                                                                        
                                                                        {
                                                                        	x:10,
                                                                            y:100,
                                                                            html:'Comentarios adicionales:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                        	x:140,
                                                                            y:95,
                                                                            width:400,
                                                                            height:60,
                                                                            id:'comentarios',
                                                                            xtype:'textarea'
                                                                        },
                                                            			{
                                                                        	x:10,
                                                                            y:165,
                                                                            xtype:'label',
                                                                            html:'Tipo de comprobante:'
                                                                        },
                                                                        cmbTipoComprobante,
                                                                        {
                                                                        	x:550,
                                                                            y:0,
                                                                            id:'datosBoleto',
                                                                            xtype:'fieldset',
                                                                            title:'Datos de comprobaci&oacute;n',
                                                                            width:375,
                                                                            height:160,
                                                                            hidden:true,
                                                                           	layout:'absolute',
                                                                            items:	[
                                                                            			{
                                                                                        	x:0,
                                                                                            y:5,
                                                                                            xtype:'label',
                                                                                            html:'Origen: <span style="color:#FF0000">*</span>'
                                                                                        },
                                                                                        {
                                                                                        	x:120,
                                                                                            y:0,
                                                                                            xtype:'textfield',
                                                                                            width:220,
                                                                                            id:'txtOrigen'
                                                                                        },
                                                                                        {
                                                                                        	x:0,
                                                                                            y:30,
                                                                                            xtype:'label',
                                                                                            html:'Destino: <span style="color:#FF0000">*</span>'
                                                                                        },
                                                                                        {
                                                                                        	x:120,
                                                                                            y:25,
                                                                                            xtype:'textfield',
                                                                                            width:220,
                                                                                            id:'txtDestino'
                                                                                        },
                                                                                        {
                                                                                        	x:0,
                                                                                            y:55,
                                                                                            xtype:'label',
                                                                                            html:'Tipo de viaje: <span style="color:#FF0000">*</span>'
                                                                                        },
                                                                                        cmbTipoViaje,
                                                                                        {
                                                                                        	x:0,
                                                                                            y:80,
                                                                                            xtype:'label',
                                                                                            html:'Fecha de salida: <span style="color:#FF0000">*</span>'
                                                                                        },
                                                                                        {
                                                                                        	x:120,
                                                                                            y:75,
                                                                                            xtype:'datefield',
                                                                                            
                                                                                            id:'txtFechaSalida'
                                                                                        },
                                                                                        cmbHoraSalida,
                                                                                        {
                                                                                        	x:288,
                                                                                            y:80,
                                                                                            hidden:true,
                                                                                            xtype:'label',
                                                                                            html:'<b>:</b>'
                                                                                        },
                                                                                        cmbMinutoSalida,
                                                                                        {
                                                                                        	x:0,
                                                                                            y:105,
                                                                                            xtype:'label',
                                                                                            id:'lblFechaRegreso',
                                                                                            hidden:true,
                                                                                            html:'Fecha de regreso: <span style="color:#FF0000">*</span>'
                                                                                        },
                                                                                        {
                                                                                        	x:120,
                                                                                            y:100,
                                                                                            xtype:'datefield',
                                                                                            id:'txtFechaRegreso',
                                                                                            hidden:true
                                                                                        },
                                                                                        cmbHoraRegreso,
                                                                                        {
                                                                                        	x:288,
                                                                                            y:105,
                                                                                            id:'lblDotHRegreso',
                                                                                            xtype:'label',
                                                                                            html:'<b>:</b>',
                                                                                            hidden:true
                                                                                        },
                                                                                        cmbMinutoRegreso
                                                                            		]
                                                                        },
                                                                        {
                                                                        	x:585,
                                                                            y:0,
                                                                            id:'datosFactura',
                                                                            xtype:'fieldset',
                                                                            title:'Datos de comprobaci&oacute;n',
                                                                            width:315,
                                                                            height:160,
                                                                           	layout:'absolute',
                                                                            items:	[
                                                                            			
                                                                                        {
                                                                                        	x:5,
                                                                                            y:10,
                                                                                            xtype:'label',
                                                                                            html:'IVA considerado al (%):'
                                                                                        },
                                                                                        cmbIVAConsidera,
                                                                                        {
                                                                                        	x:5,
                                                                                            y:40,
                                                                                            xtype:'label',
                                                                                            html:'Monto total del pedido:'
                                                                                        },
                                                                                        {
                                                                                        	x:50,
                                                                                            y:70,
                                                                                            xtype:'label',
                                                                                            html:'<span style="font-size:16px !important">&nbsp;<span id="tComprobacion" class="letraRojaSubrayada8" style="font-size:16px !important" >'+Ext.util.Format.usMoney(fila.data.total)+'</span></span>'
                                                                                        }
                                                                            		]
                                                                        } 
                                                            		]
                                                        },
                                                        {
                                                        	xtype:'fieldset',
                                                            title:'Datos de comprobante',
                                                            layout:'absolute',
                                                            x:10,
                                                            y:230,
                                                           	padding :0, 
                                                            width:950,
                                                            height:95,
                                                            items:	[
                                                            
                                                            
                                                            			{
                                                                            x:10,
                                                                            y:5,
                                                                            xtype:'label',
                                                                            id:'lblFolio',
                                                                            html:'Folio del comprobante: <span id="oblFolio" style="color:#F00">*</span>'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:5,
                                                                            xtype:'label',
                                                                            id:'lblFolioFiscal',
                                                                            hidden:true,
                                                                            html:'Folio fiscal: <span id="folioComprobante" style="color:#F00">*</span>'
                                                                        },
                                                                        {
                                                                        	id:'folio1',
                                                                        	x:90,
                                                                            y:0,
                                                                            enableKeyEvents:true,
                                                                            xtype:'textfield',
                                                                            width:85,
                                                                            listeners:	{
                                                                            				keyup:function(campo)
                                                                                            		{

                                                                                                    	if(campo.getValue().length>=8)
                                                                                                        {
                                                                                                        	gEx('folio2').focus();
                                                                                                        }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	x:185,
                                                                            y:5,
                                                                            id:'lblSep1',
                                                                            xtype:'label',
                                                                            html:'-'
                                                                        },
                                                                        {
                                                                        	id:'folio2',
                                                                        	x:200,
                                                                            y:0,
                                                                            enableKeyEvents:true,
                                                                            xtype:'textfield',
                                                                            width:50,
                                                                            listeners:	{
                                                                            				keyup:function(campo)
                                                                                            		{

                                                                                                    	if(campo.getValue().length>=4)
                                                                                                        {
                                                                                                        	gEx('folio3').focus();
                                                                                                        }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	x:260,
                                                                            y:5,
                                                                            id:'lblSep2',
                                                                            xtype:'label',
                                                                            html:'-'
                                                                        },

                                                                        {
                                                                        	id:'folio3',
                                                                        	x:275,
                                                                            y:0,
                                                                            enableKeyEvents:true,
                                                                            xtype:'textfield',
                                                                            width:50,
                                                                            listeners:	{
                                                                            				keyup:function(campo)
                                                                                            		{

                                                                                                    	if(campo.getValue().length>=4)
                                                                                                        {
                                                                                                        	gEx('folio4').focus();
                                                                                                        }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	x:335,
                                                                            y:5,
                                                                            id:'lblSep3',
                                                                            xtype:'label',
                                                                            html:'-'
                                                                        },
                                                                        {
                                                                        	id:'folio4',
                                                                        	x:350,
                                                                            y:0,
                                                                            enableKeyEvents:true,
                                                                            xtype:'textfield',
                                                                            width:50,
                                                                            listeners:	{
                                                                            				keyup:function(campo)
                                                                                            		{

                                                                                                    	if(campo.getValue().length>=4)
                                                                                                        {
                                                                                                        	gEx('folio5').focus();
                                                                                                        }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                        	x:410,
                                                                            y:5,
                                                                            id:'lblSep4',
                                                                            xtype:'label',
                                                                            html:'-'
                                                                        },
                                                                        {
                                                                        	id:'folio5',
                                                                        	x:420,
                                                                            y:0,
                                                                            enableKeyEvents:true,
                                                                            xtype:'textfield',
                                                                            width:130,
                                                                            listeners:	{
                                                                            				keyup:function(campo)
                                                                                            		{

                                                                                                    	if(campo.getValue().length>=12)
                                                                                                        {
                                                                                                        	gE('txtFileName').focus();	
                                                                                                        }
                                                                                                    }
                                                                            			}
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:0,
                                                                            xtype:'numberfield',
                                                                            id:'noFolio',
                                                                            width:50
                                                                        },
                                                                        {
                                                                            x:205,
                                                                            y:5,
                                                                            xtype:'label',
                                                                            id:'lblSerie',
                                                                            html:'Serie:'
                                                                            
                                                                        },
                                                                        {
                                                                            x:255,
                                                                            y:0,
                                                                            xtype:'textfield',
                                                                            id:'txtNoSerie',
                                                                            width:50
                                                                        },
                                                                        {
                                                                            x:325,
                                                                            y:5,
                                                                            xtype:'label',
                                                                            id:'lblNumAprobacion',
                                                                            html:'N&uacute;mero de aprobaci&oacute;n: <span id="oblNumAprobacion" style="color:#F00">*</span>'
                                                                        },
                                                                        {
                                                                            x:460,
                                                                            y:0,
                                                                            xtype:'numberfield',
                                                                            id:'noAprobacion',
                                                                            width:70
                                                                        },
                                                                        {
                                                                            xtype:'label',
                                                                            x:550,
                                                                            y:5,
                                                                            id:'lblAnioAp',
                                                                            html:'A&ntilde;o aprobaci&oacute;n: <span id="oblAnioAprobacion" style="color:#F00">*</span>'
                                                                        },
                                                                        {
                                                                            x:645,
                                                                            y:0,
                                                                            xtype:'numberfield',
                                                                            id:'anioAprobacion',
                                                                            width:50
                                                                        },
                                                            			
                                                                        {
                                                                        	xtype:'label',
                                                                            x:710,
                                                                            y:5,
                                                                            html:'Comprobante: <span id="oblComprobante" style="color:#F00">*</span>'
                                                                        },
                                                                        
                                                                        {
                                                                            x:800,
                                                                            y:0,
                                                                            html:	'<table width="290"><tr><td><div id="uploader"><p>Your browser doesn\'t have Flash, Silverlight or HTML5 support.</p></div></td></tr><tr id="filaAvance" style="display:none"><td align="right">Porcentaje de avance: <span id="porcentajeAvance"> 0%</span></td></tr></table>'
                                                                        },
                                                                       
                                                                        {
                                                                            x:695,
                                                                            y:1,
                                                                            id:'btnUploadFile',
                                                                            xtype:'button',
                                                                            text:'Seleccionar...',
                                                                            handler:function()
                                                                                    {
                                                                                        $('#containerUploader').click();
                                                                                    }
                                                                        },
                                                                        {
                                                                            x:185,
                                                                            y:10,
                                                                            hidden:true,
                                                                            html:	'<div id="containerUploader"></div>'
                                                                        },
                                                                        
                                                                        
                                                                        {
                                                                        	x:770,
                                                                            y:25,
                                                                            hidden:true,
                                                                            id:'lblAvance',
                                                                            xtype:'label',
                                                                            html:'<span style="font-size:10px; color:#000; font-weight:bold">Porcentaje de avance: <span id="porcentajeAvance">0 %</span></span>'
                                                                        },
                                                                        {
                                                                            x:290,
                                                                            y:0,
                                                                            xtype:'hidden',
                                                                            id:'idArchivo'
                
                                                                        },
                                                                        {
                                                                            x:290,
                                                                            y:0,
                                                                            xtype:'hidden',
                                                                            id:'nombreArchivo'
                                                                        } ,
                                                                        {
                                                                            x:10,
                                                                            y:35,
                                                                            xtype:'label',
                                                                            
                                                                            html:'Fecha del comprobante: <span id="oblFecha" style="color:#F00">*</span>'
                                                                        },  
                                                                        {
                                                                        	x:140,
                                                                            y:30,
                                                                            xtype:'datefield',
                                                                            id:'dteFechaComprobante'
                                                                        }
                                                                        
                                                                        
                                                            		]
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vComprobacionFiscal',
										title: 'Recepci&oacute;n de pedidos',
										width: 990,
										height:440,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	var cObj={
                                                                                                // Backend settings
                                                                                                upload_url: "../paginasFunciones/procesarDocumento.php", //lquevedor
                                                                                                file_post_name: "archivoEnvio",
                                                                                 
                                                                                                // Flash file settings
                                                                                                file_size_limit : "100 MB",
                                                                                                file_types : "*.*",			// or you could use something like: "*.doc;*.wpd;*.pdf",
                                                                                                file_types_description : "Todos los archivos",
                                                                                                file_upload_limit : 0,
                                                                                                file_queue_limit : 1,
                                                                                 
                                                                                                
                                                                                                upload_success_handler : subidaCorrecta
                                                                                            };     
																	crearControlUploadHTML5(cObj);
                                                                }
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dteFechaRecepcion=gEx('dteFechaRecepcion');
                                                                        if(dteFechaRecepcion.getValue()=='')
                                                                        {
                                                                        	function respFechaX()
                                                                            {
                                                                            	dteFechaRecepcion.focus();
                                                                            }
                                                                            msgBox('Debe indicar la fecha de recepci&oacute;n del pedido',respFechaX);
                                                                        	return;
                                                                        }
                                                                        var comentarios=gEx('dteFechaRecepcion');
                                                                    	var dteFechaComprobante=gEx('dteFechaComprobante');
                                                                        if(dteFechaComprobante.getValue()=='')
                                                                        {
                                                                        	function respFecha()
                                                                            {
                                                                            	dteFechaComprobante.focus();
                                                                            }
                                                                            msgBox('La fecha del comprobante es obligatoria',respFecha);
                                                                        	return;
                                                                        }
                                                                        
                                                                    	var tComprobacion=parseFloat(normalizarValor(gE('tComprobacion').innerHTML));
                                                                        if(tComprobacion==0)
                                                                        {
                                                                        	msgBox('El monto a comprobar no puede ser igual a $ 0.00');
                                                                        	return;
                                                                        }
                                                                        
                                                                       	if((tipoComprobante==3)||(tipoComprobante==4))
                                                                        {
                                                                        
                                                                        	if(tipoComprobante==4)
                                                                            {
                                                                            	var txtOrigen=gEx('txtOrigen');
                                                                                var txtDestino=gEx('txtDestino');
                                                                                var txtFechaSalida=gEx('txtFechaSalida');
                                                                                var txtFechaRegreso=gEx('txtFechaRegreso');
                                                                                
                                                                                if(txtOrigen.getValue()=='')
                                                                                {
                                                                                	function resp1000()
                                                                                    {
                                                                                    	txtOrigen.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar el origen del boleto de avi&oacute;n',resp1000);
                                                                                	return;
                                                                                }
                                                                                
                                                                                if(txtDestino.getValue()=='')
                                                                                {
                                                                                	function resp1001()
                                                                                    {
                                                                                    	txtDestino.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar el destino del boleto de avi&oacute;n',resp1001);
                                                                                	return;
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                if(txtFechaSalida.getValue()=='')
                                                                                {
                                                                                	function resp1002()
                                                                                    {
                                                                                    	txtFechaSalida.focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar la fecha de salida del vuelo',resp1002);
                                                                                	return;
                                                                                }
                                                                                
                                                                                if(cmbTipoViaje.getValue()=='1')
                                                                                {
                                                                                
                                                                                    if(txtFechaRegreso.getValue()=='')
                                                                                    {
                                                                                        function resp1003()
                                                                                        {
                                                                                            txtFechaRegreso.focus();
                                                                                        }
                                                                                        msgBox('Debe ingresar la fecha de regreso del vuelo',resp1003);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(txtFechaSalida.getValue()>txtFechaRegreso.getValue())
                                                                                    {
                                                                                    	function resp1004()
                                                                                        {
                                                                                        	txtFechaSalida.focus();
                                                                                        }
                                                                                        msgBox('La fecha de salida no puede ser mayor que la fecha de regreso',resp1004);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                            	}
                                                                                
                                                                            }
                                                                        
                                                                        
                                                                            function respPregunta(btn)
                                                                            {
                                                                    			if(btn=='yes')
                                                                                {
                                                                                    if(gEx('razonSocial10'))
                                                                                    {
                                                                                        if(gEx('razonSocial10').getValue().trim()=='')
                                                                                        {
                                                                                            function resp1()
                                                                                            {
                                                                                                gEx('razonSocial10').focus();
                                                                                            }
                                                                                            msgBox('Debe ingresar el nombre del proveedor (raz&oacute;n social) al cual pertenece el comprobante a ingresar');
                                                                                            return;
                                                                                        }
                                                                                    }
                                                                                
                                                                                    
                                                                                    if(uploadControl.files.length==0)
                                                                                    {
                                                                                       msgBox('Debe indicar el documento que desea agregar como comprobante del informe financiero');
                                                                                        return;
                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                                                                    uploadControl.start();
                                                                                    
                                                                                    
                                                                                }
                                                                        	}
                                                                            msgConfirm('Est&aacute; seguro de querer enviar los datos de la comprobaci&oacute;n a validaci&oacute;n? (No podr&aacute; modificar la informaci&oacute;n)',respPregunta);
                                                                        
                                                                        }
                                                                        else
                                                                        {
                                                                            var noFactura=gEx('noFolio').getValue();
                                                                        	
                                                                            
                                                                        	if(tipoComprobante!='7')
                                                                            {
                                                                                if(gEx('noFolio').getValue()=='')
                                                                                {
                                                                                    function resp3()
                                                                                    {
                                                                                        gEx('noFolio').focus();
                                                                                    }
                                                                                    msgBox('Debe ingresar el n&uacute;mero de folio del comprobante',resp3);
                                                                                    return;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                            	noFactura=gEx('folio1').getValue()+'-'+gEx('folio2').getValue()+'-'+gEx('folio3').getValue()+'-'+gEx('folio4').getValue()+'-'+gEx('folio5').getValue();
                                                                            }
                                                                            
                                                                        	switch(tipoComprobante)
                                                                            {
                                                                            	case '5': //Factura
																			    case '1': 
                                                                                case '6': //Recibo de honorarios
																				case '2':
                                                                                	if(gEx('noAprobacion').getValue()=='')
                                                                                    {
                                                                                        function resp2()
                                                                                        {
                                                                                            gEx('noAprobacion').focus();
                                                                                        }
                                                                                        msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n del proveedor ante SICOFI',resp2);
                                                                                        return;
                                                                                    }
                                                                                break;
                                                                                case '4':  //Boleto (Avión)
                                                                                break;
                                                                                case '7':	//Factura electrónica con folio fiscal
                                                                                	var folio1=gEx('folio1');
                                                                                    
                                                                                    if(folio1.getValue()=='')
                                                                                    {
                                                                                    	function resp10()
                                                                                        {
                                                                                        	folio1.focus();
                                                                                        }
                                                                                        msgConfirm('Debe ingresar el folio fiscal',resp10);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(folio1.getValue().length!=8)
                                                                                    {
                                                                                    	function resp11()
                                                                                        {
                                                                                        	folio1.focus();
                                                                                        }
                                                                                        msgConfirm('Valor del campo debe ser de 8 caracteres',resp11);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    
                                                                                    var folio2=gEx('folio2');
                                                                                    
                                                                                    if(folio2.getValue()=='')
                                                                                    {
                                                                                    	function resp12()
                                                                                        {
                                                                                        	folio2.focus();
                                                                                        }
                                                                                        msgConfirm('Debe ingresar el folio fiscal',resp12);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(folio2.getValue().length!=4)
                                                                                    {
                                                                                    	function resp13()
                                                                                        {
                                                                                        	folio2.focus();
                                                                                        }
                                                                                        msgConfirm('Valor del campo debe ser de 4 caracteres',resp13);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    
                                                                                    var folio3=gEx('folio3');
                                                                                    
                                                                                    if(folio3.getValue()=='')
                                                                                    {
                                                                                    	function resp14()
                                                                                        {
                                                                                        	folio3.focus();
                                                                                        }
                                                                                        msgConfirm('Debe ingresar el folio fiscal',resp14);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(folio3.getValue().length!=4)
                                                                                    {
                                                                                    	function resp15()
                                                                                        {
                                                                                        	folio3.focus();
                                                                                        }
                                                                                        msgConfirm('Valor del campo debe ser de 4 caracteres',resp15);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    
                                                                                    var folio4=gEx('folio4');
                                                                                    
                                                                                    if(folio4.getValue()=='')
                                                                                    {
                                                                                    	function resp16()
                                                                                        {
                                                                                        	folio4.focus();
                                                                                        }
                                                                                        msgConfirm('Debe ingresar el folio fiscal',resp16);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(folio4.getValue().length!=4)
                                                                                    {
                                                                                    	function resp17()
                                                                                        {
                                                                                        	folio4.focus();
                                                                                        }
                                                                                        msgConfirm('Valor del campo debe ser de 4 caracteres',resp17);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    
                                                                                    var folio5=gEx('folio5');
                                                                                    
                                                                                    if(folio5.getValue()=='')
                                                                                    {
                                                                                    	function resp18()
                                                                                        {
                                                                                        	folio5.focus();
                                                                                        }
                                                                                        msgConfirm('Debe ingresar el folio fiscal',resp18);
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    if(folio5.getValue().length!=12)
                                                                                    {
                                                                                    	function resp19()
                                                                                        {
                                                                                        	folio5.focus();
                                                                                        }
                                                                                        msgConfirm('Valor del campo debe ser de 12 caracteres',resp19);
                                                                                        return;
                                                                                    }
                                                                                
                                                                                
                                                                                break;
																				case '8':	//Factura electrónica sin folio fiscal
                                                                                	if(gEx('noAprobacion').getValue()=='')
                                                                                    {
                                                                                        function resp2()
                                                                                        {
                                                                                            gEx('noAprobacion').focus();
                                                                                        }
                                                                                        msgBox('Debe ingresar el n&uacute;mero de aprobaci&oacute;n del proveedor ante SICOFI',resp2);
                                                                                        return;
                                                                                    }
                                                                                    if(gEx('anioAprobacion').getValue()=='')
                                                                                    {
                                                                                        function resp3()
                                                                                        {
                                                                                            gEx('anioAprobacion').focus();
                                                                                        }
                                                                                        msgBox('Debe ingresar el a&ntilde;o de aprobaci&oacute;n',resp3);
                                                                                        return;
                                                                                    }
                                                                                break;
                                                                                
                                                                            }
                                                                        
                                                                        	function funcAjax()
                                                                            {
                                                                                var resp=peticion_http.responseText;
                                                                                arrResp=resp.split('|');
                                                                                if(arrResp[0]=='1')
                                                                                {
                                                                                    if(arrResp[1]=='1')
                                                                                    {
                                                                                    	function respPregunta(btn)
                                                                                        {
                                                                                			if(btn=='yes')
                                                                                            {
                                                                                                if(swfu.getStats().files_queued>0)
                                                                                                {
                                                                                                    swfu.startUpload();	
                                                                                                    gEx('lblAvance').show();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    
                                                                                                    msgBox('Debe indicar el documento que desea agregar como comprobante de la rercepci&oacute;n del pedido');
                                                                                                    return;
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                    	}
                                                                                    	msgConfirm('Est&aacute; seguro de querer registrar la recepci&oacute;n del pedido?',respPregunta);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	msgBox('El folio de la factura ingresado ya ha sido registrada en el sistema anteriormente, favor de verificarla');
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                }
                                                                            }
                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=153&idProveedor='+fila.data.idProveedor+'&noFactura='+noFactura+'&noSerie='+cv(gEx('txtNoSerie').getValue()),true);
                                                                        }
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    setTipoComprobante('5'); 
    
}

function subidaCorrecta(file, serverData) 
{
	try 
    {
		file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
		var arrDatos=serverData.split('|');
		if ( arrDatos[0]!='1') 
		{
		} 
		else 
		{
			gEx("idArchivo").setValue(arrDatos[1]);
            gEx("nombreArchivo").setValue(arrDatos[2]);
            guardarDocumento();
            
		    
		}
		
	} 
    catch (e) 
	{
		alert(e);
	}
}

function setTipoComprobante(tComprobante,lCampos)
{
	tipoComprobante=tComprobante;
    gEx('datosBoleto').hide();
    
    
    gEx('txtOrigen').setValue('');
    gEx('txtDestino').setValue('');
    gEx('cmbTipoViaje').setValue(0);
    gEx('txtFechaSalida').setValue('');
    gEx('cmbHoraSalida').setValue('00');
    gEx('cmbMinutoSalida').setValue('00');
    gEx('txtFechaRegreso').setValue('');
    gEx('cmbHoraRegreso').setValue('00');
    gEx('cmbMinutoRegreso').setValue('00');
    
    gEx('datosFactura').show();
    
	gE('oblFolio').innerHTML='*';
	var cmbIVAConsidera=gEx('cmbIVAConsidera');
    switch(tipoComprobante)
    {
    	case '5':
        case '1':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '6':
        case '2':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '3':
        		mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '4':
	        	mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
               
                gEx('datosFactura').hide();
                gEx('datosBoleto').show();
                
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
                
        break; 
        case '7':
                ocultarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gEx('lblFolioFiscal').show();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
        break;
        case '8':
                mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='*';
                gEx('anioAprobacion').enable();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
            break;           
    }
}

function ocultarCampos()
{
	gEx('lblFolio').hide();
    gEx('noFolio').hide();
    gEx('lblSerie').hide();
    gEx('lblNumAprobacion').hide();
    gEx('lblAnioAp').hide();
    gEx('noAprobacion').hide();
    gEx('txtNoSerie').hide();
    gEx('anioAprobacion').hide();
    gEx('lblFolioFiscal').show();
    gEx('lblSep1').show();
    gEx('folio1').show();
    gEx('lblSep2').show();
    gEx('folio2').show();
    gEx('lblSep3').show();
    gEx('folio3').show();
    gEx('lblSep4').show();
    gEx('folio4').show();
    gEx('folio5').show();
}

function mostrarCampos()
{
	gEx('lblFolio').show();
    gEx('noFolio').show();
    gEx('lblSerie').show();
    gEx('lblNumAprobacion').show();
    gEx('lblAnioAp').show();
    gEx('noAprobacion').show();
    gEx('txtNoSerie').show();
    gEx('anioAprobacion').show();
    gEx('lblFolioFiscal').hide();
    
    gEx('lblSep1').hide();
    gEx('folio1').hide();
    gEx('lblSep2').hide();
    gEx('folio2').hide();
    gEx('lblSep3').hide();
    gEx('folio3').hide();
    gEx('lblSep4').hide();
    gEx('folio4').hide();
    gEx('folio5').hide();
}

function guardarDocumento()
{
	
    var x;
    var arrConceptos='';
    var grid;
    var montoComprobacion=0;
    var fila;
    var obj;
    
    
    
    var txtOrigen=gEx('txtOrigen');
    var txtDestino=gEx('txtDestino');
    var txtFechaSalida=gEx('txtFechaSalida');
    var txtFechaRegreso=gEx('txtFechaRegreso');
    var cmbTipoViaje=gEx('cmbTipoViaje');
    var cmbHoraSalida=gEx('cmbHoraSalida');
    var cmbMinutoSalida=gEx('cmbMinutoSalida');
    var cmbHoraRegreso=gEx('cmbHoraRegreso');
    var cmbMinutoRegreso=gEx('cmbMinutoRegreso');
    var hRegreso='';
    if(txtFechaRegreso.getValue()!='')
    	hRegreso=txtFechaRegreso.getValue().format('Y-m-d');
    var fSalida='';
    if(txtFechaSalida.getValue()!='')
    	fSalida=txtFechaSalida.getValue().format("Y-m-d");
    var objBoletaAvion='{"origen":"'+cv(txtOrigen.getValue().trim())+'","destino":"'+cv(txtDestino.getValue().trim())+'","tipoViaje":"'+cmbTipoViaje.getValue()+
    				'","fechaSalida":"'+fSalida+' '+cmbHoraSalida.getValue()+':'+cmbMinutoSalida.getValue()+
                    '","fechaRegreso":"'+hRegreso+' '+cmbHoraRegreso.getValue()+':'+cmbMinutoRegreso.getValue()+'"}';
    
	var folio=gEx('noFolio').getValue();
    if(tipoComprobante==7)    
    	folio=gEx('folio1').getValue()+'-'+gEx('folio2').getValue()+'-'+gEx('folio3').getValue()+'-'+gEx('folio4').getValue()+'-'+gEx('folio5').getValue();
    var fila=gEx('gridPedidos').getSelectionModel().getSelected();
	var cadObj='{"fechaRecepcionPedido":"'+gEx('dteFechaRecepcion').getValue().format('Y-m-d')+'","comentariosAdicionales":"'+cv(gEx('comentarios').getValue())+'","tipoComprobacion":"2","fechaComprobante":"'+gEx('dteFechaComprobante').getValue().format("Y-m-d")+'","objBoletaAvion":'+objBoletaAvion+',"anioAprobacion":"'+gEx('anioAprobacion').getValue()+'","noSerie":"'+cv(gEx('txtNoSerie').getValue())+'","idFormulario":"0","idReferencia":"'+fila.data.idPedido+'","montoComprobacion":"'+montoComprobacion+'","idProveedor":"'+fila.data.idProveedor+
    '","tComprobante":"'+tipoComprobante+'","folio":"'+folio+'","noAprobacion":"'+gEx('noAprobacion').getValue()+
    '","idComprobante":"'+gEx('idArchivo').getValue()+'","nombreArchivo":"'+gEx('nombreArchivo').getValue()+'","iva":"'+iva+'"}';
	         

	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('vComprobacionFiscal').close();
            gEx('gridPedidos').getStore().reload();
            
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=154&cadObj='+cadObj,true);         
    
	
    
}

function limpiarCampos()
{
	
    gEx('noFolio').setValue('');
    gEx('noAprobacion').setValue('');
    gEx('txtNoSerie').setValue('');
    gEx('folio1').setValue('');
    gEx('folio2').setValue('');
    gEx('folio3').setValue('');
    gEx('folio4').setValue('');
    gEx('folio5').setValue('');
}

function registrarActualizacionPedido()
{
	cerrarVentanaFancy();
    gEx('gridPedidos').getStore().reload();
    gEx('btnCancelacion').disable();
}

function descargarFactura(iF)
{
	document.location.href='../paginasFunciones/obtenerArchivos.php?id='+iF;
}

function buscarPorProductoNombre()
{
	var arrCriterio=[['1','Comienza con...'],['2','Contiene la palabra...']];
	var cmbCriterioBusqueda=crearComboExt('cmbCriterioBusqueda',arrCriterio,170,5,250);
    cmbCriterioBusqueda.setValue('2');
    cmbCriterioBusqueda.on('select',cargarProductosBusqueda);
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>Criterio de b&uacute;squeda:</b>'
                                                                        },
                                                                        cmbCriterioBusqueda,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        {
                                                                            x:170,
                                                                            y:35,
                                                                            xtype:'textfield',
                                                                            width:330,
                                                                            enableKeyEvents:true,
                                                                            id:'txtNombre',
                                                                            listeners:	{
                                                                            				keyup:cargarProductosBusqueda
                                                                            			}
                                                                        }
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:410,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombre').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var gProductosBuscados=gEx('gProductosBuscados');
                                                                        var fila=gProductosBuscados.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto que desea agregar');
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                    
                                                                        
                                                                        var r=new  regProductoPedido	(
                                                                        									{
                                                                                                            	idProducto:fila.data.idProducto,
                                                                                                                llave:fila.data.llave,
                                                                                                                producto:fila.data.nombreProducto,
                                                                                                                costoUnitario:0,
                                                                                                                subtotal:0,
                                                                                                                iva:0,
                                                                                                                cantidad:1,
                                                                                                                total:0
                                                                                                            }
                                                                        								)
                                                                        gEx('gProductosPedidos').getStore().add(r);
                                                                        gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                                                       
                                                                        funcionEjecucionBusqueda=function()
                                                                        						{
                                                                                                	gEx('txtClave').setValue('');
                                                                                                }
                                                                       
                                                                        
                                                                         
                                                                         ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
      
}


function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=0;
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	//if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda();
}

function cargarProductosBusqueda()
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:gEx('cmbCriterioBusqueda').getValue(),
                                                            valor:gEx('txtNombre').getValue(),
                                                            idZona:0
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'precioUnitario'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,                                                                
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}



function mostrarAbonosAdeudo(fila)
{
	var gridAbonosHistorial=crearGridAbonosHistorial(fila);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridAbonosHistorial

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Historial de abonos',
										width: 700,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridAbonosHistorial(fila)
{

	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAbono'},
		                                                {name: 'fechaAbono',type:'date',dateFormat:'Y-m-d'},
		                                                {name:'montoAbono'},
                                                        {name: 'formaPago'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaAbono', direction: 'ASC'},
                                                            groupField: 'fechaAbono',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='37';
                                        proxy.baseParams.idAdeudo=fila.data.idAdeudo;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Fecha abono',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaAbono',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Monto abono',
                                                                width:150,
                                                                css:'text-align:right;',
                                                                sortable:true,
                                                                dataIndex:'montoAbono',
                                                                renderer:'usMoney'
                                                            },
                                                            
                                                            {
                                                                header:'Forma de pago',
                                                                width:220,
                                                                
                                                                sortable:true,
                                                                dataIndex:'formaPago',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrFormaPagoBD,val);
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAbonosHistorial',
                                                                store:alDatos,
                                                                x:10,
                                                                y:10,
                                                                height:680,
                                                                height:250,
                                                                frame:false,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar abono',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentaRegistrarAbono(fila);
                                                                                        }
                                                                                
                                                                            }
                                                                         ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function mostrarVentaRegistrarAbono(fila)
{
	var cmbFormaPagoAbono=crearComboExt('cmbFormaPagoAbono',arrFormasPagoAbono,360,35,250);
    cmbFormaPagoAbono.setValue('1');
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<b>Monto adeudo:</b>&nbsp;&nbsp;<span id="lblMontoAbono" style="font-weight:bold;color:#900; ">'+Ext.util.Format.usMoney(fila.data.adeudoTotal)+'</span>'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:40,
                                                            html:'<b>Fecha de abono:</b>'
                                                        },
                                                        {
                                                        	xtype:'datefield',
                                                            x:120,
                                                            y:35,
                                                            value:'<?php echo date("Y-m-d")?>',
                                                            id:'dteFechaAbono'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:250,
                                                            y:40,
                                                            html:'<b>Forma de pago:</b>'
                                                        },
                                                        cmbFormaPagoAbono,
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:70,
                                                            html:'<b>Monto abono:</b>'
                                                        },
                                                        {
                                                        	xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'montoAbono',
                                                            width:100,
                                                            x:120,
                                                            y:65
                                                            
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:100,
                                                            html:'<b>Comentarios adicinales:</b>'
                                                        },
                                                        {
                                                        	xtype:'textarea',
                                                            id:'txtComentarios',
                                                            width:650,
                                                            height:80,
                                                            id:'txtComentarios',
                                                            x:10,
                                                            y:130
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar Abono',
										width: 700,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('montoAbono').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var dteFechaAbono=gEx('dteFechaAbono');
                                                                        if(dteFechaAbono.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaAbono.focus();
                                                                            }
                                                                            msgBox('De indicar la fecha en que se realiz&oacute; el abono',resp);
                                                                            return;
                                                                        }
                                                                        var montoAbono=gEx('montoAbono');
                                                                        if(montoAbono.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	montoAbono.focus();
                                                                            }
                                                                            msgBox('El monto de abono ingresado no es v&aacute;lido',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        var mAbono=parseFloat(montoAbono.getValue());
                                                                        var tDeuda=parseFloat(normalizarValor(gE('lblMontoAbono').innerHTML));
                                                                        if(tDeuda<mAbono)
                                                                        {
                                                                        	msgBox('El monto del abono ingresado (<b>'+Ext.util.Format.usMoney(mAbono)+'<b>) excede el monto del adeudo (<b>'+Ext.util.Format.usMoney(tDeuda)+'<b>)');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"folioPedido":"'+fila.data.folioPedido+'","idAdeudo":"'+fila.data.idAdeudo+'","cantidadAbono":"'+mAbono+'","fechaAbono":"'+dteFechaAbono.getValue().format('Y-m-d')+
                                                                        			'","comentarios":"'+cv(gEx('txtComentarios').getValue())+'","formaPago":"'+gEx('cmbFormaPagoAbono').getValue()+'"}';
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridAbonosHistorial').getStore().reload();
                                                                            	gEx('gridAdeudos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=39&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}