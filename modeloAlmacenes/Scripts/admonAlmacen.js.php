<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>
var fechaActual;
Ext.onReady(inicializar);


function inicializar()
{
	fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>','Y-m-d');
	new Ext.Panel(	{
                                                            
                            xtype:'panel',
                            renderTo:'tblAlm',
                            height:550,
                            width:980,
                            layout:'border',
                            items:	[
                                        {
                                            region:'center',
                                            id:'tblCenter',
                                            width:940,
                                            autoScroll: true,
                                            xtype:'iframepanel',
                                            deferredRender: false,
                                            border:false,
                                            tbar:	[
                                                        
                                                        {
                                                            text:'Cat&aacute;logos',
                                                            menu:	[
                                                                        {
                                                                            text:'Categor&iacute;as de productos',
                                                                            handler:function()
                                                                                    {
                                                                                        gEx('tblCenter').load(
                                                                                                                  {
                                                                                                                      url:'../modeloAlmacenes/tblCategoriasProductos.php',
                                                                                                                      scripts:true,
                                                                                                                      params:	{
                                                                                                                                  idAlmacen:gE('idAlmacen').value,
                                                                                                                                  cPagina:'sFrm=true'
                                                                                                                               }
                                                                                                                    }
                                                                                                              );   
                                                                                        
                                                                                        
                                                                                    }
                                                                        },
                                                                        {
                                                                            text:'Productos',
                                                                            handler:function()
                                                                                    {
                                                                                        gEx('tblCenter').load(
                                                                                                                  {
                                                                                                                      url:'../modeloAlmacenes/tblProductosAlmacen.php',
                                                                                                                      scripts:true,
                                                                                                                      params:	{
                                                                                                                                  idAlmacen:gE('idAlmacen').value,
                                                                                                                                  cPagina:'sFrm=true'
                                                                                                                                  
                                                                                                                               }
                                                                                                                    }
                                                                                                              ); 
                                                                                    }
                                                                        }
                                                                    ]
                                                        },'-',
                                                        {
                                                            text:'Configuraci&oacute;n del almac&eacute;n',
                                                            menu:	[
                                                                        {
                                                                            text:'Datos del almac&eacute;n',
                                                                            handler:function()
                                                                                    {
                                                                                        modificarAlmacen();
                                                                                    }
                                                                        },
                                                                        {
                                                                            text:'Administraci&oacute;n de movimientos de productos en almac&eacute;n',
                                                                            menu:	[
                                                                                        {
                                                                                            text:'Cat&aacute;logo de status de productos',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        gEx('tblCenter').load(
                                                                                                                                  {
                                                                                                                                      url:'../modeloAlmacenes/tblTiemposMovimientoAlmacen.php',
                                                                                                                                      scripts:true,
                                                                                                                                      params:	{
                                                                                                                                                  idAlmacen:gE('idAlmacen').value,
                                                                                                                                                  cPagina:'sFrm=true'
                                                                                                                                                  
                                                                                                                                               }
                                                                                                                                    }
                                                                                                                              );  
                                                                                                    }
                                                                                        },
                                                                                        {
                                                                                            text:'Movimientos de almac&eacute;n',
                                                                                            handler:function()
                                                                                                    {
                                                                                                        gEx('tblCenter').load(
                                                                                                                                  {
                                                                                                                                      url:'../modeloAlmacenes/tblPerfilesMovimientoAlmacen.php',
                                                                                                                                      scripts:true,
                                                                                                                                      params:	{
                                                                                                                                                  idAlmacen:gE('idAlmacen').value,
                                                                                                                                                  cPagina:'sFrm=true'
                                                                                                                                                  
                                                                                                                                               }
                                                                                                                                    }
                                                                                                                              ); 
                                                                                                    }
                                                                                        }
                                                                                    ]
                                                                        },
                                                                        
                                                                        {
                                                                            icon:'../images/coins_minus.png',
                                                                            cls:'x-btn-text-icon',
                                                                            //id:'btnAgregarDescuento',
                                                                            text:'Administrar descuentos',
                                                                            handler:function()
                                                                                    {
                                                                                    
                                                                                        
                                                                                        
                                                                                        mostrarAdmonDescuentos();
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                    ]
                                                        },'-',
                                                        {
                                                            text:'Reportes',
                                                            menu:	[
                                                            			{
                                                                            text:'Cat&aacute;logo de art&iacute;culos',
                                                                            handler:function()
                                                                                    {
                                                                                    
                                                                                    	var arrParam=[['idAlmacen',gE('idAlmacen').value]];
                                                                                        enviarFormularioDatos('../reportes/Almacen/catalogoArticuloAlmacen.php',arrParam);
                                                                                    
                                                                                    }
                                                                        },
                                                                        {
                                                                            text:'Existencia de productos',
                                                                            handler:function()
                                                                                    {
                                                                                    	var arrParam=[['idAlmacen',gE('idAlmacen').value]];
                                                                                        enviarFormularioDatos('../reportes/sigloxxi/existencia.php',arrParam);
                                                                                        
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                                    ]
                                                        }
                                                    ],
                                            listeners:{
                                                            documentloaded:function(el)
                                                                        {
                                                                            var body=el.getFrameDocument().getElementsByTagName('body');
                                                                            body[0].onclick=function(val)	
                                                                                            {
                                                                                                window.parent.Ext.menu.MenuMgr.hideAll();
                                                                                            }
                                                                        } 
                                                        },
                                            loadMask:	{
                                                            msg:'Cargando'
                                                        }
                                        }
                                    ]
                        }
                    )
                                  
	gEx('tblCenter').load(
                              {
                                  url:'../modeloAlmacenes/tblProductosAlmacen.php',
                                  scripts:true,
                                  params:	{
                                              idAlmacen:gE('idAlmacen').value,
                                              cPagina:'sFrm=true'
                                              
                                           }
                                }
                          );                                   
}

function mostrarAdmonDescuentos()
{
	var gridDescuentos=crearGridDescuentosProducto(gE('idAlmacen').value,'');
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridDescuentos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Descuentos por Almac&eacute;n',
										width: 800,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function agregarDescuentoProducto(idAlmacen,minFechaDescuento)
{
	var fechaActual=Date.parseDate('<?php echo date('Y-m-d')?>','Y-m-d');
    var minFecha=fechaActual;
    if(fechaActual<minFechaDescuento)
    	minFecha=minFechaDescuento;
    var x;
    /*for(x=0;x<arrNodos.length;x++)
    {
    	var idFila=arrNodos[x].getAttribute('idFila');
		var fila=gEx('arbolCostos').getStore().getById(idFila);
        if(fila.data.fechaInicio)
        {
        	
            if((minFecha<fila.data.fechaInicio)&&())
            
            	minFecha=fila.data.fechaInicio;
            
        }
    }*/
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Porcentaje descuento:'
                                                        },
                                                        
                                                        {
                                                        	x:135,
                                                            y:5,
                                                            width:70,
                                                            id:'txtDescuento',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigencia',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:255,
                                                            y:40,
                                                            html:'al:'
                                                        },
                                                        {
                                                        	x:280,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigenciaAl',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n del descuento:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'textarea',
                                                            height:60,
                                                            width:450,
                                                            id:'txtDescripcion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar descuento por Almac&eacute;n',
										width: 500,
										height:245,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtDescuento').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var cadObj='';
                                                                        var obj='';
																		var x;
                                                                        var dteVigencia=gEx('dteVigencia');
                                                                        var dteVigenciaAl=gEx('dteVigenciaAl');
                                                                        var txtDescuento=gEx('txtDescuento');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        
                                                                        if(txtDescuento.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado no es v&aacute;lido',resp);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(parseFloat(txtDescuento.getValue())>100)
                                                                        {
                                                                        	function resp10()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado NO puede ser mayor a 100%',resp10);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicial del descuento',resp2);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigenciaAl.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteVigenciaAl.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha t&eacute;mino del descuento',resp3);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()>dteVigenciaAl.getValue())
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio no puede ser mayor que la fecha de t&eacute;rmino',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtDescripcion.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	txtDescripcion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del descuento',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                            
                                                                        obj='{"descripcion":"'+cv(txtDescripcion.getValue())+'","fechaInicio":"'+dteVigencia.getValue().format('Y-m-d')+'","fechaTermino":"'+dteVigenciaAl.getValue().format('Y-m-d')+'","descuento":"'+txtDescuento.getValue()+
                                                                            '","idProducto":"'+idAlmacen+'","llave":"","tipoDescuento":"2"}';
                                                                        
                                                                        
                                                                        
                                                                        var cObj='{"aRegistros":['+obj+']}'
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridDescuentos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=185&cadObj='+cObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDescuentosProducto(idAlmacen,llave)
{
	var fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>',"Y-m-d");
       
       var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDescuento'},
		                                                {name: 'porcentajeDescuento'},
		                                                {name:'fechaInicio', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaTermino', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'descripcionDescuento'},
                                                        {name: 'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'situacion'},
                                                        {name: 'motivoCancelacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicio', direction: 'DESC'},
                                                            groupField: 'fechaInicio',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='184';
                                        proxy.baseParams.idProducto=idAlmacen;
                                        proxy.baseParams.llave=llave;
                                        proxy.baseParams.tipoDescuento=2;
                                        
                                        gEx('btnPromoModif').disable();
	                                    gEx('btnPromoDel').disable();
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Porcentaje<br>descuento',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'porcentajeDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return removerCerosDerecha(val)+' %';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha inicio',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaInicio',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha t&eacute;rmino',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaTermino',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n del descuento',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcionDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var comp='';
                                                                            if(registro.data.motivoCancelacion!='')
	                                                                            comp=' <img src="../images/icon_comment.gif" title="'+escaparBR(registro.data.motivoCancelacion)+'" alt="'+escaparBR(registro.data.motivoCancelacion)+'" />';
                                                                        	switch(val)
                                                                            {
                                                                            	case '0':
                                                                                	return 'Inactivo'+ comp;
                                                                                break;
                                                                            	case '1':
                                                                                	return 'Activo'+ comp;
                                                                                break;
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                header:'Registrado el',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s')
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridDescuentos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:350,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                
                                                                                text:'Agregar descuento',
                                                                                handler:function()
                                                                                        {
                                                                                        
                                                                                        	var minFecha=fechaActual;
                                                                                            var gridDescuentos=gEx('gridDescuentos');
                                                                                            var ct=0;
                                                                                            var fila;
                                                                                            for(ct=0;ct<gridDescuentos.getStore().getCount();ct++)
                                                                                            {
                                                                                            	fila=gridDescuentos.getStore().getAt(ct);
                                                                                                if(fila.data.situacion=='1')
                                                                                                {
                                                                                                    if(minFecha<fila.data.fechaTermino.add(Date.DAY,1))
                                                                                                        minFecha=fila.data.fechaTermino.add(Date.DAY,1);
                                                                                                }
                                                                                            }
                                                                                         	agregarDescuentoProducto(idAlmacen,minFecha);   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnPromoModif',
                                                                                text:'Modificar fecha de t&eacute;rmino',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento cuya fecha de t&eacute;rmino desea modificar');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            mostrarVentanaFinalizarFechaTermino(fila);
                                                                                            
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            ,'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover descuento',
                                                                                id:'btnPromoDel',
                                                                                disabled:true,
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento que desea remover');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            if(fila.data.fechaInicio<=fechaActual)
                                                                                            {
                                                                                            	msgBox('No se puede remover descuentos cuya fecha de vigencia haya comenzado');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            gEx('gridDescuentos').getStore().reload();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=186&idDescuento='+fila.data.idDescuento,true);
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el descuento seleccionado?',resp);
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
                                                        
		tblGrid.getSelectionModel().on('rowselect',function(sm,numFila,registro)
        						{
                                	gEx('btnPromoModif').disable();
                                    gEx('btnPromoDel').disable();
                                	if(registro.data.fechaInicio>fechaActual)
                                    {
                                    	gEx('btnPromoDel').enable();
                                    
                                    }
                                    else
                                    {
                                    	if(registro.data.fechaTermino>=fechaActual)
                                        	gEx('btnPromoModif').enable();
                                    }
                                }
        			)                                                        
                                                        
        return 	tblGrid;	
	
}


function mostrarVentanaFinalizarFechaTermino(fila)
{
	var fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>',"Y-m-d");
    
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Ingrese la nueva fecha de t&eacute;rmino: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:30,
                                                        	xtype:'label',
                                                            html:'Si la fecha de t&eacute;rmino coincide con la fecha actual o la fecha de inicio del descuento, &eacute;ste inmediatamente ser&aacute; cancelado'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:5,
                                                            id:'dteFechaTermino',
                                                            xtype:'datefield',
                                                            value:fechaActual,
                                                            minValue:fila.data.fechaInicio,
                                                            maxValue:fila.data.fechaTermino
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo del cambio: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:500,
                                                            height:80,
                                                            xtype:'textarea',
                                                            id:'txtMotivo'
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar fecha de t&eacute;rmino',
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dteFechaTermino=gEx('dteFechaTermino');
																		if(dteFechaTermino.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaTermino.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha de t&eacute;rmino del descuento',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                            function resp2()
                                                                            {
                                                                                txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo del cambio de fecha de t&eacute;rmino del descuento',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        function respConf(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gridDescuentos').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=191&idDescuento='+fila.data.idDescuento+'&motivoCancelacion='+
                                                                                cv(txtMotivo.getValue())+'&fechaTermino='+dteFechaTermino.getValue().format("Y-m-d"),true);

                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer modificar la fecha de t&eacute;rmino del descuento?',respConf);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function desPresionarBotones(btn)
{
	var x;
    var ct=100;
    var btn;
    var boton;
    return;
    for(x=1;x<=ct;x++)
    {
        if(btn.id!=('btn'+x))
        {
        	boton=gEx('btn'+x);
            if(boton)
            	boton.toogle(false,true);
            else
            	return;
        }
    }
}

function recargarGridContratos()
{
	gEx('tblCenter').getFrameWindow().recargarGridContratos();	
}

function asignarNuevoEmpleado(idUsuario,nombre)
{
	gEx('tblCenter').getFrameWindow().asignarNuevoUsuario(idUsuario,nombre);	
}

function imprimirContrato(idContrato)
{
	gEx('tblCenter').getFrameWindow().refrescarContratos(idContrato);
	window.parent.cerrarVentanaFancy();
	
}

function modificarAlmacen()
{
	var objAlmacen=eval(bD(gE('objAlmacen').value))[0];
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:20,
                                                            y:10,
                                                            html:'Cve. Almac&eacute;n:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:180,
                                                            id:'txtCveAlmacen',
                                                            value:objAlmacen.cveAlmacen
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                            html:'Nombre del Almac&eacute;n:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:300,
                                                            id:'txtNombreAlmacen',
                                                            value:objAlmacen.nombreAlmacen
                                                        },
                                                        {
                                                        	x:20,
                                                            y:70,
                                                            html:'Descripci&oacute;n:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:350,
                                                            height:80,
                                                            id:'txtDescripcion',
                                                            value:objAlmacen.descripcion.replace(/<br \/>/gi,'\n')
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar almac&eacute;n',
										width: 580,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCveAlmacen').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCveAlmacen=gEx('txtCveAlmacen');
                                                                        if(txtCveAlmacen.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtCveAlmacen.focus();
                                                                            }
                                                                            msgBox('Debe especificar la clave del almac&eacute;n');
                                                                            return;
                                                                        }
                                                                        var txtNombreAlmacen=gEx('txtNombreAlmacen');
                                                                        if(txtNombreAlmacen.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtNombreAlmacen.focus();
                                                                            }
                                                                            msgBox('Debe especificar el nombre del almac&eacute;n');
                                                                            return;
                                                                        }
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        var cadObj='{"idAlmacen":"'+gE('idAlmacen').value+'","cveAlmacen":"'+txtCveAlmacen.getValue()+'","nombreAlmacen":"'+cv(txtNombreAlmacen.getValue())+'","descripcion":"'+cv(txtDescripcion.getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                recargarPagina();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=144&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    

}