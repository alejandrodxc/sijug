<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idAlmacen,nombreAlmacen from 6900_almacenes ORDER BY nombreAlmacen";
	$res=$con->obtenerFilas($consulta);
	$listAlmacen="";
	$arrAlmacenes="";
	$arrAlmacenesDisp="";
	while($fila=mysql_fetch_row($res))
	{
		$o="['".$fila[0]."','".cv($fila[1])."']";
		if($arrAlmacenes=="")
			$arrAlmacenes=$o;
		else
			$arrAlmacenes.=",".$o;
		if($listAlmacen=="")
			$listAlmacen=$fila [0];
		else
			$listAlmacen.=",".$fila [0];
	}
	$arrAlmacenesDisp=$arrAlmacenes;
	$o="['".$listAlmacen."','Cualquier almacén']";
	if($arrAlmacenes=="")
		$arrAlmacenes=$o;
	else
		$arrAlmacenes.=",".$o;
	
	
	$consulta="SELECT id__1005_tablaDinamica,nombreTipoEntrada FROM _1005_tablaDinamica ORDER BY nombreTipoEntrada";
	$arrMotivo=$con->obtenerFilasArreglo($consulta);
?>
var arrMotivo=<?php echo $arrMotivo?>;
var arrAlmacenesDisp=[<?php echo $arrAlmacenesDisp?>];
var arrAlmacenes=[<?php echo $arrAlmacenes?>];



Ext.onReady(inicializar);

function inicializar()
{
	var cmbAlmacen=crearComboExt('cmbAlmacen',arrAlmacenes,0,0,280);
    cmbAlmacen.setValue(arrAlmacenes[arrAlmacenes.length-1][0]);
    cmbAlmacen.on('select',function()
    						{
                            	var lastOptions = gEx('gridBajas').getStore().lastOptions;
                                Ext.apply(lastOptions.params, {
                                                                    idAlmacen: gEx('cmbAlmacen').getValue()
                                                                }
                                         );
                            	gEx('gridBajas').getStore().reload(lastOptions);
                            }
    			);
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Ingresos de productos</b></span>',
                                               	tbar:	[
                                                			{
                                                            	xtype:'label',
                                                                html:'<span style="color:#000; font-weight:bold">Ver ingresos de producto del almac&eacute;n:&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbAlmacen
                                                        ],
                                                items:	[
                                                            crearGridBajas()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearGridBajas()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAltaProducto'},
		                                                {name:'fechaAlta', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'Nombre'},
                                                        {name: 'descripcionProducto'},
                                                        {name: 'cantidadAlta'},
                                                        {name: 'idMotivoAlta'},
                                                        {name: 'comentariosAdicionales'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaBaja', direction: 'DESC'},
                                                            groupField: 'descripcionProducto',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='188';
                                        proxy.baseParams.idAlmacen=gEx('cmbAlmacen').getValue();
                                        
                                        

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            {type: 'string', dataIndex: 'descripcionProducto'},
                                                                            {type: 'date', dataIndex: 'fechaAlta'},
                                                                            {type: 'string', dataIndex: 'Nombre'},
                                                                            {type: 'list',dataIndex:'idMotivoAlta',options:arrMotivo,phpMode:true}
                                                                            
                                                                        ]
                                                        }
                                                    );    
                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Fecha de ingreso',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaAlta',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s');
                                                                        }
                                                            },
                                                            {
                                                                header:'Producto',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'descripcionProducto'
                                                            },
                                                            
                                                            {
                                                                header:'Registrado por',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'Nombre'
                                                            },
                                                            {
                                                                header:'Cantidad',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'cantidadAlta'
                                                            },
                                                            {
                                                                header:'Motivo del ingreso',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'idMotivoAlta',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrMotivo,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Comentarios adicionales',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'comentariosAdicionales',
                                                                renderer:mostrarValorDescripcion,
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridBajas',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar ingreso de producto',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaRegistro();
                                                                                        }
                                                                                
                                                                            },'-'
                                                                            
                                                                            
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:188,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		                                                                                
        return 	tblGrid;
}

function mostrarVentanaRegistro()
{
	var cmbAlmacenPedido=crearComboExt('cmbAlmacenPedido',arrAlmacenesDisp,130,5,350);
    cmbAlmacenPedido.setValue(arrAlmacenesDisp[0][0]);
    var cmbMotivoBaja=crearComboExt('cmbMotivoBaja',arrMotivo,130,95,350);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Almac&eacute;n:'
                                                        },
                                                        cmbAlmacenPedido,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Producto:'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            readOnly:true,
                                                            width:350,
                                                            x:130,
                                                            y:35,
                                                            id:'txtProducto'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:495,
                                                            y:35,
                                                            html:'<a href="javascript:buscarPorProductoNombre()"><img src="../images/pencil.png" title="Agregar producto"></a>&nbsp;&nbsp;<a href="javascript:removerProducto()"><img src="../images/delete.png" title="Remover producto"></a>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Existencia:'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            readOnly:true,
                                                            width:70,
                                                            x:130,
                                                            y:65,
                                                            id:'txtExistencia'
                                                        },
                                                        {
                                                        	x:240,
                                                            y:70,
                                                            html:'Cantidad ingreso:'
                                                        },
                                                        {
                                                        	xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:70,
                                                            x:330,
                                                            y:65,
                                                            id:'txtCantidad'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Motivo del ingreso:'
                                                        },
                                                        cmbMotivoBaja,
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:40,
                                                            y:160,
                                                            width:550,
                                                            height:80,
                                                            id:'txtComentarios',
                                                            xtype:'textarea'
                                                        }
														


													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Ingreso de producto',
										width: 650,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtProducto=gEx('txtProducto');
                                                                        var txtCantidad=gEx('txtCantidad');
                                                                        var txtComentarios=gEx('txtComentarios');
                                                                        var txtExistencia=gEx('txtExistencia');
                                                                        if((!txtProducto.idProducto)||(txtProducto.idProducto==-1))
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	
                                                                            }
                                                                            msgBox('Debe indicar el producto cuyo ingreso desea registrar',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                       /* if(txtCantidad.getValue()>txtExistencia.getValue())
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgBox('La cantidad de producto ingresada excede la cantidad existente del mismo',resp4);
                                                                            return;
                                                                        }*/
                                                                        
                                                                        if(txtCantidad.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtCantidad.focus();
                                                                            }
                                                                            msgBox('Debe indicar la cantidad de producto cuyo ingreso desea realizar',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbMotivoBaja.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbMotivoBaja.focus();
                                                                            }
                                                                            msgBox('Debe indicar el motivo del ingreso del producto',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        function respConf(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                           		var cadObj='{"idAlmacen":"'+cmbAlmacenPedido.getValue()+'","idProducto":"'+txtProducto.idProducto+'","llave":"'+txtProducto.llave+
                                                                                        '","cantidad":"'+txtCantidad.getValue()+'","motivoAlta":"'+cmbMotivoBaja.getValue()+'","comentarios":"'+cv(txtComentarios.getValue())+'"}';
                                                                                        
                                                                            
                                                                            function funcAjax()
                                                                            {
                                                                                var resp=peticion_http.responseText;
                                                                                arrResp=resp.split('|');
                                                                                if(arrResp[0]=='1')
                                                                                {
                                                                                    gEx('gridBajas').getStore().reload();
                                                                                    ventanaAM.close();
                                                                                }
                                                                                else
                                                                                {
                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                }
                                                                            }
                                                                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=187&cadObj='+cadObj,true);
                                                                                   
                                                                            }
																		}
                                                                        msgConfirm('Est&aacute; seguro de querer registrar el ingreso del producto?',respConf);
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function buscarPorProductoNombre()
{
	var arrCriterio=[['1','Comienza con...'],['2','Contiene la palabra...'],['3','C\xF3digo de barras'],['4','C\xF3digo alterno']];
	var cmbCriterioBusqueda=crearComboExt('cmbCriterioBusqueda',arrCriterio,170,5,250);
    cmbCriterioBusqueda.setValue('2');
    cmbCriterioBusqueda.on('select',function(cmb,registro)
    								{
                                    	gEx('txtNombre').focus(false,500);
                                        gEx('txtNombre').setValue('');
                                    	cargarProductosBusqueda();
                                        
                                        switch(registro.data.id)
                                        {
                                        	case '1':
                                            case '2':
                                            	gEx('lblDescripcion').setText('<b>Descripci&oacute;n del producto:</b>',false);
                                            break;
                                            case '3':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo de barras:</b>',false);
                                            break;
                                            case '4':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo alterno:</b>',false);
                                            break;
                                        }
                                        
                                    
                                    }
    						);
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>Criterio de b&uacute;squeda:</b>'
                                                                        },
                                                                        cmbCriterioBusqueda,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            id:'lblDescripcion',
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        {
                                                                            x:170,
                                                                            y:35,
                                                                            xtype:'textfield',
                                                                            width:330,
                                                                            enableKeyEvents:true,
                                                                            id:'txtNombre',
                                                                            listeners:	{
                                                                            				keyup:cargarProductosBusqueda
                                                                            			}
                                                                        }
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:410,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtNombre').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var gProductosBuscados=gEx('gProductosBuscados');
                                                                        var fila=gProductosBuscados.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto cuyo ingreso desea registrar');
                                                                            return;
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('txtProducto').setValue(fila.data.nombreProducto.replace(/<b>/gi,'').replace(/<\/b>/gi,''));
                                                                                gEx('txtProducto').idProducto=fila.data.idProducto;
                                                                                gEx('txtProducto').llave=fila.data.llave;
                                                                                gEx('txtExistencia').setValue(removerCerosDerecha(arrResp[1]));
                                                                                gEx('txtCantidad').focus();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=180&idAlmacen='+gEx('cmbAlmacenPedido').getValue()+'&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave,true);
                                                                        
                                                                    	
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
   // cargarProductosBusqueda();
      
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            {
                                                                            	var checado='';
                                                                        		
                                                                                return [
                                                                                           
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda();
}

function cargarProductosBusqueda()
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:gEx('cmbCriterioBusqueda').getValue(),
                                                            valor:gEx('txtNombre').getValue(),
                                                            idZona:0
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'precioUnitario'},
                                                        {name: 'codigoBarras'},
                                                        {name: 'codigoAlterno'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo de barras',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoBarras',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo alterno',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoAlterno',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,                                                                
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}

function removerProducto()
{
	gEx('txtProducto').setValue('');
    gEx('txtProducto').idProducto=-1;
}

