<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT id__1031_tablaDinamica,color FROM _1031_tablaDinamica ORDER BY color";
	$arrColores=$con->obtenerFilasArreglo($consulta);
?>

var arrColores=<?php echo $arrColores?>;

Ext.onReady(inicializar);

function inicializar()
{
	crearGridInventarioAlmacen();
	crearGridComprasAlmacen();
	 

	
}


function crearGridInventarioAlmacen()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'color'},
		                                                {name: 'existencia'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_Transportes.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'color', direction: 'ASC'},
                                                            groupField: 'color',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='9';
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            {
                                                                header:'Color',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'color',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrColores,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Existencia',
                                                                width:150,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'existencia',
                                                                renderer:function(val)
                                                                		{
                                                                        	return Ext.util.Format.number(val,'0.00');
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gInventarioAlmacen',
                                                                store:alDatos,
                                                                renderTo:'tblInventario',
                                                                frame:false,
                                                                cm: cModelo,
                                                                width:700,
                                                                height:250,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function crearGridComprasAlmacen()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idCompra'},
		                                                {name: 'fechaCompra',type:'date',format:'Y-m-d'},
		                                                {name: 'serie'},
                                                        {name: 'folio'},
                                                        {name: 'proveedor'},
                                                        {name: 'montoTotal'}
                                                       
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_Transportes.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaCompra', direction: 'DESC'},
                                                            groupField: 'fechaCompra',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='10';
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            {
                                                                header:'Fecha de compra',
                                                                width:130,
                                                                sortable:true,
                                                                dataIndex:'fechaCompra',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Serie',
                                                                width:100,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'serie'
                                                            },
                                                            {
                                                                header:'Folio factura',
                                                                width:100,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'folio'
                                                            },
                                                            {
                                                                header:'Proveedor',
                                                                width:320,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'proveedor'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gCompras',
                                                                store:alDatos,
                                                                renderTo:'tblCompras',
                                                                frame:false,
                                                                cm: cModelo,
                                                                width:700,
                                                                height:250,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}