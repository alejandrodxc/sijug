<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago ";
	$arrFormaPago=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idFormaPago,formaPago FROM 600_formasPago WHERE idFormaPago IN (1,7)";
	$arrFormasPagoAbono=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idMotivo,motivoDevolucion FROM 6948_motivosDevolucion WHERE aplicableDevolucion IN (1,2) ORDER BY motivoDevolucion";
	$arrMotivoDevolucion=$con->obtenerFilasArreglo($consulta);
?>
var arrMotivoDevolucion=<?php echo $arrMotivoDevolucion?>;

var arrFormasPagoAbono=<?php echo $arrFormasPagoAbono?>;
var arrFormaPagoBD=<?php echo $arrFormaPago?>;
var regProductoPedido=null;
Ext.onReady(inicializar);



function inicializar()
{
	regProductoPedido=crearRegistro(	[
    										{name: 'idProducto'},
                                            {name: 'llave'},
                                            {name: 'producto'},
                                            {name: 'costoUnitario'},
                                            {name: 'subtotal'},
                                            {name: 'iva'},
                                            {name: 'cantidad'},
                                            {name: 'total'},
                                            {name: 'montoFinal'}
    									]);	
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Devoluciones a proveedor</b></span>',
                                               	
                                                items:	[
                                                            crearGridAdeudos()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearGridAdeudos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDevolucion'},
                                                        {name: 'rfc'},
		                                                {name: 'proveedor'},
                                                        {name: 'idProveedor'},
		                                                {name:'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'folioDevolucion'},
                                                        {name: 'montoTotal'},
                                                        {name: 'descripcion'},
                                                        {name: 'idPedido'},
                                                        {name: 'total'} ,
                                                        {name: 'montoFinal'},
                                                        {name: 'idMotivoDevolucion'}                                                                                                    
                                                        
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaCreacion', direction: 'DESC'},
                                                            groupField: 'fechaCreacion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='177';
                                       

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            {type: 'string', dataIndex: 'rfc'},
                                                                            {type: 'string', dataIndex: 'proveedor'},
                                                                            {type: 'date', dataIndex: 'fechaRegistro'},
                                                                            {type: 'int', dataIndex: 'idPedido'},
                                                                            {type: 'list', dataIndex: 'idMotivoDevolucion',phpMode:true,options:arrMotivoDevolucion}
                                                                            
                                                                            
                                                                        ]
                                                        }
                                                    );    
		var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                expandOnEnter:false,
                                                tpl : new Ext.Template(
                                                    '<table >',
                                                    '<tr><td style="padding:5px; color:#666; font-style:italic">{descripcion}</td></tr>',
                                                    '</table>'
                                                )
                                            });                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'Folio de la devoluci&oacute;n',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'idDevolucion'
                                                            },
                                                            {
                                                                header:'Fecha del registro',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'RFC Proveedor',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'rfc'
                                                            },
                                                            {
                                                                header:'Proveedor',
                                                                width:260,
                                                                sortable:true,
                                                                dataIndex:'proveedor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Folio del pedido',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'idPedido'
                                                            },
                                                            {
                                                                header:'Monto del pedido',
                                                                width:130,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'total',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Monto total devoluci&oacute;n',
                                                                width:130,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'montoTotal',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Motivo de la devoluci&oacute;n',
                                                                width:180,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'idMotivoDevolucion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrMotivoDevolucion,val);
                                                                        }
                                                            },

                                                            {
                                                                header:'Diferencia compra',
                                                                width:130,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'montoFinal',
                                                                renderer:'usMoney'
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridDevolucion',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters,expander],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar devoluci&oacute;n',
                                                                                handler:function()
                                                                                        {
                                                                                        	
                                                                                            
                                                                                            mostrarVentanaRegistro();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            
                                                                            {
                                                                                icon:'../images/printer.png',
                                                                                cls:'x-btn-text-icon',
                                                                                
                                                                                text:'Reimprimir devoluci&oacute;n',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la devoluci&oacute;n que desea reimprimir');
                                                                                            	return;
                                                                                            }
                                                                                           
                                                                                            var arrParam=[['idDevolucion',fila.data.idDevolucion]];
                                                                                            enviarFormularioDatos('../reportes/sigloxxi/devolucionProveedor.php',arrParam,'POST','iImprimir');//
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:177,
                                                    
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
        											{
                                                    	
                                                    }
        							)                                                                                   
        return 	tblGrid;
}


function mostrarVentanaRegistro()
{

	
	
    
	
    var cmbMotivoDevolucion=crearComboExt('cmbMotivoDevolucion',arrMotivoDevolucion,155,280,320);
    var gridProductos=crearGridProductosPedido();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
														 {
                                                        	x:10,
                                                            y:10,
                                                            html:'Folio del pedido:'
                                                        },
														{
															x:155,
															y:5,
															xtype:'textfield',
															width:100,
															id:'txtFolio',
															enableKeyEvents:true,
															listeners:	{
																				'change':buscarPedido,
																				'keypress':function(ctrl,e)
																								{
																									if(e.keyCode==13)
																										buscarPedido();
																								}
																			}
														},
														
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del Proveedor:'
                                                        },
                                                        {
															x:155,
															y:35,
															xtype:'textfield',
															width:450,
															readOnly:true,
															id:'txtProveedor'
														},
                                                         {
                                                        	xtype:'label',
                                                            x:610,
                                                            y:40,
                                                            html:'<a href="javascript:buscarPedidoProveedor()"><img src="../images/magnifier.png" title="Buscar pedido"></a>'
                                                        },
                                                        gridProductos,
                                                        {
                                                        	x:10,
                                                            y:285,
                                                            html:'Motivo de la devoluci&oacute;n:'
                                                        },
                                                        cmbMotivoDevolucion,
                                                        {
                                                        	x:10,
                                                            y:315,
                                                            xtype:'label',
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:155,
                                                            y:310,
                                                            xtype:'textarea',
                                                            width:350,
                                                            height:90,
                                                            id:'comentarios'
                                                        },
                                                        {
                                                        	x:520,
                                                            y:275,
                                                            xtype:'fieldset',
                                                            title:'Datos del pedido',
                                                            width:300,
                                                            layout:'absolute',
                                                            height:120,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold">Monto total pedido:</span>'
                                                                        },
                                                                        {
                                                                        	x:150,
                                                                            y:0,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-weight:bold" id="lblMontoTotal">$ 0.00</span>'
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold">Monto total devoluci&oacute;n:</span>'
                                                                        },
                                                                        {
                                                                        	x:150,
                                                                            y:30,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-weight:bold" id="lblMontoDevolucion">$ 0.00</span>'
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#000; font-weight:bold">Diferencia:</span>'
                                                                        },
                                                                        {
                                                                        	x:150,
                                                                            y:60,
                                                                            xtype:'label',
                                                                            html:'<span style="color:#900; font-weight:bold" id="lblDiferencia">$ 0.00</span>'
                                                                        }
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar devoluci&oacute;n de producto',
										width: 870,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtFolio').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															id:'btnAceptarDev',
                                                            text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                        	handler: function()
																	{
																		
                                                                        var txtFolio=gEx('txtFolio');
                                                                        if(txtFolio.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtFolio.focus();
                                                                            }
                                                                            msgBox('Debe especificar el folio del pedido cuya devoluci&oacute;n desea registrar',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(cmbMotivoDevolucion.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbMotivoDevolucion.focus();
                                                                            }
                                                                            mxgBox('Debe especificar el motivo de la devoluci&oacute;n',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        var gProductosPedidos=gEx('gProductosPedidos');
                                                                        var fila;
                                                                        var x;
                                                                        var arrProductos='';
                                                                        var aux='';
                                                                        var total=0;
                                                                        var subtotal=0;
                                                                        var iva=0;
                                                                        
                                                                        for(x=0;x<gProductosPedidos.getStore().getCount();x++)
                                                                        {
                                                                        	var fila=gProductosPedidos.getStore().getAt(x);
                                                                            if(parseFloat(fila.data.cantidadDevolucion)>0)
                                                                            {
                                                                                aux='{"llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+'","costoUnitario":"'+fila.data.costoUnitario+
                                                                                    '","cantidad":"'+fila.data.cantidadDevolucion+'","subtotal":"'+fila.data.subtotalDevolucion+'","iva":"'+fila.data.ivaDevolucion+'","total":"'+
                                                                                    fila.data.totalDevolucion+'"}';
                                                                                total+=parseFloat(fila.data.totalDevolucion);
                                                                                subtotal+=parseFloat(fila.data.subtotalDevolucion);
                                                                                iva+=parseFloat(fila.data.ivaDevolucion);
                                                                                if(arrProductos=='')
                                                                                    arrProductos=aux;
                                                                                else
                                                                                    arrProductos+=','+aux
                                                                       		}
                                                                            
                                                                        }
                                                                        if(arrProductos=='')
                                                                        {
                                                                        	msgBox('Almenos debe indicar la devoulci&oacute;n de un producto');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"idMotivoDevolucion":"'+cmbMotivoDevolucion.getValue()+'","subtotal":"'+subtotal+'","iva":"'+iva+'","total":"'+total+'","comentarios":"'+cv(gEx('comentarios').getValue())+'","arrProductos":['+arrProductos+
                                                                        			'],"idPedido":"'+txtFolio.getValue()+'"}';
                                                                        function respDev(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridDevolucion').getStore().reload();
                                                                                        var arrParam=[['idDevolucion',arrResp[1]]];
                                                                                        enviarFormularioDatos('../reportes/sigloxxi/devolucionProveedor.php',arrParam,'POST','iImprimir');
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=176&cadObj='+cadObj,true);
                                                                        	}
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer registrar la devoluci&oacute;n del producto?',respDev);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridProductosPedido()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'iva'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'total'},
																	{name:'cantidadDisponible'},
																	{name:'cantidadDevolucion'},
																	{name: 'subtotalDevolucion'},
																	{name: 'ivaDevolucion'},
																	{name: 'totalDevolucion'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Producto',
															width:250,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:mostrarValorDescripcion
														},
														{
															header:'Costo Unitario',
															width:100,
                                                            align:'right',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:'usMoney'
														},
                                                        {
															header:'Cantidad',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidad',
                                                            renderer:function(val)
																		{
																			return removerCerosDerecha(val);
																		}
														},
														
														
														{
															header:'Cantidad <br>disponible',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidadDisponible',
                                                            renderer:function(val)
																		{
																			return removerCerosDerecha(val);
																		}
														},
														{
															header:'Cantidad <br>devolución',
															width:85,
                                                            align:'right',
															sortable:true,
															dataIndex:'cantidadDevolucion',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false},
                                                            renderer:function(val)
																		{
																			return removerCerosDerecha(val);
																		}
														},
														
														{
															header:'Total devolución<br>(Inc. IVA)',
															width:110,
                                                            align:'right',
															sortable:true,
															dataIndex:'totalDevolucion',
                                                            renderer:'usMoney'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:80,
                                                            x:10,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidos',
                                                            columnLines : true,
                                                            height:190,
                                                            width:810,
                                                            sm:chkRow,
                                                            plugins:[summary]
                                                           
                                                        }
                                                    );
	tblGrid.on('afteredit',function(e)
    						{
								
								if(parseFloat(e.value)>parseFloat(e.record.data.cantidadDisponible))
								{
									
									function resp()
									{
										e.record.set('cantidadDevolucion',e.originalValue);
										
									}
									msgBox('La cantidad a devolver <b>NO</b> puede ser mayor que la cantidad disponible', resp);
									return;
								}
                            	
								e.record.set('subtotalDevolucion',parseFloat(e.value)*parseFloat(e.record.data.costoUnitario));
								
								var ivaUnitario=parseFloat(e.record.data.iva)/parseFloat(e.record.data.cantidad);
								
								e.record.set('ivaDevolucion',parseFloat(e.value)*parseFloat(ivaUnitario));
								e.record.set('totalDevolucion',parseFloat(e.record.data.ivaDevolucion)+parseFloat(e.record.data.subtotalDevolucion));
								
								
								sumarTotalDevolucion();
								
								
								
                            }
    			)                                                    
	return 	tblGrid;		
}

function sumarTotalDevolucion()
{
	var gProductosPedidos=gEx('gProductosPedidos');
    var f;
    var x;
    var total=0;
    for(x=0;x<gProductosPedidos.getStore().getCount();x++)
    {
    	f=gProductosPedidos.getStore().getAt(x);
        if(!f.data.totalDevolucion)
        	f.data.totalDevolucion=0;
        total+=parseFloat(f.data.totalDevolucion);
    }
    gE('lblMontoDevolucion').innerHTML=Ext.util.Format.usMoney(total);
    var lblMontoTotal=parseFloat(normalizarValor(gE('lblMontoTotal').innerHTML));
	gE('lblDiferencia').innerHTML=Ext.util.Format.usMoney(lblMontoTotal-total);
}

function mostrarAbonosAdeudo(fila)
{
	var gridAbonosHistorial=crearGridAbonosHistorial(fila);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridAbonosHistorial

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Historial de abonos',
										width: 700,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridAbonosHistorial(fila)
{

	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAbono'},
		                                                {name: 'fechaAbono',type:'date',dateFormat:'Y-m-d'},
		                                                {name:'montoAbono'},
                                                        {name: 'formaPago'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaAbono', direction: 'ASC'},
                                                            groupField: 'fechaAbono',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='37';
                                        proxy.baseParams.idAdeudo=fila.data.idAdeudo;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Fecha abono',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaAbono',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Monto abono',
                                                                width:150,
                                                                css:'text-align:right;',
                                                                sortable:true,
                                                                dataIndex:'montoAbono',
                                                                renderer:'usMoney'
                                                            },
                                                            
                                                            {
                                                                header:'Forma de pago',
                                                                width:220,
                                                                
                                                                sortable:true,
                                                                dataIndex:'formaPago',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrFormaPagoBD,val);
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAbonosHistorial',
                                                                store:alDatos,
                                                                x:10,
                                                                y:10,
                                                                height:680,
                                                                height:250,
                                                                frame:false,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar abono',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentaRegistrarAbono(fila);
                                                                                        }
                                                                                
                                                                            }
                                                                         ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function mostrarVentaRegistrarAbono(fila)
{
	var cmbFormaPagoAbono=crearComboExt('cmbFormaPagoAbono',arrFormasPagoAbono,360,35,130);
    cmbFormaPagoAbono.setValue('1');
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'<b>Monto adeudo:</b>&nbsp;&nbsp;<span id="lblMontoAbono" style="font-weight:bold;color:#900; ">'+Ext.util.Format.usMoney(fila.data.adeudoTotal)+'</span>'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:40,
                                                            html:'<b>Fecha de abono:</b>'
                                                        },
                                                        {
                                                        	xtype:'datefield',
                                                            x:120,
                                                            y:35,
                                                            value:'<?php echo date("Y-m-d")?>',
                                                            id:'dteFechaAbono'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:250,
                                                            y:40,
                                                            html:'<b>Forma de pago:</b>'
                                                        },
                                                        cmbFormaPagoAbono,
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:70,
                                                            html:'<b>Monto abono:</b>'
                                                        },
                                                        {
                                                        	xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'montoAbono',
                                                            width:100,
                                                            x:120,
                                                            y:65
                                                            
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:100,
                                                            html:'<b>Comentarios adicinales:</b>'
                                                        },
                                                        {
                                                        	xtype:'textarea',
                                                            id:'txtComentarios',
                                                            width:500,
                                                            height:80,
                                                            id:'txtComentarios',
                                                            x:10,
                                                            y:130
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar Abono',
										width: 550,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('montoAbono').focus();
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var dteFechaAbono=gEx('dteFechaAbono');
                                                                        if(dteFechaAbono.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaAbono.focus();
                                                                            }
                                                                            msgBox('De indicar la fecha en que se realiz&oacute; el abono',resp);
                                                                            return;
                                                                        }
                                                                        var montoAbono=gEx('montoAbono');
                                                                        if(montoAbono.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	montoAbono.focus();
                                                                            }
                                                                            msgBox('El monto de abono ingresado no es v&aacute;lido',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        var mAbono=parseFloat(montoAbono.getValue());
                                                                        var tDeuda=parseFloat(normalizarValor(gE('lblMontoAbono').innerHTML));
                                                                        if(tDeuda<mAbono)
                                                                        {
                                                                        	msgBox('El monto del abono ingresado (<b>'+Ext.util.Format.usMoney(mAbono)+'<b>) excede el monto del adeudo (<b>'+Ext.util.Format.usMoney(tDeuda)+'<b>)');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var cadObj='{"folioPedido":"'+fila.data.folioPedido+'","idAdeudo":"'+fila.data.idAdeudo+'","cantidadAbono":"'+mAbono+'","fechaAbono":"'+dteFechaAbono.getValue().format('Y-m-d')+
                                                                        			'","comentarios":"'+cv(gEx('txtComentarios').getValue())+'","formaPago":"'+gEx('cmbFormaPagoAbono').getValue()+'"}';
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridAbonosHistorial').getStore().reload();
                                                                            	gEx('gridAdeudos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=39&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function buscarPedido()
{
	var idPedido=gEx('txtFolio').getValue();
	if(idPedido=='')
	{
		function resp()
		{
			gEx('txtFolio').focus();
		}
		msgBox('Debe indicar el folio de pedido',resp);
		return;
	}
	function funcAjax()
	{
		var resp=peticion_http.responseText;
		arrResp=resp.split('|');
		if(arrResp[0]=='1')
		{
        	if(arrResp[1]!='0')
            {
                var o=eval('['+arrResp[1]+']')[0];
                gEx('txtProveedor').setValue(o.proveedor);
                gEx('gProductosPedidos').getStore().loadData(o.arrProductos);
                gE('lblMontoTotal').innerHTML=Ext.util.Format.usMoney(o.totalPedido);
                gE('lblDiferencia').innerHTML=Ext.util.Format.usMoney(o.totalPedido);
                gEx('btnAceptarDev').enable();
			}
            else
            {
            	msgBox('El folio del  pedido ingresado no existe');
            	gEx('btnAceptarDev').disable();
            }
        }
		else
		{
			msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
		}
	}
	obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=181&idPedido='+idPedido,true);

}

function buscarPedidoProveedor()
{
	var idProveedor=-1;
    
    var oConf=	{
    					idCombo:'cmbProveedor',
                        anchoCombo:450,
                        campoDesplegar:'nombreProveedor',
                        campoID:'idProveedor',
                        funcionBusqueda:2,
                        raiz:'registros',
                        nRegistros:'num',
                        posX:140,
                        posY:5,
                        paginaProcesamiento:'../paginasFunciones/funcionesModulosEspeciales_Sigloxxi.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">{nombreProveedor}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProveedor'},
                                    {name:'nombreProveedor'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProveedor=-1;
                                        dSet.baseParams.funcion=3;
                                       
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProveedor=registro.get('idProveedor');
                                        gEx('gPedidosProveedor').getStore().load	(
                                        												{
                                                                                            url:'../paginasFunciones/funcionesAlmacen.php',
                                                                                            params:	{
                                                                                                        funcion:182,
                                                                                                        iProveedor:idProveedor
                                                                                                    }
                                                                                    	}
                                        											)

                                    }  
    				};

	var cmbProveedor=crearComboExtAutocompletar(oConf);

	var gridPedidosProveedor=crearGridPedidosProveedor();

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														 {
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre del Proveedor:'
                                                        },
                                                        cmbProveedor,
                                                        gridPedidosProveedor

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar pedidos de proveedor',
										width: 800,
                                        id:'vBPProveedor',
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbProveedor.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		proveedorSel();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridPedidosProveedor()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPedido'},
		                                                {name:'folioPedido'},
                                                        {name:'fechaPedido', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaRealEntrega', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'total'},
                                                        {name:'detallePedido'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaPedido', direction: 'DESC'},
                                                            groupField: 'fechaPedido',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='182';
                                        
                                    }
                        )   
       
	var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                expandOnEnter:false,
                                                tpl : new Ext.Template(
                                                    '<table >',
                                                    '<tr><td style="padding:5px; color:#666; font-style:italic">{detallePedido}</td></tr>',
                                                    '</table>'
                                                )
                                            });                                                    

	var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            
                                                                            {type: 'date', dataIndex: 'fechaPedido'},
                                                                            {type: 'int', dataIndex: 'fechaRealEntrega'}
                                                                            
                                                                        ]
                                                        }
                                                    );                                                              
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'Folio del pedido',
                                                                width:130,
                                                                sortable:true,
                                                                dataIndex:'folioPedido'
                                                            },
                                                           	{
                                                                header:'Fecha del pedido',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'fechaPedido',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                           	{
                                                                header:'Fecha de recepci&oacute;n',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'fechaRealEntrega',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                           	{
                                                                header:'Monto total del pedido',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'total',
                                                                css:'text-align:right;',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'',
                                                                width:70,
                                                                sortable:true
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gPedidosProveedor',
                                                                store:alDatos,
																x:10,
                                                                y:40,
                                                                height:300,
                                                                frame:false,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:	[
                                                                				expander,filters
                                                                			],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
		tblGrid.on('celldblclick',function(grid,nFila)
        							{
										//var fila=grid.getStore().getAt(nFila);
                                        proveedorSel();
                                        
                                    }
                   )                                                        
        return 	tblGrid;
}

function proveedorSel()
{
	var gPedidosProveedor=gEx('gPedidosProveedor')	;
    var fila=gPedidosProveedor.getSelectionModel().getSelected();
    if(!fila)
    {
    	msgBox('Debe seleccionar el pedido cuya devoluci&oacute;n desea registrar');
    	return;
    }
 	gEx('txtFolio').setValue(fila.data.idPedido);   
 	gEx('vBPProveedor').close();    
 	buscarPedido();   
  
}