<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	crearGridCategorias() 
}


function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                    
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	gEx('arbolCategorias').getStore().expandAll();
                        
                        
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    renderTo:'tblTabla',
                                                    width:960,
                                                    height:400,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    columnLines :true,
													loadMask :true,
                                                    tbar:	[
                                                                {
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Agregar categoría',
                                                                    id:'btnAgregarCat',
                                                                    handler:function()
                                                                            {
                                                                            	var arrParam=[['idCategoriaProducto','-1'],['idAlmacen',gE('idAlmacen').value],['nivel',1],['llave','']];
                                                                                enviarFormularioDatos('../modeloAlmacenes/categoriaProducto.php',arrParam);
                                                                                
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    disabled:true,
                                                                    id:'btnAgregarSub',
                                                                    text:'Agregar subcategor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                            	var fila=grid.getSelectionModel().getSelected();
                                                                                if(!fila)
                                                                                {
                                                                                	msgBox('Debe seleccionar la categor&iacute;a al cual desea agregar la subcatego&iacute;a');
                                                                                	return;
                                                                                }
                                                                                var arrParam=[['idCategoriaProducto',-1],['idAlmacen',gE('idAlmacen').value],['nivel',parseInt(fila.data.nivel)+1],['idPadre',fila.data.idCategoria],['llave',fila.data.llave]];
                                                                                enviarFormularioDatos('../modeloAlmacenes/categoriaProducto.php',arrParam);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/pencil.png',
                                                                    cls:'x-btn-text-icon',
                                                                   
                                                                    text:'Modificar categoría/subcategor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                            	var fila=grid.getSelectionModel().getSelected();
                                                                            	if(!fila)
                                                                                {
                                                                                	msgBox('Debe seleccionar la categor&iacute;a/subcategor&iacute;a que desea modificar');
                                                                                	return;
                                                                                }
                                                                                var arrParam=[['idCategoriaProducto',fila.data.idCategoria],['idAlmacen',gE('idAlmacen').value],['nivel',parseInt(fila.data.nivel)],['idPadre',fila.data._parent],['llave',fila.data.llave]];
                                                                                enviarFormularioDatos('../modeloAlmacenes/categoriaProducto.php',arrParam);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Remover categoría/subcategor&iacute;a',
                                                                    handler:function()
                                                                            {
                                                                            	var fila=grid.getSelectionModel().getSelected();
                                                                            	if(!fila)
                                                                                {
                                                                                	msgBox('Debe seleccionar la categor&iacute;a/subcategor&iacute;a que desea remover');
                                                                                	return;
                                                                                }
                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                	if(btn=='yes')
                                                                                    {
                                                                                    	function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                grid.getStore().remove(fila);
                                                                                                gEx('btnAgregarSub').disable();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=159&idCategoria='+fila.data.idCategoria,true);
                                                                                        
                                                                                    }
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer remover la categor&iacute;a/subcategor&iacute;a seleccionada?',resp);
                                                                                
                                                                            }
                                                                    
                                                                }
                                                    		], 
                                                    
                                                    
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 350
                                                                        
                                                                    },
                                                                    {
                                                                        header: 'Descripci&oacute;n',
                                                                        dataIndex: 'descripcion',
                                                                        width: 450
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	grid.getSelectionModel().on('rowselect',function(sm,numFila,registro)
    										{
                                            	gEx('btnAgregarSub').enable();
                                            }	
    
    							)
    
     
     
     
                                          
	return grid;                                          
}