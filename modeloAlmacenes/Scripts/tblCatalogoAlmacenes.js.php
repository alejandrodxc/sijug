<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	global $referenciaFiltros;
?>

function agregarAlmacen()
{
	
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:20,
                                                            y:10,
                                                            html:'Cve. Almac&eacute;n:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:5,
                                                            xtype:'textfield',
                                                            width:180,
                                                            id:'txtCveAlmacen'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:40,
                                                            html:'Nombre del Almac&eacute;n:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:35,
                                                            xtype:'textfield',
                                                            width:300,
                                                            id:'txtNombreAlmacen'
                                                        },
                                                        {
                                                        	x:20,
                                                            y:70,
                                                            html:'Descripci&oacute;n:'
                                                        },
                                                        {
                                                        	x:145,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:350,
                                                            height:80,
                                                            id:'txtDescripcion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar almac&eacute;n',
										width: 580,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCveAlmacen').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCveAlmacen=gEx('txtCveAlmacen');
                                                                        if(txtCveAlmacen.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	txtCveAlmacen.focus();
                                                                            }
                                                                            msgBox('Debe especificar la clave del almac&eacute;n');
                                                                            return;
                                                                        }
                                                                        var txtNombreAlmacen=gEx('txtNombreAlmacen');
                                                                        if(txtNombreAlmacen.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	txtNombreAlmacen.focus();
                                                                            }
                                                                            msgBox('Debe especificar el nombre del almac&eacute;n');
                                                                            return;
                                                                        }
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        var cadObj='{"referenciaFiltros":"<?php echo $referenciaFiltros?>","idAlmacen":"-1","cveAlmacen":"'+txtCveAlmacen.getValue()+'","nombreAlmacen":"'+cv(txtNombreAlmacen.getValue())+'","descripcion":"'+cv(txtDescripcion.getValue())+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                var arrParam=[['idAlmacen',arrResp[1]]];
                                                                                enviarFormularioDatos('../modeloAlmacenes/admonAlmacen.php',arrParam);
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=144&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}