<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var nodoSel=null;
Ext.onReady(inicializar);

function inicializar()
{
	if(gE('id').value!='-1')
    {
        var cargadorArbol=new Ext.tree.TreeLoader(
                                                    {
                                                        autoLoad:false,
                                                        baseParams:{
                                                                        funcion:'200',
    
                                                                    },
                                                        dataUrl:'../paginasFunciones/funcionesAlmacen.php'
                                                    }
                                                )	
        
        cargadorArbol.on('beforeload',function(proxy)
                                    {
                                        proxy.baseParams.idUnidadMedida=gE('id').value;
                                    }
                        )
                        
        
        cargadorArbol.on('load',function(proxy)
                                    {
                                        nodoSel=null;
                                    }
                        )
        
        var raiz=new  Ext.tree.AsyncTreeNode	(
                                                      {
                                                          id:'nodo0',
                                                          text:'',
                                                          draggable:false,
                                                          tipo:0,
                                                          expanded :true,
                                                          icon:'../images/Icono_txt.gif'
                                                      }
                                                )
    
    
    
        panelArbol=new Ext.ux.tree.EditorGrid	(
                                                  {
                                                      id:'aEquivalencia',
                                                      renderTo:'tblEquivalencias',
                                                      width:600,
                                                      height:250,
                                                      border:true,
                                                      enableSort:false,
                                                      loadMask: {msg: 'Cargando...' },
                                                      enableHdMenu:false,
                                                      tbar:	[
                                                                {
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Agregar equivalencia',
                                                                    handler:function()
                                                                            {
                                                                                mostrarVentanaUnidadMedida()
                                                                            }
                                                                    
                                                                },
                                                                '-',
                                                                {
                                                                    icon:'../images/pencil.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Modificar equivalencia',
                                                                    handler:function()
                                                                            {
                                                                                mostrarVentanaUnidadMedida(nodoSel)
                                                                            }
                                                                    
                                                                },
                                                                '-',
                                                                {
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Remover equivalencia',
                                                                    handler:function()
                                                                            {
                                                                                if(nodoSel==null)
                                                                                {
                                                                                    msgBox('Debe seleccionar la unidad de medida cuya equivalencia desea remover');
                                                                                    return;
                                                                                }
                                                                                
                                                                                function resp(btn)
                                                                                {
                                                                                    if(btn=='yes')
                                                                                    {
                                                                                        var cadObj='{"idUnidadMedida":"'+gE('id').value+'","idUnidadEquivalencia":"'+nodoSel.id+'"}'
                                                                                        function funcAjax()
                                                                                        {
                                                                                            var resp=peticion_http.responseText;
                                                                                            arrResp=resp.split('|');
                                                                                            if(arrResp[0]=='1')
                                                                                            {
                                                                                                gEx('aEquivalencia').getRootNode().reload();
                                                                                                
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                            }
                                                                                        }
                                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=203&cadObj='+cadObj,true);
                                                                                    }
                                                                                }
                                                                                msgConfirm('Est&aacute; seguro de querer remover la unidad de medida seleccionada?',resp)
                                                                            }
                                                                    
                                                                }
                                                                
                                                            ],
                                                      columns:	[
                                                                     {
                                                                        header:'Unidad de medida',
                                                                        width:350,
                                                                        dataIndex:'text'
                                                                    },
                                                                    {
                                                                        header:'Cantidad',
                                                                        align:'right',
                                                                        width:150,
                                                                        dataIndex:'cantidad'
                                                                    }
                                                                  ],
                                                      loader: cargadorArbol
    
                                                  }
                                              );  	
                                              
        panelArbol.on('click',nodoClick);
        panelArbol.expandAll();                                              
     }                                     
}

function nodoClick(nodo)
{
	nodoSel=nodo;
	
}

function mostrarVentanaUnidadMedida(nSel)
{
	var cmbUnidadMedida=crearComboExt('cmbUnidadMedida',[],135,5,350);
    cmbUnidadMedida.on('select',function(cmb,registro)
    								{
                                    	gEx('txtCantidad').focus(false,10);
                                    }
    					)
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Unidad de medida:'
                                                        },
                                                        cmbUnidadMedida,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Cantidad equivalente:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            width:70,
                                                            xtype:'numberfield',
                                                            decimalPrecision : 5,
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'txtCantidad'
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: !nSel?'Agregar equivalencia':'Modificar equivalencia',
										width: 530,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbUnidadMedida').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbUnidadMedida.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	cmbUnidadMedida.focus();
                                                                            }
                                                                        	msgBox('Debe indicar la unidad de medida cuya equivalencia desea registrar',resp1);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        if(gEx('txtCantidad').getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	gEx('txtCantidad').focus();
                                                                            }
                                                                        	msgBox('Debe indicar la cantidad equivalente de la unidad de medida a registrar',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                        var cadObj='{"idUnidadMedida":"'+gE('id').value+'","idUnidadEquivalencia":"'+cmbUnidadMedida.getValue()+'","cantidad":"'+gEx('txtCantidad').getValue()+'"}'
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('aEquivalencia').getRootNode().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=202&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);

	obtenerUnidadesMedidaDisponibles(ventanaAM,nSel);

}


function obtenerUnidadesMedidaDisponibles(ventana,nSel)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var arrDatos=eval(arrResp[1]);
            gEx('cmbUnidadMedida').getStore().loadData(arrDatos);
            ventana.show();
            
            if(nSel)
            {
            	arrDatos=eval(arrResp[2]);
            	gEx('cmbUnidadMedida').getStore().loadData(arrDatos);
            	gEx('cmbUnidadMedida').setValue(nSel.id);
                gEx('txtCantidad').setValue(normalizarValor(nSel.attributes.cantidad));
            }
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=201&iU='+gE('id').value,true);
    
}