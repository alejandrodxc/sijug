<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
?>

var arrAliasCarpetas=	[
							['','Buz&oacute;n de Entrada'],
                            ['Junk','Basura'],
                            ['Drafts','Borradores'],
                            ['Sent','Enviados'],
                            ['Trash','Papelera']
						]


var arrParametros=[['@apPaterno','Ap. Paterno'],['@apMaterno','Ap. Materno'],['@nombreContacto','Nombre'],['@nombreCompleto','Nombre Completo'],['@empresaEmisora','Empresa emisora']];

var arrTipoTelefono=[['0','Fijo'],['1','Celular']];

Ext.onReady(inicializar);

function inicializar()
{

	marcarTipoEmpresa();
    gE('_rfc1vch').focus();
    crearGridContactos();
    crearGridDestinatarios();
    crearGridRegistroPatronal();
    crearGridRegimen();
    if((gE('esEmpresaUsuario').value=='1')&&(gE('tDirectorios')))
	    crearGridCarpetas()
    if(gE('tPanelComprobanteFiscal'))
    {
        var tPanelComprobante=new Ext.form.HtmlEditor	(
                                                            {
                                                                renderTo:'tPanelComprobanteFiscal',
                                                                width:780,
                                                                id:'cuerpoMailComprobante',
                                                                height:250,
                                                                value:bD(gE('_cuerpoMailComprobantevch').value)
                                                                
                                                            }
                                                        )
        
    
        tPanelComprobante.getToolbar( ).add	(
                                                '-',
                                                {
                                                    icon:'../images/tag_blue_edit.png',
                                                    cls:'x-btn-text-icon',
                                                    handler:function()
                                                            {
                                                                mostrarAgregarParametro(tPanelComprobante);
                                                            }
                                                }
                                            )
    
        
         var tPanelNomina=new Ext.form.HtmlEditor		(
                                                                {
                                                                    renderTo:'tPanelNomina',
                                                                    width:780,
                                                                    height:250,
                                                                    id:'cuerpoMailNomina',
                                                                    value:bD(gE('_cuerpoMailNominavch').value)
                                                                    
                                                                }
                                                            )
    
    
        tPanelNomina.getToolbar( ).add	(
                                                '-',
                                                {
                                                    icon:'../images/tag_blue_edit.png',
                                                    cls:'x-btn-text-icon',
                                                    handler:function()
                                                            {
                                                                mostrarAgregarParametro(tPanelNomina);
                                                            }
                                                }
                                            )
	}
    var tabs = $( "#tabs" ).tabs(	{
    									heightStyle:'auto',
                                        activate: function( event, ui ) 
                                        		{

                                                	if(ui.newTab.context.innerHTML=='Facturación Electrónica')
                                                    {
                                                    	if(gE('tblRegPatronal'))
                                                        {
                                                            gEx('gRegistroPatronal').getView().refresh();
                                                            gEx('gRegimen').getView().refresh();
                                                        }
                                                        gEx('gDestinatarios').getView().refresh();
                                                    }
                                                    if(ui.newTab.context.innerHTML=='Contactos')
                                                    {
                                                    	gEx('gContacto').getView().refresh();
                                                       
                                                    }
                                                    
                                                }	
                                        
                                     }
                                );
	if(gE('chkHabilitarRecepcionComprobantesBuzon'))                                
		habilitadoImportacionClick(gE('chkHabilitarRecepcionComprobantesBuzon'));                                
}

function mostrarAgregarParametro(editor)
{
	var cmbParametro=crearComboExt('cmbParametro',arrParametros,250,5,200);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'Especifique el par&aacute;metro que desea agregar:'
                                                        },
                                                        cmbParametro

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar parametro',
										width: 550,
										height:120,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbParametro.getValue()=='')
                                                                        {
                                                                        	msgBox('Debe especificar el par&aacute;metro que desea agregar');
                                                                        	return ;
                                                                        }
                                                                        
                                                                        editor.insertAtCursor(cmbParametro.getValue());
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridRegistroPatronal()
{
	if(!gE('tblRegPatronal'))
    	return;
	var dsDatos=eval(bD(gE('aRegistroPatronal').value));
    var x;
    
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                        			{name: 'idRegistroPatronal'},
                                                                    {name: 'registroPatronal'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Registro patronal',
															width:250,
															sortable:true,
															dataIndex:'registroPatronal',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            id:'gRegistroPatronal',
                                                            border:true,
                                                            renderTo:'tblRegPatronal',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:100,
                                                            width:350,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar Registro Patronal',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro([{name:'regimenFiscal'}]);
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	idRegistroPatronal:'-1',
                                                                                                                registroPatronal:''
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Registro Patronal',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el r&eacute;gimen que desea remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el Registro Patronal seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function crearGridRegimen()
{
	if(!gE('tblRegimen'))
    	return;
	var dsDatos=[];
    var x;
    var _regimenFiscalvch=gE('_regimenFiscalvch');
    var arrRegimen=_regimenFiscalvch.value.split('|');
	if(_regimenFiscalvch.value!='')
    {
        for(x=0;x<arrRegimen.length;x++)
        {
            dsDatos.push([arrRegimen[x]]);
        }
	}    
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'regimenFiscal'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'R&eacute;gimen fiscal',
															width:250,
															sortable:true,
															dataIndex:'regimenFiscal',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            id:'gRegimen',
                                                            border:true,
                                                            renderTo:'tblRegimen',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:100,
                                                            width:350,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar r&eacute;gimen',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro([{name:'regimenFiscal'}]);
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	regimenFiscal:''
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover r&eacute;gimen',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el r&eacute;gimen que desea remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el r&eacute;gimen seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function validarFormulario()
{
	var rfc1=gE('_rfc1vch');
    var rfc2=gE('_rfc2vch');
    var rfc3=gE('_rfc3vch');
    
    var _regimenFiscalvch=gE('_regimenFiscalvch');
        
    var gRegimen=gEx('gRegimen');
    var listaRegimen='';
    
    if(gE('esEmpresaUsuario').value=='1')
    {
    
        for(x=0;x<gRegimen.getStore().getCount();x++)
        {
            fila=gRegimen.getStore().getAt(x);
            
            if(fila.data.regimenFiscal.trim()=='')
            {
                function resp10()
                {
                	$( "#tabs" ).tabs( "option", "active", 2 );   
                    gRegimen.startEditing(x,2);
                }
                msgBox('El r&eacute;gimen fiscal ingresado no es v&aacute;lido',resp10);
                return;
            }
            if(listaRegimen=='')
                listaRegimen=fila.data.regimenFiscal;
            else
                listaRegimen+='|'+fila.data.regimenFiscal;
            
        }

        _regimenFiscalvch.value=listaRegimen;
        
        
        if(gE('_codPostalvch').value.length!=5)
        {
        	function resp1000()
            {
                $( "#tabs" ).tabs( "option", "active", 0 );   
                gE('_codPostalvch').focus();
            }
            msgBox('La longitud del C.P. es de 5 caracteres',resp1000);
            return;
        }
        
        
        
        
        
    }
    
    if(gE('_mailRemitenteComprobantevch'))
    {
        var _mailRemitenteComprobantevch=gE('_mailRemitenteComprobantevch');
        if(_mailRemitenteComprobantevch.value!='')
        {
            if(!validarCorreo(_mailRemitenteComprobantevch.value))
            {
                function respMail()
                {
                    $( "#tabs" ).tabs( "option", "active", 3 );   
                    _mailRemitenteComprobantevch.focus();
                }
                msgBox('El E-mail ingresado no es v&aacute;lido',respMail);
                return;
            }
            
        }
        
        var _mailRemitenteNominavch=gE('_mailRemitenteNominavch');
        if(_mailRemitenteNominavch.value!='')
        {
            if(!validarCorreo(_mailRemitenteNominavch.value))
            {
                function respMail2()
                {
                    $( "#tabs" ).tabs( "option", "active", 4 );   
                    _mailRemitenteNominavch.focus();
                }
                msgBox('El E-mail ingresado no es v&aacute;lido',respMail2);
                return;
            }
            
        }
    
    }
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            if(arrResp[1]!='0')
            {
            	function respErr()
                {
                	gE('_rfc1vch').focus();
                }
            	msgBox('El RFC ingresado ya ha sido registrado anteriormente',respErr);
            	return;
            }
            
            if(validarFormularios('frmEnvio'))
            {
            
                var esCliente=false;
                if(gE('esCliente').checked)
                    esCliente=true;
                var esProveedor=false;
                if(gE('esProveedor').checked)
                    esProveedor=true;
            
                if((!esCliente)&&(!esProveedor)&&(gE('esEmpresaUsuario').value=='0'))
                {
                    msgBox('Debe clasificar a la empresa como: Cliente, Proveedor o ambos');
                    return;
                }
            
                var tipoEmpresa=gE('_tipoEmpresaint').value;
                if(tipoEmpresa=='1')//Persona fisica
                {
                    if((rfc1.value.length!=0)&&(rfc1.value.length!=4))
                    {
                        function resp1()
                        {
                            rfc1.focus();
                        }
                        msgBox('La logitud del RFC debe ser de 4 caracteres para personas f&iacute;sicas',resp1);
                        return;
                    }
                    
        
                }
                else
                {
                    if((rfc1.value.length!=0)&&(rfc1.value.length!=3))
                    {
                        function resp2()
                        {
                            rfc1.focus();
                        }
                        msgBox('La logitud del RFC debe ser de 3 caracteres para personas morales',resp2);
                        return;
                    }
                }
                
                if((rfc2.value.length!=0)&&(rfc2.value.length!=6))
                {
                    function resp3()
                    {
                        rfc2.focus();
                    }
                    msgBox('La logitud del RFC debe ser de 6 caracteres',resp3);
                    return;
                }
        
                if((rfc3.value.length!=0)&&(rfc3.value.length!=3))
                {
                    function resp4()
                    {
                        rfc3.focus();
                    }
                    msgBox('La logitud del RFC debe ser de 3 caracteres',resp4);
                    return;
                    
                }
                
                var arrContactos='';
                var o;
                var gContacto=gEx('gContacto');
                var x;
                var fila;
                for(x=0;x<gContacto.getStore().getCount();x++)
                {
                    fila=gContacto.getStore().getAt(x);
                    
                    o='{"objContacto":"'+fila.data.objContacto+'"}';
                    if(arrContactos=='')
                        arrContactos=o;
                    else
                        arrContactos+=','+o;
                    
                }
                
                
                
                var id=gEN('id')[0].value;
                var arrCategoria='';
                if(esCliente)
                {
                    arrCategoria='{"idCategoria":"1"}';
                }
                
                if(esProveedor)
                {
                    if(arrCategoria=='')
                        arrCategoria='{"idCategoria":"2"}';
                    else
                        arrCategoria+=',{"idCategoria":"2"}';
                }
                
                
                var arrDestinatarios='';
                var gDestinatarios=gEx('gDestinatarios');
                for(x=0;x<gDestinatarios.getStore().getCount();x++)
                {
                    fila=gDestinatarios.getStore().getAt(x);
                    
                    o='{"idContacto":"'+fila.data.idContacto+'"}';
                    if(arrDestinatarios=='')
                        arrDestinatarios=o;
                    else
                        arrDestinatarios+=','+o;
                    
                }
                var habilitarBuzonRecepcion=0;
                var configuracionBuzonRecepcion=0;
                
                var arrRegistroPatronal='';
                if(gE('esEmpresaUsuario').value=='1')
                {
                    var gRegistroPatronal=gEx('gRegistroPatronal');
                    for(x=0;x<gRegistroPatronal.getStore().getCount();x++)
                    {
                        fila=gRegistroPatronal.getStore().getAt(x);
                        
                        o='{"idRegistroPatronal":"'+fila.data.idRegistroPatronal+'","registroPatronal":"'+fila.data.registroPatronal+'"}';
                        if(arrRegistroPatronal=='')
                            arrRegistroPatronal=o;
                        else
                            arrRegistroPatronal+=','+o;
                        
                    }
                    
                    if(gE('chkHabilitarRecepcionComprobantesBuzon'))     
                    {
                        if(gE('chkHabilitarRecepcionComprobantesBuzon').checked)
                        {
                            habilitarBuzonRecepcion=1;
                            configuracionBuzonRecepcion=obtenerDatosConexionBuzon();
                        }
					}                    
                }  
                      
                var objArr='{"habilitarBuzonRecepcion":"'+habilitarBuzonRecepcion+'","configuracionBuzonRecepcion":'+configuracionBuzonRecepcion+',"arrRegistroPatronal":['+arrRegistroPatronal+'],"arrCategoria":['+arrCategoria+'],"arrContactos":['+arrContactos+'],"arrDestinatarios":['+arrDestinatarios+']}';
                
                if(id=='-1')
                {
                    gE('funcPHPEjecutarNuevo').value=bE('asociarContactoEmpresa(@idRegPadre,\''+bE(objArr)+'\')');
                }
                else
                {
                    gE('funcPHPEjecutarModif').value=bE('asociarContactoEmpresa('+id+',\''+bE(objArr)+'\')');
                }
                
                if(gE('_cuerpoMailComprobantevch'))
                {
                    gE('_cuerpoMailComprobantevch').value=gEx('cuerpoMailComprobante').getValue();
                    gE('_cuerpoMailNominavch').value=gEx('cuerpoMailNomina').getValue();
                }        
                
                gE('frmEnvio').submit();
            }
            
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=52&idEmpresa='+gE('idEmpresa').value+'&rfc1='+gE('_rfc1vch').value+'&rfc2='+gE('_rfc2vch').value+'&rfc3='+gE('_rfc3vch').value+'&referencia='+gE('_referenciavch').value,true);
}

function tipoEmpresaCheck(ctrl)
{
	if(ctrl.value=='1')
    {
    	mE('fPaterno');
        mE('fMaterno');
        gE('lblRazonSocial').innerHTML='Nombre:';
        
    }
    else
    {
    	oE('fPaterno');
        oE('fMaterno');       	
        gE('lblRazonSocial').innerHTML='Razón Social:';
        
    }
    gE('_tipoEmpresaint').value=ctrl.value;
    gE('_rfc1vch').focus();
}

function marcarTipoEmpresa()
{
	var tipoEmpresa=gE('_tipoEmpresaint').value;
    if(tipoEmpresa=='1')
    {
    	gE('tipoEmpresa1').checked=true;
        tipoEmpresaCheck(gE('tipoEmpresa1'));
    }
    else
    {
    	gE('tipoEmpresa2').checked=true;
        tipoEmpresaCheck(gE('tipoEmpresa2'));
    }
}

function estadoSel(cmb)
{
	var valor=cmb.options[cmb.selectedIndex].value;
	obtenerMunicipio(valor,'_municipiovch','1')
}

function crearGridContactos()
{

	
	var dsDatos=eval(bD(gE('arrContactos').value));
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name:	'idContacto'},
                                                                    {name: 'nombreContacto'},
                                                                    {name: 'puesto'},
                                                                    {name: 'departamento'},
                                                                    {name: 'objContacto'},
                                                                    {name: 'detalle'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var expander = new Ext.ux.grid.RowExpander({
                                                column:1,
                                                tpl : new Ext.Template(
                                                    '<table ><tr><td colspan="2" height="10">{detalle}</td></tr>'+
                                                    '</table>'
                                                )
                                            });  
                                            
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
                                                        expander,
														{
															header:'Nombre del contacto',
															width:220,
															sortable:true,
															dataIndex:'nombreContacto'
														},
														{
															header:'Puesto',
															width:130,
															sortable:true,
															dataIndex:'puesto'
														},
                                                        {
															header:'Departamento',
															width:250,
															sortable:true,
															dataIndex:'departamento'
														}


													]
												);
                            
                            
	                           
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            renderTo:'contactos',
                                                            id:'gContacto',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:170,
                                                            width:780,
                                                            sm:chkRow,
                                                            plugins:[expander],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar contacto',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaContacto();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/user_edit.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar contacto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el contacto que desea modificar');
                                                                                        	return 
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                        mostrarVentanaContacto(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/user_remove.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover contacto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el contacto que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function respC(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	var gDestinatarios=gEx('gDestinatarios');
                                                                                                var pos=obtenerPosFila(gDestinatarios.getStore(),'idContacto',fila.data.idContacto);
                                                                                                if(pos!=-1)
                                                                                                {
                                                                                                	var filaD=gDestinatarios.getStore().getAt(pos);
                                                                                                	gDestinatarios.getStore().remove(filaD);    
                                                                                                }
                                                                                            
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                                
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el contacto seleccionado?',respC);	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function mostrarVentanaContacto(filaReg)
{
	var etiqueta='';
	if(filaReg)
    	etiqueta='Modificar contacto';
    else
    	etiqueta='Agregar contacto';
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'<span class="">Ap. Paterno: <span style="color:#F00">*<span></span>'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:5,
                                                            id:'apPaterno',
                                                            xtype:'textfield',
                                                            width:140
                                                        },
                                                        {
                                                        	x:290,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'<span class="">Ap. Materno:</span>'
                                                        },
                                                        {
                                                        	x:370,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'apMaterno',
                                                            width:140
                                                        },
                                                        {
                                                        	x:300,
                                                            y:35,
                                                            width:220,
                                                            height:25,
                                                            baseCls: 'x-plain',
                                                            border:false,
                                                            xtype:'panel',
                                                            items:	[
                                                            			{
                                                                        	icon:'../images/vcard.png',
                                                                            cls:'x-btn-text-icon',
                                                                            width:110,
                                                                            height:10,
                                                                            hidden:(gE('_tipoEmpresaint').value!='1'),
                                                                            xtype:'button',
                                                                            text:'Importar datos de persona f&iacute;sica',
                                                                            handler:function()
                                                                            		{
                                                                                    	var apPaterno=gEx('apPaterno');
                                                                                        apPaterno.setValue(gE('_apPaternovch').value);
                                                                                        var apMaterno=gEx('apMaterno');
                                                                                        apMaterno.setValue(gE('_apMaternovch').value);
                                                                                        var nombre=gEx('nombre');
                                                                                        nombre.setValue(gE('_razonSocialvch').value);
                                                                                        var puesto=gEx('puesto');
                                                                                        puesto.focus();
                                                                                    }
                                                                            
                                                                        }
                                                            		]
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'label',
                                                            html:'<span class="">Nombre: <span style="color:#F00">*<span></span>'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'nombre',
                                                            width:180
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            xtype:'label',
                                                            html:'<span class="">Puesto:</span>'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:65,
                                                            xtype:'textfield',
                                                            id:'puesto',
                                                            width:320
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'label',
                                                            html:'<span class="">Departamento:</span>'
                                                        },
                                                        {
                                                        	x:100,
                                                            y:95,
                                                            xtype:'textfield',
                                                            id:'departamento',
                                                            width:400
                                                        },
                                                        {
                                                        	x:10,
                                                            y:120,
                                                            xtype:'fieldset',
                                                            title:'E-mail',
                                                            width:300,
                                                            height:180,
                                                            items:	[
                                                            			crearGridEmail()
                                                            		]
                                                        },
                                                        {
                                                        	x:320,
                                                            y:120,
                                                            xtype:'fieldset',
                                                            title:'Tel&eacute;fono',
                                                            width:340,
                                                            height:180,
                                                            items:	[
                                                            			crearGridTelefono()
                                                            		]
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: etiqueta,
										width: 700,
										height:380,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('apPaterno').focus(500,false);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var apPaterno=gEx('apPaterno');
                                                                        
                                                                        if(apPaterno.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	apPaterno.focus();
                                                                            }
                                                                            msgBox('Debe indicar el apellido paterno del contacto',resp1);
                                                                        }
                                                                        
                                                                        var apMaterno=gEx('apMaterno');
                                                                        var nombre=gEx('nombre');
                                                                        
                                                                        
                                                                        if(nombre.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	nombre.focus();
                                                                            }
                                                                            msgBox('Debe indicar el nombre del contacto',resp2);
                                                                        }
                                                                        
                                                                        var gMail=gEx('gMail');
                                                                        var gTelefono=gEx('gTelefono');
                                                                        var fila;
                                                                        var x;
                                                                        var arrMail='';
                                                                        var o='';
                                                                        var tblDetalle='';
                                                                        
                                                                        
                                                                        var tblEmail='<table><tr><td width="250"><b>E-mail</b></td></tr>';
                                                                        
                                                                        for(x=0;x<gMail.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gMail.getStore().getAt(x);
                                                                            if(!validarCorreo(fila.data.email))
                                                                            {
                                                                            	function respMail()
                                                                                {
                                                                                	gMail.startEditing(x,1);
                                                                                }
                                                                                msgBox('La direcci&oacute;n de E-mail no es v&aacute;lida',respMail);
                                                                                return;
                                                                            }
                                                                            
                                                                            tblEmail+='<tr height="21"><td>'+fila.data.email+'</td></tr>';
                                                                            
                                                                            o='{"email":"'+fila.data.email+'"}';
                                                                            if(arrMail=='')
                                                                            	arrMail=o;
                                                                            else
                                                                            	arrMail+=','+o;
                                                                        }
                                                                        tblEmail+='</table>';
                                                                        
                                                                        var tblTel='<table><tr><td width="80"><b>Tipo</b></td><td width="80"><b>Lada</b></td><td width="80"><b>Tel&eacute;fono</b></td><td width="80"><b>Extensi&oacute;n</b></td></tr>';
                                                                        var arrTel='';
                                                                        for(x=0;x<gTelefono.getStore().getCount();x++)
                                                                        {
                                                                        	fila=gTelefono.getStore().getAt(x);
                                                                            if(fila.data.telefono=='')
                                                                            {
                                                                                function respTel()
                                                                                {
                                                                                    gTelefono.startEditing(x,3);
                                                                                }
                                                                                msgBox('Debe ingresar el n&uacute;mero telefonico',respTel);
                                                                                return;
																			}                                                                            
                                                                            o='{"tipo":"'+fila.data.tipo+'","lada":"'+fila.data.lada+'","telefono":"'+fila.data.telefono+'","extension":"'+fila.data.extension+'"}';
                                                                            if(arrTel=='')
                                                                            	arrTel=o;
                                                                            else
                                                                            	arrTel+=','+o;
                                                                            tblTel+='<tr height="21"><td>'+formatearValorRenderer(arrTipoTelefono,fila.data.tipo)+'</td><td>'+fila.data.lada+'</td><td>'+fila.data.telefono+'</td><td>'+fila.data.extension+'</td></tr>';
                                                                        }
                                                                        tblTel+='</table>';
                                                                        var puesto=gEx('puesto');
                                                                        var departamento=gEx('departamento');
																		var iContacto=generarNumeroAleatorio(0,10000)+'_'+generarNumeroAleatorio(0,10000);
                                                                        
                                                                                    
                                                                        tblDetalle='<table><tr><td width:"145"><span style="color:#900"><b>E-mail de contacto:</b><span></td><td>'+tblEmail+'</td></tr><tr><td ><span style="color:#900"><b>Tel&eecute;fono de contacto:</b><span></td><td>'+tblTel+'</td></tr></table>';
                                                                        
                                                                        
                                                                        if(!filaReg)
                                                                        {
                                                                        
                                                                        	var cadObj=	'{"idContacto":"'+iContacto+'","nuevo":"1","apPaterno":"'+cv(apPaterno.getValue())+'","apMaterno":"'+cv(apMaterno.getValue())+'","nombre":"'+cv(nombre.getValue())+
                                                                        			'","puesto":"'+cv(puesto.getValue())+'","departamento":"'+cv(departamento.getValue())+'","arrMail":['+arrMail+'],"telefono":['+arrTel+']}';
                                                                        
                                                                        
                                                                            var rContacto= crearRegistro	(
                                                                                                                [
                                                                                                                    {name: 'idContacto'},
                                                                                                                    {name: 'nombreContacto'},
                                                                                                                    {name: 'puesto'},
                                                                                                                    {name: 'departamento'},
                                                                                                                    {name: 'objContacto'},
                                                                                                                    {name: 'detalle'}
                                                                                                                ]
                                                                                                            )
                                                                            
                                                                            var r=new rContacto	(
                                                                                                    {
                                                                                                        idContacto:iContacto,
                                                                                                        nombreContacto:nombre.getValue()+' '+apPaterno.getValue()+' '+apMaterno.getValue(),
                                                                                                        puesto:puesto.getValue(),
                                                                                                        departamento:departamento.getValue(),
                                                                                                        objContacto:bE(cadObj),
                                                                                                        detalle:tblDetalle
                                                                                                    }
                                                                                                )
                                                                                                
                                                                                                
                                                                                                
                                                                            var gContacto=gEx('gContacto');
                                                                            gContacto.getStore().add(r);    
                                                                        }	
                                                                        else
                                                                        {
                                                                        
                                                                        	var cadObj=	'{"idContacto":"'+filaReg.data.idContacto+'","nuevo":"0","apPaterno":"'+cv(apPaterno.getValue())+'","apMaterno":"'+cv(apMaterno.getValue())+'","nombre":"'+cv(nombre.getValue())+
                                                                        			'","puesto":"'+cv(puesto.getValue())+'","departamento":"'+cv(departamento.getValue())+'","arrMail":['+arrMail+'],"telefono":['+arrTel+']}';
                                                                        
                                                                        
                                                                        	filaReg.set('nombreContacto',nombre.getValue()+' '+apPaterno.getValue()+' '+apMaterno.getValue());
                                                                            filaReg.set('puesto',puesto.getValue());
                                                                            filaReg.set('departamento',departamento.getValue());
                                                                            filaReg.set('objContacto',bE(cadObj));
                                                                            filaReg.set('detalle',tblDetalle);
                                                                            
                                                                        }
                                                                        gEx('gDestinatarios').getView().refresh();
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
    if(filaReg)	
    {
    	
    	var oContacto=eval('['+bD(filaReg.data.objContacto)+']')[0];
    	

        var apPaterno=gEx('apPaterno');
        if(oContacto.nuevo)
        	apPaterno.setValue(decodeURIComponent(oContacto.apPaterno));
        else
            apPaterno.setValue(oContacto.apPaterno);                    
        var apMaterno=gEx('apMaterno');
        if(oContacto.nuevo)
        	apMaterno.setValue(decodeURIComponent(oContacto.apMaterno));
        else
        	apMaterno.setValue(oContacto.apMaterno);
        var nombre=gEx('nombre');
        if(oContacto.nuevo)
	        nombre.setValue(decodeURIComponent(oContacto.nombre));
        else
        	nombre.setValue(oContacto.nombre);
        
        
        var puesto=gEx('puesto');
        if(oContacto.nuevo)
	        puesto.setValue(decodeURIComponent(oContacto.puesto));
        else
       	    puesto.setValue(oContacto.puesto);
		var departamento=gEx('departamento');
        if(oContacto.nuevo)
	        departamento.setValue(decodeURIComponent(oContacto.departamento));
        else
        	departamento.setValue(oContacto.departamento);
        var gMail=gEx('gMail');
        var x;
        var aMail=[];
        for(x=0;x<oContacto.arrMail.length;x++)
        {
        	aMail.push([oContacto.arrMail[x].email]);
        }
        
        gMail.getStore().loadData(aMail);
        var gTelefono=gEx('gTelefono');
        var aTel=[];

        for(x=0;x<oContacto.telefono.length;x++)
        {
        	
        	aTel.push([oContacto.telefono[x].lada,oContacto.telefono[x].telefono,oContacto.telefono[x].extension,oContacto.telefono[x].tipo]);
        }
        gTelefono.getStore().loadData(aTel);
        
        
    }
}

function crearGridEmail()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'email'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,
														{
															header:'E-mail <span style="color:#F00">*<span>',
															width:220,
															sortable:true,
															dataIndex:'email',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            x:0,
                                                            y:0,
                                                            width:275,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:135,
															id:'gMail',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro([{name: 'email'}]);
                                                                                        var r=new reg	(
                                                                                        					{
                                                                                                            	email:''
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la direcci&oacute;n de E-mail que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la direcci&oacute;n de E-mail seleccionada?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	
    
	return 	tblGrid;
}

function crearGridTelefono()
{
	var cmbTipo=crearComboExt('cmbTipo',arrTipoTelefono);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'lada'},
                                                                    {name: 'telefono'},
                                                                    {name: 'extension'},
                                                                    {name: 'tipo'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,

														{
															header:'Tipo <span style="color:#F00">*<span>',
															width:70,
															sortable:true,
															dataIndex:'tipo',
                                                            editor:cmbTipo,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrTipoTelefono,val);
                                                                    }
														},
														{
															header:'Lada',
															width:40,
															sortable:true,
															dataIndex:'lada',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Tel&eacute;fono <span style="color:#F00">*<span>',
															width:90,
															sortable:true,
															dataIndex:'telefono',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Extensi&oacute;n',
															width:60,
															sortable:true,
															dataIndex:'extension',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
                                                            
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            x:0,
                                                            y:0,
                                                            clicksToEdit:1,
                                                            width:315,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:135,
															id:'gTelefono',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var reg=crearRegistro	(
                                                                                        							[
                                                                                                                    	{name: 'lada'},
                                                                                                                        {name: 'telefono'},
                                                                                                                        {name: 'extension'},
                                                                                                                        {name: 'tipo'}
                                                                                                                    ]
                                                                                        						);
                                                                                        var r=new reg	(
                                                                                        					{
                                                                                                            	lada:'',
                                                                                                                telefono:'',
                                                                                                                extension:'',
                                                                                                                tipo:'0'
                                                                                                            }
                                                                                        				)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el n&uacute;mero de tel&eacute;fono que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el n&uacute;mero de tel&eacute;fono seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function crearGridDestinatarios()
{

	


	var cmbNombreDestinatario=crearComboExt('cmbNombreDestinatario',[]);

	var rDestinatario= crearRegistro (
    									[
                                            {name:	'idContacto'},
                                            {name: 'nombreContacto'},
                                            {name: 'puesto'}
                                        ]
    								);
	var dsDatos=eval(bD(gE('arrDestinatarios').value));
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name:	'idContacto'},
                                                                    {name: 'nombreContacto'},
                                                                    {name: 'puesto'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
                                            
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														{
															header:'Nombre del destinatario',
															width:220,
															sortable:true,
															dataIndex:'idContacto',
                                                            editor:cmbNombreDestinatario,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var gContacto=gEx('gContacto');

                                                                    	var pos=obtenerPosFila(gContacto.getStore(),'idContacto',val);
                                                                        if(pos!=-1)
                                                                        {
                                                                            var fila=gContacto.getStore().getAt(pos);
                                                                            return fila.data.nombreContacto;
                                                                        }
                                                                    }
														},
														{
															header:'Puesto',
															width:130,
															sortable:true,
															dataIndex:'puesto',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var gContacto=gEx('gContacto');
                                                                    	var pos=obtenerPosFila(gContacto.getStore(),'idContacto',registro.data.idContacto);

                                                                        if(pos!=-1)
                                                                        {
                                                                            var fila=gContacto.getStore().getAt(pos);
                                                                            return fila.data.departamento;
                                                                        }
                                                                    }
														}


													]
												);
                            
                            
	                           
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            renderTo:'destinatarios',
                                                            id:'gDestinatarios',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:120,
                                                            width:780,
                                                            sm:chkRow,
                                                            
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar destinatario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var rDestinatario= crearRegistro (
                                                                                                                            [
                                                                                                                                {name:	'idContacto'},
                                                                                                                                {name: 'nombreContacto'},
                                                                                                                                {name: 'puesto'}
                                                                                                                            ]
                                                                                                                        );
                                                                                    	var r=new rDestinatario	(
                                                                                        							{
                                                                                                                        idContacto:'',
                                                                                                                        nombreContacto:'',	
                                                                                                                        puesto:''
                                                                                                                    }
                                                                                        						)                              
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/user_remove.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover destinatario',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el destinatario que desea remover');
                                                                                        	return 
                                                                                        }
                                                                                        function respC(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el destinatario seleccionado?',respC);	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	tblGrid.on('beforeedit',	function(e)
    							{
                                	var arrDestinatarios=[];
								    var gContacto=gEx('gContacto');
                                    var x;
                                    var fila;
                                    for(x=0;x<gContacto.getStore().getCount();x++)
                                    {
                                    	fila=gContacto.getStore().getAt(x);
                                        arrDestinatarios.push([fila.data.idContacto,fila.data.nombreContacto+' (Departamento: '+fila.data.departamento+')'])
                                    }
                                    gEx('cmbNombreDestinatario').getStore().loadData(arrDestinatarios);
                                }
    					
    			)
                                                        
	return 	tblGrid;	
}

function abrirAdmonCSD()
{
	var obj={};
    var nombreEmpresa='';
    if(gE('tipoEmpresa1').checked)
    {
    	nombreEmpresa=gE('_apPaternovch').value+' '+gE('_apMaternovch').value+' '+gE('_razonSocialvch').value;
    }
    else
    {
    	nombreEmpresa=gE('_razonSocialvch').value;
    }
    obj.titulo='Administración de CSD´s [Empresa: '+nombreEmpresa+']';
    obj.ancho='80%';
    obj.alto='80%';
    obj.url='../tesoreria/tblCertificados.php';
    obj.params=[['idReferencia',gE('idEmpresa').value],['cPagina','sFrm=true']];
    if(window.parent)
	    window.parent.abrirVentanaFancy(obj);
    else
    	abrirVentanaFancy(obj);
}

function crearPanelComprobanteFiscal()
{

}

function crearPanelNomina()
{

}

function enviarMail(t,ctrl)
{

	switch(t)
    {
    	case 1:
        	if(ctrl.checked)
	        	gE('enviarMailComprobanteReceptor').value='1';
            else
            	gE('enviarMailComprobanteReceptor').value='0';
        break;
        case 2:
        	if(ctrl.checked)
	        	gE('enviarMailNominaReceptor').value='1';
            else
            	gE('enviarMailNominaReceptor').value='0';
        break;
        
    }
}

function obtenerDatosConexionBuzon()
{
	var urlServidorEmail=gE('urlServidorEmail');
    var puertoConexion=gE('puertoConexion');
    var chkUtilizarSSL=gE('chkUtilizarSSL');
    var emailConexion=gE('emailConexion');
    var passwdConexion=gE('passwdConexion');
    var chkMoverCorreosProcesados=gE('chkMoverCorreosProcesados');
    
    var utilizarSSL=0;
    if(chkUtilizarSSL.checked)
        utilizarSSL=1;
    var moverCorreosProcesados=0;
    if(chkMoverCorreosProcesados.checked)
        moverCorreosProcesados=1;
        
    var cadDirectorios='';    
    var gDirectorio=gEx('gDirectorio') ;   
	var fila;
    var x;
    for(x=0;x<gDirectorio.getStore().getCount();x++)
    {
    	fila=gDirectorio.getStore().getAt(x);
        if(cadDirectorios=='')
        	cadDirectorios=fila.data.carpeta;
        else
        	cadDirectorios+=','+fila.data.carpeta;
    }
    
            
    var cadObj='{"directoriosBusqueda":"'+cadDirectorios+'","moverCorreosProcesados":"'+moverCorreosProcesados+'","urlServidorEmail":"'+cv(urlServidorEmail.value.trim(),true,true)+'","puertoConexion":"'+cv(puertoConexion.value.trim(),true,true)+'","utilizarSSL":"'+utilizarSSL+'","emailConexion":"'+cv(emailConexion.value.trim(),true,true)+'","passwdConexion":"'+cv(passwdConexion.value.trim(),true,true)+'"}';
    return cadObj;
}

function habilitadoImportacionClick(chk)
{
	if(chk.checked)
    {
    	habilitarControlesImportacionComprobantes();
        gE('habilitarBuzonRecepcion').value='1';
    }
    else
    {
    	desHabilitarControlesImportacionComprobantes();
        gE('habilitarBuzonRecepcion').value='0';
    }
}

function desHabilitarControlesImportacionComprobantes()
{
	var urlServidorEmail=gE('urlServidorEmail');
    var puertoConexion=gE('puertoConexion');
    var chkUtilizarSSL=gE('chkUtilizarSSL');
    var emailConexion=gE('emailConexion');
    var passwdConexion=gE('passwdConexion');
    var passwdConexion2=gE('passwdConexion2');
    var chkMoverCorreosProcesados=gE('chkMoverCorreosProcesados');
    
    urlServidorEmail.disabled=true;
    puertoConexion.disabled=true; 
    chkUtilizarSSL.disabled=true;
    emailConexion.disabled=true;
    passwdConexion.disabled=true;
    passwdConexion2.disabled=true;
    gEx('gDirectorio').disable();
    chkMoverCorreosProcesados.disabled=true;
    
    urlServidorEmail.value='';
    puertoConexion.value='';
    chkUtilizarSSL.checked=false;
    chkMoverCorreosProcesados.checked=false;
    
    emailConexion.value='';
    passwdConexion.value='';
    passwdConexion2.value='';
    gEx('gDirectorio').getStore().removeAll();
    
    oE('btnProbarConexion');
    
}

function habilitarControlesImportacionComprobantes()
{
	var urlServidorEmail=gE('urlServidorEmail');
    var puertoConexion=gE('puertoConexion');
    var chkUtilizarSSL=gE('chkUtilizarSSL');
    var emailConexion=gE('emailConexion');
    var passwdConexion=gE('passwdConexion');
    var passwdConexion2=gE('passwdConexion2');
    var chkMoverCorreosProcesados=gE('chkMoverCorreosProcesados');
    
    urlServidorEmail.disabled=false;
    puertoConexion.disabled=false; 
    chkUtilizarSSL.disabled=false;
    emailConexion.disabled=false;
    passwdConexion.disabled=false;
    passwdConexion2.disabled=false;
    chkMoverCorreosProcesados.disabled=false;
    gEx('gDirectorio').enable();
    mE('btnProbarConexion');
    
}

function realizarConexionServidorCorreo()
{
	if(validarDatosConexionBuzon())
    {
    	
    	var cadObj=obtenerDatosConexionBuzon();
        
        function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
            	
             	switch(arrResp[1])   
                {
                	case '0':
                    	
                    	msgBox('No se ha podido llevar a cabo la conexi&oacute;n debido al siguiente problema: <br><br>'+arrResp[2],null,Ext.MessageBox.ERROR);
                    break;
                    case '1':
	                    
                    	msgBox('La conexi&oacute;n se ha realizado exitosamente');
                    break;
                    
                }
                
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_Neofact.php',funcAjax, 'POST','funcion=2&cadObj='+bE(cadObj),true);
        
        
        
        
    }
}

function validarDatosConexionBuzon()
{
	var urlServidorEmail=gE('urlServidorEmail');
    var puertoConexion=gE('puertoConexion');
    var chkUtilizarSSL=gE('chkUtilizarSSL');
    var emailConexion=gE('emailConexion');
    var passwdConexion=gE('passwdConexion');
    var passwdConexion2=gE('passwdConexion2');
    
    if(urlServidorEmail.value.trim()=='')
    {
    	function resp()
        {
        	$( "#tabs" ).tabs( "option", "active", 5 );   
        	urlServidorEmail.focus();
        }
        msgBox('Debe ingresar la URL del servidor de correo al cual desea conectarse',resp);
        return false;
    }
    
    
    if(puertoConexion.value.trim()=='')
    {
    	function resp2()
        {
        	$( "#tabs" ).tabs( "option", "active", 5 );   
        	puertoConexion.focus();
        }
        msgBox('Debe ingresar el puerto de conexi&oacute;n al servidor de correo que desea configurar',resp2);
        return false;
    }
    
    if(emailConexion.value.trim()=='')
    {
    	function resp3()
        {
        	$( "#tabs" ).tabs( "option", "active", 5 );   
        	emailConexion.focus();
        }
        msgBox('Debe ingresar el Usuario/E-mail de la cuenta de correo al cual desea conectarse',resp3);
        return false;
    }
    
    if(passwdConexion.value.trim()=='')
    {
    	function resp4()
        {
        	$( "#tabs" ).tabs( "option", "active", 5 );   
        	passwdConexion.focus();
        }
        msgBox('Debe ingresar la contrase&ntilde;a de la cuenta de correo al cual desea conectarse',resp4);
        return false;
    }
    
    
    if(passwdConexion.value.trim()!=passwdConexion2.value.trim())
    {
    	function resp5()
        {
        	$( "#tabs" ).tabs( "option", "active", 5 );   
        	passwdConexion.focus();
        }
        msgBox('Las contrase&ntilde;as de la cuenta de correo ingresadas no coinciden',resp5);
        return false;
    }
    return true;
}

function crearGridCarpetas()
{
	var dsDatos=eval(bD(gE('arrBuzones').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'carpeta'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Carpeta',
															width:620,
															sortable:true,
															dataIndex:'carpeta',
                                                            renderer:formatearCarpetaBuzon
														}
														
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gDirectorio',
                                                            store:alDatos,
                                                            frame:false,
															renderTo:'tDirectorios',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:200,
                                                            width:700,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar carpeta',
                                                                            handler:function()
                                                                            		{
                                                                                    	if(validarDatosConexionBuzon())
																					    {
    	
																					    	var cadObj=obtenerDatosConexionBuzon();
                                                                                    
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    switch(arrResp[1])   
                                                                                                    {
                                                                                                        case '0':
                                                                                                            
                                                                                                            msgBox('No se ha podido llevar a cabo la conexi&oacute;n debido al siguiente problema: <br><br>'+arrResp[2],null,Ext.MessageBox.ERROR);
                                                                                                        break;
                                                                                                        case '1':
                                                                                                            
                                                                                                           var arrDatos=eval(arrResp[2]);
                                                                                                           mostrarVentanaCarpetasBuzon(arrDatos);
                                                                                                        break;
                                                                                                        
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_Neofact.php',funcAjax, 'POST','funcion=3&cadObj='+bE(cadObj),true);
                                                                                        
                                                                                    	}
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover carpeta',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la carpeta que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                                                                                                                
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la carpeta seleccionada?',resp);
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function mostrarVentanaCarpetasBuzon(arrCarpetas)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Seleccione los directorios que desea agregar para b&uacute;squeda de comprobantes fiscales:'
                                                        },
                                                        crearGridCarpetasAgregar(arrCarpetas)

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Selecci&oacute;n de carpetas para b&uacute;squeda de comprobantes fiscales',
										width: 700,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var gCarpetasAgregar=gEx('gCarpetasAgregar');
																		var filas=gCarpetasAgregar.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar los directorios que desea agregar para b&uacute;squeda de comprobantes fiscales');
                                                                            return;
                                                                        }
                                                                       	var gCarpetas= 	gEx('gDirectorio');
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(obtenerPosFila(gCarpetas.getStore(),'carpeta',filas[x].data.carpeta)==-1)
                                                                        		gCarpetas.getStore().add(filas[x].copy());
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearGridCarpetasAgregar(arrDatos)
{
	var dsDatos=arrDatos;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'carpeta'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:false});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Carpeta',
															width:550,
															sortable:true,
															dataIndex:'carpeta',
                                                            renderer:formatearCarpetaBuzon
														}
														
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gCarpetasAgregar',
                                                            store:alDatos,
                                                            frame:false,
															x:10,
                                                            y:40,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:250,
                                                            width:630,
                                                            sm:chkRow
                                                            
                                                        }
                                                    );
	return 	tblGrid;
}

function formatearCarpetaBuzon(val)
{
	var arrDatos=val.split('}INBOX.');
    if(arrDatos.length>1)
    {
    	var pos=existeValorMatriz(arrAliasCarpetas,arrDatos[1]);
        if(pos!=-1)
        	return arrAliasCarpetas[pos][1];
        return arrDatos[1].replace(/\./gi,' ==> ');
    }
    else
    {
    	var arrDatos=val.split('}INBOX');
        if(arrDatos.length>1)
        {
            var pos=existeValorMatriz(arrAliasCarpetas,arrDatos[1]);
            if(pos!=-1)
                return arrAliasCarpetas[pos][1];
            return arrDatos[1].replace(/\./gi,' ==> ');
        }
        return val;
    }
    return val;
}

function mostrarFirmaManifiesto()
{
	var obj={};
    var nombreEmpresa='';
    if(gE('tipoEmpresa1').checked)
    {
    	nombreEmpresa=gE('_apPaternovch').value+' '+gE('_apMaternovch').value+' '+gE('_razonSocialvch').value;
    }
    else
    {
    	nombreEmpresa=gE('_razonSocialvch').value;
    }
    obj.titulo='Firma de Manifiesto [Empresa: '+nombreEmpresa+']';
    obj.ancho=850;
    obj.alto=480;
    obj.url='../tesoreria/autorizacionManifiesto.php';
    obj.params=[['idEmpresa',gE('idEmpresa').value],['cPagina','sFrm=true']];
    abrirVentanaFancy(obj);	
}
