<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$arrIVAZona="";
	$idProducto=bD($_GET["iP"]);
	$consulta="SELECT categoriaIVA FROM 6901_catalogoProductos WHERE idProducto=".$idProducto;
	$idCategoria=$con->obtenerValor($consulta);
	if($idCategoria=="")
		$idCategoria=-1;
	$arrColumnas="";
	$arrCampos="";
	$consulta="SELECT  z.idZona,z.zona FROM  6939_zonasCategoriaIVA zc,6937_zonas z WHERE z.idZona=zc.idZona AND zc.idCategoria=".$idCategoria;
	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$consulta="SELECT porcentajeIVA FROM 6940_porcentajeIVACategoria WHERE idCategoria=".$idCategoria." AND idZona=".$fila[0]." and fechaInicio<='".date("Y-m-d")."' ORDER BY fechaInicio DESC";
		$porcentajeIVA=$con->obtenerValor($consulta);
		if($porcentajeIVA=="")
			$porcentajeIVA=0;
		
		$IZona="['".$fila[0]."','".cv($fila[1])."','".$porcentajeIVA."']";	
		if($arrIVAZona=="")
			$arrIVAZona=$IZona;	
		else
			$arrIVAZona.=",".$IZona;	
			
		$arrCampos.=",{name: 'precio_".$fila[0]."'},{name: 'fechaInicio_".$fila[0]."',type:'date',dateFormat:'Y-m-d'}";
		$c="
			 {
				header: 'Precio público (Con '+Ext.util.Format.number(".$porcentajeIVA.",'0.00')+'% IVA)<br><b>Zona:</b> ".cv($fila[1])."<br>',
				dataIndex: 'precio_".$fila[0]."',
				css:'text-align:right;',
				width: 200,
				renderer:function(val,meta,registro)
						{
							
							if((registro.data.ultimaDimension=='0')||(val==''))
								return '';
							else
							{
								return '<b><span style=\"color:#900\">'+Ext.util.Format.usMoney(val)+'</span></b>  '+ 
								'<a href=\"javascript:mostrarVentanaHistorialPrecios(\''+bE(registro.data.llave)+'\',\''+bE(".$fila[0].")+'\')\"><span title=\"Ver historial de precios\" alt=\"Ver historial de precios\">(A partir del: '+registro.data.fechaInicio_".$fila[0].".format('d/m/Y')+')</span></a>'
							}
						}
			}";
		$arrColumnas.=",".$c;
	}
?>
var uploadControl;
var arrIVAZona=[<?php echo $arrIVAZona?>];
var regCategoria=null;
var regProductos=null;
var imagenSel=null;
var llaveGaleria='';
var fechaActual;


Ext.onReady(inicializar);

function inicializar()
{
	fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>','Y-m-d');

	Ext.util.Format.bE=function(val)
    					{
                        	return bE(val);
                        }
	Ext.util.Format.bytesToSize=function(val)
    					{
                        	return bytesToSize(val);
                        }                        
	
                                
   	new Ext.Panel(                             
                                
                                            {
                                                

                                                renderTo:'tblBase',
                                                layout:'border',
                                                border:false,
                                               	height:700,
                                                width:920,
                                                items:	[
                                                         	{
                                                            	xtype:'panel',
                                                                region:'center',
                                                                border:false,
                                                                autoScroll:true,
                                                                contentEl:'sProducto'
                                                            }  ,
                                                            {
                                                            	region:'east',
                                                                layout:'anchor',
                                                                id:'pCategoria',
                                                                collapsible:true,
                                                                items:	[
                                                                			crearGridCategorias()
                                                                		],
                                                                width:230
                                                                
                                                                
                                                            } 
                                                        ]
                                            }
                                         
			)
	var _categoriavch=gE('_cveProductovch');
    if(_categoriavch!=null)
    	_categoriavch.focus();

     crearGridProductos();
     
   
     var x;
     var aDatos='';
     var arrDatos;
     var cadObj='';
     var imagen;
     for(x=0;x<arrInvocacion.length;x++)
     {
     	aDatos=arrInvocacion[x].split('_');
        cadObj='{"idFormulario":"'+aDatos[1]+'","idFrame":"'+arrInvocacion[x]+'"}';
        arrDatos=[['idFormulario',aDatos[1]],['idRegistro',aDatos[0]],['idAlmacen',gE('idAlmacen').value],['idProducto',gE('id').value],['cPagina','sFrm=true'],['eliminarEspacios',1],['eJs',bE('window.parent.funcionGuardar(@idRegistro,\''+bE(cadObj)+'\');return;')]];//,['accionCancelar','window.parent.cerrarVentanaFancy()']];=[['idFormulario',(iF)],['idRegistro',(iR)],['idAlmacen',(iA)],['idProducto',(iP)],['cPagina','sFrm=true'],['eliminarEspacios',1],['eJs',bE('window.parent.registroModificado(@idRegistro,"'+llave+'","'+t+'");return;')],['accionCancelar','window.parent.cerrarVentanaFancy()']
		enviarFormularioDatos('../modeloPerfiles/registroFormulario.php',arrDatos,'POST',arrInvocacion[x]);     
     }
     
     
    
    var tabs = $( "#tabs" ).tabs(
    								{
    									activate: function( event, ui ) 
                                        		{
                                                	

                                                	if(ui.newTab.context.innerHTML=='Ficha del producto')
                                                    {
                                                    	gEx('pCategoria').enable();
                                                        gEx('pCategoria').expand();
                                                    }
                                                    else
                                                    {
                                                    	gEx('pCategoria').disable();
                                                        gEx('pCategoria').collapse();
                                                    }
                                                    
                                                    
                                                    switch(ui.newTab.context.innerHTML)
                                                    {
                                                    	case 'Admón. de Claves del producto':
                                                        	gEx('arbolClaves').getView().refresh();
                                                        break;
                                                        case 'Precio a público':
                                                        	gEx('arbolCostos').getView().refresh();
                                                        break;
                                                        case 'Atributos del producto':
                                                        	gEx('arbolAtributos').getView().refresh();
                                                        break;
                                                    }

                                                }	
                                    }
    							);
	tabs.find( ".ui-tabs-nav" ).sortable(	
    										{
                                              axis: "x",
                                              stop: function() 
                                              		{
                                                		tabs.tabs( "refresh" );
                                              		}
										    }
                                        );
   
   for(x=0;x<arrInvocacion.length;x++)
   {
     	aDatos=arrInvocacion[x].split('_');
        
        if(aDatos[0]=='-1')
        	imagen='../images/cross.png';
        else
        	imagen='../images/icon_big_tick.gif';
        
        gE('img_'+aDatos[1]).src=imagen;
	}
   
   crearGridAtributos();
   crearGridClaves();
   crearGridCosto();
   
   
}

function mostrarVentanaAgregarCategoria()
{
	var gridConcepto=crearGridConcepto();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Indique las categor&iacute;as que desea agregar al producto:'
                                                        },
                                                        gridConcepto
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar categor&iacute;a',
										width: 700,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var  filas=gridConcepto.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos una categor&iacute;a para asociarla al producto');
                                                                            return;
                                                                        }
                                                                     	var x;
                                                                        var f;
                                                                        var pos;
                                                                        var gridCategorias=gEx('gridCategoriasConcepto');
                                                                        for(x=0;x<filas.length;x++)   
                                                                        {
                                                                        	
                                                                        	f=filas[x];
                                                                            pos=obtenerPosFila(gridCategorias.getStore(),'idCategoria',f.get('idCategoria'));
                                                                            if(pos==-1)
                                                                            {
                                                                            	var r=new regCategoria({idCategoria:f.get('idCategoria'),categoria:f.get('nombreCategoria')});
                                                                                gridCategorias.getStore().add(r);
                                                                            }
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

}

function crearGridConcepto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCategoria'},
		                                                {name: 'nombreCategoria'},
		                                                {name:'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreCategoria', direction: 'ASC'},
                                                            groupField: 'nombreCategoria',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='145';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;

                                    }
                        )   
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Categor&iacute;a',
                                                                width:230,
                                                                sortable:true,
                                                                dataIndex:'nombreCategoria',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcion',
                                                                renderer:mostrarValorDescripcion
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridCategorias',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:360,
                                                                sm:chkRow,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	if(gE('categoria').value=='')
        {
        	msgBox('Debe especificar la categor&iacute;a a la cual pertenece el producto');
        	return;
        }
    	var id=gE('id').value;
    	
        var gridProductos=gEx('gridProductos');
        var fila;
        
        
        var cadProductos='';
        
        for(x=0;x<gridProductos.getStore().getCount();x++)
        {
        	fila=gridProductos.getStore().getAt(x);
            obj='{"idProducto":"'+fila.get('idProducto')+'","cantidad":"'+fila.get('cantidad')+'"}';
            if(cadProductos=='')
            	cadProductos=obj;
            else
            	cadProductos+=','+obj;
        }
        var objArrProd='{"arrProductos":['+cadProductos+']}';
      
        gE('arrProductos').value=bE(objArrProd);
        
        if(gE('id').value=='-1')
        {
            gE('funcPHPEjecutarNuevo').value=bE('renombrarProductosAsociados(@idRegPadre)');
        }
        else
        {
            gE('funcPHPEjecutarModif').value=bE('renombrarProductosAsociados(@idRegPadre)');
        }
            
            gE('frmEnvio').submit();
        }
    
    
    
}

function abrirSeccion(llave,t)
{
	var iA,iP,iF,iR,w,h;
    
    var arrLlaves=bD(llave).split('_');
    iA=arrLlaves[0];
    iP=arrLlaves[1];
    iF=arrLlaves[2];
    iR=arrLlaves[3];
    w=arrLlaves[4];
    h=arrLlaves[5];
    
    
	var obj={};
    var ajuste=-50;
    obj.ancho=850;
    if((parseInt((w))-ajuste)<obj.ancho)
    	obj.ancho=parseInt((w))-ajuste;
	obj.alto=480;
    if((parseInt((h))-ajuste)<obj.alto)
    	obj.alto=parseInt((h))-ajuste;
    obj.titulo=bD(t);
    obj.params=[['idFormulario',(iF)],['idRegistro',(iR)],['idAlmacen',(iA)],['idProducto',(iP)],['cPagina','sFrm=true'],['eliminarEspacios',1],['eJs',bE('window.parent.registroModificado(@idRegistro,"'+llave+'","'+t+'");return;')],['accionCancelar','window.parent.cerrarVentanaFancy()']];
    obj.url='../modeloPerfiles/registroFormulario.php';
    abrirVentanaFancy(obj);
}

function registroModificado(iReg,llave,t)
{
	var iA,iP,iF,iR,w,h;
    
    var arrLlaves=bD(llave).split('_');
    iA=arrLlaves[0];
    iP=arrLlaves[1];
    iF=arrLlaves[2];
    iR=iReg;
    w=arrLlaves[4];
    h=arrLlaves[5];
    
    
    var llaveAux=iA+'_'+iP+'_'+iF+'_'+iR+'_'+w+'_'+h;
    gE(bD(llave)).innerHTML='<table><tr><td width="16"><img src="../images/bullet_green.png"></td><td><a  href="javascript:abrirSeccion(\''+bE(llaveAux)+'\',\''+(t)+'\')">'+bD(t)+'</a></td></tr></table>';
    gE(bD(llave)).id=llaveAux;
	cerrarVentanaFancy();
}

function crearGridProductos()
{
	regProductos=crearRegistro([{name: 'idProducto'},{name: 'cveProducto'},{name: 'nombreProducto'},{name: 'cantidad'}]);
	var dsDatos=eval(bD(gE('arrProductos').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'cveProducto'},
                                                                    {name: 'nombreProducto'},
                                                                    {name: 'cantidad'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Cve. Producto',
															width:100,
															sortable:true,
															dataIndex:'cveProducto'
														},
                                                        {
															header:'Nombre del Producto',
															width:350,
															sortable:true,
															dataIndex:'nombreProducto'
														},
                                                        {
															header:'Cantidad',
															width:80,
															sortable:true,
															dataIndex:'cantidad',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            id:'gridProductos',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            clicksToEdit:1,
                                                            columnLines : true,
                                                            renderTo:'tblProductos',
                                                            height:200,
                                                            width:620,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarProducto();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar almenos un producto a remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                           
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function mostrarVentanaAgregarProducto()
{
	var gridProductosAdd=crearGridProductosAgregar();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                            x:10,
                                                            y:10,
                                                            html:'Seleccione los productos a agregar:'
                                                        },
                                                        gridProductosAdd

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar producto',
										width: 620,
										height:370,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var gridProductos=gEx('gridProductos');
																		var filas=gridProductosAdd.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto que desea agregar');
                                                                            return;
                                                                        }
                                                                        var x;
                                                                        var r;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(obtenerPosFila(gridProductos.getStore(),'idProducto',filas[x].data.idProducto)==-1)
                                                                         	{   
                                                                                r=new regProductos	(
                                                                                                        {
                                                                                                            idProducto:filas[x].data.idProducto,
                                                                                                            cveProducto:filas[x].data.cveProducto,
                                                                                                            nombreProducto:filas[x].data.nombreProducto,
                                                                                                            cantidad:1
                                                                                                        }
                                                                                                        
                                                                                                    )
                                                                                                    
                                                                                                    
                                                                                gridProductos.getStore().add(r);  
                                                                        	}                    
                                                                        }
                                                                        ventanaAM.close();
																	
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    gEx('gridProductosAdd').getStore().load({url:'../paginasFunciones/funcionesAlmacen.php',params:{funcion:146,idAlmacen:gE('idAlmacen').value,start:0,limit:100}});
}

function crearGridProductosAgregar()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idProducto'},
                                                        {name: 'cveProducto'},
                                                        {name: 'nombreProducto'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='146';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                    }
                        )   
       
       
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'cveProducto'},
                                                                        {type: 'string', dataIndex: 'nombreProducto'}
                                                                        
                                                                    ]
                                                    }
                                                );        
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                           {
                                                                header:'Cve. Producto',
                                                                width:130,
                                                                sortable:true,
                                                                dataIndex:'cveProducto'
                                                            },
                                                            {
                                                                header:'Nombre del Producto',
                                                                width:380,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto'
                                                            }
                                                        ]
                                                    );
         
        var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )   
         
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridProductosAdd',
                                                                store:alDatos,
                                                                x:10,
                                                                y:40,
                                                                sm:chkRow,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                width:570,
                                                                height:250,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
        
}

function tipoArticuloSel(cmb)
{
	var valor=cmb.options[cmb.selectedIndex].value;
    if(valor=='1')
    	oE('filaProductos');
    else
    {
    	mE('filaProductos');
        gEx('gridProductos').getView().refresh();
    }
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    region:'center',
                                                    enableDD: false,
                                                    border:false,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    title:'<span style="color:#000;">Especifique la categor&iacute;a a la cual pertenece el producto:</span>',
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    anchor:'100% 100%',
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 200,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		var id=gE('categoria').value;
                                                                                if(id==record.data.llave)
                                                                                	checado='checked="true"';
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="radio" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel" onclick="nodoSelClick(this)" />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            	return mostrarValorDescripcion(v);
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function nodoSelClick(rdo)
{
	gE('categoria').value=rdo.id;

}

function funcionGuardar(iR,cadObj)
{
	var obj=eval('['+bD(cadObj)+']')[0];
	var arrDatos=[['idFormulario',obj.idFormulario],['idRegistro',iR],['idAlmacen',gE('idAlmacen').value],['idProducto',gE('id').value],['cPagina','sFrm=true'],['eliminarEspacios',1],['eJs',bE('window.parent.funcionGuardar(@idRegistro,\''+(cadObj)+'\');return;')]];
	enviarFormularioDatos('../modeloPerfiles/registroFormulario.php',arrDatos,'POST',obj.idFrame);
    var imagen='../images/icon_big_tick.gif';
    gE('img_'+obj.idFormulario).src=imagen;
    msgBox('La operaci&oacute;n ha sido realizada correctamente');     
}

function crearGridAtributos()
{
	if(!gE('tblAtributos'))
    	return;
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'id',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'id'},
                                                                                                                    {name: 'dimension'},
                                                                                                                    {name: 'llave'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: 'ultimaDimension'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'idDimension'},
                                                                                                                    {name: '_is_leaf', type: 'bool'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.funcion=165;
                            	proxy.baseParams.idProducto=gE('id').value;
                                
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    renderTo:'tblAtributos',
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    id:'arbolAtributos',
													store: store,
                                                    stripeRows: true,
                                                    columnLines :true,
													loadMask :true,
                                                    width:790,
                                                    height:250,
                                                    tbar:	[
                                                                {
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Agregar valor de atributo',
                                                                    handler:function()
                                                                            {
                                                                            	var arrNodos=obtenerNodosCheck();
                                                                                var nivel;
                                                                            	if(arrNodos.length==0)
                                                                                	nivel=0;
                                                                                else
                                                                                {
                                                                                	var idFila=arrNodos[0].getAttribute('idFila');
																					var fila=gEx('arbolAtributos').getStore().getById(idFila);
                                                                                    nivel=parseInt(fila.data.nivel);
                                                                                }
                                                                                mostrarVentanaAgregarValorAtributo(nivel);
                                                                            }
                                                                    
                                                                }
                                                                
                                                            ],
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Atributo',
                                                                        dataIndex: 'dimension',
                                                                        width: 500,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	var comp='<a href="javascript:removerAtributo(\''+bE(record.id)+'\')"><img width="13" height="13" src="../images/delete.png" alt="Remover atributo" title="Remover atributo"></a>&nbsp;&nbsp;';
                                                                        
                                                                        	if(record.data.ultimaDimension=='0')
                                                                            {
                                                                                return [
                                                                                             '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                             '<input type="checkbox" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="chk_'+record.id+
                                                                                             '" name="chkSelAt" onclick="nodoClick(this)" idDimension="'+record.data.idDimension+'" idFila="'+record.id+'"  />&nbsp;',
                                                                                             '&nbsp;'+comp+'<span class="ux-maximgb-tg-mastercol-editorplace">', v, '</span>'
                                                                                          ].join('');
                                                                            }
                                                                        	return comp+v;
                                                                        	
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function crearGridCosto()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'llave',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                        			{name: 'id'},
                                                                                                                    {name: 'llave'},
                                                                                                                    {name: 'dimension'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: 'ultimaDimension'},
                                                                                                                    {name: '_is_leaf', type: 'bool'}
                                                                                                                    <?php
																														echo $arrCampos;
																													?>
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.funcion=162;
                            	proxy.baseParams.idProducto=gE('id').value;
                                
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	gEx('btnModificar').disable();
                        gEx('btnAgregarDescuento').disable();
                    	gEx('arbolCostos').getStore().expandAll();
                        
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                        renderTo:'tblPrecioPublico',
                                                        enableDD: false,
                                                        border:true,
                                                        autoScroll:true,
                                                        disableSelection:true,
                                                        id:'arbolCostos',
                                                        store: store,
                                                        stripeRows: true,
                                                        columnLines :true,
                                                        loadMask :true,
                                                        width:790,
                                                        height:250,
                                                        clicksToEdit:1,
                                                        tbar:	[
                                                            			{
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            id:'btnModificar',
                                                                            text:'Modificar precio',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrNodos=obtenerNodosCheckCosto();
                                                                                    	modificarPrecioProducto(arrNodos);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/coins_minus.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            id:'btnAgregarDescuento',
                                                                            text:'Administrar descuentos',
                                                                            handler:function()
                                                                            		{
                                                                                    
                                                                                    	
                                                                                    	var arrNodos=obtenerNodosCheckCosto();
                                                                                        mostrarAdmonDescuentos(arrNodos[0]);
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/coins_minus.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Administrar descuentos (Producto General)',
                                                                            handler:function()
                                                                            		{
                                                                                        mostrarAdmonDescuentosGlobalProducto();
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                                ],
                                                        columns:	[
                                                                        {
                                                                            header: 'Especificaciones del producto',
                                                                            dataIndex: 'dimension',
                                                                            width: 365,
                                                                            renderer:function(v,meta,record)
                                                                            {
                                                                            
                                                                            	
                                                                            
                                                                            	if(record.data.ultimaDimension=='1')
                                                                            	{
                                                                                     return [
                                                                                               '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                               '<input type="checkbox" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="chk_'+record.id+
                                                                                                 '" name="chkSelAtCosto" onclick="nodoClickCosto(this)" idDimension="'+record.data.idDimension+'" idFila="'+record.id+'"  />&nbsp;',

                                                                                               '<span class="ux-maximgb-tg-mastercol-editorplace">', v, '</span>'
                                                                                            ].join('');
                                                                                 }
                                                                                 return [
                                                                                               '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                              
                                                                                               '<span class="ux-maximgb-tg-mastercol-editorplace">', v, '</span>'
                                                                                            ].join('');
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        <?php
																			echo $arrColumnas;
																		?>
                                                                        
                                                                        /*{
                                                                            header: '&Uacute;ltimo precio',
                                                                            dataIndex: 'precio',
                                                                            css:'text-align:right;',
                                                                            width: 95,

                                                                            renderer:function(val,meta,registro)
                                                                            		{
                                                                                    	if(registro.data.ultimaDimension=='0')
                                                                                        	return '';
                                                                                        else
                                                                                        	return Ext.util.Format.usMoney(val);
                                                                                    }
                                                                        },
                                                                        {
                                                                            header: 'Vigencia a partir del',
                                                                            dataIndex: 'fechaInicio',
                                                                            css:'text-align:right;',
                                                                            width: 120,
                                                                            renderer:function(val,meta,registro)
                                                                            		{
                                                                                    	if(registro.data.ultimaDimension=='0')
                                                                                        	return '';
                                                                                    	if(val)
                                                                                        	return '<a href="javascript:mostrarVentanaHistorialPrecios(\''+bE(registro.data.llave)+'\')"><img width=13 height=13 src="../images/book_spelling.png" title="Ver historial de precios" alt="Ver historial de precios"></a> '+val.format('d/m/Y');
                                                                                    }
                                                                            
                                                                            
                                                                            
                                                                        }*/
                                                                    ]
                                                         
                                                
                                                        
                                                    }
                                              );
                                          
	
    grid.on('beforeedit',function(e)
    					{
                        	if(e.record.data.ultimaDimension=='0')
                            	e.cancel=true;
                        }
    		)
     
     
    grid.on('afteredit',function(e)
    					{
                        	var tipoCodigo=0;
                            if(e.field=='codigoBarras')
                            	tipoCodigo=1;
                            if(e.field=='codigoAlternativo')
                            	tipoCodigo=2;
                        
                        
                        	var cadObj='{"tipoCodigo":"'+tipoCodigo+'","valor":"'+e.value+'","llave":"'+e.record.data.llave+'","idProducto":"'+gE('id').value+'"}';
                        
                        	function funcAjax()
                            {
                                var resp=peticion_http.responseText;
                                arrResp=resp.split('|');
                                if(arrResp[0]=='1')
                                {
                                    
                                }
                                else
                                {
                                	function resp()
                                    {
                                    	e.record.set(e.field,e.originalValue);
                                    }
                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0],resp);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=167&cadObj='+cadObj,true);

                        
                        }
    		)
	grid.galeriaGlobal=true;                                          
	return grid;  
   
}


function mostrarAdmonDescuentos(nodo)
{
	var idFila=nodo.getAttribute('idFila');
    var fila=gEx('arbolCostos').getStore().getById(idFila);

    
	var gridDescuentos=crearGridDescuentosProducto(gE('id').value,fila.data.llave);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridDescuentos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Descuentos de producto',
										width: 800,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarAdmonDescuentosGlobalProducto()
{
	
    
	var gridDescuentos=crearGridDescuentosProducto(gE('id').value,'');
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridDescuentos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Descuentos de producto',
										width: 800,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function mostrarVentanaAgregarValorAtributo(nivel)
{
	var gridAtributos=crearGridAtributosAdd(nivel);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span  style="font-weight: bold;font-color:#000 ">Atributo: </span>&nbsp;&nbsp;<b><span class="letraRojaSubrayada8" id="lblAtributo"></span></b>'
                                                        },
														gridAtributos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar valor de atributo',
										width: 700,
										height:420,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var padre='';
                                                                        var cadObj='';
                                                                        var obj='';
                                                                        var x;
                                                                        var fila;
                                                                        var arrFilas=gridAtributos.getSelectionModel().getSelections();
                                                                        var arrNodos=obtenerNodosCheck();
                                                                        if(arrNodos.length==0)
                                                                        {

                                                                            for(x=0;x<arrFilas.length;x++)
                                                                            {
                                                                                fila=arrFilas[x];
                                                                                obj='{"padre":"'+padre+'","id":"'+fila.data.idAtributo+'","idDimension":"'+fila.data.idDimension+'"}';
                                                                                if(cadObj=='')
                                                                                    cadObj=obj;
                                                                                else
                                                                                    cadObj+=','+obj;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                         	var ct;
                                                                            for(ct=0;ct<arrNodos.length;ct++)
                                                                            {
                                                                            	padre=arrNodos[ct].getAttribute('idFila');
                                                                            	for(x=0;x<arrFilas.length;x++)
                                                                                {
                                                                                    fila=arrFilas[x];
                                                                                    obj='{"padre":"'+padre+'","id":"'+fila.data.idAtributo+'","idDimension":"'+fila.data.idDimension+'"}';
                                                                                    if(cadObj=='')
                                                                                        cadObj=obj;
                                                                                    else
                                                                                        cadObj+=','+obj;
                                                                                }
                                                                            }
                                                                            
                                                                        }
                                                                        var cObj='{"idProducto":"'+gE('id').value+'","arrDimensiones":['+cadObj+']}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                gEx('arbolAtributos').getStore().reload();
                                                                                gEx('arbolClaves').getStore().reload();
                                                                                gEx('arbolCostos').getStore().reload();
                                                                                
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=164&cadObj='+cObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridAtributosAdd(nivel)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idAtributo'},
		                                                {name: 'valorAtributo'},
                                                        {name: 'idDimension'},
                                                        {name: 'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(

                                                                                  {

                                                                                      url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'valorAtributo', direction: 'ASC'},
                                                groupField: 'valorAtributo',
                                                remoteGroup:false,
                                                remoteSort: false,
                                                autoLoad:true
                                                
                                            }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='163';
                                        proxy.baseParams.nivel=nivel;
                                        proxy.baseParams.idProducto=gE('id').value;
                                    }
                        )   
                        
	alDatos.on('load',function(proxy)
    								{
                                    	gE('lblAtributo').innerHTML=proxy.reader.jsonData.etiqueta;
                                    }
                        )                           
                        
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            
                                                            {
                                                                header:'Valor del atributo',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'valorAtributo'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAtributosAdd',
                                                                store:alDatos,
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                sm:chkRow,
                                                                y:40,
                                                                height:280,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function nodoClick(nodo)
{
	var arrNodos=obtenerNodosCheck();

    if(arrNodos.length==0)
    {
    	var arrNodos=gEN('chkSelAt');
        var x;
        var aNodoAux;
        for(x=0;x<arrNodos.length;x++)
        {
            aNodoAux=arrNodos[x];
            aNodoAux.disabled=false;
            
        }
    }
    else
        if(arrNodos.length==1)
        {
            var idFila=arrNodos[0].getAttribute('idFila');
            var fila=gEx('arbolAtributos').getStore().getById(idFila);
            var idDimension=fila.data.idDimension;
            var arrNodos=gEN('chkSelAt');
            var x;
            var aNodoAux;
            for(x=0;x<arrNodos.length;x++)
            {
                aNodoAux=arrNodos[x];
                if(aNodoAux.getAttribute('idDimension')==idDimension)
                    aNodoAux.disabled=false;
                else
                    aNodoAux.disabled=true;
            }
        }
    
	  
}

function obtenerNodosCheck()
{
	var arrCheck=new Array();
	var arrNodos=gEN('chkSelAt');
    var x;
    var aNodoAux;
    for(x=0;x<arrNodos.length;x++)
    {
        aNodoAux=arrNodos[x];
        if(aNodoAux.checked)
        {
        	
            arrCheck.push(aNodoAux);
        }
    }

    return arrCheck;
}

function removerAtributo(iA)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    gEx('arbolAtributos').getStore().reload();
                    gEx('arbolClaves').getStore().reload();
                    gEx('arbolCostos').getStore().reload();
                    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=166&idAtributo='+bD(iA),true);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover el atributo seleccionado?',resp);
}

function nodoClickCosto(nodo)
{
	gEx('btnAgregarDescuento').disable();
	var arrNodos=obtenerNodosCheckCosto();
    if(arrNodos.length>0)
    {
    	gEx('btnModificar').enable();
        if(arrNodos.length==1)
	        gEx('btnAgregarDescuento').enable();
        
    }
    else
    {
    	gEx('btnModificar').disable();
        
    }
   
}

function obtenerNodosCheckCosto()
{
	var arrCheck=new Array();
	var arrNodos=gEN('chkSelAtCosto');
    var x;
    var aNodoAux;
    for(x=0;x<arrNodos.length;x++)
    {
        aNodoAux=arrNodos[x];
        if(aNodoAux.checked)
        {
        	
            arrCheck.push(aNodoAux);
        }
    }

    return arrCheck;
}

function modificarPrecioProducto(arrNodos)
{
	var tipoPrecio;
	var cmbZona=crearComboExt('cmbZona',arrIVAZona,135,5,250);

	cmbZona.on('select',function(cmb,registro)
    					{
                        	
                        	gEx('txtPorcentaje').setValue(Ext.util.Format.number(registro.data.valorComp,'0.00')+'%');
                            var txtPrecio=gEx('txtPrecio').getValue();
                            
                            if(txtPrecio!='')
                            {
                            	var publico=parseFloat(txtPrecio)+(parseFloat(txtPrecio)*(parseFloat(registro.data.valorComp)/100));
                                publico=Ext.util.Format.number(publico,'0.00');
                                gEx('txtPrecioPublico').setValue(publico);
                            }
                            
                        }
    			)


	

	var fechaActual=Date.parseDate('<?php echo date('Y-m-d')?>','Y-m-d');
    var minFecha=fechaActual;
    var x;
    /*for(x=0;x<arrNodos.length;x++)
    {
    	var idFila=arrNodos[x].getAttribute('idFila');
		var fila=gEx('arbolCostos').getStore().getById(idFila);
        if(fila.data.fechaInicio)
        {
        	
            if((minFecha<fila.data.fechaInicio)&&())
            
            	minFecha=fila.data.fechaInicio;
            
        }
    }*/
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Zona a aplicar:'
                                                        },
                                                        cmbZona,
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Precio Neto:'
                                                        },
                                                       
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            id:'txtPrecio',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            listeners:	{
                                                            				change:function(ctrl,nV,oV)
                                                                            		{
                                                                                    	
                                                                                    	var porcentaje=normalizarValor(gEx('txtPorcentaje').getValue());
                                                                                        if(porcentaje=='')
	                                                                                        porcentaje=0;
                                                                                    	var publico=parseFloat(nV)*(1+(parseFloat(porcentaje)/100));
                                                                                        publico=Ext.util.Format.number(publico,'0.00');
                                                                                        gEx('txtPrecioPublico').setValue(publico);
                                                                                        tipoPrecio=0;
                                                                                    }
                                                            			}
                                                        },
                                                         {
                                                        	x:10,
                                                            y:70,
                                                            html:'Tasa IVA:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:65,
                                                            width:'65',
                                                            readOnly:true,
                                                            id:'txtPorcentaje',
                                                            xtype:'textfield'
                                                            
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            html:'Precio público:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:95,
                                                            id:'txtPrecioPublico',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            listeners:	{
                                                            				change:function(ctrl,nV,oV)
                                                                            		{
                                                                                    
                                                                                    	var porcentaje=normalizarValor(gEx('txtPorcentaje').getValue());
                                                                                        if(porcentaje=='')
	                                                                                        porcentaje=0;
                                                                                    	var publico=parseFloat(nV)/((parseFloat(porcentaje)/100)+1);
                                                                                        publico=Ext.util.Format.number(publico,'0.00');
                                                                                        gEx('txtPrecio').setValue(publico);
                                                                                        tipoPrecio=1;
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Vigente a partir del:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:125,
                                                            xtype:'datefield',
                                                            id:'dteVigencia',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar precio de producto',
										width: 500,
										height:230,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtPrecio').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var cadObj='';
                                                                        var obj='';
																		var x;
                                                                        var dteVigencia=gEx('dteVigencia');
                                                                        var txtPrecio=gEx('txtPrecio');
                                                                        
                                                                        if(cmbZona.getValue()=='')
                                                                        {
                                                                        	function resp10()
                                                                            {
                                                                            	cmbZona.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar la zona cuyo costo desea modificar',resp10);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtPrecio.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtPrecio.focus();
                                                                            }
                                                                            msgBox('El precio del producto ingresado no es v&aacute;lido',resp);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicio de vigencia del precio ingresado',resp2);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        
                                                                        for(x=0;x<arrNodos.length;x++)
                                                                        {
                                                                            var idFila=arrNodos[x].getAttribute('idFila');
                                                                            var fila=gEx('arbolCostos').getStore().getById(idFila);
                                                                            obj='{"tipoPrecio":"'+tipoPrecio+'","idZona":"'+cmbZona.getValue()+'","fechaInicio":"'+dteVigencia.getValue().format('Y-m-d')+
                                                                            	'","precio":"'+((tipoPrecio==0)?txtPrecio.getValue():gEx('txtPrecioPublico').getValue())+'","idProducto":"'+gE('id').value+'","llave":"'+fila.data.llave+'"}';
                                                                            if(cadObj=='')
                                                                            	cadObj=obj;
                                                                            else
                                                                            	cadObj+=','+obj;
                                                                        }
                                                                        
                                                                        var cObj='{"aRegistros":['+cadObj+']}'
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('arbolCostos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=168&cadObj='+cObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    if(arrIVAZona.length==1)
    {
    	cmbZona.setValue(arrIVAZona[0][0]);
        cmbZona.disable();
        dispararEventoSelectCombo('cmbZona');
    }
}

function mostrarVentanaHistorialPrecios(llave,iZ)
{
	
   
    
	var gridHistorialPrecios=crearGridHistorialCostos(bD(llave),bD(iZ));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gridHistorialPrecios
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Historial de precios',
										width: 450,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridHistorialCostos(llave,iZ)
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPrecio'},
		                                                {name: 'precio'},
		                                                {name:'fechaInicio', type:'date', dateFormat:'Y-m-d'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicio', direction: 'DESC'},
                                                            groupField: 'fechaInicio',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='169';
                                        proxy.baseParams.idProducto=gE('id').value;
                                        proxy.baseParams.llave=llave;
                                        proxy.baseParams.idZona=iZ;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Precio Neto de Venta',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'precio',
                                                                css:'text-align:right;',
                                                                renderer:'usMoney'
                                                            },
                                                            {
                                                                header:'Vigencia a partir del',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'fechaInicio',
                                                                css:'text-align:right;',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridHistorialPrecio',
                                                                store:alDatos,
                                                                frame:false,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                height:220,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}


function mostrarGaleria(llave)
{
	
    
	var vistaImagenes=crearVistaImagen(bD(llave));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'anchor',
											items: 	[
                                            			{
                                                            xtype:'panel',
                                                          	anchor:'100% 100%',  
                                                            border:true,
                                                            frame:false,
                                                            tbar:	[
                                                                        {
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar imagen de archivo',
                                                                            handler:function()
                                                                                    {
                                                                                        mostrarVentanaAgregarImagen(bD(llave));
                                                                                    }
                                                                            
                                                                        },'-',
                                                                         {
                                                                            icon:'../images/camera_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar imagen de c&aacute;mara',
                                                                            handler:function()
                                                                                    {
                                                                                    	llaveGaleria=bD(llave);
                                                                                        var obj={};
                                                                                        obj.funcionAceptar=imagenCapturadaAceptada;
                                                                                        var c=new cImagen(obj);
                                                                                        
                                                                                        c.mostarVenanaCtrlImagen();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover imagen',
                                                                            disabled:true,
                                                                            id:'btnDelImagen',
                                                                            handler:function()
                                                                                    {
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                    	vistaImagenes.getStore().reload();
                                                                                                        gEx('btnDelImagen').disable();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=171&idImagen='+imagenSel.data.idProductoImagen,true);
                                                                                                
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover la imagen seleccionada',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                                    ],
                                                            items:	[
                                                                        vistaImagenes
                                                                    ]
                                                    	}
                                                    ]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Galeria de productos',
										width: 650,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function crearVistaImagen(llave)
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                            			{name:'idProductoImagen'},
                                               			{name:'idImagen'},
		                                                {name: 'tamano'}
                                            		],
                                            root:'registros'
                                        }
                                      );
                                      
	var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'idImagen', direction: 'ASC'},
                                              groupField: 'idImagen',
                                              remoteGroup:false,
                                              remoteSort: false,
                                              autoLoad:true
                                              
                                          }) 
                                          
	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnDelImagen').disable();
                                        proxy.baseParams.funcion='170';
                                        proxy.baseParams.idProducto=gE('id').value;
                                        proxy.baseParams.llave=llave;
                                        
                                    }
                        )                                             
     
     
    
                                          
	var dataview = new Ext.DataView({
                                        store: alDatos,
                                       	visible:true,
                                        frame:false,
                                       	region:'center',
                                        border:false,
                                        autoHeight:true,
                                        tpl  : new Ext.XTemplate(
                                                                
                                                                    '<tpl for=".">',
                                                                        '<div class="thumb-wrap" style="width:150px; height:150px"><div class="thumb" >',
                                                                            '<img width="75" height="75" src="../paginasFunciones/obtenerArchivos.php?id={idImagen:bE}" /></div>',
                                                                            '<span style="color: #000; font-size:10px"><b>{tamano:bytesToSize}</b></span>',
                                                                        '</div>',
                                                                    '</tpl>'

                                                            ),
                                        
                                       
                                        id: 'phones',
                                        overClass:'x-view-over',
									    itemSelector: 'div.thumb-wrap',
                                        singleSelect: true,
                                        multiSelect : false,
                                        emptyText : '<div style="padding:10px;">Sin imagenes</div>', 
                                        autoScroll  : true
                                    });
                                    
	dataview.on('click',function(dv,idx,nodo,e)
    							{
                                	imagenSel=dv.getRecord(nodo);
                                    gEx('btnDelImagen').enable();
                                    
                                }
    					)                                     
	
	return dataview;
}

function mostrarVentanaAgregarImagen(llave)
{
	llaveGaleria=llave;
	var tabla='<div><input type="text" id="txtFileName" disabled="true" style="border: solid 1px; background-color: #FFFFFF; width: 250px" /></div><div class="flash" id="fsUploadProgress">'+ 
					'</div><input type="hidden" name="hidFileID" id="hidFileID" value="" /> ';   
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														 {
                                                              xtype:'label',
                                                              x:10,
                                                              y:10,
                                                              html:'Archivo de imagen: <span id="oblComprobante" style="color:#F00">*</span>'
                                                          },
                                                          
                                                         {
                                                              x:120,
                                                              y:5,
                                                              html:	'<table width="290"><tr><td><div id="uploader"><p>Your browser doesn\'t have Flash, Silverlight or HTML5 support.</p></div></td></tr><tr id="filaAvance" style="display:none"><td align="right">Porcentaje de avance: <span id="porcentajeAvance"> 0%</span></td></tr></table>'
                                                          },
                                                         
                                                          {
                                                              x:415,
                                                              y:6,
                                                              id:'btnUploadFile',
                                                              xtype:'button',
                                                              text:'Seleccionar...',
                                                              handler:function()
                                                                      {
                                                                          $('#containerUploader').click();
                                                                      }
                                                          },
                                                          {
                                                              x:185,
                                                              y:10,
                                                              hidden:true,
                                                              html:	'<div id="containerUploader"></div>'
                                                          }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar imagen de producto',
										width: 510,
										height:120,
										layout: 'fit',
										plain:true,
										modal:true,
                                        id:'vAgregarDocumento',
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	var cObj={
                                                                                  // Backend settings
                                                                                  upload_url: "../paginasFunciones/procesarDocumento.php", //lquevedor
                                                                                  file_post_name: "archivoEnvio",
                                                                   
                                                                                  // Flash file settings
                                                                                  file_size_limit : "1 MB",
                                                                                  file_types : "*.gif;*.png;*.jpg",			// or you could use something like: "*.doc;*.wpd;*.pdf",
                                                                                  file_types_description : "Archivos de imagen",
                                                                                  file_upload_limit : 0,
                                                                                  file_queue_limit : 1,
                                                                   
                                                                                  
                                                                                  upload_success_handler : subidaCorrecta
                                                                              };
                                                                              
                                                                              crearControlUploadHTML5(cObj); 
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	if(uploadControl.files.length==0)
                                                                        {
                                                                           msgBox('Debe indicar el archivo de imagen que desea agregar');
                                                                            return;
                                                                        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                                                        uploadControl.start();
                                                                    
                                                                    
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
}

function subidaCorrecta(file, serverData) 
{
	try 
    {
		file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
		
		var arrDatos=serverData.split('|');
		if ( arrDatos[0]!='1') 
		{
			
		} 
		else 
		{
			
            
            function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                   
                    gEx('phones').getStore().reload();
                    gEx('vAgregarDocumento').close();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=172&idComprobante='+arrDatos[1]+'&nombreArchivo='+arrDatos[2]+'&idProducto='+gE('id').value+'&llave='+llaveGaleria,true);

            
			
            
		}
		
	} 
    catch (e) 
	{
		alert(e);
	}
}

function crearGridClaves()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'llave',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                        			{name: 'id'},
                                                                                                                    {name: 'llave'},
                                                                                                                    {name: 'dimension'},
                                                                                                                    {name: 'codigoBarras'},
                                                                                                                    {name: 'codigoAlternativo'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: 'ultimaDimension'},
                                                                                                                    {name: 'permiteGaleria'},
                                                                                                                    {name: '_is_leaf', type: 'bool'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.funcion=173;
                            	proxy.baseParams.idProducto=gE('id').value;
                                
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	gEx('btnModificar').disable();
                        gEx('btnAgregarDescuento').disable();
                    	gEx('arbolCostos').getStore().expandAll();
                        if(gEx('arbolCostos').galeriaGlobal)
                        	gEx('btnGaleriaProducto').show();
                        else
                        	gEx('btnGaleriaProducto').hide();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.EditorGridPanel(	{
                                                        renderTo:'tblCostos',
                                                        enableDD: false,
                                                        border:true,
                                                        autoScroll:true,
                                                        disableSelection:true,
                                                        id:'arbolClaves',
                                                        store: store,
                                                        stripeRows: true,
                                                        columnLines :true,
                                                        loadMask :true,
                                                        width:790,
                                                        height:250,
                                                        clicksToEdit:1,
                                                        tbar:	[
                                                        			{
                                                                        	icon:'../images/application_view_icons.png',
                                                                            cls:'x-btn-text-icon',
                                                                            id:'btnGaleriaProducto',
                                                                            text:'Administrar Galeria de imagenes',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarGaleria('');
                                                                                    }
                                                                            
                                                                        }
                                                        		],
                                                        columns:	[
                                                                        {
                                                                            header: 'Especificaciones del producto',
                                                                            dataIndex: 'dimension',
                                                                            width: 365,
                                                                            renderer:function(v,meta,record)
                                                                            {
                                                                            
                                                                                 return [
                                                                                               '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                              
                                                                                               '<span class="ux-maximgb-tg-mastercol-editorplace">', v, '</span>'
                                                                                            ].join('');
                                                                                
                                                                                
                                                                            }
                                                                        },
                                                                        {
                                                                            header: '',
                                                                            dataIndex: 'permiteGaleria',
                                                                            width: 22,
                                                                            align:'center',
                                                                            renderer:function(val,meta,registro)
                                                                            		{
                                                                                    	var compGaleria='';
                                                                                        if(val=='1')
                                                                                        {
                                                                                        	grid.galeriaGlobal=false;
                                                                                            compGaleria='<a href="javascript:mostrarGaleria(\''+bE(registro.data.llave)+'\')"><img width="13" height="13" src="../images/application_view_icons.png" title="Galeria de imagenes" alt="Galeria de imagenes"></a>';
                                                                                           
                                                                                        }
                                                                                        return compGaleria;
                                                                                    }
                                                                            
                                                                        },
                                                                        
                                                                        
                                                                        {
                                                                            header: 'Codigo de barras',
                                                                            dataIndex: 'codigoBarras',
                                                                            width: 130,
                                                                            css:'text-align:right;',
                                                                            editor:	{xtype:'textfield'},
                                                                            renderer:function(val,meta,registro)
                                                                            		{
                                                                                    	if(registro.data.ultimaDimension=='0')
                                                                                        	return '';
                                                                                        else
                                                                                        	return val;
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                            header: 'Codigo alternativo',
                                                                            dataIndex: 'codigoAlternativo',
                                                                            width: 120,
                                                                            css:'text-align:right;',
                                                                            editor:	{xtype:'textfield'},
                                                                            renderer:function(val,meta,registro)
                                                                            		{
                                                                                    	if(registro.data.ultimaDimension=='0')
                                                                                        	return '';
                                                                                        else
                                                                                        	return val;
                                                                                    }
                                                                            
                                                                        }
                                                                    ]
                                                         
                                                
                                                        
                                                    }
                                              );
                                          
	
    grid.on('beforeedit',function(e)
    					{
                        	if(e.record.data.ultimaDimension=='0')
                            	e.cancel=true;
                        }
    		)
     
     
    grid.on('afteredit',function(e)
    					{
                        	var tipoCodigo=0;
                            if(e.field=='codigoBarras')
                            	tipoCodigo=1;
                            if(e.field=='codigoAlternativo')
                            	tipoCodigo=2;
                        
                        
                        	var cadObj='{"tipoCodigo":"'+tipoCodigo+'","valor":"'+e.value+'","llave":"'+e.record.data.llave+'","idProducto":"'+gE('id').value+'"}';
                        
                        	function funcAjax()
                            {
                                var resp=peticion_http.responseText;
                                arrResp=resp.split('|');
                                if(arrResp[0]=='1')
                                {
                                    
                                }
                                else
                                {
                                	function resp()
                                    {
                                    	e.record.set(e.field,e.originalValue);
                                    }
                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0],resp);
                                }
                            }
                            obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=167&cadObj='+cadObj,true);

                        
                        }
    		)
	grid.galeriaGlobal=true;                                          
	return grid;  
   
}

function imagenCapturadaAceptada(resp)
{
    var arrResp=resp.split('|');
    if(arrResp[0]=='1')
    {
    	function funcAjax2()
        {
            var resp2=peticion_http.responseText;
            var arrRespFinal=resp2.split('|');
            if(arrRespFinal[0]=='1')
            {
            	gEx('vTomarImagen').close();
                gEx('phones').getStore().reload();
                gEx('vAgregarDocumento').close();
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax2, 'POST','funcion=172&idComprobante='+arrResp[1]+'&nombreArchivo='+arrResp[1]+'.png&idProducto='+gE('id').value+'&llave='+llaveGaleria,true);
    }
    else
    {
        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
    }
}


function agregarDescuentoProducto(idProducto,llave,minFechaDescuento)
{
	var fechaActual=Date.parseDate('<?php echo date('Y-m-d')?>','Y-m-d');
    var minFecha=fechaActual;
    if(fechaActual<minFechaDescuento)
    	minFecha=minFechaDescuento;
    var x;
    /*for(x=0;x<arrNodos.length;x++)
    {
    	var idFila=arrNodos[x].getAttribute('idFila');
		var fila=gEx('arbolCostos').getStore().getById(idFila);
        if(fila.data.fechaInicio)
        {
        	
            if((minFecha<fila.data.fechaInicio)&&())
            
            	minFecha=fila.data.fechaInicio;
            
        }
    }*/
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Porcentaje descuento:'
                                                        },
                                                        
                                                        {
                                                        	x:135,
                                                            y:5,
                                                            width:70,
                                                            id:'txtDescuento',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigencia',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:255,
                                                            y:40,
                                                            html:'al:'
                                                        },
                                                        {
                                                        	x:280,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigenciaAl',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n del descuento:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'textarea',
                                                            height:60,
                                                            width:450,
                                                            id:'txtDescripcion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar descuento a producto',
										width: 500,
										height:245,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtDescuento').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var cadObj='';
                                                                        var obj='';
																		var x;
                                                                        var dteVigencia=gEx('dteVigencia');
                                                                        var dteVigenciaAl=gEx('dteVigenciaAl');
                                                                        var txtDescuento=gEx('txtDescuento');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        
                                                                        if(txtDescuento.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado no es v&aacute;lido',resp);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(parseFloat(txtDescuento.getValue())>100)
                                                                        {
                                                                        	function resp10()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado NO puede ser mayor a 100%',resp10);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicial del descuento',resp2);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigenciaAl.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteVigenciaAl.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha t&eacute;mino del descuento',resp3);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()>dteVigenciaAl.getValue())
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio no puede ser mayor que la fecha de t&eacute;rmino',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtDescripcion.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	txtDescripcion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del descuento',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                            
                                                                        obj='{"descripcion":"'+cv(txtDescripcion.getValue())+'","fechaInicio":"'+dteVigencia.getValue().format('Y-m-d')+'","fechaTermino":"'+dteVigenciaAl.getValue().format('Y-m-d')+'","descuento":"'+txtDescuento.getValue()+
                                                                            '","idProducto":"'+idProducto+'","llave":"'+llave+'","tipoDescuento":"0"}';
                                                                        
                                                                        
                                                                        
                                                                        var cObj='{"aRegistros":['+obj+']}'
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridDescuentos').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=185&cadObj='+cObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDescuentosProducto(idProducto,llave)
{
	
       
       var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDescuento'},
		                                                {name: 'porcentajeDescuento'},
		                                                {name:'fechaInicio', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaTermino', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'descripcionDescuento'},
                                                        {name: 'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'situacion'},
                                                        {name: 'motivoCancelacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicio', direction: 'DESC'},
                                                            groupField: 'fechaInicio',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='184';
                                        proxy.baseParams.idProducto=idProducto;
                                        proxy.baseParams.llave=llave;
                                        proxy.baseParams.tipoDescuento=0;
                                        gEx('btnPromoModif').disable();
	                                    gEx('btnPromoDel').disable();
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Porcentaje<br>descuento',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'porcentajeDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return removerCerosDerecha(val)+' %';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha inicio',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaInicio',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha t&eacute;rmino',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaTermino',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n del descuento',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcionDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var comp='';
                                                                            if(registro.data.motivoCancelacion!='')
	                                                                            comp=' <img src="../images/icon_comment.gif" title="'+escaparBR(registro.data.motivoCancelacion)+'" alt="'+escaparBR(registro.data.motivoCancelacion)+'" />';
                                                                        	switch(val)
                                                                            {
                                                                            	case '0':
                                                                                	return 'Inactivo'+ comp;
                                                                                break;
                                                                            	case '1':
                                                                                	return 'Activo'+ comp;
                                                                                break;
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                header:'Registrado el',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s')
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridDescuentos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:350,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar descuento',
                                                                                handler:function()
                                                                                        {
                                                                                        
                                                                                        	var minFecha=fechaActual;
                                                                                            var gridDescuentos=gEx('gridDescuentos');
                                                                                            var ct=0;
                                                                                            var fila;
                                                                                            for(ct=0;ct<gridDescuentos.getStore().getCount();ct++)
                                                                                            {
                                                                                            	fila=gridDescuentos.getStore().getAt(ct);
                                                                                                if(fila.data.situacion=='1')
                                                                                                {
                                                                                                    if(minFecha<fila.data.fechaTermino.add(Date.DAY,1))
                                                                                                        minFecha=fila.data.fechaTermino.add(Date.DAY,1);
                                                                                            	}
                                                                                            }
                                                                                         	agregarDescuentoProducto(idProducto,llave,minFecha);   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnPromoModif',
                                                                                text:'Modificar fecha de t&eacute;rmino',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento cuya fecha de t&eacute;rmino desea modificar');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            mostrarVentanaFinalizarFechaTermino(fila);
                                                                                            
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            ,'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnPromoDel',
                                                                                text:'Remover descuento',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento que desea remover');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            if(fila.data.fechaInicio<=fechaActual)
                                                                                            {
                                                                                            	msgBox('No se puede remover descuentos cuya fecha de vigencia haya comenzado');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            gEx('gridDescuentos').getStore().reload();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=186&idDescuento='+fila.data.idDescuento,true);
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el descuento seleccionado?',resp);
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
                                                        
	 tblGrid.getSelectionModel().on('rowselect',function(sm,numFila,registro)
                                                {
                                                    gEx('btnPromoModif').disable();
                                                    gEx('btnPromoDel').disable();
                                                    if(registro.data.fechaInicio>fechaActual)
                                                    {
                                                        gEx('btnPromoDel').enable();
                                                    
                                                    }
                                                    else
                                                    {
                                                        if(registro.data.fechaTermino>=fechaActual)
                                                            gEx('btnPromoModif').enable();
                                                    }
                                                }
                                    )                                                           
                                                        
        return 	tblGrid;	
	
}

function mostrarVentanaFinalizarFechaTermino(fila)
{
	var fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>',"Y-m-d");
    
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Ingrese la nueva fecha de t&eacute;rmino: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:30,
                                                        	xtype:'label',
                                                            html:'Si la fecha de t&eacute;rmino coincide con la fecha actual o la fecha de inicio del descuento, &eacute;ste inmediatamente ser&aacute; cancelado'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:5,
                                                            id:'dteFechaTermino',
                                                            xtype:'datefield',
                                                            value:fechaActual,
                                                            minValue:fila.data.fechaInicio,
                                                            maxValue:fila.data.fechaTermino
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo del cambio: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:500,
                                                            height:80,
                                                            xtype:'textarea',
                                                            id:'txtMotivo'
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar fecha de t&eacute;rmino',
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dteFechaTermino=gEx('dteFechaTermino');
																		if(dteFechaTermino.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaTermino.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha de t&eacute;rmino del descuento',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                            function resp2()
                                                                            {
                                                                                txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo del cambio de fecha de t&eacute;rmino del descuento',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        function respConf(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gridDescuentos').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=191&idDescuento='+fila.data.idDescuento+'&motivoCancelacion='+
                                                                                cv(txtMotivo.getValue())+'&fechaTermino='+dteFechaTermino.getValue().format("Y-m-d"),true);

                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer modificar la fecha de t&eacute;rmino del descuento?',respConf);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}