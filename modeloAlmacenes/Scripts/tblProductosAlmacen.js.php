<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


function mostrarProducto(val)
{
	return '<img src="../modeloAlmacenes/obtenerImagenProducto.php?iP='+bE(val)+'" width="45" height="60">';
}




function crearGridCostoProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCostoProducto'},
		                                                {name: 'costoTotal'},
		                                                {name:'fechaInicioVigencia', type:'date', dateFormat:'Y-m-d'},
		                                                {name:'fechaFinVigencia', type:'date', dateFormat:'Y-m-d'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicioVigencia', direction: 'DESC'},
                                                            groupField: 'fechaInicioVigencia',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            
                                                            {
                                                                header:'Costo del producto',
                                                                width:150,
                                                                align:'right',
                                                                sortable:true,
                                                                dataIndex:'costoTotal',
                                                                renderer:'usMoney',
                                                                editor:	{
                                                                			xtype:'numberfield',
                                                                            allowNegative:false,
                                                                            allowDecimals:true	
                                                                		}
                                                            },
                                                            {
                                                                header:'Vigencia del costo del',
                                                                width:160,
                                                                sortable:true,
                                                                dataIndex:'fechaInicioVigencia',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'al',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaFinVigencia',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
                                                                        		return val.format('d/m/Y');
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridCostos',
                                                                store:alDatos,
                                                                region:'south',
                                                                frame:false,
                                                                sm:chkRow,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                clicksToEdit:2,
                                                                height:200,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar nuevo costo',
                                                                                handler:function()
                                                                                        {
                                                                                        	var grid_tblTabla=gEx('grid_tblTabla');
                                                                        					var fProducto=grid_tblTabla.getSelectionModel().getSelected();
                                                                                            if(!fProducto)
                                                                                            {
                                                                                            	msgBox('Debe selecionar el producto cuyo costo desea agregar');
                                                                                            	return;
                                                                                            }
                                                                                         	mostrarVentanaNuevoCosto();   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover costo',
                                                                                disabled:true,
                                                                                id:'btnRemoverCosto',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el costo de producto que desea remover');
                                                                                            	return;
 
                                                                                            }
                                                                                            function respDel(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                    function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            gEx('gridCostos').getStore().reload();	 
                                                                                                            gEx('btnRemoverCosto').disable();    
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=149&idCostoProducto='+fila.data.idCostoProducto,true);
                                                                                            	}
                                                                                        	}
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el costo seleccionado?',respDel);
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		
        tblGrid.on('beforeedit',function(e)
        						{
                                	if(e.record.data.fechaInicioVigencia<Date.parseDate('<?php echo date("Y-m-d") ?>','Y-m-d'))
                                    {
                                    	e.cancel=true;
                                    }
                                }
                  )
        tblGrid.on('afteredit',function(e)
        						{
                                	
                                	function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                            
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=148&idCostoProducto='+e.record.data.idCostoProducto+'&costoTotal='+e.value,true);

                                }
                  )    
		tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
        											{
                                                    	
                                                        
                                                         /*gEx('btnRemoverCosto').disable();
                                                    	if((nFila==0) &&(registro.data.fechaInicioVigencia>=Date.parseDate('<?php echo date("Y-m-d") ?>','Y-m-d')))
                                                        {
                                                            gEx('btnRemoverCosto').enable();
                                                        }*/
                                                    }
        								)  
		tblGrid.getSelectionModel().on('rowdeselect',function(sm,nFila,registro)
        											{
                                                    	 //gEx('btnRemoverCosto').disable();
                                                    }
        								)                                                                                                              
        return 	tblGrid;	
}


function productoSel(sm,nFila,registro)
{
	gEx('gridCostos').getStore().load({url: '../paginasFunciones/funcionesAlmacen.php',params:{funcion:147,idProducto:registro.data.idProducto}});
}

function productoUnSel(sm,nFila,registro)
{
	gEx('gridCostos').getStore().removeAll();
}

function mostrarVentanaNuevoCosto()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Costo del producto:'
                                                        },
                                                        {
                                                        	x:170,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false,
                                                            id:'txtCosto',
                                                            width:110
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del costo a partir del:'
                                                        },
                                                        {
                                                        	x:170,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'fInicio',
                                                            value:'<?php echo date("Y-m-d")?>',
                                                            minValue:'<?php echo date("Y-m-d")?>'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar costo de producto',
										width: 370,
										height:170,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCosto').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var txtCosto=gEx('txtCosto');
                                                                        var fInicio=gEx('fInicio');
                                                                        if(txtCosto.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtCosto.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el costo del producto',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(fInicio.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	fInicio.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha a partir de la cual el nuevo costo estar&aacute; vigente',resp2);
                                                                            return;
                                                                        }
                                                                        var grid_tblTabla=gEx('grid_tblTabla');
                                                                        var fProducto=grid_tblTabla.getSelectionModel().getSelected();
                                                                        var cadObj='{"idProducto":"'+fProducto.data.idProducto+'","costo":"'+txtCosto.getValue()+'","fechaVigencia":"'+fInicio.getValue().format('Y-m-d')+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridCostos').getStore().reload();	     
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=150&cadObj='+cadObj,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}
