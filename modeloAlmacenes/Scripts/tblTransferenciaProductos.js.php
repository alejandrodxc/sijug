<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	include_once("latis/cAlmacen.php");
	
	$arrAlmacenesTodos=obtenerTodosAlmacenesDisponiblesJS();
	$arrAlmacenes=obtenerAlmacenesDisponiblesJS();
	
	$arrEmpresas="";	
	$consulta="SELECT idEmpresa,CONCAT(rfc1,'-',rfc2,'-',rfc3) AS rfc,IF(tipoEmpresa=1,CONCAT(apPaterno,' ',apMaterno,' ',razonSocial),razonSocial) AS empresa FROM 6927_empresas ORDER BY  empresa";
	$resEmp=$con->obtenerFilas($consulta);
	while($fEmp=mysql_fetch_row($resEmp))
	{
		$o="['".$fEmp[0]."','[".$fEmp[1]."] ".cv($fEmp[2])."']";
		if($arrEmpresas=="")
			$arrEmpresas=$o;
		else
			$arrEmpresas.=",".$o;
	}
	$arrEmpresas="[".$arrEmpresas."]";
	$consulta="SELECT idTipoComprobante,comprobante FROM 106_tipoComprobante where situacion=1 ORDER BY comprobante";
	$arrCategoriasDisponibles=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT u.idUnidadMedida,IF(abreviatura='' OR abreviatura IS NULL,u.unidadMedida,CONCAT('(',u.abreviatura,') ',u.unidadMedida)) AS unidadMedida 
					FROM 6923_unidadesMedida u order by unidadMedida";
	
	$arrUnidadesMedida=$con->obtenerFilasArreglo($consulta);					
?>

var arrAlmacenesTodos=<?php echo $arrAlmacenesTodos?>;
var arrUnidadesMedida=<?php echo $arrUnidadesMedida?>;
var arrAlmacenesDisp=[];
var arrAlmacenes=[<?php echo $arrAlmacenes?>];
var arrEmpresas=<?php echo $arrEmpresas?>;
var arrCategoriasDisponibles=<?php echo $arrCategoriasDisponibles?>;
var regProductoPedido=null;
var arrSituacion=[['1','Activo'],['2','Cancelado'],['1,2','Cualquier situaci\xF3n']];
Ext.onReady(inicializar);

var arrFormaPago=[['1','Inmediato'],['2','A cr\xE9dito']];

function inicializar()
{
	arrAlmacenesDisp=arrAlmacenes.slice();
    arrAlmacenesDisp.splice(arrAlmacenesDisp.length-1,1);
	regProductoPedido=crearRegistro(	[
    										{name: 'idProducto'},
                                            {name: 'llave'},
                                            {name: 'producto'},
                                            {name: 'costoUnitario'},
                                            {name: 'subtotal'},
                                            {name: 'tasaIVA'},
                                            {name: 'iva'},
                                            {name: 'cantidad'},
                                            {name: 'total'},
                                            {name: 'unidadMedida'},
                                            {name: 'arrUnidadesMedida'}
    									]);
	var cmbAlmacen=crearComboExt('cmbAlmacen',arrAlmacenes,0,0,280);
    cmbAlmacen.setValue(arrAlmacenes[arrAlmacenes.length-1][0]);
    cmbAlmacen.on('select',function()
    						{
                            	obtenerPedidos();
                            }
    			);
    var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion,0,0,300);
    cmbSituacion.on('select',function()
    						{
                            	obtenerPedidos();
                            }
    			);
    cmbSituacion.setValue('1,2');
    
    
    new Ext.Panel({
                    
                    renderTo:'tblPedidos',
                    width:960,
                    height:480,
                    layout:'border',
                    tbar:	[
                                {
                                    xtype:'label',
                                    html:'<span style="color:#000; font-weight:bold">Ver transferencias del almac&eacute;n:&nbsp;&nbsp;</span>'
                                },
                                cmbAlmacen,
                                '-',
                                {
                                    xtype:'label',
                                    html:'<span style="color:#000; font-weight:bold">En situaci&oacute;n:&nbsp;&nbsp;</span>'
                                },
                                cmbSituacion
                                
                            ],
                    items:	[
                                crearGridPedidos()
                            ]
                } 
               )
}

function crearGridPedidos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name:'responsableRegistro'},
                                                        {name:'idAlmacenOrigen'},
                                                        {name:'idAlmacenDestino'},
                                                        {name: 'situacionTransferencia'},
                                                        {name: 'comentariosAdicionales'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'idRegistro', direction: 'ASC'},
                                                            groupField: 'idRegistro',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='210';
                                        proxy.baseParams.idAlmacen=gEx('cmbAlmacen').getValue();
                                        proxy.baseParams.situacion=gEx('cmbSituacion').getValue();
                                        
                                        

                                    }
                        )   
                        
	var tamPagina=100;
        var paginador=	new Ext.PagingToolbar	(
                                                    {
                                                          pageSize: tamPagina,
                                                          store: alDatos,
                                                          displayInfo: true,
                                                          disabled:false
                                                      }
                                                   )                         
                        
       var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[ 	
                                                                            
                                                                            {type: 'int', dataIndex: 'idRegistro'},
                                                                            {type: 'list', dataIndex: 'idAlmacenOrigen', options:arrAlmacenesDisp,phpMode:true},
                                                                            {type: 'list', dataIndex: 'idAlmacenDestino', options:arrAlmacenesTodos,phpMode:true},
                                                                            {type: 'date', dataIndex: 'fechaRegistro'}
                                                                            
                                                                        ]
                                                        }
                                                    );    
                                                    
                                                    
                                                        
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Folio de <br>la transferencia',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'idRegistro',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href=javascript:mostrarVentanaRecepcion(\''+bE(registro.data.idRegistro)+'\')>'+val+'</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s');
                                                                        }
                                                            },
                                                            {
                                                                header:'Almac&eacute;n origen',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'idAlmacenOrigen',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrAlmacenesTodos,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Almac&eacute;n destino',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'idAlmacenDestino',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrAlmacenesTodos,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Registrado por',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'responsableRegistro',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Comentarios adicionales',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'comentariosAdicionales',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n de la transferencia',
                                                                width:180,
                                                                
                                                                sortable:true,
                                                                dataIndex:'situacionTransferencia',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSituacion,val);
                                                                        }
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridPedidos',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                border:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[filters],
                                                                bbar:[paginador],
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Registrar transferencia',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaRegistro();
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            
                                                                           
                                                                            
                                                                            
                                                                            
                                                                            {
                                                                                icon:'../images/cross.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnCancelacion',
                                                                                text:'Cancelar transferencia',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=gEx('gridPedidos').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la compra que desea cancelar');
                                                                                                return;
                                                                                            }
                                                                                            
                                                                                            mostrarVentanaCancelacionCompra(fila);
                                                                                        

                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
		tblGrid.getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesAlmacen.php',
                                        params:	{
                                        			funcion:210,
                                                    idAlmacen:gEx('cmbAlmacen').getValue(),
                                                    situacion:gEx('cmbSituacion').getValue(),
                                                    limit:tamPagina,
                                                    start:0
                                                    
                                        		}
                                    }
    							);  
		tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
        											{
                                                    
                                                    	
                                                    	
                                                    	if(registro.data.situacionTransferencia=='1')
                                                        {
                                                        	
                                                            gEx('btnCancelacion').enable();
                                                        }
                                                        else
                                                        {
                                                        	
                                                            gEx('btnCancelacion').disable();
                                                        }
                                                    }
        							)
		tblGrid.getSelectionModel().on('rowdeselect',function(sm,nFila,registro)
        											{
                                                    
                                                    	
                                                    	
                                                    	
                                                    }
        							)                                                                                                                       
        return 	tblGrid;
}

function obtenerPedidos()
{
	var gridPedidos=gEx('gridPedidos');
    var lastOptions = gridPedidos.getStore().lastOptions;
    Ext.apply(lastOptions.params, {
                                        idAlmacen: gEx('cmbAlmacen').getValue()
                                    }
              );
	Ext.apply(lastOptions.params, {
                                        situacion: gEx('cmbSituacion').getValue()
                                    }
              );
    
    
    gridPedidos.getStore().reload();
    
}

function mostrarVentanaRegistro()
{
	
    
    
	var cmbAlmacenPedido=crearComboExt('cmbAlmacenPedido',arrAlmacenesDisp,170,5,350);
    cmbAlmacenPedido.setValue(arrAlmacenesDisp[0][0]);
    
    var cmbAlmacenDestinoProducto=crearComboExt('cmbAlmacenDestinoProducto',arrAlmacenesDisp,170,35,350);
    
    
    var gridProductos=crearGridProductosTransferencia();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Almac&eacute;n origen del producto:'
                                                        },
                                                        cmbAlmacenPedido,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Almac&eacute;n destino del producto:'
                                                        },
                                                        cmbAlmacenDestinoProducto,
                                                        {
                                                        	x:590,
                                                            y:10,
                                                            html:'Fecha de la transferencia:'
                                                        },
                                                        {
                                                        	x:740,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaTransferencia',
                                                            value:'<?php echo date("Y-m-d")?>'
                                                        },
                                                        
                                                       
                                                         
                                                        gridProductos,
                                                        
                                                        {
                                                        	x:10,
                                                            y:345,
                                                            xtype:'label',
                                                            html:'Comentarios adicionales:'
                                                        },
                                                        {
                                                        	x:155,
                                                            y:345,
                                                            xtype:'textarea',
                                                            width:720,
                                                            height:60,
                                                            id:'comentarios'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registrar transferencia de producto',
										width: 960,
										height:500,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                        gEx('cmbAlmacenDestinoProducto').focus(false,500);
                                                                    }
                                                                }
													},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                        	handler: function()
																	{
																		var dteFechaTransferencia=gEx('dteFechaTransferencia');	
                                                                        
                                                                        if(cmbAlmacenPedido.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbAlmacenPedido.focus();
                                                                            }
                                                                            msgBox('Debe especificar el almac&eacute;n origen de los productos a transferir',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbAlmacenDestinoProducto.getValue()=='')
                                                                        {
                                                                        	function resp22()
                                                                            {
                                                                            	cmbAlmacenDestinoProducto.focus();
                                                                            }
                                                                            msgBox('Debe especificar el almac&eacute;n destino de los productos a transferir',resp22);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbAlmacenDestinoProducto.getValue()==cmbAlmacenPedido.getValue())
                                                                        {
                                                                        	function resp23()
                                                                            {
                                                                            	cmbAlmacenDestinoProducto.focus();
                                                                            }
                                                                            msgBox('El almac&eacute;n origen NO puede ser igual al almac&eacute;n destino',resp23);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(dteFechaTransferencia.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaTransferencia.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha de la transferencia',resp3);
                                                                            return;
                                                                        }                                                   
                                                                        
                                                                                                                                          
                                                                        
                                                                       
                                                                        var gProductosPedidos=gEx('gProductosPedidos');
                                                                        var fila;
                                                                        var x;
                                                                        var arrProductos='';
                                                                        var aux='';
                                                                        
                                                                        
                                                                        for(x=0;x<gProductosPedidos.getStore().getCount();x++)
                                                                        {
                                                                        	var fila=gProductosPedidos.getStore().getAt(x);
                                                                            aux='{"idUnidadMedida":"'+fila.data.unidadMedida+'","llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+
                                                                            	'","cantidad":"'+fila.data.cantidad+'"}';
                                                                            
                                                                            if(arrProductos=='')
                                                                            	arrProductos=aux;
                                                                            else
                                                                            	arrProductos+=','+aux
                                                                            
                                                                        }
                                                                        if(arrProductos=='')
                                                                        {
                                                                        	msgBox('Almenos debe agregar un producto a transferir');
                                                                        	return;
                                                                        }
                                                                      
                                                                        var cadObj='{"comentarios":"'+cv(gEx('comentarios').getValue())+'","arrProductos":['+arrProductos+'],"idAlmacenOrigen":"'+cmbAlmacenPedido.getValue()+
                                                                        			'","idAlmacenDestino":"'+cmbAlmacenDestinoProducto.getValue()+'","fechaTransferencia":"'+dteFechaTransferencia.getValue().format('Y-m-d')+'"}'
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            
                                                                            	var arrErrores=eval(arrResp[1]);
                                                                            	if(arrErrores.length==0)
                                                                                {
                                                                                    function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                            gEx('gridPedidos').getStore().reload();
                                                                                            var arrParam=[['idPedido',arrResp[1]]];
                                                                                            //enviarFormularioDatos('../reportes/Almacen/impresionPedido.php',arrParam,'POST','iImprimir');
                                                                                            
                                                                                            ventanaAM.close();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=211&cadObj='+cadObj,true);
                                                                               	}
                                                                                else
                                                                                {
                                                                                	var x;
                                                                                    var fila;	
                                                                                    var filaAux;
                                                                                    var xAux;
                                                                                    for(x=0;x<arrErrores.length;x++)
                                                                                    {
                                                                                    	fila=arrErrores[x];
                                                                                        var pos=-1;
                                                                                        for(xAux=0;xAux<gridProductos.getStore().getCount();xAux++)
                                                                                        {
                                                                                        	filaAux=gridProductos.getStore().getAt(xAux);
                                                                                            if((fila.idProducto==filaAux.data.idProducto)&&(fila.llave==filaAux.data.llave))
                                                                                            {
                                                                                            	filaAux.set('sinExistencia',1);
                                                                                            }                                                                                            
                                                                                            
                                                                                        }                                                             
                                                                                        
                                                                                        
                                                                                    }
                                                                                    
                                                                                    msgBox('No se ha podido llevar acabo la operaci&oacute;n debido a que la existencia de algunos productos NO es suficiente para cubrir la transferencia')
                                                                                    
                                                                                
                                                                                } 
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=212&cadObj='+cadObj,true);
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridProductosTransferencia()
{
	var cmbUnidadMedida=crearComboExt('cmbUnidadMedida',[]);
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'unidadMedida'},
                                                                    {name: 'arrUnidadesMedida'},
                                                                    {name: 'existencia'},
                                                                    {name: 'sinExistencia'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Producto',
															width:300,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(registro.data.sinExistencia==1)
                                                                    		return '<img width="13" height="13" src="../images/exclamation.png" title="La existencia del producto NO es suficiente para cubrir la transferencia" alt="La existencia del producto NO es suficiente para cubir la transferencia"> '+mostrarValorDescripcion(val);
                                                                     	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Existencia',
															width:310,
															sortable:true,
															dataIndex:'existencia',
                                                            renderer:mostrarValorDescripcion
														},
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'cantidad',
                                                            editor:{xtype:'numberfield',allowDecimals:true,allowNegative:false},
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000.00');
                                                                    }
                                                                   
														},
                                                        {
															header:'Unidad de medida',
															width:150,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'unidadMedida',
                                                            editor:cmbUnidadMedida,
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(registro.data.arrUnidadesMedida,val));
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:75,
                                                            x:10,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidos',
                                                            columnLines : true,
                                                            height:240,
                                                            width:915,
                                                            sm:chkRow,
                                                            plugins:[summary],
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	if(gEx('cmbAlmacenPedido').getValue()=='')
                                                                                        {
                                                                                        	function resp()
                                                                                            {
                                                                                            	gEx('cmbAlmacenPedido').focus();
                                                                                            }
                                                                                        	msgBox('Debe indicar el almac&eacute;n al cual pertenecer&aacute; el pedido',resp);
                                                                                        	return;
                                                                                        }
                                                                                        buscarPorProductoNombre(1);

                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover producto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el producto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	tblGrid.getStore().remove(filas);
                                                                                            if(tblGrid.getStore().getCount()==0)
	                                                                                            gEx('cmbAlmacenPedido').enable();
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el producto seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
	tblGrid.on('beforeedit',function(e)
    						{
                            	gEx('cmbUnidadMedida').getStore().loadData(e.record.data.arrUnidadesMedida);
                            }
              )                                                    
                                                    
	
    			                                                
	return 	tblGrid;		
}

function mostrarVentanaRecepcion(idRegistro)
{
	var pos=obtenerPosFila(gEx('gridPedidos').getStore(),'idRegistro',bD(idRegistro));
	var fila=gEx('gridPedidos').getStore().getAt(pos);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                            x:0,
                                                            y:0,
                                                            width:960,
                                                            height:130,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Datos de recepción',
                                                            items:	[
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Folio del pedido:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:10,
                                                                            html:'<span class="letraRojaSubrayada8"><b>'+fila.data.idRegistro+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Almac&eacute;n origen:',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                        {
                                                                            x:140,
                                                                            y:40,
                                                                            html:'<span class="letraRojaSubrayada8"><b>'+formatearValorRenderer(arrAlmacenesTodos,fila.data.idAlmacenOrigen)+'</b></span>',
                                                                            xtype:'label'
                                                                            
                                                                        },
                                                                       {
                                                                          x:10,
                                                                          y:70,
                                                                          html:'Almac&eacute;n destino:',
                                                                          xtype:'label'
                                                                          
                                                                      },
                                                                      {
                                                                          x:140,
                                                                          y:70,
                                                                          html:'<span class="letraRojaSubrayada8"><b>'+formatearValorRenderer(arrAlmacenesTodos,fila.data.idAlmacenDestino)+'</b></span>',
                                                                          xtype:'label'
                                                                          
                                                                      }
                                                                       
                                                                        
                                                                        
                                                                    ]
                                                        },
                                                       {
                                                            x:0,
                                                            y:140,
                                                            width:960,
                                                            height:300,
                                                            xtype:'fieldset',
                                                            title:'Datos de la transferencia',
                                                            layout:'absolute',
                                                            items:	[
                                                                        crearGridProductosPedidoRecepcion(),
                                                                        {
                                                                            x:10,
                                                                            y:200,
                                                                            xtype:'label',
                                                                            html:'Comentarios de la transferencia:'
                                                                        },
                                                                        {
                                                                            x:190,
                                                                            y:200,
                                                                            xtype:'textarea',
                                                                            width:700,
                                                                            height:60,
                                                                            readOnly:true,
                                                                            id:'comentariosAdicionales'
                                                                        }
                                                                    ]
                                                        }
                                                   ]
                                                                   
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vComprobacionFiscal',
										title: 'Recepci&oacute;n de pedidos',
										width: 990,
										height:560,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('comentarios').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	ventanaAM.close();
                                                                        
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
	
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj=eval('['+arrResp[1]+']')[0];
            
            gEx('comentariosAdicionales').setValue(escaparBR(obj.comentarios));
            gEx('gProductosPedidosRecepcion').getStore().loadData(obj.arrProductos);
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=214&idTransferencia='+fila.data.idRegistro,true);

            
}

function crearGridProductosPedidoRecepcion()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idProducto'},
                                                                    {name: 'llave'},
                                                                    {name: 'producto'},
                                                                    
                                                                    {name: 'cantidad'},
                                                                    
                                                                    {name: 'unidadMedida'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var summary = new Ext.ux.grid.GridSummary(); 
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Producto',
															width:300,
															sortable:true,
															dataIndex:'producto',
                                                            renderer:mostrarValorDescripcion
														},
														
                                                        {
															header:'Cantidad',
															width:70,
                                                            align:'right',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000.00');
                                                                    },
															sortable:true,
															dataIndex:'cantidad'
														},
                                                         {
															header:'Unidad de medida',
															width:150,
                                                            align:'center',
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'unidadMedida',
                                                            
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrUnidadesMedida,val));
                                                                    }
                                                        }
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            cm: cModelo,
                                                            clicksToEdit:1,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gProductosPedidosRecepcion',
                                                            columnLines : true,
                                                            height:190,
                                                            width:990,
                                                           
                                                            plugins:[summary]
                                                            
                                                        }
                                                    );
	                                            
	                                                  
	return 	tblGrid;
}

function subidaCorrecta(file, serverData) 
{
	try 
    {
		file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
		var progress = new FileProgress(file, this.customSettings.progress_target);
		progress.setComplete();
		progress.setStatus("Completado");
		progress.toggleCancel(false);
		var arrDatos=serverData.split('|');
		if ( arrDatos[0]!='1') 
		{
			this.customSettings.upload_successful = false;
		} 
		else 
		{
			gEx("idArchivo").setValue(arrDatos[1]);
            gEx("nombreArchivo").setValue(arrDatos[2]);
            gEx('lblAvance').hide();
            guardarDocumento();
            
			this.customSettings.upload_successful = true;
            
		}
		
	} 
    catch (e) 
	{
		alert(e);
	}
}

function setTipoComprobante(tComprobante,lCampos)
{
	tipoComprobante=tComprobante;
    gEx('datosBoleto').hide();
    
    
    gEx('txtOrigen').setValue('');
    gEx('txtDestino').setValue('');
    gEx('cmbTipoViaje').setValue(0);
    gEx('txtFechaSalida').setValue('');
    gEx('cmbHoraSalida').setValue('00');
    gEx('cmbMinutoSalida').setValue('00');
    gEx('txtFechaRegreso').setValue('');
    gEx('cmbHoraRegreso').setValue('00');
    gEx('cmbMinutoRegreso').setValue('00');
    
    
    gEx('datosFactura').show();
    
	gE('oblFolio').innerHTML='*';
	var cmbIVAConsidera=gEx('cmbIVAConsidera');
    switch(tipoComprobante)
    {
    	case '5':
        case '1':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '6':
        case '2':
        		mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.enable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '3':
        		mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
        break;
        case '4':
	        	mostrarCampos();
                gEx('noAprobacion').disable();
                gEx('txtNoSerie').disable();
                gE('oblFolio').innerHTML='';
                gE('oblNumAprobacion').innerHTML='';
                gE('oblAnioAprobacion').innerHTML='';
                gEx('anioAprobacion').disable();
               
                gEx('datosFactura').hide();
                gEx('datosBoleto').show();
                
                cmbIVAConsidera.disable();
                if(lCampos)
	                limpiarCampos();
                
        break; 
        case '7':
                ocultarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gEx('lblFolioFiscal').show();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
        break;
        case '8':
                mostrarCampos();
                gEx('noFolio').enable();
                gEx('noAprobacion').enable();
                gEx('txtNoSerie').enable();
                gE('oblFolio').innerHTML='*';
                gE('oblNumAprobacion').innerHTML='*';
                gE('oblAnioAprobacion').innerHTML='*';
                gEx('anioAprobacion').enable();
                cmbIVAConsidera.enable();
                if(lCampos)
                    limpiarCampos();
            break;           
    }
}

function ocultarCampos()
{
	gEx('lblFolio').hide();
    gEx('noFolio').hide();
    gEx('lblSerie').hide();
    gEx('lblNumAprobacion').hide();
    gEx('lblAnioAp').hide();
    gEx('noAprobacion').hide();
    gEx('txtNoSerie').hide();
    gEx('anioAprobacion').hide();
    gEx('lblFolioFiscal').show();
    gEx('lblSep1').show();
    gEx('folio1').show();
    gEx('lblSep2').show();
    gEx('folio2').show();
    gEx('lblSep3').show();
    gEx('folio3').show();
    gEx('lblSep4').show();
    gEx('folio4').show();
    gEx('folio5').show();
}

function mostrarCampos()
{
	gEx('lblFolio').show();
    gEx('noFolio').show();
    gEx('lblSerie').show();
    gEx('lblNumAprobacion').show();
    gEx('lblAnioAp').show();
    gEx('noAprobacion').show();
    gEx('txtNoSerie').show();
    gEx('anioAprobacion').show();
    gEx('lblFolioFiscal').hide();
    
    gEx('lblSep1').hide();
    gEx('folio1').hide();
    gEx('lblSep2').hide();
    gEx('folio2').hide();
    gEx('lblSep3').hide();
    gEx('folio3').hide();
    gEx('lblSep4').hide();
    gEx('folio4').hide();
    gEx('folio5').hide();
}

function guardarDocumento()
{
	
    var x;
    var arrConceptos='';
    var grid;
    var montoComprobacion=0;
    var fila;
    var obj;
    
    var gProductosPedidos=gEx('gProductosPedidosRecepcion');
    var fila;
    var x;
    var arrProductos='';
    var aux='';
    var total=0;
    var subtotal=0;
    var iva=0;
    
    for(x=0;x<gProductosPedidos.getStore().getCount();x++)
    {
        var fila=gProductosPedidos.getStore().getAt(x);
        aux='{"idUnidadMedida":"'+fila.data.unidadMedida+'","tasaIVA":"'+fila.data.tasaIVA+'","llave":"'+fila.data.llave+'","idProducto":"'+fila.data.idProducto+'","costoUnitario":"'+fila.data.costoUnitario+
            '","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+'","iva":"'+fila.data.iva+'","total":"'+
            fila.data.total+'"}';
        total+=parseFloat(fila.data.total);
        subtotal+=parseFloat(fila.data.subtotal);
        iva+=parseFloat(fila.data.iva);
        if(arrProductos=='')
            arrProductos=aux;
        else
            arrProductos+=','+aux
        
    }
    
    
  
	
    var fila=gEx('gridPedidos').getSelectionModel().getSelected();
    
    var fechaRecepcionPedido='';
    if(gEx('dteFechaRecepcion').getValue()!='')
	    fechaRecepcionPedido=gEx('dteFechaRecepcion').getValue().format('Y-m-d');
        
   	var cadObj='{"idPedido":"","arrProductos":['+arrProductos+'],"fechaRecepcionPedido":"'+fechaRecepcionPedido+'","comentariosAdicionales":"'+cv(gEx('comentarios').getValue())+'"}';
	         
	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            gEx('vComprobacionFiscal').close();
            gEx('gridPedidos').getStore().reload();
            
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=154&cadObj='+cadObj,true);         
    
	
    
}

function limpiarCampos()
{
	
    gEx('noFolio').setValue('');
    gEx('noAprobacion').setValue('');
    gEx('txtNoSerie').setValue('');
    gEx('folio1').setValue('');
    gEx('folio2').setValue('');
    gEx('folio3').setValue('');
    gEx('folio4').setValue('');
    gEx('folio5').setValue('');
}

function registrarActualizacionPedido()
{
	cerrarVentanaFancy();
    gEx('gridPedidos').getStore().reload();
    gEx('btnCancelacion').disable();
}

function descargarFactura(iF)
{
	document.location.href='../paginasFunciones/obtenerArchivos.php?id='+iF;
}

function buscarPorProductoNombre(idZona)
{
	var regProducto=null;
	var arrCriterio=[['1','Comienza con...'],['2','Contiene la palabra...'],['3','C\xF3digo de barras'],['4','C\xF3digo alterno']];
	var cmbCriterioBusqueda=crearComboExt('cmbCriterioBusqueda',arrCriterio,170,5,250);
    cmbCriterioBusqueda.setValue('2');
    cmbCriterioBusqueda.on('select',function(cmb,registro)
    								{
                                    	gEx('txtNombre').focus(false,500);
                                        gEx('txtNombre').setValue('');
                                    	cargarProductosBusqueda(1);
                                        
                                        switch(registro.data.id)
                                        {
                                        	case '1':
                                            case '2':
                                            	gEx('lblDescripcion').setText('<b>Descripci&oacute;n del producto:</b>',false);
                                            break;
                                            case '3':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo de barras:</b>',false);
                                            break;
                                            case '4':
                                            	gEx('lblDescripcion').setText('<b>C&oacute;digo alterno:</b>',false);
                                            break;
                                        }
                                        
                                    
                                    }
    						);
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
    
    
    
    var oConf=	{
    					idCombo:'cmbCodigoAlterno',
                        anchoCombo:200,
                        campoDesplegar:'codigoAlterno',
                        campoID:'llaveProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:5,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'},
                                    {name: 'llaveProducto'},
                                    {name: 'idUnidadMedida'},
                                    {name: 'arrUnidadesMedida'}
                                    
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    
                                    	regProducto=null;
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbCodigoAlterno').getRawValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        dSet.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        
                                        
                                        
                                        gEx('cmbDescripcion').setValue(''); 
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	regProducto=registro;
                                       	gEx('cmbDescripcion').setRawValue(registro.data.descripcion); 
                                        
                                        
                                        
                                        var r=new  regProductoPedido	(
                                                                            {
                                                                                idProducto:regProducto.data.idProducto,
                                                                                llave:regProducto.data.llave,
                                                                                producto:regProducto.data.descripcion,
                                                                                cantidad:1,
                                                                                unidadMedida:registro.data.idUnidadMedida,
                                                                                arrUnidadesMedida:registro.data.arrUnidadesMedida,
                                                                                existencia:'',
                                                                                sinExistencia:0
                                                                            }
                                                                        )
                                        if(!existeProducto(r))
                                        {                                
                                            obtenerExistenciaProducto(r);                                
                                            gEx('gProductosPedidos').getStore().add(r);
                                            gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                        }
                                       	gEx('vBuscarDescripcion').close();
                                        
                                    }  
    				};

    
	var cmbCodigoAlterno=crearComboExtAutocompletar(oConf);
    
     var oConf=	{
    					idCombo:'cmbDescripcion',
                        anchoCombo:330,
                        campoDesplegar:'descripcion',
                        campoID:'llaveProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'},
                                    {name: 'llaveProducto'},
                                    {name: 'idUnidadMedida'},
                                    {name: 'arrUnidadesMedida'}
                                    
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    	regProducto=null;
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbDescripcion').getRawValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        dSet.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        gEx('cmbCodigoAlterno').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    

                                    	regProducto=registro;
                                        gEx('cmbCodigoAlterno').setRawValue(registro.data.codigoAlterno); 
                                        
                                        
                                         var r=new  regProductoPedido	(
                                                                            {
                                                                                idProducto:regProducto.data.idProducto,
                                                                                llave:regProducto.data.llave,
                                                                                producto:regProducto.data.descripcion,
                                                                                cantidad:1,
                                                                                unidadMedida:registro.data.idUnidadMedida,
                                                                                arrUnidadesMedida:registro.data.arrUnidadesMedida,
                                                                                existencia:'',
                                                                                sinExistencia:0
                                                                            }
                                                                        )
                                        
                                        if(!existeProducto(r))
                                        { 
                                            obtenerExistenciaProducto(r);        
                                                                           
                                            gEx('gProductosPedidos').getStore().add(r);
                                            
                                            gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
										}                                       	
                                        gEx('vBuscarDescripcion').close();
                                       
                                        
                                       	
                                    }  
    				};

    
	var cmbDescripcion=crearComboExtAutocompletar(oConf);
    
    
    
    
 
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>C&oacute;digo alterno:</b>'
                                                                        },
                                                                        cmbCodigoAlterno,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            id:'lblDescripcion',
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        cmbDescripcion
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:440,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbCodigoAlterno').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var gProductosBuscados=gEx('gProductosBuscados');
                                                                        var fila=gProductosBuscados.getSelectionModel().getSelected();
                                                                        if(!fila)
                                                                        {
                                                                        	msgBox('Debe seleccionar el producto que desea agregar');
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                    
                                                                        
                                                                        var r=new  regProductoPedido	(
                                                                        									{
                                                                                                            	idProducto:fila.data.idProducto,
                                                                                                                llave:fila.data.llave,
                                                                                                                producto:fila.data.nombreProducto,
                                                                                                                cantidad:1,
                                                                                                                unidadMedida:fila.data.idUnidadMedida,
                                                                                								arrUnidadesMedida:fila.data.arrUnidadesMedida,
                                                                                                                existencia:'',
                                                                                                                sinExistencia:0
                                                                                                            }
                                                                        								)
                                                                         if(!existeProducto(r))
                                      									  { 
                                                                                obtenerExistenciaProducto(r);        
                                                                                
                                                                                gEx('gProductosPedidos').getStore().add(r);
                                                                                gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                                                               
                                                                                funcionEjecucionBusqueda=function()
                                                                                                        {
                                                                                                            gEx('txtClave').setValue('');
                                                                                                        }
                                                                       	}
                                                                        
                                                                         
                                                                         ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    //cargarProductosBusqueda();
      
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	proxy.baseParams.idAlmacen=gEx('cmbAlmacenPedido').getValue();
                                proxy.baseParams.funcion=158;
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            {
                                                                            	var checado='';
                                                                        		
                                                                                return [
                                                                                           
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda(1);
}

function cargarProductosBusqueda(idZona)
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:gEx('cmbCriterioBusqueda').getValue(),
                                                            valor:'',
                                                            idZona:idZona
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'tasaIVA'},
                                                        {name: 'precioUnitario'},
                                                        {name: 'codigoBarras'},
                                                        {name: 'codigoAlterno'},
                                                        {name: 'unidadMedida'},
                                                        {name: 'arrUnidadesMedida'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo de barras',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoBarras',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo alterno',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoAlterno',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true, 
                                                                listeners:	{
                                                                				rowdblclick:function(grid,nFila)
                                                                                            {
                                                                                                var fila=grid.getStore().getAt(nFila);
                                                                                                console.log(fila);
                                                                                                var r=new  regProductoPedido	(
                                                                                                                                    {
                                                                                                                                        idProducto:fila.data.idProducto,
                                                                                                                                        llave:fila.data.llave,
                                                                                                                                        producto:fila.data.nombreProducto,
                                                                                                                                        cantidad:1,
                                                                                                                                        unidadMedida:fila.data.unidadMedida,
                                                                                                                                        arrUnidadesMedida:fila.data.arrUnidadesMedida,
                                                                                                                                        existencia:'',
                                                                                                                                        sinExistencia:0
                                                                                                                                    }
                                                                                                                                )
                                                                                                if(!existeProducto(r))
                                        														{ 
                                                                                                    obtenerExistenciaProducto(r);        
                                                                                                    gEx('gProductosPedidos').getStore().add(r);
                                                                                                    gEx('gProductosPedidos').startEditing(gEx('gProductosPedidos').getStore().getCount()-1,3);
                                                                                               	}
                                                                                               	gEx('vBuscarDescripcion').close();
                                                                                            }
                                                                			} ,                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}

function mostrarVentanaCancelacionCompra(fila)
{

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            html:'Ingrese el motivo de la cancelaci&oacute;n de la transferencia:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            id:'txtMotivo',
                                                            xtype:'textarea',
                                                            width:580,
                                                            height:80
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar compra',
										width: 630,
										height:230,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    
                                                                    	if(gEx('txtMotivo').getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	gEx('txtMotivo').focus();
                                                                            }
                                                                        	msgBox('Debe ingresar el motivo de la cancelaci&oacute;n de la compra',resp);
                                                                        	return;
                                                                        }
                                                                    
                                                                    
																		function resp(btn)
                                                                        {
                                                                            if(btn=='yes')
                                                                            {
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('gridPedidos').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=213&cA='+cv(gEx('txtMotivo').getValue())+'&idTransferencia='+fila.data.idRegistro,true);
                                                                            }
                                                                            
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar la transferencia seleccionada?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	


	
}

function obtenerExistenciaProducto(fila)
{
	function funcAjax(peticion_http)
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
         	fila.set('existencia',arrResp[1]);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWebV2('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=180&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave+'&idAlmacen='+gEx('cmbAlmacenPedido').getValue(),true);
    
    
    
}

function existeProducto(fila)
{
	var encontrado=false;
	var gProductosPedidos=gEx('gProductosPedidos')	;
    var filaAux;
    var x;
    for(x=0;x<gProductosPedidos.getStore().getCount();x++)
    {
    	filaAux=gProductosPedidos.getStore().getAt(x);
        
        if((filaAux.data.idProducto==fila.data.idProducto)&&(filaAux.data.llave==fila.data.llave))
        {
        	encontrado=true;
        }
        
        
    }
    
    
    return encontrado;
    
}