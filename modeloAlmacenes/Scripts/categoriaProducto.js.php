<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select idProceso,nombre from 4001_procesos order by nombre";
	$arrProcesos=($con->obtenerFilasArreglo($consulta));
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
?>
var fechaActual;
var regSeccion=null;
var regDimension=null;
var arrProcesos=<?php echo $arrProcesos?>;
var arrSiNo=<?php echo $arrSiNo?>;
Ext.onReady(inicializar);

function inicializar()
{
	fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>','Y-m-d');
	regSeccion=crearRegistro	([ {name: 'idFormulario'},{name: 'nombreFormulario'},{name: 'proceso'},{name: 'orden'}]);
    regDimension=crearRegistro(	
    							[
                                	{name: 'idDimension'},
                                    {name: 'dimension'},
                                    {name: 'etiqueta'},
                                    {name: 'orden'}
    							]
                               );
	var _categoriavch=gE('_nombreCategoriavch');
    if(_categoriavch!=null)
    	_categoriavch.focus();
	crearGridSecciones(); 
    var tabs = $( "#tabs" ).tabs(
    								{
                                        activate: function( event, ui ) 
                                                {
                                                        
    
                                                        switch(ui.newTab.context.innerHTML)
                                                        {
                                                            case 'Administrar descuentos':
                                                                gEx('gridDescuentosCategoria').getView().refresh();
                                                            break;
                                                            case 'Dimensionamiento del producto':
                                                            
                                                                gEx('arbolDimensiones').getView().refresh();
                                                            break;
                                                            
                                                            
                                                        }
                                                  }
                                	}
    							);
    crearGridDimensiones();      
    dimensionamientoChange(gE('_dimensionarint')); 
    crearGridDescuentosProducto();
   
   
}

function validarFrm()
{
	var cadSecciones='';
    var tSecciones=gEx('tSecciones');
    var x;
    var fila;
    var o='';
    var id=gE('idCategoria').value;
    
    
    var arbolDimensiones=gEx('arbolDimensiones');
    var cadDimensiones='';
    
    
    var _dimensionarint=gE('_dimensionarint');
    var dimensionar=_dimensionarint.options[_dimensionarint.selectedIndex].value;
    if(dimensionar=='1')
    {
        for(x=0;x<arbolDimensiones.getStore().getCount();x++)
        {
            fila=arbolDimensiones.getStore().getAt(x);
            
            o='{"idDimension":"'+fila.get('idDimension')+'","orden":"'+fila.get('orden')+'","etiqueta":"'+cv(fila.get('etiqueta'))+'","permiteGaleria":"'+fila.get('permiteGaleria')+'"}';
            if(cadDimensiones=='')
                cadDimensiones=o;
            else        
                cadDimensiones+=','+o;
            
        }
   	} 
    
    for(x=0;x<tSecciones.getStore().getCount();x++)
    {
    	fila=tSecciones.getStore().getAt(x)
        o='{"idFormulario":"'+fila.get('idFormulario')+'","orden":"'+fila.get('orden')+'"}';
        if(cadSecciones=='')
        	cadSecciones=o;
        else
        	cadSecciones+=','+o;
    }
    cadSecciones='{"arrSecciones":['+cadSecciones+'],"arrDimensiones":['+cadDimensiones+']}';
   
    if(id=='-1')
    {
        gE('funcPHPEjecutarNuevo').value=bE('asociarSeccionesProducto(@idRegPadre,\''+bE(cadSecciones)+'\',1)');
    }
    else
    {
        gE('funcPHPEjecutarModif').value=bE('asociarSeccionesProducto('+id+',\''+bE(cadSecciones)+'\',0)');
    }
    
	if(validarFormularios('frmEnvio'))
    	gE('frmEnvio').submit();
}

function crearGridSecciones()
{
	var dsDatos=eval((gE('arrSecciones').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idFormulario'},
                                                                    {name: 'nombreFormulario'},
                                                                    {name: 'proceso'},
                                                                    {name: 'orden',type:'int'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Nombre del formulario',
															width:220,
															sortable:true,
															dataIndex:'nombreFormulario',
                                                            renderer:function(val)
                                                                    {
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Proceso',
															width:200,
															sortable:true,
															dataIndex:'proceso',
                                                            renderer:function(val)
                                                                    {
                                                                    	return mostrarValorDescripcion(val);
                                                                    }
														},
														{
															header:'Orden',
															width:110,
															sortable:true,
															dataIndex:'orden'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            id:'tSecciones',
                                                            renderTo:'tblSecciones',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:130,
                                                            width:635,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar secci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaFormulariosDinamicos();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover secci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos una secci&oacute;n a remover');
                                                                                        	return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                                var x;
                                                                                                for(x=0;x<tblGrid.getStore().getCount();x++)
                                                                                                {
                                                                                                	tblGrid.getStore().getAt(x).set('orden',(x+1));
                                                                                                }
                                                                                            }
                                                                                            
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover las secciones seleccionadas?',resp);
                                                                                    }
                                                                            
                                                                        },
                                                                        '-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;<b>Modificar orden de secci&oacute;n:&nbsp;&nbsp;</b>'
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignUp.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la secci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.orden>1)
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.orden-2);
                                                                                            fila.data.orden-=1;
                                                                                            filaAux.data.orden+=1;
                                                                                            tblGrid.getStore().sort('orden','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/SignDown.gif',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la secci&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        if(fila.data.orden<tblGrid.getStore().getCount())
                                                                                        {
                                                                                        	var filaAux=tblGrid.getStore().getAt(fila.data.orden);
                                                                                            fila.data.orden+=1;
                                                                                            filaAux.data.orden-=1;
                                                                                            tblGrid.getStore().sort('orden','ASC');
                                                                                        }
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}

function mostrarVentanaFormulariosDinamicos()
{
	
	
    var alOpciones=		new Ext.data.SimpleStore(
                                                    {
                                                        fields:	[
                                                                 	{name:'idFormulario'},
                                                                    {name:'nombre'}, 
                                                                    {name:'titulo'},
                                                                    {name: 'descripcion'},
                                                                    {name:'idProceso'},
                                                                    {name:'formularioBase'}
                                                                      
                                                                ]
                                                    }
                                                );
    
    
    var dsOpciones= [];
    
    alOpciones.loadData(dsOpciones);
    
    var cmFrmDTD= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'Nombre del formulario',
                                                            width:200,
                                                            dataIndex:'nombre'
                                                        },
                                                        {
                                                        	header:'T\xEDtulo',
                                                            width:150,
                                                            dataIndex:'titulo'
                                                        },
                                                        {
                                                        	header:'Descripci\xF3n',
                                                            width:330,
                                                            dataIndex:'descripcion'
                                                        }
                                                        
                                                       
                                                    ]
                                                );
    
    
    var tblOpciones=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridFormularios',
                                                            store:alOpciones,
                                                            frame:true,
                                                            cm: cmFrmDTD,
                                                            height:300,
                                                            width:750
                                                            
                                                        }
                                                    );
    
    panelGrid=new Ext.Panel	(
                                {
                                    y:50,
                                    items:	[
                                                tblOpciones
                                            ]
                                }
                            );
                            
    
    var cmbProcesos=crearComboExt('cmbProcesos',arrProcesos,480,5,260);
    cmbProcesos.on('select',cargarFormularios);
    
    var form = new Ext.form.FormPanel(	
                                        {
                                            baseCls: 'x-plain',
                                            layout:'absolute',
                                            defaultType: 'textfield',
                                            items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'label',
                                                            html:'Elija el proceso al cual pertenece el formulario que desea utilizar como secci&oacute;n complementaria:'
                                                        },
                                                        cmbProcesos,
                                                        panelGrid
                                                    ]
                                        }
                                    );
    
    btnSiguiente=new Ext.Button	(
                                    {
                                        text: 'Aceptar',
                                        minWidth:80,
                                        id:'btnFinalizar',
                                        listeners:	{
                                                        click:
                                                                {
                                                                    fn:function()
                                                                    {
                                                                    	var tSecciones=gEx('tSecciones');
                                                                        var filaSel= gEx('gridFormularios').getSelectionModel().getSelected();
                                                                        if(filaSel==null)
                                                                        {
                                                                        	msgBox('Debe seleccionar el formulario que desa utilizar como secci&oacute;n');
                                                                        	return;
                                                                        }
                                                                        
                                                                        
                                                                       	if(obtenerPosFila(tSecciones.getStore(),'idFormulario',filaSel.get('idFormulario'))==-1)
                                                                        {
                                                                            var r=new regSeccion(	{
                                                                                                    idFormulario:filaSel.get('idFormulario'),
                                                                                                    nombreFormulario:filaSel.get('nombre'), 
                                                                                                    proceso:cmbProcesos.getRawValue(),
                                                                                                    orden:(tSecciones.getStore().getCount()+1)
                                                                                                }
                                                                                            );
                                                                            tSecciones.getStore().add(r);
                                                                        }
                                                                        ventanaSelForm.close();
                                                                        
                                                                    }
                                                                }
                                                    }
                                    }
                                )
    
    ventanaSelForm = new Ext.Window(
                                            {
                                                title: 'Selecci\xF3n de formulario',
                                                width: 780 ,
                                                height:450,
                                                minWidth: 300,
                                                minHeight: 100,
                                                layout: 'fit',
                                                plain:true,
                                                modal:true,
                                                bodyStyle:'padding:5px;',
                                                buttonAlign:'center',
                                                items: 	[
                                                            form
                                                        ],
                                                listeners : {
                                                            show : {
                                                                        buffer : 10,
                                                                        fn : function() 
                                                                        {
                                                          			                  
                                                                        }
                                                                    }
                                                        },
                                                buttons:	[
                                                                btnSiguiente,
                                                                {
                                                                    text: 'Cancelar',
                                                                    handler:function()
                                                                    {
                                                                    	
                                                                        ventanaSelForm.close();
                                                                        
                                                                    }
                                                                }
                                                            ]
                                            }
                                        );
	
    ventanaSelForm.show();
}

function cargarFormularios(combo,registro,indice)
{

	function funcResp()
    {
    	var arrResp=peticion_http.responseText.split('|');
        if(arrResp[0]=='1')
		{
            	var arrTablas=eval(arrResp[1]);
                var almacen=Ext.getCmp('gridFormularios').getStore();
                almacen.loadData(arrTablas);
               	
		}
		else
		{
			msgBox('No se ha podido realizar la operación debido al siguiente problema:'+' <br />'+arrResp[0]);
		}
    }
    obtenerDatosWeb('../paginasFunciones/funcionesFormulario.php',funcResp, 'POST','funcion=14&idProceso='+registro.get('id'),true);
}

function crearGridDimensiones()
{
	var cmbGaleria=crearComboExt('cmbGaleria',arrSiNo);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idDimension'},
                                                        {name: 'dimension'},
                                                        {name: 'etiqueta'},
                                                        {name: 'orden', type:'int'},
                                                        {name: 'permiteGaleria'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );

    var store=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'orden', direction: 'ASC'},
                                              groupField: 'orden',
                                              remoteGroup:false,
                                              remoteSort: false,
                                              autoLoad:true
                                              
                                          }) 
    
    
    store.on('beforeload',function(proxy)
    						{
                            	
                                proxy.baseParams.funcion=161;
                                proxy.baseParams.idCategoria=gE('idCategoria').value;
                            }
    		)
   
   
    				
                    
	store.on('load',function(almacen)
    				{
                    	
                    }    
            )
    			                    
                    
    grid=  new Ext.grid.EditorGridPanel(	{
                                                    renderTo:'sDimension',
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    id:'arbolDimensiones',
													store: store,
                                                    stripeRows: true,
                                                    columnLines :true,
													loadMask :true,
                                                    width:850,
                                                    height:300,
                                                    sm:chkRow,
                                                    anchor:'100% 100%',
                                                    tbar:	[
                                                                {
                                                                    icon:'../images/add.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Agregar dimensi&oacute;n',
                                                                    handler:function()
                                                                            {
                                                                                mostrarVentanaAgregarDimension();
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/delete.png',
                                                                    cls:'x-btn-text-icon',
                                                                    text:'Remover dimensi&oacute;n',
                                                                    handler:function()
                                                                            {
                                                                               var fila=grid.getSelectionModel().getSelected();
                                                                               if(!fila)
                                                                               {
                                                                               		msgBox('Debe seleccionar la dimensi&oacute;n que desea remover');
                                                                               		return;
                                                                               } 
                                                                               function resp(btn)
                                                                               {
                                                                               		if(btn=='yes')
                                                                                    {	
                                                                                    	grid.getStore().remove(fila);
                                                                                        var x;
                                                                                        for(x=0;x<grid.getStore().getCount();x++)
                                                                                        {
                                                                                            grid.getStore().getAt(x).set('orden',(x+1));
                                                                                        }
                                                                                    }
                                                                               }
                                                                               msgConfirm('Es&aacute; seguro de querer remover la dimensi&oacute;n seleccionada?',resp);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    xtype:'label',
                                                                    html:'&nbsp;<b>Modificar orden de dimensi&oacute;n:&nbsp;</b>'
                                                                },'-',
                                                                {
                                                                    icon:'../images/SignUp.gif',
                                                                    handler:function()
                                                                            {
                                                                            	var tblGrid=grid;
                                                                                var fila=tblGrid.getSelectionModel().getSelected();
                                                                                if(!fila)
                                                                                {
                                                                                    msgBox('Debe seleccionar la dimensi&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                    return;
                                                                                }
                                                                                if(fila.data.orden>1)
                                                                                {
                                                                                    var filaAux=tblGrid.getStore().getAt(fila.data.orden-2);
                                                                                    fila.data.orden-=1;
                                                                                    filaAux.data.orden+=1;
                                                                                    tblGrid.getStore().sort('orden','ASC');
                                                                                }
                                                                                
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/SignDown.gif',
                                                                    handler:function()
                                                                            {
                                                                            	var tblGrid=grid;
                                                                                var fila=tblGrid.getSelectionModel().getSelected();
                                                                                if(!fila)
                                                                                {
                                                                                    msgBox('Debe seleccionar la dimensi&oacute;n cuyo orden de aplicaci&oacute;n desea modificar');
                                                                                    return;
                                                                                }
                                                                                if(fila.data.orden<tblGrid.getStore().getCount())
                                                                                {
                                                                                    var filaAux=tblGrid.getStore().getAt(fila.data.orden);
                                                                                    fila.data.orden+=1;
                                                                                    filaAux.data.orden-=1;
                                                                                    tblGrid.getStore().sort('orden','ASC');
                                                                                }
                                                                                
                                                                            }
                                                                    
                                                                }
                                                                
                                                            ],
                                                    columns:	[
                                                    				chkRow,
                                                                    {
                                                                        header: 'Dimensi&oacute;n',
                                                                        dataIndex: 'dimension',
                                                                        width: 290,
                                                                        renderer:function(v,meta,record)
                                                                                    {
                                                                                        return mostrarValorDescripcion(v);
                                                                                    }
                                                                    },
                                                                    {
                                                                        header: 'Etiqueta',
                                                                        dataIndex: 'etiqueta',
                                                                        width: 180,
                                                                        editor:{xtype:'textfield'},
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	return mostrarValorDescripcion(v);
                                                                        }
                                                                    },
                                                                     {
                                                                        header: 'Orden',
                                                                        dataIndex: 'orden',
                                                                        width: 100
                                                                        
                                                                    },
                                                                     {
                                                                        header: 'Asociar galeria de im&aacute;genes',
                                                                        dataIndex: 'permiteGaleria',
                                                                        width: 170,
                                                                        editor:cmbGaleria,
                                                                        renderer:function(val)
                                                                        		{
                                                                                	return formatearValorRenderer(arrSiNo,val);
                                                                                }
                                                                        
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    grid.on('afteredit',function(e)
   						{
                        	if(e.field=='permiteGaleria')
                            {
                                 if(e.value=='1')
                                 {
                                 	var x;
                                    var fila;
                                    for(x=0;x<e.grid.getStore().getCount();x++)
                                    {
                                    	fila=e.grid.getStore().getAt(x);
                                        fila.set('permiteGaleria','0');
                                    }
                                    e.record.set('permiteGaleria','1');
                                 }
                            }
                        }
   			)
                                          
	return grid;  
}

function mostrarVentanaAgregarDimension()
{
	var gridDimension=crearGridDimensionesAdd();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Seleccione la dimensi&oacute;n que desea agregar:'
                                                        },
                                                        gridDimension
                                                        
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar dimensi&oacute;n',
										width: 630,
										height:400,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var arbolDimensiones=gEx('arbolDimensiones');
																		var filas=gridDimension.getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar las dimensiones que desea agregar a producto');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(obtenerPosFila(arbolDimensiones.getStore(),'idDimension',filas[x].data.idDimension)==-1)
                                                                            {
                                                                                var r=new regDimension	(
                                                                                                            {
                                                                                                                idDimension:filas[x].data.idDimension,
                                                                                                                dimension:filas[x].data.nombreDimension,
                                                                                                                orden:(arbolDimensiones.getStore().getCount()+1),
                                                                                                                etiqueta:filas[x].data.nombreDimension,
                                                                                                                permiteGaleria:0
                                                                                                            }
                                                                                                        )
                                                                                                        
                                                                            
                                                                                arbolDimensiones.getStore().add(r);
                                                                        	}
                                                                        }
                                                                        ventanaAM.close();
		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();		
	
}

function crearGridDimensionesAdd()
{
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDimension'},
		                                                {name: 'nombreDimension'},
                                                        {name: 'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreDimension', direction: 'ASC'},
                                                            groupField: 'nombreDimension',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='160';
                                        proxy.baseParams.idCategoria=gE('idCategoria').value;
                                        
                                    }
                        )   
       
       
	var expander = new Ext.ux.grid.RowExpander({
                                                column:2,
                                                tpl : new Ext.Template(
                                                    '<table ><tr><td colspan="2" height="10"></td></tr><tr><td width:"230"><span class="letraRojaSubrayada8"><b>Descripci&oacute;n:</b></span></td><td><span class="copyrigthSinPadding">{descripcion}</span><br /><br /></td></tr>'+
                                                    '</table>'
                                                )
                                            });       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            expander,
                                                            
                                                            {
                                                                header:'Dimensi&oacute;n',
                                                                width:460,
                                                                sortable:true,
                                                                dataIndex:'nombreDimension'
                                                            }
                                                        ]
         
         
         
         
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAddDimensiones',
                                                                store:alDatos,
                                                                x:10,
                                                                y:30,
                                                                width:580,
                                                                height:330,
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[expander],
                                                                sm:chkRow,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function dimensionamientoChange(cmb)
{
	

	var valor=cmb.options[cmb.selectedIndex].value;
    switch(valor)
    {
    	case '0':
        	$( "#tabs" ).tabs("disable",1);
        break;
        case '1':
        	$( "#tabs" ).tabs("enable",1);
        break;
    }
}



function agregarDescuentoProducto(idCategoria,minFechaDescuento)
{
	var fechaActual=Date.parseDate('<?php echo date('Y-m-d')?>','Y-m-d');
    var minFecha=fechaActual;
    if(fechaActual<minFechaDescuento)
    	minFecha=minFechaDescuento;
    var x;
   
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Porcentaje descuento:'
                                                        },
                                                        
                                                        {
                                                        	x:135,
                                                            y:5,
                                                            width:70,
                                                            id:'txtDescuento',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigencia del:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigencia',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:255,
                                                            y:40,
                                                            html:'al:'
                                                        },
                                                        {
                                                        	x:280,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigenciaAl',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                         {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n del descuento:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'textarea',
                                                            height:60,
                                                            width:450,
                                                            id:'txtDescripcion'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar descuento por categor&iacute;a',
										width: 500,
										height:245,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtDescuento').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var cadObj='';
                                                                        var obj='';
																		var x;
                                                                        var dteVigencia=gEx('dteVigencia');
                                                                        var dteVigenciaAl=gEx('dteVigenciaAl');
                                                                        var txtDescuento=gEx('txtDescuento');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        
                                                                        if(txtDescuento.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado no es v&aacute;lido',resp);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(parseFloat(txtDescuento.getValue())>100)
                                                                        {
                                                                        	function resp10()
                                                                            {
                                                                            	txtDescuento.focus();
                                                                            }
                                                                            msgBox('El porcentaje de descuento ingresado NO puede ser mayor a 100%',resp10);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        
                                                                        if(dteVigencia.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha inicial del descuento',resp2);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigenciaAl.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteVigenciaAl.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha t&eacute;mino del descuento',resp3);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()>dteVigenciaAl.getValue())
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio no puede ser mayor que la fecha de t&eacute;rmino',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(txtDescripcion.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	txtDescripcion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n del descuento',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                            
                                                                        obj='{"descripcion":"'+cv(txtDescripcion.getValue())+'","fechaInicio":"'+dteVigencia.getValue().format('Y-m-d')+'","fechaTermino":"'+dteVigenciaAl.getValue().format('Y-m-d')+'","descuento":"'+txtDescuento.getValue()+
                                                                            '","idProducto":"'+idCategoria+'","llave":"","tipoDescuento":"1"}';
                                                                        var cObj='{"aRegistros":['+obj+']}'
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gridDescuentosCategoria').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=185&cadObj='+cObj,true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDescuentosProducto()
{
	
       
       var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idDescuento'},
		                                                {name: 'porcentajeDescuento'},
		                                                {name:'fechaInicio', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'fechaTermino', type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'descripcionDescuento'},
                                                        {name: 'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'situacion'},
                                                        {name: 'motivoCancelacion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicio', direction: 'DESC'},
                                                            groupField: 'fechaInicio',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='184';
                                        proxy.baseParams.idProducto=gE('idCategoria').value;
                                        proxy.baseParams.llave='';
                                        proxy.baseParams.tipoDescuento=1;
                                        gEx('btnPromoModif').disable();
	                                    gEx('btnPromoDel').disable();
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Porcentaje<br>descuento',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'porcentajeDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return removerCerosDerecha(val)+' %';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha inicio',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaInicio',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha t&eacute;rmino',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaTermino',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n del descuento',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'descripcionDescuento',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var comp='';
                                                                            if(registro.data.motivoCancelacion!='')
	                                                                            comp=' <img src="../images/icon_comment.gif" title="'+escaparBR(registro.data.motivoCancelacion)+'" alt="'+escaparBR(registro.data.motivoCancelacion)+'" />';
                                                                        	switch(val)
                                                                            {
                                                                            	case '0':
                                                                                	return 'Inactivo'+ comp;
                                                                                break;
                                                                            	case '1':
                                                                                	return 'Activo'+ comp;
                                                                                break;
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                header:'Registrado el',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaRegistro',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i:s')
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridDescuentosCategoria',
                                                                store:alDatos,
                                                                renderTo:'sDescuentos',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                height:350,
                                                                width:960,
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar descuento',
                                                                                handler:function()
                                                                                        {
                                                                                        
                                                                                        	var minFecha=fechaActual;
                                                                                            var gridDescuentos=gEx('gridDescuentosCategoria');
                                                                                            var ct=0;
                                                                                            var fila;
                                                                                            for(ct=0;ct<gridDescuentos.getStore().getCount();ct++)
                                                                                            {
                                                                                            	fila=gridDescuentos.getStore().getAt(ct);
                                                                                                if(fila.data.situacion=='1')
                                                                                                {
                                                                                                    if(minFecha<fila.data.fechaTermino.add(Date.DAY,1))
                                                                                                        minFecha=fila.data.fechaTermino.add(Date.DAY,1);
                                                                                            	}
                                                                                            }
                                                                                         	agregarDescuentoProducto(gE('idCategoria').value,minFecha);   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/pencil.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnPromoModif',
                                                                                text:'Modificar fecha de t&eacute;rmino',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentosCategoria').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento cuya fecha de t&eacute;rmino desea modificar');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            mostrarVentanaFinalizarFechaTermino(fila);
                                                                                            
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                            
                                                                            ,'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                id:'btnPromoDel',
                                                                                text:'Remover descuento',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=gEx('gridDescuentosCategoria').getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar el descuento que desea remover');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            if(fila.data.fechaInicio<=fechaActual)
                                                                                            {
                                                                                            	msgBox('No se puede remover descuentos cuya fecha de vigencia haya comenzado');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            gEx('gridDescuentosCategoria').getStore().reload();
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=186&idDescuento='+fila.data.idDescuento,true);
                                                                                                    
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover el descuento seleccionado?',resp);
                                                                                            
                                                                                        }
                                                                                
                                                                            }
                                                                        ],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        
        
        
        tblGrid.getSelectionModel().on('rowselect',function(sm,numFila,registro)
        						{
                                	gEx('btnPromoModif').disable();
                                    gEx('btnPromoDel').disable();
                                	if(registro.data.fechaInicio>fechaActual)
                                    {
                                    	gEx('btnPromoDel').enable();
                                    
                                    }
                                    else
                                    {
                                    	if(registro.data.fechaTermino>=fechaActual)
                                        	gEx('btnPromoModif').enable();
                                    }
                                }
        			)   
        
        return 	tblGrid;	
	
}

function mostrarVentanaFinalizarFechaTermino(fila)
{
	var fechaActual=Date.parseDate('<?php echo date("Y-m-d")?>',"Y-m-d");
    
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Ingrese la nueva fecha de t&eacute;rmino: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:30,
                                                        	xtype:'label',
                                                            html:'Si la fecha de t&eacute;rmino coincide con la fecha actual o la fecha de inicio del descuento, &eacute;ste inmediatamente ser&aacute; cancelado'
                                                        },
                                                        {
                                                        	x:220,
                                                            y:5,
                                                            id:'dteFechaTermino',
                                                            xtype:'datefield',
                                                            value:fechaActual,
                                                            minValue:fila.data.fechaInicio,
                                                            maxValue:fila.data.fechaTermino
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo del cambio: <span style="color:#F00">*</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            width:500,
                                                            height:80,
                                                            xtype:'textarea',
                                                            id:'txtMotivo'
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar fecha de t&eacute;rmino',
										width: 550,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var dteFechaTermino=gEx('dteFechaTermino');
																		if(dteFechaTermino.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaTermino.focus();
                                                                            }
                                                                            msgBox('Debe especificar la fecha de t&eacute;rmino del descuento',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        var txtMotivo=gEx('txtMotivo');
                                                                        if(txtMotivo.getValue()=='')
                                                                        {
                                                                            function resp2()
                                                                            {
                                                                                txtMotivo.focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo del cambio de fecha de t&eacute;rmino del descuento',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        function respConf(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gridDescuentosCategoria').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=191&idDescuento='+fila.data.idDescuento+'&motivoCancelacion='+
                                                                                cv(txtMotivo.getValue())+'&fechaTermino='+dteFechaTermino.getValue().format("Y-m-d"),true);

                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer modificar la fecha de t&eacute;rmino del descuento?',respConf);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

