<?php
	include_once("latis/conexionBD.php");
	include_once("latis/cExcel.php");
	include_once("funcionesReporteTribunal.php");
	
	$fechaReporte=date("d/m/Y");
	$fechaR=date("Y-m-d");
	$codigoUnidad=-1;
	//$conRol=0;
	
	if(isset($_POST["idCentro"]))
		$codigoUnidad=$_POST["idCentro"];

	if(isset($_POST["conRol"]))
		$conRol=$_POST["conRol"]; //mostrar Rol 0=No,1=Si
		
	//$codigoUnidad='001';
	
	$sql="";
	if($codigoUnidad!=-1)
	{
		$sql=" AND a.Institucion='".$codigoUnidad."'";
	}
	
	$consulta="SELECT u.idUsuario,i.nom,i.Paterno,i.Materno,u.Login,u.Password,o.unidad FROM 802_identifica i,800_usuarios u,801_adscripcion a,
			817_organigrama o WHERE i.idUsuario=u.idUsuario AND u.idUsuario=a.idUsuario AND a.Institucion=o.codigoUnidad$sql 
			AND u.idUsuario not in(2376,2381,2380,2379,2378,2384,2382,2383,1,2385)ORDER BY unidad,i.Paterno,i.Materno,i.nom";
	$res=$con->obtenerFilas($consulta);

$libro=new cExcel("reporteCuentasUsuarios.xls",true);
$libro->setValor("B3",$fechaReporte);

	$libro->setAnchoColumna("A",'15'); 
	$libro->setAnchoColumna("B",'25'); 
	$libro->setAnchoColumna("C",'20');
	$libro->setAnchoColumna("D",'20'); 
	$libro->setAnchoColumna("E",'30'); 
	$libro->setAnchoColumna("F",'15'); 
	$libro->setAnchoColumna("G",'15'); 
	$libro->setAnchoColumna("H",'30'); 

$linea=6;
	while($fila=mysql_fetch_row($res))
	{
		//$nombreUnidad=obtenerNombreUGA($fila[6]);
		
		$libro->setValor("A".$linea,$fila[0]);
		$libro->setValor("B".$linea,$fila[1]);
		$libro->setValor("C".$linea,$fila[2]);
		$libro->setValor("D".$linea,$fila[3]);
		$libro->setValor("F".$linea,$fila[4]);
		$libro->setValor("G".$linea,$fila[5]);
		$libro->setValor("H".$linea,$fila[6]);
		$linea++;
		if($conRol==1)
		{
			$consultaRol="SELECT nombreGrupo FROM 8001_roles r,807_usuariosVSRoles u WHERE r.idRol=u.idRol AND u.idUsuario='".$fila[0]."' 
						ORDER BY nombreGrupo";
			$resRol=$con->obtenerFilas($consultaRol);
			
			while($row=mysql_fetch_row($resRol))
			{
				$libro->setValor("E".$linea,$row[0]);
				$linea++;
			}
		}
		
	}

	//$libro->generarArchivo("HTML");
	$libro->generarArchivo("Excel5","CatalogoUsuarios.xls");

?>