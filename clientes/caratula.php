<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");
?>
<?php
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=archivo.xls");
header("Pragma: no-cache");
header("Expires: 0");
header('charset=utf-8');
?>

<?php session_start();
	$sql = "SELECT *FROM 4081_colorEstilo";
    $fila= $con->obtenerPrimeraFila($sql);
	$colorFondoEx=$fila[0];
	$colorFondoIn=$fila[1];
	$colorMenu=$fila[2];
	$colorBarraIn=$fila[3];
	$colorMenuIn=$fila[4];
	$colorBanner=$fila[5];
	$colorLink=$fila[6];
	$colorTiTabla=$fila[7];
	$colorCelda1=$fila[8];
	$colorCelda2=$fila[9];
	$colorLeTabla=$fila[10];
	$colorTxTabla=$fila[11];
	$colorFuMenu=$fila[12];
	$tamFuMenu=$fila[13];
	$colorBordeIn=$fila[14];
	$botonazo=$fila[15];
	$disenoBanner=$fila[16];
	$colorLeNivel1=$fila[20];
	$colorLeNivel2=$fila[21];
	$tituloMenuIzq=$fila[22];
	$colorLePieIzq=$fila[23];
	$colorLetra=$fila[26];
	$colorTxTabla1=$fila[24];
	$colorTxTabla2=$fila[25];
	$colorTxTabla3=$fila[26];
	$colorTxTabla4=$fila[27];	
	$colorTxTabla5=$fila[29];
	$colorCelda3=$fila[28];
	$colorBorde1=$fila[30];
	
	$colortxtImpre1=$fila[31];
	$colortxtImpre2=$fila[32];
	$colorCeldaImp1=$fila[33];
	$colorCeldaImp2=$fila[34];
?>
<?php
 function generarTablaAval($fila,$nAval,$secAvales)
{
	global $con;
	
	if($fila[1]!="")
	{
		$arrDatosCli=explode(",",$fila[1]);
		$ct=1;
		foreach($arrDatosCli as $aval)
		{
			$arrDAval=explode("_",$aval);
			if($arrDAval[2]=="1")
				$consulta="select concat(nombres,' ',paterno,' ',materno) as nombre, telefonos,email from 703_clientes where idCliente=".$arrDAval[0];
			else
				$consulta="select empresa, telefonos,email from 700_empresas where idEmpresa=".$arrDAval[0];
			$filaC=$con->obtenerPrimeraFila($consulta);
?>
 
 
 			<tr height=21 >
              <td height=21 class=xl87 >Propietario <?php echo $ct?>: </td>
              <td class=xl108 colspan=4 >
                        <?php 
                            echo utf8_decode($filaC[0]);
                         ?>
                         </td>
              <td class=xl87 >Tel&eacute;fono:</td>
              <td colspan=4 class=xl108> <?php 
                            echo $filaC[1];
                         ?></td>
              <td class=xl97></td>
 			</tr>
            <tr class=xl65 height=20 >
              <td height=20 class=xl87 >Relaci&oacute;n/Parentesco:</td>
              <td class=xl108 colspan=4 ><label>
                           <?php
                              $consulta="select parentesco from 713_relacionParentesco where idRelacionParentesco=".$fila[6];
                              $parentesco=$con->obtenerValor($consulta);
                              echo utf8_decode($parentesco);
                          ?>
                         </label></td>
              <td class=xl87 style='border-top:none'>Correo electr&oacute;nico:</td>
              <td colspan=4 class=xl108><label id="lblEmail">
                         <?php 
                            echo utf8_decode($filaC[2]);
                         ?>
                         </label></td>
              <td class=xl97></td>
            </tr>
            <?php
			$ct++;
		}
	}
			?>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl87 >Patrimonio:</td>
  <td colspan=9 class=xl108 ><label>
         <?php
            echo utf8_decode($fila[3]);
        ?>
         </label></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl87 style='height:15.0pt'>Valor Declarado:</td>
  <td colspan=2 class=xl108><label>
         <?php
            echo number_format($fila[2],"2",".",",");
        ?>
         </label></td>
  <td colspan=2 class=xl87 >Disponible a Hipoteca:</td>
  
  <td class=xl108 ><label>
           <?php
              $consulta="select texto from 1004_siNo where idIdioma=".$_SESSION["leng"]." and valor=".$fila[4];
                $texto=$con->obtenerValor($consulta);
                echo utf8_decode($texto);
          ?>
         </label></td>
   
  <td colspan=4 class=xl271>&nbsp;</td>
  <td class=xl97></td>
 </tr>
 <tr >
  <td height=21 class=xl87 >Superficie aprox.:</td>
  
  <td class=xl108 > <label>
         <?php
            echo number_format($fila[5],"2",".",",");
        ?>
        </label>
  </td >
  <td colspan=8 class=xl271>&nbsp;</td>
  <td class=xl97></td>
 </tr>

 <tr>
 <td colspan=10 class=xl107 height=22>&nbsp;</td>
 </tr>

<?php
}
?>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 11">
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<script type="text/javascript" src="../Scripts/masks.js"></script>
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.39in .2in .47in .24in;
	mso-header-margin:.31in;
	mso-footer-margin:.31in;
	mso-horizontal-page-align:center;
	mso-vertical-page-align:center;}
.font28
	{color:red;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.font31
	{color:black;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Tahoma, sans-serif;
	mso-font-charset:0;}
.font32
	{color:black;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Tahoma, sans-serif;
	mso-font-charset:0;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style46
	{color:blue;
	font-size:16.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-style-name:Hiperv�nculo;
	mso-style-id:8;}
a:link
	{color:blue;
	font-size:16.5pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
a:visited
	{color:purple;
	font-size:12.1pt;
	font-weight:400;
	font-style:normal;
	text-decoration:underline;
	text-underline-style:single;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.style50
	{mso-number-format:"_-\0022$\0022* \#\,\#\#0\.00_-\;\\-\0022$\0022* \#\,\#\#0\.00_-\;_-\0022$\0022* \0022-\0022??_-\;_-\@_-";
	mso-style-name:Moneda;
	mso-style-id:4;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
.style54
	{mso-number-format:0%;
	mso-style-name:Porcentual;
	mso-style-id:5;}
td
	{mso-style-parent:style0;
	padding:0px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}

.xl87
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre1?>;
	font-size:10.0pt;
	font-weight:700;
	mso-number-format:"mmm\\-yyyy";
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp1?>;
	mso-pattern:auto none;}
.xl96
	{mso-style-parent:style50;
	color:#<?php echo $colortxtImpre2?>;
	font-size:12.0pt;
	font-weight:700;
	mso-number-format:"_-\0022$\0022* \#\,\#\#0_-\;\\-\0022$\0022* \#\,\#\#0_-\;_-\0022$\0022* \0022-\0022??_-\;_-\@_-";
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none; }

.xl24
	{mso-style-parent:style0;
	mso-number-format:"\0022$\0022\#\,\#\#0\.00";}

.xl109
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-number-format:"\0022$\0022\#\,\#\#0\.00";
	text-align:center;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}


.xl106
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}
.xl107
	{mso-style-parent:style0;
	color:000000;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:center;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:FFFFFF;
	mso-pattern:auto none;}	
.xl108
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}

.xl160
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre1?>;
	font-size:10.0pt;
	font-weight:700;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp1?>;
	mso-pattern:auto none;}
.xl270
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:12.0pt;
	font-weight:700;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:center;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}
.xl271
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre1?>;
	font-size:11.0pt;
	font-weight:700;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#FFFFFF;
	mso-pattern:auto none;}
	
.xl272
	{mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:11.0pt;
	font-weight:700;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:center;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#FFFFFF;
	mso-pattern:auto none;}
.xl1081 {mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}
.xl1091 {mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-number-format:"\0022$\0022\#\,\#\#0\.00";
	text-align:center;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}
.xl1082 {mso-style-parent:style0;
	color:#<?php echo $colortxtImpre2?>;
	font-size:10.0pt;
	font-family:"Trebuchet MS", sans-serif;
	mso-font-charset:0;
	text-align:left;
	border-top:.5pt solid windowtext;
	border-right:.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:.5pt solid windowtext;
	background:#<?php echo $colorCeldaImp2?>;
	mso-pattern:auto none;}
-->
</style>
<![if !supportAnnotations]><style id="dynCom" type="text/css"><!-- --></style>

<script language="JavaScript"><!--

function msoCommentShow(com_id,anchor_id) {
	if(msoBrowserCheck()) {
	   c = document.all(com_id);
	   a = document.all(anchor_id);
	   if (null != c) {
		var cw = c.offsetWidth;
		var ch = c.offsetHeight;
		var aw = a.offsetWidth;
		var ah = a.offsetHeight;
		var x = a.offsetLeft;
		var y = a.offsetTop;
		var el = a;
		while (el.tagName != "BODY") {
		   el = el.offsetParent;
		   x = x + el.offsetLeft;
		   y = y + el.offsetTop;
		   }		
		var bw = document.body.clientWidth;
		var bh = document.body.clientHeight;
		var bsl = document.body.scrollLeft;
		var bst = document.body.scrollTop;
		if (x + cw + ah/2 > bw + bsl && x + aw - ah/2 - cw >= bsl ) {
		   c.style.left = x + aw - ah / 2 - cw; 
		}
		else {
		   c.style.left = x + ah/2; 
		}
		if (y + ch + ah/2 > bh + bst && y + ah/2 - ch >= bst ) {
	 	   c.style.top = y + ah/2 - ch;
		} 
		else {
		   c.style.top = y + ah/2;
		}
		c.style.visibility = "visible";
	   }
	}
}

function msoCommentHide(com_id) {
	if(msoBrowserCheck()) {
	  c = document.all(com_id)
	  if (null != c) {
	    c.style.visibility = "hidden";
	    c.style.left = "-10000";
	    c.style.top = "-10000";
	  }
	}
}

function msoBrowserCheck() {
 ms=navigator.appVersion.indexOf("MSIE");
 vers = navigator.appVersion.substring(ms+5, ms+6);
 ie4 = (ms>0) && (parseInt(vers) >=4);
 return ie4
}

if (msoBrowserCheck()) {
document.styleSheets.dynCom.addRule(".msocomspan1","position:absolute");
document.styleSheets.dynCom.addRule(".msocomspan2","position:absolute");
document.styleSheets.dynCom.addRule(".msocomspan2","left:-1.5ex");
document.styleSheets.dynCom.addRule(".msocomspan2","width:2ex");
document.styleSheets.dynCom.addRule(".msocomspan2","height:0.5em");
document.styleSheets.dynCom.addRule(".msocomanch","font-size:0.5em");
document.styleSheets.dynCom.addRule(".msocomanch","color:red");
document.styleSheets.dynCom.addRule(".msocomhide","display: none");
document.styleSheets.dynCom.addRule(".msocomtxt","visibility: hidden");
document.styleSheets.dynCom.addRule(".msocomtxt","position: absolute");        
document.styleSheets.dynCom.addRule(".msocomtxt","top:-10000");         
document.styleSheets.dynCom.addRule(".msocomtxt","left:-10000");         
document.styleSheets.dynCom.addRule(".msocomtxt","width: 33%");                 
document.styleSheets.dynCom.addRule(".msocomtxt","background: infobackground");
document.styleSheets.dynCom.addRule(".msocomtxt","color: infotext");
document.styleSheets.dynCom.addRule(".msocomtxt","border-top: 1pt solid threedlightshadow");
document.styleSheets.dynCom.addRule(".msocomtxt","border-right: 2pt solid threedshadow");
document.styleSheets.dynCom.addRule(".msocomtxt","border-bottom: 2pt solid threedshadow");
document.styleSheets.dynCom.addRule(".msocomtxt","border-left: 1pt solid threedlightshadow");
document.styleSheets.dynCom.addRule(".msocomtxt","padding: 3pt 3pt 3pt 3pt");
document.styleSheets.dynCom.addRule(".msocomtxt","z-index: 100");
}

// -->
</script>
<![endif]>
</head>

<body link=blue vlink=purple class=xl71>

    <?php
						
							$idCredito=-1;
							if(isset($_POST["idCredito"]))
								$idCredito=$_POST["idCredito"];
							
							
							$consulta="select * from 750_creditos where idCredito=".$idCredito;
							$filaCred=$con->obtenerPrimeraFila($consulta);
							$idCliente=$filaCred[1];
							$tipoCliente=$filaCred[2];
						?>


<?php
							
							if($tipoCliente==2)
							{
								$consulta="select empresa,rfc,fechaAltaSAT,direccionF,colF,ciudadf,telefonos,email,web,noEmpleados,nominaMensual,numf,cpf,direccionv,
											colv,ciudadv,numv,cpv,tiempoResidenciaf,tiempoResidenciav,valorResidenciaF,valorResidenciaV,superficief,superficiev,
											tipoResidenciaf,tipoResidenciav,rentaResidenciaf,rentaResidenciav,funcionRepLegal,actividadPrincipal,familiar,
											tipoResidenciaf,idRepresentanteLegal,delf,delv,construidof,construidov,RFC2,RFC3
											from 700_empresas where idEmpresa=".$idCliente;
								$filaCl=$con->obtenerPrimeraFila($consulta);
							}
													
							else
							{
								$consulta="select  paterno,rfc,fechaAltaSAT,direccionF,colF,ciudadf,telefonos,email,web,noEmpleados,nominaMensual,numf,cpf,direccionv,
											colv,ciudadv,numv,cpv,tiempoResidenciaf,tiempoResidenciav,valorResidenciaF,valorResidenciaV,superficief,superficiev,
											tipoResidenciaf,tipoResidenciav,rentaResidenciaf,rentaResidenciav,funcionRepLegal,actividadPrincipal,
											materno,tipoResidenciaf,idRepresentanteLegal,delf,delv,construidof,construidov,nombres,RFC2,RFC3 
											from 703_clientes where idCliente=".$idCliente;
								
								$filaCl=$con->obtenerPrimeraFila($consulta);
							}
							
						?>
                        


<table x:str border=0 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed;width:600pt'>

 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 width=141 style='height:15.0pt;width:106pt' align=left
  valign=top>
  <![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:4;margin-left:1px;margin-top:3px;width:226px;
  height:51px'><img  src="http://rccorporativo.dnsdojo.com/images/rccorporativoLogo.gif"
  alt="Logo RC.png" v:shapes="_x0035__x0020_Imagen" width="237" height="65"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=20 class=xl65 width=141 style='height:15.0pt;width:106pt'><a
    name="Print_Area"></a></td>
   </tr>
  </table>
  </span></td>
  <td class=xl65 width=120 style='width:90pt'></td>
  <td class=xl65 width=141 style='width:106pt'></td>
  <td class=xl65 width=100 style='width:75pt'></td>
  <td class=xl65 width=100 style='width:75pt'></td>
  <td class=xl65 width=100 style='width:75pt'></td>
  <td class=xl65 width=118 style='width:89pt'></td>
  <td class=xl65 width=100 style='width:75pt'></td>
  <td class=xl65 width=100 style='width:75pt'></td>
  <td class=xl65 width=109 style='width:82pt'></td>
  <td class=xl97 width=119 style='width:89pt'></td>
 </tr>
 <tr class=xl65 height=24 style='height:18.0pt'>
  <td height=24 colspan=6 class=xl65 style='height:18.0pt;mso-ignore:colspan'></td>
  <td colspan=2 class=xl160 >SOLICITUD DE
  CREDITO</td>
  <td colspan=2 class=xl65 style='mso-ignore:colspan'></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 colspan=10 class=xl65 style='height:15.0pt;mso-ignore:colspan'></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=14 style='mso-height-source:userset;height:10.5pt'>
  <td height=14 colspan=10 class=xl65 style='height:10.5pt;mso-ignore:colspan'></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=21 style='height:15.75pt'>
  <td colspan=5 height=21 class=xl160 >EMPRESA</td>
  
  <td colspan=2 class=xl160 >RFC:</td>
 
  <td colspan=3 class=xl160 >Telefonos:</td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=24 >
  <td colspan=5 height=24 class=xl270 >

  <?php 
  	if($tipoCliente==2)
		{
			echo $filaCl[0];
			
		}
  		else
			{
				echo $filaCl[0].' '.$filaCl[30].', '.$filaCl[37];
			}
			?>
  
  
  </td>
  <td colspan=2 class=xl106 >
  <?php 
  	if($tipoCliente==2)
		{
		 echo $filaCl[1]."-".$filaCl[37]."-".$filaCl[38];
	
		}
  		else
			{
  			echo $filaCl[1]."-".$filaCl[38]."-".$filaCl[39];
			}
?>
			
  </td>
  <td colspan=3 class=xl106 >'<?php echo $filaCl[6]?></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=21 >
  <td height=19 class=xl160 colspan=3 >REPRESENTANTE LEGAL:</td>
  
  <td colspan=2 class=xl160 >RELACION CON
  LA EMPRESA</td>
  <td colspan=2 class=xl160 >CORREO ELECTRONICO</td>
  
  <td class=xl160 colspan=3 >Nextel, Celular:</td>
  
  <td class=xl97></td>
 </tr>
 
 	<?php
	if($filaCl[32]!="")
	{
		  $arrRepresentantes=explode(",",$filaCl[32]);
		  foreach($arrRepresentantes as $representante)
		  {
			  $arrDatosRep=explode("_",$representante);
	?>
	
	
            <tr class=xl65 height=20 style='height:15.0pt'>
                <td colspan=3 height=20 class=xl108 >
                  <?php
                      $consulta="select * from 703_clientes where idCliente=".$arrDatosRep[0];
                      $filaDatosC=$con->obtenerPrimeraFila($consulta);
                      echo  utf8_decode($filaDatosC[5]." ".$filaDatosC[1]." ". $filaDatosC[2]);
                  ?>
                </td>
                <td colspan=2 class=xl108 >		
				<?php
					  $consulta="select  relacion from 706_relacionSocio where idRelacionSocio='".$arrDatosRep[1]."'";
					  $relacion=$con->obtenerValor($consulta);
					  echo utf8_decode( $relacion);
				?>
                </td>
                <td colspan=2 class=xl108><?php echo utf8_decode($filaDatosC[10])?></td>
                <td colspan=3 class=xl106 >'<?php echo $filaDatosC[9] ?></td>
                <td class=xl97></td>
            </tr>
		<?php
		  }
	}
		?>            
 
	<tr class=xl65 height=20 >
      	<td colspan=3 height=20 class=xl160 >SOCIOS:</td>
      	<td colspan=2 class=xl160 >CARGO</td>
      	<td colspan=2 class=xl160 >PORCENTAJE</td>
      	<td colspan=3 class=xl160 >EXPERIENCIA EN EL SECTOR</td>
      	<td class=xl97></td>
	</tr>

  <?php
      	$consulta="select if(tipoSocio=1,(select nombre from 700_vclientes where idCliente=idClienteSocio),(select empresa from 700_empresas where idEmpresa=idClienteSocio) )as socio, s.idClienteSocio, rs.relacion,porcentaje,experiencia,tipoSocio from 715_sociosCliente s,706_relacionSocio rs where rs.idRelacionSocio=s.idCargo and idCliente=".$idCliente;
      	$resSoc=$con->obtenerFilas($consulta);
      	while($filaSoc=mysql_fetch_row($resSoc))
      	{
  ?>
         	<tr class=xl65 height=20 >
            	<td colspan=3 height=20 class=xl106 ><?php echo utf8_decode($filaSoc[0]) ?></td>
              	<td colspan=2 class=xl106 ><?php echo utf8_decode($filaSoc[2]) ?></td>
             	<td colspan=2 class=xl106 ><?php echo utf8_decode($filaSoc[3]) ?></td>
              	<td colspan=3 class=xl106 ><?php echo utf8_decode($filaSoc[4]) ?></td>
              	<td class=xl97></td>
         	</tr>
  <?php
		}
  ?>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >EMPRESA FAMILIAR:</td>
  <td class=xl106 >
  									<?php
									if($tipoCliente==2)
								{
									$valorFam=$filaCl[30];
										if ($valorFam=="")
											$valorFam=-1;
										
									
									
										$consulta="select texto from 1004_siNo where idIdioma=".$_SESSION["leng"]." and valor=".$valorFam;
										$valorFam=$con->obtenerValor($consulta);
										echo utf8_decode($valorFam);
								}
									?>
  </td>
  <td colspan=8 class=xl107 >&nbsp;</td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl160 >DOMICILIO FISCAL:</td>
  	<td colspan=9 align=left class=xl108 >
	<?php echo utf8_decode($filaCl[3])?>&nbsp;<?php echo utf8_decode($filaCl[11])?>,&nbsp;Col.&nbsp;
	<?php echo utf8_decode($filaCl[4])?>,&nbsp;Del.&nbsp;
	<?php echo utf8_decode($filaCl[33])?>,&nbsp;C.P. <?php echo $filaCl[12]?>,&nbsp;Cd.&nbsp;<?php echo utf8_decode($filaCl[5])?></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160>&nbsp;</td>
  <td colspan=2 class=xl107>
  								<?php
									$valProp=$filaCl[24];
									if ($valProp=="")
										$valProp=-1;
									
								  	$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$valProp;
									$tipo=$con->obtenerValor($consulta);
									echo utf8_decode($tipo);
								  ?>
  </td>
  <td colspan=2 class=xl160>Tiempo de Residencia:</td>
  
  <td class=xl106 ><?php echo utf8_decode($filaCl[18])?></td>
  <td colspan=4 class=xl160 >Renta mens:</td>
  
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >Valor Declarado:</td>
  <td class=xl109 x:num="<?php echo utf8_decode($filaCl[20]) ?>"><?php echo '$ '. number_format($filaCl[20],2,".",",") ?></td>
  <td class=xl107 ></td>
  <td colspan=2 class=xl160 >Superficie Aprox.:</td>
  <td class=xl108 x:num="<?php echo $filaCl[22] ?>"><?php echo number_format($filaCl[22],2,".",",")?></td>
  <td colspan=4 class=xl109><?php echo number_format($filaCl[26],2,".",",")?></td>
 
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >DOM. PARTICULAR:</td>
  <td colspan=9 class=xl108 > <?php echo utf8_decode($filaCl[13])?> <?php echo utf8_decode($filaCl[16])?>, C.P. <?php echo utf8_decode($filaCl[17])?>, Col. <?php echo utf8_decode($filaCl[14])?>, Del. <?php echo utf8_decode($filaCl[34])?>, Cd. <?php echo utf8_decode($filaCl[15])?></td>
  
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >&nbsp;</td>
  <td colspan=2 class=xl107><?php
  									$valProp=$filaCl[25];
									if ($valProp=="")
										$valProp=-1;
									
									
								  	$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$valProp;
									$tipo=$con->obtenerValor($consulta);
									echo utf8_decode($tipo);
								  ?></td>
  <td colspan=2 class=xl160 >Tiempo de Residencia:</td>
  
  <td class=xl106 ><?php echo utf8_decode($filaCl[19])?></td>
  <td colspan=4 class=xl160 >Renta mens:</td>
 
 </tr>
 <tr class=xl65 height=22 >
  <td height=22 class=xl160 >Valor Declarado:</td>9
  <td class=xl109 ><?php echo number_format($filaCl[21],2,".",",")?></td>
  <td class=xl107 >&nbsp;</td>
  <td class=xl160 colspan=2 >Superficie Aprox.:</td>
  <td class=xl106 ><?php echo number_format($filaCl[23],2,".",",")?></td>
  <td colspan=4 class=xl109 ><?php echo number_format($filaCl[27],2,".",",")?></td>
  <td class=xl97></td>
 </tr>
 <tr>
 <td colspan=9 class=xl107 height=22>&nbsp;</td>
 </tr>
 <tr class=xl65 height=22>
  <td height=22 class=xl160 >Actividad Principal :</td>
  <td colspan=5 class=xl108 ><?php
									echo utf8_decode($filaCl[29]);
								?></td>
  <td class=xl160 colspan=2 >Antig�edad en Hacienda:</td>
  <td colspan=2 class=xl108 >
  <?PHP 
  		if($filaCl[2]!="")
		  	echo "'".date("d/m/Y",strtotime($filaCl[2])) 
  ?>
  </td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >Principales Clientes:</td>
  <td colspan=9 class=xl108 >
  
  <?php
		$consulta="select idPrincipalesClientes,e.empresa from 715_principalesClientes pc,700_empresas e where e.idEmpresa=pc.idClientePrincipal and pc.idCredito=".$idCredito;
		$resCli=$con->obtenerFilas($consulta);
		while($filaCli=mysql_fetch_row($resCli))
			{
				echo utf8_decode($filaCli[1]).'. ' ;
        	}
	?>

  
  
  
  </td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=21 style='height:15.75pt'>
  <td height=21 class=xl160 >Pagina WEB:</td>
  <td colspan=5 class=xl108 ><?php echo utf8_decode($filaCl[8])?></td>
 
  <td colspan=2 class=xl160 >Antig�edad de Operaci�n:</td>
  
  <td colspan=2 class=xl106 ><span class="xl1081"><?PHP echo "'".date("d/m/Y",strtotime($filaCl[2])) ?></span></td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl160 style='height:15.0pt'>N� Empleados:</td>
  <td class=xl106 ><?php echo $filaCl[9] ?></td>
  <td colspan=4 class=xl107>&nbsp;</td>
  <td colspan=2 class=xl160 >N�mina Mensual:</td>
  <td colspan=2 class=xl109><?php echo $filaCl[10] ?></td>

  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20>
  <td height=20 class=xl160 >Monto Solicitado:</td>
  <td colspan=2 class=xl109 ><?php
									$valor=number_format($filaCred[8],2,'.',',');
										echo $valor;
								?>
                                
    </td>
  <td class=xl160 colspan=2>MontoSugerido por RC:</td>
  <td class=xl109 ><?php
									$valor=number_format($filaCred[9],2,'.',',');
									echo $valor;
									
								?> </td>
  <td colspan=4 class=xl107 >&nbsp;</td>
  <td class=xl97></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl160 >Destino de Credito:</td>
  <td class=xl108 colspan=9 ><?php echo utf8_decode($filaCred[22]) ?></td>
  <td class=xl98></td>
 </tr>
 
  <tr>
 <td colspan=10 class=xl107 height=22>&nbsp;</td>
 </tr>
 
 <tr class=xl65 height=21 >
   <td height=21 colspan=3  class=xl160 >CUENTAS DE CHEQUES DEL SOLICITANTE Y EMPRESAS DEL GRUPO:</td>
   <td colspan=7 class=xl160 >SALDO PROMEDIO ULTIMOS 6 MESES</td>
   <td class=xl100></td>
 </tr>
 <tr class=xl65 height=20 >
  <td height=20 class=xl87 >N� CUENTA</td>
  <td class=xl87 >BANCO</td>
  <td class=xl87>TITULAR</td>
  <?php
  	$arrMeses[0]="Ene";
	$arrMeses[1]="Feb";
	$arrMeses[2]="Mar";
	$arrMeses[3]="Abr";
	$arrMeses[4]="May";
	$arrMeses[5]="Jun";
	$arrMeses[6]="Jul";
	$arrMeses[7]="Ago";
	$arrMeses[8]="Sep";
	$arrMeses[9]="Oct";
	$arrMeses[10]="Nov";
	$arrMeses[11]="Dic";
	$consulta="select fechaEntrada from 750_creditos where idCredito=".$idCredito;
	
	$fechaRef=$con->obtenerValor($consulta);
	$fecha=strtotime($fechaRef);
	$nFecha=strtotime('-6 months',$fecha);
	$arrTitulos=array();
	for($x=0;$x<6;$x++)
	{
		$fechaV=strtotime('+'.$x.' months',$nFecha);
		$mes=date('m',$fechaV);
		$mesLetra=$arrMeses[$mes-1];
		$anio=date('Y',$fechaV);
		$titulo=$mesLetra."-".$anio;
		array_push($arrTitulos,$titulo);
	}
	foreach($arrTitulos as $titulo)
	{
		echo "<td class=xl87 >'".$titulo."</td>";
	}
	
	$consulta="select id_713_saldoCuentasBanco,
				cuenta,
				(select nombreBan from 600_bancos where idBanco=s.idBanco) as banco,
				if(s.tipoTitular=1,(select nombre from 700_vclientes where idCliente=s.idTitular),
				(select empresa from 700_empresas where idEmpresa=s.idTitular)) as titular,
				saldoM6,saldoM5,saldoM4,saldoM3,saldoM2,saldoM1,antiguedad,'Saldo promedio ultimos 6 meses' as cuentasCliente from 713_saldosCuentasBanco s
			where idCredito=".$idCredito;
	$resSaldo=$con->obtenerFilas($consulta);
	
  ?>
  <td class=xl87 >Antiguedad de cta</td>
  <td class=xl100></td>
 </tr>
 
 <?php
 	$arrSuma=array();
	$arrSuma[0]=0;
	$arrSuma[1]=0;
	$arrSuma[2]=0;
	$arrSuma[3]=0;
	$arrSuma[4]=0;
	$arrSuma[5]=0;
	
 	while($fila=mysql_fetch_row($resSaldo))
	{
		$arrSuma[0]+=$fila[4];
		$arrSuma[1]+=$fila[5];
		$arrSuma[2]+=$fila[6];
		$arrSuma[3]+=$fila[7];
		$arrSuma[4]+=$fila[8];
		$arrSuma[5]+=$fila[9];
 ?>
 	<tr class=xl65 height=22 >
      <td height=22 class=xl108 ><?php echo utf8_decode($fila[1])?></td>
      <td class=xl108 ><?php echo utf8_decode($fila[2])?></td>
      <td class=xl108 ><?php echo utf8_decode($fila[3])?></td>
      <td class=xl109><?php echo $fila[4] ?></td>
      <td class=xl109 ><?php echo $fila[5]?></td>
      <td class=xl109 ><?php echo $fila[6]?></td>
      <td class=xl109 ><?php echo $fila[7]?></td>
      <td class=xl109 ><?php echo $fila[8]?></td>
      <td class=xl109 ><?php echo $fila[9]?></td>
      <td class=xl109 ><?php echo utf8_decode($fila[10])?></td>
	</tr>
 <?php
	}
 ?>
 <tr class=xl65 height=23 >
   <td colspan=3 height=23 class=xl271>TOTAL</td>
   <td class=xl109 ><?php echo $arrSuma[0]?></td>
   <td class=xl109 ><?php echo $arrSuma[1]?></td>
   <td class=xl109 ><?php echo $arrSuma[2]?></td>
   <td class=xl109 ><?php echo $arrSuma[3]?></td>
   <td class=xl109 ><?php echo $arrSuma[4]?></td>
   <td class=xl109 ><?php echo $arrSuma[5]?></td>
   <td class=xl272 >&nbsp;</td>
   
 </tr>
 <tr class=xl65 height=26 style='mso-height-source:userset;height:19.5pt'>
  <td height=26 class=xl207 style='height:19.5pt;border-top:none'>&nbsp;</td>
  <td class=xl72 style='border-top:none'>&nbsp;</td>
  <td class=xl73 style='border-top:none'>&nbsp;</td>
  <td colspan=3 class=xl271width=100 >SALDOS PROMEDIO SEMESTRAL:</td>
 
  <td class=xl87 >Saldo Promedio Semestral</td>
  <td colspan=3 class=xl109><?php echo ($arrSuma[0]+$arrSuma[1]+$arrSuma[2]+$arrSuma[3]+$arrSuma[4]+$arrSuma[5])/6 ?> </td>
 
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl207 style='height:15.0pt;border-top:none'>&nbsp;</td>
  <td class=xl72 style='border-top:none'>&nbsp;</td>
  <td class=xl73 style='border-top:none'>&nbsp;</td>
  <td colspan=7 class=xl87 style='border-right:1.5pt solid black'>DEPOSITOS
  ULTIMOS 6 MESES</td>
  <td class=xl101></td>
 </tr>
 <tr class=xl65 height=20 style='height:15.0pt'>
  <td height=20 class=xl87 >N� CUENTA</td>
  <td class=xl87 >BANCO</td>
  <td class=xl87 >TITULAR</td>
  <?php
  foreach($arrTitulos as $titulo)
	{
		echo "<td class=xl87 >'".$titulo."</td>";
	}
	?>
  <td class=xl87 >ANTIG�EDAD</td>
  <td class=xl100></td>
 </tr>
 <?php
 	$consulta="	select id_714_depositoCuentasBanco,
				cuenta,
				(select nombreBan from 600_bancos where idBanco=s.idBanco) as banco,
				if(s.tipoTitular=1,(select nombre from 700_vclientes where idCliente=s.idTitular),(select empresa from 700_empresas where idEmpresa=s.idTitular)) as titular,
				depositoM6,depositoM5,depositoM4,depositoM3,depositoM2,depositoM1,antiguedad from 714_depositosCuentasBanco s
				where idCredito=".$idCredito;
	$arrSuma[0]=0;
	$arrSuma[1]=0;
	$arrSuma[2]=0;
	$arrSuma[3]=0;
	$arrSuma[4]=0;
	$arrSuma[5]=0;
	$resSaldo=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($resSaldo))
	{
		$arrSuma[0]+=$fila[4];
		$arrSuma[1]+=$fila[5];
		$arrSuma[2]+=$fila[6];
		$arrSuma[3]+=$fila[7];
		$arrSuma[4]+=$fila[8];
		$arrSuma[5]+=$fila[9];
 ?>
     <tr class=xl65 height=22 >
      <td height=22 class=xl108 ><?php echo utf8_decode($fila[1])?></td>
      <td class=xl108><?php echo utf8_decode($fila[2])?></td>
      <td class=xl108><?php echo utf8_decode($fila[3])?></td>
      <td class=xl108><?php echo $fila[4]?> </td>
      <td class=xl108><?php echo $fila[5]?> </td>
      <td class=xl108><?php echo $fila[6]?> </td>
      <td class=xl108><?php echo $fila[7]?> </td>
      <td class=xl108><?php echo $fila[8]?> </td>
      <td class=xl108><?php echo $fila[9]?> </td>
      <td class=xl108><?php echo utf8_decode($fila[10])?></td>
      
     </tr>
 <?php
	}
 ?>
 <tr class=xl65 height=23 >
   <td colspan=3 height=23 class=xl271>TOTAL</td>
   <td class=xl109><?php echo $arrSuma[0]?> </td>
   <td class=xl109 style='border-left:none' ><?php echo $arrSuma[1]?> </td>
   <td class=xl109 style='border-left:none' ><?php echo $arrSuma[2]?> </td>
   <td class=xl109 style='border-left:none' ><?php echo $arrSuma[3]?> </td>
   <td class=xl109 style='border-left:none' ><?php echo $arrSuma[4]?> </td>
   <td class=xl109 style='border-left:none' ><?php echo $arrSuma[5]?> </td>
   <td class=xl272 style='border-left:none'>&nbsp;</td>
   <td class=xl102></td>
 </tr>
 <tr class=xl65 height=26 >
  <td height=26 class=xl160 >MESES CON MAYORES VENTAS EN EL A�O:</td>
  <td colspan=4 class=xl96 >
 											 <?php
                                                    if($filaCred[19]!="")
                                                    {
                                                        $consulta="select opcion from 951_catalogoOpcionesVarios where valorOpcion in (".$filaCred[19].") and tipoOpcion=4 and idIdioma=".$_SESSION["leng"]." ";
                                                        $listaMeses=$con->obtenerListaValores($consulta);
                                                        echo utf8_decode($listaMeses);
                                                    }
                                                ?>
                                                
  </td>
  
  
  <td colspan=2 class=xl87 >Saldo Promedio Semestral</td>
  <td colspan=3 class=xl109> <?php echo ($arrSuma[0]+$arrSuma[1]+$arrSuma[2]+$arrSuma[3]+$arrSuma[4]+$arrSuma[5])/6 ?></td>
  
 </tr>
 <tr class=xl65 height=22 style='mso-height-source:userset;height:16.5pt'>
  <td  height=22 class=xl160 >VENTAS ACUMULADAS A�O ANTERIOR:</td>
  
  <td class=xl96 ><?php echo $filaCred[20] ?> </td>
  
  
  <td colspan=3 class=xl160 >VENTAS ACUMULADAS A�O ACTUAL:</td>
  <td class=xl96 ><?php echo $filaCred[21] ?></td>
  
  <td colspan=4 class=xl271>&nbsp;</td>
  
  <td class=xl103></td>
 </tr>
 <tr>
 <td colspan=10 class=xl107 height=22>&nbsp;</td>
 </tr>
 </table>

<table >
	<tr height="21">
        <td class=xl160 height="26">
        AVALES
        </td>
    </tr>
	<?php
		$consulta="select * from 709_aval where idCredito=".$idCredito;
		
		$resAvales=$con->obtenerFilas($consulta);
		$ct=1;
		while($filaAval=mysql_fetch_row($resAvales))
		{
		?>
            <tr>
                <td>
                <?php
                    generarTablaAval($filaAval,$ct,$secAvales);
                    $ct++;
                ?>
                </td>
            </tr>
		<?php
		}
    ?>
</table>
<table>
 <tr height=21 >
   <td height=21 class=xl160 >BIENES MUEBLES:</td>
   <td colspan=9 class=xl108>&nbsp;</td>
   <td class=xl97></td>
 </tr>
 <tr>
 <td colspan=10 class=xl107 height=22>&nbsp;</td>
 </tr>
 
  <tr height=20 >
   <td height=20 class=xl160 >CONDICIONADO A:</td>
   <td colspan=9 class=xl108><?php echo utf8_decode($filaCred[18])?></td>
   <td class=xl97></td>
 </tr>
 <tr>
 <td colspan=10 class=xl107 height=22>&nbsp;</td>
 </tr>
 
 <tr height=21>
   <td height=21 class=xl160 >ACUERDO DE COMIT�</td>
   <td class=xl160 >Fecha Entrada</td>
   <td class=xl160 >Fecha Comit�</td>
   <td class=xl160 >Fecha fondeo</td>
   <td colspan=2 class=xl160 >Promotor:</td>
   <td colspan=2 class=xl160 >Gerente de grupo:</td>
   <td class=xl160 colspan=2 >Captaci�n del cliente via:</td>
   <td class=xl97></td>
 </tr>
 <tr height=20 >
  <td height=20 class=xl160 >
  <?php 
  $idAcuerdo=$filaCred[15];
		if($idAcuerdo=="")
			{
					$idAcuerdo="-1";
					$consulta="select acuerdo from 753_acuerdosComite where idAcuerdoComite=".$idAcuerdo;
					echo utf8_decode($con->obtenerValor($consulta));
			}
			
			
			$idCaptacion=$filaCred[7];
			$mostrarFilaExpo=false;
			$nomExpo="";
			if($idCaptacion==12)
			{
				$mostrarFilaExpo=true;
				$consulta="select nomExpo from 750_creditos where idCredito=".$idCredito;
				$nomExpo=$con->obtenerValor($consulta);
			}
  ?>
  
  
  </td>
  <td class=xl108><?php
  						if($filaCred[4]!="")
							echo "'".date("d/m/Y",strtotime($filaCred[4]));
					?>
    </td>
  <td class=xl108><?php
  						if($filaCred[5]!="")
							echo "'".date("d/m/Y",strtotime($filaCred[5]));
					?></td>
  <td class=xl108><?php
  						if($filaCred[6]!="")
							echo "'".date("d/m/Y",strtotime($filaCred[6]));
					?></td>
  <td colspan=2 class=xl108><?php
							echo utf8_decode(obtenerNombreUSuario($filaCred[3]));
							?>
                                    
    </td>
  <td colspan=2 class=xl108>
  <?php
                                	$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$filaCred[3]." and codigoRol like '38_%'";
									$rol=$con->obtenerValor($consulta);
									if($rol!="")
									{
										$datosRol=explode("_",$rol);
										$consulta="select idUsuario from 807_usuariosVSRoles where codigoRol='39_".$datosRol[1]."'";
										$idGerente=$con->obtenerValor($consulta);
										if($idGerente!="")
											echo utf8_decode(obtenerNombreUsuario($idGerente));
										
									}
									
								?>
  </td>
  <td colspan=2 class=xl108>
  								<?php
										$consulta="select via from 716_captacionVia where idCaptacion=".$filaCred[7];
										$via=$con->obtenerValor($consulta);
										echo utf8_decode($via); 
									?>
                                
                                  </td>
  <td class=xl104></td>
 </tr>
 <?php
 	if($mostrarFilaExpo)
	{
 ?>
 <tr>
 	<td class=xl160>
    Nombre de la expo:
    </td>
    <td colspan="9" class="xl108">
    <?php
		echo $nomExpo;
	?>
    </td>
 </tr>
 <?php
 	}
 ?>
 <tr height=20 >
  <td height=20 class=xl160>Monto Solicitado:</td>
  <td class=xl160 >Monto Autorizado:</td>
  <td class=xl160 >Fecha renovacion linea</td>
  <td class=xl160 >&nbsp;</td>
  <td colspan=2 class=xl160 >Cerrador RC Corporativo:</td>
  
  <td colspan=2 class=xl160 >Cerrador Fondo:</td>
  
  <td class=xl160 >Tasa Interes:</td>
  <td class=xl160 >% Comision:</td>
  <td class=xl104></td>
 </tr>
 <tr height=17 >
  <td height=17 class=xl109>
    <?php
									$valor=number_format($filaCred[8],2,'.',',');
										echo $valor;
								?>
  </td>
  <td class=xl109>
    <?php
									$valor=number_format($filaCred[9],2,'.',',');
										echo $valor;
								?>
  </td>
  <td colspan=2 class=xl108>
  <?php
  	if($filaCred[10]!="")
		echo date("d/m/Y",strtotime($filaCred[10]));
  ?>
  &nbsp;
  </td>
  <td colspan=2 class=xl108><?php
									$idCerrador=$filaCred[11];
									if($idCerrador!="")
										echo utf8_decode(obtenerNombreUsuario($idCerrador));
									
								?></td>
  <td colspan=2 class=xl108><?php
									$idCerradorFondo=$filaCred[12];
									if($idCerradorFondo!="")
										echo utf8_decode(obtenerNombreUsuario($idCerradorFondo));
								?></td>
  <td class=xl108><?php echo utf8_decode($filaCred[13]) ?></td>
  <td class=xl108><?php echo utf8_decode($filaCred[14]) ?></td>
  <td class=xl104></td>
 </tr>
 <tr height=21 >
  <td height=21 class=xl108>&nbsp;</td>
 
  <td colspan=2  class=xl160 style='border-top:none'>Conferencia Capital Humano</td>
  
  <td class=xl108 colspan=7><?php echo utf8_decode($filaCred[14]) ?></td>
  <td class=xl104></td>
 </tr>
 <tr class=xl65 height=20 >
  <td colspan=2 height=20 class=xl160 >Status de la operaci�n</td>
  <td class=xl108 colspan=8>&nbsp;</td>
  <td class=xl97></td>
 </tr>

 
</table>
</body>

</html>
