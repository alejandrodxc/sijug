<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
$paramPOST=true;
$paramGET=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}


?>
<title><?php echo $tituloPagina ?></title>
<style type="text/css">
<!--
@import url("../css/estiloFicha.css");
-->
</style>
</head>
<body>
	<br />
    <?php
		$idCredito="-1";
		if(isset($objParametros->idCredito))
			$idCredito=$objParametros->idCredito;
		$consulta="select * from 750_creditos where idCredito=".$idCredito;
		$fila=$con->obtenerPrimeraFila($consulta);
		
		$mEtapa=false;
			$arrEtapas=array();
			if(!existeRol("'1_0'"))
			{
				switch($fila[17])
				{
					case 1:
						if(existeRol("'38_-1'")) //Promotor
						{
							array_push($arrEtapas,2);
						}
					break;
					case 2:
						if(existeRol("'41_0'")) //Analisis
						{
							array_push($arrEtapas,3);
						}
					break;
					case 3:
						if(existeRol("'42_0'")) //DG
						{
							array_push($arrEtapas,2);
							array_push($arrEtapas,4);
							array_push($arrEtapas,7);
							array_push($arrEtapas,9);
						}
					break;
					case 4:
						if(existeRol("'42_0'")) //DG
						{
							array_push($arrEtapas,2);
							array_push($arrEtapas,7);
							array_push($arrEtapas,9);
							array_push($arrEtapas,11);						
						}
						if(existeRol("'43_0'")) //Comite
						{
							array_push($arrEtapas,2);
							array_push($arrEtapas,7);
							array_push($arrEtapas,9);
							array_push($arrEtapas,11);						
						}
					
					break;
					case 5:
						if(existeRol("'39_-1'")) //Gerente
						{
							array_push($arrEtapas,6);
							
						}
						if(existeRol("'40_0'")) //Cerrador
						{
							array_push($arrEtapas,6);
						}
					break;
					case 6:
						if(existeRol("'39_-1'")) //Gerente
						{
							array_push($arrEtapas,10);
							
						}
						if(existeRol("'40_0'")) //Cerrador
						{
							array_push($arrEtapas,10);
						}
					break;
					case 7:
					break;
					case 8:
					break;
					case 9:
					case 10:
					break;
					case 11:
						if(existeRol("'39_-1'")) //Gerente
						{
							array_push($arrEtapas,5);
							array_push($arrEtapas,8);
							array_push($arrEtapas,7);
						}
						if(existeRol("'40_0'")) //Cerrador
						{
							array_push($arrEtapas,5);
							array_push($arrEtapas,8);
							array_push($arrEtapas,7);
						}
					break;
				}
			}
			else
			{
				$consulta="select numEstado,estadoCredito from 752_estadosCredito where numEstado<>".$fila[17]." order by numEstado";
				$resEdo=$con->obtenerFilas($consulta);
				while($filaEdo=mysql_fetch_row($resEdo))
				{
					array_push($arrEtapas,$filaEdo[0]);
					
				}
			}		
			$mostrarEliminar=false;		
			if((existeRol("'42_0'"))||(existeRol("'41_0'"))||(existeRol("'1_0'")))
				$mostrarEliminar=true;
				

	?>
    <table width="300">
    <tr height="21">
    	<td width="5"></td>
    	<td class="filaAzul10" width="105">Folio:</td>
        <td class="copyrigthSinPadding">&nbsp;&nbsp;<?php echo $fila[23] ?></td>
    </tr>
    
    
    <tr height="21">
    	<td width="5"></td>
    	<td class="filaAzul10" width="105">Fecha de registro:</td>
        <td class="copyrigthSinPadding">&nbsp;&nbsp;<?php echo date('d/m/Y',strtotime($fila[4]))?>&nbsp;<a href="javascript:modificarFechaRegistro('<?php echo bE(date('d/m/Y',strtotime($fila[4])))?>')"><img src="../images/pencil.png" alt='Modificar fecha de registro' title='Modificar fecha de registro' /></a></td>
    </tr>
    <tr height="21">
    	<td></td>
    	<td  class="filaAzul10">Registrado por: </td>
        <td class="copyrigthSinPadding">&nbsp;&nbsp;<?php echo obtenerNombreUsuario($fila[3])?></td>
    </tr>
    <tr height="21">
    	<td></td>
    	<td  class="filaAzul10">Situación actual:</td>
        <td class="copyrigthSinPadding">&nbsp;&nbsp;
        <?php
			$consulta="select estadoCredito from 752_estadosCredito where numEstado=".$fila[17];
			$estado=$con->obtenerValor($consulta);
			echo $estado;
		?>
        </td>
    </tr>
    <tr>
    	<td colspan="3" align="left">
        	<table>
            <tr height="21">
            <td width="15">
            </td>
            <td>
        	<br />
        		<a href="javascript:imprimirFormato()"><img src="../images/printer.png" /></a>&nbsp;<a href="javascript:imprimirFormato()"><span class="copyrigthSinPadding">Imprimir</span></a>
            </td>
            </tr>
            
            
            
            <tr height="21">
            	<td></td>
            	<td>
                <a href="javascript:mostrarHistorialCredito()">
                <img src='../images/magnifier.png' /></a>&nbsp;<a href="javascript:mostrarHistorialCredito()"><span class="copyrigthSinPadding">Ver historial del crédito</span>
                </a>
                </td>
            </tr>
            <?php 
				if($mostrarEliminar)
				{
			?>
             <tr height="21">
            	<td></td>
            	<td>
                <br />
                <a href="javascript:eliminarCredito()">
                <img src='../images/cancel_round.png' /></a>&nbsp;<a href="javascript:eliminarCredito()"><span class="copyrigthSinPadding">Eliminar crédito</span>
                </a>
                </td>
            </tr>
            <?php	
				}
			?>
            </table>
        </td>  
    </tr>
    
    <tr>
    	<td colspan="3" class="">
        
        <?php
			
			if(sizeof($arrEtapas)>0)
			{
				$consulta="select numEstado,estadoCredito from 752_estadosCredito order by numEstado";
				$resAc=$con->obtenerFilas($consulta);
				$arrAcciones=array();
				while($filaAc=mysql_fetch_row($resAc))
				{
					if(existeValor($arrEtapas,$filaAc[0]))
					{
						$obj[0]=$filaAc[0];
						$obj[1]=$filaAc[1];
						array_push($arrAcciones,$obj);
					}
				}
				$mEtapa=true;
			}
				
			if($mEtapa)
			{
		?>
                <table id="hor-minimalist-b" style="width:100% !important">
                    <thead>
                    <tr>
                        <th colspan="3">
                        Enviar a etapa:
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    
                        
						
                        
                        
                        foreach($arrAcciones as $accion)
                        {
                            echo "	<tr>
                                        <td><img src='../images/bullet_green.png'></td>
                                        <td><a href='javascript:enviarEtapa(\"".bE($accion[0])."\")'>".$accion[0].".- ".$accion[1]."</td>
                                    </tr>";
                        }
                    ?>
                    </tbody>
                </table>
    	<?php
			}
		?>
        </td>
    </tr>
    </table>
</body>
</html>