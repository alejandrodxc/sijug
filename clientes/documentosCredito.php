<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />

<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
$paramPOST=true;
$paramGET=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$arrValores=null;
$arrLlaves=null;
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$guardarConfSession=true;
$pagRegresar="../principal/inicio.php";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);

if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}


?>
<title><?php echo $tituloPagina ?></title>
<style type="text/css">
<!--
@import url("../css/estiloFicha.css");
-->
</style>
</head>
<body>
	<br />
    <?php
		$idCredito="-1";
		if(isset($objParametros->idCredito))
			$idCredito=$objParametros->idCredito;
		$consulta="select * from 750_creditos where idCredito=".$idCredito;
		$fila=$con->obtenerPrimeraFila($consulta);
		$sL=false;
	?>
    <table>
	<tr>
    	<td colspan="3">
        <table id="hor-minimalist-b" style="width:100% !important">
        <thead>
    	<tr>
        	<th colspan="3">
            <span class="letraExt">Para agregar un documento de click </span><a href="javascript:subirDocumento()"><span style="color:#F00"><b>AQUÍ</b></span></a>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
			$consulta="select idAnexo,titulo,descripcion from 751_documentosAnexos where idCredito=".$idCredito;
			$resAnexo=$con->obtenerFilas($consulta);
			if($con->filasAfectadas>0)
			{
				while($fAnexo=mysql_fetch_row($resAnexo))
				{
			?>
			<tr id='fila_<?php echo $fAnexo[0]?>'>
				<td width="10">
				<img src="../images/bullet_green.png" />
				</td>
				<td width="60">
				<?php 
					if(!$sL) 
					{
				?>
					<a href="../clientes/obtenerDocumentoAnexo.php?doc=<?php echo base64_encode($fAnexo[0])?>">
					<img src="../images/Save.png" title='Descargar documento' alt='Descargar documento' height="16"  width="16" />
					</a>&nbsp;
					<a href="javascript:removerAnexo('<?php echo base64_encode($fAnexo[0])?>')">
					<img src="../images/cancel_round.png" title='Eliminar documento' alt='Eliminar documento' />
					</a>
				<?php
					}
				?>
				&nbsp;
				</td>
				<td>
				<a href="../clientes/obtenerDocumentoAnexo.php?doc=<?php echo base64_encode($fAnexo[0])?>" alt='<?php echo $fAnexo[2]?>' title='<?php echo $fAnexo[2]?>'>
				<span class="letraExt">
				<?php
					echo $fAnexo[1];
				?>
                </span>
				</a>
				</td>
				
			</tr>
			<?php		
				}
			}
			else
			{
				echo "<tr><td>Sin documentos asociados</td></tr>";
			}
		?>
        </tbody>
    </table>
        </td>
    </tr>
</table>
</body>
</html>