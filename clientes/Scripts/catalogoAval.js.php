<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select idRelacionParentesco, parentesco from 713_relacionParentesco order by parentesco";
	$res=$con->obtenerFilas($consulta);
	$cadOpciones="";
	while($fila=mysql_fetch_row($res))
	{
		$opt="<option value='".$fila[0]."'>".uEJ($fila[1])."</option>";
		$cadOpciones.=$opt;
	}
?>

Ext.onReady(inicializar);
var criterioB;
function inicializar()
{
	var idAval=gE('idAval').value;
     var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:850,
                                        height:450,
                                        items: [
                                                    {
                                                       title: 'Aval',
                                                       contentEl:'tblGral'
                                                    }
                                                ]
                                    }
                               );
    var mascara=new Mask('$ ###,##0.00','number');
    var _valorDeclaradoflo=gE('_valorDeclaradoflo');
	_valorDeclaradoflo.value=mascara.format(_valorDeclaradoflo.value);
    mascara.attach(_valorDeclaradoflo);
    
}

function validarFrm()
{
	
    var arrCombosRep=gEN('cmbRelaciones');
    var x;
    var datosCombo;
    var relacion;
    var obj='';
    var cadObj='';
    for(x=0;x<arrCombosRep.length;x++)
    {
    	datosCombo=arrCombosRep[x].id.split('_');
        relacion=obtenerValorSelect(arrCombosRep[x]);
        obj=datosCombo[1]+'_'+relacion+'_'+datosCombo[2];
        if(cadObj=='')
        	cadObj=obj;
        else
        	cadObj+=','+obj;
    }
    
    gE('_idClienteint').value=cadObj;
	if(validarFormularios('frmEnvio'))
    {
    	gE('frmEnvio').submit();
    }
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}


function cerrarVentana()
{
	TB_remove();
}

var filaCliSel;

function mostrarVentanaAgregarAval()
{
	criterioB='1';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'},
                                                {name: 'mail'},
                                                {name: 'telefono'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    cmbNombre.setPosition(120,45);
    cmbNombre.setWidth(350);
    cmbNombre.on('select',function(cmb,registro)	
    						{
                            	filaCliSel=registro;
                            }
    			)
    var lblEtiqueta;
    if(criterioB=='1')
    {
    	lblEtiqueta='Nombre del aval:';
        lblDebe='Debe seleccionar un aval';
        lblEtiquetaNoExiste='&iquest;No existe el aval? agr&eacute;guelo ';
        lblTitulo='Agregar aval';
    }
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Tipo de aval:'
                                                       },
                                                       {
                                                   			xtype:'radio',
                                                            id:'chkCliente',
                                                            x:110,
                                                            y:5,
                                                            name:'rdoCliente',
                                                            value:'1',
                                                           	boxLabel :'Cliente',
                                                            checked:true,
                                                            listeners:	{
                                                                                check:function(chk,estado)
                                                                                	{
                                                                                    	if(estado)
                                                                                        {
                                                                                            criterioB=1;
                                                                                            cmbNombre.reset();
                                                                                            cmbNombre.focus();
																						}                                                                                        
                                                                                    }
                                                                        } 
                                                            
                                                        },
                                                        {
                                                        	xtype:'radio',
                                                        	id:'chkEmpresa',
                                                            value:'2',
                                                            name:'rdoCliente',
                                                            x:200,
                                                            y:5,
                                                           	boxLabel :'Empresa',
                                                            listeners:	{
                                                                                check:function(chk,estado)
                                                                                	{
                                                                                    	if(estado)
                                                                                        {
                                                                                            criterioB=2;
                                                                                            cmbNombre.reset();
                                                                                            cmbNombre.focus();
																						}                                                                                        
                                                                                    }
                                                                        } 
                                                        },
														{
                                                        	x:10,
                                                            y:50,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:270,
                                                            y:75,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarAval()"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        ventanaAM.close();
                                                                        
                                                                        var registro=filaCliSel;
                                                                        var obj='[{"idRep":"'+registro.get('idUsuario')+'","nombre":"'+registro.get('Nombre')+'","mail":"'+registro.get('mail')+'","tel":"'+registro.get('telefono')+'"}]';
                                                                       	llenarDatosAval(obj);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]

									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreCliente(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	
        var idCredito=gE('_idCreditoint').value;
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
        dSet.baseParams.idCredito=idCredito;
		dSet.baseParams.campoBusqueda=criterioB;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    var idCredito=gE('idCredito').value;
    vAgregarC.close();
	if(tc==1)
		TB_show(lblAplicacion,'../clientes/catalogoClientes.php?cPagina=sFrm=true&idCliente=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
	else
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresas.php?cPagina=sFrm=true&idEmpresa=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
}

function llenarDatosAval(cadObj)
{
	
	var obj=eval(cadObj)[0];
    var idRep=obj.idRep;
    var arrResp=new Array();
    arrResp[1]=obj.nombre;
    arrResp[2]=obj.mail;
    arrResp[3]=obj.tel;
	TB_remove();
    var tblAvales=gE('tblAvales');
    var nFila=tblAvales.insertRow(-1);
    nFila.setAttribute('id','fila1_'+idRep+'_'+criterioB);
    nFila.setAttribute('height','21');
    var celdaLblPropietario=nFila.insertCell(-1);
    setClase(celdaLblPropietario,'filaRosa10');
    celdaLblPropietario.innerHTML='Propietario <span name="lblContador">'+((tblAvales.rows.length+1)/2)+'</span>: &nbsp;<a href="javascript:removerPropietario(\''+bE(idRep+'')+'\',\''+bE(criterioB+'')+'\')"><img src="../images/delete.png" title="Remover propietario" alt="Remover propietario" /></a>';
    celdaLblPropietario.setAttribute('width','110');
    var celdaPropietario=nFila.insertCell(-1);
    setClase(celdaPropietario,'filaAzul10');
    celdaPropietario.innerHTML=arrResp[1];
    celdaPropietario.setAttribute('width','250');
    var celdaLblTel=nFila.insertCell(-1);
    setClase(celdaLblTel,'filaRosa10');
    celdaLblTel.innerHTML='Tel&eacute;fono:';
    celdaLblTel.setAttribute('width','120');
    var celdaTel=nFila.insertCell(-1);
    setClase(celdaTel,'filaAzul10');
    celdaTel.innerHTML=arrResp[3];
    celdaTel.setAttribute('width','230');
    var nFila=tblAvales.insertRow(-1);
    nFila.setAttribute('id','fila2_'+idRep+'_'+criterioB);
    var celdaLblRel=nFila.insertCell(-1);
    celdaLblRel.innerHTML='Relaci&oacute;n/Parentesco:';
    setClase(celdaLblRel,'filaRosa10');
    
    var celdaRelacion=nFila.insertCell(-1);
    setClase(celdaRelacion,'filaAzul10');
    celdaRelacion.innerHTML="<select name='cmbRelaciones' class='camp_form' id='cmbRelacion_"+idRep+"_"+criterioB+"'><?php echo $cadOpciones?></select>";
    
    var celdaLblMail=nFila.insertCell(-1);
    setClase(celdaLblMail,'filaRosa10');
    celdaLblMail.innerHTML='Correo electr&oacute;nico:';
    
    var celdaMail=nFila.insertCell(-1);
    setClase(celdaMail,'filaAzul10');
    celdaMail.innerHTML=arrResp[2];
}

function agregarAval()
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
    if(criterioB==1)
		TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosAval",1)')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
	else        
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosAval",2)')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function modificarAval(iC)
{
	
	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente='+iC+'&ejecutarExt='+Base64.encode('window.parent.llenarDatosAval(@idRegistro)')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function removerPropietario(iP,tP)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var fila=gE('fila1_'+bD(iP)+'_'+bD(tP));
        	fila.parentNode.removeChild(fila);
            fila=gE('fila2_'+bD(iP)+'_'+bD(tP));
        	fila.parentNode.removeChild(fila);
            var arrCt=gEN('lblContador');
            var x;
            for(x=1;x<=arrCt.length;x++)
            {
            	arrCt[x-1].innerHTML=x;
            }
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover al propietario seleccionado?',resp)
}