<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="select a.idAval,b.nombres,b.paterno,b.materno,b.idCliente,c.Nombre,d.folio from 709_aval a, 703_clientes b, 802_identifica c, 750_creditos d where b.idResponsable=c.idUsuario and a.idCliente=b.idCliente order by b.paterno, b.materno, b.nombres ";
	$arrAval=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrAval?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idAval'},
                                                                    {name: 'paterno'},
                                                                    {name: 'materno'},
                                                                    {name: 'nombres'},
                                                                     {name: 'idCliente'},
                                                                    {name: 'nombre'},
                                                                    {name: 'credito'}                                                                ]
                                                    	}
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'aval',
															width:250,
															sortable:true,
															dataIndex:'idAval'
														},
                                                        {
															header:'Paterno',
															width:250,
															sortable:true,
															dataIndex:'paterno'
														},
                                                        {
															header:'Materno',
															width:250,
															sortable:true,
															dataIndex:'materno'
														},
                                                        {
															header:'Nombre',
															width:250,
															sortable:true,
															dataIndex:'nombres'
														},
														{
															header:'Promotor',
															width:200,
															sortable:true,
															dataIndex:'nombre'
														},
                                                        {
															header:'Credito',
															width:200,
															sortable:true,
															dataIndex:'credito'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:650,
                                                            renderTo:'tblAval',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nuevo aval',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idAval','-1']];
                                                                                    	enviarFormularioDatos('catalogoAval.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar aval',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar al aval a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idAval=fila.get('idAval');
                                                                                       var arrParam=[['idAval',idAval]];
                                                                                    	enviarFormularioDatos('catalogoAval.php',arrParam);
                                                                                    }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
}

