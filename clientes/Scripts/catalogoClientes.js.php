<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$idCliente=base64_decode($_GET["idCliente"]);
?>

var registroClienteP=Ext.data.Record.create	(
                                                [
                                                    {name: 'idClienteP'},
                                                    {name: 'cliente'}
                                                ]
                                            )

Ext.onReady(inicializar);

function inicializar()
{
	var idEmpresa=gE('idCliente').value;
    
    var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:890,
                                        height:700,
                                        items: [
                                                    {
                                                       title: 'Persona',
                                                       contentEl:'tblGral'
                                                    }
                                                    
                                                     
                                                ]
                                    }
                               );
                               
	
	crearCampoFecha('sp_fechaAltaSatdte','_fechaAltaSatdte');
    var mascara=new Mask('$ ###,##0.00','number');
    var _nominaMensualflo=gE('_nominaMensualflo');
	_nominaMensualflo.value=mascara.format(_nominaMensualflo.value);
    mascara.attach(_nominaMensualflo);
      
    var _rentaResidenciafflo=gE('_rentaResidenciafflo');
	_rentaResidenciafflo.value=mascara.format(_rentaResidenciafflo.value);
    mascara.attach(_rentaResidenciafflo);
    
     var _valorResidenciafflo=gE('_valorResidenciafflo');
	_valorResidenciafflo.value=mascara.format(_valorResidenciafflo.value);
    mascara.attach(_valorResidenciafflo);
    
    var _rentaResidenciavflo=gE('_rentaResidenciavflo');
	_rentaResidenciavflo.value=mascara.format(_rentaResidenciavflo.value);
    mascara.attach(_rentaResidenciavflo);
    
     var _valorResidenciavflo=gE('_valorResidenciavflo');
	_valorResidenciavflo.value=mascara.format(_valorResidenciavflo.value);
    mascara.attach(_valorResidenciavflo);
}

function validarFrm()
{
	var Representantes='';
    var arrCombosRep=gEN('relacionesRepresentante');
    var x;
    var datosCombo;
    var relacion;
    var obj='';
    var cadObj='';
    for(x=0;x<arrCombosRep.length;x++)
    {
    	datosCombo=arrCombosRep[x].id.split('_');
        relacion=obtenerValorSelect(arrCombosRep[x]);
        obj=datosCombo[1]+'_'+relacion;
        if(cadObj=='')
        	cadObj=obj;
        else
        	cadObj+=','+obj;
    }
    gE('_idRepresentanteLegalvch').value=cadObj;
	if(validarFormularios('frmEnvio'))
    {
    	if(!gEx('f_sp_fechaAltaSatdte').isValid())
        {
        	msgBox('La fecha de alta en hacienda no es v&aacute;lida');
        	return;
        }
    	var idCliente=gE('idCliente').value;
    	var rfc1=gE('_rfcvch').value;
        var rfc2=gE('_RFC2int').value;
        var rfc3=gE('_RFC3vch').value;
        var paterno=gE('_paternovch').value;
        var materno=gE('_maternovch').value;
        var nombre=gE('_nombresvch').value;
    	function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
            	gE('frmEnvio').submit();
            }
            else
            {
            	switch(arrResp[0])
                {
                	case '2':
                    	msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la persona: '+arrResp[1]);
                        return;
                    break;
                    case '3':
                    	msgBox('El nombre ingresado ya ha sido registrado previamente');
                        return;
                    break;
                	default:
	                	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=17&idCliente='+idCliente+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&paterno='+cv(paterno)+'&materno='+cv(materno)+'&nombre='+cv(nombre),true);
    }
}

function validarRFC()
{
	var idCliente=gE('idCliente').value;
    var rfc1=gE('_rfcvch').value;
    var rfc2=gE('_RFC2int').value;
    var rfc3=gE('_RFC3vch').value;
	var paterno=gE('_paternovch').value;
    var materno=gE('_maternovch').value;
    var nombre=gE('_nombresvch').value;
    if((rfc1.trim()=='')||(rfc2.trim()=='')||(rfc3.trim()==''))
    {
        msgBox('Los datos del RFC ingresados no son v&aacute;lidos');
        return;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
			msgBox('El RFC de la persona no ha sido registrado anteriormente');
            return;
        }
        else
        {
            switch(arrResp[0])
            {
                case '2':
                    msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la persona: '+arrResp[1]);
                    return;
                break;
                default:
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=17&idCliente='+idCliente+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&paterno='+cv(paterno)+'&materno='+cv(materno)+'&nombre='+cv(nombre),true);
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}


function cerrarVentana()
{
	TB_remove();
}

var filaCliSel=null;

function mostrarVentanaAgregarRep()
{
	var criterioB='1';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'},
                                                {name: 'mail'},
                                                {name: 'telefono'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    cmbNombre.on('select',function(cmb,registro)	
    						{
                            	filaCliSel=registro;
                            }
    			)
    var lblEtiqueta;
    if(criterioB=='1')
    {
    	lblEtiqueta='Nombre del representante:';
        lblDebe='Debe seleccionar un representante';
        lblEtiquetaNoExiste='&iquest;No existe el representante? agr&eacute;guelo ';
        lblTitulo='Agregar representante';
    }
    else
    {
    	lblEtiqueta='Nombre de la empresa:';
        lblDebe='Debe seleccionar una empresa';
        lblEtiquetaNoExiste='&iquest;No existe la empresa? agr&eacute;guela ';
        lblTitulo='Agregar empresa';
    }
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:220,
                                                            y:45,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarRepresentante('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        ventanaAM.close();
                                                                        var registro=filaCliSel;
                                                                        var obj='[{"idRep":"'+registro.get('idUsuario')+'","nombre":"'+registro.get('Nombre')+'","mail":"'+registro.get('mail')+'","tel":"'+registro.get('telefono')+'"}]';
                                                                       	llenarDatosRepresentante(obj);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]

									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreCliente(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB='1';
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=criterioB;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    var idCredito=gE('idCredito').value;
    vAgregarC.close();
	if(tc==1)
		TB_show(lblAplicacion,'../clientes/catalogoClientes.php?cPagina=sFrm=true&idCliente=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
	else
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresas.php?cPagina=sFrm=true&idEmpresa=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
}

function llenarDatosRepresentante(cadObj)
{
	var obj=eval(cadObj)[0];
    TB_remove();
    var filaRep=gE('filaRep_'+obj.idRep);
    if(filaRep==null)
    {
        var tblRepresentantes=gE('tblRepresentantes');
        var nFila=tblRepresentantes.insertRow(-1);
        nFila.id='filaRep_'+obj.idRep;
        var celdaRep=nFila.insertCell(-1);
        setClase(celdaRep,'filaAzul10');
        celdaRep.setAttribute('colspan',3);
        celdaRep.innerHTML='<table width="100%"><tbody><tr><td width="80%"><label id="lblRepresentante_'+obj.idRep+'">'+obj.nombre+'</label></td><td width="20%"><a href="javascript:modificarRepresentante(\''+bE(obj.idRep)+'\')"><img src="../images/pencil.png" alt="Modificar datos del representante" title="Modificar datos del representante"></a>&nbsp;<a href="javascript:removerRepresentante(\''+bE(obj.idRep)+'\')"><img src="../images/delete.png" alt="Remover representante" title="Remover representante"></a></td></tr></tbody></table>';
        var celdaRel=nFila.insertCell(-1);
        setClase(celdaRel,'filaAzul10');
        celdaRel.innerHTML='<select id="relacionRep_'+obj.idRep+'" class="camp_form" name="relacionesRepresentante"><option value="2">Representante Legal</option><option value="5">Socio y Rep. Legal</option><option value="8">Firmas mancomunadas</option></select>';
        var celdaMail=nFila.insertCell(-1);
        setClase(celdaMail,'filaAzul10');
        celdaMail.innerHTML='<label id="lblEmail_'+obj.idRep+'">'+obj.mail+'</label>';
        var celdaNextel=nFila.insertCell(-1);
        setClase(celdaNextel,'filaAzul10');
        celdaNextel.innerHTML='<label id="lblCel_'+obj.idRep+'">'+obj.tel+'</label>';
   }
   else
   {
        gE('lblRepresentante_'+obj.idRep).innerHTML=obj.nombre;		
        gE('lblEmail_'+obj.idRep).innerHTML=obj.mail;		
        gE('lblCel_'+obj.idRep).innerHTML=obj.tel;		
   }
       
}

function agregarRepresentante(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosRepresentante",1)')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function modificarRepresentante(iC)
{
	
	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente='+bD(iC)+'&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosRepresentante",1)')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function mostrarVentanaAgregarCliente()
{
	var criterioB='2';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
    var gridSocios=Ext.getCmp('gridClientesP');
    var clientesP=recoletarValoresGrid(gridSocios,'idClienteP');
    if(clientesP=='')
    	clientesP='-1';
	var cmbNombre= inicializarCmbNombreClienteP(pPagina,lector,parametros,'2|'+clientesP);
    var lblEtiqueta;
   	lblEtiqueta='Nombre del cliente:';
    lblDebe='Debe seleccionar un cliente';
    lblEtiquetaNoExiste='&iquest;No existe el cliente? agr&eacute;guelo ';
    lblTitulo='Agregar cliente';
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:220,
                                                            y:45,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarCliente('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
                                        
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        
                                                                        var gridClientesP=Ext.getCmp('gridClientesP');
                                                                        var r=new registroClienteP	(
                                                                        								{
                                                                                                        	idClienteP:cmbNombre.getValue(),
                                                                                                            cliente:cmbNombre.getRawValue()
                                                                                                        }
                                                                        							)
                                                                        gridClientesP.getStore().add(r);
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreClienteP(pagina,lector, parametros,comp)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB='2';
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
        
		dSet.baseParams.campoBusqueda=criterioB;
        if(comp !=undefined)
        {
        	dSet.baseParams.comp=comp;
        }
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
	TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarExt='+Base64.encode('window.parent.llenarDatosCliente(@idRegistro)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function removerRepresentante(iC)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var fila=gE('filaRep_'+bD(iC));
        	fila.parentNode.removeChild(fila);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover al representante seleccionado?',resp)
}


function llenarDatosCliente(iC)
{
	TB_remove();
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var gridClientesP=Ext.getCmp('gridClientesP');
            var r=new registroClienteP	(
                                            {
                                                idClienteP:iC,
                                                cliente:arrResp[1]
                                            }
                                        )
            gridClientesP.getStore().add(r);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=9&idEmp='+iC,true);
}

function cambiaResidencia(combo)
{
	var valor=combo.options[combo.selectedIndex].value;
    if(valor=='1')
    {
    	//oE - dE
    	oE('_rentaResidenciafflo');
        gE('_rentaResidenciafflo').value='';
        gE('_rentaResidenciafflo').setAttribute('val','');
        mE('_valorResidenciafflo');
        gE('_valorResidenciafflo').setAttribute('val','obl');
        mE('_superficiefflo');
        gE('_superficiefflo').setAttribute('val','obl');
        mE('_construidofvch');
        gE('_construidofvch').setAttribute('val','obl');
    }
    else
    {
    	//mE - hE
    	mE('_rentaResidenciafflo');
        gE('_rentaResidenciafflo').setAttribute('val','obl');
        oE('_valorResidenciafflo');
        gE('_valorResidenciafflo').value='';
        gE('_valorResidenciafflo').setAttribute('val','');
        oE('_superficiefflo');
        gE('_superficiefflo').value='';
        gE('_superficiefflo').setAttribute('val','');
        oE('_construidofvch');
        gE('_construidofvch').setAttribute('val','');
        gE('_construidofvch').value='';
    }
}

function cambiaResidenciav(combo)
{
	var valor=combo.options[combo.selectedIndex].value;
    if(valor=='1')
    {
    	//oE - dE
    	oE('_rentaResidenciavflo');
        gE('_rentaResidenciavflo').value='';
        gE('_rentaResidenciavflo').setAttribute('val','');
        mE('_valorResidenciavflo');
        gE('_valorResidenciavflo').setAttribute('val','obl');
        mE('_superficievflo');
        gE('_superficievflo').setAttribute('val','obl');
        mE('_construidovvch');
        gE('_construidovvch').setAttribute('val','obl');
    }
    else
    {
    	//mE - hE
    	mE('_rentaResidenciavflo');
        gE('_rentaResidenciavflo').setAttribute('val','obl');
        oE('_valorResidenciavflo');
        gE('_valorResidenciavflo').value='';
        gE('_valorResidenciavflo').setAttribute('val','');
        oE('_superficievflo');
        gE('_superficievflo').value='';
        gE('_superficievflo').setAttribute('val','');
        oE('_construidovvch');
        gE('_construidovvch').setAttribute('val','');
        gE('_construidovvch').value='';
    }
}

