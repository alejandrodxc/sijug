<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$idCredito=base64_decode($_GET["idCredito"]);
	
	$sSaldos=bD($_GET["S"]);
	$sBienes=bD($_GET["B"]);
	$secSaldos=false;
	if($sSaldos=="1")
		$secSaldos=true;
	$sMontos=bD($_GET["S"]);
	$secMontos=false;
	if($sMontos=="1")
		$secMontos=true;
		
	$secBienes=false;	
	if($sBienes=="1")
		$secBienes=true;
	
	$consulta="select idBanco,nombreBan from 600_bancos order by nombreBan";
	$arrBancos=uEJ($con->obtenerFilasArreglo($consulta));
	
	$consulta="select idCliente,nombre from 700_vclientes order by nombre";
	$arrClientes=uEJ($con->obtenerFilasArreglo($consulta));
	
	$consulta="select idEmpresa,empresa from 700_empresas order by empresa";
	$arrEmpresas=uEJ($con->obtenerFilasArreglo($consulta));
	
	$consulta="select idBienesCredito,bien from 714_bienesCredito where idCredito=".$idCredito;
	$arrBienes=uEJ($con->obtenerFilasArreglo($consulta));
	
	$arrMeses[0]="Ene";
	$arrMeses[1]="Feb";
	$arrMeses[2]="Mar";
	$arrMeses[3]="Abr";
	$arrMeses[4]="May";
	$arrMeses[5]="Jun";
	$arrMeses[6]="Jul";
	$arrMeses[7]="Ago";
	$arrMeses[8]="Sep";
	$arrMeses[9]="Oct";
	$arrMeses[10]="Nov";
	$arrMeses[11]="Dic";
	$consulta="select fechaEntrada from 750_creditos where idCredito=".$idCredito;
	
	$fechaRef=$con->obtenerValor($consulta);
	$fecha=strtotime(date("Y-m-01",strtotime($fechaRef)));
	$nFecha=strtotime('-6 months',$fecha);
	
	$arrDeposito="";
	$arrAlmacen="";
	$arrColumnas="";
	$arrColumnasDepo="";
	$objAl="";
	$objDep="";
	$arrValoresNuevo="";
	$arrValoresNuevoDepo="";
	$objValorNuevo="";
	for($x=0;$x<6;$x++)
	{
		$fechaV=strtotime('+'.$x.' months',$nFecha);
		$mes=date('m',$fechaV);
		$mesLetra=$arrMeses[$mes-1];
		$anio=date('Y',$fechaV);
		$objAl="{name:'saldo_".$x."'},";
		$objDep="{name:'depo_".$x."'},";
		if($arrAlmacen=="")
			$arrAlmacen=$objAl;
		else
			$arrAlmacen.="".$objAl;
		
		if($arrDeposito=="")
			$arrDeposito=$objDep;
		else
			$arrDeposito.=$objDep;
			
		$objCol="{
					header:'".$mesLetra."-".$anio."',
					width:105,
					sortable:true,
					dataIndex:'saldo_".$x."',
					editor:new Ext.form.NumberField({allowDecimals:true}),
					renderer: 'usMoney',
					summaryType: 'sum'
				},
				";
		$objDepo="{
					header:'".$mesLetra."-".$anio."',
					width:105,
					sortable:true,
					dataIndex:'depo_".$x."',
					editor:new Ext.form.NumberField({allowDecimals:true}),
					renderer: 'usMoney',
					summaryType: 'sum'
				},
				";
		if($arrColumnas=="")
			$arrColumnas=$objCol;
		else
			$arrColumnas.="".$objCol;
		
		if($arrColumnasDepo=="")
			$arrColumnasDepo=$objDepo;
		else
			$arrColumnasDepo.="".$objDepo;
		
		$objValorNuevo="saldo_".$x.":0";
		if($arrValoresNuevo=="")
			$arrValoresNuevo=$objValorNuevo;
		else
			$arrValoresNuevo.=",".$objValorNuevo;
		$objValorNuevoDepo="depo_".$x.":0";
		if($arrValoresNuevoDepo=="")
			$arrValoresNuevoDepo=$objValorNuevoDepo;
		else
			$arrValoresNuevoDepo.=",".$objValorNuevoDepo;
	}


$consulta="select id_713_saldoCuentasBanco,cuenta,(select nombreBan from 600_bancos where idBanco=s.idBanco) as banco,idBanco,if(s.tipoTitular=1,(select nombre from 
			700_vclientes where idCliente=s.idTitular),(select empresa from 700_empresas where idEmpresa=s.idTitular)) as titular,idTitular,tipoTitular,saldoM6,saldoM5,saldoM4,saldoM3,saldoM2,saldoM1,antiguedad,'Saldo promedio ultimos 6 meses' as cuentasCliente from 713_saldosCuentasBanco s
			where idCredito=".$idCredito;
		
$arrCtas=uEJ($con->obtenerFilasArreglo($consulta));
$consulta="select id_714_depositoCuentasBanco,cuenta,(select nombreBan from 600_bancos where idBanco=s.idBanco) as banco,idBanco,if(s.tipoTitular=1,(select nombre from 
			700_vclientes where idCliente=s.idTitular),(select empresa from 700_empresas where idEmpresa=s.idTitular)) as titular,idTitular,tipoTitular,depositoM6,depositoM5,depositoM4,depositoM3,depositoM2,depositoM1,antiguedad,'Depositos ultimos 6 meses' as cuentasCliente,idCuentaRef from 714_depositosCuentasBanco s
			where idCredito=".$idCredito;
		
$arrCtasDepo=uEJ($con->obtenerFilasArreglo($consulta));

$consulta="select valorOpcion,opcion from 951_catalogoOpcionesVarios where tipoOpcion=4 and idIdioma=".$_SESSION["leng"]." ";
$arrMesesC=$con->obtenerFilasArreglo($consulta);

?>

var arrBancos=<?php echo $arrBancos?>;
var arrClientes=<?php echo $arrClientes?>;
var arrEmpresas=<?php echo $arrEmpresas?>;
var mascaraMoneda;
Ext.onReady(inicializar);


function inicializar()
{
	mascaraMoneda = new Mask("$ #,###.00", "number");
	var gridCuentasSaldos=crearGridCuentasSaldos();
    var gridCuentas=crearGridCuentasDepositos();
    gE('lblSaldoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioSaldo());
    gE('lblDepositoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioDeposito());
    
    var ventasAcumuladasAnt=gE('ventasAcumuladasAnt');
    if(ventasAcumuladasAnt!=null)
	    mascaraMoneda.attach(ventasAcumuladasAnt);
    var ventasAcumuladasAct=gE('ventasAcumuladasAct');
    if(ventasAcumuladasAct!=null)
    	mascaraMoneda.attach(ventasAcumuladasAct);
	var montoSol=gE('montoSol');
    if(montoSol!=null)
	    mascaraMoneda.attach(gE('montoSol'));
    var montoSug=gE('montoSug') ;
    if(montoSug!=null)  
	    mascaraMoneda.attach(gE('montoSug'));
    var txtMontoAut=gE('txtMontoAut')
    if(txtMontoAut!=null)
    	mascaraMoneda.attach(txtMontoAut);
    	
    inicializarCombos();
    crearGridBienesMuebles();
    
    
    var dteFechaRenovLinea=gE('dteFechaRenovLinea');
    var fecha;
    if(dteFechaRenovLinea!=null)
    {
    	var hFechaRen=gE('hFechaRen').value;
    	fecha=new Ext.form.DateField	(
        							{
                                    	id:'fDteFechaRenovLinea',
                                    	renderTo:'dteFechaRenovLinea',
                                        format:'d/m/Y',
                                        width:160,
                                        value:hFechaRen
                                    }
        						)
		fecha.on('select',	function(campo)
        					{
                            	var idCredito=gE('idCredito').value;
                            	var valor=campo.getValue().format('Y-m-d');
                            	function funcAjax()
                                {
                                      var resp=peticion_http.responseText;
                                      arrResp=resp.split('|');
                                      if(arrResp[0]=='1')
                                      {
                                         
                                        
                                      }
                                      else
                                      {
                                          msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                      }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo=14&valor='+cv(valor)+'&idCredito='+idCredito,true);

                            }
        		)                                
    }	
    
    var dteFechaComite=gE('dteFechaComite');
    var fecha;
    if(dteFechaComite!=null)
    {
    	var hFechaComite=gE('hFechaComite').value;
    	fecha=new Ext.form.DateField	(
                                            {
                                                id:'dDteFechaComite',
                                                renderTo:'dteFechaComite',
                                                format:'d/m/Y',
                                                width:100,
                                                value:hFechaComite
                                            }
                                        )
		fecha.on('select',	function(campo)
        					{
                            	var idCredito=gE('idCredito').value;
                            	var valor=campo.getValue().format('Y-m-d');
                            	function funcAjax()
                                {
                                      var resp=peticion_http.responseText;
                                      arrResp=resp.split('|');
                                      if(arrResp[0]=='1')
                                      {
                                         
                                        
                                      }
                                      else
                                      {
                                          msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                      }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo=15&valor='+cv(valor)+'&idCredito='+idCredito,true);

                            }
        		)                                
    }	
    
    var dteFechaFondeo=gE('dteFechaFondeo');
    var fecha;
    if(dteFechaFondeo!=null)
    {
    	var hFechaFondeo=gE('hFechaFondeo').value;
    	fecha=new Ext.form.DateField	(
                                            {
                                                id:'dDteFechaFondeo',
                                                renderTo:'dteFechaFondeo',
                                                format:'d/m/Y',
                                                width:100,
                                                value:hFechaFondeo
                                            }
                                        )
		fecha.on('select',	function(campo)
        					{
                            	var idCredito=gE('idCredito').value;
                            	var valor=campo.getValue().format('Y-m-d');
                            	function funcAjax()
                                {
                                      var resp=peticion_http.responseText;
                                      arrResp=resp.split('|');
                                      if(arrResp[0]=='1')
                                      {
                                         
                                        
                                      }
                                      else
                                      {
                                          msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                      }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo=16&valor='+cv(valor)+'&idCredito='+idCredito,true);

                            }
        		)                                
    }	
    
    
    var dteFechaEntrada=gE('dteFechaEntrada');
    var fecha;
    if(dteFechaEntrada!=null)
    {
    	var hFechaEntrada=gE('hFechaEntrada').value;
    	fecha=new Ext.form.DateField	(
                                            {
                                                id:'dDteFechaEntrada',
                                                renderTo:'dteFechaEntrada',
                                                format:'d/m/Y',
                                                width:100,
                                                value:hFechaEntrada
                                            }
                                        )
		fecha.on('select',	function(campo)
        					{
                            	var idCredito=gE('idCredito').value;
                            	var valor=campo.getValue().format('Y-m-d');
                            	function funcAjax()
                                {
                                      var resp=peticion_http.responseText;
                                      arrResp=resp.split('|');
                                      if(arrResp[0]=='1')
                                      {
                                         
                                        
                                      }
                                      else
                                      {
                                          msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                      }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo=17&valor='+cv(valor)+'&idCredito='+idCredito,true);

                            }
        		)                                
    }	

}

var registroBien=Ext.data.Record.create	(
											[
												{name: 'idBien'},
												{name: 'bien'}
											]
										)

function crearGridBienesMuebles()
{
	var dsDatos=<?php echo $arrBienes?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idBien'},
                                                                    {name: 'bien'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Bien Mueble',
															width:750,
															sortable:true,
															dataIndex:'bien'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'tblBienesMuebles',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:850,
                                                            sm:chkRow,
                                                            renderTo:'tblBienMueble',
                                                            <?php 
															if($secBienes)
															{
															?>
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            text:'Agregar Bien',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function ()
                                                                            		{
                                                                                    	mostrarVentanaAgregarBien(null);
                                                                                    }
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            text:'Modificar Bien',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function ()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el bien a modificar');
                                                                                        	return;
                                                                                        }
                                                                                    	mostrarVentanaAgregarBien(filas[0]);
                                                                                    }
                                                                        }
                                                                        ,'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            text:'Remover Bien',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function ()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el bien a remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	var cadBien='';
                                                                                                var x;
                                                                                                for(x=0;x<filas.length;x++)
                                                                                                {
                                                                                                    if(cadBien=='')
                                                                                                        cadBien=filas[x].get('idBien');
                                                                                                    else
                                                                                                        cadBien+=','+filas[x].get('idBien');
                                                                                                }
                                                                                            
                                                                                            	function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                    	tblGrid.getStore().remove(filas);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=8&cadBien='+cadBien,true);
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        msgConfirm('Est&aacute; seguro de querer remover los bienes seleccionados?',resp)
                                                                                        
                                                                                        
                                                                                    }
                                                                        }
                                                                        
                                                            		]
                                                               <?php
															}
															   ?>
                                                        }
                                                    );
	return 	tblGrid;	
}

function inicializarCombos()
{
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'4',
						criterio:''
					};
	inicializarCmbNombre(pPagina,lector,parametros);
}

function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
		var aNombre=Ext.getCmp('cmbNombre').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=criterioB;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														applyTo:'Nombre',
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
	function funcElemSeleccionado(combo,registro)
	{	
		Ext.getCmp('content').load({url:'../procesoCreditos/creditos.php',scripts:true,params:{cPagina:'sFrm=true',idProceso:idProcesoC,idEntidad:registro.get('idUsuario'),tEntidad:criterioB}});
	}
	
	comboNombre.on('select',funcElemSeleccionado);	
}

var registroCta=Ext.data.Record.create	(
			
											[
                                            	{name: 'idCuenta'},
												{name: 'cuenta'},
                                                {name: 'banco'},
                                                 {name: 'idBanco'},
                                                {name: 'titular'},
                                                {name: 'idTitular'},
                                                {name: 'tipoTitular'},
                                                <?php
                                                    echo $arrAlmacen;
                                                ?>
                                                {name: 'antiguedad'},
                                                {name: 'cuentasCliente'}
											]
										)


var registroDepo=Ext.data.Record.create	(
			
											[
                                            	{name: 'idCuenta'},
												{name: 'cuenta'},
                                                {name: 'banco'},
                                                 {name: 'idBanco'},
                                                {name: 'titular'},
                                                {name: 'idTitular'},
                                                {name: 'tipoTitular'},
                                                <?php
                                                    echo $arrDeposito;
                                                ?>
                                                {name: 'antiguedad'},
                                                {name: 'cuentasCliente'},
                                                {name: 'idCuentaRef'}
											]
										)

function crearGridCuentasSaldos()
{
	var dsDatos=<?php echo $arrCtas?>;
    var lector=	new Ext.data.ArrayReader	(
                                                    {
                                                        fields:	[
                                                        			{name: 'idCuenta'},
                                                                    {name: 'cuenta'},
                                                                    {name: 'banco'},
                                                                    {name: 'idBanco'},
                                                                    {name: 'titular'},
                                                                    {name: 'idTitular'},
                                                                    {name: 'tipoTitular'},
                                                                    <?php
																		echo $arrAlmacen;
																	?>
                                                                    {name: 'antiguedad'},
                                                                    {name: 'cuentasCliente'}
                                                                ]
                                                    }
                                                );

    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
    editorFila.on('beforeedit',funcEditorFilaBeforeEdit)
    editorFila.on('validateedit',funcEditorValida);
    editorFila.on('canceledit',funcEditorCancelEdit);
                                              
    var summary = new Ext.ux.grid.GroupSummary();
	
    var gS=new Ext.data.GroupingStore(
    									{
                                            reader: lector,
                                            data: dsDatos,
                                            sortInfo: {field: 'idCuenta', direction: 'ASC'},
                                            groupField: 'cuentasCliente'
                                        }
                                    )
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Cuenta',
															width:120,
															sortable:true,
															dataIndex:'cuenta',
                                                            editor:new Ext.form.NumberField({id:'txtCuenta',allowDecimals:false})
														},
														{
															header:'Banco',
															width:180,
															sortable:true,
															dataIndex:'banco',
                                                            editor:new Ext.form.TextField(
                                                            								{
                                                                                            	id:'txtBanco',
                                                                                            	readOnly:true,
                                                                                            	listeners:	{
                                                                                                    				focus:textFocus
                                                                                                    		}
                                                                                            }
                                                                                          )
														},
                                                        {
															header:'Titular',
															width:200,
															sortable:true,
															dataIndex:'titular',
                                                            editor:new Ext.form.TextField	(
                                                            									{
                                                                                                    id:'txtTitular',
                                                                                                    readOnly:true,
                                                                                                    listeners:	{
                                                                                                                        focus:textFocus
                                                                                                                }
                                                                                            	}
                                                            								),
                                                          	summaryRenderer: function(v, params, data)
                                                            				{
                                                                                return '<span class="letraRoja">Total:</span>';
                                                                            }
														},
                                                        <?php
															echo $arrColumnas;
														?>
                                                        {
															header:'Antig\xFCedad Cta.',
															width:120,
															sortable:true,
															dataIndex:'antiguedad',
                                                            editor:new Ext.form.TextField({id:'txtAntiguedad'})
														},
                                                        {
                                                        	header:'Cuentas Cliente',
                                                            width:120,
                                                            dataIndex:'cuentasCliente',
                                                            hidden:true
                                                        }
													]
												);

	<?php 
	if($secSaldos)
	{
	?>                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
    
    <?php
	}
	else
	{
	?>
        
	var tblGrid=	new Ext.grid.GridPanel	(
    <?php
	}
    ?>
    
                                                        {
                                                            id:'tblGridSaldos',
                                                            store:gS,
                                                            frame:true,
                                                            y:40,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:850,
                                                            autoScroll:true,
                                                            clicksToEdit:2,
                                                            /*view: new Ext.grid.GroupingView(	
                                                            									{
                                                                                                    forceFit: false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups: false,
                                                                                                    enableGroupingMenu: false,
                                                                                                    hideGroupedColumn: true
                                                                                                    
                                                                                                }
                                                                                            ),*/
                                                                
                                                            plugins: [
                                                            <?php 
															if($secSaldos)
															{
															?>
                                                            			editorFila,
                                                            <?php
															}
															?>
                                                                     	summary
                                                                     ],
                                                            renderTo:'tblCuentas'
                                                            <?php 
															if($secSaldos)
															{
															?>
                                                            ,tbar:	[
                                                            			{
                                                                        	id:'btnAgregar',
                                                                        	text:'Agregar',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	agregarRegistro(editorFila,tblGrid);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	id:'btnRemover',
                                                                        	text:'Remover',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	removerRegistro();
                                                                                    }
                                                                        }
                                                            		]
                                                             <?php
															}
															 ?>
                                                        }
                                                    );
                                                    
	return 	tblGrid;		
}

function funcEditorValida(rowEditor,obj,registro,nFila)
{	
	
	if(obj.cuenta=='')
    {
    	function resp()
        {
        	Ext.getCmp('txtCuenta').focus();
        }
    	msgBox('Debe ingresar el n&uacute;mero de cuenta',resp);
        return false;
    }
    
    if(obj.banco=='')
    {
    	function respP()
        {
        	Ext.getCmp('txtBanco').focus();
        }
    	msgBox('Debe ingresar el nombre del banco',respP);
        return false;
    }
	
    var idBanco=Ext.getCmp('txtBanco').idBanco;
    
    if(obj.titular=='')
    {
    	function respOG()
        {
        	Ext.getCmp('txtTitular').focus();
        }
    	msgBox('Debe ingresar al titular de la cuenta',respOG);
        return false;
    }
    var txtTitular=Ext.getCmp('txtTitular');
    var idTitular=txtTitular.idTitular;
    var tipoTitular=txtTitular.tipoTitular;
    if(obj.antiguedad=='')
    {
    	function respPeriodo()
        {
        	Ext.getCmp('txtAntiguedad').focus();
        }
    	msgBox('Debe ingresar la antig\xFCedad de la cuenta',respPeriodo);
        return false;
    }
    var idCredito=gE('idCredito').value;
    var idCuenta=registro.get('idCuenta');
    var cadObj='{"idCuenta":"'+idCuenta+'","idCredito":"'+idCredito+'","cuenta":"'+obj.cuenta+'","idBanco":"'+idBanco+'","idTitular":"'+idTitular+
    		'","tipoTitular":"'+tipoTitular+'","M6":"'+obj.saldo_0+'","M5":"'+obj.saldo_1+'","M4":"'+obj.saldo_2+'","M3":"'+
            obj.saldo_3+'","M2":"'+obj.saldo_4+'","M1":"'+obj.saldo_5+'","antiguedad":"'+cv(obj.antiguedad)+'"}';
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	registro.set('idCuenta',arrResp[1]);
        	registro.set('idBanco',idBanco);
            registro.set('idTitular',idTitular);
            registro.set('tipoTitular',tipoTitular);
            Ext.getCmp('btnAgregar').enable();
		    Ext.getCmp('btnRemover').enable();
            nuevoReg=false;
            
            var idCuenta2=arrResp[2];
            if(idCuenta=='-1')
            {
                var idCredito=gE('idCredito').value;
                var r=new registroDepo(
                                        {
                                            idCuenta:idCuenta2,
                                            cuenta:obj.cuenta,
                                            banco:obj.banco,
                                            idBanco:Ext.getCmp('txtBanco').idBanco,
                                            titular:obj.titular,
                                            idTitular:txtTitular.idTitular,
                                            tipoTitular:txtTitular.tipoTitular,
                                            antiguedad:obj.antiguedad,
                                            <?php
                                                echo $arrValoresNuevoDepo;
                                            ?>,
                                            idCuentaRef:arrResp[1],
                                            cuentasCliente:'Depositos ultimos 6 meses'
                                        }
                                    );
                gE('lblSaldoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioSaldo());
                Ext.getCmp('tblGridDepositos').getStore().add(r);
                gE('lblDepositoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioDeposito());

    		}
            else
            {
            	var tblGridDepositos=Ext.getCmp('tblGridDepositos');
            	 var posFilaD=obtenerPosFila(tblGridDepositos.getStore(),'idCuentaRef',arrResp[1]);
                 if(posFilaD!=-1)
                 {
                 	var filaDepo=tblGridDepositos.getStore().getAt(posFilaD);
                    filaDepo.set('cuenta',obj.cuenta);
                 	filaDepo.set('banco',obj.banco);
                    filaDepo.set('idBanco',Ext.getCmp('txtBanco').idBanco);
                    filaDepo.set('titular',obj.titular);
                    filaDepo.set('idTitular',txtTitular.idTitular);
                    filaDepo.set('tipoTitular',txtTitular.tipoTitular);
                    filaDepo.set('antiguedad',obj.antiguedad);
                    gE('lblSaldoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioSaldo());
                    gE('lblDepositoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioDeposito());
                    //tblGridDepositos.getStore().save();
                 }
            }
            
            
            //Ext.getCmp('tblGridSaldos').getStore().save();    		
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            Ext.getCmp('btnAgregar').enable();
		    Ext.getCmp('btnRemover').enable();
            Ext.getCmp('tblGridSaldos').getStore().rejectChanges();
            nuevoReg=false;
            return false;
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=1&obj='+cadObj,true);
     
}

function funcEditorFilaBeforeEdit(ctrl,fila)
{
	
	var tblGrid=Ext.getCmp('tblGridSaldos');
    var registro=tblGrid.getStore().getAt(fila);
	Ext.getCmp('txtBanco').idBanco=registro.get('idBanco');
    Ext.getCmp('txtTitular').idTitular=registro.get('idTitular');
    Ext.getCmp('txtTitular').tipoTitular=registro.get('tipoTitular');
	        
}

function funcEditorCancelEdit(rowEdit,obj,registro,nFila)
{
	if(nuevoReg)
    {
    	var grid=Ext.getCmp('tblGridSaldos');
        grid.getStore().removeAt(grid.getStore().getCount()-1);
    }
    Ext.getCmp('btnAgregar').enable();
    Ext.getCmp('btnRemover').enable();
    nuevoReg=false;
}

function textFocus(ctrl)
{
	switch(ctrl.id)
    {
    	case 'txtBanco':
        	mostrarVentanaBancos(ctrl.id);
        break;
        case 'txtTitular':
        	mostrarVentanaCliente(ctrl.id);
        break;
    }	
}



var nuevoReg=false;

function agregarRegistro(editorFila,tblGrid)
{
	var idCredito=gE('idCredito').value;
	var r=new registroCta(
                            {
                            	idCuenta:'-1',
                                <?php
									echo $arrValoresNuevo;
								?>,
                                cuentasCliente:'Saldo promedio ultimos 6 meses'
                                
							}
                        );
    editorFila.stopEditing();
    tblGrid.getStore().add(r);
    nuevoReg=true;
    editorFila.startEditing(tblGrid.getStore().getCount()-1);	
     Ext.getCmp('btnAgregar').disable();
    Ext.getCmp('btnRemover').disable();
}

function mostrarVentanaBancos(idCtrl)
{
	var gridBancos=generarGridTablaBancos();
    
    function aceptarClick()
    {
    	var filaTDoc=gridBancos.getSelectionModel().getSelected();
        if(filaTDoc==null)
        {
            msgBox('Primero debe seleccionar un banco');
            return;
        }
        ctrl=Ext.getCmp(idCtrl);
        ctrl.setValue(filaTDoc.get('nombreBan'));
        ctrl.idBanco=filaTDoc.get('idBanco');
        ventanaAM.close();
    }
    gridBancos.on('rowdblclick',function()
    									{
                                        	aceptarClick();
                                        }
    						)
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	html:'Seleccione un banco:',
                                                            x:10,
                                                            y:10
                                                        },
                                                        gridBancos
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Bancos',
										width: 635,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															id:'btnTPAceptar',
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		aceptarClick();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
                                                                       
																	}
														}
													]
									}
								);
	ventanaAM.on('close',function()
    					{
                        	
                        }
                 )   
   	gridBancos.getStore().load({params:{funcion:302}});                             
	ventanaAM.show();	
}

function generarGridTablaBancos()
{
    var alDatos = new Ext.data.JsonStore	(
                                                        {
                                                            root: 'registros',
                                                            totalProperty: 'numReg',
                                                            idProperty: 'codigo',
                                                            fields:	[
                                                                        {name: 'idBanco'},
		                                                                {name: 'nombreBan'},
                                                                        {name: 'descripcion'}
                                                                    ],
                                                            remoteSort:false,
                                                            proxy: new Ext.data.HttpProxy	(
                                                                                                {
                                                                                                    url: '../paginasFunciones/funcionesProyectos.php'
                                                                                                }
                                                                                            )
                                                        }
                                                    );                                            

    
    var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[
                                                        				{
                                                                            type:'string',
                                                                           	dataIndex:'nombreBan' 
																		}
                                                        			]
                                                    }
                                                ); 
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Banco',
															width:200,
															sortable:true,
															dataIndex:'nombreBan'
														},
                                                        {
															header:'Descripci&oacute;n',
															width:400,
															sortable:true,
															dataIndex:'descripcion'
														}
														
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridBancos',
                                                            store:alDatos,
                                                            frame:true,
                                                            x:10,
                                                            y:40,
                                                            cm: cModelo,
                                                            height:300,
                                                            width:580,
                                                            plugins: filters
                                                            
                                                        }
                                                    );
	return 	tblGrid;
}

function removerRegistro()
{
	var tblGrid=Ext.getCmp('tblGridSaldos');
    var arrCeldaCel=tblGrid.getSelectionModel().getSelectedCell();

    if(arrCeldaCel==null)
    {
    	msgBox('Primero debe seleccionar la cuenta a remover');
        return;
    }
    var fila=tblGrid.getStore().getAt(arrCeldaCel[0]);
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	var idCuenta=fila.get('idCuenta');
                	tblGrid.getStore().remove(fila);
                    var tblGridDepositos=Ext.getCmp('tblGridDepositos');
                    var posFilaD=obtenerPosFila(tblGridDepositos.getStore(),'idCuentaRef',idCuenta);
                    if(posFilaD!=-1)
	                    tblGridDepositos.getStore().removeAt(posFilaD);
                    gE('lblSaldoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioSaldo());
				    gE('lblDepositoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioDeposito());
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesProyectos.php',funcAjax, 'POST','funcion=305&idCuenta='+fila.get('idCuenta'),true);
        
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover la cuenta seleccionada?',resp)
}

function crearGridCuentasDepositos()
{
	var dsDatos=<?php echo $arrCtasDepo?>;
    var lector=	new Ext.data.ArrayReader	(
                                                    {
                                                        fields:	[
                                                        			{name: 'idCuenta'},
                                                                    {name: 'cuenta'},
                                                                    {name: 'banco'},
                                                                     {name: 'idBanco'},
                                                                    {name: 'titular'},
                                                                    {name: 'idTitular'},
                                                                    {name: 'tipoTitular'},
                                                                    <?php
																		echo $arrDeposito;
																	?>
                                                                    {name: 'antiguedad'},
                                                                    {name: 'cuentasCliente'},
                                                                    {name: 'idCuentaRef'}
                                                                ]
                                                    }
                                                );

    
    
    var summary = new Ext.ux.grid.GroupSummary();
	var editorFila=new Ext.ux.grid.RowEditor	(
    												{
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
    editorFila.on('beforeedit',funcEditorFilaBeforeEdit2)
    editorFila.on('validateedit',funcEditorValida2);
    editorFila.on('canceledit',funcEditorCancelEdit2);
    var gS=new Ext.data.GroupingStore(
    									{
                                            reader: lector,
                                            data: dsDatos,
                                            sortInfo: {field: 'idCuenta', direction: 'ASC'},
                                            groupField: 'cuentasCliente'
                                        }
                                    )
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Cuenta',
															width:120,
															sortable:true,
															dataIndex:'cuenta'
														},
														{
															header:'Banco',
															width:180,
															sortable:true,
															dataIndex:'banco'
														},
                                                        {
															header:'Titular',
															width:200,
															sortable:true,
															dataIndex:'titular',
                                                          	summaryRenderer: function(v, params, data)
                                                            				{
                                                                                return '<span class="letraRoja">Total:</span>';
                                                                            }
														},
                                                        <?php
															echo $arrColumnasDepo;
														?>
                                                        {
															header:'Antig\xFCedad Cta.',
															width:120,
															sortable:true,
															dataIndex:'antiguedad'
														},
                                                        {
                                                        	header:'Cuentas Cliente',
                                                            width:120,
                                                            dataIndex:'cuentasCliente',
                                                            hidden:true
                                                        }
													]
												);
                                                
	<?php 
	if($secMontos)
	{
	?>                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
    
    <?php
	}
	else
	{
	?>
        
	var tblGrid=	new Ext.grid.GridPanel	(
    <?php
	}
    ?>
                                                        {
                                                            id:'tblGridDepositos',
                                                            store:gS,
                                                            frame:true,
                                                            y:40,
                                                            cm: cModelo,
                                                            height:260,
                                                            width:850,
                                                            autoScroll:true,
                                                            clicksToEdit:2,
                                                           /* view: new Ext.grid.GroupingView(	
                                                            									{
                                                                                                    forceFit: false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups: false,
                                                                                                    enableGroupingMenu: false,
                                                                                                    hideGroupedColumn: true
                                                                                                    
                                                                                                }
                                                                                            ),*/
                                                                
                                                            plugins: [
                                                            		<?php 
																		if($secMontos)
																		{
																		?>
																					editorFila,
																		<?php
																		}
																		?>
																					summary
                                                                     ],
                                                            renderTo:'tblCuentasDeposito'
                                                        }
                                                    );
	                                  
	return 	tblGrid;		
}

var titularCta;

function mostrarVentanaCliente()
{
	titularCta=1;	
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombre(pPagina,lector,parametros);
    
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Tipo del titular:'
                                                        },
                                                        {	
                                                        	xtype:'radio',
                                                        	id:'chkCliente',
                                                            x:130,
                                                            y:5,
                                                            name:'rdoCliente',
                                                            value:'1',
                                                           	boxLabel :'Cliente',
                                                            checked:true,
                                                            listeners:	{
                                                                                check:function()
                                                                                	{
	                                                                                    titularCta=1;
                                                                                    	cmbNombre.setValue('');
                                                                                        cmbNombre.focus();
                                                                                    }
                                                                        } 
                                                            
                                                        },
                                                        {
                                                        	xtype:'radio',
                                                        	id:'chkEmpresa',
                                                            value:'2',
                                                            name:'rdoCliente',
                                                            x:200,
                                                            y:5,
                                                           	boxLabel :'Empresa',
                                                            listeners:	{
                                                                                check:function()
                                                                                	{
                                                                                    	titularCta=2;
                                                                                    	cmbNombre.setValue('');
                                                                                        cmbNombre.focus();
                                                                                    }
                                                                        } 
                                                        },
                                                        
														{
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del titular:'
                                                        },
                                                        cmbNombre,
                                                         {
                                                        	x:180,
                                                            y:70,
                                                        	html:'&iquest;No existe el titular de la cuenta? agr&eacute;guela <a href="javascript:agregarTitularCuenta()"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vTitularCta',
										title: 'Agregar titular de cuenta',
										width: 500,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var titular=cmbNombre.getValue();
                                                                        if(titular=='')
                                                                        {
                                                                        	msgBox('Debe seleccionar al titular de la cuenta');
                                                                        	return;
                                                                        }
                                                                        var tipoTitular=1;
                                                                        var chkEmpresa=Ext.getCmp('chkEmpresa');
                                                                        if(chkEmpresa.getValue())
                                                                            tipoTitular=2;
                                                                        var txtTitular=Ext.getCmp('txtTitular');
                                                                        txtTitular.setValue(cmbNombre.getRawValue());
                                                                        txtTitular.idTitular=titular;
                                                                        txtTitular.tipoTitular=tipoTitular;
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombre(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB=1;
        var chkEmpresa=Ext.getCmp('chkEmpresa');
        if(chkEmpresa.getValue())
        	criterioB=2;
		var aNombre=Ext.getCmp('cmbNombre').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=criterioB;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombre',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:130,
                                                        y:35,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function funcEditorValida2(rowEditor,obj,registro,nFila)
{	
	var idCuenta=registro.get('idCuenta');
    var cadObj='{"idCuenta":"'+idCuenta+'","D6":"'+obj.depo_0+'","D5":"'+obj.depo_1+'","D4":"'+obj.depo_2+'","D3":"'+
            obj.depo_3+'","D2":"'+obj.depo_4+'","D1":"'+obj.depo_5+'"}';
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
			gE('lblDepositoPromedio').innerHTML=mascaraMoneda.format(calcularPromedioDeposito());

            Ext.getCmp('tblGridDepositos').getStore().save();    		
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            Ext.getCmp('btnAgregar').enable();
		    Ext.getCmp('btnRemover').enable();
            Ext.getCmp('tblGridDepositos').getStore().rejectChanges();
            nuevoReg=false;
            return false;
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=2&obj='+cadObj,true);
     
}

function funcEditorFilaBeforeEdit2(ctrl,fila)
{
	
	var tblGrid=Ext.getCmp('tblGridSaldos');
    var registro=tblGrid.getStore().getAt(fila);
	Ext.getCmp('txtBanco').idBanco=registro.get('idBanco');
    Ext.getCmp('txtTitular').idTitular=registro.get('idTitular');
    Ext.getCmp('txtTitular').tipoTitular=registro.get('tipoTitular');
	        
}

function funcEditorCancelEdit2(rowEdit,obj,registro,nFila)
{
	if(nuevoReg)
    {
    	var grid=Ext.getCmp('tblGridSaldos');
        grid.getStore().removeAt(grid.getStore().getCount()-1);
    }
    Ext.getCmp('btnAgregar').enable();
    Ext.getCmp('btnRemover').enable();
    nuevoReg=false;
}

function calcularPromedioSaldo()
{
	var tblGridSaldos=Ext.getCmp('tblGridSaldos');
	var x;
	var fila;
    var total=0;
    var saldoFinal=0;
    var arrTotales=new Array();
    var ct;
    for(ct=0;ct<6;ct++)
    {
    	total=0;
        for(x=0;x<tblGridSaldos.getStore().getCount();x++)
        {
            fila=tblGridSaldos.getStore().getAt(x);
            total+=parseFloat(fila.get('saldo_'+ct));
        }
        arrTotales.push(total);
	}
    for(x=0;x<arrTotales.length;x++)
    {
    	saldoFinal+=arrTotales[x];
    }       
    
    saldoFinal=saldoFinal/arrTotales.length;
    return saldoFinal;
}

function calcularPromedioDeposito()
{
	var tblGridSaldos=Ext.getCmp('tblGridDepositos');
	var x;
	var fila;
    var total=0;
    var saldoFinal=0;
    var arrTotales=new Array();
    var ct;
    for(ct=0;ct<6;ct++)
    {
    	total=0;
        for(x=0;x<tblGridSaldos.getStore().getCount();x++)
        {
            fila=tblGridSaldos.getStore().getAt(x);
            total+=parseFloat(fila.get('depo_'+ct));
        }
        arrTotales.push(total);
	}
    for(x=0;x<arrTotales.length;x++)
    {
    	saldoFinal+=arrTotales[x];
    }       
    
    saldoFinal=saldoFinal/arrTotales.length;
    return saldoFinal;
}

function mostrarVentanaAgregarEmpresaCliente()
{
	var criterioB=gE('tipoCliente').value;
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    var lblEtiqueta;
    if(criterioB=='1')
    {
    	lblEtiqueta='Nombre del cliente:';
        lblDebe='Debe seleccionar un cliente';
        lblEtiquetaNoExiste='&iquest;No existe el cliente? agr&eacute;guelo ';
        lblTitulo='Agregar cliente';
    }
    else
    {
    	lblEtiqueta='Nombre de la empresa:';
        lblDebe='Debe seleccionar una empresa';
        lblEtiquetaNoExiste='&iquest;No existe la empresa? agr&eacute;guela ';
        lblTitulo='Agregar empresa';
    }
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:240,
                                                            y:45,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarCliente('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 500,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        var idCredito=gE('idCredito').value;
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	actualizaCredito(arrResp[1]);
                                                                            	ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=3&idCliente='+cmbNombre.getValue()+'&tipoCliente='+criterioB+'&idCredito='+idCredito,true);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreCliente(pagina,lector, parametros)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB=gE('tipoCliente').value;
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=criterioB;
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:150,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    var idCredito=gE('idCredito').value;
    vAgregarC.close();
	if(tc==1)
		TB_show(lblAplicacion,'../clientes/catalogoClientes.php?cPagina=sFrm=true&idCliente=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
	else
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresas.php?cPagina=sFrm=true&idEmpresa=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
}

function modificarCliente()
{
	 var idCliente=gE('idCliente').value;
    var idCredito=gE('idCredito').value;
    var tc=gE('tipoCliente').value;
	if(tc==1)
		TB_show(lblAplicacion,'../clientes/catalogoClientes.php?ejecutarExt='+Base64.encode('window.parent.cerrarVentana()')+'&cPagina=sFrm=true&idCliente='+idCliente+'&TB_iframe=true&height=500&width=900',"","scrolling=yes",actualizarPagina);
	else
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresas.php?ejecutarExt='+Base64.encode('window.parent.cerrarVentana()')+'&cPagina=sFrm=true&idEmpresa='+idCliente+'&TB_iframe=true&height=550&width=900',"","scrolling=yes",actualizarPagina);
}

function actualizarPagina()
{
	window.parent.recargarInfoCredito();
	gE('frmEnvioActualiza').submit();
}

function actualizaCredito(iC)
{
	gE('idCredito').value=iC;
	gE('frmEnvioActualiza').submit();
}	

function actualizaCreditoThickBox(iC)
{
	TB_remove();
	gE('idCredito').value=iC;
	gE('frmEnvioActualiza').submit();
}	

function mostrarCatalogoMeses()
{
	var idCredito=gE('idCredito').value;
	var gridMeses=crearGridMeses();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                        	html:'Seleccione los meses que desea indicar como mayor venta en el a&ntilde;o:'
                                                        },
														gridMeses	

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Meses con mayores ventas en el a&ntilde;o',
										width: 300,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filas=gridMeses.getSelectionModel().getSelections();
                                                                        var x;
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Al menos debe seleccionar un mes');
                                                                        	return;
                                                                        }
                                                                        var cadMes='';
                                                                        var idMes='';
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(idMes=='')
                                                                            {
                                                                            	idMes=filas[x].get('idMes');
                                                                                cadMes=filas[x].get('mes')
                                                                            }
                                                                            else
                                                                            {
                                                                            	idMes+=','+filas[x].get('idMes');
                                                                                cadMes+=','+filas[x].get('mes')
                                                                            }
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gE('lblMesesVentas').innerHTML=cadMes;
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo=1&valor='+idMes+'&idCredito='+idCredito,true);

                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridMeses()
{


	var dsDatos=<?php echo $arrMesesC?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idMes'},
                                                                    {name: 'mes'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Mes',
															width:150,
															sortable:true,
															dataIndex:'mes'
														}
                                                   ]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridMesesC',
                                                            store:alDatos,
                                                            frame:true,
                                                            x:15,
                                                            y:40,
                                                            cm: cModelo,
                                                            height:300,
                                                            width:240,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;		
}

function actualizarCampoCredito(ctrl,c)
{
	var valor;
	if((Base64.decode(c)!='7')&&(Base64.decode(c)!='9'))
    	valor=ctrl.value;
    else
    	valor=ctrl.options[ctrl.selectedIndex].value;
	var idCredito=gE('idCredito').value;
    
    if(ctrl.id=='txtExpo')
    {
    	if(valor=='')
        {
        	function respVacio()
            {
                ctrl.focus();
            }
            msgBox('El nombre de la Expo es obligatorio',respVacio);
            return false;
        }    
    }
    
	function funcAjax()
      {
          var resp=peticion_http.responseText;
          arrResp=resp.split('|');
          if(arrResp[0]=='1')
          {
             if(ctrl.id=='cmbCaptacion')
             {
             	if(valor=='12')
                {
                	mE('filaExpo')
                    gE('txtExpo').focus();
                }
                else
                	oE('filaExpo');
                    gE('txtExpo').value='';

             }
            
          }
          else
          {
              msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
          }
      }
      obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo='+Base64.decode(c)+'&valor='+cv(valor)+'&idCredito='+idCredito,true);

}

function agregarAval()
{
	var idCredito=gE('idCredito').value;
    var iC='-1';
    TB_show(lblAplicacion,'../clientes/catalogoAval.php?cPagina=sFrm=true&idCredito='+idCredito+'&idAval='+iC+'&ejecutarExt='+Base64.encode('window.parent.actualizaCreditoThickBox('+idCredito+');return;')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function modificarAval(iA)
{
	var idCredito=gE('idCredito').value;
    TB_show(lblAplicacion,'../clientes/catalogoAval.php?cPagina=sFrm=true&idCredito='+idCredito+'&idAval='+iA+'&ejecutarExt='+Base64.encode('window.parent.actualizaCreditoThickBox('+idCredito+')')+'&TB_iframe=true&height=480&width=890',"","scrolling=no");
}

function removerAval(iA)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
            function funcAjax()
              {
                  var resp=peticion_http.responseText;
                  arrResp=resp.split('|');
                  if(arrResp[0]=='1')
                  {
                    var idCredito=gE('idCredito').value;
                     actualizaCredito(idCredito);
                    
                  }
                  else
                  {
                      msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                  }
              }
              obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=6&idAval='+iA,true);
		}              
	}
    msgConfirm('Est&aacute; seguro de querer remover al aval seleccionado?',resp);
}

function mostrarVentanaAgregarBien(registro)
{
	var valor='';
	if(registro==null)
    	idB='-1';
    else
    {
    	idB=registro.get('idBien');
    	valor=registro.get('bien');    
    }   
        
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Bien Mueble:'
                                                        },
                                                        {
                                                        	id:'txtBien',
                                                        	xtype:'textarea',
                                                            x:100,
                                                            y:5,
                                                            width:350,
                                                            height:80,
                                                            value:valor
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Bienes Muebles',
										width: 500,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	Ext.getCmp('txtBien').focus(false,400);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var idCredito=gE('idCredito').value;
																		var txtBien=Ext.getCmp('txtBien');
                                                                        if(txtBien.getValue()=='')
                                                                        {
                                                                        	function respB()
                                                                            {
                                                                            	txtBien.focus();
                                                                            }
                                                                        	msgBox('Debe ingresar la descripci&oacute;n del Bien',respB);
                                                                        	return;
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	if(idB=='-1')
                                                                                {
                                                                                    var tblBienesMuebles=Ext.getCmp('tblBienesMuebles');
                                                                                    var r=new registroBien	(
                                                                                                                {
                                                                                                                    idBien:arrResp[1],
                                                                                                                    bien:txtBien.getValue()
                                                                                                                }
                                                                                                            )
                                                                                     tblBienesMuebles.getStore().add(r);
                                                                                 }
                                                                                 else
                                                                                 	registro.set('bien',txtBien.getValue());
                                                                                 ventanaAM.close();
                                                                                                        
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=7&idBien='+idB+'&idCredito='+idCredito+'&bien='+cv(txtBien.getValue()),true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function cerrarVentana()
{
	TB_remove();
}

function mostrarVentanaAgregarCliente()
{
	var criterioB='2';
    var idCredito=gE('idCredito').value;
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
    var gridSocios=Ext.getCmp('gridClientesP');
   
	var cmbNombre= inicializarCmbNombreClienteP(pPagina,lector,parametros,'2|'+idCredito);
    var lblEtiqueta;
   	lblEtiqueta='Nombre del cliente:';
    lblDebe='Debe seleccionar un cliente';
    lblEtiquetaNoExiste='&iquest;No existe el cliente? agr&eacute;guelo ';
    lblTitulo='Agregar cliente';
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:220,
                                                            y:45,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarCliente('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
                                        
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        
                                                                       vincularCliente(cmbNombre.getValue());
                                                                       ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreClienteP(pagina,lector, parametros,comp)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB='2';
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
        
		dSet.baseParams.campoBusqueda=criterioB;
        if(comp !=undefined)
        {
        	dSet.baseParams.comp=comp;
        }
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function vincularCliente(idCliente)
{
    var idCredito=gE('idCredito').value;
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	actualizaCredito(idCredito);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=13&idCredito='+idCredito+'&idCliente='+idCliente,true);
    
    
}

function agregarCliente(tc)
{
	var idCredito=gE('idCredito').value;
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
	TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('vincularClienteCredito('+idCredito+',idRegPadre);return')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function removerClienteP(iC)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                   var fila=gE('fila_'+bD(iC));
                   fila.parentNode.removeChild(fila);
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=14&iC='+iC,true);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover al cliente principal seleccionado?',resp)
    
}

function modificarCerrador(tC)
{
	var criterioB='1';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'4',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    var lblEtiqueta;
    if(criterioB=='1')
    {
    	lblEtiqueta='Nombre del cerrador:';
        lblDebe='Debe seleccionar un cerrador';
        lblEtiquetaNoExiste='';
        lblTitulo='Agregar cerrador';
    }
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                       														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var campo=18;
                                                                        if(bD(tC)=='2')
                                                                        	campo=19;
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        
                                                                       	var nCerrador=cmbNombre.getRawValue();
                                                                        var idCerrador=cmbNombre.getValue();
                                                                        var idCredito=gE('idCredito').value;
                                                                        ventanaAM.close();
                                                                        function funcAjax()
                                                                        {
                                                                              var resp=peticion_http.responseText;
                                                                              arrResp=resp.split('|');
                                                                              if(arrResp[0]=='1')
                                                                              {
                                                                                 if(bD(tC)=='1')
                                                                                 	gE('cerradorRC').innerHTML=nCerrador;
                                                                                 else
                                                                                 	gE('cerradorFondo').innerHTML=nCerrador;
                                                                                
                                                                              }
                                                                              else
                                                                              {
                                                                                  msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                              }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=5&campo='+campo+'&valor='+cv(idCerrador)+'&idCredito='+idCredito,true);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function inicializarCmbNombreCliente(pagina,lector, parametros,comp)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	var criterioB='1';
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
        
		dSet.baseParams.campoBusqueda=criterioB;
        if(comp !=undefined)
        {
        	dSet.baseParams.comp=comp;
        }
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}


function verCreditosSocio(iS,tC)
{
	var idCredito=gE('idCredito').value;
	TB_show(lblAplicacion,'../clientes/creditosSocio.php?cPagina=sFrm=true&tCliente='+tC+'&idSocio='+iS+'&idCredito='+bE(idCredito)+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function agregarTitularCuenta()
{

	gEx('vTitularCta').close();
	if(titularCta=='1')
    	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"vincularTitularCta",1)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
	else
	    TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"vincularTitularCta",2)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");        
}

function vincularTitularCta(cadObj)
{
	TB_remove();
    var obj=eval(cadObj)[0];
    var txtTitular=Ext.getCmp('txtTitular');
    txtTitular.setValue(obj.nombre);
    txtTitular.idTitular=obj.idRep;
    txtTitular.tipoTitular=titularCta;
}