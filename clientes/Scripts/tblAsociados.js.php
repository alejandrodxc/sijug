<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="select b.nombres,b.paterno,b.materno,b.idCliente,c.Nombre from 703_clientes b, 802_identifica c where b.idResponsable=c.idUsuario and b.cliente='0' order by b.paterno, b.materno, b.nombres ";
	$arrClientes=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrClientes?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                        			{name: 'nombres'},
                                                                    {name: 'paterno'},
                                                                    {name: 'materno'},
                                                                    
                                                                     {name: 'idCliente'},
                                                                    {name: 'nombre'}                                                                
                                                                ]
                                                    	}
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Paterno',
															width:140,
															sortable:true,
															dataIndex:'paterno'
														},
                                                        {
															header:'Materno',
															width:140,
															sortable:true,
															dataIndex:'materno'
														},
                                                        {
															header:'Nombre',
															width:200,
															sortable:true,
															dataIndex:'nombres'
														},
														{
															header:'Promotor',
															width:200,
															sortable:true,
															dataIndex:'nombre'
														}
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:460,
                                                            width:750,
                                                            renderTo:'tblClientes',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nuevo asociado',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idCliente','-1']];
                                                                                    	enviarFormularioDatos('catalogoAsociados.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar asociado',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar al asociado a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idCliente=fila.get('idCliente');
                                                                                       var arrParam=[['idCliente',idCliente]];
                                                                                    	enviarFormularioDatos('catalogoAsociados.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Eliminar asociado',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar al asociado a eliminar');
                                                                                            return;
                                                                                       }
                                                                                       
                                                                                       function resp(btn)
                                                                                       {
                                                                                       		if(btn=='yes')
                                                                                            {
                                                                                            	var idCliente=fila.get('idCliente');
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                    	tblGrid.getStore().remove(fila);	
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=21&idCliente='+idCliente+'&idTipoCliente=1',true);
                                                                                            }
                                                                                       }
                                                                                       msgConfirm('Est&aacute; seguro de querer eliminar al asociado seleccionado?',resp)
                                                                                       
                                                                                       
                                                                                    }
                                                                        }

                                                            		]
                                                            
                                                        }
                                                    );
	
}

