<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	var idCliente=gE('idCliente').value;
     var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:900,
                                        height:450,
                                        items: [
                                                    {
                                                       title: 'General',
                                                       contentEl:'tblGral'
                                                    }
                                                    
                                                     
                                                ]
                                    }
                               );
                               
	crearCampoFecha('sp_fechaAltaSatdte','_fechaAltaSatdte');
    var mascara=new Mask('$ ###,##0.00','number');
    var _nominaMensualflo=gE('_nominaMensualflo');
	_nominaMensualflo.value=mascara.format(_nominaMensualflo.value);
    mascara.attach(_nominaMensualflo);
    
}

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	var eJs=gE('eJs');
        var _paternovch=gE('_paternovch').value;
        var _maternovch=gE('_maternovch').value;
        var _nombresvch=gE('_nombresvch').value;
        var nUsuario=_paternovch+' '+_maternovch+' '+_nombresvch;
        if(eJs!=null)
        	eJs.value=Base64.encode("window.parent.abrirSocio('@idPadre','"+nUsuario+"','1')");
    	gE('frmEnvio').submit();
    }
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}


function cerrarVentana()
{
	TB_remove();
}
