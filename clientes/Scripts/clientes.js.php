<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="select numEstado,estadoCredito from 752_estadosCredito order by estadoCredito";
	$arrEstados=uEJ($con->obtenerFilasArreglo($consulta));
?>
Ext.onReady(inicializar);
var nodoSel=null;
var criterioB=1;
var idProcesoC=130;
var arrEstados=<?php echo $arrEstados?>;
function inicializar()
{
	var IdUsuario=gE("idUsuario").value;
   
	Ext.QuickTips.init();
	var detailEl;
    var idCred=gE('idCredito').value;
    
	
	var detailsPanel = 	{
                            id: 'tblDetalles',
                            title: 'Datos generales',
                            region: 'north',
                            autoScroll: true,
                            collapsible:true,
                            autoLoad:	{
                                            				url:'../clientes/datosCredito.php',
                                                            params:	{	
                                                            			idCredito:idCred,cPagina:'sFrm=true'
                                                                        
                                                            		}
                                            			
                                            			}
                        };
	var documentos = 	{
                            id: 'tblDocumentos',
                            title: 'Documentos anexos',
                            region: 'center',
                            collapsible:true,
                            autoScroll: true,
                            autoLoad:	{
                                            				url:'../clientes/documentosCredito.php',
                                                            params:	{	
                                                            			idCredito:idCred,cPagina:'sFrm=true'
                                                                        
                                                            		}
                                            			
                                            			}
                        };                        
	

    new Ext.Viewport(	{
                            layout: 'border',
                            title: '',
                            items: [
                                        {
                                            xtype: 'box',
                                            region: 'north',
                                            applyTo: 'header',
                                            height: 30
                                        },
                                        {
                                            layout: 'accordion',
                                            id: 'layout-browser',
                                            region:'west',
                                            border: false,
                                            split:true,
                                            margins: '2 0 5 5',
                                            width: 250,
                                            items: [ detailsPanel,documentos],
                                            collapsible:true,
                                            title:'Opciones del cr&eacute;dito'
                                           
                                        },
                                        {
                                            id: 'content',
                                            region: 'center',
                                            xtype:'iframepanel',
                                            autoLoad:{
                                            			url:'../clientes/creditosClientes.php',
                                                        params:{idCredito:idCred,cPagina:'sFrm=true'},
                                                        scripts:true
                                            		},	
                                            loadMask:	{
                                                            msg:'Cargando'
                                                        }
                                        }
                                    ]
						}
                    );
                    
          
                    
                    
	
                     
}

function recargarInfoCredito()
{
	var idCred=gE('idCredito').value;
	Ext.getCmp('tblDetalles').load({
                          url:'../clientes/datosCredito.php',
                          params:	{	
                                      idCredito:idCred,cPagina:'sFrm=true'
                                      
                                  }
                      
                      })
}

function recargarInfoDocumentos()
{
	var idCred=gE('idCredito').value;
	Ext.getCmp('tblDocumentos').load({
                          				url:'../clientes/documentosCredito.php',
                                        params:	{	
                                                    idCredito:idCred,cPagina:'sFrm=true'
                                                    
                                                }
                      
                      					})
}

function abrirSocio(idSocio,nombre,tipo)
{
	var cmbNombre=Ext.getCmp('cmbNombre');
    cmbNombre.setValue(nombre);
 	criterioB=tipo;   
    if(criterioB=='1')
    	idProcesoC=130;
    else
    	idProcesoC=124;
    gE('chk_'+tipo).checked=true;
	Ext.getCmp('content').load({url:'../procesoCreditos/creditos.php',scripts:true,params:{cPagina:'sFrm=true',idProceso:idProcesoC,idEntidad:idSocio,tEntidad:criterioB}});
    TB_remove();
}

function nuevo()
{
	var arrOpt=[['1','Persona f\xEDsica'],['2','Empresa']];
    var cmbTipoEmpresa=crearComboExt('cmbTipoEmpresa',arrOpt,160,5);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	html:'Tipo de empresa a crear',
                                                            x:10,
                                                            y:10
                                                        },
														cmbTipoEmpresa

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Tipo de cliente',
										width: 420,
										height:140,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var tEmpresa=cmbTipoEmpresa.getValue();
																		if(tEmpresa=='')
                                                                        {
                                                                        	msgBox('Debe seleccionar el tipo de empresa a crear');
                                                                        	return;
                                                                        }
                                                                        ventanaAM.close();
                                                                        if(tEmpresa=='1')
                                                                        	nuevaPersonaFisica();
                                                                        else
                                                                        	nuevaEmpresa();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function subirDocumento()
{
	var idCredito=gE('idCredito').value;
	var fp = new Ext.FormPanel	(
									{
										layout:'absolute',
										width: 500,
										frame: true,
										height:350,
										bodyStyle: 'padding: 10px 10px 0 10px;',
										items:	[
												 	{
                                                    	x:10,
                                                        y:10,
                                                    	html:'T&iacute;tulo:'
                                                  	},
                                                    
													{
                                                    	x:100,
                                                        y:5,
                                                        width:275,
														name:'titular',
														xtype: 'textfield',
														id: 'titulo'
													
													},
                                                    {
                                                    	x:10,
                                                        y:40,
                                                    	html:'Descripci&oacute;n:'
                                                  	},
													{
                                                    	x:100,
                                                        y:35,
                                                        width:275,
                                                        height:120,
														name:'descript', 
														xtype: 'textarea',
														id: 'describe'
													 },
                                                     {
                                                     	x:30,
                                                        y:165,
                                                     	id:'lblcontrol',
                                                        xtype:'label',
                                                        html:'<input type="file" id="inputSubida" name="inputSubida" />'
                                                     }
                                                     
													
													 
												]
									}
								);
	
		ventana=new Ext.Window(
							   		{
										title:'Subir documento',
										width:450,
										height:355,
                                       	layout:'fit',
										buttonAlign:'center',
										items:[fp],
										modal:true,
										plain:true,
										listeners:
                                                    {
                                                        show:
                                                                {
                                                                    buffer:10,
                                                                    fn:function()
                                                                            {
                                                                                gEx('titulo').focus(false,500);
                                                                            }
                                                                }
                                                    },
											buttons: 	[
															{
																text: 'Agregar',
																handler: function()
																		{
																			
                                                                            var titulo=Ext.getCmp('titulo');
                                                                            var txtTitulo=titulo.getValue();
                                                                            if(txtTitulo=='')
                                                                            {
                                                                            	function resp2()
                                                                                {
                                                                                	Ext.getCmp('titulo').focus();
                                                                                }
                                                                            	msgBox('Debe ingresar el t&iacute;tulo del documento',resp2);
                                                                                return;
                                                                            }
                                                                            var descripcion=gEx('describe').getValue();
                                                                            $('#inputSubida').uploadifySettings('scriptData',{'titular':cv(txtTitulo),'descript':cv(descripcion)})
                                                                            $('#inputSubida').uploadifyUpload();
																			
																		}
															},
															{
																text: 'Cancelar',
																handler: function()
																		{
																			ventana.close();
																		}
															}
														]
									}
							   )
		ventana.show();  
       
        crearControlSubida(idCredito,ventana);
		
}

function crearControlSubida(iC,ventana)
{
  $('#inputSubida').uploadify(
                                  {
                                      uploader: '../Scripts/jqueryUpload/uploadify.swf',
                                      script:'../clientes/guardarDocumentoAnexos.php',
                                      cancelImg: '../Scripts/jqueryUpload/cancel.png',
                                      buttonText:'Examinar...',
                                      scriptData:{
                                                      idCredito:iC,
                                                      iU:'<?php echo $_SESSION["idUsr"]?>'
                                                  },
                                      onComplete:function(e,cola,obj,response)
                                                  {
                                                      	var resp=response;
                                                        arrResp=resp.split('|');
                                                        if(arrResp[0]=='1')
                                                        {
                                                        	recargarInfoDocumentos();
															ventana.close(); 
                                                        }
                                                        else
                                                        {
                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                        }
                                                  }
                                  }
                          );
}

function removerAnexo(iA)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	var fila=gE('fila_'+bD(iA))
                    fila.parentNode.removeChild(fila);
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=12&iA='+iA,true);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover el documento seleccionado?',resp);
}

function enviarEtapa(e)
{
	var pos=existeValorMatriz(arrEstados,bD(e));
	var idCredito=gE('idCredito').value;
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	if(validarCambioEtapa(e))
            {
                function funcAjax()
                {
                    var resp=peticion_http.responseText;
                    arrResp=resp.split('|');
                    if(arrResp[0]=='1')
                    {
                        recargarPagina();
                    }
                    else
                    {
                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=15&idCredito='+idCredito+'&et='+e,true);
           }
        }
    }
    msgConfirm('Est&aacute; seguro de querer enviar este cr&eacute;dito a la etapa: <b>'+arrEstados[pos][1],resp)+'</b>';
}

function validarCambioEtapa(e)
{
	var contenedor=Ext.getCmp('content');
    var statusCred=gE('estadoCredito').value;
    var etapaDest=bD(e);
    
    var cmbCaptacion=contenedor.getFrameWindow().gE('cmbCaptacion');
    if(cmbCaptacion!=null)
    {
    	if(cmbCaptacion.options[cmbCaptacion.selectedIndex].value=='12')
        {
        	var txtExpo=contenedor.getFrameWindow().gE('txtExpo');
            if(txtExpo.value=='')
            {
            	function respVacio()
                {
                	txtExpo.focus();
                }
                msgBox('El nombre de la Expo es obligatorio',respVacio);
                return false;
            }
        
        }
    
    }
    
    switch(statusCred)
    {
    	case '4': //voBo Comite
        	var dteFechaComite=contenedor.getFrameWindow().Ext.getCmp('dDteFechaComite');
            if(dteFechaComite!=null)
            {
            	if(dteFechaComite.getValue()=='')
                {
                	function resp()
                    {
                    	dteFechaComite.focus();
                    }
                	msgBox('Debe ingresar la fecha de la resoluci&oacute;n por parte del comit&eacute;',resp);
                    return false;
                }
            }
        	
        break;
    	case '5': //Cerrado
        	var dteFechaFondeo=contenedor.getFrameWindow().Ext.getCmp('dDteFechaFondeo');
            if(dteFechaFondeo!=null)
            {
            	if(dteFechaFondeo.getValue()=='')
                {
                	function resp2()
                    {
                    	dteFechaFondeo.focus();
                    }
                	msgBox('Debe ingresar la fecha en que se llev&oacute; acabo el fondeo',resp2);
                    return false;
                }
            }
        break;
    }
	return true;
}

function mostrarHistorialCredito()
{
	var iC=bE(gE('idCredito').value);
    TB_show(lblAplicacion,'../clientes/historialCredito.php?cPagina=sFrm=true&iC='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=yes");
}

function imprimirFormato()
{
	var idCredito=gE('idCredito').value;
    var arrParam=[['idCredito',idCredito]];
    enviarFormularioDatos('../clientes/caratula.php',arrParam);
}

function modificarFechaRegistro(fP)
{
	var idCredito=gE('idCredito').value;
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                        	html:'Fecha de registro:'
                                                        },
                                                        {
                                                        	id:'dteFechaReg',
                                                        	x:130,
                                                            y:5,
                                                            xtype:'datefield',
                                                            format:'d/m/Y',
                                                            value:bD(fP)
                                                       	}
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cambiar fecha de registro del pr&eacute;stamo',
										width: 300,
										height:110,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var dteFechaReg=gEx('dteFechaReg');
                                                                        if(dteFechaReg.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaReg.focus();
                                                                            }
                                                                            msgBox('La fecha ingresada no es v&aacute;lida',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	recargarPagina();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=19&idCredito='+idCredito+'&fechaRegistro='+dteFechaReg.getValue().format('Y-m-d'),true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function eliminarCredito()
{
	var idCredito=gE('idCredito').value;
    function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	opener.parent.recargarPagina();
                	window.close();
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=22&idCredito='+idCredito,true);

        }
    }
    msgConfirm('Est&aacute; seguro de querer eliminar este cr&eacute;dito?',resp);
}