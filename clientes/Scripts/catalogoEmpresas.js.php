<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select idRelacionSocio,relacion from 706_relacionSocio order by relacion";
	$arrRelacion=uEJ($con->obtenerFilasArreglo($consulta));
	$idCliente=base64_decode($_GET["idCliente"]);
	$consulta="select idSocio,if(tipoSocio=1,(select nombre from 700_vclientes where idCliente=idClienteSocio),(select empresa from 700_empresas where idEmpresa=idClienteSocio)) as socio,idClienteSocio,idCargo,porcentaje,experiencia,tipoSocio from 715_sociosCliente where idCliente=".$idCliente;
	$arrSocios=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);
var criterioB;
function inicializar()
{
	var idEmpresa=gE('idEmpresa').value;
     var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:870,
                                        height:700,
                                        items: [
                                                    {
                                                       title: 'General',
                                                       contentEl:'tblGral'
                                                    }
                                                    
                                                     
                                                ]
                                    }
                               );
                               
	crearCampoFecha('sp_fechaAltaSatdte','_fechaAltaSatdte');
    var mascara=new Mask('$ ###,##0.00','number');
    var _nominaMensualflo=gE('_nominaMensualflo');
	_nominaMensualflo.value=mascara.format(_nominaMensualflo.value);
    mascara.attach(_nominaMensualflo);
    
    var _rentaResidenciafflo=gE('_rentaResidenciafflo');
	_rentaResidenciafflo.value=mascara.format(_rentaResidenciafflo.value);
    mascara.attach(_rentaResidenciafflo);
    
     var _valorResidenciafflo=gE('_valorResidenciafflo');
	_valorResidenciafflo.value=mascara.format(_valorResidenciafflo.value);
    mascara.attach(_valorResidenciafflo);

    var _rentaResidenciavflo=gE('_rentaResidenciavflo');
	_rentaResidenciavflo.value=mascara.format(_rentaResidenciavflo.value);
    mascara.attach(_rentaResidenciavflo);
    
     var _valorResidenciavflo=gE('_valorResidenciavflo');
	_valorResidenciavflo.value=mascara.format(_valorResidenciavflo.value);
    mascara.attach(_valorResidenciavflo);
	crearGridSocios();
    
    
}
var arrRelacion=<?php echo $arrRelacion?>;

var registroSocio=Ext.data.Record.create	(
                                                [
                                                    {name: 'idSocio'},
                                                    {name: 'socio'},
                                                    {name: 'idCliente'},
                                                    {name: 'cargo'},
                                                    {name: 'porcentaje'},
                                                    {name: 'experiencia'},
                                                    {name: 'tipoSocio'}
                                                ]
                                            )
var arrColEval=[['socio','2'],['cargo','3'],['porcentaje','4'],['experiencia','5']];                                            

function crearGridSocios()
{
	
	var cmbCargo=crearComboExt('cmbCargo',arrRelacion);
	var dsDatos=<?php echo $arrSocios?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                    			{name: 'idSocio'},
                                                                {name: 'socio'},
                                                                {name: 'idCliente'},
                                                                {name: 'cargo'},
                                                                {name: 'porcentaje'},
                                                                {name: 'experiencia'},
                                                                {name: 'tipoSocio'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Socio',
															width:250,
															sortable:true,
															dataIndex:'socio',
                                                            editor: new Ext.form.TextField(
                                                            									{
                                                                                                	id:'txtSocio',
                                                                                                    readOnly:true,
                                                                                                    listeners:	{
                                                                                                    				focus:mostrarVentanaAgregarSocio
                                                                                                    			}
                                                                                                }
                                                                                           )
														},
														{
															header:'Cargo',
															width:200,
															sortable:true,
															dataIndex:'cargo',
                                                            editor:cmbCargo,
                                                            renderer:function (val)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrRelacion,val);
                                                                        if(pos!=-1)
                                                                        	return arrRelacion[pos][1];
                                                                    }
														}
                                                        ,
														{
															header:'Porcentaje',
															width:100,
															sortable:true,
															dataIndex:'porcentaje',
                                                            editor:new Ext.form.NumberField	(
                                                            									{
                                                                                                    allowDecimals:true,
                                                                                                    allowNegative:false
                                                                                                }
                                                            								)
														},
														{
															header:'Experiencia en el sector',
															width:200,
															sortable:true,
															dataIndex:'experiencia',
                                                            editor: new Ext.form.TextField()
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridSocios',
                                                            store:alDatos,
                                                            frame:true,
                                                            renderTo:'tblSocios',
                                                            cm: cModelo,
                                                            height:150,
                                                            width:850,
                                                            sm:chkRow,
                                                            clicksToEdit :1, 
                                                            tbar:[
                                                                    {
                                                                        id:'btnAgregar',
                                                                        text:'Agregar',
                                                                        icon:'../images/add.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                                {
                                                                                	var r=new registroSocio(
                                                                                    							{
                                                                                                                	idSocio:'-1',
                                                                                                                    idCliente:'-1',
                                                                                                                 	socio:'',
                                                                                                                    cargo:'',
                                                                                                                    porcentaje:'',
                                                                                                                    experiencia:'',
                                                                                                                    tipoSocio:''   
                                                                                                                }
                                                                                    						)
                                                                                    tblGrid.getStore().add(r);
                                                                                }
                                                                    },
                                                                    {
                                                                        id:'btnRemover',
                                                                        text:'Remover',
                                                                        icon:'../images/delete.png',
                                                                        cls:'x-btn-text-icon',
                                                                        handler:function()
                                                                                {
                                                                                     var gridSocios=Ext.getCmp('gridSocios');
			                                                                        var filas=gridSocios.getSelectionModel().getSelections();
                                                                                    if(filas.length==0)
                                                                                    {
                                                                                    	msgBox('Al menos debe seleccionar un socio para remover');
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    
                                                                                    function resp(btn)
                                                                                    {
                                                                                    	if(btn=='yes')
                                                                                        {
                                                                                        	gridSocios.getStore().remove(filas);
                                                                                        }
                                                                                    }
                                                                                    msgConfirm('Est&aacute; seguro de querer remover a los socios seleccionados?',resp);
                                                                                    return;
                                                                                    
                                                                                }
                                                                    }
                                                                ]
                                                        }
                                                    );
	return 	tblGrid;	
}



function validarFrm()
{
	var Representantes='';
    var arrCombosRep=gEN('relacionesRepresentante');
    var x;
    var datosCombo;
    var relacion;
    var obj='';
    var cadObj='';
    for(x=0;x<arrCombosRep.length;x++)
    {
    	datosCombo=arrCombosRep[x].id.split('_');
        relacion=obtenerValorSelect(arrCombosRep[x]);
        obj=datosCombo[1]+'_'+relacion;
        if(cadObj=='')
        	cadObj=obj;
        else
        	cadObj+=','+obj;
    }
    gE('_idRepresentanteLegalvch').value=cadObj;
	if((validarFormularios('frmEnvio'))&&(validarSocios()))
    {
    	
        var idEmpresa=gE('idEmpresa').value;
    	var rfc1=gE('_rfcvch').value;
        var rfc2=gE('_RFC2vch').value;
        var rfc3=gE('_RFC3vch').value;
        var empresa=gE('_empresavch').value;
        
        if(!gEx('f_sp_fechaAltaSatdte').isValid())
        {
        	msgBox('La fecha de alta en hacienda no es v&aacute;lida');
        	return;
        }
        
    	function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
            	var cadProc=recolectarvaloresSocios();
                var aux=bD(gE('funcPHPEjecutarNuevo').value)+cadProc;
                gE('funcPHPEjecutarNuevo').value=bE(aux);
                gE('frmEnvio').submit();
            }
            else
            {
            	switch(arrResp[0])
                {
                	case '2':
                    	msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la empresa: '+arrResp[1]);
                        return;
                    break;
                    case '3':
                    	msgBox('El nombre de la empresa ingresada ya ha sido registrado previamente');
                        return;
                    break;
                	default:
	                	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=18&idEmpresa='+idEmpresa+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&empresa='+cv(empresa),true);

    
    	
    }
}


function validarRFC()
{
	var idEmpresa=gE('idEmpresa').value;
	var rfc1=gE('_rfcvch').value;
    var rfc2=gE('_RFC2vch').value;
    var rfc3=gE('_RFC3vch').value;
    var empresa=gE('_empresavch').value;
    if((rfc1.trim()=='')||(rfc2.trim()=='')||(rfc3.trim()==''))
    {
        msgBox('Los datos del RFC ingresados no son v&aacute;lidos');
        return;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
			msgBox('El RFC de la empresa no ha sido registrado anteriormente');
            return;
        }
        else
        {
            switch(arrResp[0])
            {
                case '2':
                    msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la empresa: '+arrResp[1]);
                    return;
                break;
                default:
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=18&idEmpresa='+idEmpresa+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&empresa='+cv(empresa),true);
}

function recolectarvaloresSocios()
{
	var gridSocios=Ext.getCmp('gridSocios');
    var dSet=gridSocios.getStore();
    var x;
    var fila;
    var obj='';
    var cadSoc='';
    for(x=0;x<dSet.getCount();x++)
    {
    	fila=dSet.getAt(x);
        obj=fila.get('idCliente')+'_'+fila.get('cargo')+'_'+fila.get('porcentaje')+'_'+fila.get('experiencia')+'_'+fila.get('tipoSocio');
        if(cadSoc=='')	
        	cadSoc=obj;
        else
        	cadSoc+='~'+obj;
    }
    var cadProc='guardarSociosEmpresa(idRegPadre,\''+cadSoc+'\')';
    
    return cadProc;

}



function validarSocios()
{
	var x=0;
    var gridSocios=Ext.getCmp('gridSocios');
    var dSet=gridSocios.getStore();
    var pos;
    for(x=0;x<arrColEval.length;x++)
    {
    	pos=validarCampoNoVacio(dSet,arrColEval[x][0]);
    	if(pos!=-1)
        {
        	pos-=1;
        	function resp()
            {
            	gridSocios.getSelectionModel().selectRow(pos);
                gridSocios.startEditing(pos,parseInt(arrColEval[x][1]));
            }
        	msgBox('El campo es obligatorio',resp)
        	return false;
        }
    }
    return true;
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}

function cerrarVentana()
{
	TB_remove();
}

var filaCliSel=null;

function mostrarVentanaAgregarRep()
{
	criterioB='1';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'},
                                                {name: 'mail'},
                                                {name: 'telefono'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    cmbNombre.on('select',function(cmb,registro)	
    						{
                            	filaCliSel=registro;
                            }
    			)
    var lblEtiqueta;
    if(criterioB=='1')
    {
    	lblEtiqueta='Nombre del representante:';
        lblDebe='Debe seleccionar un representante';
        lblEtiquetaNoExiste='&iquest;No existe el representante? agr&eacute;guelo ';
        lblTitulo='Agregar representante';
    }
    else
    {
    	lblEtiqueta='Nombre de la empresa:';
        lblDebe='Debe seleccionar una empresa';
        lblEtiquetaNoExiste='&iquest;No existe la empresa? agr&eacute;guela ';
        lblTitulo='Agregar empresa';
    }
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:20,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:220,
                                                            y:45,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarRepresentante('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        ventanaAM.close();
                                                                        var registro=filaCliSel;
                                                                        var obj='[{"idRep":"'+registro.get('idUsuario')+'","nombre":"'+registro.get('Nombre')+'","mail":"'+registro.get('mail')+'","tel":"'+registro.get('telefono')+'"}]';
                                                                       	
                                                                       	llenarDatosRepresentante(obj);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}




function inicializarCmbNombreCliente(pagina,lector, parametros,comp)
{
	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros
									}
								 );
	
	function cargarDatos(dSet)
	{
    	
		var aNombre=Ext.getCmp('cmbNombreEmpresa').getRawValue();
		dSet.baseParams.criterio=aNombre;
        
		dSet.baseParams.campoBusqueda=criterioB;
        if(comp !=undefined)
        {
        	dSet.baseParams.comp=comp;
        }
	}
	
	ds.on('beforeload',cargarDatos);
	
	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Nombre}<br />{Complementario}&nbsp;---<br>',
										'</div></tpl>'
									 )
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														id:'cmbNombreEmpresa',
														store:ds,
														displayField:'Nombre',
                                                        valueField:'idUsuario',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:300,
														pageSize:10,
                                                        x:180,
                                                        y:15,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );
    return comboNombre;
}

function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    var idCredito=gE('idCredito').value;
    vAgregarC.close();
	if(tc==1)
		TB_show(lblAplicacion,'../clientes/catalogoClientes.php?cPagina=sFrm=true&idCliente=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
	else
    	TB_show(lblAplicacion,'../clientes/catalogoEmpresas.php?cPagina=sFrm=true&idEmpresa=-1&idCredito='+idCredito+'&TB_iframe=true&height=500&width=900',"","scrolling=no");
}

function llenarDatosRepresentante(cadObj)
{

	var obj=eval(cadObj)[0];
    var arrResp=new Array();
    arrResp[1]=obj.nombre;
    arrResp[2]=obj.mail;
    arrResp[3]=obj.tel;
	TB_remove();
	var filaRep=gE('filaRep_'+obj.idRep);
    if(filaRep==null)
    {
        var tblRepresentantes=gE('tblRepresentantes');
        var nFila=tblRepresentantes.insertRow(-1);
        nFila.id='filaRep_'+obj.idRep;
        var celdaRep=nFila.insertCell(-1);
        setClase(celdaRep,'filaAzul10');
        celdaRep.setAttribute('colspan',3);
        celdaRep.innerHTML='<table width="100%"><tbody><tr><td width="80%"><label id="lblRepresentante_'+obj.idRep+'">'+arrResp[1]+'</label></td><td width="20%"><a href="javascript:modificarRepresentante(\''+bE(obj.idRep)+'\')"><img src="../images/pencil.png" alt="Modificar datos del representante" title="Modificar datos del representante"></a>&nbsp;<a href="javascript:removerRepresentante(\''+bE(obj.idRep)+'\')"><img src="../images/delete.png" alt="Remover representante" title="Remover representante"></a></td></tr></tbody></table>';
        var celdaRel=nFila.insertCell(-1);
        setClase(celdaRel,'filaAzul10');
        celdaRel.innerHTML='<select id="relacionRep_'+obj.idRep+'" class="camp_form" name="relacionesRepresentante"><option value="2">Representante Legal</option><option value="5">Socio y Rep. Legal</option><option value="8">Firmas mancomunadas</option></select>';
        var celdaMail=nFila.insertCell(-1);
        setClase(celdaMail,'filaAzul10');
        celdaMail.innerHTML='<label id="lblEmail_'+obj.idRep+'">'+arrResp[2]+'</label>';
        var celdaNextel=nFila.insertCell(-1);
        setClase(celdaNextel,'filaAzul10');
        celdaNextel.innerHTML='<label id="lblCel_'+obj.idRep+'">'+arrResp[3]+'</label>';
   }
   else
   {
        gE('lblRepresentante_'+obj.idRep).innerHTML=arrResp[1];		
        gE('lblEmail_'+obj.idRep).innerHTML=arrResp[2];		
        gE('lblCel_'+obj.idRep).innerHTML=arrResp[3];		
   }
}

function agregarRepresentante(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosRepresentante",1)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function removerRepresentante(iC)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	var fila=gE('filaRep_'+bD(iC));
        	fila.parentNode.removeChild(fila);
        }
    }
    msgConfirm('Est&aacute; seguro de querer remover al representante seleccionado?',resp)
}


function modificarRepresentante(iC)
{
	
	TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente='+bD(iC)+'&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosRepresentante",1)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function mostrarVentanaAgregarSocio()
{
	criterioB='1';
	var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario'},
												{name:'Nombre'},
                                                {name: 'complementario'}
											]
										);
	var parametros=	{
						funcion:'3',
						criterio:''
					};
    var gridSocios=Ext.getCmp('gridSocios');
    
	var cmbNombre= inicializarCmbNombreCliente(pPagina,lector,parametros);
    cmbNombre.setPosition(160,45);
    var lblEtiqueta;
   
    lblEtiqueta='Nombre del socio:';
    lblDebe='Debe seleccionar un socio';
    lblEtiquetaNoExiste='&iquest;No existe el socio? agr&eacute;guelo ';
    lblTitulo='Agregar socio';
    
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Tipo de socio:'
                                                       },
                                                       {
                                                   			xtype:'radio',
                                                            id:'chkCliente',
                                                            x:110,
                                                            y:5,
                                                            name:'rdoCliente',
                                                            value:'1',
                                                           	boxLabel :'Cliente',
                                                            checked:true,
                                                            listeners:	{
                                                                                check:function(chk,estado)
                                                                                	{
                                                                                    	if(estado)
                                                                                        {
                                                                                            criterioB=1;
                                                                                            cmbNombre.reset();
                                                                                            cmbNombre.focus();
																						}                                                                                        
                                                                                    }
                                                                        } 
                                                            
                                                        },
                                                        {
                                                        	xtype:'radio',
                                                        	id:'chkEmpresa',
                                                            value:'2',
                                                            name:'rdoCliente',
                                                            x:200,
                                                            y:5,
                                                           	boxLabel :'Empresa',
                                                            listeners:	{
                                                                                check:function(chk,estado)
                                                                                	{
                                                                                    	if(estado)
                                                                                        {
                                                                                            criterioB=2;
                                                                                            cmbNombre.reset();
                                                                                            cmbNombre.focus();
																						}                                                                                        
                                                                                    }
                                                                        } 
                                                        },
														{
                                                        	x:10,
                                                            y:50,
                                                            html:lblEtiqueta
                                                        },
                                                        cmbNombre,
                                                        {
                                                        	x:240,
                                                            y:75,
                                                        	html:lblEtiquetaNoExiste+'<a href="javascript:agregarSocio('+criterioB+')"><b><font color="red">AQU&Iacute;</font></a></b>'
                                                        }														
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vAgregarC',
										title: lblTitulo,
										width: 520,
										height:180,
										layout: 'fit',
										plain:true,
										modal:true,
                                        
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	cmbNombre.focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		if(cmbNombre.getValue()=='')
                                                                        {
                                                                        	msgBox(lblDebe);
                                                                        	return;
                                                                        }
                                                                        
                                                                        var gridSocios=Ext.getCmp('gridSocios');
                                                                        var filas=gridSocios.getSelectionModel().getSelections();
                                                                        filas[0].set('socio',cmbNombre.getRawValue());
                                                                        filas[0].set('idCliente',cmbNombre.getValue());
                                                                        filas[0].set('tipoSocio',criterioB);                   
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function llenarDatosSocio(cadObj)
{
	TB_remove();
    var obj=eval(cadObj)[0];
    var gridSocios=Ext.getCmp('gridSocios');
    var filas=gridSocios.getSelectionModel().getSelections();
    filas[0].set('socio',obj.nombre);
    filas[0].set('idCliente',obj.idRep); 
    filas[0].set('tipoSocio',criterioB);  
	
}

function agregarSocio(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
    if(criterioB=='1')
		TB_show(lblAplicacion,'../clientes/catalogoAsociados.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosSocio",1)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
    else    
	    TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarPHP='+Base64.encode('llenarDatosRepresentante(idRegPadre,"llenarDatosSocio",2)')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}



function agregarCliente(tc)
{
	var vAgregarC=Ext.getCmp('vAgregarC');
    vAgregarC.close();
	TB_show(lblAplicacion,'../clientes/catalogoEmpresasAsociadas.php?cPagina=sFrm=true&idCliente=-1&ejecutarExt='+Base64.encode('window.parent.llenarDatosCliente(@idRegistro);return;')+'&TB_iframe=true&height=440&width=890',"","scrolling=no");
}

function llenarDatosCliente(iC)
{
	TB_remove();
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	var gridClientesP=Ext.getCmp('gridClientesP');
            var r=new registroClienteP	(
                                            {
                                                idClienteP:iC,
                                                cliente:arrResp[1]
                                            }
                                        )
            gridClientesP.getStore().add(r);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=9&idEmp='+iC,true);
}

function cambiaResidencia(combo)
{
	var valor=combo.options[combo.selectedIndex].value;
    if(valor=='1')
    {
    	//oE - dE
    	oE('_rentaResidenciafflo');
        gE('_rentaResidenciafflo').value='';
        gE('_rentaResidenciafflo').setAttribute('val','');
        mE('_valorResidenciafflo');
        gE('_valorResidenciafflo').setAttribute('val','obl');
        mE('_superficiefflo');
        gE('_superficiefflo').setAttribute('val','obl');
        mE('_construidofvch');
        gE('_construidofvch').setAttribute('val','obl');
    }
    else
    {
    	//mE - hE
    	mE('_rentaResidenciafflo');
        gE('_rentaResidenciafflo').setAttribute('val','obl');
        oE('_valorResidenciafflo');
        gE('_valorResidenciafflo').value='';
        gE('_valorResidenciafflo').setAttribute('val','');
        oE('_superficiefflo');
        gE('_superficiefflo').value='';
        gE('_superficiefflo').setAttribute('val','');
        oE('_construidofvch');
        gE('_construidofvch').setAttribute('val','');
        gE('_construidofvch').value='';
    }
}


function cambiaResidenciav(combo)
{
	var valor=combo.options[combo.selectedIndex].value;
    if(valor=='1')
    {
    	//oE - dE
    	oE('_rentaResidenciavflo');
        gE('_rentaResidenciavflo').value='';
        gE('_rentaResidenciavflo').setAttribute('val','');
        mE('_valorResidenciavflo');
        gE('_valorResidenciavflo').setAttribute('val','obl');
        mE('_superficievflo');
        gE('_superficievflo').setAttribute('val','obl');
        mE('_construidovvch');
        gE('_construidovvch').setAttribute('val','obl');
    }
    else
    {
    	//mE - hE
    	mE('_rentaResidenciavflo');
        gE('_rentaResidenciavflo').setAttribute('val','obl');
        oE('_valorResidenciavflo');
        gE('_valorResidenciavflo').value='';
        gE('_valorResidenciavflo').setAttribute('val','');
        oE('_superficievflo');
        gE('_superficievflo').value='';
        gE('_superficievflo').setAttribute('val','');
        oE('_construidovvch');
        gE('_construidovvch').setAttribute('val','');
        gE('_construidovvch').value='';
    }
}
