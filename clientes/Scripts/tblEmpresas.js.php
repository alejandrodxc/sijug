<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="select b.empresa,b.idEmpresa,a.tipo,c.Nombre from 700_empresas b, 701_tipoEmpresas a, 802_identifica c where a.idTipo=b.tipo and b.idResponsable=c.idUsuario and b.cliente='1' order by b.empresa";
	$arrEmpresas=uEJ($con->obtenerFilasArreglo($consulta));
	
?>

Ext.onReady(inicializar);

function inicializar()
{
	var dsDatos=<?php echo $arrEmpresas?>;
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'empresa'},
                                                                     {name: 'idEmpresa'},
                                                                    {name: 'Tipo'},
                                                                    {name: 'Nombre'}                                                                ]
                                                    	}
                                                );

    alDatos.loadData(dsDatos);
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														
														{
															header:'Empresa',
															width:500,
															sortable:true,
															dataIndex:'empresa'
														},
														
                                                        {
															header:'Responsable',
															width:200,
															sortable:true,
															dataIndex:'Nombre'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            height:460,
                                                            width:800,
                                                            renderTo:'tblEmpresas',
                                                            tbar:	[
                                                            			{
                                                                        	text:'Nueva Empresa',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                    	var arrParam=[['idEmpresa','-1']];
                                                                                    	enviarFormularioDatos('catalogoEmpresas.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Modificar empresa',
                                                                            icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar la empresa a modificar');
                                                                                            return;
                                                                                       }
                                                                                       var idEmpresa=fila.get('idEmpresa');
                                                                                       var arrParam=[['idEmpresa',idEmpresa]];
                                                                                    	enviarFormularioDatos('catalogoEmpresas.php',arrParam);
                                                                                    }
                                                                        },
                                                                        {
                                                                        	text:'Eliminar empresa',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            handler:function()
                                                                            		{
                                                                                       var fila=tblGrid.getSelectionModel().getSelected();
                                                                                       if(fila==null)
                                                                                       {
                                                                                       		msgBox('Debe seleccionar a la empresa a eliminar');
                                                                                            return;
                                                                                       }
                                                                                       
                                                                                       function resp(btn)
                                                                                       {
                                                                                       		if(btn=='yes')
                                                                                            {
                                                                                            	var idCliente=fila.get('idEmpresa');
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                    	tblGrid.getStore().remove(fila);	
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=21&idCliente='+idCliente+'&idTipoCliente=2',true);
                                                                                            }
                                                                                       }
                                                                                       msgConfirm('Est&aacute; seguro de querer eliminar a la empresa seleccionada?',resp)
                                                                                       
                                                                                       
                                                                                    }
                                                                        }
                                                            		]
                                                            
                                                        }
                                                    );
	
}

