<?php session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="select idUnidadesRoles,unidadRol from 4084_unidadesRoles where idCategoria=7 order by unidadRol";
	$arrGrupos=uEJ($con->obtenerFilasArreglo($consulta));
?>
var arrGrupos=<?php echo $arrGrupos?>;
var p=window.parent;

Ext.onReady(inicializar);

function inicializar()
{
	crearCampoFecha('dteFInicio','fInicio');
    crearCampoFecha('dteFin','fFin');
}

function verListadoCredito(l,titulo)
{
	var arrParam=[['tipoTablero',bE('4')],['parametro',l],['cPagina','sFrm=true|mR1=true'],['titulo',titulo]];
    enviarFormularioDatos('../clientes/listadoCreditos.php',arrParam);
}

function verGrupos()
{
	var arrParam=[['vG',''],['cPagina','sFrm=true'],['fInicio',gE('fInicio').value],['fFin',gE('fFin').value]];
	enviarFormularioDatos('../clientes/tableroCredito.php',arrParam);
}
function verGral()
{
	var arrParam=[['cPagina','sFrm=true'],['fInicio',gE('fInicio').value],['fFin',gE('fFin').value]];
	enviarFormularioDatos('../clientes/tableroCredito.php',arrParam);
}

function verDistGraficaGpo()
{
	var vG=gE('vG');
 	var listGrupos='';
    if(vG!=null)
    	listGrupos=vG.value;
	p.mostrarTickBox('../graficas/creditosGrupo.php?listGrupos='+listGrupos+'&cPagina=sFrm=true&fInicio='+gE('fInicio').value+'&fFin='+gE('fFin').value,500,800);	
}

function verDistGraficaTablero(iG)
{
	var comp='';
	if(iG!=undefined)
    	comp='&listGrupos='+(iG);
	p.mostrarTickBox('../graficas/creditosTablero.php?cPagina=sFrm=true&fInicio='+gE('fInicio').value+'&fFin='+gE('fFin').value+comp,530,800);	
}

function actualizarPeriodo()
{
	gE('fInicio').value=Ext.getCmp('f_dteFInicio').getValue().format('Y-m-d');
    gE('fFin').value=Ext.getCmp('f_dteFin').getValue().format('Y-m-d');
    gE('frmEnvioFecha').submit();
}

function verFiltroGrupo()
{
	var gridGrupos=crearGridGrupos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                        	html:'Seleccione los grupos que desea observar: '
                                                        },
                                                        gridGrupos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Filtrado por grupo',
										width: 310,
										height:370,
                                        y:55,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var filas=gridGrupos.getSelectionModel().getSelections();
																		if(filas.length==0)
                                                                        {
                                                                        	msgBox('Al menos debe seleccionar un grupo');
                                                                            return;
                                                                        }
                                                                        var listGrupos='';
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(listGrupos=='')
                                                                            	listGrupos=filas[x].get('idGrupo');
                                                                            else
                                                                            	listGrupos+=','+filas[x].get('idGrupo');
                                                                        }
                                                                        gE('vG').value=listGrupos;
                                                                        gE('frmEnvioFecha').submit();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridGrupos()
{
	var dsDatos=arrGrupos;
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idGrupo'},
                                                                {name: 'grupo'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Grupo',
															width:150,
															sortable:true,
															dataIndex:'grupo'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridGrupos',
                                                            store:alDatos,
                                                            frame:true,
                                                            y:40,
                                                            x:15,
                                                            cm: cModelo,
                                                            height:240,
                                                            width:240,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;
}

function verPromotor(iG)
{
	var comp='';
	if(iG!=undefined)
    	comp='&listGrupos='+(iG);
	p.mostrarTickBox('../graficas/creditosPromotor.php?cPagina=sFrm=true&fInicio='+gE('fInicio').value+'&fFin='+gE('fFin').value+comp,530,800);	
}

function verDistGraficaUsr()
{
	var idUsuario=gE('idUsuario').value;
	p.mostrarTickBox('../graficas/creditosUsuario.php?cPagina=sFrm=true&fInicio='+gE('fInicio').value+'&fFin='+gE('fFin').value+'&idUsr='+idUsuario,530,800);	
}