<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	var idEmpresa=gE('idEmpresa').value;
     var tabs = new Ext.TabPanel(
                                    {
                                        renderTo: 'tab',
                                        activeTab: 0,
                                        width:850,
                                        height:250,
                                        items: [
                                                    {
                                                       title: 'General',
                                                       contentEl:'tblGral'
                                                    }
                                                ]
                                    }
                               );
}

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	
    	var idEmpresa=gE('idEmpresa').value;
    	var rfc1=gE('_rfcvch').value;
        var rfc2=gE('_RFC2int').value;
        var rfc3=gE('_RFC3vch').value;
        var empresa=gE('_empresavch').value;
    	function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                gE('frmEnvio').submit();
            }
            else
            {
            	switch(arrResp[0])
                {
                	case '2':
                    	msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la empresa: '+arrResp[1]);
                        return;
                    break;
                    case '3':
                    	msgBox('El nombre de la empresa ingresada ya ha sido registrado previamente');
                        return;
                    break;
                	default:
	                	msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=18&idEmpresa='+idEmpresa+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&empresa='+cv(empresa),true);

    }
}

function validarRFC()
{
	var idEmpresa=gE('idEmpresa').value;
	var rfc1=gE('_rfcvch').value;
    var rfc2=gE('_RFC2int').value;
    var rfc3=gE('_RFC3vch').value;
    var empresa=gE('_empresavch').value;
    if((rfc1.trim()=='')||(rfc2.trim()=='')||(rfc3.trim()==''))
    {
        msgBox('Los datos del RFC ingresados no son v&aacute;lidos');
        return;
    }
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
			msgBox('El RFC de la empresa no ha sido registrado anteriormente');
            return;
        }
        else
        {
            switch(arrResp[0])
            {
                case '2':
                    msgBox('El RFC ingresado ya ha sido registrado previamente bajo el nombre de la empresa: '+arrResp[1]);
                    return;
                break;
                default:
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRcCorporativo.php',funcAjax, 'POST','funcion=18&idEmpresa='+idEmpresa+'&rfc1='+cv(rfc1)+'&rfc2='+cv(rfc2)+'&rfc3='+cv(rfc3)+'&empresa='+cv(empresa),true);
}

function refrescarSucursales()
{
	var idBanco=gE('idBanco');
	gE('idSucursales').src='../bancos/tblSucursales.php?idBanco='+idBanco;
}

function nuevaSucursal()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function modificarSucursal(iS)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoSucursales.php?idSucursal='+iS+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarSucursales);
}

function nuevaCuenta()
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta=-1&idBanco='+idBanco+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function modificarCuenta(iC)
{
	var idBanco=gE('idBanco').value;
	TB_show(lblAplicacion,'../bancos/catalogoCuentas.php?idCuenta='+iC+'&TB_iframe=true&height=520&width=800',"","scrolling=no",refrescarCuentas);
}

function refrescarCuentas()
{
	var idBanco=gE('idBanco');
	gE('idCuentas').src='../bancos/tblCuentas.php?idBanco='+idBanco;
}

function cerrarVentana()
{
	TB_remove();
}
