<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");

function generarTablaAval($fila,$nAval,$secAvales)
{
	global $con;
	
?>

	<table width="100%">
    <tr>
    <td>
    	<span class="letraRojaSubrayada8">Aval <?php echo $nAval?></span><br /><br />
        <table cellspacing="0" cellpadding="0" width="100%">
       	<?php
			if($fila[1]!="")
			{
				$arrDatosCli=explode(",",$fila[1]);
				$ct=1;
				foreach($arrDatosCli as $aval)
				{
					$arrDAval=explode("_",$aval);
					if($arrDAval[2]=="1")
						$consulta="select concat(nombres,' ',paterno,' ',materno) as nombre, telefonos,email from 703_clientes where idCliente=".$arrDAval[0];
					else
						$consulta="select empresa, telefonos,email from 700_empresas where idEmpresa=".$arrDAval[0];
			
					$filaC=$con->obtenerPrimeraFila($consulta);
			?>
					<tr height='23'>
						 <td width="20%"  class="filaRosa10">Propietario <?php echo $ct?>: </td>
						 <td class="filaAzul10" width="20%">
						 <label id="lblAval" >
						 <?php 
							echo $filaC[0];
						 ?>
						 </label>
						 <input type="hidden" name="_idClienteint" id="_idClienteint"  value=""/>
						 </td>
						 <td colspan="2"  class="filaRosa10">Tel&eacute;fono:</td>
						 <td colspan="3" class="filaAzul10">
						 <label id="lblCel">
						 <?php 
							echo $filaC[1];
						 ?>
						 </label>
						 </td>
					</tr>
					<tr height='23'>
					 <td  class="filaRosa10" >Relación/Parentesco:</td>
					 <td class="filaAzul10">
						 <label>
						   <?php
							  $consulta="select parentesco from 713_relacionParentesco where idRelacionParentesco=".$arrDAval[1];
							  $parentesco=$con->obtenerValor($consulta);
							  echo $parentesco;
						  ?>
						 </label>
						 </td>
						 <td colspan="2"  class="filaRosa10">Correo electr&oacute;nico:</td>
						 <td colspan="3" class="filaAzul10"> 
						 <label id="lblEmail">
						 <?php 
							echo $filaC[2];
						 ?>
						 </label>
					 </td>
					</tr>
		   <?php
		   			$ct++;
				}
			}
		?>
       <tr height='23'>
       
         <td  class="filaRosa10">Patrimonio:</td>
         <td colspan="6" class="filaAzul10">
         <label>
         <?php
            echo $fila[3];
        ?>
         </label>
         </td>
         
       </tr>
       <tr height='23'>
         <td  class="filaRosa10">Valor Declarado:</td>
         <td class="filaAzul10">
         <label>$ 
         <?php
            echo number_format($fila[2],"2",".",",");
        ?>
         </label>
         </td>
         <td   class="filaRosa10">Disponible a Hipoteca:</td>
         <td align="left" class="filaAzul10">
         <label>
           <?php
              $consulta="select texto from 1004_siNo where idIdioma=".$_SESSION["leng"]." and valor=".$fila[4];
                $texto=$con->obtenerValor($consulta);
                echo $texto;
          ?>
         </label>
         </td>
         <td align="left" colspan="4" class="filaRosa10">                    </td>
         
       </tr>
       <tr height='23'>
         <td  class="filaRosa10">Superficie aprox.:</td>
         <td class="filaAzul10">
         <label>
         <?php
            echo $fila[5];
        ?>
         </label>
         </td>
         <td colspan="5" class="filaRosa10">&nbsp;</td>
              </tr>
     </table>
 <br />

	</td>
    <td valign="top">
    <?php if($secAvales){?>
    	<a href="javascript:modificarAval(<?php echo $fila[0] ?>)">
	    	<img src="../images/pencil.png" title='Modificar datos de aval' alt='Modificar datos de aval' />
        </a>
    	<a href="javascript:removerAval(<?php echo $fila[0] ?>)">
    	<img src="../images/delete.png" title='Remover aval' alt='Remover aval'/>
        </a>
    <?php } ?>
    </td>
    </tr>
    </table>
<?php
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$mostrarMenuIzq=false;
?>
<style type="text/css">
<!--
@import url("../css/estiloFicha.css");
-->
</style>
<link rel="stylesheet" type="text/css"  href="../Scripts/ux/grid/GroupSummary.css" media="screen" />
<link rel="stylesheet" type="text/css"  href="../Scripts/ux/resources/style.css" media="screen" />
<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../Scripts/masks.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GroupSummary.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/EditableItem.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/RangeMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/menu/ListMenu.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GridFilters.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/Filter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/StringFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/DateFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/ListFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/NumericFilter.js"></script>
<script type="text/javascript" src="../Scripts/ux/grid/filter/BooleanFilter.js"></script>
<link rel="stylesheet" type="text/css" href="../Scripts/ux/grid/RowEditor.css"/>
<script type="text/javascript" src="../Scripts/ux/grid/RowEditor.js"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<link rel="stylesheet" href="../Scripts/thickbox/thickboxExt.css" type="text/css" />
<script type="text/javascript" src="../Scripts/thickbox/jquery.js"></script>
<script type="text/javascript" src="../Scripts/thickbox/thickbox.js"></script>
<style>
	.x-window
	{
		z-index:11501 !important;
	}
	.ext-el-mask
	{
		z-index:11500 !important;
	}
	.x-combo-list
	{
		z-index:11502 !important;
	}
</style>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
                                  ?>
                                  
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <?php
								$conRol="SELECT codigoRol FROM 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
								$varPromotor = false;
									$varRoot = false;
									$varGerente = false;
									$varCerrador = false;
									$varAnalisis = false;
								$resUsr=$con->obtenerFilas($conRol);
								while($filaUsr=mysql_fetch_row($resUsr))
								{			
									$rol= obtenerTituloRol($filaUsr[0]);
									$arrRol=explode("_",$filaUsr[0]);
									switch($arrRol[0])
									{
										case 38:
											$varPromotor = true;
										break;
										case 1:
											$varRoot = true;
										break;
										case 39:
											$varGerente = true;
										break;
										case 40:
											$varCerrador =true;
										break;
										case 41:
											$varAnalisis =true;
										break;
									}
								}
							?>
	                   	<tr>
                        <td align="center">
                        <?php
						
							$idCredito=-1;
							if(isset($objParametros->idCredito))
								$idCredito=$objParametros->idCredito;

							$consulta="select * from 750_creditos where idCredito=".$idCredito;
							$filaCred=$con->obtenerPrimeraFila($consulta);
							$idCliente=$filaCred[1];
							$tipoCliente=$filaCred[2];
							
							$secGral=false;
							$secSaldos=false;
							$secMontos=false;
							$secAvales=false;
							$secBienes=false;
							$secObservaciones=false;
							$secInteres=false;
							$fechaComite=false;
							$fechaFondeo=false;
							$fechaEntrada=false;
							$estado=$filaCred[17];
							$cerradorRC=false;
							$cerradorFondo=false;
							$acComite=false;
							
							if(($estado!=7)&&($estado!=8)&&($estado!=9)&&($estado!=10))
							{
									switch($estado)
									{
										case 1: //Registro
											if(existeRol("'41_0'")) //Analisis
											{
												
												
											}
											if(existeRol("'38_-1'")) //Promotor
											{
												$secGral=true;
												$secSaldos=true;
												$secMontos=true;
												$secAvales=true;
												$secBienes=true;
											
											}
										break;
										case 2: //Análisis de crédito
											if(existeRol("'41_0'")) //Analisis
											{
												$secGral=true;
												$secSaldos=true;
												$secMontos=true;
												$secAvales=true;
												$secBienes=true;
												$secObservaciones=true;
												$secInteres=true;
												$fechaComite=true;
												$fechaFondeo=true;
												$fechaEntrada=true;
												$cerradorRC=true;
												$cerradorFondo=true;
												$acComite=true;
											}
										break;
										case 3: //VoBo Dirección
											if(existeRol("'41_0'")) //Analisis
											{
												$secObservaciones=true;
											}
										break;
										case 4:  //VoBo Comité
											if(existeRol("'41_0'")) //Analisis
											{
												$secObservaciones=true;
												
											}
											if(existeRol("'42_0'")) //DG
											{
												$fechaComite=true;						
												$acComite=true;
											}
											if(existeRol("'43_0'")) //Comite
											{
												$fechaComite=true;
												$acComite=true;
											}
										
										break;
										case 5:  //Cerrado
											if(existeRol("'39_-1'")) //Gerente
											{
												$secInteres=true;
												$fechaFondeo=true;
												
											}
											if(existeRol("'40_0'")) //Cerrador
											{
												$secInteres=true;
												$fechaFondeo=true;
												
											}
										break;
										case 6: //Fondeado
											if(existeRol("'39_-1'")) //Gerente
											{
												$secInteres=true;
												
											}
											if(existeRol("'40_0'")) //Cerrador
											{
												$secInteres=true;
												
											}
										break;
										case 11:
											if(existeRol("'39_-1'")) //Gerente
											{
												$secInteres=true;
												
											}
											if(existeRol("'40_0'")) //Cerrador
											{
												$secInteres=true;
												
											}
										break;
									}
							}
							
							if((existeRol("'1_0'"))||(existeRol("'42_0'"))||(existeRol("'41_0'"))) //Root
							{
								$secGral=true;
								$secSaldos=true;
								$secMontos=true;
								$secAvales=true;
								$secBienes=true;
								$secObservaciones=true;
								$secInteres=true;
								$fechaComite=true;
								$fechaFondeo=true;
								$fechaEntrada=true;
								$cerradorRC=true;
								$cerradorFondo=true;
								$acComite=true;
							}

							$sS="0";
							if($secSaldos)
								$sS="1";
								
							$sM="0";
							if($secMontos)	
								$sM="1";
							$sB="0";
							if($secBienes)
								$sB="1";
							$consulta="select estadoCredito from 752_estadosCredito where numEstado=".$filaCred[17];
							$estado=$con->obtenerValor($consulta);
		?>
                        <script type="text/javascript" src="Scripts/cuentasBanco.js.php?S=<?php echo bE($sS)?>&M=<?php echo bE($sM) ?>&B=<?php echo bE($sB) ?>&idCredito=<?php echo base64_encode($idCredito)?>"></script>
                         <table width="800">
                         <tr height='23'>
                         	<td align="left">
                         
	                        <?php
							
							if($tipoCliente==2)
							{
								$consulta="select empresa,rfc,fechaAltaSAT,direccionF,colF,ciudadf,telefonos,email,web,noEmpleados,nominaMensual,numf,cpf,direccionv,
											colv,ciudadv,numv,cpv,tiempoResidenciaf,tiempoResidenciav,valorResidenciaF,valorResidenciaV,superficief,superficiev,
											tipoResidenciaf,tipoResidenciav,rentaResidenciaf,rentaResidenciav,funcionRepLegal,actividadPrincipal,familiar,tipoResidenciaf,idRepresentanteLegal,delf,delv,construidof,construidov,RFC2,RFC3
											from 700_empresas where idEmpresa=".$idCliente;
								$filaCl=$con->obtenerPrimeraFila($consulta);
								
						?>		
								<table cellspacing="0" cellpadding="0" width="100%">
                              <tr height='23'>
                                <td colspan="4" class="filaRosa10">Ficha de Cr&eacute;dito para Empresa 
								<?php 
									if($secGral)
									{?>
                                        [MODIFICAR FICHA <a href='javascript:modificarCliente()'><img src="../images/pencil.png" alt='Modificar datos de la empresa' title='Modificar datos de la empresa'/></a>]
								<?php 
									} ?></td>
                                <td width="18%" class="filaRosa10">RFC:</td>
                                <td width="17%" class="filaRosa10">Telefonos:</td>
                              </tr>
                              <tr height='23'>
                                <td colspan="4" align="left" class="filaAzul10" >
                                <label ><?php echo $filaCl[0]?></label>
								<input type="hidden" id="idCliente" value="<?php echo $idCliente ?>" /></td>
                                <td align="left" class="filaAzul10"><label><?php echo $filaCl[1]?>-<?php echo $filaCl[37]?>-<?php echo $filaCl[38]?></label></td>
                                <td align="left" class="filaAzul10"><label><?php echo $filaCl[6]?></label></td>
                              </tr>
                              <tr height='23'>
                                <td colspan="3" class="filaRosa10">REPRESENTANTE LEGAL                                </td>
                                <td width="28%" class="filaRosa10">RELACION CON LA EMPRESA</td>
                                <td class="filaRosa10">CORREO ELECTRONICO</td>
                                <td class="filaRosa10">Nextel, Celular:</td>
                              </tr>
                              <?php
							  	$idC="-1";
								if($filaCl[32]!="")
								{
									$arrRepresentantes=explode(",",$filaCl[32]);
									foreach($arrRepresentantes as $representante)
									{
										$arrDatosRep=explode("_",$representante);
							  ?>
                                      <tr height='23'>
                                        <td colspan="3" align="left" class="filaAzul10">
                                         <label>
                                        <?php
                                        $consulta="select * from 703_clientes where idCliente=".$arrDatosRep[0];
                                        $filaDatosC=$con->obtenerPrimeraFila($consulta);
                                        echo  $filaDatosC[5]." ".$filaDatosC[1]." ". $filaDatosC[2]
                                        ?>
                                        </label>
                                        </td>
                                        <td class="filaAzul10">
                                        <?php
                                                $consulta="select  relacion from 706_relacionSocio where idRelacionSocio='".$arrDatosRep[1]."'";
                                                $relacion=$con->obtenerValor($consulta);
                                            ?>	
                                            <label>
                                            <?php
                                                echo $relacion;
                                            ?>
                                            </label>
                                        </td>
                                        <td align="left" class="filaAzul10">
                                            <label><?php echo $filaDatosC[10]?></label>
                                        </td>
                                        <td class="filaAzul10"><label><?php echo $filaDatosC[9]?></label></td>
                                      </tr>
                              <?php
									}
								}
							  ?>
                              <tr height="21">
                              	<td class="filaBlanca10" colspan="6">
                                SOCIOS
                                </td>
                              </tr>
                              <tr>
                              	<td colspan="6" >
                                	<table  style="width:100% !important" >
                                    <thead>
                                    <tr>
                                    	<th style="width:35%" !important; class="filaRosa10">Socio</th>
                                        <th class="filaRosa10">Cargo</th>
                                        <th class="filaRosa10">Porcentaje</th>
                                        <th style="width:30%" !important; class="filaRosa10">Experiencia</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
										
										//$consulta ="select (select nombre from 700_vclientes where idCliente=idClienteSocio) as socio,rs.relacion,porcentaje,experiencia,a.idCredito from 715_sociosCliente s,706_relacionSocio rs, 715_principalesClientes a where rs.idRelacionSocio=s.idCargo and s.idCliente = a.idClientePrincipal and not a.idCredito =".$idCredito;
										$consulta="select if(tipoSocio=1,(select nombre from 700_vclientes where idCliente=idClienteSocio),(select empresa from 700_empresas where idEmpresa=idClienteSocio) )as socio, s.idClienteSocio, rs.relacion,porcentaje,experiencia,tipoSocio from 715_sociosCliente s,706_relacionSocio rs where rs.idRelacionSocio=s.idCargo and idCliente=".$idCliente;
										
										$resSoc=$con->obtenerFilas($consulta);
										while($filaSoc=mysql_fetch_row($resSoc))
										{
									?>
                                    	<tr>
                                        	<td class="filaAzul10"><?php echo $filaSoc[0] ?>
                                            		<?php
														
														$conOtroCred="select idSocio from 715_sociosCliente s,750_creditos c where s.idCliente=c.idCliente and s.tipoSocio=".$filaSoc[5]." and idCredito<>".$idCredito." and idClienteSocio=".$filaSoc[1]." and c.status not in (1,8,10,7)";
														//echo $conOtroCred;
														$SocDup=$con->obtenerFilas($conOtroCred);
														
														if($con->filasAfectadas>0)
														{ 
															echo "<a href='javascript:verCreditosSocio(\"".bE($filaSoc[1])."\",\"".bE($filaSoc[5])."\")'><img src='../images/exclamation.png' title='El socio indicado cuenta con otros créditos registrados' alt='El socio indicado cuenta con otros créditos registrados'></a>";
														}
																
								
														?>
                                
                                            
                                            </td>
                                            <td  class="filaAzul10"><?php echo $filaSoc[2] ?></td>
                                            <td  class="filaAzul10"><?php echo $filaSoc[3] ?></td>
                                            <td  class="filaAzul10"><?php echo $filaSoc[4]?></td>
                                        </tr>
                                    <?php
										}
									
									?>
                                    </tbody>
                                    </table>
                                </td>
                              </tr>
                              <tr height='23'>
                                <td width="15%" class="filaRosa10" >ACTIVIDAD PRINCIPAL</td>
                                <td colspan="5" class="filaAzul10" >
                                    <label>
                                    <?php
										echo $filaCl[29];
									?>
                                    </label>
                                
                                </td>
                              </tr>
                            
                                  <tr height='23'>
                                    <td  class="filaRosa10">EMPRESA FAMILIAR:</td>
                                    <td width="61" align="left" class="filaAzul10">
                                    <label>
                                    <?php
										
										$valorFam=$filaCl[30];
										if ($valorFam=="")
											$valorFam=-1;
										
										$consulta="select texto from 1004_siNo where idIdioma=".$_SESSION["leng"]." and valor=".$valorFam;
										$valorFam=$con->obtenerValor($consulta);
										echo $valorFam;
									?>
                                    </label>
                                    </td>
                                    
                                    <td  class="filaRosa10">No. empleados:</td>
                                    <td  align="left" class="filaAzul10"><label><?php echo number_format($filaCl[9],0,".",",")?></label></td>
                                    <td width="18%" align="left" class="filaRosa10">Nom. Mensual</td>
                                    <td  align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[10],2,".",",")?></label></td>
                                    </tr>
                                  <tr height='23'>
                                    <td class="filaRosa10">ALTA EN HACIENDA:</td>
                                    <td class="filaAzul10"> 
                                    <label>
									<?php 
									if($filaCl[2]!="")
										echo date("d/m/Y",strtotime($filaCl[2]))
									?>
                                    </label></td>
                                    <td class="filaRosa10">WEB:</td>
                                    
                                    <td colspan="12" align="left" class="filaAzul10"><label><?php echo $filaCl[8]?></label></td>
                                    </tr>
                                  </table>
								<table width="100%">
                                    <td colspan="16" class="filaBlanca10">DOMICILIO FISCAL</td>
                                    </tr>
                                  <tr height='23'>
                                    <td class="filaRosa10">Calle:</td>
                                    <td colspan="4" align="left" class="filaAzul10"><label><?php echo $filaCl[3]?></label></td>
                                    <td class="filaRosa10">Num.</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[11]?></label></td>
                                    <td class="filaRosa10">C.P.</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[12]?></label></td>
                                    <td class="filaRosa10">Col.</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[4]?></label></td>
                                    <td class="filaRosa10">Cd.</td>
                                    <td colspan="2" class="filaAzul10"><label><?php echo $filaCl[5]?></label></td>
                                    
                                    <td class="filaRosa10">Del.</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[33]?></label></td>
                                    
                                  </tr>
                                  <tr height='23'>
                                    <td class="filaRosa10">Propiedad</td>
                                    <td class="filaAzul10">
                                    <label>
                                  <?php
								  	$valProp=$filaCl[24];
									if ($valProp=="")
										$valProp=-1;
										
								  	$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$valProp;
									$tipo=$con->obtenerValor($consulta);
									echo $tipo;
								  ?>
                                 </label>
                                    </td>
                                    <td colspan="6" class="filaRosa10">Tiempo de residencia</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[18]?></label></td>
                                    <td colspan="4" class="filaRosa10">Renta mensual </td>
                                    <td colspan="3" class="filaAzul10"><label>$ <?php echo number_format($filaCl[26],2,".",",")?></label></td>
                                    </tr>
                                  <tr height='23'>
                                    <td colspan="2" class="filaRosa10">Valor declarado</td>
                                    <td colspan="5" class="filaAzul10"><label>$ <?php echo number_format($filaCl[20],2,".",",")?></label></td>
                                    
                                    <td colspan="3" class="filaRosa10">Superficie apr&oacute;x.</td>
                                    <td colspan="2" class="filaAzul10"><label><?php echo $filaCl[22]?></label></td>
                                    <td colspan="3" class="filaRosa10">Mts. construcci&oacute;n</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[35]?></label></td>
                                    
                                  </tr>
                                
                                <tr height='23'>
                                  <td colspan="16" class="filaBlanca10">DOMICILIO PARTICULAR</td>
                                </tr>
                                <tr height='23'>
                                  <td class="filaRosa10">Calle:</td>
                                  <td colspan="4" class="filaAzul10"><label><?php echo $filaCl[13]?></label></td>
                                  <td width="20" class="filaRosa10">Num.</td>
                                  <td class="filaAzul10"><label><?php echo $filaCl[16]?></label></td>
                                  <td class="filaRosa10">C.P.</td>
                                  <td class="filaAzul10"><label><?php echo $filaCl[17]?></label></td>
                                  <td class="filaRosa10">Col.</td>
                                  <td colspan="2" class="filaAzul10"><label><?php echo $filaCl[14]?></label></td>
                                  <td class="filaRosa10">Cd.</td>
                                  <td class="filaAzul10"><label><?php echo $filaCl[15]?></label></td>
                                  
                                    <td class="filaRosa10">Del.</td>
                                    <td class="filaAzul10"><label><?php echo $filaCl[34]?></label></td>
                                </tr>
                                <tr height='23'>
                                  <td class="filaRosa10">Propiedad</td>
                                  <td class="filaAzul10" align="left">
                                  <label>
                                  <?php
								  	$valProp=$filaCl[25];
									if ($valProp=="")
										$valProp=-1;
								  
								  	$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$valProp;
									$tipo=$con->obtenerValor($consulta);
									echo $tipo;
								  ?>
                                 </label>
                                  </td>
                                  <td colspan="6" class="filaRosa10">Tiempo de residencia</td>
                                  <td class="filaAzul10"><label><?php echo $filaCl[19]?></label></td>
                                  <td colspan="4" class="filaRosa10">Renta mensual: </td>
                                  <td colspan="3" class="filaAzul10"><label>$ <?php echo number_format($filaCl[27],2,".",",")?></label></td>
                                </tr>
                                <tr height='23'>
                                  <td colspan="2" class="filaRosa10">Valor declarado</td>
                                  <td colspan="5" align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[21],2,".",",")?></label></td>
                                  
                                  <td colspan="3" class="filaRosa10">Superficie Aprox.</td>
                                  <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[23]?></label></td>
                                  
                                  <td colspan="3" class="filaRosa10">Mts. construcci&oacute;n</td>
                                  <td class="filaAzul10"><label><?php echo $filaCl[36]?></label></td>
                                    
                                </tr>
                                </table>
                                  
                                  
                                <table>
                                <tr>
                                	<td colspan="16" class="filaBlanca10" colspan="2">
                                    PRINCIPALES CLIENTES&nbsp;
									<?php 
									if($secGral)
									{?>
                                    	<a href="javascript:mostrarVentanaAgregarCliente()"><img src='../images/add.png' title="Agregar principal cliente" alt="Agregar principal cliente" /></a>
									<?php 
									}
									?>
                                    </td>
                                </tr>
                                 <TR>
                                	<td colspan="16">
                                    <table  width="100%">
                                    <tbody>
                                    <?php
										$consulta="select idPrincipalesClientes,e.empresa from 715_principalesClientes pc,700_empresas e where e.idEmpresa=pc.idClientePrincipal and pc.idCredito=".$idCredito;
										
										$resCli=$con->obtenerFilas($consulta);
										while($filaCli=mysql_fetch_row($resCli))
										{
									?>
                                    <tr id='fila_<?php echo $filaCli[0]?>'>
                                    	<td class="filaAzul10" width="30" align="center">
										<?php 
										if($secGral)
										{?>
                                        	<a href='javascript:removerClienteP("<?php echo base64_encode($filaCli[0])?>")'><img src='../images/delete.png' title="Remover cliente" alt="Remover cliente" /></a><?php 
										} ?>
                                        </td><td class="filaAzul10"><?php echo $filaCli[1]?>
                                        </td>
                                        
                                    </tr>
                                    <?php
										}
									?>
                                    </td>
                                </TR>
                              </table>
                              
							<?php		
							}
							else
							{
								$consulta="select  paterno,materno,rfc,fechaAltaSAT,nombres,direccionF,colF,ciudadf,telefonos,email,web,noEmpleados,nominaMensual,numf,cpf,direccionv,
											colv,ciudadv,numv,cpv,tiempoResidenciaf,tiempoResidenciav,valorResidenciaF,valorResidenciaV,superficief,superficiev,
											tipoResidenciaf,tipoResidenciav,rentaResidenciaf,rentaResidenciav,funcionRepLegal,actividadPrincipal,tipoResidenciaf,idRepresentanteLegal,RFC2,RFC3 from
											703_clientes where idCliente=".$idCliente;
								$filaCl=$con->obtenerPrimeraFila($consulta);
								
							?>
                        	 <table cellspacing="0" cellpadding="0" width="100%">
                              <tr height='23'>
                                <td colspan="2" class="filaRosa10">Paterno&nbsp;
								<?php 
								if($secGral)
								{?>
                                	<a href='javascript:modificarCliente()'><img src="../images/pencil.png" alt='Modificar datos de la empresa' title='Modificar datos de la empresa'/></a>
								<?php 
								} 
								?></td>
                                <td width="17%" class="filaRosa10">Materno</td>
                                <td colspan="2" class="filaRosa10">Nombres</td>
                                <td width="19%" class="filaRosa10">RFC:</td>
                                <td width="18%" class="filaRosa10">Telefonos:</td>
                              </tr>
                              <tr height='23'>
                                <td colspan="2" align="left" class="filaAzul10">
                                <input type="hidden" id="idCliente" value="<?php echo $idCliente ?>" />
                                <label><?php echo $filaCl[0]?></label>
                                </td>
                                <td align="left" class="filaAzul10"><label><?php echo $filaCl[1]?></label></td>
                                <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[4]?></label></td>
                                <td align="left" class="filaAzul10">
                                
                          
                                <label><?php echo $filaCl[2]?>-<?php echo $filaCl[34]?>-<?php echo $filaCl[35]?></label>
                                
                                
                                
                                </td>
                                <td align="left" class="filaAzul10"><label><?php echo $filaCl[8]?></label></td>
                              </tr>
                              <tr height='23'>
                                <td colspan="3" class="filaRosa10">REPRESENTANTE LEGAL:</td>
                                <td colspan="2" class="filaRosa10">RELACION CON LA EMPRESA</td>
                                <td class="filaRosa10">CORREO ELECTRONICO</td>
                                <td class="filaRosa10">Nextel, Celular:</td>
                              </tr>
                               <?php
							  	$idC="-1";
								
								if($filaCl[33]!="")
								{
									$arrRepresentantes=explode(",",$filaCl[33]);

									foreach($arrRepresentantes as $representante)
									{
										$arrDatosRep=explode("_",$representante);
							  ?>
                                      <tr height='23'>
                                        <td colspan="3" align="left" class="filaAzul10">
                                         <label>
                                        <?php
                                        $consulta="select * from 703_clientes where idCliente=".$arrDatosRep[0];
                                        $filaDatosC=$con->obtenerPrimeraFila($consulta);
                                        echo  $filaDatosC[5]." ".$filaDatosC[1]." ". $filaDatosC[2]
                                        ?>
                                        </label>
                                        </td>
                                        <td class="filaAzul10" colspan="2">
                                        <?php
                                                $consulta="select  relacion from 706_relacionSocio where idRelacionSocio='".$arrDatosRep[1]."'";
                                                $relacion=$con->obtenerValor($consulta);
                                            ?>	
                                            <label>
                                            <?php
                                                echo $relacion;
                                            ?>
                                            </label>
                                        </td>
                                        <td align="left" class="filaAzul10">
                                            <label><?php echo $filaDatosC[10]?></label>
                                        </td>
                                        <td class="filaAzul10"><label><?php echo $filaDatosC[9]?></label></td>
                                      </tr>
                              <?php
									}
								}
							  ?>
                              <tr height='23'>
                                <td width="16%" class="filaRosa10" >ACTIVIDAD PRINCIPAL</td>
                                <td colspan="6" class="filaAzul10" >
                                <label>
                                <?php echo $filaCl[31] ?>
                                </label>
                                </td>
                              </tr>
                              <tr height='23'>
                                <td width="16%" class="filaRosa10">ALTA EN HACIENDA</td>
                                <td width="4%" class="filaAzul10">
                                <label><?php 
								if($filaCl[3]!="")
									echo date('d/m/Y',strtotime($filaCl[3]))
									?>
                                </label></td>
                                
                                <td colspan="2" class="filaRosa10">No. EMPLEADOS</td>
                                <td width="18%"  align="left" class="filaAzul10"><label><?php echo number_format($filaCl[11],0,".",",")?></label></td>
                                <td width="19%" align="left" class="filaRosa10">Nom. Mensual</td>
                                <td  align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[12],2,".",",")?></label></td>
                                </tr>
                              <tr height='23'>
                                <td class="filaRosa10">PORTAL WEB</td>
                                <td colspan="6" class="filaRosa10">
                                  <label><?php echo $filaCl[10]?></label>                                </td>
                                </tr>
                              </table>

                            <table width="100%">
                              
                              
                              <tr height='23'>
                                <td colspan="16" class="filaBlanca10">DOMICILIO FISCAL</td>
                                </tr>
                              <tr height='23'>
                                <td width="70" class="filaRosa10">Calle:</td>
                                <td colspan="5" align="left" class="filaAzul10"><label><?php echo $filaCl[5]?></label></td>
                                <td width="38" class="filaRosa10">Num.</td>
                                <td width="42" align="left" class="filaAzul10"><label><?php echo $filaCl[13]?></label></td>
                                <td width="36" class="filaRosa10">C.P.</td>
                                <td width="32" align="left" class="filaAzul10"><label><?php echo $filaCl[14]?></label></td>
                                <td width="56" class="filaRosa10" class="filaAzul10">Colonia:</td>
                                <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[6]?></label></td>
                                <td width="52" class="filaRosa10">Ciudad:</td>
                                <td width="104" align="left" class="filaAzul10"><label><?php echo $filaCl[7]?></label></td>
                              </tr>
                              <tr height='23'>
                                <td class="filaRosa10">Propiedad:</td>
                                <td width="64" class="filaAzul10">
                                <label>
                                <?php
									$idPropiedad="-1";
									if($filaCl[32]!="")
										$idPropiedad=$filaCl[32];
									$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$idPropiedad;
									$categoria=$con->obtenerValor($consulta);
									echo $categoria;
								?>
                                
                                </label></td>
                                <td colspan="5" class="filaRosa10">Tiempo de Residencia:</td>
                                <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[20]?></label></td>
                               
                                <td colspan="4" class="filaRosa10">Renta mensual: </td>
                                <td colspan="2" align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[28],2,".",",")?></label></td>
                                </tr>
                              <tr height='23'>
                                <td colspan="2" class="filaRosa10">Valor Declarado:</td>
                                <td colspan="4" align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[22],2,".",",")?></label></td>
                                
                                <td colspan="4" class="filaRosa10">Superficie Aprox.:</td>
                                <td colspan="5" align="left" class="filaAzul10"><label><?php echo $filaCl[24]?></label></td>
                              </tr>
                            </table>
                              <table cellspacing="2" cellpadding="2" width="100%">
                                <tr height='23'>
                                  <td colspan="16" class="filaBlanca10">DOMICILIO PARTICULAR</td>
                                </tr>
                                <tr height='23'>
                                  <td width="9%" class="filaRosa10">Calle:</td>
                                  <td colspan="5" align="left" class="filaAzul10"><label><?php echo $filaCl[15]?></label></td>
                                  <td width="13%" class="filaRosa10">Num.</td>
                                  <td width="6%" align="left" class="filaAzul10" ><label><?php echo $filaCl[18]?></label></td>
                                  <td width="5%" class="filaRosa10">C.P.</td>
                                  <td width="5%" align="left" class="filaAzul10"><label><?php echo $filaCl[19]?></label></td>
                                  <td width="7%" class="filaRosa10">Colonia:</td>
                                  <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[16]?></label></td>
                                  <td width="7%" class="filaRosa10">Ciudad:</td>
                                  <td width="11%" align="left" class="filaAzul10"><label><?php echo $filaCl[17]?></label></td>
                                </tr>
                                <tr height='23'>
                                  <td class="filaRosa10">Propiedad:</td>
                                  <td width="8%" class="filaAzul10">
                                  <label>
                                <?php
									$idTipoPropiedad="-1";
									if($filaCl[27]!="")
										$idTipoPropiedad=$filaCl[27];
									$consulta="select tipo from 705_tipoResidencia where idTipoResidencia=".$idTipoPropiedad;
									$categoria=$con->obtenerValor($consulta);
									echo $categoria;
								?>
                                
                                </label>
                                  
                                  </td>
                                  <td colspan="5" class="filaRosa10">Tiempo de Residencia:</td>
                                  <td colspan="2" align="left" class="filaAzul10"><label><?php echo $filaCl[21]?></label></td>
                                  
                                  <td colspan="4" class="filaRosa10">Renta mensual: </td>
                                  <td colspan="2" align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[29],2,".",",")?></label></td>
                                </tr>
                                <tr height='23'>
                                  <td colspan="2" class="filaRosa10">Valor Declarado:</td>
                                  <td colspan="4"align="left" class="filaAzul10"><label>$ <?php echo number_format($filaCl[23],2,".",",")?></label></td>
                                
                                  <td colspan="5" class="filaRosa10">Superficie Aprox.:</td>
                                  <td colspan="5" align="left" class="filaAzul10"><label><?php echo $filaCl[25]?></label></td>
                                </tr>
                                <tr>
                                	<td colspan="16" class="filaBlanca10" colspan="2">
                                    PRINCIPALES CLIENTES&nbsp;<?php if($secGral){?>
                                    <a href="javascript:mostrarVentanaAgregarCliente()"><img src='../images/add.png' title="Agregar principal cliente" alt="Agregar principal cliente" />
                                    </a><?php }?>
                                    </td>
                                </tr>
                                <TR>
                                	<td colspan="16">
                                    <table  style="width:100% !important" >
                                    
                                    <tbody >
                                    <?php
										$consulta="select idPrincipalesClientes,e.empresa from 715_principalesClientes pc,700_empresas e where e.idEmpresa=pc.idClientePrincipal and pc.idCredito=".$idCredito;
										
										$resCli=$con->obtenerFilas($consulta);
										while($filaCli=mysql_fetch_row($resCli))
										{
									?>
                                    <tr id='fila_<?php echo $filaCli[0]?>'>
                                    	<td class="filaAzul10" width="30" align="center">
										<?php 
										if($secGral)
										{?>
                                        	<a href='javascript:removerClienteP("<?php echo base64_encode($filaCli[0])?>")'><img src='../images/delete.png' title="Remover cliente" alt="Remover cliente" /></a>
										<?php 
										} 
										?></td><td class="filaAzul10"><?php echo $filaCli[1]?>
                                        </td>
                                        
                                    </tr>
                                    <?php
										}
									?>
                                    </td>
                                </TR>
                              </table>
                              
                        <?php
							}
							?>		
                           <form method="post" action="creditosClientes.php" id='frmEnvioActualiza'> 
                                <input type="hidden" id="idCredito" name="idCredito" value="<?php echo $idCredito?>" />
                                <input type="hidden" id="cPagina" name="cPagina" value="sFrm=true" />
                         	</form>
                         	<table width="100%" >
                            <tr height='21'>
                                  <td  colspan="5" class="filaBlanca10">MONTOS</td>
                                  
                                  
                            </tr>
                            <tr height="21">
                             	<td width="130" class="filaRosa10" >Monto Solicitado:</td>
                                <td colspan="2" width="130" class="filaAzul10">
                                 <?php
									$valor=number_format($filaCred[8],2,'.',',');
									if($secMontos)
									{
								?>
                                
                                <input type="text" value="$ <?php echo $valor?>" name="montoSol" id="montoSol" size="13" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('4')?>')" />
                                <?php
									}
									else
									{
								?>
                                <label>$ <?php echo $valor?></label>
                                <?php
									}
								?>
                                </td>
                                
                                <td align="left" width="170" class="filaRosa10" >
                                 Monto sugerido por RC:
	                            </td>
                                <td class="filaAzul10" >
                                 <?php
									$valor=number_format($filaCred[9],2,'.',',');
									if($secMontos)
									{
								
									
								?>
                                <input type="text" value="$ <?php echo $valor?>" name="montoSug" id="montoSug" size="13" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('5')?>')"/>
                                <?php
									}
									else
									{
								?>
                                <label>$ <?php echo $valor?></label>
                                <?php
									}
								?>
                             	</td>
                                
                             </tr>
                            <tr>
                             	<td colspan="5">
                                	<table width="100%">
                                    <tr>
                                        <td valign="top" width="130" class="filaRosa10">
                                        Destino crédito:
                                        
                                        </td>
                                        <td colspan="3" class="filaAzul10">
                                        <?php
                                        if($secMontos)
										{
										?>
									
                                        <textarea cols="90" rows="3" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('6')?>')"><?php echo $filaCred[22] ?></textarea>
                                         <?php
											}
											else
											{
										?>
										<label><?php echo $filaCred[22] ?></label>
										<?php
											}
										?>
                                        
                                        </td>
                                   </tr>
                                   </table>
                               </td>
                             </tr>
							</table>		
                           	<table width="100%">
                           		<tr height='21'>
                                  	<td  colspan="5" class="filaBlanca10">SALDOS GENERAL</td>
                                </tr>

                            	<tr>
		                            <td align="left">
                                        <span id="tblCuentas">
                                        </span>
                                        <span class="filaRosa10" >Saldo promedio semestral:
                                        </span>&nbsp;<span class="filaAzul10" > <span class="letraRoja" id='lblSaldoPromedio'>$ 0.00</span></span><br />
                            		</td>
                                </tr>
                            	<tr height='21'>
                                  	<td  colspan="5" class="filaBlanca10">DEP&Oacute;SITOS</td>
                                </tr>
                            	<tr>
                            		<td align="left">
                            
                                		<span id="tblCuentasDeposito"></span>
                                		<span align="right" class="filaRosa10" >Saldo promedio semestral:
	                            		</span>&nbsp;
                                        <span class="filaAzul10"> <span class="letraRoja" id='lblDepositoPromedio'>$ 0.00</span></span>
                             			<table width="100%">
                             			<tr height='21'>
                                  			<td  colspan="5" class="filaBlanca10" width="60%">
                                  				Meses con mayores ventas en el año: <?php if($secMontos){?><a href='javascript:mostrarCatalogoMeses()'><img src="../images/add.png" alt='Modificar meses con mayores ventas en el año' title='Modificar meses con mayores ventas en el año' /></a><?php }?>
                                            &nbsp;
                                            </td>
                                			<td class="filaAzul10" width="40%">
                                                <label id='lblMesesVentas'>
                                                <?php
                                                    if($filaCred[19]!="")
                                                    {
                                                        $consulta="select opcion from 951_catalogoOpcionesVarios where valorOpcion in (".$filaCred[19].") and tipoOpcion=4 and idIdioma=".$_SESSION["leng"]." ";
                                                        $listaMeses=$con->obtenerListaValores($consulta);
                                                        echo $listaMeses;
                                                    }
                                                ?>
                                                
                                                
                                                </label>
                                            &nbsp;
                                              </td>
                                		</tr>
			                             <tr height="21">
                             				<td class="filaRosa10">
                                				Ventas acumuladas año anterior:
                                			</td>
                                			<td class="filaAzul10" style="width:150px"><?php
												$valor=number_format($filaCred[20],2,'.',',');
												if($secMontos)
												{
											
												?>
												<input type="text" value="$ <?php echo $valor?>" name="ventasAcumuladasAnt" id="ventasAcumuladasAnt" size="13" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('2')?>')" />
												 <?php
													}
													else
													{
												?>
												<label style="width:120px">$ <?php echo $valor?></label>
												<?php
													}
												?>
											</td>
                                            <td class="filaRosa10"  >
                                             Ventas acumuladas año actual:
                                            
                                            </td>
                                            <td class="filaAzul10" style="width:150px"> <?php
                                                $valor=number_format($filaCred[21],2,'.',',');
                                                if($secMontos)
                                                {
                                            
                                            ?>
                                            <input type="text" value="$ <?php echo $valor?>" name="ventasAcumuladasAct" id="ventasAcumuladasAct" size="13" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('3')?>')"/>
                                             <?php
                                                }
                                                else
                                                {
                                            ?>
                                            <label>$ <?php echo $valor?></label>
                                            <?php
                                                }
                                            ?>
                                            </td>
                             			</tr>
                             			</table>   
                                        <table width="100%">
                                        <tr height="21">
                                            <td align="left" class="filaBlanca10">
                                            AVALES&nbsp;&nbsp;<?php if($secAvales){?><a href="javascript:agregarAval()"><img src="../images/add.png" title="Agregar aval" alt="Agregar aval" /></a><?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $consulta="select * from 709_aval where idCredito=".$idCredito;
                                        
                                        $resAvales=$con->obtenerFilas($consulta);
                                        $ct=1;
                                        while($filaAval=mysql_fetch_row($resAvales))
                                        {
                                        ?>
                                        <tr>
                                            <td>
                                            <?php
                                                generarTablaAval($filaAval,$ct,$secAvales);
                                                $ct++;
                                            ?>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                        </table>
                                        <table width="100%">
                                        <tr height="21">
                                            <td align="left" class="filaBlanca10">
                                            BIENES MUEBLES</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <span id="tblBienMueble"></span>
                                            </td>
                                        </tr>
                                        </table>
                            		</td>
                            	</tr>
                            	</table>   
                         	<table >
                            <tr>
                            	<td  class="filaRosa10" valign="top" width="250">Condicionado a:</td>
                                <td valign="top" class="filaAzul10" width="643" class="">
                                <?php
									 if($secInteres)
									{
								?>
                                	<textarea style="height:150px; width:650px" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('8')?>')"><?php echo $filaCred[18]?></textarea>
                                <?php
									}
									else
									{
										echo "<label>".$filaCred[18]."</label>";
									}
								?>
                                </td>
                            </tr>
                            </table>
                            
                            <table width="850">
                            <tr height="21">
                                <td width="200" class="filaBlanca10" align="center">ACUERDO DEL COMITÉ</td>
                                <td width="100" class="filaBlanca10" align="center">FECHA ENTRADA</td>
                                <td width="100" class="filaBlanca10" align="center">FECHA COMITÉ</td>
                                <td width="100" class="filaBlanca10" align="center">FECHA FONDEO</td>
                                <td width="300" class="filaBlanca10" align="center">PROMOTOR</td>
                                
                            </tr>
                            <tr height="21">
                            	<td class="filaAzul10" align="center">
                                <?php
                                 if($acComite)
								{
								?>
                                	 <select id="cmbAcuerdoComite" onChange="actualizarCampoCredito(this,'<?php echo base64_encode('9')?>')">
                                	<option value="-1">Seleccione</option>
                                	<?php
										$consulta="select idAcuerdoComite,acuerdo from 753_acuerdosComite order by acuerdo";
										$con->generarOpcionesSelect($consulta,$filaCred[15]);
									?>
                                    </select>
                                <?php
								}
								else
								{
									$idAcuerdo=$filaCred[15];
									if($idAcuerdo=="")
										$idAcuerdo="-1";
									$consulta="select acuerdo from 753_acuerdosComite where idAcuerdoComite=".$idAcuerdo;
									echo "<label>".$con->obtenerValor($consulta)."</label>";
								}
								?>
                                </td>
                                <td class="filaAzul10" align="left">
                                	<?php
									if($fechaEntrada)
									{
								?>
                                	<span id='dteFechaEntrada'></span>
                                    <input type="hidden" value="<?php echo $filaCred[4]?>" id='hFechaEntrada' />
                                <?php
									}
									else
									{
										echo "<label>";
										echo date("d/m/Y",strtotime($filaCred[4]));
										echo "</label>";
									}
									?>
                                </td>
                                <td class="filaAzul10" align="left">
                                <?php
									if($fechaComite)
									{
								?>
                                	<span id='dteFechaComite'></span>
                                    <input type="hidden" value="<?php echo $filaCred[5]?>" id='hFechaComite' />
                                <?php
									}
									else
									{
										echo "<label>";
										if($filaCred[5]!="")
											echo date("d/m/Y",strtotime($filaCred[5]));
										echo "</label>";
									}
									?>
                                    
                                    
                                </td>
                                <td class="filaAzul10" align="left">
                                <?php
									if($fechaFondeo)
									{
								?>
                                	<span id='dteFechaFondeo'></span>
                                    <input type="hidden" value="<?php echo $filaCred[6]?>" id='hFechaFondeo' />
                                <?php
									}
									else
									{
										echo "<label>";
										if($filaCred[6]!="")
											echo date("d/m/Y",strtotime($filaCred[6]));
										echo "</label>";
									}
									?>
                                </td>
                                <td class="filaAzul10" align="left">
                                	<label>
                                    <?php
										echo obtenerNombreUSuario($filaCred[3]);
									?>	
                                    </label>
                                </td>
                                
                                
                            </tr>
                            </table>
                            <table width="850">
                            <tr height="21">
                               <td width="250" class="filaBlanca10" align="center">GERENTE DE GRUPO</td>
                               <td width="250" class="filaBlanca10" align="center">CAPTACIÓN DE CLIENTE VIA</td>
                               <td width="150" class="filaBlanca10" align="center">MONTO SOLICITADO</td>
                               <td width="150" class="filaBlanca10" align="center">MONTO AUTORIZADO</td>
                            </tr>
                            <tr height="21">
                            	<td class="filaAzul10" align="left">
                                <label>
                                <?php
                                	$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$filaCred[3]." and codigoRol like '38_%'";
									$rol=$con->obtenerValor($consulta);
									if($rol!="")
									{
										$datosRol=explode("_",$rol);
										$consulta="select idUsuario from 807_usuariosVSRoles where codigoRol='39_".$datosRol[1]."'";
										$idGerente=$con->obtenerValor($consulta);
										if($idGerente!="")
											echo obtenerNombreUsuario($idGerente);
										
									}
									
								?>
                                </label>	
                                </td>
                                <td class="filaAzul10" align="center">
                               
                                
                                <?php
									echo $_SERVER["SERVER_ADDR"];
									$idCaptacion=$filaCred[7];
									$mostrarFilaExpo='none';
									$nomExpo="";
									if($idCaptacion==12)
									{
										$mostrarFilaExpo='';
										$consulta="select nomExpo from 750_creditos where idCredito=".$idCredito;
										$nomExpo=$con->obtenerValor($consulta);
									}
                                  if($secGral)
								  {
								?>
                                  <select id='cmbCaptacion' onChange="actualizarCampoCredito(this,'<?php echo base64_encode('7')?>')">
                                  <option value="-1">Seleccione</option>
                                  <?php
								  
								  
								  
								  	$consulta="select idCaptacion,via from 716_captacionVia order by via";
									$con->generarOpcionesSelect($consulta,$filaCred[7]);
								  ?>
                                  </select>
                                  <?php
									}
									else
									{
										$consulta="select via from 716_captacionVia where idCaptacion=".$filaCred[7];
										$via=$con->obtenerValor($consulta);
									?>
                                    <label>
                                    <?php
										echo $via;
									?>
                                    </label>
                                    <?php
									}
								  ?>	
                                </td>
                                <td class="filaAzul10" align="left">
                               	<?php
									$valor=number_format($filaCred[8],2,'.',',');
								?>
                                 <label>$ <?php echo $valor?></label>
                                </td>
                                <td class="filaAzul10" align="left">
                                	<?php
                                    $valor=number_format($filaCred[9],2,'.',',');
									if($secInteres)
									{
                                    ?>
                                	<input type="text" width="30" value="<?php echo $valor?>" id="txtMontoAut" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('10')?>')"/>
                                    <?php
									}
									else
									{
										echo "<label>".$valor."</label>";
									}
									?>
                                </td>
                            </tr>
                            <tr id='filaExpo' style="display:<?php echo $mostrarFilaExpo?>">
                            	<td colspan="4">
                                	<table >
                                	<tr height="24">
                                        <td width="150" class="filaRosa10">
                                            Ingrese expo:
                                        </td>
                                        <td colspan="3" class="filaAzul10" width="700">
                                        <?php 
											if($secGral)
											{
										?>
                                            <input type="text" id='txtExpo' value="<?php echo $nomExpo?>" size="90" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('20')?>')"/>
                                        <?php
											}
											else
											{
										?>
                                        	<label>
                                            <?php
												echo $nomExpo;
											?>
                                            <label>
                                        <?php	
											}
										?>
                                        
                                        </td>
                                    </tr>
                                    </table>
                                </td>
                            </tr>
                            
                            </table>
                            <table width="850">
                            <tr>
                                <td width="200" class="filaBlanca10" align="center">FECHA RENOVACIÓN LÍNEA</td>
                                <td width="250" class="filaBlanca10" align="center">CERRADOR RC CORPORATIVO</td>
                                <td width="250" class="filaBlanca10" align="center">CERRADOR FONDO</td>
                                <td width="100" class="filaBlanca10" align="center">TASA INTERÉS</td>
                            </tr>
                            <tr height="21">
                            	<td class="filaAzul10" align="left">
                                <?php
                                	if($secInteres)
									{
                                ?>
                                	<span id='dteFechaRenovLinea'></span>
                                    <input type="hidden" value="<?php echo $filaCred[10]?>" id='hFechaRen' />
                               <?php
									}
									else
									{
							?>
                            		<label>
                                    	<?php
											echo date("d/m/Y",strtotime($filaCred[10]));
										?>
                                    </label>
                            <?php
										
									}
                               ?>
                                </td>
                                <td class="filaAzul10" align="left">
                                <label id='cerradorRC'>
                                <?php
									$idCerrador=$filaCred[11];
									if($idCerrador!="")
										echo obtenerNombreUsuario($idCerrador);
									
								?>
                                </label>
                                <?php 
									if($cerradorRC)
									{
										echo "&nbsp;<a href='javascript:modificarCerrador(\"".bE('1')."\")'><img src='../images/pencil.png' alt='Modificar cerrador' title='Modificar cerrador'></a>";
									}
								?>	
                                </td>
                                <td class="filaAzul10" align="left">
                                 <label id="cerradorFondo">
                                <?php
									$idCerradorFondo=$filaCred[12];
									if($idCerradorFondo!="")
										echo obtenerNombreUsuario($idCerradorFondo);
								?>
                                </label>	
                                <?php 
									if($cerradorFondo)
									{
										echo "&nbsp;<a href='javascript:modificarCerrador(\"".bE('2')."\")'><img src='../images/pencil.png' alt='Modificar cerrador' title='Modificar cerrador'></a>";
									}
								?>	
                                </td>
                                <td class="filaAzul10" align="left">
                                	<?php
                                    $valor=$filaCred[13];
                                    
                                    if($secInteres)
									{
                                    ?>
                                	<input type="text" width="10" value="<?php echo $valor?>" id="txtInteres" onKeyPress="return soloNumero(event,true,false,this)" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('11')?>')"/>
                                    <?php
									}
									else
									{
										echo "<label>".$valor."</label>";
									}
									?>
                                	
                                </td>
                            </tr>
                            </table>
                            <table width="850">
                            <tr>
                                    <td width="200" class="filaBlanca10" align="center">% COMISIÓN</td>
                                    <td  class="filaBlanca10" align="center" colspan="2"></td>
                                    
                            </tr>
                            <tr height="21">
                            	<td class="filaAzul10" align="left">
                                	<?php
                                    $valor=$filaCred[14];
									 if($secInteres)
									{
                                    ?>
                                	<input type="text" width="10" value="<?php echo $valor?>" id="txtComision" onKeyPress="return soloNumero(event,true,false,this)" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('12')?>')"/>
                                    <?php
									}
									else
									{
										echo "<label>".$valor."</label>";
									}
									
                                    ?>
                                	
                                </td>
                                <td class="filaAzul10" align="left" colspan="2">
                                	
                                </td>
                                
                            </tr>
                            </table>	
                                 
                            </td>
                         </tr>
                         </table>  
                         <table >
                            <tr>
                            	<td  class="filaRosa10" valign="top" width="185">Conferencia Capital Humano:</td>
                                <td valign="top" class="filaAzul10" width="643">
                                <?php
									 if($secInteres)
									{
								?>
                                	<textarea style="height:70px; width:650px" onBlur="actualizarCampoCredito(this,'<?php echo base64_encode('13')?>')"><?php echo $filaCred[16]?></textarea>
                                <?php
									}
									else
									{
										echo "<label>".$filaCred[16]."</label>";
									}
								?>
                                </td>
                            </tr>
                         </table> 
                         <table> 
                         <tr height="21">
                            <td  class="filaRosa10" valign="top" width="185">Status de la operación:</td>
                            <td valign="top" class="filaAzul10">&nbsp;
                                 <label>
                                <?php
                                    $consulta="select estadoCredito from 752_estadosCredito where numEstado=".$filaCred[17];
                                    echo $con->obtenerValor($consulta);
                                ?>
                                </label>
                            </td>
                        </tr>
                         </table>
                         <input type="hidden" name="tipoCliente" id="tipoCliente" value="<?php echo $tipoCliente?>" />
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
