<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>


<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>
<?php 
$procesoName="";
$mostrarRegresar=true;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=true;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$paramGET=true;
	$mostrarMenuIzq=false;
	$guardarConfSession=true;
?>
<script type="text/javascript" src="../Scripts/thickbox/jquery.js"></script>
<script type="text/javascript" src="../Scripts/masks.js"></script>
<link rel="stylesheet" href="../Scripts/thickbox/thickboxExt.css" type="text/css" />
<script type="text/javascript" src="../Scripts/thickbox/thickbox.js"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<script type="text/javascript" src="../Scripts/base64.js"></script>


<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}

$ctParams=sizeof($arrLlaves);

$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
			
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}





?>


<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
<!--
#centro {
	text-align: right;
}
-->
</style>
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		gE('frmRegresar').submit();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>

<?php
	
?>

</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
		<div class="links_hayas">
		<ul class="tabs_hayas">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=2";
					$res=$con->obtenerFilas($consulta);
					while($filaM=mysql_fetch_row($res))
					{
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion" ;
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<li><span><a href="'.$fila[1].'">'.$fila[0].'</a></span></li>';
						}
					}
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
			<?php
				if($mostrarMenuNivel2)
				{
					$consulta="select distinct(pO.idOpcion) from 813_paginasVSOpciones pO,
							810_paginas p,812_permisosOpcionesMenus pM where pO.idPagina=p.idPagina 
							and pM.idOpcion=pO.idOpcion and pM.idRol in(".$_SESSION["idRol"].") and p.pagina='".$nomPagina."' and pO.posicion=1";
					$res=$con->obtenerFilas($consulta);
		
					while($filaM=mysql_fetch_row($res))
					{
		
						$consulta2="select distinct(oP.textoOpcion),oP.paginaUrlDestino from 809_opciones oP,811_menusVSOpciones mO where
									 mO.idOpcion=oP.idOpcion and idIdioma=".$_SESSION["leng"]." and mO.idMenu=".$filaM[0]." order by oP.textoOpcion";
						$opcionesNivel1= $con->obtenerFilas($consulta2);
						while($fila=mysql_fetch_row($opcionesNivel1))
						{
							echo '<a href="'.$fila[1].'">'.$fila[0].'</a>';
						}
					}
				}
			
			?>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					
					<table border="0" width="<?php echo $tamColumIzq?>" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td>
                                  
                                  	<?php 
										if($mostrarUsuario)
										{
									?>
                                  
								  	<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table" <?php mostrarSiLog(); ?>>
                                      <tr>
                                        <td  class="box_body_l"></td>
                                        <td  style="width:100%;" class="box_body box_body_td">
										<table id="tblLog" border="0" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
											  <table width="100%">
											  <Tr>
											  <td align="center" width="200" >
											  <label id="lblUsr" class="letraVerde" ><?php echo $_SESSION["nombreUsr"] ?></label>
											  </td>
											  <td>
											  <a href="javascript:cerrarSesion();"><img src="../images/Exit.png" height="30" width="30" alt="<?php echo $et["cerrarSesion"] ?>" title="<?php echo $et["cerrarSesion"] ?>" /></a>
											  </td>
											  </Tr>
											  </table>
                                              </td>
                                            </tr>
                                            
                                        </table>
										</td>
                                        <td  class="box_body_r"></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td  style="width:100%;" class="box_body_b"></td>
                                        <td></td>
                                      </tr>
                                  </table>
                                  <?php
								  
										}
                                  ?>
                                  
								  </td>
                                </tr>
                            </table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
							  	<?php
									$consulta=" select distinct(tm.textoMenu),tm.idMenu 
												from 813_paginasVSOpciones pO, 808_titulosMenu tm,812_permisosOpcionesMenus oM,810_paginas pa 
												where pO.posicion=3 and oM.idOpcion=pO.idOpcion and pO.idPagina=pa.idPagina and 
												tm.idMenu=pO.idOpcion and tm.idIdioma=".$_SESSION["leng"]." and oM.tipo=1 and oM.idRol in(".$_SESSION["idRol"].") and pa.pagina='".$nomPagina."' order by tm.textoMenu";
									
									$opcionesNivel1= $con->obtenerFilas($consulta);
									while($fila=mysql_fetch_row($opcionesNivel1))
									{
										$cabecera=generarCabeceraMenu($fila[0]);
										$opciones="";
										$opciones=generarOpciones($fila[1]);
										$tabla='<table id="tblSistema" width="210px">
										<tr>
											<td>
												<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_heading2_table">
												<tr>'.$cabecera.									
												'</tr>
												</table>
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>
														<table border="0" width="100%" cellspacing="0" cellpadding="0"  class="box_body_table">
														<tr>
															<td  class="box_body_l"></td>
															<td  style="width:100%;" class="box_body box_body_td">
																<ul>'.$opciones.
																'</ul>
															</td>
															<td  class="box_body_r">
															</td>
														</tr>
														<tr>
															<td>
															</td>
															<td  style="width:100%;" class="box_body_b">
															</td>
															<td>
															</td>
														</tr>
														</table>
													</td>
												</tr>
												</table>
											</td>
										</tr>
										</table>';
										echo $tabla;
									}
									
								?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
								  	{
							?> 
                                        <table align="left" id="tblRegresar1">
                                        <tr>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
                                        </td>
                                        <td>
                                        <a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
                                        </td>
                                        </tr>
                                        </table>
                                        <br />
							<?php 
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
						<br />
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
//permisos roles
								$conRol="SELECT codigoRol FROM 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
									$varPromotor = false;
									$varRoot = false;
									$varAnalisis = false;
								$resUsr=$con->obtenerFilas($conRol);
								while($filaUsr=mysql_fetch_row($resUsr))
								{			
										$rol= obtenerTituloRol($filaUsr[0]);
										$arrRol=explode("_",$filaUsr[0]);
										switch($arrRol[0])
										{
											case 38:
												$varPromotor = true;
												
											break;
											case 1:
											case 42:
												$varRoot = true;
												
											break;
											case 41:
												$varAnalisis =true;
												
											break;
										}
								}
							?>
                        	<?php
								$idEmpresa="-1";
								if(isset($objParametros->idEmpresa))
									$idEmpresa=$objParametros->idEmpresa;
								$consulta="select * from 700_empresas where idEmpresa=".$idEmpresa;

								$fila=$con->obtenerPrimeraFila($consulta);
								$ejecutarExt=false;
								$funExtEjec="";
								if(isset($objParametros->ejecutarExt))
								{
									$ejecutarExt=true;
									$funExtEjec=$objParametros->ejecutarExt;
								}
								$asociarCredito=false;
								$idCredito="-1";
								if(isset($objParametros->idCredito))
								{
									$asociarCredito=true;
									$idCredito=$objParametros->idCredito;
								}
							?>
                            <script type="text/javascript" src="Scripts/catalogoEmpresas.js.php?idCliente=<?php echo base64_encode($idEmpresa)?>"></script>
                        	<table>
                            <tr>
                            <td align="left">
                        		<span id="tab"></span>
                           </td>
                           </tr>
                           </table>
                          <div id="tblGral" class="x-hide-display" align="left">
                           
                             <form method="post" action="../paginasFunciones/guardarDatos.php" id="frmEnvio">
                            <table cellspacing="0" cellpadding="0" width="890" >
                              <tr>
                                
                                <td class="filaRosa10">RFC: <a href='javascript:validarRFC()'><img src='../images/icon_big_tick.gif' title='Validar RFC' alt='Validar RFC' /></a></td>
                                <td colspan="4" class="filaRosa10">EMPRESA </td>
                                <td class="filaRosa10">Telefonos:</td>
                              </tr>
                              <tr>
                                
                                <td class="filaAzul10" width="300">
                                <input name="_rfcvch" type="text" class="camp_form" id="_rfcvch" value="<?php echo $fila[3]?>" size="4" maxlength="3" val="obl" campo="RFC ABREVIACION"  />-<input name="_RFC2vch" type="text" class="camp_form" id="_RFC2vch" value="<?php echo $fila[42]?>" size="6" maxlength="6" val="obl" campo="RFC FECHA"  />-<input name="_RFC3vch" type="text" class="camp_form" id="_RFC3vch" value="<?php echo $fila[43]?>" size="3" val="obl" maxlength="3" campo="RFC HOMOCLAVE"  />
                                </td>
                                <td colspan="4" class="filaAzul10"><input name="_empresavch" type="text" class="camp_form" id="_empresavch" value="<?php echo $fila[1]?>" size="70" val="obl" campo="Empresa" /></td>
                                <td class="filaAzul10"><input name="_telefonosvch" type="text" class="camp_form" id="_telefonosvch" value="<?php echo $fila[9]?>" size="30" val="obl" campo="Telefonos" /></td>
                              </tr>
                             
                              <tr>
                              	<td colspan="10">
                                	<table id="tblRepresentantes" width="100%">
                                    <tbody>
                                     <tr>
                                        <td colspan="2" class="filaRosa10">REPRESENTANTE LEGAL:</td>
                                        <td class="filaRosa10">
                                        <?php
                                          $idClienteRep="-1";
                                           if($fila[35]!="")
                                                $idClienteRep=$fila[35];
                                      ?>
                                        <a href="javascript:mostrarVentanaAgregarRep()">
                                        <img src="../images/add.png" border=0 title='Agregar representante legal' alt='Agregar representante legal'/>
                                        </a>
                                        
                                        </td>
                                        <td class="filaRosa10">RELACION CON LA EMPRESA</td>
                                        <td class="filaRosa10">CORREO ELECTRONICO</td>
                                        <td class="filaRosa10">Nextel, Celular:
                                        <input type="hidden" name="_idRepresentanteLegalvch" id="_idRepresentanteLegalvch" value="<?php echo $fila[35]?>" /> 
                                         </td>     
                                      </tr>
								  <?php
                                  if($fila[35]!="")
                                  {
                                      $arrRepresentantes=explode(",",$fila[35]);	
                                      foreach($arrRepresentantes as $representante)
                                      {
                                          $datosRep=explode("_",$representante);
                                  ?>
                                          <tr id='filaRep_<?php echo $datosRep[0]?>'>
                                            <td colspan="3" class="filaAzul10">
                                            <table width="100%" >
                                            <?php
                                                
                                                        $consulta="select * from 703_clientes where idCliente=".$datosRep[0];
                                                        $filaDatosC=$con->obtenerPrimeraFila($consulta);
                                                        $nRepresentante=$filaDatosC[5]." ".$filaDatosC[1]." ". $filaDatosC[2];
                                                        echo "	<tr >
                                                                    <td width='80%'>
                                                                        <label id='lblRepresentante_".$datosRep[0]."'>".$nRepresentante."</label>
                                                                    </td>
                                                                    <td width='20%'>
                                                                        <a href='javascript:modificarRepresentante(\"".bE($datosRep[0])."\")'><img src='../images/pencil.png' border=0 alt='Modificar datos del representante' title='Modificar datos del representante'/></a>&nbsp;<a href='javascript:removerRepresentante(\"".bE($datosRep[0])."\")'><img src='../images/delete.png' border=0 alt='Remover representante' title='Remover representante'/></a>
                                                                    </td>
                                                                </tr>"	;
                                                    
                                            ?>
                                            
                                            </table>
                                            </td>
                                            <td class="filaAzul10">
                                            <select name='relacionesRepresentante' class="camp_form"  id="relacionRep_<?php echo $datosRep[0]?>" >
                  <?php
                                                    $consulta="select idRelacionSocio, relacion from 706_relacionSocio where idRelacionSocio in(2,5,8)";
                                                    $idRelacion=$con->obtenerValor($consulta);
                                                    $con->generarOpcionesSelect($consulta,$datosRep[1]);
                                                ?>
                                            </select>
                                            
                                            </td>
                                            <td class="filaAzul10">
                                                <label id="lblEmail_<?php echo $datosRep[0]?>"><?php echo $filaDatosC[10]?></label>
                                            </td>
                                            <td class="filaAzul10"><label id="lblCel_<?php echo $datosRep[0]?>"><?php echo $filaDatosC[9]?></label></td>
                                          </tr>
                                 <?php
                                      }
                                 }
                                 ?>
                                 	</tbody>
                                 	</table>
                             	</td>
                              </tr>
                               <tr>
                              	<td colspan="6" class="filaBlanca10">
                                	SOCIOS
                                </td>
                              </tr>
                              <tr>
                              	<td colspan="6" class="filaAzul10">
                                	
                                	<span id="tblSocios"></span>
                                    <br />
                                </td>
                              </tr>
                              <tr>
                                <td class="filaRosa10" >ACTIVIDAD PRINCIPAL</td>
                                <td colspan="5" class="filaAzul10" >
                                <input type="text" name="_actividadPrincipalvch" value="<?php echo $fila[14]?>" size="120" maxlength="250" />
								</td>
                              </tr>
                       
                          <tr>
                            <td class="filaRosa10">EMPRESA FAMILIAR:</td>
                            <td class="filaAzul10" ><select name="_familiarint" class="camp_form" >
                              <?php
								  $consulta="select valor,texto from 1004_siNo where idIdioma=".$_SESSION["leng"];
								  $con->generarOpcionesSelect($consulta,$fila[5]);
							  ?>
                            </select></td>
                            <td class="filaRosa10">No. empleados:</td>
                            <td class="filaAzul10" ><input name="_noEmpleadosint" type="text" class="camp_form" id="_noEmpleadosint" value="<?php echo $fila[12]?>" size="10"  campo="No. Empleados" onkeypress="return soloNumero(event,false,false,this)" /></td>
                            <td class="filaRosa10">Nom. Mensual</td>
                            <td class="filaAzul10"><input name="_nominaMensualflo" type="text" class="camp_form" id="_nominaMensualflo" value="<?php echo $fila[13]?>" size="10" campo="Nómina mensual" /></td>
                           </tr>
                          <tr>
                            <td class="filaRosa10">ALTA EN HACIENDA:</td>
                                <td class="filaAzul10" >
                                <span id="sp_fechaAltaSatdte"></span>
                                  
                                  <input name="_fechaAltaSatdte" type="hidden" id="_fechaAltaSatdte" value="<?php if($fila[4]=="") { echo date("d/m/Y");} else {echo date("d/m/Y",strtotime($fila[4]));}?>" val="obl" campo="Fecha Alta SAT" extid="f_sp_fechaAltaSatdte"  />
                                </td>
                            <td class="filaRosa10">WEB:</td>
                                <td colspan="3" class="filaAzul10" ><input name="_webvch" type="text" class="camp_form" id="_webvch" value="<?php echo $fila[11]?>" size="60"  campo="WEB" /></td>
                          </tr>
    
         </table>
                              
                              <table cellspacing="2" cellpadding="2" width="870">

  	<tr height='23'>
    	<td colspan="12" class="filaBlanca10">DOMICILIO FISCAL</td>
    </tr>
  	<tr height='23'>
    	<td width="70" class="filaRosa10">Calle:</td>
    	<td class="filaAzul10"><input name="_direccionfvch" type="text" class="camp_form" id="_direccionfvch" value="<?php echo $fila[6]?>" size="25" val="obl" campo="Direcci&oacute;n" /></td>
    	<td class="filaRosa10">Num.</td>
    	<td class="filaAzul10"><input name="_numfvch" type="text" class="camp_form" id="_numfvch" value="<?php echo $fila[18]?>" size="8" val="obl" campo="N&uacute;mero dom. fiscal" /></td>
    	<td class="filaRosa10">C.P.</td>
	    <td class="filaAzul10"><input name="_cpfint" type="text" class="camp_form" onkeypress="return soloNumero(event,false,false,this)" id="_cpfint" value="<?php echo $fila[19]?>" size="6" maxlength="6" val="obl" campo="C.P." /></td>
    	<td class="filaRosa10">Col.</td>
    	<td class="filaAzul10"><input name="_colfvch" type="text" class="camp_form" id="_colfvch" value="<?php echo $fila[7]?>" size="15" val="obl" campo="Colonia" /></td>
    	<td class="filaRosa10">Del.</td>
    	<td class="filaAzul10"><input name="_delfvch" type="text" class="camp_form" id="_delfvch" value="<?php echo $fila[38]?>" size="15" val="obl" campo="Delegaci&oacute;n" /></td>
    	<td class="filaRosa10">Cd.</td>
    	<td class="filaAzul10"><input name="_ciudadfvch" type="text" class="camp_form" id="_ciudadfvch" value="<?php echo $fila[8]?>" size="15" val="obl" campo="Ciudad" /></td>
  	</tr>
  	<tr height='23'>
    	<td class="filaRosa10">Propiedad</td>
    	<td class="filaAzul10">
        	<select name="_tipoResidenciafint" class="camp_form" onchange="cambiaResidencia(this)"  val='obl' campo='Tipo residencia fiscal'>
            <option value="-1">Seleccione</option>
      		 <?php
										$consulta="select idTipoResidencia,tipo from 705_tipoResidencia ";
										$con->generarOpcionesSelect($consulta,$fila[31]);
										$compRenta="";
										$propiaObl="";
										$rentadaObl="";
										if($fila[31]=="1") //propia
										{
											$compRenta="display:none";
											$propiaObl="obl";
											
											
										}
										else
										{
											$compRenta2="display:none";
											$rentadaObl="obl";
										}
									?>
    </select></td>
        <td colspan="3" class="filaRosa10">Tiempo de Residencia:</td>
        <td class="filaAzul10"><input name="_tiempoResidenciafvch" type="text" class="camp_form" id="_tiempoResidenciafint" value="<?php echo $fila[25]?>" size="6" maxlength="30" val="obl" campo="Tiempo de residencia" /></td>
        <td colspan="3" class="filaRosa10">Renta mensual:</td>
        <td colspan="3" class="filaAzul10"><input name="_rentaResidenciafflo" style="<?php echo $compRenta?>" type="text" class="camp_form" id="_rentaResidenciafflo" value="<?php echo $fila[33]?>" size="15" val="<?php echo $rentaObl?>" campo="Renta residencia fiscal" /></td>
	</tr>
	<tr height='23'>
    	<td colspan="2" class="filaRosa10">Valor    Declarado</td>
    	<td colspan="2"class="filaAzul10"><input name="_valorResidenciafflo" type="text" class="camp_form" style="<?php echo $compRenta2?>" id="_valorResidenciafflo" value="<?php echo $fila[27]?>" size="15" val="<?php echo $propiaObl?>" campo="Valor declarado de residencia fiscal" /></td>
    	<td colspan="3" class="filaRosa10">Superficie Aprox.</td>
    	<td class="filaAzul10"><input name="_superficiefflo" type="text" class="camp_form" id="_superficiefflo"  style="<?php echo $compRenta2?>" value="<?php echo $fila[29]?>" size="8" val="<?php echo $propiaObl?>" campo="Superficie aprox. de residencia fiscal" /></td>
    	<td colspan="2" class="filaRosa10">Metros Construcción</td>
    	<td colspan="2" class="filaAzul10"><input name="_construidofvch" type="text" class="camp_form" style="<?php echo $compRenta2 ?>" id="_construidofvch" value="<?php echo $fila[40]?>" size="17"  val="<?php echo $propiaObl?>" campo="Metros construidos" /></td>
  </tr>
</table>

 <table cellspacing="2" cellpadding="2" width="870">
                                <tr height='23'>
                                  <td colspan="12" class="filaBlanca10">DOMICILIO PARTICULAR</td>
                                </tr>
                                <tr height='23'>
                                  <td  class="filaRosa10">Calle:</td>
                                  <td class="filaAzul10" ><input name="_direccionvvch" type="text" class="camp_form" id="_direccionvvch" value="<?php echo $fila[20]?>" size="25" val="obl" campo="Calle particular" /></td>
                                  <td class="filaRosa10">Num.</td>
                                  <td class="filaAzul10"><input name="_numvvch" type="text" class="camp_form" id="_numvvch" value="<?php echo $fila[23]?>" size="8" val="obl" campo="Número dom. particular" /></td>
                                  <td class="filaAzul10"><span class="filaRosa10">C.P.</span></td>
                                  <td class="filaAzul10"><input name="_cpvint" type="text" class="camp_form" id="_cpvint" onkeypress="return soloNumero(event,false,false,this)" value="<?php echo $fila[24]?>" size="6" maxlength="6" val="obl" campo="C.P." /></td>
                                  <td class="filaRosa10">Col.</td>
                                  <td class="filaAzul10"><input name="_colvvch" type="text" class="camp_form" id="_colvvch" value="<?php echo $fila[21]?>" size="15" val="obl" campo="Colonia de domicilio particular" /></td>
                                  <td class="filaRosa10">Del.</td>
                                  <td class="filaAzul10"><input name="_delvvch" type="text" class="camp_form" id="_delvvch" value="<?php echo $fila[39]?>" size="15" val="obl" campo="Delegaci&oacute;n particular" />                                 </td>
                                  <td class="filaRosa10">Cd.</td>
                                  <td class="filaAzul10"><input name="_ciudadvvch" type="text" class="camp_form" id="_ciudadvvch" value="<?php echo $fila[22]?>" size="15" val="obl" campo="Ciudad de domicilio particular" /></td>
                                </tr>
                                <tr height='23'>
                                  <td  class="filaRosa10">Propiedad</td>
                                  <td class="filaAzul10">
                                  <select name="_tipoResidenciavint" class="camp_form" onchange="cambiaResidenciav(this)" val='obl' campo='Tipo residencia particular'>
                                  <option value="-1">Seleccione</option>
                                    <?php
										$consulta="select idTipoResidencia,tipo from 705_tipoResidencia ";
										$con->generarOpcionesSelect($consulta,$fila[32]);
										
										$compRentav="";
										$propiaOblv="";
										$rentadaOblv="";
										if($fila[32]=="1") //propia
										{
											$compRentav="display:none";
											$propiaOblv="obl";
										}
										else
										{
											$compRenta2v="display:none";
											$rentadaOblv="obl";
										}
									?>
                                  </select></td>
                                  <td colspan="3" class="filaRosa10">Tiempo de Residencia</td>
                                  <td class="filaAzul10"><input name="_tiempoResidenciavvch" type="text" class="camp_form" id="_tiempoResidenciavint" value="<?php echo $fila[26]?>" size="6" maxlength="30" val="obl" campo="Tiempo de residencia" /></td>
                                  
                                  <td colspan="2" class="filaRosa10">Renta mensual</td>
                                  <td colspan="4" class="filaAzul10"><input name="_rentaResidenciavflo" style="<?php echo $compRentav?>" type="text" class="camp_form" id="_rentaResidenciavflo" value="<?php echo $fila[34]?>" size="12" val="<?php echo $rentaOblv?>" campo="Renta residencia particular" /></td>
                                </tr>
                                <tr height='23'>
                                  <td  class="filaRosa10">Valor    Declarado</td>
                                  <td  class="filaAzul10"><input name="_valorResidenciavflo" type="text" style="<?php echo $compRenta2v?>" class="camp_form" id="_valorResidenciavflo" value="<?php echo $fila[28]?>" size="18" val="<?php echo $propiaOblv?>" campo="Valor declarado de residencia particular" /></td>
                                  
                                  <td colspan="3" class="filaRosa10">Superficie Aprox.</td>
                                  <td colspan="2" class="filaAzul10"><input name="_superficievflo" type="text" class="camp_form" style="<?php echo $compRenta2v?>" id="_superficievflo" value="<?php echo $fila[30]?>" size="12" val="<?php echo $propiavOblv?>" campo="Superficie aprox. de residencia fiscal" /></td>
                                  <td colspan="2" class="filaRosa10">Metros Construcción</td>
                                  <td colspan="3" class="filaAzul10"><input name="_construidovvch" type="text" class="camp_form" style="<?php echo $compRenta2v?>" id="_construidovvch" value="<?php echo $fila[41]?>" size="17"  val="<?php echo $propiavOblv?>" campo="Metros construidos" /></td>
                                </tr>
                              </table>
                              
                              <table width="100%">
                              <?php 
									if($varRoot)
									{
								?>
                                <tr>
                                	<td colspan="6">
                                    	<br><br>
                                    	<table>
                                        	<tr>
                                            	<td class="filaRosa10">
                                                Promotor:
                                                </td>
                                                <td>
                                                <select name="_idResponsableint" class="camp_form" >
												<?php
                                                    $consulta="SELECT a.idUsuario, b.Nombre FROM 807_usuariosVSRoles a, 800_usuarios b where a.idUsuario=b.idUsuario and a.idRol='38' order by b.Nombre";
                                                    $con->generarOpcionesSelect($consulta,$fila[15]);
                                                ?>
                                                </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php
								}
								else 
								{ ?>
									<input name="_idResponsableint" type="hidden" id="_idResponsableint" value="<?php echo $_SESSION["idUsr"]?>"  />
								<?php 
								}
								?>
                              </table>
                              
                              <br />
                           <table width="100%"  cellpadding="1" cellspacing="4" bgcolor="#333333">
                            <tr>
                            	<td width="100%" align="center">
                                <br />
                                	<input type="button" value="Aceptar" onclick="validarFrm()" class="btnAceptar"/>
                                </td>
                            </tr>
                            <input type="hidden" name="idEmpresa" id="idEmpresa" value="<?php echo $idEmpresa?>" />
                            <input name="_fechaRegistrodta" type="hidden" id="_fechaRegistrodta" value=""  />
                            <input type="hidden" name="tabla" value="700_empresas" />
                            <input type="hidden" name="id" value="<?php echo $idEmpresa?>" />
                            <input type="hidden" name="campoId" value="idEmpresa" />
                            <input type="hidden" name="pagRedireccion" value="../clientes/tblEmpresas.php"/>	
                            <input type="hidden" name="_clienteint" id="_clienteint" value="1" />
                            <?php
								$funcPHP="";
								if($idEmpresa=="-1")
								{
									$funcPHP=bE('crearCredito(idRegPadre,2)|');
							?>
                                    <input type="hidden" name="reemplazarIDSesion"  value="<?php echo $nConfiguracion?>" />
                                    <input type="hidden" name="sentenciaReemplazo" value='"idEmpresa":"-1"' />
                                    <input type="hidden" name="valorReemplazo" value='"idEmpresa":"idRegPadre"' />
                                    <input type="hidden" name="_statusint" value="1" />
                                    <input type="hidden" name="_tipoint" value="1" />
                                
                            <?php
								}
								
                            ?>	
                            <input type="hidden" name="funcPHPEjecutarNuevo" id="funcPHPEjecutarNuevo"  value="<?php echo $funcPHP?>" />		
                           </table>
								<input type="hidden" name="post" value="configuracion" />
                                <input type="hidden" name="valorPost" value="<?php echo $nConfiguracion?>" />
                            	
                           <?php     
								if($ejecutarExt)
								{
							?>
									<input type="hidden" name="eJs" id="eJs" value="<?php echo $funExtEjec?>" />
							<?php
								}
								?>
                           </form>
                           <br /><br />
                          </div>
                           
                          
                           
                          
                           
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
