<?php session_start();
	include("latis/conexionBD.php"); 
	
	$parametros="";
	if(isset($_POST["funcion"]))
	{
		$funcion=$_POST["funcion"];
		if(isset($_POST["param"]))
		{
			$p=$_POST["param"];
			$parametros=json_decode($p,true);
			
		}
		
	}	
	
	switch($funcion)
	{
		case 1: 
			registrarDatosTRD();
		break;
		case 2:
			registrarRegistroTRD();
		break;
		case 3:
			obtenerRegistrosTRD();
		break;
		case 4:
			rmeoverRegistroTRD();
		break;
	}
	
	
	function registrarDatosTRD()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		if($obj->idRegistro==-1)
		{
			$consulta="INSERT INTO 908_tablasRetencionDocumental(cveTabla,nombreTabla,descripcion,version,idResponsableCreacion,fechaCreacion) 
					VALUES('".cv($obj->cveTabla)."','".cv($obj->nombreTabla)."','".cv($obj->descripcion)."','".cv($obj->version)."',".$_SESSION["idUsr"].",'".date("Y-m-d H:i:s")."')";
		
		
			if($con->ejecutarConsulta($consulta))
				$obj->idRegistro=$con->obtenerUltimoID();
		}
		else
		{
			$consulta="update 908_tablasRetencionDocumental set cveTabla='".cv($obj->cveTabla)."',nombreTabla='".cv($obj->nombreTabla).
					"',descripcion='".cv($obj->descripcion)."',version='".cv($obj->version)."' where idTablaRetencion=".$obj->idRegistro;
			$con->ejecutarConsulta($consulta);
		}
		
		echo "1|".$obj->idRegistro;
	}
	
	
	function registrarRegistroTRD()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		$idTablaRetencion=$obj->idTablaRetencion;
		
		$idSerie=isset($obj->idSerie)?$obj->idSerie:"NULL";
		$idSubSerie=isset($obj->idSubSerie)?$obj->idSubSerie:"NULL";
		$tipoDocumento=isset($obj->tipoDocumento)?$obj->tipoDocumento:"NULL";
		
		$retencionArchivoGestion="NULL";
		$unidadRetencionArchivoGestion="NULL";
		$retencionArchivoCentral="NULL";
		$unidadRetencionArchivoCentral="NULL";
		
		$conservacionTotal="0";
		$eliminacion="0";
		$microfilmacionDigitalizacion="0";
		$seleccion="0";
		$procedimiento="NULL";
		
		$tipoElemento=$obj->tipoRegistro;
		$cveElemento=$obj->cveElemento;
		
		
		
		$arrSoporte=explode(",",$obj->soporte);
		
		$soporteFisico=existeValor($arrSoporte,1)?1:0;
		$soporteElectronico=existeValor($arrSoporte,2)?1:0;
		
		if($obj->tRetencionGestion!="")
			$retencionArchivoGestion=$obj->tRetencionGestion;
		if($obj->pRetencionGestion!="")
			$unidadRetencionArchivoGestion=$obj->pRetencionGestion;
		
		if($obj->tRetencionCentral!="")
			$retencionArchivoCentral=$obj->tRetencionCentral;
		
		if($obj->pRetencionCentral!="")
			$unidadRetencionArchivoCentral=$obj->pRetencionCentral;
		
		
		switch($obj->disposicionFinal)
		{
			case 1://CT
				$conservacionTotal=1;
			break;
			case 2://E
				$eliminacion=1;
			break;
			case 3://MT
				$microfilmacionDigitalizacion=1;
			break;
			case 4://S
				$seleccion=1;
			break;
		}
		
		
		$procedimiento=$obj->procedimiento;
		$tituloElemento=isset($obj->tituloElemento)?$obj->tituloElemento:"";
		
		$consulta="";
		if($obj->idRegistro==-1)
		{
			$consulta="INSERT INTO 908_registrosTablasRetencionDocumental(idTablaRetencion,idSerie,idSubSerie,tipoDocumento,soporteFisico,soporteElectronico,retencionArchivoGestion,
					unidadRetencionArchivoGestion,retencionArchivoCentral,unidadRetencionArchivoCentral,conservacionTotal,eliminacion,microfilmacionDigitalizacion,seleccion,procedimiento,
					tipoElemento,cveElemento,tituloElemento) values(".$idTablaRetencion.",".$idSerie.",".$idSubSerie.",'".$tipoDocumento."',".$soporteFisico.",".$soporteElectronico.",".$retencionArchivoGestion.
					",".$unidadRetencionArchivoGestion.",".$retencionArchivoCentral.",".$unidadRetencionArchivoCentral.",".$conservacionTotal.",".$eliminacion.",".$microfilmacionDigitalizacion.",".$seleccion.
					",'".cv($procedimiento)."',".$tipoElemento.",'".cv($cveElemento)."','".cv($tituloElemento)."')";

		}
		else
		{
			$consulta="update 908_registrosTablasRetencionDocumental set idSerie=".$idSerie.",idSubSerie=".$idSubSerie.",tipoDocumento=".$tipoDocumento.",soporteFisico=".$soporteFisico.
					",soporteElectronico=".$soporteElectronico.",retencionArchivoGestion=".$retencionArchivoGestion.",unidadRetencionArchivoGestion=".$unidadRetencionArchivoGestion.
					",retencionArchivoCentral=".$retencionArchivoCentral.",unidadRetencionArchivoCentral=".$unidadRetencionArchivoCentral.",conservacionTotal=".$conservacionTotal.
					",eliminacion=".$eliminacion.",microfilmacionDigitalizacion=".$microfilmacionDigitalizacion.",seleccion=".$seleccion.",procedimiento='".cv($procedimiento)."',
					cveElemento='".cv($cveElemento)."',tituloElemento='".cv($tituloElemento)."'
					where idRegistro=".$obj->idRegistro;
		}
 		eC($consulta);
		echo "1|";
	}
	
	
	function obtenerRegistrosTRD()
	{
		global $con;
		$idTablaRetencion=$_POST["idTablaRetencion"];
		$arrRegistros="";
		$numReg=0;
		$consulta="SELECT idRegistro,idSerie,idSubSerie,tipoDocumento,soporteFisico,soporteElectronico,retencionArchivoGestion,unidadRetencionArchivoGestion,retencionArchivoCentral,unidadRetencionArchivoCentral,
					conservacionTotal,eliminacion,microfilmacionDigitalizacion,seleccion,procedimiento,tituloElemento,cveElemento,tipoElemento FROM 908_registrosTablasRetencionDocumental WHERE 
					idTablaRetencion=".$idTablaRetencion." AND tipoElemento=1 ORDER BY cveElemento";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$o='{"idRegistro":"'.$fila["idRegistro"].'","idSerie":"","cveSerie":"'.$fila["cveElemento"].'","idSubSerie":"","cveSubSerie":"","tipoDocumento":"'.cv($fila["tituloElemento"]).
			'","soporteFisico":'.($fila["soporteFisico"]==1?"true":"false").',"soporteElectronico":'.
				($fila["soporteElectronico"]==1?"true":"false").',"retencionArchivoGestion":"'.$fila["retencionArchivoGestion"].'","unidadRetencionArchivoGestion":"'.$fila["unidadRetencionArchivoGestion"].'",
					"retencionArchivoCentral":"'.$fila["retencionArchivoCentral"].'","unidadRetencionArchivoCentral":"'.$fila["unidadRetencionArchivoCentral"].
					'","conservacionTotal":'.($fila["conservacionTotal"]==1?"true":"false").',"eliminacion":'.($fila["eliminacion"]==1?"true":"false").
					',"microfilmacionDigitalizacion":'.($fila["microfilmacionDigitalizacion"]==1?"true":"false").
					',"seleccion":'.$fila["seleccion"].',"procedimiento":"'.cv($fila["procedimiento"]).'","tipoElemento":"'.$fila["tipoElemento"].'"}';
		
			if($arrRegistros=="")
				$arrRegistros=$o;
			else
				$arrRegistros.=",".$o;
			$numReg++;	
				
			$consulta="SELECT idRegistro,idSerie,idSubSerie,tipoDocumento,soporteFisico,soporteElectronico,retencionArchivoGestion,unidadRetencionArchivoGestion,retencionArchivoCentral,unidadRetencionArchivoCentral,
					conservacionTotal,eliminacion,microfilmacionDigitalizacion,seleccion,procedimiento,tituloElemento,cveElemento,tipoElemento FROM 908_registrosTablasRetencionDocumental WHERE 
					idTablaRetencion=".$idTablaRetencion." and idSerie=".$fila["idRegistro"]." AND tipoElemento=2 ORDER BY cveElemento";
			$resSubserie=$con->obtenerFilas($consulta);
			while($filaSubserie=mysql_fetch_assoc($resSubserie))
			{
				$o='{"idRegistro":"'.$filaSubserie["idRegistro"].'","idSerie":"'.$fila["idRegistro"].'","cveSerie":"'.$fila["cveElemento"].'","idSubSerie":"'.$filaSubserie["idRegistro"].
					'","cveSubSerie":"'.cv($filaSubserie["cveElemento"]).'","tipoDocumento":"'.cv($filaSubserie["tituloElemento"]).
				'","soporteFisico":'.($filaSubserie["soporteFisico"]==1?"true":"false").',"soporteElectronico":'.
					($filaSubserie["soporteElectronico"]==1?"true":"false").',"retencionArchivoGestion":"'.$filaSubserie["retencionArchivoGestion"].'","unidadRetencionArchivoGestion":"'.
					$filaSubserie["unidadRetencionArchivoGestion"].'",
						"retencionArchivoCentral":"'.$filaSubserie["retencionArchivoCentral"].'","unidadRetencionArchivoCentral":"'.$filaSubserie["unidadRetencionArchivoCentral"].
						'","conservacionTotal":'.($filaSubserie["conservacionTotal"]==1?"true":"false").',"eliminacion":'.($filaSubserie["eliminacion"]==1?"true":"false").
						',"microfilmacionDigitalizacion":'.($filaSubserie["microfilmacionDigitalizacion"]==1?"true":"false").
						',"seleccion":'.$filaSubserie["seleccion"].',"procedimiento":"'.cv($filaSubserie["procedimiento"]).'","tipoElemento":"'.$filaSubserie["tipoElemento"].'"}';
			
				if($arrRegistros=="")
					$arrRegistros=$o;
				else
					$arrRegistros.=",".$o;		
				
				$numReg++;
				
				
				$consulta="SELECT idRegistro,idSerie,idSubSerie,tipoDocumento,soporteFisico,soporteElectronico,retencionArchivoGestion,unidadRetencionArchivoGestion,retencionArchivoCentral,unidadRetencionArchivoCentral,
					conservacionTotal,eliminacion,microfilmacionDigitalizacion,seleccion,procedimiento,tituloElemento,cveElemento,tipoElemento FROM 908_registrosTablasRetencionDocumental WHERE 
					idTablaRetencion=".$idTablaRetencion." and idSerie=".$fila["idRegistro"]." and idSubSerie=".$filaSubserie["idRegistro"]." AND tipoElemento=3 ORDER BY cveElemento";
				$resTipoDocumental=$con->obtenerFilas($consulta);
				while($filaTipoDocumental=mysql_fetch_assoc($resTipoDocumental))
				{
					$o='{"idRegistro":"'.$filaTipoDocumental["idRegistro"].'","idSerie":"'.$fila["idRegistro"].'","cveSerie":"'.$fila["cveElemento"].'","idSubSerie":"'.$filaSubserie["idRegistro"].
						'","cveSubSerie":"'.cv($filaSubserie["cveElemento"]).'","tipoDocumento":"'.cv($filaTipoDocumental["tipoDocumento"]).
					'","soporteFisico":'.($filaTipoDocumental["soporteFisico"]==1?"true":"false").',"soporteElectronico":'.
						($filaTipoDocumental["soporteElectronico"]==1?"true":"false").',"retencionArchivoGestion":"'.$filaTipoDocumental["retencionArchivoGestion"].'","unidadRetencionArchivoGestion":"'.
						$filaTipoDocumental["unidadRetencionArchivoGestion"].'",
							"retencionArchivoCentral":"'.$filaTipoDocumental["retencionArchivoCentral"].'","unidadRetencionArchivoCentral":"'.$filaTipoDocumental["unidadRetencionArchivoCentral"].
							'","conservacionTotal":'.($filaTipoDocumental["conservacionTotal"]==1?"true":"false").',"eliminacion":'.($filaTipoDocumental["eliminacion"]==1?"true":"false").
							',"microfilmacionDigitalizacion":'.($filaTipoDocumental["microfilmacionDigitalizacion"]==1?"true":"false").
							',"seleccion":'.$filaTipoDocumental["seleccion"].',"procedimiento":"'.cv($filaTipoDocumental["procedimiento"]).'","tipoElemento":"'.$filaTipoDocumental["tipoElemento"].'"}';
				
					if($arrRegistros=="")
						$arrRegistros=$o;
					else
						$arrRegistros.=",".$o;		
					
					$numReg++;
				}
				
			}
		}
		
		
		$arrSeries="";
		$consulta="SELECT idRegistro,CONCAT('[',cveElemento,'] ',tituloElemento) AS serie FROM 908_registrosTablasRetencionDocumental WHERE idTablaRetencion=".$idTablaRetencion." and tipoElemento=1  ORDER BY tituloElemento";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			
			$consulta="SELECT idRegistro,CONCAT('[',cveElemento,'] ',tituloElemento) AS subserie FROM 908_registrosTablasRetencionDocumental WHERE 
					idTablaRetencion=".$idTablaRetencion." and idSerie=".$fila["idRegistro"]." and tipoElemento=2  ORDER BY tituloElemento";
		
			$arrSubserie=$con->obtenerFilasArreglo($consulta);
			$o="['".$fila["idRegistro"]."','".cv($fila["serie"])."',".$arrSubserie."]";
			if($arrSeries=="")
				$arrSeries=$o;
			else
				$arrSeries.=",".$o;
		}
		echo '{"numReg":"'.$numReg.'","registros":['.$arrRegistros.'],"arrSeries":['.$arrSeries.']}';
		
		
	}
	
	function rmeoverRegistroTRD()
	{
		global $con;
		$cadObj=$_POST["cadObj"];
		$obj=json_decode($cadObj);
		
		$x=0;
		$consulta[$x]="begin";
		$x++;
		$consulta[$x]="DELETE FROM 908_registrosTablasRetencionDocumental WHERE idRegistro=".$obj->idRegistro;
		$x++;
		switch($obj->tipoElemento)
		{
			case '1':
				$consulta[$x]="DELETE FROM 908_registrosTablasRetencionDocumental WHERE idSerie=".$obj->idRegistro;
				$x++;
			break;
			case '2':
				$consulta[$x]="DELETE FROM 908_registrosTablasRetencionDocumental WHERE idSubSerie=".$obj->idRegistro;
				$x++;
			break;
			
		}
		
		$consulta[$x]="commit";
		$x++;
		
		eB($consulta);
	}
	
?>