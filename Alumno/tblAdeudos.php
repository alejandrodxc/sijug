<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$guardarConfSession=true;
	$mostrarMenuIzq=false;
	$paramGET=true;
	$tituloModulo="Adeudos";
?>

<link rel="stylesheet" type="text/css" href="../Scripts/ux/grid/GridSummary.css" />
<style>
	.x-grid3-cell-first .x-grid-cell-inner
	{
		padding-left: 0px !important;
	}
</style>
<style>
	#main_content, .p15, #example_content 
	{
		padding:0px !important;
	}
</style>
<script type="text/javascript" src="../Scripts/funcionesProcesos.js.php"></script>
<script type="text/javascript" src="../Scripts/ux/grid/GridSummary.js"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="left">
                        	
                        		<script type="text/javascript" src="../Scripts/base64.js"></script>
                        	<script type="text/javascript" src="../Alumno/Scripts/tblAdeudos.js.php"></script>
                        	<?php
								echo formatearTituloPagina($tituloModulo);
								$idUsuario=$_SESSION["idUsr"];
								if(isset($objParametros->idUsuarioRegistro))
									$idUsuario=$objParametros->idUsuarioRegistro;
								$idInstanciaPlan=-1;
								if(isset($objParametros->idInstanciaPlan))
									$idInstanciaPlan=$objParametros->idInstanciaPlan;
									
								$consulta="SELECT idConcepto FROM 564_conceptosVSCategorias c WHERE idCategoria=4";
								$listConceptos=$con->obtenerListaValores($consulta);
								if($listConceptos=="")
									$listConceptos=-1;
								$maxColumna=0;
								$arrRegistros="";
								$oRegistro="";
								$condComplementario="";
								
								if($idInstanciaPlan!=-1)
								{
									$arrDimensiones["IdInstanciaPlan"]=$idInstanciaPlan;
									$listMovimientos=obtenerIdAsientoPagoReferenciado($arrDimensiones);
									
									$consulta="SELECT idMovimiento,(select count(*) from 6012_detalleAsientoPago WHERE idAsientoPago=m.idMovimiento AND idDimension=7) FROM 6011_movimientosPago m WHERE idUsuario=".$idUsuario."  AND situacion=1";
									$rMov=$con->obtenerFilas($consulta);
									while($fMovimiento=mysql_fetch_row($rMov))
									{
										if($fMovimiento[1]==0)
										{
											if($listMovimientos=="")
												$listMovimientos=$fMovimiento[0];
											else
												$listMovimientos.=",".$fMovimiento[0];
										}	
									}
									
									if($listMovimientos!=-1)
										$condComplementario=" and idMovimiento in(".$listMovimientos.")";
										
										
										
								}
								
								
								$maxColumna=1;
								$consulta="SELECT idMovimiento,idReferencia,idConcepto,pagado,fechaPago,fechaVencimiento,montoPagado,descripcionAdeudo FROM 6011_movimientosPago 
										WHERE  idUsuario=".$idUsuario." AND pagado in (0,1) AND idConcepto IN (".$listConceptos.") and situacion=1 ".$condComplementario." order by fechaVencimiento";
								$res=$con->obtenerFilas($consulta);
								while($fila=mysql_fetch_row($res))
								{
									$consulta="SELECT monto,fechaInicio,fechaFin FROM 6012_asientosPago WHERE idReferenciaMovimiento=".$fila[0];
									$resPago=$con->obtenerFilas($consulta);
									if($con->filasAfectadas>$maxColumna)
										$maxColumna=$con->filasAfectadas;
								}
								if(mysql_num_rows($res)>0)
									mysql_data_seek($res,0);
								

								while($fila=mysql_fetch_row($res))
								{
									$situacion="";
									switch($fila[3])
									{
										case 1:
											$situacion=1;
										break;
										case 0:
											$situacion=0;
											if($fila[5]!="")
											{
												if(strtotime($fila[5])<strtotime(date("Y-m-d")))
												{
													$situacion=2;
												}
											}
										break;
									}
									$oRegistro="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$situacion."','".$fila[4]."','".$fila[5]."'";
									$consulta="SELECT cveConcepto,nombreConcepto FROM 561_conceptosIngreso WHERE idConcepto=".$fila[2];
									$fConcepto=$con->obtenerPrimeraFila($consulta);
									
									$compConcep="";
									if($fila[7]!="")
									{
										$compConcep="<br>(".cv($fila[7]).")";
									}
									
									$oRegistro.=",'[".$fConcepto[0]."] ".cv($fConcepto[1]).$compConcep."','".$fila[6]."'";
									$consulta="SELECT monto,fechaInicio,fechaFin FROM 6012_asientosPago WHERE idReferenciaMovimiento=".$fila[0];
									$resPago=$con->obtenerFilas($consulta);
									
									$nPago=0;	
									while($fPago=mysql_fetch_row($resPago))
									{
										$detallePago="";
										$f["fechaInicio"]=$fPago[1];
										$f["fechaFin"]=$fPago[2];
										if(($f["fechaInicio"]!="")&&($f["fechaFin"]!=""))
										{
											$detallePago="Del ".date("d/m/Y",strtotime($f["fechaInicio"]))." al ".date("d/m/Y",strtotime($f["fechaFin"]))."";
										}
										else
										{
											if($f["fechaInicio"]!="")
											{
												$detallePago="A partir del ".date("d/m/Y",strtotime($f["fechaInicio"]))."";
											}
											else
											{
												if($f["fechaFin"]!="")
												{
													$detallePago="Hasta del ".date("d/m/Y",strtotime($f["fechaFin"]))."";
												}
												else
													$detallePago.=",''";
											}
										}
										$oRegistro.=",'".$fPago[0]."','".$detallePago."'";	
										$nPago++;
									}
									for($nPos=$nPago;$nPos<$maxColumna;$nPos++)
									{
										$oRegistro.=",'',''";	
									}
									$consulta="SELECT idEstado,id__900_tablaDinamica FROM _900_tablaDinamica WHERE idEstado<>4 and idReferencia=".$fila[0];
									$fDatosFecha=$con->obtenerPrimeraFila($consulta);
									$oRegistro.=",'".$fDatosFecha[0]."','".$fDatosFecha[1]."'";
									$oRegistro.=']';
									if($arrRegistros=="")
										$arrRegistros=$oRegistro;
									else
										$arrRegistros.=",".$oRegistro;
								}
								$arrRegistros= '['.$arrRegistros.']';
								
								$maxColumnaOtros=0;
								$arrRegistrosOtros="";
								$oRegistro="";
								
								$consulta="SELECT idMovimiento,idReferencia,idConcepto,pagado,fechaPago,fechaVencimiento,montoPagado,descripcionAdeudo FROM 6011_movimientosPago 
										WHERE  idUsuario=".$idUsuario." AND pagado in (0,1) AND idConcepto not IN (".$listConceptos.") and situacion=1 ".$condComplementario." order by fechaVencimiento";

								$res=$con->obtenerFilas($consulta);
								$maxColumnaOtros=1;
								
								while($fila=mysql_fetch_row($res))
								{
									$consulta="SELECT monto,fechaInicio,fechaFin FROM 6012_asientosPago WHERE idReferenciaMovimiento=".$fila[0];
									$resPago=$con->obtenerFilas($consulta);
									if($con->filasAfectadas>$maxColumna)
										$maxColumnaOtros=$con->filasAfectadas;
								}
								if(mysql_num_rows($res)>0)
									mysql_data_seek($res,0);
								
								while($fila=mysql_fetch_row($res))
								{
									$situacion="";
									switch($fila[3])
									{
										case 1:
											$situacion=1;
										break;
										case 0:
											$situacion=0;
											if($fila[5]!="")
											{
												if(strtotime($fila[5])<strtotime(date("Y-m-d")))
												{
													$situacion=2;
												}
											}
										break;
									}
									$oRegistro="['".$fila[0]."','".$fila[1]."','".$fila[2]."','".$situacion."','".$fila[4]."','".$fila[5]."'";
									$consulta="SELECT cveConcepto,nombreConcepto FROM 561_conceptosIngreso WHERE idConcepto=".$fila[2];
									$fConcepto=$con->obtenerPrimeraFila($consulta);
									
									$compConcep="";
									if($fila[7]!="")
									{
										$compConcep="<br>(".cv($fila[7]).")";
									}
									
									$oRegistro.=",'[".$fConcepto[0]."] ".cv($fConcepto[1]).$compConcep."','".$fila[6]."'";
									$consulta="SELECT monto,fechaInicio,fechaFin FROM 6012_asientosPago WHERE idReferenciaMovimiento=".$fila[0];
									$resPago=$con->obtenerFilas($consulta);
									
									$nPago=0;		
									while($fPago=mysql_fetch_row($resPago))
									{
										$detallePago="";
										$f["fechaInicio"]=$fPago[1];
										$f["fechaFin"]=$fPago[2];
										if(($f["fechaInicio"]!="")&&($f["fechaFin"]!=""))
										{
											$detallePago="Del ".date("d/m/Y",strtotime($f["fechaInicio"]))." al ".date("d/m/Y",strtotime($f["fechaFin"]))."";
										}
										else
										{
											if($f["fechaInicio"]!="")
											{
												$detallePago="A partir del ".date("d/m/Y",strtotime($f["fechaInicio"]))."";
											}
											else
											{
												if($f["fechaFin"]!="")
												{
													$detallePago="Hasta del ".date("d/m/Y",strtotime($f["fechaFin"]))."";
												}
												else
													$detallePago.=",''";
											}
										}
										$oRegistro.=",'".$fPago[0]."','".$detallePago."'";	
										$nPago++;	
									}
									
									for($nPos=$nPago;$nPos<$maxColumnaOtros;$nPos++)
									{
										$oRegistro.=",'',''";	
									}
									
									$consulta="SELECT idEstado,id__900_tablaDinamica FROM _900_tablaDinamica WHERE idEstado<>4 and idReferencia=".$fila[0];
									$fDatosFecha=$con->obtenerPrimeraFila($consulta);
									$oRegistro.=",'".$fDatosFecha[0]."','".$fDatosFecha[1]."'";
									$oRegistro.=']';
									if($arrRegistrosOtros=="")
										$arrRegistrosOtros=$oRegistro;
									else
										$arrRegistrosOtros.=",".$oRegistro;
								}
								$arrRegistrosOtros= '['.$arrRegistrosOtros.']';
								

							?>
                            
                            <input type="hidden" id="maxNumPago" value="<?php echo $maxColumna?>" />
                            <input type="hidden" id="arrRegistros" value="<?php echo bE($arrRegistros)?>" />
                            <input type="hidden" id="maxColumnaOtros" value="<?php echo $maxColumnaOtros?>" />
                            <input type="hidden" id="arrRegistrosOtros" value="<?php echo bE($arrRegistrosOtros)?>" />
                            <table width="780">
                            	<tr>
                                	<td>
                                    <?php
										if($idInstanciaPlan!=-1)
										{
											$consulta="SELECT idEstado,id__933_tablaDinamica FROM _933_tablaDinamica WHERE idUsuarioRegistro=".$idUsuario." AND idInstanciaPlan=".$idInstanciaPlan." AND idEstado  IN(1,2)";
											$fPlanPagos=$con->obtenerPrimeraFila($consulta);
											if($fPlanPagos)
											{
												$compPagos='<a href="javascript:window.parent.verRegistroProyecto(\''.bE($fPlanPagos[1]).'\',\''.bE(783).'\',\''.bE(933).'\')"><img  src="../images/exclamation.png" title="Existe una solicitud de cambio de plan de pagos vigente, para observar dicha solicitud de click AQUÍ" alt="Existe una solicitud de cambio de plan de pagos vigente, para observar dicha solicitud de click AQUÍ"></a>';
											}
											else
											{
												$compPagos='<a href="javascript:window.parent.verRegistroProyecto(\''.bE(-1).'\',\''.bE(191).'\',\''.bE(933).'\',\''.bE("[['actorInicio','1'],['idUsuarioRegistro','".$idUsuario."'],['idInstanciaPlan','".$idInstanciaPlan."']]").'\')"><img  src="../images/pencil.png" title="Solicitar cambio de plan de pagos" alt="Solicitar cambio de plan de pagos"></a>';
											}
											
											
											$consulta="SELECT p.idPlanPagos,nombrePlanPago FROM 4537_situacionActualAlumno s,6020_planesPago p 
													WHERE idAlumno=".$idUsuario." AND idInstanciaPlanEstudio=".$idInstanciaPlan." AND p.idPlanPagos=s.idPlanPagos";
											$fPlanPagos=$con->obtenerPrimeraFila($consulta);
									?>
                                    	<table>
                                        	<tr>
                                            	<td width="190"><span class="corpo8_bold">Plan de Pago seleccionado:</span></td>
                                                <td width="600"><span class="letraRojaSubrayada8"><?php echo $fPlanPagos[1]." ".$compPagos?></span></td>
                                                
                                            </tr>
                                        </table>
                                        <br />
                                    <?php
										}
									?>
                                    </td>
                                </tr>
                            	<tr>
                                	<td align="left">
                                    	<fieldset class="frameHijoV3"><legend>Pagos de colegiatura</legend>
                                        <span style="font-size:11px">
                                        <table>
                                        	<tr>
                                            	<td><img src="../images/control_pause.png" /></td><td>&nbsp;= En Espera de Pago</td><td width="15"></td>
                                                <td><img src="../images/icon_big_tick.gif" /></td><td>&nbsp;= Pago Realizado</td><td width="15"></td>
                                                <td><img src="../images/cancel_round.png" /></td><td>&nbsp;= Pago Vencido</td><td width="15"></td>
                                                <td><img src="../images/page_white_acrobat.png" /></td><td>&nbsp;= Descargar Formato de Pago Referenciado</td>
                                            </tr>
                                            <tr height="10">
                                            <td></td>
                                            </tr>
                                        </table>
                                        </span>
                                    	<span id="tblPagos"></span>
                                         </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <br /><br />
                            <table width="780">
                            	<tr>
                                	<td align="left">
                                    	<fieldset class="frameHijoV3"><legend>Otros pagos vigentes</legend>
                                        
                                    	<span id="tblPagosOtros"></span>
                                         </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
