<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares WHERE idCiclo IN 
				(SELECT DISTINCT g.idCiclo FROM 4520_grupos g,4517_alumnosVsMateriaGrupo a WHERE a.idGrupo=g.idGrupos AND a.idUsuario=".$_SESSION["idUsr"]." AND a.situacion=1)ORDER BY nombreCiclo";
	$arrCiclos=$con->obtenerFilasArregloAsocPHP($consulta);	
	$consulta="SELECT idCiclo FROM 4526_ciclosEscolares WHERE situacion=1";
	$idCicloActivo=$con->obtenerValor($consulta);			
	if(!isset($arrCiclos[$idCicloActivo]))
	{
		if(sizeof($arrCiclos))
		{
			foreach($arrCiclos as $ciclo=>$resto)
				$idCicloActivo=$ciclo;
		}
	}
	$arrCadCiclos="";
	foreach($arrCiclos as $ciclo=>$resto)
	{
		$obj="['".$ciclo."','".$resto."']";
		
		if($arrCadCiclos=='')
			$arrCadCiclos=$obj;
		else
			$arrCadCiclos.=','.$obj;
			
	}
	$arrCadCiclos="[".$arrCadCiclos."]";
	$consulta="SELECT idStatus,nombreStatus FROM 4527_status";
	$arrSituacion=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCategoria,nombreCategoria FROM 4533_categoriasMaterialDidactico WHERE (idUsuario=".$_SESSION["idUsr"]." OR idUsuario IS NULL) ORDER BY nombreCategoria";
	$arrCategorias=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT extension,urlVisor,ancho,alto FROM 4534_visoresMaterial";
	$arrVisores=$con->obtenerFilasArreglo($consulta);
	$consulta="select idStatus,nombreStatus FROM 4527_status where idStatus in (10,11)";
	$arrTiposSesion=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT DISTINCT idPeriodo FROM 4517_alumnosVsMateriaGrupo WHERE idUsuario=".$_SESSION["idUsr"];
	$listaPeriodos=$con->obtenerListaValores($consulta);
	if($listaPeriodos=="")
		$listaPeriodos=-1;
	
	$consulta="SELECT id__464_gridPeriodos,nombrePeriodo FROM _464_gridPeriodos where id__464_gridPeriodos in(".$listaPeriodos.")";
	$arrPeriodos=$con->obtenerFilasArreglo($consulta);
	
	$consulta=" SELECT idPeriodo FROM 4520_grupos g,4519_asignacionProfesorGrupo a WHERE g.idCiclo=".$idCicloActivo." AND '".date("Y-m-")."'>=fechaInicio 
				AND '".date("Y-m-")."'<=fechaFin AND a.idGrupo=g.idGrupos AND a.situacion=1 AND idUsuario=".$_SESSION["idUsr"];

	$idPeriodoActivo=$con->obtenerValor($consulta);
	if($idPeriodoActivo=="")
		$idPeriodoActivo=-1;	
		
	$consulta="SELECT id__984_conversionCalificaciones,abreviatura FROM _984_conversionCalificaciones";
	$arrAbreviatura=$con->obtenerFilasArreglo($consulta);		
?>
var arrAbreviatura=<?php echo $arrAbreviatura?>;
var arrPeriodos=<?php echo $arrPeriodos?>;
var arrVisores=<?php echo $arrVisores?>;
var arrSituacion=<?php echo $arrSituacion?>;
var arrCategorias=<?php echo $arrCategorias?>;
var arrTiposSesion=<?php echo $arrTiposSesion?>;
var nodoSel=null;
var pagoReferenciado='';
Ext.onReady(inicializar);

var nodoSel=null;
var nodoTemarioSel=null;
var filaSesion=null;


var idPlanEstudio='-1';
var idCiclo='-1';
var idPeriodo='-1';
var idUsuario=-1;


function inicializar()
{
	var cmbCiclo=crearComboExt('cmbCiclo',<?php echo $arrCadCiclos?>,0,0,140);
    cmbCiclo.setValue('<?php echo $idCicloActivo ?>');
    
    cmbCiclo.on('select',function()
    						{
                            	recargarDatos();
                            }
    			)
	idCiclo=cmbCiclo.getValue();
	var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodos,0,0,140);
   	<?php 
		if($idPeriodoActivo!=-1)
		{
	?>
    	cmbPeriodo.setValue('<?php echo $idPeriodoActivo ?>');
    <?php
		}
		else
		{
	?>
    	cmbPeriodo.setValue(arrPeriodos[0][0]);
    <?php
		}
	?>   
    cmbPeriodo.on('select',function()
    						{
                            	recargarDatos();
                            }
    			)                
	idPeriodo= cmbPeriodo.getValue();               
	var IdUsuario=gE("idUsuario").value;
    idUsuario=IdUsuario;
    var Ciclo=gE("ciclo").value;
    var vUsuario=gE('vistaActual').value;
	Ext.QuickTips.init();
	var detailEl;
    
    var gridCalificaciones=crearGridCalificaciones();
	var contentPanel = 
    {
		id: 'content-panel',
		region: 'center', 
		layout: 'Border',
		margins: '2 5 5 0',
		activeItem: 0,
		border: false,
		items: 	[
        			{
                    	xtype:'tabpanel',
                        region:'north',
                        border:false,
                        height:280, 
                        activeTab:0,
                        frame:false,
                        items:	[
      								crearGridSesiones()
                                        
                                    
                        		]
                    }
                    ,
                    {
                    	xtype:'tabpanel',
                        region:'center',
					    activeTab: 0,  
                        
                        split:true, 
                        id:'panelContenido',
                        items:	[
                        			
                                    {
                                    	id:'panelMaterial',
                                    	title:'Material did&aacute;ctico',
                                        layout:'anchor',
                                        items:[crearGridMaterialDidactico()]
                                    },
                                    {
                                    	id:'panelCalificaciones',
                                    	title:'Calificaciones',
                                        layout:'anchor',
                                        items:[gridCalificaciones]
                                    }
                                    
                        		]
                    }
				]
	};
    
    var cargadorArbol=new Ext.tree.TreeLoader(
												{
													baseParams:{
																	funcion:'30',
																	idUsuario:IdUsuario,
                                                                    idCiclo:gEx('cmbCiclo').getValue(),
                                                                    idPeriodo:gEx('cmbPeriodo').getValue(),
                                                                    vista:vUsuario,
                                                                    vAlumno:1
																},
													dataUrl:'../paginasFunciones/funcionesProgramaAcademicoV2.php'
												}
											)	
    
    
    cargadorArbol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.idCiclo=gEx('cmbCiclo').getValue();
                                        proxy.baseParams.idPeriodo=gEx('cmbPeriodo').getValue();
                                    }
                    )
    
	var raiz=new  Ext.tree.AsyncTreeNode	(
                                                  {
                                                      id:'0',
                                                      text:'',
                                                      draggable:false,
                                                      expanded :true,
                                                      icon:'../images/Icono_txt.gif',
                                                      expanded  :true
                                                  }
                                            )
    
    
    var treePanel = new Ext.tree.TreePanel	(	{
                                                    id: 'tree-panel',
                                                    title: '<?php echo $tituloDefMateriaPlural?>',
                                                    region:'north',
                                                    split: true,
                                                    autoScroll: true,
                                                    rootVisible: false,
                                                    lines: true,
                                                    singleExpand: true,
                                                    useArrows: true,
                                                    loader: cargadorArbol,
                                                    collapsible:true,
                                                    root: raiz,
                                                    height:270
                                                }
											);
    
	
    var detalleMateria=	{
    						xtype:'tabpanel',
                            region:'center',
                           
						    activeTab: 0,                            
                            items:	[
                            			{
                                            title:'Temario',
                                            id:'panelTemario',
                                            autoScroll: true,
                                            tbar:	[
                                            			{
                                                              icon:'../images/Icono_css_incl.gif',
                                                              cls:'x-btn-text-icon',
                                                              tooltip:'Ver ficha de la materia',
                                                              disabled:true,
                                                              id:'btnFichaMateria',
                                                              handler:function()
                                                                      {
                                                                          verFichaMateria();
                                                                      }
                                                              
                                                          },'-',
                                                          {
                                                              icon:'../images/page_accept.png',
                                                              cls:'x-btn-text-icon',
                                                              tooltip :'Ver criterios de evaluaci&oacute;n',
                                                              disabled:true,
                                                              hidden:true,
                                                              id:'btnVerCriterios',
                                                              handler:function()
                                                                      {
                                                                          verCriteriosEvaluacion();
                                                                      }
                                                              
                                                          }
                                            		],
                                            items:[
                                            			crearArbolTemario()
                                            		]
                                            
                                        }
                            		]
    					}

    new Ext.Viewport(	
    
    					{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:[
                                            		{
                                                          icon:'../images/salir.gif',
                                                          cls:'x-btn-text-icon',
                                                          text:'Salir',
                                                          handler:function()
                                                                  {
                                                                  	window.location.href='../principal/inicio.php';
                                                                  }
                                                          
                                                      }	,'-',
                                                      {
                                                          xtype:'label',
                                                          html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8"><b>Ciclo:</b></span>&nbsp;&nbsp;'
                                                      },
                                                      cmbCiclo,'-',
                                                      	
                                                      {
                                                          xtype:'label',
                                                          html:'&nbsp;&nbsp;<span class="letraRojaSubrayada8"><b>Periodo:</b></span>&nbsp;&nbsp;'
                                                      },
                                                      cmbPeriodo
                                                      
                                                  ],
                                            items:	[
                                            			{
                                                            layout: 'border',
                                                            id: 'layout-browser',
                                                            region:'west',
                                                            border: false,
                                                            split:true,
                                                            margins: '2 0 5 5',
                                                            width: 340,
                                                            collapsible:true,
                                                            items: [treePanel, detalleMateria]
                                                        },
                                                        contentPanel
                                                    
                                            		]
                                        }
                                    ]
                        }
    					
                    );
	treePanel.expandAll();   
  
    treePanel.on('click', 	function(n)
                            {
                            	nodoSel=n;
                                gEx('btnFichaMateria').enable();
                                gEx('btnVerCriterios').enable();
                                if(nodoSel.attributes.idGrupo=='-1')
                                {
                                	gEx('btnFichaMateria').disable();
                                    gEx('btnVerCriterios').disable();
                                }
                                recargarTemario();
                                var tamPagina=30;
                                gEx('gridCalificaciones').getStore().reload();
                                gEx('gridSesiones').getStore().reload();
                                gEx('gridMaterialDidactico').getStore().load( {
                                                                                    url: '../paginasFunciones/funcionesPlanteles.php',
                                                                                    params:{
                                                                                                start:0,
                                                                                                limit:tamPagina,
                                                                                                idGrupo:nodoSel.attributes.idGrupo
                                                                                            }
                                                                                    }
                                                                                ) ;     
                            }
    			);
                     
}

function crearGridCalificaciones()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idCalificacion'},
                                                        {name: 'nombreExamen'},
		                                                {name: 'tipoExamen'},
		                                                {name:'noExamen'},
                                                        {name: 'calificacion'},
                                                        {name: 'funcionRenderer'},
                                                        {name: 'calificacionMinima'},
                                                        {name: 'comp'},
                                                        {name: 'referencia'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesProgramaAcademicoV2.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreExamen', direction: 'ASC'},
                                                            groupField: 'nombreExamen',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='69';
                                        proxy.baseParams.idUsuario=gE("idUsuario").value;
                                        proxy.baseParams.idGrupo=nodoSel.attributes.idGrupo;
                                    }
                        )   
	alDatos.on('load',function(proxy)
    								{
                                    	if(pagoReferenciado!='')
                                        {
                                        	ejecutarGenerarPagoReferenciado(pagoReferenciado);
                                            pagoReferenciado='';
                                        }
                                    }
                        )                           
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	{
                                                                header:'',
                                                                width:30,
                                                                align:'left',
                                                                sortable:true,
                                                                dataIndex:'idReferencia',
                                                                renderer:function(val,meta,registro)
                                                                        {
                                                                            var cad='';
                                                                            if(registro.data.referencia!='')
                                                                                cad+='<a href="javascript:obtenerFormatoParaPagoReferenciado(\''+bE(registro.data.referencia)+'\')"><img src="../images/page_white_acrobat.png" title="Descargar formato de pago" alt="Descargar formato de pago"></a>';
                                                                             
                                                                             return cad;
                                                                        }
                                                            },
                                                            {
                                                                header:'Ex&aacute;men',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'nombreExamen'
                                                            },
                                                            {
                                                                header:'Calificaci&oacute;n',
                                                                width:120,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'calificacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        
                                                                        	
                                                                        	if(isNaN(val))
                                                                            	return val;
                                                                            
                                                                        	if(registro.data.funcionRenderer!="")
                                                                            {
                                                                            	
                                                                            	eval('var func='+registro.data.funcionRenderer+'('+val+');');
                                                                               
                                                                               	var color='030';
                                                                                var valPonderado=parseFloat(val)/10;
                                                                                if(valPonderado<parseFloat(registro.data.calificacionMinima))
                                                                                	color='F00';
                                                                                return '<span style="color:#'+color+'"><b>'+func+'</b></span>';
                                                                                
	
                                                                            }
                                                                            return val;
                                                                        }
                                                            },
                                                            {
                                                                header:'',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'comp'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridCalificaciones',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                anchor:'100% 100%',
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                      
		                                                        
                                                        
        return 	tblGrid;	
}

function crearGridTramites()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idTramite'},
		                                                {name: 'idFormulario'},
		                                                {name:'idRegistro'},
		                                                {name:'fechaSolicitud', type:'date'},
                                                        {name: 'actor'},
                                                        {name: 'situacion'},
                                                        {name: 'descripcion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaSolicitud', direction: 'ASC'},
                                                            groupField: 'fechaSolicitud',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='87';
                                        proxy.baseParams.idAlmacen=gE('idAlmacen').value;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Tipo tr&aacute;mite',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'idFormulario'
                                                            },
                                                            {
                                                                header:'Fecha Solicitud',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'fechaSolicitud'
                                                            },
                                                            {
                                                                header:'Descripci&oacute;n',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'descripcion'
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n',
                                                                width:400,
                                                                sortable:true,
                                                                dataIndex:'situacion'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridTramites',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                anchor:'100% 100%',
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
	

	
}

function nodoClick(nodo)
{
	nodoSel=nodo;
    
}


function recargarDatos()
{
	var treePanel=gEx('tree-panel');
    gEx('btnFichaMateria').disable();
    gEx('btnVerCriterios').disable();

                    
	treePanel.getRootNode().reload();
    treePanel.expandAll();
    nodoSel=null;
    recargarTemario();
    gEx('gridMaterialDidactico').getStore().removeAll();
    gEx('gridCalificaciones').getStore().removeAll();
    gEx('gridSesiones').getStore().removeAll();
}

function recargarTemario()
{
	var temario= gEx('temario');
    temario.getRootNode().reload();
    temario.expandAll();
    nodoTemarioSel=null;
}

function crearArbolTemario()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'31'
                                                                   
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesProgramaAcademicoV2.php',
                                                    autoLoad:false,	
                                                    listeners:	{
                                                    				beforeload: function(treeloader, node, callback) 
                                                                    			{
                                                                                    treeloader.treeLoadingMask = new Ext.LoadMask(Ext.getBody(), {msg:"Cargando, por favor espere..."});
                                                                                    treeloader.treeLoadingMask.show();
                                                                                    return true;
                                                                                },
                                                                   load: 	function(treeloader, node, response) 
                                                                            {
                                                                                treeloader.treeLoadingMask.hide();
                                                                                return true;
                                                                            },
                                                                   loadexception: function(treeloader, node, response) 
                                                                                    {
                                                                                        treeloader.treeLoadingMask.hide();
                                                                                        return true;
                                                                                    },
                                                                            
                                                    			}
                                                                
                                                }
                                            )		
										
	cargadorArbol.on('beforeload',function(tree,nodo)
    								{
                                    	var idGrupo='-1';
                                        if(nodoSel!=null)
	                                        idGrupo=nodoSel.attributes.idGrupo;
                                    	this.baseParams.idGrupo=idGrupo;
                                        var noSesion="-1";
                                        var gridSesiones=gEx('gridSesiones');
                                        var fila=gridSesiones.getSelectionModel().getSelected();
                                        var panelMaterial=gEx('panelTemario');
                                        if((fila)&&(panelMaterial!=null))
                                        {
                                        	noSesion=fila.get('noSesion');
                                            panelMaterial.setTitle('Temario [Sesi&oacute;n: '+fila.get('noSesion')+', '+arrDias[fila.get('fechaSesion').getDay()]+' '+fila.get('fechaSesion').format('d/m/Y')+' ]');
                                        }
                                        else
                                        {
                                        	if(panelMaterial!=null)
	                                        	panelMaterial.setTitle('Temario');
                                        }
                                        this.baseParams.noSesion=noSesion;
                                        
                                    }
    				)										
		
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
															id:'temario',
															useArrows:true,
                                                            frame:false,
                                                            border:false,
															autoScroll: true,
															animate:true,
                                                            enableDD:true,
															root:raiz,
															loader: cargadorArbol,
															rootVisible:false
                                                           
														}
													)
	                                               
	arbolOpciones.on('click',funcClikArbolTemario);
    return arbolOpciones;							
}

function funcClikArbolTemario(nodo, evento)
{
	nodoTemarioSel=nodo;
   
	
}

function crearGridSesiones()
{
	
	 var alDatos= new Ext.data.JsonStore({
                                            
                                            totalProperty :'numReg',
                                            fields: [
                                            			{name: 'idSesion'},
                                                        {name: 'noSesion', type:'int'},
                                                        {name: 'fechaSesion',  type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'horario'},
                                                        {name: 'tipoSesion'},
                                                        {name: 'registroAsistencia'},
                                                        {name: 'comentarios'}
                                            		],
                                             proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesProgramaAcademicoV2.php'
                                                                                      
                                                                                  }

                                                                              ),
                                            sortInfo: {field: 'noSesion', direction: 'ASC'},
                                            autoLoad:true,
                                            root:'registros',
                                            remoteSort: false
                                        }
                                      );
    alDatos.setDefaultSort('noSesion', 'ASC');
    
	alDatos.on('beforeload',function(proxy)
    								{
                                    	var idGrupo='-1';
                                        if(nodoSel!=null)
	                                        idGrupo=nodoSel.attributes.idGrupo;
                                    	this.baseParams.idGrupo=idGrupo;
                                        this.baseParams.idUsuario=<?php echo $_SESSION["idUsr"]?>;
                                        this.baseParams.funcion=35;
                                        gEx('btnConfSesion').disable();
                                        gEx('btnModifSesion').disable();
                                    }
                        )
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});                             
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'No. sesi&oacute;n',
															width:90,
															sortable:true,
															dataIndex:'noSesion'
														},
                                                        {
															header:'D&iacute;a sesi&oacute;n',
															width:90,
															sortable:true,
                                                            renderer:function(val,meta,registro)
                                                                    {
                                                                    	return arrDias[registro.get('fechaSesion').getDay()];
                                                                    }
															
														},
                                                        {
															header:'Fecha sesi&oacute;n',
															width:110,
															sortable:true,
															dataIndex:'fechaSesion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val!=null)&&(val!=''))
                                                                        {
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                                    }
														},
														{
															header:'Horario',
															width:210,
															sortable:true,
															dataIndex:'horario'
                                                            
														},
														{
															header:'Tipo sesi&oacute;n',
															width:150,
															sortable:true,
															dataIndex:'tipoSesion',
                                                            renderer:function(val)
                                                            		{
                                                                    	
                                                                    	return formatearValorRenderer(arrSituacion,val);
                                                                    }
														},
                                                        {
															header:'Registr&oacute; asistencia',
															width:130,
															sortable:true,
                                                            align:'center',
															dataIndex:'registroAsistencia',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	var fechaHoy=new Date();
                                                                        if(fechaHoy>registro.get('fechaSesion'))
                                                                        {
                                                                        	switch(val)
                                                                            {
                                                                            	case '0':
                                                                                	return '<img src="../images/cross.png" height="13" width="13" />';
                                                                                break;
                                                                                case '1':
                                                                                		return '<img src="../images/icon_big_tick.gif" height="13" width="13"/>';
                                                                                break;
                                                                                default:
                                                                                	return 'Sin pase de lista';
                                                                                break;
                                                                            }
                                                                        }
                                                                        return '';
                                                                    }
                                                            
														}
                                                        
                                                        
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gridSesiones',
                                                            store:alDatos,
                                                            region:'center',
                                                            height:295,
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            sm:chkRow,
                                                            split:true,
                                                            title:'Sesiones',
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnConfSesion',
                                                                        	icon:'../images/page_process.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            hidden:true,
                                                                            text:'Configurar sesi&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la sesi&oacute;n que desea configurar');
                                                                                            return;
                                                                                        }
                                                                                    	mostrarVentanaConfiguracionSesion(fila);
                                                                                        
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	id:'btnModifSesion',
                                                                        	icon:'../images/page_white_edit.png',
                                                                            cls:'x-btn-text-icon',
                                                                            disabled:true,
                                                                            hidden:true,
                                                                            text:'Modificar sesi&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar la sesi&oacute;n que desea modificar');
                                                                                            return;
                                                                                        }
                                                                                    	mostrarVentanaConfiguracionSesion(fila);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		],                     
                                                            viewConfig: {
                                                                            forceFit:false,
                                                                            enableRowBody:true,
                                                                            getRowClass : formatearFila
                                                                        }
                                                        }
                                                    );
	var ejecutarDesSelection=false;                                                    
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
                                              {
	                                              	ejecutarDesSelection=true;
	                                              	var tamPagina=30;
                                              		gEx('btnConfSesion').disable();
                                                    gEx('btnModifSesion').disable();
                                                    if(registro!=null)
                                                    {
                                                        if(registro.get('tipoSesion')=='9')
                                                        {
                                                            gEx('btnConfSesion').enable();
                                                        }
                                                        else
                                                        {
                                                            if((registro.get('tipoSesion')=='10')||(registro.get('tipoSesion')=='11')||(registro.get('tipoSesion')=='12'))
                                                                 gEx('btnModifSesion').enable();
                                                        }
                                                    }
                                                    gEx('temario').getRootNode().reload();
                                                    gEx('temario').expandAll();
                                                    gEx('gridMaterialDidactico').getStore().load( {
                                                                                    url: '../paginasFunciones/funcionesPlanteles.php',
                                                                                    params:{
                                                                                                start:0,
                                                                                                limit:tamPagina,
                                                                                                idGrupo:nodoSel.attributes.idGrupo
                                                                                            }
                                                                                    }
                                                                                ) ; 
                                                    
                                              }
                             		)   
	tblGrid.getSelectionModel().on('beforerowselect',function(sm,fila,registro)
                                              {
                                              		if(sm.getSelected()!=null)
                                                    	ejecutarDesSelection=false;
                                                    
                                              }
                                    )                                    
	tblGrid.getSelectionModel().on('rowdeselect',function(sm,fila,registro)
                                              {
                                              		if(!ejecutarDesSelection)
                                                    {
                                                    	ejecutarDesSelection=true;
                                                    	return;
                                                    }
	                                              	var tamPagina=30;
                                                    gEx('temario').getRootNode().reload();
                                                    gEx('temario').expandAll();
                                              		gEx('btnConfSesion').disable();
                                                    gEx('btnModifSesion').disable();
                                                    gEx('gridMaterialDidactico').getStore().load( {
                                                                                    url: '../paginasFunciones/funcionesPlanteles.php',
                                                                                    params:{
                                                                                                start:0,
                                                                                                limit:tamPagina,
                                                                                                idGrupo:nodoSel.attributes.idGrupo
                                                                                            }
                                                                                    }
                                                                                ) ; 
                                                    
                                              }
                             		)                                                                              
	return 	tblGrid;		
}



function formatearFila(record, rowIndex, p, ds) 
{
	var xf = Ext.util.Format;
    if(record.get('comentarios')!='')
	    p.body = '<br><p style="margin-left: 20em;margin-right: 3em;text-align:justify"><span class="copyrigthSinPaddingNegro"><table><tr><td><span class="letraRojaSubrayada8">Comentarios:</span>&nbsp;&nbsp;</td><td align="left"  ><span class="letraExt">'+record.get('comentarios')+'</span></td></tr></table></span></p><br>';
    else
    	p.body = '<p style="margin-left: 10em;margin-right: 3em;text-align:justify"><span class="copyrigthSinPaddingNegro"><table><tr><td></td><td align="left"  ><span class="letraRojaSubrayada8"></span></td></tr></table></span></p>';
    return 'x-grid3-row-expanded';
}

function crearGridMaterialDidactico()
{
	
	var tamPagina=30;
	 var alDatos= new Ext.data.JsonStore({
                                            
                                            totalProperty :'numReg',
                                            fields: [
                                            			{name: 'idMaterialDidactico'},
                                                        {name: 'titulo'},
                                                        {name: 'fechaPublicacion',  type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'tipo'},
                                                        {name: 'descripcion'},
                                                        {name: 'tamano', type:'int'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomArchivo'}
                                            		],
                                             proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesPlanteles.php',
                                                                                      
                                                                                      
                                                                                  }

                                                                              ),
                                            sortInfo: {field: 'fechaPublicacion', direction: 'ASC'},
                                            autoLoad:false,
                                            root:'registros',
                                            remoteSort: false
                                        }
                                      );
    alDatos.setDefaultSort('fechaPublicacion', 'ASC');
    
	alDatos.on('beforeload',function(proxy)
    								{
                                    	var idGrpo='-1';
                                        var noSesion="-1";
                                        var gridSesiones=gEx('gridSesiones');
                                        var fila=gridSesiones.getSelectionModel().getSelected();
                                        var panelMaterial=gEx('panelMaterial');
                                        if((fila)&&(panelMaterial!=null))
                                        {
                                        	noSesion=fila.get('noSesion');
                                            panelMaterial.setTitle('Material did&aacute;ctico [Sesi&oacute;n: '+fila.get('noSesion')+', '+arrDias[fila.get('fechaSesion').getDay()]+' '+fila.get('fechaSesion').format('d/m/Y')+' ]');
                                        }
                                        else
                                        {
                                        	if(panelMaterial!=null)
	                                        	panelMaterial.setTitle('Material did&aacute;ctico');
                                        }
                                        if(nodoSel!=null)
                                            idGrpo=nodoSel.attributes.idGrupo;
                                    	this.baseParams.funcion=20;
                                    	this.baseParams.idGrupo=idGrpo;
                                        this.baseParams.noSesion=noSesion;
                                    }
                        )
	 var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
     var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'titulo'},
                                                                        {type: 'date', dataIndex: 'fechaPublicacion'},
                                                                        {type: 'list', dataIndex: 'idCategoria',options:arrCategorias,phpMode:true}
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                      
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});                             
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'',
															width:70,
                                                            align:'left',
															sortable:true,
                                                            dataIndex:'idMaterialDidactico',
															renderer:function (val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        var arrArchivo=registro.get('nomArchivo').split('.');
                                                                        var pos=existeValorMatriz(arrVisores,arrArchivo[1]);
                                                                        if(pos!=-1)
                                                                        {
                                                                        	var cadObj='[{"url":"'+arrVisores[pos][1]+'","ancho":'+arrVisores[pos][2]+',"alto":'+
                                                                            	arrVisores[pos][3]+',"titulo":"'+registro.get('titulo')+'","params":[[\'i\',\''+bE(registro.get('idMaterialDidactico'))+'\'],[\'cPagina\',\'sFrm=true\']]}]';
                                                                            comp='&nbsp;&nbsp;&nbsp;<a href="javascript:verMaterial(\''+bE(cadObj)+'\')"><img src="../images/control_play_blue.png" title="Ver en l&iacute;nea" alt="Ver en l&iacute;nea"></a>';
	
                                                                        }
                                                                    	return '&nbsp;<a href="../media/obtenerMaterialDidactico.php?i='+bE(val)+'"><img src="../images/download.png" title="Descargar material did&aacute;ctico" alt="Descargar material did&aacute;ctico"></a>'+comp;
                                                                    }
                                                            
														},
														{
															header:'T&iacute;tulo',
															width:335,
															sortable:true,
															dataIndex:'titulo'
														},
                                                        {
															header:'Tipo',
															width:160,
															sortable:true,
															dataIndex:'tipo',
                                                            hidden:true,
														},
														{
															header:'Tamano (Kb)',
															width:100,
                                                            align:'right',
															sortable:true,
                                                            dataIndex:'tamano',
															renderer:function (val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000');
                                                                    }
                                                            
														},
														{
															header:'Categor&iacute;a',
															width:160,
															sortable:true,
															dataIndex:'idCategoria',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCategorias,val);
                                                                    }
														},
                                                       
                                                        {
															header:'Fecha de publicaci&oacute;n',
															width:130,
															sortable:true,
                                                            dataIndex:'fechaPublicacion',
															renderer:function(val)
                                                            		{
                                                                    	if(val)
	                                                                    	return val.format('d/m/Y');
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            anchor:'100% 100%',
                                                            id:'gridMaterialDidactico',
                                                            store:alDatos,
                                                            frame:true,
                                                            border:true,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            sm:chkRow,
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            viewConfig: {
                                                                            forceFit:false,
                                                                            enableRowBody:true,
                                                                            getRowClass : formatearFilaMaterial
                                                                        }
                                                            
                                                        }
                                                    );
	
    var idGrpo='-1';
    if(nodoSel!=null)
        idGrpo=nodoSel.attributes.idGrupo;	                                        
	tblGrid.getStore().load( {
    							url: '../paginasFunciones/funcionesPlanteles.php',
                              	params:{
                                        start:0,
                                        limit:tamPagina,
                                        idGrupo:idGrpo
                                    	}
                                }
                            ) ;                                   
	return 	tblGrid;	
}

function formatearFilaMaterial(record, rowIndex, p, ds) 
{
	var xf = Ext.util.Format;
    var descripcion=record.data.descripcion;
    if(descripcion=='')
    	descripcion='Sin descripci&oacute;n';
    p.body = '<br /><p style="margin-left: 10em;margin-right: 3em;text-align:justify"><table><tr><td><span class="letraRojaSubrayada8">Descripci&oacute;n: </span>&nbsp;&nbsp;</td><td  style="text-align:justify"><span class="copyrigthSinPadding">'+descripcion+'</span></td></tr></table></p><br /><br />';
    return 'x-grid3-row-expanded';
}

function verMaterial(c)
{
	var conf=eval(bD(c))[0];
    abrirVentanaFancy(conf);
}

function crearGridMaterialDidacticoSesion(fila)
{
	var tamPagina=30;
	 var alDatos= new Ext.data.JsonStore({
                                            
                                            totalProperty :'numReg',
                                            fields: [
                                            			{name: 'idMaterialDidactico'},
                                                        {name: 'titulo'},
                                                        {name: 'fechaSubida',  type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'tipo'},
                                                        {name: 'descripcion'},
                                                        {name: 'tamano', type:'int'},
                                                        {name: 'idCategoria'},
                                                        {name: 'nomArchivo'}
                                            		],
                                             proxy : new Ext.data.HttpProxy	(
                                                                                  {
                                                                                      url: '../paginasFunciones/funcionesPlanteles.php',
                                                                                      
                                                                                      
                                                                                  }

                                                                              ),
                                            sortInfo: {field: 'fechaSubida', direction: 'ASC'},
                                            autoLoad:false,
                                            root:'registros',
                                            remoteSort: false
                                        }
                                      );
    alDatos.setDefaultSort('fechaSubida', 'ASC');
    
	alDatos.on('beforeload',function(proxy)
    								{
                                    	this.baseParams.funcion=18;
                                    	this.baseParams.idGrupo=nodoSel.attributes.idGrupo;
                                        this.baseParams.noSesion=fila.get('noSesion');
                                    }
                        )
	 var paginador=	new Ext.PagingToolbar	(
                                                {
                                                      pageSize: tamPagina,
                                                      store: alDatos,
                                                      displayInfo: true,
                                                      disabled:false
                                                  }
                                               )  
     var filters = new Ext.ux.grid.GridFilters	(

    												{

                                                    	filters:	[ 	
                                                        				{type: 'string', dataIndex: 'titulo'},
                                                                        {type: 'date', dataIndex: 'fechaSubida'},
                                                                        {type: 'list', dataIndex: 'idCategoria',options:arrCategorias,phpMode:true}
                                                        				
                                                                    ]

                                                    }

                                                ); 
                                                                      
    var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});                             
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
                                                        {
															header:'',
															width:70,
                                                            align:'left',
															sortable:true,
                                                            dataIndex:'idMaterialDidactico',
															renderer:function (val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        var arrArchivo=registro.get('nomArchivo').split('.');
                                                                        var pos=existeValorMatriz(arrVisores,arrArchivo[1]);
                                                                        if(pos!=-1)
                                                                        {
                                                                        	var cadObj='[{"url":"'+arrVisores[pos][1]+'","ancho":'+arrVisores[pos][2]+',"alto":'+
                                                                            	arrVisores[pos][3]+',"titulo":"'+registro.get('titulo')+'","params":[[\'i\',\''+bE(registro.get('idMaterialDidactico'))+'\'],[\'cPagina\',\'sFrm=true\']]}]';
                                                                            comp='&nbsp;&nbsp;&nbsp;<a href="javascript:verMaterial(\''+bE(cadObj)+'\')"><img src="../images/control_play_blue.png" title="Ver en l&iacute;nea" alt="Ver en l&iacute;nea"></a>';
	
                                                                        }
                                                                    	return '&nbsp;<a href="../media/obtenerMaterialDidactico.php?i='+bE(val)+'"><img src="../images/download.png" title="Descargar material did&aacute;ctico" alt="Descargar material did&aacute;ctico"></a>'+comp;
                                                                    }
                                                            
														},
														{
															header:'T&iacute;tulo',
															width:335,
															sortable:true,
															dataIndex:'titulo'
														},
                                                        
														{
															header:'Tamano (Kb)',
															width:100,
                                                            align:'right',
															sortable:true,
                                                            dataIndex:'tamano',
															renderer:function (val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0,000');
                                                                    }
                                                            
														},
														{
															header:'Categor&iacute;a',
															width:160,
															sortable:true,
															dataIndex:'idCategoria',
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrCategorias,val);
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            anchor:'100% 100%',
                                                            id:'gridMaterialSesion',
                                                            store:alDatos,
                                                            frame:true,
                                                            border:true,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,
                                                            loadMask:true,
                                                            sm:chkRow,
                                                            x:0,
                                                            y:40,
                                                            bbar:[paginador],
                                                            plugins:[filters],
                                                            viewConfig: {
                                                                            forceFit:false,
                                                                            enableRowBody:true,
                                                                            getRowClass : formatearFilaMaterial
                                                                        },
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar material did&aacute;ctico',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaMaterialDidactico(tblGrid.getStore());
                                                                                    }
                                                                            
                                                                        },
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover material did&aacute;ctico',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(fila==null)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el material did&aacute;ctico que desea remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(fila);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el material did&aacute;ctico selecionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                            		]
                                                        }
                                                    );
	tblGrid.getSelectionModel().on('rowselect',function(sm,fila,registro)
                                              {
                                                  //if(tblGrid.getSelectionModel().getSelections().length==1)
                                                    // gEx('gridAlumnos').getStore().load({params:{funcion:6,idGrupo:registro.get('idGrupo')}});        
                                              }
                             		)                                            
	tblGrid.getStore().load( {
    							url: '../paginasFunciones/funcionesPlanteles.php',
                              	params:{
                                        start:0,
                                        limit:tamPagina
                                    	}
                                }
                            ) ;                                   
	return 	tblGrid;		
}

function verFichaMateria()
{
	var obj={};
    obj.url='../planteles/materia.php';
    obj.titulo=nodoSel.text;
    obj.ancho=840;
    obj.alto=450;
    obj.params=[['idAliasClaveMateria',nodoSel.attributes.idAliasClaveMateria],['cPagina','sFrm=true'],['sF','true']];
    abrirVentanaFancy(obj);
}


function verCriteriosEvaluacion()
{
	var arrParam=[['idMateria',nodoSel.attributes.idMateria],['cPagina','sFrm=true'],['sL','true']];
    var conf={};
    conf.ancho=700;
    conf.modal=false;
    conf.alto=450;
    conf.titulo='Criterios de evaluación';
    conf.url='../Profesores/criteriosDeEvaluacion.php';
    conf.params=arrParam;
    abrirVentanaFancy(conf);
}

function mostrarVentanaBeca()
{
	var gBeca=gridBecas();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gBeca
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Seleccione la Beca cuya solicitud desea realizar',
										width: 500,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function gridBecas()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idTipoBeca'},
		                                                {name: 'planBeca'},
                                                        {name: 'nombreBeca'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreBeca', direction: 'ASC'},
                                                            groupField: 'nombreBeca',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='114';
                                       
                                    }
                        )   
       var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Tipo de Beca',
                                                                width:380,
                                                                sortable:true,
                                                                dataIndex:'planBeca'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridTipoBeca',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                height:300,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                sm:chkRow,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function mostrarVentanaExamen()
{
	var gExamenes=gridExamenes();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														gExamenes
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Seleccione El tipo de Ex&aacute;men cuya solicitud desea realizar',
										width: 500,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function gridExamenes()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idTipoExamen'},
                                                        {name: 'nombreExamen'},
                                                        {name: 'noExamen'},
                                                        {name: 'calificacion'},
                                                        
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreExamen', direction: 'ASC'},
                                                            groupField: 'nombreBeca',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='114';
                                       
                                    }
                        )   
       var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            {
                                                                header:'Ex&aacute;men',
                                                                width:380,
                                                                sortable:true,
                                                                dataIndex:'nombreExamen'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridTipoExamen',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                height:300,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                sm:chkRow,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function regresar1Pagina()
{
	gEx('gridCalificaciones').getStore().reload();
}

function regresar2Pagina()
{
	gEx('gridCalificaciones').getStore().reload();
}

function recargarContenedorCentral()
{
	gEx('gridCalificaciones').getStore().reload();
    
}

function regresar1PaginaContenedor()
{
	gEx('gridCalificaciones').getStore().reload();
}

function regresarPagina2Contenedor()
{
	gEx('gridCalificaciones').getStore().reload();
}

function regresarContenedorCentral()
{
	gEx('gridCalificaciones').getStore().reload();
}

function formateoEstandar(val)
{	
	if(val=='N/E')
    	return val;
	return Ext.util.Format.number(val,"0.00");
}

function verDetallesSinDerecho(tE,aDetalles)
{
	var gridCondiciones=crearGridCondiciones(eval(bD(aDetalles)));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'<b>Tipo de evaluaci&oacute;n:</b>&nbsp;&nbsp;<span style="color:#900"><b>'+bD(tE)+'</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'A continuaci&oacute;n se describe los criterios por los cuales no se cuenta con el derecho a solicitar la evaluaci&oacute;n:'
                                                        },
                                                        gridCondiciones
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Sin derecho a soliciar evaluaci&oacute;n',
										width: 580,
										height:380,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridCondiciones(a)
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'condicion'},
                                                                {name: 'situacion'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(a);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														{
															header:'Condici&oacute;n',
															width:300,
															sortable:true,
															dataIndex:'condicion'
														},
														{
															header:'Situaci&oacute;n',
															width:120,
															sortable:true,
                                                            align:'center',
															dataIndex:'situacion',
                                                            renderer:function(val)
                                                            		{
                                                                    	switch(val)
                                                                        {
                                                                        	case '0':
                                                                            	return '<img src="../images/cross.png" width="13" height="13" />';
                                                                            break;
                                                                            case '1':
                                                                            	return '<img src="../images/icon_big_tick.gif" width="13" height="13" />';
                                                                            break;
                                                                        }
                                                                    
                                                                    }
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            y:70,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            
                                                            columnLines : true,
                                                            height:200,
                                                            width:550
                                                            
                                                        }
                                                    );
	return 	tblGrid;	
}


function abrirSolicitudExamen(iF,iR,a,param,param2)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",a],['dComp',bE(accion)],['actorInicio',1],['eliminarEspacios','1'],['eJs',bE('return;')],['accionCancelar','window.parent.cerrarVentanaFancy()']];
    
    if(param)
    {
	    var aParam=eval(bD(param));
        var x;
        var pos;
        for(x=0;x<aParam.length;x++)
        {
        	pos=existeValorMatriz(arrDatos,aParam[x][0]);
        	if(pos==-1)
	        	arrDatos.push([aParam[x][0],aParam[x][1]]);
            else
            	arrDatos[pos][1]=aParam[x][1];
        }
    }
    if(param2)
    {
	    var aParam=eval(bD(param2));
        var x;
        var pos;
        for(x=0;x<aParam.length;x++)
        {
        	pos=existeValorMatriz(arrDatos,aParam[x][0]);
        	if(pos==-1)
	        	arrDatos.push([aParam[x][0],aParam[x][1]]);
            else
            	arrDatos[pos][1]=aParam[x][1];
        }
    }
    
    var obj={};
    
    if(bD(iR)=='-1')
        obj.url="../modeloPerfiles/registroFormulario.php";
    else
    	obj.url="../modeloPerfiles/verFichaFormulario.php";
    obj.params=arrDatos;
    obj.ancho=720;
    obj.alto=380;
    
    abrirVentanaFancy(obj);
    
    
    
}

function mostrarPagoReferencia(ref)
{
	pagoReferenciado=ref;
	recargarContenedorCentral();
    
}

function ejecutarGenerarPagoReferenciado(ref)
{
	cerrarVentanaFancy();
	generarDocumentoPagoReferenciado(ref,'iFrame');
    
}