<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var ignorarRecarga=false;

Ext.onReady(inicializar);

function inicializar()
{
	crearGridAdeudos();
    crearGridAdeudosOtros();
}

function crearGridAdeudos()
{
	var dsDatos=eval(bD(gE('arrRegistros').value));
    var maxNumPago=parseInt(gE('maxNumPago').value);
    var arrCampos=	[
                        {name: 'idMovimiento'},
                        {name:'idReferencia'}, 
                        {name:'idConcepto'},
                        {name:'situacion'},
                        {name: 'fechaPago', type:'date', dateFormat:'Y-m-d'},
                        {name: 'fechaVencimiento', type:'date', dateFormat:'Y-m-d'},
                        {name:'nombreConcepto'},
                        {name:'montoPagado'}
                    ]
    var arrColumnas= [
                        {
                        	header:'',
                            width:20,
                            align:'center',
                            sortable:true,
                            dataIndex:'idReferencia',
                            renderer:function(val,meta,registro)
                            		{
                                    	var cad='';
                                        
                                        if((registro.data.situacion=='0')||(registro.data.situacion=='2'))
                                        	cad+='<a href="javascript:obtenerFormatoParaPagoReferenciado(\''+bE(registro.data.idReferencia)+'\')"><img src="../images/page_white_acrobat.png"></a>';
                                         
                                         return cad;
                                    }
                        },
                        {
                            header:'Concepto',
                            width:280,
                            sortable:true,
                            dataIndex:'nombreConcepto',
                            renderer:function(val,meta,registro)
                            		{
                                    	
                                         return val;
                                    }
                        },
                        {
                            header:'Fecha de Vencimiento',
                            width:120,
                            sortable:true,
                            css:'text-align:right !important;',
                            dataIndex:'fechaVencimiento',
                            renderer:function(val,meta,registro)
                                    {
                                    	var comp='';
                                        
                                       var idSituacion=registro.data.situacionSolExtension;
                                       if(idSituacion=='')
                                       		idSituacion=0;
                                        else
                                        	idSituacion=parseInt(idSituacion);
                                        switch(idSituacion)
                                        {
                                        	case 0:
                                            	comp='<a href="javascript:mostrarSolicitudPago(\''+bE(registro.data.idMovimiento)+'\')"><img  src="../images/calendar_edit.jpg" title="Solicitar extensi&oacute;n de pago" alt="Solicitar extensi&oacute;n de pago"></a>&nbsp;&nbsp;';
                                            break;
                                            case 1:
                                            	comp='<a href="javascript:modificarSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                	'<img  src="../images/exclamation.png" title="Existe una solicitud de extensi&oacute;n de pago que no ha sido enviada a autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                    ' alt="Existe una solicitud de extensi&oacute;n de pago que no ha sido enviada a autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                            break;
                                            case 2:
                                            	comp='<a href="javascript:verSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                	'<img  src="../images/exclamation.png" title="Existe una solicitud de extensi&oacute;n de pago en espera de autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                    ' alt="Existe una solicitud de extensi&oacute;n de pago en espera de autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                            break;
                                            
                                        }
                                        if(val)
                                            return comp+val.format('d/m/Y');
                                    }
                        },
                        {
                            header:'Situaci&oacute;n',
                            width:70,
                            sortable:true,
                            dataIndex:'situacion',
                            renderer:function(val,meta,registro)
                            	{
                                	var  comp='';
                                    var idSituacion=registro.data.situacionSolExtension;
                                     if(idSituacion=='')
                                          idSituacion=0;
                                      else
                                          idSituacion=parseInt(idSituacion);
                                      switch(idSituacion)
                                      {
                                          
                                          case 3:
                                              comp='<a href="javascript:verSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                  '<img  src="../images/exclamation.png" title="La fecha de vencimiento fue ampliada debido a una solicitud de extensi&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                  ' alt="La fecha de vencimiento fue ampliada debido a una solicitud de extensi&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                          break;
                                          
                                      }
                                	switch(val)
                                    {
                                    	case '0':
                                        	return '<img src="../images/control_pause.png">'+comp;
                                        break;
                                        case '1':
                                        	return '<img src="../images/icon_big_tick.gif">'+comp;
                                        break;
                                        case '2':
                                        	return '<img src="../images/cancel_round.png">'+comp;
                                        break;
                                    }
                                }
                        }
                        
                    ]
                    
	var x;
    var etPago='';
    
    for(x=0;x<maxNumPago;x++)
    {
    	etPago='Pago con<br>descuento';
        if(x>0)
        	etPago='Pago normal';
    	arrCampos.push({name: 'pago_'+x});
        arrCampos.push({name: 'et_'+x});
        columna=	{
        				header:etPago,
                        width:100,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'pago_'+x,
                        summaryType:'sum',
                        css:'text-align:right !important;',
                        renderer:function(val,meta,registro)
                                    {
                                    	if(val=='')
                                        	return '';
                                    	var cadFinal=Ext.util.Format.usMoney(val);
                                        return '<span style="color:#030; ">'+cadFinal+'</span>';
                                    }
        			};
        arrColumnas.push(columna);
        columna=	{
        				header:'Vigencia Pago',
                        width:170,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'et_'+x,
                        css:'text-align:left !important;'
        			};
        arrColumnas.push(columna);
    }   
    arrCampos.push({name: 'situacionSolExtension'});
    arrCampos.push({name: 'idRegSolExtension'});
    arrColumnas.push(	{
                            header:'Fecha de pago',
                            width:110,
                            sortable:true,
                            dataIndex:'fechaPago',
                            renderer:function(val)
                                    {
                                        if(val)
                                            return val.format('d/m/Y');
                                    }
                        })
	arrColumnas.push(	{
                            header:'Monto pagado',
                            width:100,
                            sortable:true,
                            dataIndex:'montoPagado',
                            summaryType:'sum',
                            renderer:function(val)
                                    {
                                        return Ext.util.Format.usMoney(val);
                                    }
                        })                        
                     
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	arrCampos
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	arrColumnas
												);
    var summary = new Ext.ux.grid.GridSummary();                                              
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            renderTo:'tblPagos',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:750,
                                                            plugins:[summary],
                                                            sm:chkRow
                                                            /*tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]*/
                                                        }
                                                    );
	return 	tblGrid;	
}

function crearGridAdeudosOtros()
{
	var dsDatos=eval(bD(gE('arrRegistrosOtros').value));
    var maxNumPago=parseInt(gE('maxColumnaOtros').value);
    var arrCampos=	[
                        {name: 'idMovimiento'},
                        {name:'idReferencia'}, 
                        {name:'idConcepto'},
                        {name:'situacion'},
                        {name: 'fechaPago', type:'date', dateFormat:'Y-m-d'},
                        {name: 'fechaVencimiento', type:'date', dateFormat:'Y-m-d'},
                        {name:'nombreConcepto'},
                        {name:'montoPagado'}
                    ]
    var arrColumnas= [
                        {
                        	header:'',
                            width:20,
                            align:'center',
                            sortable:true,
                            dataIndex:'idReferencia',
                            renderer:function(val,meta,registro)
                            		{
                                    	var cad='';
                                        if(registro.data.situacion=='0')
                                        	cad+='<a href="javascript:obtenerFormatoParaPagoReferenciado(\''+bE(registro.data.idReferencia)+'\')"><img src="../images/page_white_acrobat.png"></a>';
                                         
                                         return cad;
                                    }
                        },
                        {
                            header:'Concepto',
                            width:280,
                            sortable:true,
                            dataIndex:'nombreConcepto',
                            renderer:function(val,meta,registro)
                            		{
                                    	
                                         return val;
                                    }
                        },
                        {
                            header:'Fecha de Vencimiento',
                            width:120,
                            sortable:true,
                            css:'text-align:right !important;',
                            dataIndex:'fechaVencimiento',
                            renderer:function(val,meta,registro)
                                    {
                                    	var comp='';
                                       var idSituacion=registro.data.situacionSolExtension;
                                       
                                       if(idSituacion=='')
                                       		idSituacion=0;
                                        else
                                        	idSituacion=parseInt(idSituacion);
                                        switch(idSituacion)
                                        {
                                        	case 0:
                                            	comp='<a href="javascript:mostrarSolicitudPago(\''+bE(registro.data.idMovimiento)+'\')"><img  src="../images/calendar_edit.jpg" title="Solicitar extensi&oacute;n de pago" alt="Solicitar extensi&oacute;n de pago"></a>&nbsp;&nbsp;';
                                            break;
                                            case 1:
                                            	comp='<a href="javascript:modificarSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                	'<img  src="../images/exclamation.png" title="Existe una solicitud de extensi&oacute;n de pago que no ha sido enviada a autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                    ' alt="Existe una solicitud de extensi&oacute;n de pago que no ha sido enviada a autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                            break;
                                            case 2:
                                            	comp='<a href="javascript:verSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                	'<img  src="../images/exclamation.png" title="Existe una solicitud de extensi&oacute;n de pago en espera de autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                    ' alt="Existe una solicitud de extensi&oacute;n de pago en espera de autorizaci&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                            break;
                                            
                                        }
                                        if(val)
                                            return comp+val.format('d/m/Y');
                                    }
                        },
                        {
                            header:'Situaci&oacute;n',
                            width:70,
                            sortable:true,
                            dataIndex:'situacion',
                            renderer:function(val,meta,registro)
                            	{
                                	var  comp='';
                                    var idSituacion=registro.data.situacionSolExtension;
                                     if(idSituacion=='')
                                          idSituacion=0;
                                      else
                                          idSituacion=parseInt(idSituacion);
                                      switch(idSituacion)
                                      {
                                          
                                          case 3:
                                              comp='<a href="javascript:verSolicitudPago(\''+bE(registro.data.idRegSolExtension)+'\')">'+
                                                  '<img  src="../images/exclamation.png" title="La fecha de vencimiento fue ampliada debido a una solicitud de extensi&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"'+
                                                  ' alt="La fecha de vencimiento fue ampliada debido a una solicitud de extensi&oacute;n para observar dicha solicitud d&eacute; click AQU&Iacute;"></a>&nbsp;&nbsp;';
                                          break;
                                          
                                      }
                                	switch(val)
                                    {
                                    	case '0':
                                        	return '<img src="../images/control_pause.png">'+comp;
                                        break;
                                        case '1':
                                        	return '<img src="../images/icon_big_tick.gif">'+comp;
                                        break;
                                        case '2':
                                        	return '<img src="../images/cancel_round.png">'+comp;
                                        break;
                                    }
                                }
                        }
                        
                    ]
                    
	var x;
    for(x=0;x<maxNumPago;x++)
    {
    	arrCampos.push({name: 'pago_'+x});
        arrCampos.push({name: 'et_'+x});
        columna=	{
        				header:'Monto Pago',
                        width:100,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'pago_'+x,
                        summaryType:'sum',
                        css:'text-align:right !important;',
                        renderer:function(val,meta,registro)
                                    {
                                    	var cadFinal=Ext.util.Format.usMoney(val);
                                        return '<span style="color:#030; ">'+cadFinal+'</span>';
                                    }
        			};
        arrColumnas.push(columna);
        columna=	{
        				header:'Vigencia Pago',
                        width:170,
                        sortable:false,
                        menuDisabled :true,
                        dataIndex:'et_'+x,
                        css:'text-align:left !important;'
        			};
        arrColumnas.push(columna);
    }   
    arrCampos.push({name: 'situacionSolExtension'});
    arrCampos.push({name: 'idRegSolExtension'});
    arrColumnas.push(	{
                            header:'Fecha de pago',
                            width:110,
                            sortable:true,
                            dataIndex:'fechaPago',
                            renderer:function(val)
                                    {
                                        if(val)
                                            return val.format('d/m/Y');
                                    }
                        })
	arrColumnas.push(	{
                            header:'Monto pagado',
                            width:100,
                            sortable:true,
                            dataIndex:'montoPagado',
                            summaryType:'sum',
                            renderer:function(val)
                                    {
                                        return Ext.util.Format.usMoney(val);
                                    }
                        })                        
                     
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	arrCampos
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	arrColumnas
												);
    var summary = new Ext.ux.grid.GridSummary();                                              
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:false,
                                                            renderTo:'tblPagosOtros',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:750,
                                                            plugins:[summary],
                                                            sm:chkRow
                                                            
                                                        }
                                                    );
	return 	tblGrid;	
}

function mostrarSolicitudPago(iM)
{
	var arrDatos=[["idFormulario",900],["idRegistro",-1],["idReferencia",bD(iM)],["actor",bE(182)],['dComp',bE('agregar')],['actorInicio','1']];
    var ventanaAbierta=window.open('',"vAuxiliar2", "toolbar=no,directories=no,menubar=no,status=no,scrollbars=yes,fullscreen=yes");
    enviarFormularioDatosV("../modeloPerfiles/vistaDTD.php",arrDatos,'POST','vAuxiliar2');	
}

function modificarSolicitudPago(iR)
{
	verRegistroProyecto(iR,bE(713),bE(900));
}

function verSolicitudPago(iR)
{
	verRegistroProyecto(iR,bE(0),bE(900));
}

function regresar1Pagina()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresar2Pagina()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function recargarContenedorCentral()
{
	if(!ignorarRecarga)
		recargarPagina();
    
}

function regresar1PaginaContenedor()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresarPagina2Contenedor()
{
	if(!ignorarRecarga)
		recargarPagina();
}

function regresarContenedorCentral()
{
	if(!ignorarRecarga)
		recargarPagina();
}
