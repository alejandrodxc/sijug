<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	$( "#accordion" ).accordion({ active: false, collapsible:true, heightStyle:'content' });
	var aPlanesEstudio=eval(bD(gE('aPlanesEstudio').value));
	var cmbPlan=crearComboExtFormulario('cmbPlanEstudio','hPlanEstudio',aPlanesEstudio);
    cmbPlan.setWidth(400);
    cmbPlan.on('select',obtenerDatosAlumno);
    cmbPlan.setValue(gE('hPlanEstudio').value);
    if(gE('hPlanEstudio').value!='')
    {

    	dispararEventoSelectCombo(cmbPlan.id);
        
    }
    
   
    
    
}

function obtenerDatosAlumno(cmb,registro)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var obj=eval('['+arrResp[1]+']')[0];
            gE('lblAnio').innerHTML=obj.anioIngreso;
            gE('lblGrado').innerHTML=obj.gradoActual;
            gE('lblSituacion').innerHTML=obj.situacionActual;
            gE('idCiclo').value=obj.idCiclo;
            gE('idPeriodo').value=obj.idPeriodo;
            cargarDatosAdeudos();
		    cargarDatosOpciones();
            cargarDatosSituacionTramites();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_UGM_2.php',funcAjax, 'POST','funcion=16&idUsuario='+gE('idUsuario').value+'&idInstancia='+registro.data.id,true);
}

function cargarDatosAdeudos()
{
	 gE('frmAdeudos').src='../Alumno/tblAdeudos.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';

}

function cargarDatosOpciones()
{
     gE('frmOpciones').src='../portal/visorOpcionesEntidadBase.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';

}

function cargarDatosSituacionTramites()
{
     gE('frmTramites').src='../portal/visorTramites.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';

}

function recargarContenedorCentral()
{
	gE('frmAdeudos').src='../Alumno/tblAdeudos.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';
    gE('frmTramites').src='../portal/visorTramites.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';
    gE('frmOpciones').src='../portal/visorOpcionesEntidadBase.php?idUsuarioRegistro='+gE('idUsuario').value+'&idInstanciaPlan='+gEx('f_cmbPlanEstudio').getValue()+'&cPagina=sFrm=true';
    
}

function obtenerPagoReferenciado(iR)
{

	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var ref=arrResp[1];
			var arrParam=[['referencia',ref]];
		    enviarFormularioDatos('../reportes/generarPagoReferenciado.php',arrParam,'POST','iImprimir');
            cargarDatosSituacionTramites();
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesProyectos.php',funcAjax, 'POST','funcion=338&idConcepto=6&idFormulario=910&idRegistro='+iR,true);

	
}