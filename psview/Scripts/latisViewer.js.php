var maxArchivo;
var archivoActual=1;

Ext.onReady(inicializar);


function inicializar()
{
	var x=1;
    var nArchivos=gE('nArchivos').value;                        
	if(nArchivos!='')                        
    {
    	maxArchivo=parseInt(nArchivos);
    }
    var dsDatos=[];
    for(x=1;x<=maxArchivo;x++)
    	dsDatos.push([x]);

	var nameRecord = Ext.data.Record.create(	[
                                              		{
                                                		name: 'numImagen'
                                              		}
                                                 ]
                                            );
        
	var arrayReader = new Ext.data.ArrayReader({}, nameRecord);   
    var memoryProxy = new Ext.data.MemoryProxy(dsDatos);
     
	var alDatos=	new Ext.data.Store	(
                                            {
                                            	reader: arrayReader,
                                                proxy: memoryProxy
                                                
                                            }
                                        );
	
    alDatos.load();

    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title:'<span class="letraVino14N">'+bD(gE('nombreArchivo').value)+'</span> <span class="negrita14">[Vista previa]</span>',
                                                tbar:	[
                                                        	  {
                                                                icon:'../images/download.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Descargar documento',
                                                                handler:function()
                                                                        {
                                                                            var arrParam=[['nomArchivo',gE('nombreArchivo').value]];   
                                                                            enviarFormularioDatos("../media/descargarDocumentoPDFVisor.php",arrParam);
                                                                        }
                                                                
                                                            }
                                                        ],
                                                bbar:	[
                                                          
                                                            new Ext.PagingToolbar({id:'pagingToolBar',buttonAlign :'right',pageSize: 1,store:alDatos,listeners: { change:tabChange}})
                                                            
                                                        ],
                                                items:	[
                                                             {
                                                                  region:'center',
                                                                  id:'tblCenter',
                                                                  autoScroll: true,
                                                                  xtype:'iframepanel',
                                                                  deferredRender: false,
                                                                  border:false,
                                                                  listeners:{
                                                                                  documentloaded:function(el)
                                                                                              {
                                                                                                 
                                                                                              } 
                                                                              },
                                                                  loadMask:	{
                                                                                  msg:'Cargando'
                                                                              }
                                                              }
                                                        ]
                                            }
                                         ]
                            }
                        )  
	
    if(maxArchivo>0)
      	mostrarImagen(1);
        

	gEx('pagingToolBar').refresh.hide();
    
}

function tabChange(tab,numPagina)
{
	mostrarImagen(numPagina.activePage);
}

function mostrarImagen(nImagen)
{
	gEx('tblCenter').load	(
    							 {
                                      url:'../psview/visorImagen.php',
                                      scripts:true,
                                      params:	{
                                                  nombreImg:bE(gE('directorioImg').value+'/tmp'+nImagen+'.gif')
                                                  
                                                  
                                               }
                                }
    						);
}