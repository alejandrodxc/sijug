<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{

    new Ext.Panel	(
    					{
                            xtype:'panel',
                            renderTo:'tblCuerpo',
                            layout:'border',
                            width:'100%',
                            height:obtenerDimensionesNavegador()[0]-80,
                            
                            tbar:	[
                                        {
                                            icon:'../images/page_white_acrobat.png',
                                            cls:'x-btn-text-icon',
                                            hidden:(gE('situacion').value!='2'),
                                            text:'Obtener PDF del documento',
                                            handler:function()
                                                    {
                                                        var arrParam=[['idComprobante',gE('idComprobante').value],['getPDF',true]];
                                                        enviarFormularioDatos('../formatosFacturasElectronicas/cfdiNomina_1.php',arrParam);
                                                    }
                                            
                                        }
                                    ],
                            items:	[
                                         new Ext.ux.IFrameComponent({ 
                                                                        id: 'content', 
                                                                        region: 'center',
                                                                        anchor:'100% 100%',
                                                                        url: '../paginasFunciones/white.php',
                                                                        style: 'width:100%;height:100%' 
                                                                    }) 
                                    ]
                        }
                    )
                        
	gEx('content').load({url:'../formatosFacturasElectronicas/cfdiNomina_1.php',params:{idComprobante:gE('idComprobante').value,cPagina:'sFrm=true'},scripts:true})                         
}