<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{

    new Ext.Panel	(
    					{
                            xtype:'panel',
                            renderTo:'tblCuerpo',
                            layout:'border',
                            width:'100%',
                            height:obtenerDimensionesNavegador()[0]-80,
                            
                            tbar:	[
                                        {
                                            icon:'../images/page_white_acrobat.png',
                                            cls:'x-btn-text-icon',
                                            hidden:(gE('situacion').value!='2'),
                                            text:'Obtener PDF del documento',
                                            handler:function()
                                                    {
                                                        var arrParam=[['iC',bE(gE('idComprobante').value)],['getPDF',true]];
                                                        enviarFormularioDatos('../formatosFacturasElectronicas/cfdi_1.php',arrParam);
                                                    }
                                            
                                        },'-',
                                         {
                                            icon:'../images/Icono_xml.gif',
                                            cls:'x-btn-text-icon',
                                            hidden:(gE('situacion').value!='2'),
                                            text:'Obtener XML del documento',
                                            handler:function()
                                                    {
                                                        location.href='../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(gE('idComprobante').value);
                                                    }
                                            
                                        },'-',
                                        {
                                            icon:'../images/page_remove.png',
                                            cls:'x-btn-text-icon',
                                            hidden:(gE('situacion').value!='2'),
                                            text:'Cancelar CFDI',
                                            handler:function()
                                                    {
                                                    	mostrarVentaCancelarCFDI();
                                                    
                                                     	
                                                    }
                                            
                                        },'-',
                                        {
                                            icon:'../images/arrow_refresh.PNG',
                                            cls:'x-btn-text-icon',
                                            hidden:(gE('situacion').value!='5'),
                                            text:'Reintentar timbrar el comprobante',
                                            handler:function()
                                                    {
                                                    	function resp(btn)
                                                        {
                                                        	if(btn=='yes')
                                                            {
                                                            	function funcAjax()
                                                                {
                                                                    var resp=peticion_http.responseText;
                                                                    arrResp=resp.split('|');
                                                                    if(arrResp[0]=='1')
                                                                    {
                                                                        window.parent.cerrarVentanaFancy();
                                                                    }
                                                                    else
                                                                    {
                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                    }
                                                                }
                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=75&iC='+gE('idComprobante').value,true);
                                                            }
                                                        }
                                                        msgConfirm('Est&aacute; seguro de querer reintentar timbrar el comprobante?',resp);
                                                    
                                                     	
                                                    }
                                            
                                        }
                                    ],
                            items:	[
                                         new Ext.ux.IFrameComponent({ 
                                                                        id: 'content', 
                                                                        region: 'center',
                                                                        anchor:'100% 100%',
                                                                        url: '../paginasFunciones/white.php',
                                                                        style: 'width:100%;height:100%' 
                                                                    }) 
                                    ]
                        }
                    )
                        
	gEx('content').load({url:gE('formatoFactura').value,params:{iC:bE(gE('idComprobante').value),cPagina:'sFrm=true'},scripts:true})                         
}


function mostrarVentaCancelarCFDI()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo de la cancelaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:480,
                                                            height:100,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar CFDI',
										width: 530,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	if(gEx('txtMotivo').getValue()=='')
                                                                        {
                                                                        
                                                                        	function resp1()
                                                                            {
                                                                            	gEx('txtMotivo').focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo de la cancelaci&oacute;n',resp1);
                                                                            return;
                                                                        }
																		function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	
                                                                                        
                                                                                         if(gE('esVistaListado').value=='0')
                                                                                              window.parent.gEx('grid_tblTabla').getStore().reload();
                                                                                          else
                                                                                              window.parent.regresar1Pagina();
                                                                                        
                                                                                        window.parent.cerrarVentanaFancy();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=48&motivo='+cv(gEx('txtMotivo').getValue())+'&iC='+gE('idComprobante').value,true);
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar el CFDI?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	
	   
	
}