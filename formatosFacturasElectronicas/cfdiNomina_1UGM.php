<?php include_once('latis/tcpdf/tcpdf.inc.php');
		include_once("latis/conexionBD.php"); 
		include_once("latis/cfdi/cNomina.php");
		include_once("latis/numeroToLetra.php");
		include_once("latis/cCodigoBarras.php");

$descargarPDF=false;
if(isset($_POST["getPDF"]))		
	$descargarPDF=true;
	
$almacenarPDF=false;	

if(isset($_GET["almacenarPDF"]))		
{
	$almacenarPDF=true;
	
}
	
$idComprobante=-1;
$xml="";
$objComprobante=NULL;
$c=new cNominaCFDI();
if(isset($_POST["idComprobante"])||isset($_GET["idComprobante"]))		
{
	if(isset($_POST["idComprobante"]))
		$idComprobante=$_POST["idComprobante"];
	else
		$idComprobante=$_GET["idComprobante"];
	$objComprobante=$c->cargarComprobanteXMLObjeto($idComprobante);
}
else
{
	
	if(isset($_POST["xml"]))
	{
		$xml=bD($_POST["xml"]);
		$objComprobante=$c->convertirXMLCadenaToObj($xml);
	}
}


		

$consulta="SELECT idReferencia FROM 687_certificadosSelloDigital WHERE idCertificado=".$objComprobante["idCertificado"];
$idEmpresa=$con->obtenerValor($consulta);
$consulta="SELECT logoEmpresa FROM 6927_empresas WHERE idEmpresa=".$idEmpresa;
$idImagen=$con->obtenerValor($consulta);



$regimenFiscal="";
foreach($objComprobante["datosEmisor"]["regimenFiscal"] as $r)
{
	if($regimenFiscal=="")
		$regimenFiscal=$r;
	else
		$regimenFiscal.=", ".$r;
}




$domicilio="";
if($objComprobante["datosEmisor"]["domicilio"]["calle"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["calle"];
else
	$domicilio.=" N/E";
$domicilio.=" No.";	
if($objComprobante["datosEmisor"]["domicilio"]["noExterior"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["noExterior"];
else
	$domicilio.=" N/E";

if($objComprobante["datosEmisor"]["domicilio"]["noInterior"]!="")
	$domicilio.=" Int. ".$objComprobante["datosEmisor"]["domicilio"]["noInterior"];

$domicilio.=" Colonia:";	
if($objComprobante["datosEmisor"]["domicilio"]["colonia"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["colonia"]."";	
else
	$domicilio.=" N/E.";
	
	
	
	
$domicilio.=" C.P.:";	
if($objComprobante["datosEmisor"]["domicilio"]["codigoPostal"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["codigoPostal"].".";	
else
	$domicilio.=" N/E.";

$domicilio2="";
if($objComprobante["datosEmisor"]["domicilio"]["municipio"]!="")
{
	$domicilio2=$objComprobante["datosEmisor"]["domicilio"]["municipio"];
}
if($objComprobante["datosEmisor"]["domicilio"]["estado"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=", ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
	else
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
		
}

if($objComprobante["datosEmisor"]["domicilio"]["pais"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
	else
		$domicilio2=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
		
}
else
{
	if($domicilio2!="")
		$domicilio2.=".";	
}


$urlImagen="../images/s.gif";
$altoBase=110;
$ancho="200";
if($idImagen!="")
{
	$imagen=$baseDir."/documentosUsr/archivo_".$idImagen;
	$datosImagen=getimagesize($imagen);
	
	
	if($datosImagen[1]>$altoBase)
	{
		$porcentaje=$altoBase/$datosImagen[1];
		$ancho=$datosImagen[0]*$porcentaje;
	
	}
	else
	{
		
		$porcentaje=($datosImagen[1]/110);	
		$ancho=$datosImagen[0]*$porcentaje;
	}
	$urlImagen="../documentosUsr/archivo_".$idImagen;
	$idImagen=bE($idImagen);
	
	
	
}

$arrLineas=obtenerCostoHoraDocente($idComprobante);


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetFont('helvetica', '', 10);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// add a page
$pdf->AddPage();
$datosEmisor="
			<span class='tituloEmpresa'><b>".$objComprobante["datosEmisor"]["razonSocial"]."</b></span><br>
			<B>RFC:</B> ".$objComprobante["datosEmisor"]["rfc"]."<br>
			".$domicilio."<br>
			".$domicilio2."<BR><BR>
			<B>REGISTRO PATRONAL:</B> ".$objComprobante["datosNomina"]["registroPatronal"]."<br>
			<B>RÉGIMEN FISCAL: </B> ".$regimenFiscal."
			";
$params=TCPDF_STATIC::serializeTCPDFtagParameters(array('../images/cbb.jpg','','',30,30));

$f=function($x)
	{
		$a=func_get_args();
		unset($a[0]);
		return call_user_func_array($x,$a);
	};
$d=0;


function formatearFecha($fecha)
{
	return date("d/m/Y",strtotime($fecha))	;
}

function formatearMoneda($monto)
{
	return '$ '.number_format($monto,2);
}
$texto="
<span>
<b>FOLIO:</b> <br>
".$objComprobante["serie"]." ".$objComprobante["folio"]."<BR>
<b>FOLIO FISCAL (UUID)</b> 	<BR>
".$objComprobante["folioUUID"]."<BR>
<b>NO. DE SERIE DEL CERTIFICADO DEL SAT</b><BR>
".$objComprobante["noCertificadoSAT"]."<br>
<b>NO. DE SERIE DEL CERTIFICADO DEL EMISOR</b><BR>
".$objComprobante["noCertificado"]."<BR>
<b>FECHA Y HORA DE CERTIFICACION</b><BR>
".$objComprobante["fechaCertificacionSAT"]."<BR>
<b>FECHA Y HORA DE EMISIÓN DE CFDI</b><BR>
".$objComprobante["fechaComprobante"]."<BR>
</span>
";


$aTotal=explode(".",$objComprobante["total"]);

$cbbTotal=str_pad($aTotal[0],10,"0",STR_PAD_LEFT).".".str_pad($aTotal[1],6,"0",STR_PAD_RIGHT);

$cadenaCBB="?re=".$objComprobante["datosEmisor"]["rfc"]."&rr=".$objComprobante["datosReceptor"]["rfc"]."&tt=".$cbbTotal."&id=".$objComprobante["folioUUID"];

$cBarras=new cCodigoBarras($cadenaCBB,"QR","",1,2,60);
$urlCBB="../archivosTemporalesCodigoBarras/".$cBarras->generarCodigoBarrasImagenArchivo();
$urlArchivoCBB=$baseDir."/archivosTemporalesCodigoBarras/".$cBarras->generarCodigoBarrasImagenArchivo();


$html =<<<EOD
<!-- EXAMPLE OF CSS STYLE -->
<style>
	.tituloEmpresa
	{
		font-size:14px;	
	}
	.celdaTitulo
	{
		background-color:#666;
		color:#FFF;
		font-size:9px;
		font-weight:bold;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		
		
		
	}
	
	.celdaValor
	{
		color:#000;
		font-size:9px;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		
	}
	
	.celdaTotal
	{
		color:#000;
		font-size:12px;
		text-align:right;
		padding-top:3px;
		padding-bottom:3px;
		background-color:#DDD;
	}
	
	.leyenda
	{
		font-size:11px;
	}
</style>
<body>

	<table width="100%">
	<tr>
		<td align="left">
			<table>
				<tr>
					<td align="center" width="200">
						<img src="$urlImagen" width="$ancho" height="$altoBase">
					</td>
					<td width="5"></td>
					<td style="font-size:10px" text-align="left" width="250">
					$datosEmisor
					</td>
					<td align="center" style="font-size:8px">
						$texto
					</td>
				</tr>
			</table>
		
			<table style="border:none;" cellspacing="0">
				<tr>
					<td width="80" class="celdaTitulo" >Num. Empleado</td>
					<td width="200" class="celdaTitulo">Nombre</td>
					<td width="200" class="celdaTitulo">Departamento</td>
					
				</tr>
				<tr>
					<td  class="celdaValor">{$objComprobante["datosNomina"]["numEmpleado"]}</td>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["razonSocial"]}</td>
					<td  class="celdaValor">{$objComprobante["datosNomina"]["departamento"]}</td>
					
				</tr>
				<tr>
					<td width="170" class="celdaTitulo" colspan="2">Puesto</td>
					<td width="100" class="celdaTitulo">RFC</td>
					<td width="130" class="celdaTitulo">CURP</td>
					<td width="100" class="celdaTitulo">IMSS</td>
					<td width="100" class="celdaTitulo">Periodicidad</td>
				</tr>
				<tr>
					<td class="celdaValor" colspan="2">{$objComprobante["datosNomina"]["puesto"]}</td>
					<td class="celdaValor">{$objComprobante["datosReceptor"]["rfc"]}</td>
					<td class="celdaValor">{$objComprobante["datosNomina"]["curp"]}</td>
					<td class="celdaValor" >{$objComprobante["datosNomina"]["numSS"]}</td>
					<td class="celdaValor">{$objComprobante["datosNomina"]["periodicidadPago"]}</td>

				</tr>
				<tr>
					<td width="70" class="celdaTitulo">SDI</td>
					<td width="80" class="celdaTitulo">D&iacute;as trabajados</td>
					<td width="150" class="celdaTitulo">Periodo de pago</td>
					<td width="100" class="celdaTitulo">Tipo Jornada</td>
					<td width="100" class="celdaTitulo">Tipo Contrato</td>
					<td width="100" class="celdaTitulo">Fecha de pago</td>
				</tr>
				<tr>
					<td class="celdaValor">{$f('formatearMoneda',$objComprobante["datosNomina"]["sdi"])}</td>
					<td class="celdaValor">{$objComprobante["datosNomina"]["numDiasPagados"]}</td>
					<td class="celdaValor">Del {$f('formatearFecha',$objComprobante["datosNomina"]["fechaInicioPago"])} al {$f('formatearFecha',$objComprobante["datosNomina"]["fechaFinPago"])}</td>
					<td class="celdaValor">{$objComprobante["datosNomina"]["tipoJornada"]}</td>
					<td class="celdaValor">{$objComprobante["datosNomina"]["tipoContrato"]}</td>
					<td class="celdaValor">{$f('formatearFecha',$objComprobante["datosNomina"]["fechaPago"])}</td>
				</tr>
			</table>
		
			<br>
		
			<table style="border:none;" cellspacing="0">
				<tr>
					<td width="90" class="celdaTitulo">Clave</td>
					<td width="260" class="celdaTitulo" style="text-align:left">Concepto</td>
					<td width="90" class="celdaTitulo" >Percepci&oacute;n</td>
					<td width="90" class="celdaTitulo">Deducci&oacute;n</td>
					<td width="90" class="celdaTitulo">Importe</td>
				</tr>
EOD;
$conceptos="";
foreach($objComprobante["datosNomina"]["arrPercepciones"] as $p)
{
	$conceptos.='<tr>
						<td class="celdaValor">'.$p["tipoPercepcion"].'</td>
						<td  class="celdaValor" style="text-align:left">'.$p["descripcion"].'</td>
						<td  class="celdaValor" style="text-align:right">$ '.number_format(($p["importeGravado"]+$p["importeExento"]),2).'</td>
						<td  class="celdaValor"></td>
						<td  class="celdaValor" style="text-align:right">$ '.number_format(($p["importeGravado"]+$p["importeExento"]),2).'</td>
				</tr>';
}

foreach($objComprobante["datosNomina"]["arrDeducciones"] as $p)
{
	$conceptos.='<tr>
						<td class="celdaValor">'.$p["tipoDeduccion"].'</td>
						<td  class="celdaValor" style="text-align:left">'.$p["descripcion"].'</td>
						<td  class="celdaValor"></td>
						<td  class="celdaValor" style="text-align:right">$ '.number_format(($p["importeGravado"]+$p["importeExento"]),2).'</td>
						
						<td  class="celdaValor" style="text-align:right">- $ '.number_format(($p["importeGravado"]+$p["importeExento"]),2).'</td>
				</tr>';
}
$conceptos.='<tr>
						<td class="celdaValor"></td>
						<td  class="celdaValor" style="text-align:left"></td>
						<td  class="celdaValor"></td>
						<td  class="celdaTotal" style="text-align:right"><b>Total:</b></td>
						<td  class="celdaTotal" style="text-align:right"> $ '.number_format($objComprobante["total"],2).'</td>
				</tr>
				<tr>
					<td colspan="5" class="celdaTitulo" style="text-align:left">'.convertirNumeroLetra($objComprobante["total"],true).' '.parteDecimal(number_format($objComprobante["total"],2)).'/100 M.N.</td>
				</tr>
				</table>
				';
				
$html2="";				
if($objComprobante["folioUUID"]!="")				
{
	$html2=<<<EOD2
					
					
				
				<table style="border:none;" cellspacing="0">
					
					<tr>
						<td width="500" >
						<span style="font-size:11px; font-weight:bold">
						Cadena original del complemento de certificado digital del SAT<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["cadenaOriginalComplententoSAT"]}
						</span>
						</td>
						<td width="10"></td>
						<td width="195" rowspan="3" align="left" ><img src="$urlCBB" width="120" height="120"></td>
					</tr>
					
					<tr>
						<td  >
						<span style="font-size:11px; font-weight:bold">
						Sello digital del emisor<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["selloCFD"]}
						</span>
						</td>
						<td ></td>
						
					</tr>
					
					<tr>
						<td  >
						<span style="font-size:11px; font-weight:bold">
						Sello digital del SAT<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["SelloDigitalSAT"]}
						</span>
						</td>
						<td ></td>
			
					</tr>
				</table><br><br><br><br><br><br>
				<table width="100%">
					<tr>
						<td align="center">
						__________________________________________<br>
						<span class="leyenda">Firma del Empleado</span>
						</td>
						
						
					</tr>
					<tr>
						<td align="right"><br><br>
						<span class="leyenda">Esta es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			</td>
		</tr>
		</table>
EOD2;
}
else
{
	$html2=<<<EOD2
					
					
				
				<br><br><br><br><br><br>
				<table width="100%">
					<tr>
						<td align="center">
						__________________________________________<br>
						<span class="leyenda">Firma del Empleado</span>
						</td>
						
						
					</tr>
					<tr>
						<td align="right"><br><br>
						<span class="leyenda">Esta es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			</td>
		</tr>
		</table>
EOD2;
}


$html2.='
		<br><br>
		<table width="100%">
		<tr>
			<td align="left">

		<table style="border:none;" cellspacing="0">
			<tr>
				<td style="font-size:13px;"><b>Desglose de Sueldo horas</b></td>
			</tr>
			<tr>
				<td width="150"class="celdaTitulo">HRS</td>
				<td width="90"class="celdaTitulo">REP</td>
				<td width="90" class="celdaTitulo">FALTA</td>
				<td width="90" class="celdaTitulo">TOTAL HRS</td>
				<td width="90" class="celdaTitulo">COSTO HRS</td>
				<td width="90" class="celdaTitulo">IMPORTE</td>
			</tr>
			';
			//varDump($arrLineas);
				foreach($arrLineas as $l)
				{
					if(!isset($l["ObtenerImporteFalta"]))
						$l["ObtenerImporteFalta"]=0;
					if(!isset($l["totalPercepcion"]))
						$l["totalPercepcion"]=0;	
					if(!isset($l["ImporteDiaFestivo"]))
						$l["ImporteDiaFestivo"]=0;
					if(!isset($l["importeReposicion"]))
						$l["importeReposicion"]=0;
					if(!isset($l["ImporteFaltaNoPagadas"]))
						$l["ImporteFaltaNoPagadas"]=0;
					if(!isset($l["pagoPorSuplencia"]))
						$l["pagoPorSuplencia"]=0;	
						
					$horasDocente=$l["totalPercepcion"]+ $l["pagoPorSuplencia"]+$l["ImporteDiaFestivo"];
					$totalHoras=$horasDocente." [".$l["nivel"]."]";
					$horasSuma=$totalHoras+$l["importeReposicion"]-$l["ObtenerImporteFalta"]-$l["ImporteFaltaNoPagadas"];
					$subTotal=$horasSuma*$l["costo"];
					$faltas=$l["ObtenerImporteFalta"]+$l["ImporteFaltaNoPagadas"];
					$html2.='			
								<tr>
									<td class="celdaValor">'.$totalHoras.'</td>
									<td class="celdaValor">'.$l["importeReposicion"].'</td>
									<td class="celdaValor">'.$faltas.'</td>
									<td class="celdaValor">'.$horasSuma.'</td>
									<td class="celdaValor">'.$l["costo"].'</td>
									<td class="celdaValor">'.formatearMoneda($subTotal).'</td>
								</tr>
					';
				}
$html2.='</table>
		</td>
		</tr>
		</table>

		';

$html.=$conceptos.$html2;
//
$pdf->writeHTML($html, true, false, true, false, '');


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
if($descargarPDF)
{
	$pdf->Output($objComprobante["datosReceptor"]["rfc"].'.pdf', 'D');
	unlink($urlArchivoCBB);
	return;
}

if($almacenarPDF)
{
	$pdf->Output("../archivosTemporales/".$idComprobante.'.pdf', 'F');
	unlink($urlArchivoCBB);
	return;
}

function obtenerCostoHoraDocente($idComprobante)
{
	global $con;
	$consulta="SELECT idUsuario,idNomina,codDepartamento FROM 671_asientosCalculosNomina WHERE idComprobante='".$idComprobante."'";
	$res=$con->obtenerPrimeraFila($consulta);
	$idNomina=$res[1];
	$idUsuario=$res[0];
	$codUnidad=$res[2];

			$consultaHoras="SELECT h.idUsuario,h.horas,h.costoHora,h.idGrupo,calculo FROM 4556_costoHoraDocentes h,4520_grupos AS g 
							WHERE h.idGrupo=g.idGrupos AND g.Plantel='".$codUnidad."' AND h.idUsuario='".$idUsuario."' 
							AND h.idNomina='".$idNomina."'";
			$horas=$con->obtenerFilas($consultaHoras);
			$numeroReg=mysql_num_rows($horas);
			$arrCostos=array();
		  	while($row=mysql_fetch_row($horas))
			{
				$obtenerNivel="SELECT p.nivelPlanEstudio,t.txtAbreviatura FROM 4500_planEstudio p,4520_grupos g,_401_tablaDinamica AS t 
								WHERE g.idPlanEstudio=p.idPlanEstudio AND g.idGrupos='".$row[3]."' AND t.id__401_tablaDinamica=p.nivelPlanEstudio";
				$niveles=$con->obtenerPrimeraFila($obtenerNivel);
				if(!isset($arrCostos[$niveles[0]."_".$row[2]."_".$row[4]]))
					 $arrCostos[$niveles[0]."_".$row[2]."_".$row[4]]=$row[1];//nivel_costoHora_calculo_horas
				else
					 $arrCostos[$niveles[0]."_".$row[2]."_".$row[4]]+=$row[1];
			}
//			
			//varDump($arrCostos);
				$arrLineas=array();
				$nLinea=0;
				$columna="";
				$dedu=0;
				$costo=0;
				$sumHora=0;
				$valorCol=0;
				$sumHora1=0;

				foreach($arrCostos as $nivel=>$valor)	
				{
					$dato=explode("_",$nivel);
					$nomNivel=$dato[0];
					$costo=$dato[1];
					$tipo=$dato[2];
					$hora=$valor;
					switch($tipo)
					{
						case "totalPercepcion":
							$valorCol=$valor;
						break;
						case "ObtenerImporteFalta":
							$valorCol=$valor;
						break;
						case "ImporteDiaFestivo":
							$valorCol=$valor;
						break;
						case "importeReposicion":
							$valorCol=$valor;
						break;
						case "ImporteFaltaNoPagadas":
							$valorCol=$valor;
						break;
						case "pagoPorSuplencia":
							$valorCol=$valor;
						break;
						
					}
					if(!isset($arrLineas[$nomNivel."_".$costo]))
					{
						$arrLineas[$nomNivel."_".$costo][$tipo]=$valorCol;
						
						$nombreN="SELECT txtAbreviatura FROM _401_tablaDinamica WHERE id__401_tablaDinamica='".$nomNivel."'";
						$nivelAbrev=$con->obtenerValor($nombreN);
						$nivelA="";
						if($nivelAbrev)
							$nivelA=$nivelAbrev;
						$arrLineas[$nomNivel."_".$costo]["costo"]=$costo;
						$arrLineas[$nomNivel."_".$costo]["nivel"]=$nivelA;
						
						$arrLineas[$nomNivel."_".$costo]["TImporte"]=$costo*$sumHora;
					}
					else
						$arrLineas[$nomNivel."_".$costo][$tipo]=$valorCol;
						
					if($tipo=="totalPercepcion"||$tipo=="pagoPorSuplencia")	
						$arrLineas[$nomNivel."_".$costo]["sumHora"]=$valor;
					if($tipo=="ImporteDiaFestivo")
						$arrLineas[$nomNivel."_".$costo]["sumHora1"]=$valor;
				}
				return $arrLineas;	
}

?>

<body style="background-color:#FFF">
<table width="100%">
<tr>
	<td align="center">
		<table>
        	<tr>
            	<td align="right">
        <?php
			echo $html;
		?>
        		</td>
            </tr>
		</table>
    </td>
</tr>
</table>
</body>