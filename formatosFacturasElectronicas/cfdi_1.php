<?php include_once('latis/tcpdf/tcpdf.inc.php');

		include_once("latis/conexionBD.php"); 

		include_once("latis/cfdi/cFactura.php");
		include_once("latis/numeroToLetra.php");
		include_once("latis/cCodigoBarras.php");




ini_set('memory_limit', '500M');
$motivoDescuento="";
$descargarPDF=false;
if(isset($_POST["getPDF"]))		
	$descargarPDF=true;
	
	
	
$almacenarPDF=false;	

if(isset($_GET["almacenarPDF"]))		
{
	$almacenarPDF=true;
	
}	
	
$idComprobante=-1;
$xml="";
$objComprobante=NULL;
$c=new cFacturaCFDI();

$comentariosAdicionales="";
$arrTotales=array();
$arrConceptos=array();
$buscarUnidadMedida=true;
if(isset($_POST["iC"])||isset($_GET["idComprobante"]))		
{
	if(isset($_POST["iC"]))
		$idComprobante=bD($_POST["iC"]);
	else
		$idComprobante=($_GET["idComprobante"]);
	
	$objComprobante=$c->cargarComprobanteXMLObjeto($idComprobante);

	$motivoDescuento=$objComprobante["motivoDescuento"];
	$consulta="SELECT idReferencia,tipoUso FROM 703_relacionFoliosCFDI WHERE idFolio=".$idComprobante;

	$fDatosComprobante=$con->obtenerPrimeraFila($consulta);
	$idReferencia=$fDatosComprobante[0];
	
	
	switch($fDatosComprobante[1])
	{
		case 4:
		case 6:	
		
			$buscarUnidadMedida=false;
			$arrTotales=array();
			
			$baseConcepto='{"idConcepto":"","tasaConcepto":"","montoConcepto":"","etiqueta":""}';
			$oTotal=json_decode($baseConcepto);
			
			$subtotal=$objComprobante["subtotal"];
			$oTotal->idConcepto="7";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$subtotal;
			$oTotal->etiqueta="Subtotal";
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			$descuento=$objComprobante["descuento"];
			$oTotal->idConcepto="6";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$descuento;
			$oTotal->etiqueta="Descuento";			
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			
			$oIVA=NULL;
			
			foreach($objComprobante["impuestos"]["traslados"] as $traslado)
			{
				if($traslado["impuesto"]=="IVA")	
				{
					$oIVA=$traslado;
					break;	
				}
			}
			
			
			if($oIVA!=NULL)
			{
				$iva=0;
				$oTotal->tasaConcepto="16";
				if($oIVA)
				{
					$iva=$oIVA["importe"];
					$oTotal->tasaConcepto=$oIVA["tasa"];
				}
				$oTotal->idConcepto="1";			
				$oTotal->montoConcepto=$iva;
				$oTotal->etiqueta="IVA";
				array_push($arrTotales,$oTotal);
			}
			$oTotal=json_decode($baseConcepto);
			$total=$objComprobante["total"];
			$oTotal->idConcepto="12";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$total;
			$oTotal->etiqueta="Total";
			array_push($arrTotales,$oTotal);
			
			foreach($objComprobante["conceptos"] as $cAux)
			{
				$objTmp='{"clave":"","cabecera":"","descripcion":"","unidadMedida":"","costoUnitario":"","descuentoUnitario":"","cantidad":"","subtotal":"","iva":"","tasaIVA":"","total":"","descuentoTotal":""}';
				$cTmp=json_decode($objTmp);
				$cTmp->cAux="";
				$cTmp->cabecera="";
				$cTmp->descripcion=$cAux["descripcion"];
				$cTmp->unidadMedida=$cAux["unidad"];
				$cTmp->costoUnitario=$cAux["valorUnitario"];
				$cTmp->descuentoUnitario="";
				$cTmp->cantidad=$cAux["cantidad"];
				$cTmp->subtotal="";
				$cTmp->iva="";
				$cTmp->tasaIVA="";
				$cTmp->total=$cAux["importe"];
				$cTmp->descuentoTotal="";		
				array_push($arrConceptos,$cTmp);
			}
			
		break;
		
		default:
		
			$consulta="SELECT comentariosAdicionales FROM 706_comprobanteFactura WHERE idComprobanteFactura=".$idReferencia;
			$comentariosAdicionales=$con->obtenerValor($consulta);
			if(trim($comentariosAdicionales)!="")
				$comentariosAdicionales="(".$comentariosAdicionales.")";
			$consulta="SELECT tipoConcepto,idConcepto,tasaConcepto,montoConcepto,etiquetaConcepto FROM 709_impuestosRetencionesComprobante WHERE idComprobante=".$idReferencia." ORDER BY idImpuestoRetencion";
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				$oTotal=json_decode('{"tipoConcepto":"'.$fila[0].'","idConcepto":"'.$fila[1].'","tasaConcepto":"'.$fila[2].'","montoConcepto":"'.$fila[3].'","etiqueta":"'.cv($fila[4]).'"}');
				array_push($arrTotales,$oTotal);
			}
		
			$consulta="SELECT clave,descripcionConcepto,cabecera,unidadMedida,costoUnitario,descUnitario,cantidad,subtotal,totalIVA,tasaIVA,total,totalDescuento FROM 707_conceptosFactura WHERE idComprobante=".$idReferencia;
			
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				$objTmp='{"clave":"","cabecera":"","descripcion":"","unidadMedida":"","costoUnitario":"","descuentoUnitario":"","cantidad":"","subtotal":"","iva":"","tasaIVA":"","total":"","descuentoTotal":""}';
				$cTmp=json_decode($objTmp);
				$cTmp->clave=$fila[0];
				$cTmp->cabecera=$fila[2];
				$cTmp->descripcion=$fila[1];
				$cTmp->unidadMedida=$fila[3];
				$cTmp->costoUnitario=$fila[4];
				$cTmp->descuentoUnitario=$fila[5];
				$cTmp->cantidad=$fila[6];
				$cTmp->subtotal=$fila[7];
				$cTmp->iva=$fila[8];
				$cTmp->tasaIVA=$fila[9];
				$cTmp->total=$fila[10];
				$cTmp->descuentoTotal=$fila[11];		
				array_push($arrConceptos,$cTmp);
			}
		
		break;	
	}
}
else
{
	
	if(isset($_POST["xml"]))
	{
		$buscarUnidadMedida=false;
		$xml=bD($_POST["xml"]);
		
		$cadObj=bD($_POST["cadObj"]);
		$oComprobante=NULL;
		if(isset($_POST["objSerializado"]))
			$oComprobante=unserialize($cadObj);
		else
			$oComprobante=json_decode($cadObj);
		
		$motivoDescuento="";
		if(!isset($_POST["objSerializado"]))
			$motivoDescuento=$oComprobante->motivoDescuento;
		else
			$motivoDescuento=$oComprobante["motivoDescuento"];
			
		$comentariosAdicionales="";	
		if(!isset($_POST["objSerializado"]))	
			
			$comentariosAdicionales=($oComprobante->comentariosAdicionales);
		else	
			$comentariosAdicionales=($oComprobante["comentariosAdicionales"]);
		
		
		if(trim($comentariosAdicionales)!="")
			$comentariosAdicionales="(".$comentariosAdicionales.")";
		
		
		$oComp=array();
		
		if(!isset($_POST["objSerializado"]))
		{
			$buscarUnidadMedida=true;
			foreach($oComprobante->arrConceptos as $cTmp)
			{
				$cTmp->cabecera=urldecode($cTmp->cabecera);
				array_push($arrConceptos,$cTmp);
			}
			$oComp["idCertificado"]=$oComprobante->idCertificado;
			$oComp["idSerie"]=$oComprobante->idSerie;
			$oComp["idClienteFactura"]=$oComprobante->idCliente;
			$arrTotales=$oComprobante->arrTotales;
		}
		else
		{
			
			foreach($oComprobante["conceptos"] as $cTmp)
			{
				
				$oTmp=json_decode('{"cabecera":"","cantidad":"'.$cTmp["cantidad"].'","unidadMedida":"'.$cTmp["unidad"].'","noIdentificacion":"'.$cTmp["noIdentificacion"].
								'","descripcion":"'.$cTmp["descripcion"].'","costoUnitario":"'.$cTmp["valorUnitario"].'","total":"'.$cTmp["importe"].'"}');
				
				
				array_push($arrConceptos,$oTmp);
			}
			$oComp["idCertificado"]=$oComprobante["idCertificado"];
			$oComp["idSerie"]=$oComprobante["idSerie"];
			$oComp["idClienteFactura"]=$oComprobante["idClienteFactura"];
			
			$arrTotales=array();
			
			$baseConcepto='{"idConcepto":"","tasaConcepto":"","montoConcepto":"","etiqueta":""}';
			$oTotal=json_decode($baseConcepto);
			
			$subtotal=$oComprobante["subtotal"];
			$oTotal->idConcepto="7";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$subtotal;
			$oTotal->etiqueta="Subtotal";
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			$descuento=$oComprobante["descuento"];
			$oTotal->idConcepto="6";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$descuento;
			$oTotal->etiqueta="Descuento";			
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			
			$oIVA=NULL;
			$iva=0;
			foreach($oComprobante["impuestos"]["traslados"] as $traslado)
			{
				if($traslado["impuesto"]=="IVA")	
				{
					$oIVA=$traslado;
					$iva+=$traslado["importe"];
				}
			}
			
			$oTotal->tasaConcepto="16";
			/*if($oIVA)
			{
				//$iva=$oIVA["importe"];
				$oTotal->tasaConcepto=$oIVA["tasa"];
			}*/
			
			if($oIVA!=NULL)
			{
				$oTotal->idConcepto="1";			
				$oTotal->montoConcepto=$iva;
				$oTotal->etiqueta="IVA";
				array_push($arrTotales,$oTotal);
			}
			$oTotal=json_decode($baseConcepto);
			$total=$oComprobante["total"];
			$oTotal->idConcepto="12";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$total;
			$oTotal->etiqueta="Total";
			array_push($arrTotales,$oTotal);

		}
		
		
		
		
		
		$objComprobante=$c->convertirXMLCadenaToObj($xml,$oComp);
	}
}






$consulta="SELECT idReferencia FROM 687_certificadosSelloDigital WHERE idCertificado=".$objComprobante["idCertificado"];
$idEmpresa=$con->obtenerValor($consulta);
$consulta="SELECT logoEmpresa FROM 6927_empresas WHERE idEmpresa=".$idEmpresa;
$idImagen=$con->obtenerValor($consulta);
$regimenFiscal="";
foreach($objComprobante["datosEmisor"]["regimenFiscal"] as $r)
{
	if($regimenFiscal=="")
		$regimenFiscal=$r;
	else
		$regimenFiscal.=", ".$r;
}

$domicilio="";
if($objComprobante["datosEmisor"]["domicilio"]["calle"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["calle"];
else
	$domicilio.=" N/E";
$domicilio.=" No.";	
if($objComprobante["datosEmisor"]["domicilio"]["noExterior"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["noExterior"];
else
	$domicilio.=" N/E";

if($objComprobante["datosEmisor"]["domicilio"]["noInterior"]!="")
	$domicilio.=" Int. ".$objComprobante["datosEmisor"]["domicilio"]["noInterior"];

$domicilio.=" Colonia:";	
if($objComprobante["datosEmisor"]["domicilio"]["colonia"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["colonia"]."";	
else
	$domicilio.=" N/E.";
	
$domicilio.=" C.P.:";	
if($objComprobante["datosEmisor"]["domicilio"]["codigoPostal"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["codigoPostal"].".";	
else
	$domicilio.=" N/E.";

$domicilio2="";
if($objComprobante["datosEmisor"]["domicilio"]["municipio"]!="")
{
	$domicilio2=$objComprobante["datosEmisor"]["domicilio"]["municipio"];
}
if($objComprobante["datosEmisor"]["domicilio"]["estado"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=", ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
	else
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
		
}

if($objComprobante["datosEmisor"]["domicilio"]["pais"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
	else
		$domicilio2=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
		
}
else
{
	if($domicilio2!="")
		$domicilio2.=".";	
}

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetFont('helvetica', '', 10);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// add a page
//$style3 = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '0', 'color' => array(0, 0, 0));



//$paramsLinea=TCPDF_STATIC::serializeTCPDFtagParameters(array(5,50,280,50,$style3));
$urlImagen="../images/s.gif";
$altoBase=110;
$ancho="200";
if($idImagen!="")
{
	$imagen=$baseDir."/documentosUsr/archivo_".$idImagen;
	$datosImagen=getimagesize($imagen);
	
	
	if($datosImagen[1]>$altoBase)
	{
		$porcentaje=$altoBase/$datosImagen[1];
		$ancho=$datosImagen[0]*$porcentaje;
	
	}
	else
	{
		
		$porcentaje=($datosImagen[1]/110);	
		$ancho=$datosImagen[0]*$porcentaje;
	}
	$urlImagen="../documentosUsr/archivo_".$idImagen;
	$idImagen=bE($idImagen);
	
	
	
}
$pdf->AddPage();
$datosEmisor="
			<span class='tituloEmpresa'><b>".$objComprobante["datosEmisor"]["razonSocial"]."</b></span><br>
			<B>RFC:</B> ".$objComprobante["datosEmisor"]["rfc"]."<br>
			".$domicilio."<br>
			".$domicilio2."<BR><BR>
			<B>RÉGIMEN FISCAL: </B> ".$regimenFiscal."<br>
			<B>LUGAR DE EXPEDICI&Oacute;N: </B> ".$objComprobante["lugarExpedicion"]."
			";
$params=TCPDF_STATIC::serializeTCPDFtagParameters(array('../images/cbb.jpg','','',30,30));

$f=function($x)
	{
		$a=func_get_args();
		unset($a[0]);
		return call_user_func_array($x,$a);
	};
$d=0;


function formatearFecha($fecha)
{
	return date("d/m/Y",strtotime($fecha))	;
}

function formatearMoneda($monto)
{
	return '$ '.number_format($monto,2);
}
$texto="
<span>
<b>FOLIO:</b> <br>
".$objComprobante["serie"]." ".$objComprobante["folio"]."<BR>
<b>FOLIO FISCAL (UUID)</b> 	<BR>
".$objComprobante["folioUUID"]."<BR>
<b>NO. DE SERIE DEL CERTIFICADO DEL SAT</b><BR>
".$objComprobante["noCertificadoSAT"]."<br>
<b>NO. DE SERIE DEL CERTIFICADO DEL EMISOR</b><BR>
".$objComprobante["noCertificado"]."<BR>
<b>FECHA Y HORA DE CERTIFICACION</b><BR>
".$objComprobante["fechaCertificacionSAT"]."<BR>
<b>FECHA Y HORA DE EMISIÓN DE CFDI</b><BR>
".$objComprobante["fechaComprobante"]."<BR>
</span>
";

$calleReceptor="";

if($objComprobante["datosReceptor"]["domicilio"]["calle"]!="")
	$calleReceptor.=$objComprobante["datosReceptor"]["domicilio"]["calle"];
else
	$calleReceptor.="N/E";
if($objComprobante["datosReceptor"]["domicilio"]["noExterior"]!="")
	$calleReceptor.=" No. ".$objComprobante["datosReceptor"]["domicilio"]["noExterior"];
	
if($objComprobante["datosReceptor"]["domicilio"]["noInterior"]!="")
	$calleReceptor.=" Int. ".$objComprobante["datosReceptor"]["domicilio"]["noInterior"];

$aTotal=explode(".",$objComprobante["total"]);

$cbbTotal=str_pad($aTotal[0],10,"0",STR_PAD_LEFT).".".str_pad($aTotal[1],6,"0",STR_PAD_RIGHT);

$cadenaCBB="?re=".$objComprobante["datosEmisor"]["rfc"]."&rr=".$objComprobante["datosReceptor"]["rfc"]."&tt=".$cbbTotal."&id=".$objComprobante["folioUUID"];

$cBarras=new cCodigoBarras($cadenaCBB,"QR","",1,2,60);

$nombreArchivoCBB=$cBarras->generarCodigoBarrasImagenArchivo();
$urlCBB="../archivosTemporalesCodigoBarras/".$nombreArchivoCBB;

$urlArchivoCBB=$baseDir."/archivosTemporalesCodigoBarras/".$nombreArchivoCBB;

//fciudadanaq
$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
	.tituloEmpresa
	{
		font-size:14px;	
	}
	.celdaTitulo
	{
		background-color:#666;
		color:#FFF;
		font-size:9px;
		font-weight:bold;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		
		
		
	}
	
	.celdaValor
	{
		color:#000;
		font-size:9px;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		
	}
	
	.celdaTotal
	{
		color:#000;
		font-size:10px;
		text-align:right;
		padding-top:3px;
		padding-bottom:3px;
		background-color:#DDD;
	}
	
	.leyenda
	{
		font-size:11px;
	}
</style>
<body>
	<table width="100%">
	<tr>
		<td align="left">
			<table>
				<tr>
					<td align="center" width="200">
						<img src="$urlImagen" width="$ancho" height="$altoBase">
					</td>
					<td width="5"></td>
					<td style="font-size:8px" text-align="left" width="250">
					$datosEmisor
					</td>
					<td align="center" style="font-size:8px">
						$texto
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="400" class="celdaTitulo">CLIENTE</td>
					<td width="180" class="celdaTitulo">RFC</td>
				</tr>
				<tr>
					<td class="celdaValor" style="text-align: left">{$objComprobante["datosReceptor"]["razonSocial"]}</td>
					<td class="celdaValor" style="text-align: center">{$objComprobante["datosReceptor"]["rfc"]}</td>
				</tr>
				
			</table>
			<table>
				<tr>
					<td width="200" class="celdaTitulo">CALLE Y NUMERO</td>
					<td width="150" class="celdaTitulo">COLONIA</td>
					<td width="80" class="celdaTitulo">C.P</td>
					<td width="200" class="celdaTitulo">LOCALIDAD</td>
				</tr>
				<tr>
					<td  class="celdaValor">$calleReceptor</td>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["domicilio"]["colonia"]}</td>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["domicilio"]["codigoPostal"]}</td>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["domicilio"]["localidad"]}</td>
				</tr>
				
			</table>
			<table>
				<tr>
					<td width="200" class="celdaTitulo">ESTADO</td>
					<td width="150" class="celdaTitulo">PAÍS</td>
				</tr>
				<tr>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["domicilio"]["estado"]}</td>
					<td  class="celdaValor">{$objComprobante["datosReceptor"]["domicilio"]["pais"]}</td>
				</tr>
			</table><br><br>
			<table>
				<tr>
					<td width="90" class="celdaTitulo">CANTIDAD</td>
					<td  width="120" class="celdaTitulo">UNIDAD DE MEDIDA</td>
					<td width="250" class="celdaTitulo">DESCRIPCI&Oacute;N</td>
					<td width="90" class="celdaTitulo">P. UNITARIO</td>
					<td width="90" class="celdaTitulo">IMPORTE</td>
				</tr>
EOF;
$conceptos="";

foreach($arrConceptos as $c)
{
	if($buscarUnidadMedida)
	{
		$consulta="select unidadMedida FROM 6923_unidadesMedida where idUnidadMedida=".$c->unidadMedida;

		$c->unidad=$con->obtenerValor($consulta);
	}
	else
		$c->unidad=$c->unidadMedida;
	if($c->cabecera!="")
	{
		$conceptos.='<tr>
						<td class="celdaValor" style="text-align: center" colspan="5">'.$c->cabecera.'</td>
					</tr>
					';
	}
	
	$total=str_replace(",","",number_format($c->cantidad,2))*str_replace(",","",number_format($c->costoUnitario,2));
	
	$conceptos.='<tr>
						<td class="celdaValor" style="text-align: right">'.number_format($c->cantidad,2).'</td>
						<td   class="celdaValor">'.$c->unidad.'</td>
						<td  class="celdaValor" style="text-align: left">'.$c->descripcion.'</td>
						<td class="celdaValor" style="text-align: right">$ '.number_format($c->costoUnitario,2).'</td>
						<td  class="celdaValor" style="text-align: right">$ '.number_format($total,2).'</td>
					</tr>
					';

}
$conceptos.='
			<tr>
					<td align="right" colspan="5">
						<table>
						<tr>
							<td  width="400" align="left" STYLE="font-size:9px"><br><br>
							('.convertirNumeroLetra($objComprobante["total"],true).' '.parteDecimal(number_format($objComprobante["total"],2)).'/100 M.N.)<BR><BR>
							<B>MÉTODO DE PAGO:</B> 
							'.$objComprobante["metodoDePago"].' <br><br>
							--- '.$objComprobante["formaPago"].' ---
							'.$comentariosAdicionales;
		if($motivoDescuento!="")							
		{
			$conceptos.='<BR><BR><b>MOTIVO DE DESCUENTO:</b><br><i>'.$motivoDescuento.'</i>';
		}
							
							
$conceptos.='				<br><br><br>			
							</td>
							<td width="50">
							</td>
							<td>
								<table >';
							
							foreach($arrTotales as $t)
							{
								$clase="";
								$clase="celdaValor";
								if($t->idConcepto==12)
									$clase="celdaTotal";
									
								$lblConcepto=$t->etiqueta;
								if($lblConcepto=="")
								{
									$consulta="SELECT descripcion FROM 711_catalogoImpuestosRetenciones WHERE idConcepto=".$t->idConcepto;	
									$lblConcepto=$con->obtenerValor($consulta);
								}
								
								
								if(($t->tasaConcepto!="")&&($t->tasaConcepto!=0))
									$lblConcepto.=" (".number_format($t->tasaConcepto,2)."%)";
								$conceptos.='
									<tr>
										<td width="100"  class="'.$clase.'" style="text-align:right"> 
										'.$lblConcepto.'
										</td>
										<td  width="100" align="right" class="'.$clase.'"   style="text-align:right">
										$ '.number_format($t->montoConcepto,2).'
										</td>
									</tr>';
							}
		$conceptos.=	'		</table>
							</td>
						</tr>
						</table>
					</td>
				</tr>
			</table>';
$html2="";				
if($objComprobante["folioUUID"]!="")				
{
	$html2=<<<EOD2
					
					
				
				<table style="border:none;" cellspacing="0">
					
					<tr>
						<td width="500" >
						<span style="font-size:11px; font-weight:bold">
						Cadena original del complemento de certificado digital del SAT<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["cadenaOriginalComplententoSAT"]}
						</span>
						</td>
						<td width="10"></td>
						<td width="195" rowspan="3" align="left" ><img src="$urlCBB" width="120" height="120"></td>
					</tr>
					
					<tr>
						<td  >
						<span style="font-size:11px; font-weight:bold">
						Sello digital del emisor<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["selloCFD"]}
						</span>
						</td>
						<td ></td>
						
					</tr>
					
					<tr>
						<td  >
						<span style="font-size:11px; font-weight:bold">
						Sello digital del SAT<br />
						</span>
						<span style="font-size:7px; ">
						{$objComprobante["SelloDigitalSAT"]}
						</span>
						</td>
						<td ></td>
			
					</tr>
				</table><br><br><br>
				<table width="100%">
					
					<tr>
						<td align="right"><br><br>
						<span class="leyenda">Esta es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			</td>
		</tr>
		</table>
EOD2;
}
else
{
	$html2=<<<EOD2
					
					
				
				<br><br><br>
				<table width="100%">
					
					<tr>
						<td align="right"><br><br>
						<span class="leyenda">Esta es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			</td>
		</tr>
		</table>
EOD2;
}

$html.=$conceptos.$html2;


//
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
if($descargarPDF)
{
	$pdf->Output($objComprobante["datosReceptor"]["rfc"].'.pdf', 'D');
	unlink($urlArchivoCBB);
	return;
}

if($almacenarPDF)
{
	$pdf->Output("../archivosTemporales/".$idComprobante.'.pdf', 'F');
	unlink($urlArchivoCBB);
	return;
}


?>

<body style="background-color:#FFF">
<table width="100%">
<tr>
	<td align="center">
		<table>
        	<tr>
            	<td align="right">
        <?php
			echo $html;
		?>
        		</td>
            </tr>
		</table>
    </td>
</tr>
</table>
</body>