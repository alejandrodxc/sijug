<?php
	include("latis/conexionBD.php");
	include_once("latis/numeroToLetra.php");
	include_once("latis/cfdi/cFactura.php");
	//header ("Content-Type:text/xml");
	$xml=base64_decode($_POST["xml"]);
	$f=new cFacturaCFDI();
	$objFactura=$f->convertirXMLCadenaToObj($xml);
	
?>
<html>
<head>
<style type="text/css">
.encabezado {
	text-align: center;
	font-weight: bold;
	color: #FFF;
	background-color: #000;
}
.empresa {
	text-align: center;
	font-weight: bold;
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 14px;
}
.datosEmpresa {
	font-family: Georgia, "Times New Roman", Times, serif;
}
.datosEmpresa {
	text-align: center;
	font-size: 12px;
}
.folio {
	font-weight: bold;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	background-color: #000;
	color: #FFF;
}
.datosConcepto {
	text-align: center;
	font-weight: bold;
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 10px;
	color: #FFF;
	background-color: #000;
}
.subtotal {
	text-align: right;
	font-weight: bold;
	color: #FFF;
	background-color: #000;
}
</style>
</head>
<body style="background-color:#FFF">


<?php
	
	
	$factura = new SimpleXMLElement($xml);
	$datosComprobante=$factura->xpath('//cfdi:Comprobante')[0];
	
	$arrFechas=explode("T",$datosComprobante["fecha"]);
	$fecha=convertirFechaLetra($arrFechas[0],true,true);
	
	$datosEmisor=$factura->xpath('//cfdi:Comprobante//cfdi:Emisor')[0];
	
	$domicilioReceptor=$factura->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal')[0];
	
	$municipioE=formatearValorArregloAsoc($domicilioReceptor,"municipio");
	$estadoE=formatearValorArregloAsoc($domicilioReceptor,"estado");
	$paisE=formatearValorArregloAsoc($domicilioReceptor,"pais");
	
	
	$calleE=formatearValorArregloAsoc("calle",$domicilioReceptor,false);	
	$noExteriorE=formatearValorArregloAsoc("noExterior",$domicilioReceptor,false);	
	$noInteriorE=formatearValorArregloAsoc("noInterior",$domicilioReceptor,false);	
	$coloniaE=formatearValorArregloAsoc("colonia",$domicilioReceptor,false);	
	$codigoPostalE=formatearValorArregloAsoc("codigoPostal",$domicilioReceptor,false);
	$domicilioEmisor="";
	if($calleE!="")
		$domicilioEmisor="Calle ".$calleE;
	if($noExteriorE!="")
		$domicilioEmisor.=" No. ".$noExteriorE;
	
	if($noInteriorE!="")
		$domicilioEmisor.=" Int. ".$noInteriorE;
		
	if($coloniaE!="")
		$domicilioEmisor.=" Colonia ".$coloniaE;	
	
	if($codigoPostalE!="")
		$domicilioEmisor.=" C.P. ".$codigoPostalE;	
	
	if($municipioE!="")
	{
		if($domicilioEmisor!="")
			$domicilioEmisor.=".<br>";
		$domicilioEmisor.=$municipioE;	
		
	}
	if($estadoE!="")
		$domicilioEmisor.=", ".$estadoE;
	if($paisE!="")
		$domicilioEmisor.=", ".$paisE;	
			
	$datosReceptor=$factura->xpath('//cfdi:Comprobante//cfdi:Receptor')[0];
	$domicilioReceptor=$factura->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio')[0];
	
	
	$municipio=formatearValorArregloAsoc($domicilioReceptor,"municipio");
	$estado=formatearValorArregloAsoc($domicilioReceptor,"estado");
	$pais=formatearValorArregloAsoc($domicilioReceptor,"pais");
	
	
	$calleRec=formatearValorArregloAsoc("calle",$domicilioReceptor,true);	
	$noExteriorRec=formatearValorArregloAsoc("noExterior",$domicilioReceptor);	
	$noInteriorRec=formatearValorArregloAsoc("noInterior",$domicilioReceptor);	
	$coloniaRec=formatearValorArregloAsoc("colonia",$domicilioReceptor,true);	
	$codigoPostalRec=formatearValorArregloAsoc("codigoPostal",$domicilioReceptor,true);	
	
	$direccion="Calle ".$calleRec;
	if($noExteriorRec!="")
		$direccion.=" No. ".$noExteriorRec;
	
	if($noInteriorRec!="")
		$direccion.=" No. Int. ".$noInteriorRec;
	$iva=0;
	
	$arrConceptos=$factura->xpath('//cfdi:Comprobante//cfdi:Concepto');
	
	$arrImpuestosTraslados=$factura->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado');
	foreach($arrImpuestosTraslados as $t);
	{
		if($t["impuesto"]=="IVA")	
			$iva+=$t["importe"];
	}
	
	if($datosComprobante["folio"]=="NO ASIGNADO")
		$datosComprobante["folio"]="";
	
?>
<table width="90%" border="1" align="center">
  <tr>
    <td colspan="2" class="encabezado">FACTURA: <?php echo $datosComprobante["serie"]." ".$datosComprobante["folio"]?></td>
  </tr>
  <tr>
    <td width="34%"><img src="../images/logoDemo.jpg" width="240" height="160" alt="logo">
</td>
    <td width="66%"><table width="100%" border="0">
      <tr>
        <td class="empresa"><?php echo $datosEmisor["nombre"]?></td>
      </tr>
      <tr>
        <td class="datosEmpresa"><?php echo $domicilioEmisor?></td>
      </tr>
      
        <td class="datosEmpresa">RFC: <?php echo $datosEmisor["rfc"]?></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="90%" border="1" align="center">
  <tr>
    <td width="34%" class="folio">Folio Fiscal:</td>
    <td width="66%">&nbsp;</td>
  </tr>
  <tr>
    <td class="folio">No. de Serie del Certificado del SAT</td>
    <td></td>
  </tr>
  <tr>
    <td class="folio">Fecha y Hora de Certificación:</td>
    <td><?php echo $datosComprobante["fecha"]?></td>
  </tr>
  <tr>
    <td class="folio">No. de Serie del Certificado del emisor:</td>
    <td><?php echo $datosComprobante["noCertificado"]?></td>
  </tr>
  <tr>
    <td colspan="2">Lugar y fecha de elaboración: <?php echo $datosComprobante["LugarExpedicion"].", ".$fecha ?></td>
  </tr>
  <tr>
    <td colspan="2">Forma de Pago: <?php echo $datosComprobante["formaDePago"]?></td>
  </tr>
</table>
<table width="90%" border="1" align="center">
  <tr>
    <td>Nombre: <?php echo $datosReceptor["nombre"]?></td>
    <td>RFC: <?php echo $datosReceptor["rfc"]?></td>
  </tr>
  <tr>
    <td>Dirección: <?php echo $direccion?></td>
    <td>CP: <?php echo $codigoPostalRec?></td>
  </tr>
  <tr>
    <td>Colonia: <?php echo $coloniaRec?></td>
    <td>Ciudad: <?php echo $municipio?></td>
  </tr>
  <tr>
    <td>Estado: <?php echo $estado?></td>
    <td>País: <?php echo $pais?></td>
  </tr>
</table>
<table width="90%" border="1" align="center">
  <tr>
    <td class="datosConcepto">Cantidad</td>
    <td class="datosConcepto">Descripción</td>
    <td class="datosConcepto">Unidad</td>
    <td class="datosConcepto">Precio</td>
    <td class="datosConcepto">Importe</td>
  </tr>
  <?php
  	foreach($arrConceptos as $c)
	{
  ?>
  <tr>
    <td><?php echo number_format(($c["cantidad"]*1),2)?></td>
    <td><?php echo $c["descripcion"]?></td>
    <td><?php echo $c["unidad"]?></td>
    <td><?php echo "$ ".number_format(($c["valorUnitario"]*1),2)?></td>
    <td><?php echo "$ ".number_format(($c["importe"]*1),2)?></td>
  </tr>
  <?php
	}
  ?>
</table>
<table width="90%" border="1" align="center">
  <tr>
    <td width="66%" class="encabezado">Importe en Letras</td>
    <td width="18%" class="subtotal">Subtotal:</td>
    <td width="16%"><?php echo "$ ".number_format($datosComprobante["subTotal"]*1,2)?></td>
  </tr>
  <tr>
    <td rowspan="3"><?php echo convertirNumeroLetra($datosComprobante["total"]*1)." ".str_pad(parteDecimal(($datosComprobante["total"]*1),false),2,"0",STR_PAD_LEFT)."/100"?></td>
    <td class="subtotal">Descuentos:</td>
    <td><?php echo "$ ".number_format($datosComprobante["descuento"]*1,2)?></td>
  </tr>
  <tr>
    <td class="subtotal">I.V.A.:</td>
    <td><?php echo "$ ".number_format($iva,2)?></td>
  </tr>
  <tr>
    <td class="subtotal">Total:</td>
    <td><?php echo "$ ".number_format($datosComprobante["total"]*1,2)?></td>
  </tr>
</table>
<table width="90%" border="1" align="center">
  <tr>
    <td colspan="2" class="encabezado">Sello Digital del CFDI</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="encabezado">Sello del SAT</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="encabezado">Cadena original del complemento de certificación digital del SAT</td>
  </tr>
  <tr>
    <td width="22%"><img src="../images/hologramaDemo.jpg" width="203" height="160"></td>
    <td width="78%">&nbsp;</td>
  </tr>
</table>
</body>
</html>
