<?php
	include_once('latis/tcpdf/tcpdf.inc.php');
	include_once('latis/conexionBD.php');
	
	include_once('latis/numeroToLetra.php');
	include_once('latis/cAlmacen.php');


	$fechaReporte=date('d-m-Y');
	$fechaR=date('Y-m-d');
	$idDatosF=-1;
	$fechaInicial='1970-08-07';
	$fechaFinal='1970-08-07';
	$idCliente=-1;
	
if(isset($_POST['idCliente']))
	$idCliente=$_POST['idCliente'];
	


$fechaInicial="01/09/2013";
$fechaFinal="31/12/2013";

$fechaI=cambiaraFechaMysql($fechaInicial);
$fechaF=cambiaraFechaMysql($fechaFinal);
$periodo="Del ".$fechaInicial." al ".$fechaFinal;




?>



<style type="text/css">
.encabezado {
	text-align: center;
	font-weight: bold;
	background-color: #000;
}
.encabezado {
	color: #FFF;
}
.subtotal {
	text-align: right;
	background-color: #000;
	color: #FFF;
}
.pie {
	text-align: center;
}
</style>
<table width="100%" border="0">
  <tr>
    <td width="56%">logo</td>
    <td width="44%"><table width="100%" border="1">
      <tr>
        <td class="encabezado">Folio Fiscal</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="encabezado">Factura Número</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="encabezado">No. de serie del CSD del emisor</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="encabezado">Fecha y hora de emisión</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="1">
  <tr class="encabezado">
    <td>Fecha y hora de certificado</td>
    <td>No. de serie del CSD del SAT</td>
    <td>Forma de pago</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td colspan="3" class="encabezado">Emisor</td>
  </tr>
  <tr>
    <td colspan="2">Razon social: INSTITUTO DE INNOVACION Y APRENDIZAJE A.C.</td>
    <td width="24%">RFC: IIA1301182XA</td>
  </tr>
  <tr>
    <td>Calle y número: Borbon # 7</td>
    <td>Colonia: Centro</td>
    <td>Ciudad: Coatepec</td>
  </tr>
  <tr>
    <td>Municipio o deleg:</td>
    <td>Estado:</td>
    <td>CP:</td>
  </tr>
  <tr>
    <td>País:</td>
    <td>Regimen Fiscal:</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="encabezado">
    <td colspan="3">Receptor</td>
  </tr>
  <tr>
    <td colspan="2">Razón social:</td>
    <td>RFC:</td>
  </tr>
  <tr>
    <td>Calle y número:</td>
    <td>Colonia:</td>
    <td>Ciudad:</td>
  </tr>
  <tr>
    <td>Municipio o deleg:</td>
    <td>Estado:</td>
    <td>CP:</td>
  </tr>
  <tr>
    <td>País:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1">
  <tr class="encabezado">
    <td>Cantidad</td>
    <td>Unidad de Medida</td>
    <td>Concepto</td>
    <td>Precio unitario</td>
    <td>Importe</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td width="63%">&nbsp;</td>
    <td width="37%"><table width="100%" border="1">
      <tr>
        <td width="40%" class="subtotal">Subtotal</td>
        <td width="60%">&nbsp;</td>
      </tr>
      <tr>
        <td class="subtotal">IVA 16%</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="subtotal">Total</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="100%" border="1">
  <tr>
    <td colspan="2">TOTAL EN LETRA:</td>
  </tr>
  <tr>
    <td width="41%">Metodo de pago:</td>
    <td width="59%">Número de cuenta:</td>
  </tr>
  <tr>
    <td>Condiciones de Pago:</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td width="70%"><table width="100%" border="1">
      <tr>
        <td class="encabezado">Cadena original del complemento de certificación digital del SAT</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="encabezado">Sello digital del emisor</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="encabezado">Sello digital del SAT</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td width="30%">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="1">
  <tr>
    <td><p class="pie">Este documento es una representación impresa de un CFDI</p>
    <p class="pie">Página 1 de 1</p></td>
  </tr>
</table>
<p>&nbsp;</p>
