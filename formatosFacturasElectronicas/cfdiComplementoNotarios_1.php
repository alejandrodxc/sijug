<?php include_once('latis/tcpdf/tcpdf.inc.php');

		include_once("latis/conexionBD.php"); 

		include_once("latis/cfdi/cFactura.php");
		include_once("latis/numeroToLetra.php");
		include_once("latis/cCodigoBarras.php");




ini_set('memory_limit', '500M');
$motivoDescuento="";
$descargarPDF=false;
if(isset($_POST["getPDF"]))		
	$descargarPDF=true;
	
	
	
$almacenarPDF=false;	

if(isset($_GET["almacenarPDF"]))		
{
	$almacenarPDF=true;
	
}	
	
$idComprobante=-1;
$xml="";
$objComprobante=NULL;
$c=new cFacturaCFDI();

$comentariosAdicionales="";
$arrTotales=array();
$arrConceptos=array();
$buscarUnidadMedida=true;


$objDatosNotario=array();

if(isset($_POST["iC"])||isset($_GET["idComprobante"]))		
{
	if(isset($_POST["iC"]))
		$idComprobante=bD($_POST["iC"]);
	else
		$idComprobante=($_GET["idComprobante"]);
	
	$objComprobante=$c->cargarComprobanteXMLObjeto($idComprobante);

	$motivoDescuento=$objComprobante["motivoDescuento"];
	$consulta="SELECT idReferencia,tipoUso FROM 703_relacionFoliosCFDI WHERE idFolio=".$idComprobante;

	$fDatosComprobante=$con->obtenerPrimeraFila($consulta);
	$idReferencia=$fDatosComprobante[0];
	
	
	switch($fDatosComprobante[1])
	{
		case 4:
		case 6:	
		
			$buscarUnidadMedida=false;
			$arrTotales=array();
			
			$baseConcepto='{"idConcepto":"","tasaConcepto":"","montoConcepto":"","etiqueta":""}';
			$oTotal=json_decode($baseConcepto);
			
			$subtotal=$objComprobante["subtotal"];
			$oTotal->idConcepto="7";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$subtotal;
			$oTotal->etiqueta="Subtotal";
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			$descuento=$objComprobante["descuento"];
			$oTotal->idConcepto="6";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$descuento;
			$oTotal->etiqueta="Descuento";			
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			
			$oIVA=NULL;
			
			foreach($objComprobante["impuestos"]["traslados"] as $traslado)
			{
				if($traslado["impuesto"]=="IVA")	
				{
					$oIVA=$traslado;
					break;	
				}
			}
			
			
			if($oIVA!=NULL)
			{
				$iva=0;
				$oTotal->tasaConcepto="16";
				if($oIVA)
				{
					$iva=$oIVA["importe"];
					$oTotal->tasaConcepto=$oIVA["tasa"];
				}
				$oTotal->idConcepto="1";			
				$oTotal->montoConcepto=$iva;
				$oTotal->etiqueta="IVA";
				array_push($arrTotales,$oTotal);
			}
			$oTotal=json_decode($baseConcepto);
			$total=$objComprobante["total"];
			$oTotal->idConcepto="12";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$total;
			$oTotal->etiqueta="Total";
			array_push($arrTotales,$oTotal);
			
			foreach($objComprobante["conceptos"] as $cAux)
			{
				$objTmp='{"clave":"","cabecera":"","descripcion":"","unidadMedida":"","costoUnitario":"","descuentoUnitario":"","cantidad":"","subtotal":"","iva":"","tasaIVA":"","total":"","descuentoTotal":""}';
				$cTmp=json_decode($objTmp);
				$cTmp->cAux="";
				$cTmp->cabecera="";
				$cTmp->descripcion=$cAux["descripcion"];
				$cTmp->unidadMedida=$cAux["unidad"];
				$cTmp->costoUnitario=$cAux["valorUnitario"];
				$cTmp->descuentoUnitario="";
				$cTmp->cantidad=$cAux["cantidad"];
				$cTmp->subtotal="";
				$cTmp->iva="";
				$cTmp->tasaIVA="";
				$cTmp->total=$cAux["importe"];
				$cTmp->descuentoTotal="";		
				array_push($arrConceptos,$cTmp);
			}
			
		break;
		
		default:
		
			$consulta="SELECT comentariosAdicionales FROM 706_comprobanteFactura WHERE idComprobanteFactura=".$idReferencia;
			$comentariosAdicionales=$con->obtenerValor($consulta);
			if(trim($comentariosAdicionales)!="")
				$comentariosAdicionales="(".$comentariosAdicionales.")";
			$consulta="SELECT tipoConcepto,idConcepto,tasaConcepto,montoConcepto,etiquetaConcepto FROM 709_impuestosRetencionesComprobante WHERE idComprobante=".$idReferencia." ORDER BY idImpuestoRetencion";
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				$oTotal=json_decode('{"tipoConcepto":"'.$fila[0].'","idConcepto":"'.$fila[1].'","tasaConcepto":"'.$fila[2].'","montoConcepto":"'.$fila[3].'","etiqueta":"'.cv($fila[4]).'"}');
				array_push($arrTotales,$oTotal);
			}
		
			$consulta="SELECT clave,descripcionConcepto,cabecera,unidadMedida,costoUnitario,descUnitario,cantidad,subtotal,totalIVA,tasaIVA,total,totalDescuento FROM 707_conceptosFactura WHERE idComprobante=".$idReferencia;
			
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				$objTmp='{"clave":"","cabecera":"","descripcion":"","unidadMedida":"","costoUnitario":"","descuentoUnitario":"","cantidad":"","subtotal":"","iva":"","tasaIVA":"","total":"","descuentoTotal":""}';
				$cTmp=json_decode($objTmp);
				$cTmp->clave=$fila[0];
				$cTmp->cabecera=$fila[2];
				$cTmp->descripcion=$fila[1];
				$cTmp->unidadMedida=$fila[3];
				$cTmp->costoUnitario=$fila[4];
				$cTmp->descuentoUnitario=$fila[5];
				$cTmp->cantidad=$fila[6];
				$cTmp->subtotal=$fila[7];
				$cTmp->iva=$fila[8];
				$cTmp->tasaIVA=$fila[9];
				$cTmp->total=$fila[10];
				$cTmp->descuentoTotal=$fila[11];		
				array_push($arrConceptos,$cTmp);
			}
		
		break;	
	}
	
	$consulta="SELECT * FROM 724_datosComplementoNotario WHERE idComprobante=".$fDatosComprobante[0];
	$fNotario=$con->obtenerPrimeraFila($consulta);
	
	
	
	$consulta="SELECT idReferencia FROM 687_certificadosSelloDigital WHERE idCertificado=".$objComprobante["idCertificado"];
	$idEmpresa=$con->obtenerValor($consulta);

	$consulta="SELECT noNotaria,curp,entidadFederativa,adscripcion FROM _1026_tablaDinamica WHERE empresa=".$idEmpresa;
	$fDatosNotaria=$con->obtenerPrimeraFila($consulta);
	
	
	$arrDatosInmueble="";
	$consulta="SELECT * FROM 725_inmueblesComplementoNotario WHERE idComprobante=".$fDatosComprobante[0];

	$res=$con->obtenerFilas($consulta);
	while($f=mysql_fetch_row($res))
	{
		$oAux='{"idTipoInmueble":"'.$f[2].'","calle":"'.cv($f[3]).'","noExterior":"'.cv($f[4]).'","noInterior":"'.cv($f[5]).'","colonia":"'.cv($f[6]).'","localidad":"'.cv($f[7]).
				'","referencia":"'.cv($f[8]).'","municipio":"'.cv($f[9]).'","estado":"'.cv($f[10]).'","pais":"'.cv($f[11]).'","cp":"'.$f[12].'"}';
		if($arrDatosInmueble=="")
			$arrDatosInmueble=$oAux;
		else
			$arrDatosInmueble.=",".$oAux;
	}
	
	$arrDatosEnajenante="";
	
	$consulta="SELECT (nombre),(apellidoPaterno),(apellidoMaterno),UPPER(rfc),UPPER(curp),porcentaje,tipoPersona FROM 726_involucradosComplementoNotario 
				WHERE idComprobante=".$fDatosComprobante[0]." AND tipoInvolucrado=1";
	$res=$con->obtenerFilas($consulta);
	while($f=mysql_fetch_row($res))
	{
		$oAux='{"apPaterno":"'.cv($f[1]).'","apMaterno":"'.cv($f[2]).'","nombre":"'.cv($f[0]).'","rfc":"'.cv($f[3]).'","curp":"'.cv($f[4]).'","tipoPersona":"'.$f[6].
				'","porcentaje":"'.cv($f[5]).'"}';
		if($arrDatosEnajenante=="")
			$arrDatosEnajenante=$oAux;
		else
			$arrDatosEnajenante.=",".$oAux;
	}
	
	$arrDatosAdquiriente="";
	
	
	$consulta="SELECT (nombre),(apellidoPaterno),(apellidoMaterno),UPPER(rfc),UPPER(curp),porcentaje,tipoPersona FROM 726_involucradosComplementoNotario 
				WHERE idComprobante=".$fDatosComprobante[0]." AND tipoInvolucrado=2";
	$res=$con->obtenerFilas($consulta);
	while($f=mysql_fetch_row($res))
	{
		$oAux='{"apPaterno":"'.cv($f[1]).'","apMaterno":"'.cv($f[2]).'","nombre":"'.cv($f[0]).'","rfc":"'.cv($f[3]).'","curp":"'.cv($f[4]).'","tipoPersona":"'.$f[6].
				'","porcentaje":"'.cv($f[5]).'"}';
		if($arrDatosAdquiriente=="")
			$arrDatosAdquiriente=$oAux;
		else
			$arrDatosAdquiriente.=",".$oAux;
	}
	
	
	$cDatosNotario='{"incluirComplemento":"'.$fNotario[7].'","datosNotaria":{"noNotaria":"'.$fDatosNotaria[0].'","curpNotario":"'.$fDatosNotaria[1].'","entidadFederativa":"'.$fDatosNotaria[2].
					'","adscripcion":"'.cv($fDatosNotaria[3]).'"},"datosOperacion":{"noInstrumentoNotarial":"'.$fNotario[3].'","fechaInstrumentoNotarial":"'.$fNotario[2].'","subtotal":"'.
					$fNotario[5].'","iva":"'.$fNotario[6].'","total":"'.$fNotario[4].'"},"datosInmueble":['.$arrDatosInmueble.'],"tipoPropietario":"'.$fNotario[8].'","datosEnajenante":['.$arrDatosEnajenante.
					'],"tipoAdquiriente":"'.$fNotario[9].'","datosAdquiriente":['.$arrDatosAdquiriente.']}';
	$objDatosNotario=json_decode($cDatosNotario);
	
	
}
else
{
	
	if(isset($_POST["xml"]))
	{
		$buscarUnidadMedida=false;
		$xml=bD($_POST["xml"]);
		
		$cadObj=bD($_POST["cadObj"]);
		$oComprobante=NULL;
		if(isset($_POST["objSerializado"]))
			$oComprobante=unserialize($cadObj);
		else
			$oComprobante=json_decode($cadObj);
		
		
		
		$motivoDescuento="";
		if(!isset($_POST["objSerializado"]))
			$motivoDescuento=$oComprobante->motivoDescuento;
		else
			$motivoDescuento=$oComprobante["motivoDescuento"];
			
		$comentariosAdicionales="";	
		if(!isset($_POST["objSerializado"]))	
			
			$comentariosAdicionales=($oComprobante->comentariosAdicionales);
		else	
			$comentariosAdicionales=($oComprobante["comentariosAdicionales"]);
		
		
		if(trim($comentariosAdicionales)!="")
			$comentariosAdicionales="(".$comentariosAdicionales.")";
		
		
		$oComp=array();
		
		if(!isset($_POST["objSerializado"]))
		{
			$buscarUnidadMedida=true;
			foreach($oComprobante->arrConceptos as $cTmp)
			{
				$cTmp->cabecera=urldecode($cTmp->cabecera);
				array_push($arrConceptos,$cTmp);
			}
			$oComp["idCertificado"]=$oComprobante->idCertificado;
			$oComp["idSerie"]=$oComprobante->idSerie;
			$oComp["idClienteFactura"]=$oComprobante->idCliente;
			$arrTotales=$oComprobante->arrTotales;
			
			$objDatosNotario=$oComprobante->datosComplemento;
	
			
			/*
			
			stdClass Object
(
    [incluirComplemento] => 1
    [datosNotaria] => stdClass Object
        (
            [noNotaria] => 12
            [curpNotario] => CURP
            [entidadFederativa] => 30
            [adscripcion] => UNDÉCIMA DEMARCACIÓN
        )

    [datosOperacion] => stdClass Object
        (
            [noInstrumentoNotarial] => 57651
            [fechaInstrumentoNotarial] => 2015-03-31
            [subtotal] => 763510
            [iva] => 0
            [total] => 763510
        )

    [datosInmueble] => Array
        (
            [0] => stdClass Object
                (
                    [idTipoInmueble] => 02
                    [calle] => toluca
                    [noExterior] => 1207
                    [noInterior] => 
                    [colonia] => Progreso
                    [localidad] => Xalapa
                    [referencia] => 
                    [municipio] => Xalapa
                    [estado] => 30
                    [pais] => MEX
                    [cp] => 91130
                )

        )

    [tipoPropietario] => 2
    [datosEnajenante] => Array
        (
            [0] => stdClass Object
                (
                    [apPaterno] => GONZÁLEZ
                    [apMaterno] => QUIJANO
                    [nombre] => FAUSTNO
                    [rfc] => RFC
                    [curp] => CURP
                    [tipoPersona] => 3
                    [porcentaje] => 50
                )

            [1] => stdClass Object
                (
                    [apPaterno] => SÁNCHEZ
                    [apMaterno] => CAMACHO
                    [nombre] => MARIA SUSANA
                    [rfc] => RFC
                    [curp] => CURP
                    [tipoPersona] => 3
                    [porcentaje] => 50
                )

        )

    [tipoAdquiriente] => 1
    [datosAdquiriente] => Array
        (
            [0] => stdClass Object
                (
                    [apPaterno] => GONZÁLEZ
                    [apMaterno] => SANCHEZ
                    [nombre] => ELISA
                    [rfc] => RFC
                    [curp] => CURP
                    [tipoPersona] => 1
                    [porcentaje] => 
                )

        )

)
			
			*/
			
			
		}
		else
		{
			
			foreach($oComprobante["conceptos"] as $cTmp)
			{
				
				$oTmp=json_decode('{"cabecera":"","cantidad":"'.$cTmp["cantidad"].'","unidadMedida":"'.$cTmp["unidad"].'","noIdentificacion":"'.$cTmp["noIdentificacion"].
								'","descripcion":"'.$cTmp["descripcion"].'","costoUnitario":"'.$cTmp["valorUnitario"].'","total":"'.$cTmp["importe"].'"}');
				
				
				array_push($arrConceptos,$oTmp);
			}
			$oComp["idCertificado"]=$oComprobante["idCertificado"];
			$oComp["idSerie"]=$oComprobante["idSerie"];
			$oComp["idClienteFactura"]=$oComprobante["idClienteFactura"];
			
			$arrTotales=array();
			
			$baseConcepto='{"idConcepto":"","tasaConcepto":"","montoConcepto":"","etiqueta":""}';
			$oTotal=json_decode($baseConcepto);
			
			$subtotal=$oComprobante["subtotal"];
			$oTotal->idConcepto="7";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$subtotal;
			$oTotal->etiqueta="Subtotal";
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			$descuento=$oComprobante["descuento"];
			$oTotal->idConcepto="6";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$descuento;
			$oTotal->etiqueta="Descuento";			
			array_push($arrTotales,$oTotal);
			
			$oTotal=json_decode($baseConcepto);
			
			$oIVA=NULL;
			$iva=0;
			foreach($oComprobante["impuestos"]["traslados"] as $traslado)
			{
				if($traslado["impuesto"]=="IVA")	
				{
					$oIVA=$traslado;
					$iva+=$traslado["importe"];
				}
			}
			
			$oTotal->tasaConcepto="16";
			/*if($oIVA)
			{
				//$iva=$oIVA["importe"];
				$oTotal->tasaConcepto=$oIVA["tasa"];
			}*/
			
			if($oIVA!=NULL)
			{
				$oTotal->idConcepto="1";			
				$oTotal->montoConcepto=$iva;
				$oTotal->etiqueta="IVA";
				array_push($arrTotales,$oTotal);
			}
			$oTotal=json_decode($baseConcepto);
			$total=$oComprobante["total"];
			$oTotal->idConcepto="12";
			$oTotal->tasaConcepto="";
			$oTotal->montoConcepto=$total;
			$oTotal->etiqueta="Total";
			array_push($arrTotales,$oTotal);

		}
		
		
		
		
		
		$objComprobante=$c->convertirXMLCadenaToObj($xml,$oComp);
	}
}






$consulta="SELECT idReferencia FROM 687_certificadosSelloDigital WHERE idCertificado=".$objComprobante["idCertificado"];
$idEmpresa=$con->obtenerValor($consulta);
$consulta="SELECT logoEmpresa FROM 6927_empresas WHERE idEmpresa=".$idEmpresa;
$idImagen=$con->obtenerValor($consulta);
$regimenFiscal="";
foreach($objComprobante["datosEmisor"]["regimenFiscal"] as $r)
{
	if($regimenFiscal=="")
		$regimenFiscal=$r;
	else
		$regimenFiscal.=", ".$r;
}

$domicilio="";
if($objComprobante["datosEmisor"]["domicilio"]["calle"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["calle"];
else
	$domicilio.=" N/E";
$domicilio.=" No.";	
if($objComprobante["datosEmisor"]["domicilio"]["noExterior"]!="")
	$domicilio.=" ".$objComprobante["datosEmisor"]["domicilio"]["noExterior"];
else
	$domicilio.=" N/E";

if($objComprobante["datosEmisor"]["domicilio"]["noInterior"]!="")
	$domicilio.=" Int. ".$objComprobante["datosEmisor"]["domicilio"]["noInterior"];

$domicilio.=" Colonia:";	
if($objComprobante["datosEmisor"]["domicilio"]["colonia"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["colonia"]."";	
else
	$domicilio.=" N/E.";
	
$domicilio.=" C.P.:";	
if($objComprobante["datosEmisor"]["domicilio"]["codigoPostal"]!="")
	$domicilio.="  ".$objComprobante["datosEmisor"]["domicilio"]["codigoPostal"].".";	
else
	$domicilio.=" N/E.";

$domicilio2="";
if($objComprobante["datosEmisor"]["domicilio"]["municipio"]!="")
{
	$domicilio2=$objComprobante["datosEmisor"]["domicilio"]["municipio"];
}
if($objComprobante["datosEmisor"]["domicilio"]["estado"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=", ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
	else
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["estado"].".";
		
}

if($objComprobante["datosEmisor"]["domicilio"]["pais"]!="")
{
	
	if($domicilio2!="")
		$domicilio2.=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
	else
		$domicilio2=" ".$objComprobante["datosEmisor"]["domicilio"]["pais"].".";
		
}
else
{
	if($domicilio2!="")
		$domicilio2.=".";	
}

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetFont('helvetica', '', 10);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// add a page
//$style3 = array('width' => 0.1, 'cap' => 'butt', 'join' => 'round', 'dash' => '0', 'color' => array(0, 0, 0));



//$paramsLinea=TCPDF_STATIC::serializeTCPDFtagParameters(array(5,50,280,50,$style3));
$urlImagen="../images/s.gif";
$altoBase=110;
$ancho="200";
if($idImagen!="")
{
	$imagen=$baseDir."/documentosUsr/archivo_".$idImagen;
	$datosImagen=getimagesize($imagen);
	
	
	if($datosImagen[1]>$altoBase)
	{
		$porcentaje=$altoBase/$datosImagen[1];
		$ancho=$datosImagen[0]*$porcentaje;
	
	}
	else
	{
		
		$porcentaje=($datosImagen[1]/110);	
		$ancho=$datosImagen[0]*$porcentaje;
	}
	$urlImagen="../documentosUsr/archivo_".$idImagen;
	$idImagen=bE($idImagen);
	
	
	
}
$pdf->AddPage();
$datosEmisor="
			<span class='tituloEmpresa'><b>".$objComprobante["datosEmisor"]["razonSocial"]."</b></span><br>
			".$objComprobante["datosEmisor"]["rfc"]."<br>
			<u>Domicilio Fiscal</u><br>
			".$domicilio."<br>
			".$domicilio2."<BR>
			Tel.<br>
			
			";
$params=TCPDF_STATIC::serializeTCPDFtagParameters(array('../images/cbb.jpg','','',30,30));

$f=function($x)
	{
		$a=func_get_args();
		unset($a[0]);
		return call_user_func_array($x,$a);
	};
$d=0;


function formatearFecha($fecha)
{
	return date("d/m/Y",strtotime($fecha))	;
}

function formatearMoneda($monto)
{
	return '$ '.number_format($monto,2);
}
$texto="
<span>
<b>Factura No:</b> 
".$objComprobante["serie"]." ".$objComprobante["folio"]."<BR>
<b>FOLIO FISCAL (UUID)</b> 	<BR>
".$objComprobante["folioUUID"]."<BR>
<b>NO. DE SERIE DEL CERTIFICADO DEL SAT</b><BR>
".$objComprobante["noCertificadoSAT"]."<br>
<b>NO. DE SERIE DEL CERTIFICADO DEL EMISOR</b><BR>
".$objComprobante["noCertificado"]."<BR>
<b>FECHA Y HORA DE CERTIFICACION</b><BR>
".$objComprobante["fechaCertificacionSAT"]."<BR>
<b>FECHA Y HORA DE EMISIÓN DE CFDI</b><BR>
".$objComprobante["fechaComprobante"]."<BR>
</span>
";

$calleReceptor="";

if($objComprobante["datosReceptor"]["domicilio"]["calle"]!="")
	$calleReceptor.=$objComprobante["datosReceptor"]["domicilio"]["calle"];
else
	$calleReceptor.="N/E";
if($objComprobante["datosReceptor"]["domicilio"]["noExterior"]!="")
	$calleReceptor.=" No. ".$objComprobante["datosReceptor"]["domicilio"]["noExterior"];
	
if($objComprobante["datosReceptor"]["domicilio"]["noInterior"]!="")
	$calleReceptor.=" Int. ".$objComprobante["datosReceptor"]["domicilio"]["noInterior"];

$aTotal=explode(".",$objComprobante["total"]);

$cbbTotal=str_pad($aTotal[0],10,"0",STR_PAD_LEFT).".".str_pad($aTotal[1],6,"0",STR_PAD_RIGHT);

$cadenaCBB="?re=".$objComprobante["datosEmisor"]["rfc"]."&rr=".$objComprobante["datosReceptor"]["rfc"]."&tt=".$cbbTotal."&id=".$objComprobante["folioUUID"];

$cBarras=new cCodigoBarras($cadenaCBB,"QR","",1,2,60);

$nombreArchivoCBB=$cBarras->generarCodigoBarrasImagenArchivo();
$urlCBB="../archivosTemporalesCodigoBarras/".$nombreArchivoCBB;

$urlArchivoCBB=$baseDir."/archivosTemporalesCodigoBarras/".$nombreArchivoCBB;

//fciudadanaq


$tblInmueble="";


foreach($objDatosNotario->datosInmueble as $d)
{
	$consulta="SELECT upper(descripcion) FROM 728_tiposInmuebleComplementoNotario WHERE cveInmueble='".$d->idTipoInmueble."'";
	$tipoInmueble=$con->obtenerValor($consulta);
	
	$consulta="SELECT UPPER(estado) FROM 820_estados WHERE cveEstadoSAT='".$d->estado."'";
	$estado=$con->obtenerValor($consulta);
	
	$direccion=$d->calle;
	if($d->noExterior!="")	
		$direccion.=" NO. ".$d->noExterior;
		
	if($d->noInterior!="")	
		$direccion.=" NO. INT. ".$d->noInterior;	
	
	if($d->colonia!="")	
		$direccion.=" COLONIA ".$d->colonia;	
	
	if($d->localidad!="")	
		$direccion.=", ".$d->localidad;	
		
	if($d->municipio!="")	
		$direccion.=", ".$d->municipio;	
	
	if($d->cp!="")	
		$direccion.=", ".$d->cp;		
		
		
		
	
	$tblInmueble.='<tr><td class="celdaValor">'.($tipoInmueble).'</td><td class="celdaValor" align="left">'.($direccion).'</td><td class="celdaValor">'.$estado.'</td><td class="celdaValor">MÉXICO</td></tr>';
}


$tblEnajenante="";


foreach($objDatosNotario->datosEnajenante as $d)
{
	
	
	if(($d->porcentaje=="")||($d->porcentaje=="0"))
		$d->porcentaje="100";


	$d->porcentaje=number_format($d->porcentaje,2);
		
	$tblEnajenante.='<tr><td class="celdaValor" align="left">'.($d->nombre." ".$d->apPaterno." ".$d->apMaterno).'</td><td class="celdaValor">'.strtoupper($d->rfc).
					'</td><td class="celdaValor">'.strtoupper($d->curp).'</td><td class="celdaValor">'.$d->porcentaje.'</td></tr>';
}


$tblAdquiriente="";

foreach($objDatosNotario->datosAdquiriente as $d)
{
	if(($d->porcentaje=="")||($d->porcentaje=="0"))
		$d->porcentaje="100";


	$d->porcentaje=number_format($d->porcentaje,2);

		
	$tblAdquiriente.='<tr><td class="celdaValor" align="left">'.($d->nombre." ".$d->apPaterno." ".$d->apMaterno).'</td><td class="celdaValor">'.strtoupper($d->rfc).
					'</td><td class="celdaValor">'.strtoupper($d->curp).'</td><td class="celdaValor">'.$d->porcentaje.'</td></tr>';
}


$consulta="SELECT UPPER(estado) FROM 820_estados WHERE cveEstadoSAT='".$objDatosNotario->datosNotaria->entidadFederativa."'";
$entidadNotaria=$con->obtenerValor($consulta);
$fechaInstrumento=date("d/m/Y",strtotime($objDatosNotario->datosOperacion->fechaInstrumentoNotarial));
$montoOperacion='$ '.number_format($objDatosNotario->datosOperacion->total,2);
$subtotal='$ '.number_format($objDatosNotario->datosOperacion->subtotal,2);
$iva='$ '.number_format($objDatosNotario->datosOperacion->iva,2);

$consulta="SELECT codigoiso4217 FROM  603_tipoMoneda WHERE moneda='".$objComprobante["moneda"]."'";
$claveMoneda=$con->obtenerValor($consulta);

$arrFecha=explode("T",$objComprobante["fechaComprobante"]);
$fechaCFDI=strtotime($arrFecha[0]);

$fechaExpedicion=date("d",$fechaCFDI)." de ".$arrMesLetra[(date("m",$fechaCFDI)*1)-1]." de ".date("Y",$fechaCFDI);


$direccionCliente="";
$direccionCliente2="";


$domicilio="";
if($objComprobante["datosReceptor"]["domicilio"]["calle"]!="")
	$direccionCliente.=" ".$objComprobante["datosReceptor"]["domicilio"]["calle"];
else
	$direccionCliente.=" N/E";
$direccionCliente.=" No.";	
if($objComprobante["datosReceptor"]["domicilio"]["noExterior"]!="")
	$direccionCliente.=" ".$objComprobante["datosReceptor"]["domicilio"]["noExterior"];
else
	$direccionCliente.=" N/E";

if($objComprobante["datosReceptor"]["domicilio"]["noInterior"]!="")
	$direccionCliente.=" Int. ".$objComprobante["datosReceptor"]["domicilio"]["noInterior"];

$direccionCliente.=" Colonia:";	
if($objComprobante["datosReceptor"]["domicilio"]["colonia"]!="")
	$direccionCliente.="  ".$objComprobante["datosReceptor"]["domicilio"]["colonia"]."";	
else
	$direccionCliente.=" N/E.";
	
$direccionCliente.=" C.P.:";	
if($objComprobante["datosReceptor"]["domicilio"]["codigoPostal"]!="")
	$direccionCliente.="  ".$objComprobante["datosReceptor"]["domicilio"]["codigoPostal"].".";	
else
	$direccionCliente.=" N/E.";

$direccionCliente2="";
if($objComprobante["datosReceptor"]["domicilio"]["municipio"]!="")
{
	$direccionCliente2=$objComprobante["datosReceptor"]["domicilio"]["municipio"];
}
if($objComprobante["datosReceptor"]["domicilio"]["estado"]!="")
{
	
	if($direccionCliente2!="")
		$direccionCliente2.=", ".$objComprobante["datosReceptor"]["domicilio"]["estado"].".";
	else
		$direccionCliente2.=" ".$objComprobante["datosReceptor"]["domicilio"]["estado"].".";
		
}

if($objComprobante["datosReceptor"]["domicilio"]["pais"]!="")
{
	
	if($direccionCliente2!="")
		$direccionCliente2.=" ".$objComprobante["datosReceptor"]["domicilio"]["pais"].".";
	else
		$direccionCliente2=" ".$objComprobante["datosReceptor"]["domicilio"]["pais"].".";
		
}
else
{
	if($direccionCliente2!="")
		$direccionCliente2.=".";	
}

$direccionCliente.=" ".$direccionCliente2;

$celdaCampos="celdaCampos8";
$celdaValor="celdaCamposValor8";



$conceptos="";

foreach($arrConceptos as $c)
{
	if($buscarUnidadMedida)
	{
		$consulta="select unidadMedida FROM 6923_unidadesMedida where idUnidadMedida=".$c->unidadMedida;

		$c->unidad=$con->obtenerValor($consulta);
	}
	else
		$c->unidad=$c->unidadMedida;
	if($c->cabecera!="")
	{
		$conceptos.='<tr>
						<td class="celdaValor" style="text-align: center" colspan="5">'.$c->cabecera.'</td>
					</tr>
					';
	}
	
	$total=str_replace(",","",number_format($c->cantidad,2))*str_replace(",","",number_format($c->costoUnitario,2));
	
	$conceptos.='<tr>
						<td class="celdaValor" style="text-align: center">'.number_format($c->cantidad,2).'</td>
						<td   class="celdaValor">'.$c->unidad.'</td>
						<td  class="celdaValor" style="text-align: left">'.$c->descripcion.'</td>
						<td class="celdaValor" style="text-align: right">$ '.number_format($c->costoUnitario,2).'</td>
						<td  class="celdaValor" style="text-align: right">$ '.number_format($total,2).'</td>
					</tr>
					';

}


$importeLetra=convertirNumeroLetra($objComprobante["total"],true).' '.parteDecimal(number_format($objComprobante["total"],2));

if($motivoDescuento!="")
{
	$motivoDescuento='<BR><BR><b>MOTIVO DE DESCUENTO:</b><br><i>'.$motivoDescuento.'</i>';
}
							
							
$tblDesgloce.='				
							<table >';

							foreach($arrTotales as $t)
							{
								if(($t->idConcepto!=7)&&($t->idConcepto!=1)&&($t->idConcepto!=12))
									continue;
								$clase="";
								$clase="celdaValor";
								
									
								$lblConcepto=$t->etiqueta;
								if($lblConcepto=="")
								{
									$consulta="SELECT descripcion FROM 711_catalogoImpuestosRetenciones WHERE idConcepto=".$t->idConcepto;	
									$lblConcepto=$con->obtenerValor($consulta);
								}
								
								
								if(($t->tasaConcepto!="")&&($t->tasaConcepto!=0))
									$lblConcepto.=" (".number_format($t->tasaConcepto,2)."%)";
								
								$lblConcepto=strtoupper($lblConcepto);	
								if($t->idConcepto==1)	
								{
									$lblConcepto="IVA(IVA 16.00%)";
								}
									
								$tblDesgloce.='
									<tr>
										<td width="90"  class="celdaCamposValor" style="text-align:right"> 
										<b>'.$lblConcepto.'</b>
										</td>
										<td  width="100" align="right" class="celdaCamposValor"   style="text-align:right">
										$ '.number_format($t->montoConcepto,2).'
										</td>
									</tr>';
							}
		$tblDesgloce.=	'	</table>
							';

$html2="";				
if($objComprobante["folioUUID"]!="")				
{
	$html2=<<<EOD2
					
					
				<table width="100%">
				<tr>
					<td width="530">
						<table style="border:none;" cellspacing="0">
							<tr>
								<td  width="500">
								<span style="font-size:11px; font-weight:bold"><br>
								SELLO DIGITAL DEL CFDI<br />
								</span>
								</td>
								<td ></td>
								
							</tr>
							<tr>
								<td  class="celdaValor">
								<span style="font-size:7px; ">
								{$objComprobante["selloCFD"]}
								</span>
								</td>
								<td ></td>
								
							</tr>
							<tr>
								<td  width="500">
								<span style="font-size:11px; font-weight:bold"><br>
								SELLO DIGITAL DEL SAT<br />
								</span>
								</td>
								<td ></td>
					
							</tr>
							<tr>
								<td  class="celdaValor">
								<span style="font-size:7px; ">
								{$objComprobante["SelloDigitalSAT"]}
								</span>
								</td>
								<td ></td>
					
							</tr>
							
							<tr>
								<td width="500" >
								<span style="font-size:11px; font-weight:bold"><br>
								CADENA ORIGINAL DEL COMPLEMENTO DE CERTIFICADO DIGITAL DEL SAT<br />
								</span>
								
								</td>
								
							</tr>
							<tr>
								<td width="500" class="celdaValor">
								<span style="font-size:7px; ">
								{$objComprobante["cadenaOriginalComplententoSAT"]}
								</span>
								</td>
							</tr>
						</table>
						
					</td>
					<td>
						<br><br><br><br>
						<td width="195" rowspan="3" align="left" ><img src="$urlCBB" width="100" height="100"></td>
					</td>	
				</tr>
				</table>
						<br><br><br>
				<table width="100%">
					
					<tr>
						<td align="center"><br><br>
						<span class="leyenda">Este documento es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			
EOD2;
}
else
{
	$html2=<<<EOD2
					
					
				
				<br><br><br>
				<table width="100%">
					
					<tr>
						<td align="center"><br><br><br><br>
						<span class="leyenda">Este documento es una representación impresa de un CFDI</span>
						</td>
					</tr>
				</table>
				
	
			
EOD2;
}




$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
	.tituloEmpresa
	{
		font-size:11px;	
	}
	.celdaTitulo
	{
		background-color:#FFF;
		color:#000;
		font-weight:bold;
		font-size:9px;
		font-weight:bold;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		
		border-bottom-width:1px; 
		border-bottom-style:solid; 
		border-bottom-color:#000;
		
		border-left-width:1px; 
		border-left-style:solid; 
		border-left-color:#000;
		
		border-top-width:1px; 
		border-top-style:solid; 
		border-top-color:#000;
		
		border-right-width:1px; 
		border-right-style:solid; 
		border-right-color:#000;
		
	}
	
	.celdaCampos
	{
		background-color:#FFF;
		color:#000;
		font-weight:bold;
		font-size:9px;
		
		
	}
	
	.celdaCampos8
	{
		background-color:#FFF;
		color:#000;
		font-weight:bold;
		font-size:8px;
		
		
	}
	
	.celdaCamposValor
	{
		background-color:#FFF;
		color:#000;
		font-size:9px;
		
		
	}
	
	.celdaCamposValor8
	{
		background-color:#FFF;
		color:#000;
		font-size:8px;
		
		
	}
	
	.celdaValor
	{
		color:#000;
		font-size:8px;
		text-align:center;
		padding-top:3px;
		padding-bottom:3px;
		border-bottom-width:1px; 
		border-bottom-style:solid; 
		border-bottom-color:#000;
		
		border-left-width:1px; 
		border-left-style:solid; 
		border-left-color:#000;
		
		border-top-width:1px; 
		border-top-style:solid; 
		border-top-color:#000;
		
		border-right-width:1px; 
		border-right-style:solid; 
		border-right-color:#000;
		
	}
	
	.celdaTotal
	{
		color:#000;
		font-size:10px;
		text-align:right;
		padding-top:3px;
		padding-bottom:3px;
		background-color:#DDD;
	}
	
	.celdaFolio
	{
		font-size:8px; 
		border-bottom-width:1px; 
		border-bottom-style:solid; 
		border-bottom-color:#000;
		
		border-left-width:1px; 
		border-left-style:solid; 
		border-left-color:#000;
		
		border-top-width:1px; 
		border-top-style:solid; 
		border-top-color:#000;
		
		border-right-width:1px; 
		border-right-style:solid; 
		border-right-color:#000;
	}
	
	.leyenda
	{
		font-size:11px;
	}
</style>
<body>
	<table width="100%">
	<tr>
		<td align="left">
			<table>
				<tr>
					<td align="center" width="150">
						<img src="$urlImagen" width="$ancho" height="$altoBase">
					</td>
					<td width="5"></td>
					<td style="font-size:8px" text-align="left" width="330">
					$datosEmisor
					</td>
					<td align="center" class="celdaFolio" >
						$texto
					</td>
				</tr>
			</table>
			<br><br>
			
			<table >
				<td  align="left" valign="top">
					<table >
						<tr>
							<td width="230">
								<table>
									<tr>
										<td width="60" class="$celdaCampos" valign="top">
											CLIENTE:
										</td>
										<td width="170" class="$celdaValor" valign="top">
										{$objComprobante["datosReceptor"]["razonSocial"]}
										</td>
									</tr>
									
									<tr>
										<td   class="$celdaCampos" valign="top">
											RFC:
										</td>
										<td  class="$celdaValor" valign="top">
										{$objComprobante["datosReceptor"]["rfc"]}
										</td>
									</tr>
									<tr>
										<td   class="$celdaCampos" valign="top">
											DIRECCIÓN:
										</td>
										<td  class="$celdaValor" valign="top">$direccionCliente
										</td>
									</tr>
									
								</table>
								
							</td>
							<td  valign="top">
								<table>
									<tr>
										<td class="$celdaCampos" width="100">Régimen fiscal:</td><td width="3"></td><td  class="$celdaValor" colspan="4">$regimenFiscal</td>
									</tr>
									<tr>
										<td class="$celdaCampos" width="100">Lugar de expedición:</td><td width="3"></td><td width="130" class="$celdaValor">{$objComprobante["lugarExpedicion"]}</td><td width="3"></td><td class="$celdaCampos" width="100">Fecha de expedición</td><td width="3"></td><td width="130" class="$celdaValor">$fechaExpedicion</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Forma de pago:</td><td width="3"></td><td  class="$celdaValor">{$objComprobante["formaPago"]}</td><td width="3"></td><td class="$celdaCampos" >Clave de moneda:</td><td width="3"></td><td  class="$celdaValor">$claveMoneda</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Método de pago:</td><td width="3"></td><td  class="$celdaValor">{$objComprobante["metodoDePago"]}</td><td width="3"></td><td class="$celdaCampos" >No. de notaría:</td><td width="3"></td><td class="$celdaValor">{$objDatosNotario->datosNotaria->noNotaria}</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Entidad de la notaria:</td><td width="3" class="$celdaValor"></td><td class="$celdaValor">$entidadNotaria</td><td width="3"></td><td class="$celdaCampos" >CURP de notario:</td><td width="3"></td><td  class="$celdaValor">{$objDatosNotario->datosNotaria->curpNotario}</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Adscripcion:</td><td width="3"></td><td  class="$celdaValor">{$objDatosNotario->datosNotaria->adscripcion}</td><td width="3"></td><td class="$celdaCampos" >No. de instrumento notarial:</td><td width="3"></td><td  class="$celdaValor">{$objDatosNotario->datosOperacion->noInstrumentoNotarial}</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Fecha del instrumento:</td><td width="3"></td><td  class="$celdaValor">{$fechaInstrumento}</td><td width="3"></td><td class="$celdaCampos" >Monto de la operación:</td><td width="3"></td><td  class="$celdaValor">$montoOperacion</td>
									</tr>
									<tr>
										<td class="$celdaCampos">Subtotal:</td><td width="3"></td><td  class="$celdaValor">$subtotal</td><td width="3"></td><td class="$celdaCampos" >IVA:</td><td width="3"></td><td  class="$celdaValor">$iva</td>
									</tr>
								</table>

							
							
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<br><br>
								<table>
									<tr>
										<td width="180" class="celdaTitulo">TIPO DE INMUEBLE</td>
										<td  width="230" class="celdaTitulo">DIRECCIÓN</td>
										<td width="120" class="celdaTitulo">ESTADO</td>
										<td width="120" class="celdaTitulo">PAÍS</td>
					
									</tr>$tblInmueble
								</table><BR><br>
								<table>
									<tr>
										<td width="310" class="celdaTitulo">NOMBRE DEL ENAJENANTE</td>
										<td  width="120" class="celdaTitulo">RFC</td>
										<td width="120" class="celdaTitulo">CURP</td>
										<td width="100" class="celdaTitulo">PORCENTAJE</td>
					
									</tr>$tblEnajenante
								</table><BR><br>
								<table>
									<tr>
										<td width="310" class="celdaTitulo">NOMBRE DEL ADQUIRIENTE</td>
										<td  width="120" class="celdaTitulo">RFC</td>
										<td width="120" class="celdaTitulo">CURP</td>
										<td width="100" class="celdaTitulo">PORCENTAJE</td>
					
									</tr>$tblAdquiriente
								</table><BR><br>
								
								<table>
								<tr>
									<td width="90" class="celdaTitulo">CANTIDAD</td>
									<td  width="100" class="celdaTitulo">UNIDAD DE MEDIDA</td>
									<td width="290" class="celdaTitulo">DESCRIPCI&Oacute;N</td>
									<td width="80" class="celdaTitulo">P. UNITARIO</td>
									<td width="80" class="celdaTitulo">IMPORTE</td>
								</tr>$conceptos
								<tr>
									<td colspan="5" class="celdaCamposValor" align="left"><br><br>
									
										<table>
											<tr>
												<td width="450" class="celdaCamposValor">
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>IMPORTE CON LETRA: $importeLetra/100 M.N.</B><BR><BR>
													$comentariosAdicionales
													$motivoDescuento

												</td>
												<td>
												$tblDesgloce
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
								 	<td colspan="5" align="left">
										$html2
									
									</td>
								</tr>
								</table>	

							</td>
						</tr>
					</table>
				</td>
				<td>
				</td>
			</table>
			
			<br>
		</td>
	</tr>
	</table>	
			
			
EOF;




//
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
if($descargarPDF)
{
	$pdf->Output($objComprobante["datosReceptor"]["rfc"].'.pdf', 'D');
	unlink($urlArchivoCBB);
	return;
}

if($almacenarPDF)
{
	$pdf->Output("../archivosTemporales/".$idComprobante.'.pdf', 'F');
	unlink($urlArchivoCBB);
	return;
}


?>

<body style="background-color:#FFF">
<table width="100%">
<tr>
	<td align="center">
		<table>
        	<tr>
            	<td align="right">
        <?php
			echo $html;
		?>
        		</td>
            </tr>
		</table>
    </td>
</tr>
</table>
</body>