<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var arrCertificados;

function obtenerCertificados(cmb)
{
	var idEmpresa=cmb.options[cmb.selectedIndex].value;
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
           
            arrCertificados=eval(arrResp[1]);
            llenarCombo(gE('_idCertificadoint'),arrCertificados,true);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=62&idEmpresa='+idEmpresa,true);
}

function obtenerSeries(cmb)
{
	var idCertificado=cmb.options[cmb.selectedIndex].value;
    
    var pos=existeValorMatriz(arrCertificados,idCertificado);
    var arrSeries=arrCertificados[pos][2];
    llenarCombo(gE('_idSerieint'),arrSeries,true);
}