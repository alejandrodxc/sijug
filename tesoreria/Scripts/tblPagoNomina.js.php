<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT ciclo,ciclo FROM 550_cicloFiscal ORDER BY ciclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT ciclo FROM 550_cicloFiscal where status=1";
	$cicloActivo=$con->obtenerValor($consulta);
	
	
	
		
?>

var arrMeses=new Array();
<?php
	$ct=0;
	foreach($arrMesLetra as $mes)
	{
		echo "arrMeses[".$ct."]='".$mes."';";
		$ct++;
	}
?>

Ext.onReady(inicializar);

function inicializar()
{

	var cmbCiclo=crearComboExt('cmbCiclo',<?php echo $arrCiclo?>,0,0,150);	
	<?php
		if($cicloActivo!="")
		{
	?>
    cmbCiclo.setValue('<?php echo $cicloActivo?>');
   <?php
		}
   ?>

	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idNomina'},
		                                                {name: 'nQuincena', type: 'int'},
														{name: 'noQuincena'},
                                                        {name: 'descripcion'},
                                                        {name: 'inicioPeriodo',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'finPeriodo',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'nombrePerfil'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nQuincena', direction: 'ASC'},
                                                            groupField: 'nombrePerfil',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='1';
                                        proxy.baseParams.ciclo=cmbCiclo.getValue();
                                    }
                        )   
       
	var expander = new Ext.ux.grid.RowExpander({
                                                column:3,
                                                tpl : new Ext.Template(
                                                    '<span class="copyrigthSinPadding">{descripcion}</span>'
                                                )
                                            });       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            chkRow,
                                                            expander,
                                                            
                                                            {
                                                                header:'No. quincena',
                                                                width:100,
                                                                sortable:true,
                                                                dataIndex:'noQuincena'
                                                            },
                                                            {
                                                                header:'Mes',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'mes',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return arrMeses[Math.ceil(registro.get('nQuincena')/2)-1];
                                                                        }
                                                            },
                                                            {
                                                                header:'Periodo del:',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'inicioPeriodo',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Al:',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'finPeriodo',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y')
                                                                        }
                                                            },
                                                            {
                                                                header:'Nombre perfil',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'nombrePerfil'
                                                            },
                                                            {
                                                                header:'',
                                                                width:200
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridNomina',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                width:850,
                                                                height:400,
                                                                sm:chkRow,
                                                                renderTo:'tblQuincena',
                                                                plugins:[expander],
                                                                columnLines : true,
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                				html:'<span class="letraRojaSubrayada8"><b>Ciclo:</b></span>&nbsp;&nbsp;'
                                                                            },
                                                                            cmbCiclo,'-',
                                                                            {
                                                                                icon:'../images/coins_add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Ingresar al módulo de pago de n&oacute;mina',
                                                                                handler:function()
                                                                                        {
                                                                                            var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(fila==null)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la n&oacute;mincon con la cual desea ingresar al m&oacute;dulo de pago de n&oacute;mina');
                                                                                                return;
                                                                                            }
                                                                                            var arrParam=[['idNomina',fila.get('idNomina')]];
                                                                                            enviarFormularioDatos('../tesoreria/pagoNomina.php',arrParam);
                                                                                        }
                                                                                
                                                                            }
                                                                		],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :true
                                                                                                })
                                                            }
                                                        );

}