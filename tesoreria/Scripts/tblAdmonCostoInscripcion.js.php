<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$plantel=$_SESSION["codigoInstitucion"];
	$consulta="SELECT idProgramaEducativo,nombreProgramaEducativo FROM 4500_programasEducativos where idProgramaEducativo 
			in (SELECT DISTINCT p.idProgramaEducativo FROM 4513_instanciaPlanEstudio i,4500_planEstudio p WHERE p.idPlanEstudio=i.idPlanEstudio AND i.sede='".$plantel."')";
	$arrProgramas=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares where situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama  WHERE institucion=1 ORDER BY unidad";
	$arrPlanteles=$con->obtenerFilasArreglo($consulta);

	$arrColumnas='';
	$arrCampos='';
	
	
	$consulta="select unidad from 817_organigrama where codigoUnidad='".$plantel."'";
	$nomPlantel=$con->obtenerValor($consulta);

	$consulta="SELECT DISTINCT i.idPeriodicidad FROM 4513_instanciaPlanEstudio i,4500_planEstudio p WHERE p.idPlanEstudio=i.idPlanEstudio AND i.sede='".$plantel."' and i.situacion=1";
	$listPeriodos=$con->obtenerListaValores($consulta);
	if($listPeriodos=="")
		$listPeriodos=-1;
	$consulta="SELECT id__464_gridPeriodos,concat(txtDescripcion,': ',nombrePeriodo) FROM _464_gridPeriodos g,_464_tablaDinamica t WHERE 
					g.idReferencia=t.id__464_tablaDinamica and t.id__464_tablaDinamica IN (".$listPeriodos.") order by txtDescripcion,nombrePeriodo";			
	$arrPeriodos=$con->obtenerFilasArreglo($consulta);
	
	

	$consulta="select idPerfilCosteo,funcionIniciacion FROM 6022_perfilesCosteo";
	$arrPerfiles=$con->obtenerFilasArreglo($consulta);


?>

var plantel='<?php echo $plantel?>';
var arrProgramas=<?php echo $arrProgramas?>;
var arrCiclo=<?php echo $arrCiclo?>;
var arrPlanteles=<?php echo $arrPlanteles?>;
var arrPeriodos=<?php echo $arrPeriodos?>;
var arrPerfiles=<?php echo $arrPerfiles?>;
var arrNodos=new Array();
var perfilCosteo='';
var consideraFechaVencimiento=0;

Ext.onReady(inicializar);
function inicializar()
{
	var gridPlanesEstudio=crearGridPlanesEstudio();
    var arrSeviciosNivel=[['1','Plantel'],['2','Programa Educativo'],['3','Plan de Estudios'],['4','Grado']];                    
	var cmbServiciosNivel=crearComboExt('cmbServiciosNivel',arrSeviciosNivel,0,0,200);
	cmbServiciosNivel.setValue('1');
    cmbServiciosNivel.on('select',function(cmb,registro)	
    								{
                                    	gEx('cmbPeriodo').getStore().removeAll();
                                    	switch(registro.data.id)
                                        {
                                        	case '1':
                                            	gEx('cmbProgramaEducativo').reset();
                                            	gEx('cmbProgramaEducativo').disable();
                                                gEx('cmbPlanEstudio').reset();
                                            	gEx('cmbPlanEstudio').disable();
                                                gEx('cmbPeriodo').reset();
                                                gEx('cmbPeriodo').enable();
                                                function funcAjax()
                                                {
                                                    var resp=peticion_http.responseText;
                                                    arrResp=resp.split('|');
                                                    if(arrResp[0]=='1')
                                                    {
                                                        gEx('cmbPeriodo').getStore().loadData(eval(arrResp[1]));
                                                    }
                                                    else
                                                    {
                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                    }
                                                }
                                                obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=142&plantel=<?php echo $plantel?>',true);
                                            break;
                                            case '2':
                                            	
                                            	gEx('cmbProgramaEducativo').enable();
                                                /*gEx('cmbPlanEstudio').reset();
                                            	gEx('cmbPlanEstudio').disable();*/
                                                gEx('cmbPeriodo').reset();
                                                gEx('cmbPeriodo').enable();
                                                if(gEx('cmbProgramaEducativo')!='')
                                                	dispararEventoSelectCombo('cmbProgramaEducativo');
                                            break;
                                            case '3':
                                            case '4':
                                            	
                                            	gEx('cmbProgramaEducativo').enable();
                                                /*gEx('cmbPlanEstudio').reset();
                                            	gEx('cmbPlanEstudio').enable();*/
                                                gEx('cmbPeriodo').reset();
                                                gEx('cmbPeriodo').enable();
                                                if(gEx('cmbProgramaEducativo')!='')
                                                	dispararEventoSelectCombo('cmbProgramaEducativo');
                                            break;
                                        }
                                        
                                        
                                        
                                        recargarGrid();
                                    }
    					)
   
   	var cmbProgramaEducativo=crearComboExt('cmbProgramaEducativo',arrProgramas,0,0,380);
    cmbProgramaEducativo.on('select',function(cmb,registro)
                                        {
                                            function funcAjax()
                                            {
                                                var resp=peticion_http.responseText;
                                                arrResp=resp.split('|');
                                                if(arrResp[0]=='1')
                                                {
                                                    var arrPeriodo=eval(arrResp[1]);
                                                    gEx('cmbPeriodo').getStore().loadData(arrPeriodo);
                                                    var arrDatos=eval(arrResp[2]);
                                                    gEx('cmbPlanEstudio').getStore().loadData(arrDatos);
                                                    recargarGrid();
                                                    gEx('cmbCiclo').focus();

                                                }
                                                else
                                                {
                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                }
                                            }
                                            obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=73&plantel='+plantel+'&idPrograma='+registro.get('id'),true);
                                            
                                      }
                            )
    cmbProgramaEducativo.disable();
   
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<span class="letraVino14N">Administraci&oacute;n de costos de Servicios</span>'
                                                        }
                                                    ],
                                            items:	[
                                                    
                                                        {
                                                            xtype:'panel',
                                                            region:'center',
                                                            layout:'border',
                                                            tbar:	[
                                                            			{
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span class="negrita12">Mostrar servicios asociados a:&nbsp;&nbsp;&nbsp;&nbsp;</span>'
                                                                        },
                                                                        cmbServiciosNivel,
                                                                        
                                                                        '-',
                                                                        {
                                                                            xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span class="negrita12">Programa Educativo / &Aacute;rea / Nivel acad&eacute;mico:&nbsp;&nbsp;&nbsp;</span>'
                                                                        },
                                                                        cmbProgramaEducativo
                                                                        
                                                                        
                                                                    ],
                                                            items:	[
                                                                        gridPlanesEstudio
                                                                    ]
                                                        }
                                    				 ]
										}
									]                                        
                                                     
						}
                    )   
}

function crearGridPlanesEstudio()
{
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,90);
    cmbCiclo.setValue('<?php echo $cicloActivo?>');
    cmbCiclo.on('select',function(cmb,registro)
                                {
                                	recargarGrid()
                                }
    					)
    
    var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodos,0,0,230);
	cmbPeriodo.on('select',function(cmb,registro)
                                {
                                	recargarGrid()
                                }
    					)
	
	var cmbPlanEstudio=crearComboExt('cmbPlanEstudio',[],0,0,410);
    cmbPlanEstudio.on('select',function(cmb,registro)
    							{
                                	recargarGrid()
                                }
    				)
	cmbPlanEstudio.disable();
    
	        
    var cargadorArbol=new Ext.tree.TreeLoader	(
                                                        {
                                                            autoLoad:false,
                                                            baseParams:{
                                                                            funcion:'74'
                                                                        },
                                                            dataUrl: '../paginasFunciones/funcionesPlanteles.php'
                                                        }	
		                                         )	
                                                 
                                                 
	cargadorArbol.on('beforeload',function(proxy)
    								{
                                    	var idProgramaEducativo=-1;
                                        if(gEx('cmbProgramaEducativo').getValue()!='')
                                        	idProgramaEducativo=gEx('cmbProgramaEducativo').getValue();
                                       
                                        proxy.baseParams.plantel=plantel;
                                        proxy.baseParams.idProgramaEducativo=idProgramaEducativo;
                                        proxy.baseParams.idServicioNivel=gEx('cmbServiciosNivel').getValue();
                                        var idPlanEstudio=-1;
                                        if(gEx('cmbPlanEstudio').getValue()!='')
                                        	idPlanEstudio=gEx('cmbPlanEstudio').getValue();
                                        proxy.baseParams.idPlanEstudio=idPlanEstudio;
                                        var idCiclo=-1;
                                        if(cmbCiclo.getValue()!='')
                                        	idCiclo=cmbCiclo.getValue();
                                        proxy.baseParams.idCiclo=idCiclo;
                                        var idPeriodo=-1;
                                        if(cmbPeriodo.getValue()!='')
                                        	idPeriodo=cmbPeriodo.getValue();
                                        proxy.baseParams.idPeriodo=idPeriodo;
                                        
                                        
                                        gEx('cmbConceptosSeleccionar').getStore().removeAll();
                                        gEx('cmbConceptosSeleccionar').setValue('');
                                        
                                        
                                    }
                        )          

	cargadorArbol.on('load',function(proxy,nodo,response)
    								{
                                    	var arrConceptos=[];

                                    	obtenerConceptosHijos(eval(response.responseText),arrConceptos);
                                        gEx('cmbConceptosSeleccionar').getStore().loadData(arrConceptos);
                                    	
                                    }
                    )                        
	
    var cmbConceptosSeleccionar=crearComboExt('cmbConceptosSeleccionar',[],0,0,320);    
    cmbConceptosSeleccionar.on('select',function(cmb,registro)
    									{
                                        	var x=0;
                                            var arbolCostos=gEx('arbolCostos');
                                            arbolCostos.checkAllNodes(false);
                                            for(x=0;x<registro.data.valorComp.length;x++)
                                            {
                                            	arbolCostos.checkNode(registro.data.valorComp[x],true);
                                            }
                                        
                                        }
    							)                                                             
                                                 
    var tree = new Ext.ux.tree.TreeGrid(	
    										{
                                            	id:'arbolCostos',
                                        		region:'center',
                                                enableSort:true,
                                                lines:true,
                                                border:false,
                                                stripeRows : true,
                                                borderWidth : Ext.isBorderBox ? 0 : 2, 
                                                cls : 'x-treegrid',
                                                columns:	[
                                                                {
                                                                    header:'',
                                                                    width:650,
                                                                    sortable:false,
                                                                    dataIndex:'text'
                                                                },
                                                                {
                                                                    header:'Costo <span id="btnModificarCosto" style="cursor:pointer; cursor: hand"><a onClick="asignarCosto()"><img width="13" height="13" src="../images/pencil.png" title="Modificar costo" alt="Modificar costo"/></a></span>',
                                                                    width:130,
                                                                    align:'center',
                                                                    css:'text-align:right !important;',
                                                                    sortable:false,
                                                                    dataIndex:'costo'
                                                                }
                                                			],
                                        		tbar:	[
                                                			
                                                            /*{
                                                                xtype:'label',
                                                                html:'&nbsp;&nbsp;<span class="negrita12">Plan de estudios:&nbsp;&nbsp;&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbPlanEstudio,'-',*/
                                                            {
                                                                xtype:'label',
                                                                html:'&nbsp;&nbsp;<span class="negrita12">Ciclo:&nbsp;&nbsp;&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbCiclo,'-',
                                                            {
                                                                xtype:'label',
                                                                html:'&nbsp;&nbsp;<span class="negrita12">Periodo:&nbsp;&nbsp;&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbPeriodo,
                                                            '-',
                                                            
                                                            {
                                                                icon:'../images/elbow-minus-nl.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Contraer',
                                                                handler:function()
                                                                        {
                                                                            tree.collapseAll();

                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                icon:'../images/elbow-plus-nl.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Expandir',
                                                                handler:function()
                                                                        {
                                                                            tree.expandAll();
                                                                        }
                                                                
                                                            },'-',
                                                            {
                                                                xtype:'label',
                                                                html:'<span class="negrita12">Seleccionar concepto:&nbsp;&nbsp;&nbsp;&nbsp;</span>'
                                                            },
                                                            cmbConceptosSeleccionar
                                                            
                                                            
                                                            
                                                        ],	
                                                loader: cargadorArbol
                                            }
									);

	tree.on('checkchange',function(nodo,valor)
    						{
                            	
                            	var arbolCostos=gEx('arbolCostos');
                                arrNodos=arbolCostos.getCheckNodes();
                                mE('btnModificarCosto');
                                perfilCosteo='';
                                consideraFechaVencimiento=0;
                                var x;
                                var llaveCompatibilidad='';
                                for(x=0;x<arrNodos.length;x++)
                                {
                                	if(perfilCosteo=='')
                                    {
                                    	perfilCosteo=arrNodos[x].attributes.perfilCosteo;
                                        llaveCompatibilidad=arrNodos[x].attributes.llaveCompatibilidad;
                                    }
                                    if((perfilCosteo!=arrNodos[x].attributes.perfilCosteo)||(llaveCompatibilidad!=arrNodos[x].attributes.llaveCompatibilidad))
                                    {
                                    	oE('btnModificarCosto');
                                    	return;
                                    }
                                    if(arrNodos[x].attributes.consideraFechaVencimiento=='1')
                                    {
                                    	consideraFechaVencimiento=1;
                                    }
                                }
                                
                            }
    		)                                    
    return 	tree;
    
    
                                
    return 	tree;
    
    
}


function obtenerConceptosHijos(nodos,arrConceptos)
{
	var x;

    for(x=0;x<nodos.length;x++)
    {
    	
    	if(nodos[x].esConcepto=='1')
        {
        	var arrDatos=nodos[x].id.split("_");
            var pos=existeValorMatriz(arrConceptos,arrDatos[4]);
            
        	if(pos==-1)
            {
				arrConceptos.push([arrDatos[4],nodos[x].text,[nodos[x].id]]);            	
            }
            else
            	arrConceptos[pos][2].push(nodos[x].id);            	
        }
        else
        {
        	
        	obtenerConceptosHijos(nodos[x].children,arrConceptos)
        }
    }
}


function guardarCambiosRegistro(e)
{
	var arrDatos=e.field.split('_');

	var cadObj='{"costo":"'+e.value+'","plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+
    			'","idConcepto":"'+arrDatos[2]+'","tipoConcepto":"'+arrDatos[1]+'","idElemento":"'+e.record.data.idElemento+'","tipoElemento":"'+e.record.data.tipoElemento+
                '","idInstanciaPlanEstudio":"'+e.record.data.idInstanciaPlan+'"}';
    
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            
        }
        else
        {
        	function resp()
            {
            	e.record.set(e.field,e.originalValue);
            }
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0],resp);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=12&cadObj='+cadObj,true);
}



function mostrarVentanaImportarCostosServicio()
{
	var cmbPlantel2=crearComboExt('cmbPlantel2',arrPlanteles,130,5,300);
    cmbPlantel2.setValue(plantel);
    cmbPlantel2.on('select',function (cmb,registro)
    						{
                            	cmbProgramaEducativo2.getStore().removeAll();
                            	cmbProgramaEducativo2.setValue('');
                                cmbPeriodo2.getStore().removeAll();
                                cmbPeriodo2.setValue('');
                                cmbPlanEstudio2.getStore().removeAll();
                                cmbPlanEstudio2.setValue('');
                                gEx('gridCostoConceptos2').getStore().removeAll();
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                     	gEx('cmbProgramaEducativo2').getStore().loadData(eval(arrResp[1]));   
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=76&plantel='+registro.get('id'),true);
                                
                                
                            }
    				)

	var arrProgramaEducativo=[];
    var cmbProgramaEducativo=gEx('cmbProgramaEducativo');
    var x;
    var fila;
    for(x=0;x<cmbProgramaEducativo.getStore().getCount();x++)
    {
    	fila=cmbProgramaEducativo.getStore().getAt(x);
        arrProgramaEducativo.push([fila.get('id'),fila.get('nombre')]);
    }
    
	var cmbProgramaEducativo2=crearComboExt('cmbProgramaEducativo2',arrProgramaEducativo,130,35,350);
    cmbProgramaEducativo2.setValue(cmbProgramaEducativo.getValue());
    cmbProgramaEducativo2.on('select',function(cmb,registro)
    									{
                                        	cmbPeriodo2.getStore().removeAll();
                                        	cmbPeriodo2.setValue('');
                                            cmbPlanEstudio2.getStore().removeAll();
                                            cmbPlanEstudio2.setValue('');
                                            gEx('gridCostoConceptos2').getStore().removeAll();
                                            function funcAjax()
                                            {
                                                var resp=peticion_http.responseText;
                                                arrResp=resp.split('|');
                                                if(arrResp[0]=='1')
                                                {
                                                 	
                                                    var arrDatos=eval(arrResp[1]);
                                                    var idPeriodo=cmbPeriodo2.getValue();
                                                    cmbPeriodo2.getStore().loadData(arrDatos);
                                                    cmbPeriodo2.reset();
                                                    if(existeValorMatriz(arrDatos,idPeriodo)!=-1)
                                                    {
                                                    	cmbPeriodo2.setValue(idPeriodo);
                                                    }
                                                    gEx('cmbPlanEstudio2').getStore().loadData(eval(arrResp[2]));

                                                }
                                                else
                                                {
                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                }
                                            }
                                            obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=73&plantel='+gEx('cmbPlantel2').getValue()+'&idPrograma='+registro.get('id'),true);
                                        }
    						)
    var arrCiclo=[];
    var cmbCiclo=gEx('cmbCiclo');
    for(x=0;x<cmbCiclo.getStore().getCount();x++)
    {
    	fila=cmbCiclo.getStore().getAt(x);
        arrCiclo.push([fila.get('id'),fila.get('nombre')]);
    }
    
   	var cmbCiclo2=crearComboExt('cmbCiclo2',arrCiclo,130,65,120);
    cmbCiclo2.setValue(cmbCiclo.getValue());
    cmbCiclo2.on('select',function(cmb,registro)
    						{
                            	cargarCostosImportacion();	
                            }
    			)
    var cmbPeriodo=gEx('cmbPeriodo');
    var arrPeriodo=[];
    for(x=0;x<cmbPeriodo.getStore().getCount();x++)
    {
    	fila=cmbPeriodo.getStore().getAt(x);
        arrPeriodo.push([fila.get('id'),fila.get('nombre')]);
    }
    var cmbPeriodo2=crearComboExt('cmbPeriodo2',arrPeriodo,350,65,320);
    cmbPeriodo2.setValue(cmbPeriodo.getValue());
    cmbPeriodo2.on('select',function(cmb,registro)
    						{
                            	cargarCostosImportacion();	
                            }
    			)
    var arrPlanEstudio=[];
    var cmbPlanEstudio=gEx('cmbPlanEstudio');
    for(x=0;x<cmbPlanEstudio.getStore().getCount();x++)
    {
    	fila=cmbPlanEstudio.getStore().getAt(x);
        if(fila.get('id')!=cmbPlanEstudio.getValue())
	        arrPlanEstudio.push([fila.get('id'),fila.get('nombre')]);
    }
    var cmbPlanEstudio2=crearComboExt('cmbPlanEstudio2',arrPlanEstudio,130,95,450);
   	cmbPlanEstudio2.on('select',function(cmb,registro)
    							{
                                	cargarCostosImportacion();
                                }
    					)
    
    var gridCostosImportacion=crearGridCostosImportacion();
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'fieldset',
                                                            width:700,
                                                            height:160,
                                                            layout:'absolute',
                                                            defaultType: 'label',
                                                            title:'Datos del plan de estudios origen',
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            html:'Plantel:'
                                                                        },
                                                                        cmbPlantel2,
                                                            			{
                                                                        	x:10,
                                                                            y:40,
                                                                            html:'Programa Educativo:'
                                                                        },
                                                                        cmbProgramaEducativo2,
                                                                        {
                                                                        	x:10,
                                                                            y:70,
                                                                            html:'Ciclo:'
                                                                        },
                                                                        cmbCiclo2,
                                                                        {
                                                                        	x:280,
                                                                            y:70,
                                                                            html:'Periodo:'
                                                                        },
                                                                        cmbPeriodo2,
                                                                        {
                                                                        	x:10,
                                                                            y:100,
                                                                            html:'Plan de Estudios:'
                                                                        },
                                                                        cmbPlanEstudio2
                                                            		]
                                                        },
                                                        gridCostosImportacion

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Importar costos de Servicios de Plan de Estudio',
										width: 750,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		function resp(btn)	
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	var x=0;
                                                                                var fila;
                                                                                var gridCostoConceptos=gEx('gridCostoConceptos');
                                                                                var pos;
                                                                                var filaAux;
                                                                                for(x=0;x<gridCostosImportacion.getStore().getCount();x++)
                                                                                {
                                                                                	fila=gridCostosImportacion.getStore().getAt(x);
                                                                                    pos=obtenerPosFila(gridCostoConceptos.getStore(),'idConceptoIngreso',fila.get('idConceptoIngreso'));
                                                                                    if(pos!=-1)
                                                                                    {
                                                                                    	filaAux=gridCostoConceptos.getStore().getAt(pos);
                                                                                    	filaAux.set('costo',fila.get('costo'));
                                                                                    }
                                                                                    
                                                                                }
                                                                                ventanaAM.close();
                                                                                	
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer importar los costos del Plan de Estudios seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridCostosImportacion()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idConceptoIngreso'},
		                                                {name: 'descripcion'},
		                                                {name:'costo'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'descripcion', direction: 'ASC'},
                                                            groupField: 'descripcion',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='75';
                                        proxy.baseParams.idInstanciaPlan=gEx('cmbPlanEstudio2').getValue();
                                        proxy.baseParams.plantel=plantel;
                                        proxy.baseParams.idCiclo=gEx('cmbCiclo2').getValue();
                                        proxy.baseParams.idPeriodo=gEx('cmbPeriodo2').getValue();
                                        
                                        
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Concepto',
                                                                width:450,
                                                                sortable:true,
                                                                dataIndex:'descripcion'
                                                            },
                                                            {
                                                                header:'Costo del concepto',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'costo',
                                                                renderer:'usMoney',
                                                                css:'text-align:right !important;',
                                                                editor:	{
                                                                			xtype:'numberfield',
                                                                            allowDecimals:true,
                                                                            allowNegative:false
                                                                		}	
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridCostoConceptos2',
                                                                x:10,
                                                                y:180,
                                                                height:220,
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:true,
                                                                cm: cModelo,
                                                                clicksToEdit:1,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit: false,
                                                                                                    enableGrouping :false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function cargarCostosImportacion()
{
	var gridCostoConceptos2=gEx('gridCostoConceptos2');
    gridCostoConceptos2.getStore().reload();
}



function recargarGrid()
{
	gEx('arbolCostos').getRootNode().removeAll();
    switch(gEx('cmbServiciosNivel').getValue())
    {
    	case '1':
        	if(gEx('cmbPeriodo').getValue()=='')
            {
            	return;
            }
        break;
        case '2':
        	if((gEx('cmbProgramaEducativo').getValue()=='')||(gEx('cmbPeriodo').getValue()==''))
            {
            	return;
            }
        break;
        case '3':
        case '4':
        	if(gEx('cmbPeriodo').getValue()=='')
            {
            	return;
            }
        break;
    }
	gEx('arbolCostos').getRootNode().reload();
    gEx('arbolCostos').expandAll();
}

function lanzarInterfaceCosteo(idNodo)
{
	var arbolCostos=gEx('arbolCostos');
	arbolCostos.checkAllNodes(false);
    arbolCostos.checkNode(bD(idNodo),true);
    arrNodos=arbolCostos.getCheckNodes();
    nodo=arrNodos[0];
    consideraFechaVencimiento=parseInt(nodo.attributes.consideraFechaVencimiento);
    perfilCosteo=nodo.attributes.perfilCosteo;
    asignarCosto();
}

function asignarCosto()
{
	var arbolCostos=gEx('arbolCostos');
    arrNodos=arbolCostos.getCheckNodes();
	if(arrNodos.length==0)
    {
    	msgBox('Debe seleccionar el servicio cuyo costo desea modificar');
    	return;
    }
	if(perfilCosteo=='')
    {
    	if(arrNodos.length>1)
	    	msgBox('Los servicios seleccionados <b>NO</b> cuentan con una interface de asignaci&oacute;n de costo configurado');
        else
        	msgBox('El servicio seleccionado <b>NO</b> cuentan con una interface de asignaci&oacute;n de costo configurado');
    	return ;
    }
    
    var pos=existeValorMatriz(arrPerfiles,perfilCosteo+'',0,true);
    
    if(pos==-1)
    {
    	msgBox('El perfil de costeo asignado al servicio no se encuentra disponible en el sistema');
    	return;
    }
    
    eval(arrPerfiles[pos][1]+'("'+bE(perfilCosteo)+'");');
    
}