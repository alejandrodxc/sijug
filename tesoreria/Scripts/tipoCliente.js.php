<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	gE('frmEnvio').submit();
    }
}


function mostrarVentanaFuncion()
{
	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                            {
                                                var iConsulta=idConsulta;
                                                var r=new registroConcepto	(
                                                                                {
                                                                                    idConsulta:iConsulta,
                                                                                    nombreConsulta:nombre,
                                                                                    nombreCategoria:'',
                                                                                    descripcion:'',
                                                                                    valorRetorno:'',
                                                                                    parametros:''
                                                                                }
                                                                            )
                                                                            
                                                conceptoSeleccionadoFiltro(r, gEx('vAgregarExp'));	
                                            }
    	mostrarVentanaExpresion(conceptoSeleccionadoFiltro,true);
}


function conceptoSeleccionadoFiltro(fila,ventana)
{
	gE('_idFuncionInterpretacionint').value=fila.get('idConsulta');
    gE('lblFuncion').innerHTML=fila.get('nombreConsulta');
    ventana.close();
}

function removerFuncionAplicacion()
{
	gE('_idFuncionInterpretacionint').value='';
    gE('lblFuncion').innerHTML='No especificada';
    
}