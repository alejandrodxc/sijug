<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{

    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Factura CFDI</b></span>',
                                               	tbar:	[
                                                			{
                                                                icon:'../images/email_go.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Enviar Factura CFDI',
                                                                handler:function()
                                                                        {
                                                                            function resp(btn)
                                                                            {
                                                                            	if(btn=='yes')
                                                                                {
                                                                                	
                                                                                    window.parent.limpiarCaja();
                                                                                    window.parent.cerrarVentanaFancy();
                                                                                    

                                                                                }
                                                                            }
                                                                            msgConfirm('Est&aacute; seguro de querer enviar la factura CFDI?',resp)
                                                                        }
                                                                
                                                            }
                                                		],
                                                items:	[
                                                             new Ext.ux.IFrameComponent({ 
                                                                                            id: 'content', 
                                                                                            region: 'center',
                                                                                            anchor:'100% 100%',
                                                                                            url: '../paginasFunciones/white.php',
                                                                                            style: 'width:100%;height:100%' 
                                                                                        }) 
                                                        ]
                                            }
                                         ]
                            }
                            
                            
                            
                        )  
	gEx('content').load({url:'../formatosFacturasElectronicas/factura2.php',params:{xml:gE('XMLFactura').value,cPagina:'sFrm=true'},scripts:true})                         
}