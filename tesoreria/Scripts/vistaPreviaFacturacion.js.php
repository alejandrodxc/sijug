<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{

    new Ext.Panel	(
    					{
                            xtype:'panel',
                            renderTo:'tblCuerpo',
                            layout:'border',
                            width:'100%',
                            height:obtenerDimensionesNavegador()[0]-80,
                            
                            tbar:	[
                            			{
                                            icon:'../images/download.png',
                                            cls:'x-btn-text-icon',
                                            text:'Descargar XML Sin Timbrar',
                                            handler:function()
                                                    {
                                                        var arrParam=[['XML',gE('XMLSinTimbre').value]];
                                                        enviarFormularioDatos('../tesoreria/descargarXMLSINTimbre.php',arrParam);
                                                    }
                                            
                                        },'-',
                                        {
                                            icon:'../images/page_accept.png',
                                            cls:'x-btn-text-icon',
                                            text:'Generar Factura CFDI',
                                            handler:function()
                                                    {
                                                        function resp(btn)
                                                        {
                                                            if(btn=='yes')
                                                            {
                                                                function funcAjax()
                                                                {
                                                                    var resp=peticion_http.responseText;
                                                                    arrResp=resp.split('|');
                                                                    if(arrResp[0]=='1')
                                                                    {
                                                                        window.parent.regresarPagina();
                                                                        window.parent.cerrarVentanaFancy();
                                                                    }
                                                                    else
                                                                    {
                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                    }
                                                                }
                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=47&cadObj='+gE('cadObj').value,true);
                    
                                                            }
                                                        }
                                                        msgConfirm('Est&aacute; seguro de querer generar la factura CFDI?',resp)
                                                    }
                                            
                                        }
                                    ],
                            items:	[
                                         new Ext.ux.IFrameComponent({ 
                                                                        id: 'content', 
                                                                        region: 'center',
                                                                        anchor:'100% 100%',
                                                                        url: '../paginasFunciones/white.php',
                                                                        style: 'width:100%;height:100%' 
                                                                    }) 
                                    ]
                        }
                    )
                        
	gEx('content').load({url:gE('formatoFactura').value,params:{cadObj:gE('cadObj').value,xml:gE('XMLFactura').value,cPagina:'sFrm=true'},scripts:true})                         
}