<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	var arrCertificados=eval(bD(gE('arrCertificados').value));
	var cmbCertificado=crearComboExt('cmbCertificado',arrCertificados,0,0,180);
    if(gE('idCertificado').value!='-1')
    {
    	cmbCertificado.setValue(gE('idCertificado').value);
    }
    
    if(arrCertificados.length<=1)
    {
    	cmbCertificado.disable();
    }
    
    var cmbSerie=crearComboExt('cmbSerie',[],0,0,110);
    
    cmbCertificado.on('select',function(cmb,registro)
    							{
                                	cmbSerie.setValue('');
                                	cmbSerie.getStore().loadData(registro.data.valorComp);
                                    if(registro.data.valorComp.length<=1)
                                    	cmbSerie.disable();
                                }
    				)
    
    
    cmbSerie.on('select',function(cmb,registro)
    					{
                        	location.href='../tesoreria/vistaPreviaFacturacionCaja.php?cPagina=sFrm=true&empresaEmisora='+gE('idEmpresa').value+
                            '&idCliente='+gE('idCliente').value+'&idVenta='+gE('idVenta').value+'&tipoFacturacion='+gE('tipoFacturacion').value+
                            '&idCertificado='+gEx('cmbCertificado').getValue()+'&idSerie='+registro.data.id;
                        }
    			)
    
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Facturaci&oacute;n (Vista previa)</b></span>',
                                               	tbar:	[
                                                			{
                                                                icon:'../images/download.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Descargar XML Sin Timbrar',
                                                                handler:function()
                                                                        {
                                                                            var arrParam=[['XML',gE('XMLFactura').value]];
                                                                            enviarFormularioDatos('../tesoreria/descargarXMLSINTimbre.php',arrParam);
                                                                        }
                                                                
                                                            }
                                                			,'-',
                                                            {
                                                            	html:'Certificado:&nbsp;&nbsp;'
                                                            },
                                                            cmbCertificado,
                                                            '-',
                                                            {
                                                            	html:'Serie:&nbsp;&nbsp;'
                                                            },
                                                            cmbSerie,'-',
                                                            {
                                                                icon:'../images/page_accept.png',
                                                                cls:'x-btn-text-icon',
                                                                text:'Generar CFDI',
                                                                handler:function()
                                                                        {
                                                                            function resp(btn)
                                                                            {
                                                                            	if(btn=='yes')
                                                                                {
                                                                                	function funcAjax()
                                                                                    {
                                                                                        var resp=peticion_http.responseText;
                                                                                        arrResp=resp.split('|');
                                                                                        if(arrResp[0]=='1')
                                                                                        {
                                                                                        	if(arrResp[1]=="")
                                                                                            {
                                                                                                if(gE('esVistaListado').value=='0')
                                                                                                {
                                                                                                	if(gE('tipoFacturacion').value=='0')
                                                                                                    {
                                                                                                    	
	                                                                                                    window.parent.limpiarCaja();
                                                                                                    }
                                                                                                    else
                                                                                                    	window.parent.gEx('gridAbonosHistorial').getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                    window.parent.regresar1Pagina();
                                                                                                
                                                                                                if(window.parent.imprimirComprobanteVenta)
                                                                                                {
                                                                                                	window.parent.imprimirComprobanteVenta(gE('idVenta').value,gE('idEmpresa').value,gE('idCertificado').value,gE('idSerie').value);
                                                                                                }
                                                                                                    
                                                                                                window.parent.cerrarVentanaFancy();
                                                                                                
                                                                                                
                                                                                                
                                                                                           	}
                                                                                           	else
                                                                                           	{
                                                                                              	function respAux()
                                                                                              	{
                                                                                                  	window.parent.regresar1Pagina();
                                                                                                  	window.parent.cerrarVentanaFancy();
                                                                                              	}
                                                                                              	msgBox('Un problema ha impedido que la factura sea generada (Ver datos de la venta en m&oacute;dulo de ventas caja para m&aacute;s detalle)',respAux);
                                                                                           	}
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                        }
                                                                                    }
                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=46&tipoFacturacion='+gE('tipoFacturacion').value+'&idVenta='+
                                                                                    				gE('idVenta').value+'&idCliente='+gE('idCliente').value+'&idEmpresa='+gE('idEmpresa').value+'&idCertificado='+gE('idCertificado').value+
                                                                                                    '&idSerie='+gE('idSerie').value,true);

                                                                                }
                                                                            }
                                                                            msgConfirm('Est&aacute; seguro de querer generar la factura CFDI?',resp)
                                                                        }
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                		],
                                                items:	[
                                                             new Ext.ux.IFrameComponent({ 
                                                                                            id: 'content', 
                                                                                            region: 'center',
                                                                                            anchor:'100% 100%',
                                                                                            url: '../paginasFunciones/white.php',
                                                                                            style: 'width:100%;height:100%' 
                                                                                        }) 
                                                        ]
                                            }
                                         ]
                            }
                            
                            
                            
                        )  
	gEx('content').load({url:gE('moduloImpresionFactura').value,params:{idCliente:gE('idCliente').value,iVenta:gE('idVenta').value,xml:gE('XMLFactura').value,cPagina:'sFrm=true',objSerializado:1,cadObj:gE('cadObj').value},scripts:true})                         
    dispararEventoSelectCombo('cmbCertificado');
    
    if(gE('idSerie').value!='-1')
    {
    	cmbSerie.setValue(gE('idSerie').value);
    }
    
}