<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT DISTINCT idPeriodicidad  FROM 4513_instanciaPlanEstudio";
	$lPeriodicidad=$con->obtenerListaValores($consulta);
	if($lPeriodicidad=="")
		$lPeriodicidad=-1;
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares ORDER BY nombreCiclo";
	$arrCiclo=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idCiclo,nombreCiclo FROM 4526_ciclosEscolares where situacion=1";
	$cicloActivo=$con->obtenerValor($consulta);
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama  WHERE institucion=1 ORDER BY unidad";
	$arrPlanteles=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT id__464_gridPeriodos,concat(txtDescripcion,': [',prioridad,'] ',nombrePeriodo) FROM _464_gridPeriodos g,_464_tablaDinamica t WHERE 
					g.idReferencia=t.id__464_tablaDinamica and t.id__464_tablaDinamica in(".$lPeriodicidad.")  order by txtDescripcion,prioridad";			
	$arrPeriodos=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idConcepto,nombreConcepto,cveConcepto FROM 561_conceptosIngreso WHERE situacion=1 and nivelCosteo<>5 ORDER BY nombreConcepto";
	$arrConceptosIngreso=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT codigoUnidad,unidad FROM 817_organigrama WHERE institucion=1  ORDER BY unidad";
	$arrPlanteles=$con->obtenerFilasArreglo($consulta);
	
	$consulta="select idPerfilCosteo,funcionIniciacion FROM 6022_perfilesCosteo";
	$arrPerfiles=$con->obtenerFilasArreglo($consulta);
	
	
?>	

var esGridCosteo=true;
var consideraFechaVencimiento=null;
var perfilCosteo=null;
var arrPlanteles=<?php echo $arrPlanteles?>;
var arrConceptosIngreso=<?php echo $arrConceptosIngreso?>;
var arrPeriodos=<?php echo $arrPeriodos?>;
var arrCiclo=<?php echo $arrCiclo?>;
var arrPerfiles=<?php echo $arrPerfiles?>;

Ext.onReady(inicializar);
function inicializar()
{

	var cmbPlantel=crearComboExt('cmbPlantel',arrPlanteles,0,0,220);	
    
    cmbPlantel.on('select',function(cmb,registro)
    						{
                            	cargarCostoServicios();
                            }
    			)

	var cmbPeriodo=crearComboExt('cmbPeriodo',arrPeriodos,0,0,350);
    cmbPeriodo.on('select',function(cmb,registro)
    						{
                            	cargarCostoServicios();
                            }
    			)
	var cmbCiclo=crearComboExt('cmbCiclo',arrCiclo,0,0,90);
    cmbCiclo.setValue('<?php echo $cicloActivo?>');
    cmbCiclo.on('select',function(cmb,registro)
    						{
                            	cargarCostoServicios();
                            }
    			)
    
    
    
    
    var objConf={};
    objConf.confVista='<tpl for="."><div class="search-item">[{valorComp}] {nombre}</div></tpl>';
    objConf.typeAhead=true;
    var cmbConceptos=crearComboExt('cmbConceptos',arrConceptosIngreso,0,0,550,objConf);
    cmbConceptos.on('select',function(cmb,registro)
    							{
                                	gEx('txtCveConcepto').setValue(registro.data.valorComp);
                                    cargarCostoServicios();
                                }
    				)
    
    
	new Ext.Viewport(	{
                            layout: 'border',
                            items: [
                            			{
                                        	xtype:'panel',
                                            region:'center',
                                            layout:'border',
                                            tbar:	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<span class="letraVino14N">Administraci&oacute;n de costos de Servicios</span>'
                                                        }
                                                    ],
                                            items:	[
                                                    
                                                        {
                                                            xtype:'panel',
                                                            region:'center',
                                                            border:false,
                                                            layout:'border',
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Plantel:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbPlantel,'-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Ciclo:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbCiclo
                                                                        ,
                                                                        '-',
                                                                        {
                                                                        	xtype:'label',
                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Periodo:</b></span>&nbsp;&nbsp;'
                                                                        },
                                                                        cmbPeriodo,
                                                                        '-'
                                                                        
                                                                        
                                                                    ],
                                                            items:	[
                                                            			{
                                                                        	xtype:'panel',
                                                                            border:false,
                                                                            region:'center',
                                                                            layout:'border',
                                                                            tbar:	[
                                                                            			{
                                                                                            xtype:'label',
                                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Cve Concepto:</b></span>&nbsp;&nbsp;'
                                                                                        },
                                                                                        {
                                                                                            xtype:'textfield',
                                                                                            width:50,
                                                                                            enableKeyEvents:true,
                                                                                            id:'txtCveConcepto',
                                                                                            listeners:	{
                                                                                            				keyup:function(ctrl,e)
                                                                                                            		{
                                                                                                                    	if(e.getKey()==e.ENTER)
                                                                                                                        {
                                                                                                                        	cmbConceptos.focus();
                                                                                                                        }
                                                                                                                    },
                                                                                            				change:function(ctrl,nValor,oValor)
                                                                                                            		{
                                                                                                                    	buscarClaveConcepto();	
                                                                                                                    }
                                                                                            			}
                                                                                        },
                                                                                        '-',
                                                                                        {
                                                                                            xtype:'label',
                                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Concepto:</b></span>&nbsp;&nbsp;'
                                                                                        },
                                                                                        cmbConceptos,'-',
                                                                                        {
                                                                                            icon:'../images/coins.png',
                                                                                            cls:'x-btn-text-icon',
                                                                                            id:'btnModificarCosto',
                                                                                            text:'Modificar costo',
                                                                                            handler:function()
                                                                                                    {
                                                                                                    	var filas=gEx('gCostos').getSelectionModel().getSelections();
                                                                                                    	if(filas.length==0)    
                                                                                                        {
                                                                                                        	msgBox('Debe seleccionar almenos un concepto cuyo costo desea modificar');
                                                                                                            return;
                                                                                                        }
                                                                                                    
                                                                                                       asignarCosto(); 
                                                                                                    }
                                                                                            
                                                                                        },
                                                                                        
                                                                                        
                                                                                        {
                                                                                            xtype:'label',
                                                                                            hidden:true,
                                                                                            id:'lCosto',
                                                                                            html:'&nbsp;&nbsp;<span style="color:#000"><b>Costo:</b></span>&nbsp;&nbsp;'
                                                                                        },
                                                                                        {
                                                                                        	
                                                                                            hidden:true,
                                                                                        	xtype:'textfield',
                                                                                            id:'tCosto',
                                                                                            width:100,
                                                                                            enableKeyEvents:true,
                                                                                            listeners:	{
                                                                                            				focus:function(ctrl)
                                                                                                            		{
                                                                                                                    	lanzarInterfaceCosteo();
                                                                                                                    }
                                                                                            			}
                                                                                        }
                                                                            		],
                                                                            items:	[
                                                                            			crearGridCostos()
                                                                            		]
                                                                        }
                                                                    ]
                                                        }
                                    				 ]
										}
									]                                        
                                                     
						}
                    )  
}


function buscarClaveConcepto()
{
	var cmbConceptos=gEx('cmbConceptos');
    var txtCveConcepto=gEx('txtCveConcepto');
    var pos=obtenerPosFila(cmbConceptos.getStore(),'valorComp',txtCveConcepto.getValue());
    if(pos!=-1)
    {
        cmbConceptos.setValue(cmbConceptos.getStore().getAt(pos).data.id); 	
        dispararEventoSelectCombo('cmbConceptos');
    }
    else
    {
    
        cmbConceptos.setValue('');
        function resp()
        {
            txtCveConcepto.focus();
        }
        msgBox('La clave del concepto ingresada NO existe',resp);
    }
}

function crearGridCostos()
{
    var lector= new Ext.data.JsonReader({
                                                
                                                totalProperty:'numReg',
                                                fields: [
                                                			{name:'id'},
                                                            {name:'idProgramaEducativo'},
                                                            {name:'programaEducativo'},
                                                            {name: 'idPlanEstudios'},
                                                            {name: 'planEstudios'},
                                                            {name:'idGrado'},
                                                            {name:'grado'},
                                                            {name:'costo'}
                                                        ],
                                                root:'registros'
                                                
                                            }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'programaEducativo', direction: 'ASC'},
                                                            groupField: 'programaEducativo',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('lCosto').hide();
                                        gEx('tCosto').hide();
                                        
                                    }
                        )   

	alDatos.on('load',function(st,registros,options)
    								{
                                    	gEx('btnModificarCosto').show();
                                    	var gCostos=gEx('gCostos');
                                        
                                        gCostos.enable();
                                        var x;
                                        for(x=1;x<5;x++)
                                        {
                                        	gCostos.getColumnModel().setHidden(x,false);
                                        }
                                        
                                        
                                        
                                    	switch(st.reader.jsonData.nivelCosteo)
                                        {
                                        	case '1':
                                            	gCostos.getStore().clearGrouping();
                                                gCostos.disable();
                                                gEx('btnModificarCosto').hide();
                                                gEx('lCosto').show();
		                                        gEx('tCosto').show();
                                                gEx('tCosto').setValue(Ext.util.Format.usMoney(registros[0].data.costo));
                                                gCostos.getStore().removeAll();
                                            break;
                                            case '2':
                                            	gCostos.getStore().clearGrouping();
												gCostos.getColumnModel().setHidden(2,true);
                                                gCostos.getColumnModel().setHidden(3,true);
                                            break;
                                            case '3':
                                            	gCostos.getStore().groupBy('programaEducativo',true);
                                                gCostos.getColumnModel().setHidden(1,true);
                                                gCostos.getColumnModel().setHidden(3,true);
                                            break;
                                            case '4':
                                            	gCostos.getStore().groupBy('planEstudios',true);
                                                gCostos.getColumnModel().setHidden(1,true);
                                                gCostos.getColumnModel().setHidden(2,true);
                                                gCostos.getView().refresh();
                                            break;
                                        }
                                        
                                        consideraFechaVencimiento=st.reader.jsonData.fechaVencimiento;
										perfilCosteo=st.reader.jsonData.perfilCosteo;
                                        
                                    }
                        )   
       
	var chkRow=new Ext.grid.CheckboxSelectionModel();       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                        	chkRow,
                                                            {
                                                                header:'Programa educativo',
                                                                width:500,
                                                                sortable:true,
                                                                dataIndex:'programaEducativo',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Plan estudio',
                                                                width:700,
                                                                sortable:true,
                                                                dataIndex:'planEstudios',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Grado',
                                                                width:600,
                                                                sortable:true,
                                                                dataIndex:'grado',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Costo',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'costo',
                                                                css:'text-align:right;',
                                                                renderer:function(val)
                                                                		{
                                                                        	return '<a href="javascript:lanzarInterfaceCosteo()">'+Ext.util.Format.usMoney(val);
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gCostos',
                                                                store:alDatos,
                                                                region:'center',
                                                                border:false,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                sm:chkRow,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:true,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;
}

function cargarCostoServicios()
{
	var tblGrid=gEx('gCostos');
    tblGrid.getStore().removeAll();
    var cmbPlantel=gEx('cmbPlantel');
	var cmbPeriodo=gEx('cmbPeriodo');
	var cmbCiclo=gEx('cmbCiclo');
	var cmbConceptos =gEx('cmbConceptos');
    
    if((cmbPlantel.getValue()=='')||(cmbPeriodo.getValue()=='')||(cmbCiclo.getValue()=='')||(cmbConceptos.getValue()==''))
    {
    	return;
    }
    
    
    
    tblGrid.getStore().load	(
    							{
                                	url:'../paginasFunciones/funcionesTesoreria.php',
                                    params:	{
                                    			funcion:65,
                                    			plantel:cmbPlantel.getValue(),
                                                ciclo:cmbCiclo.getValue(),
                                                periodo:cmbPeriodo.getValue(),
                                                idConcepto:cmbConceptos.getValue()
                                    		}
                                }
    						)
    
    
}

function lanzarInterfaceCosteo()
{
    asignarCosto();
}

function asignarCosto()
{
	
	if((perfilCosteo==null)||(perfilCosteo==''))
    {
       	msgBox('El concepto seleccionado <b>NO</b> cuentan con una interface de asignaci&oacute;n de costo configurado');
    	return ;
    }
    
    var pos=existeValorMatriz(arrPerfiles,perfilCosteo+'',0,true);
    
    if(pos==-1)
    {
    	msgBox('El perfil de costeo asignado al servicio no se encuentra disponible en el sistema');
    	return;
    }
    
    eval(arrPerfiles[pos][1]+'("'+bE(perfilCosteo)+'");');
    
}