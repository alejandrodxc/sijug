<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT(razonSocial,' ',apPaterno,' ',apMaterno),razonSocial) AS nombreEmpresa,
				(SELECT CONCAT(municipio,', ',UPPER(estado)) FROM  821_municipios m,820_estados e WHERE cveMunicipio=e.municipio AND m.cveEstado=e.cveEstado)  
				FROM 6927_empresas e WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1 order by razonSocial,apPaterno,apMaterno";
	$arrEmpresas=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idMoneda,moneda FROM 603_tipoMoneda ORDER BY moneda";
	$arrMoneda=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idFormaPago,formaPago FROM 710_metodoPagoComprobante ORDER BY idFormaPago";
	$arrMetodoPago=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idTipoComprobante,tipoComprobante FROM 705_tiposComprobantes ORDER BY tipoComprobante";
	$arrTipoComprobante=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idUnidadMedida,unidadMedida FROM 6923_unidadesMedida ORDER BY unidadMedida";
	$arrUnidadesMedida=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=1 ORDER BY descripcion";
	$arrImpuestos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=2 ORDER BY descripcion";
	$arrRetenciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=0 ORDER BY descripcion";
	$arrIntermedios=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=3 ORDER BY descripcion";
	$arrDescuentos=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT * FROM 711_catalogoImpuestosRetenciones where institucion='' OR institucion IS NULL or institucion='".$_SESSION["codigoInstitucion"]."' ORDER BY prioridad";
	$arrConceptosImpRet=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT tipoConcepto,idConcepto,porcentajeDefault,'0' AS monto,prioridad,porcentajeModificable,comentarios,removible,descripcion as lblConcepto FROM 711_catalogoImpuestosRetenciones WHERE 
			idConcepto IN (1,5,6,7,12) ORDER BY prioridad";

	$arrConceptosDefault=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idAlmacen FROM 6900_almacenes WHERE referencia='".$referenciaFiltros."'";
	$listAlmacenes=$con->obtenerListaValores($consulta);
	if($listAlmacenes=="")
		$listAlmacenes=-1;
		
	$consulta="SELECT idRegistro,nombreComplemento FROM 6956_complementosComprobantes";
	$arrComplementos=$con->obtenerFilasArreglo($consulta);
	
		
?>
var arrComplementos=<?php echo $arrComplementos?>;
var arrDescuentos=<?php echo $arrDescuentos?>;
var arrConceptosDefault=<?php echo $arrConceptosDefault?>;
var arrConceptosImpRet=<?php echo $arrConceptosImpRet?>;
var arrIntermedios=<?php echo $arrIntermedios?>;
var arrImpuestos=<?php echo $arrImpuestos?>;
var arrRetenciones=<?php echo $arrRetenciones?>;
var arrUnidadesMedida=<?php echo $arrUnidadesMedida?>;
var arrTipoComprobante=<?php echo $arrTipoComprobante?>;
var idEmpresaFacturacion=-1;
var arrMetodoPago=<?php echo $arrMetodoPago?>;
var arrFormaPago=[['1','Pago en una sola exhibici\xF3n'],['2','Parcialidad']];
var arrMoneda=<?php echo $arrMoneda?>;
var arrEmpresas=<?php echo $arrEmpresas?>;
var reg=null;
Ext.onReady(inicializar);

function inicializar()
{
	arrComplementos.splice(0,0,['0','Ninguno']);
	var cmbComplemento=crearComboExt('cmbComplemento',arrComplementos,560,100,220);
	cmbComplemento.setValue('0');

	cmbComplemento.on('select',function(cmb,registro)
    							{
                                
                                	var x;
                                    var fila;
                                    for(x=0;x<cmb.getStore().getCount();x++)
                                    {
                                    	fila=cmb.getStore().getAt(x);
                                    	gEx('tPanelFactura').remove('panel_'+fila.data.id,true);    
                                    }
                                	
                                	if(registro.data.id!='0')
                                    {
                                		eval("gEx('tPanelFactura').add(crearPanel_"+registro.data.id+"());");
									}
                                    //panelListadoRegistros.setActiveTab('panel_'+registro.data.id);      
                                }
    				)

	 reg=crearRegistro	(
                            [
                                {name: 'idConcepto'},
                                {name: 'tipoConcepto'},
                                {name: 'codigoAlternativo'},
                                {name: 'descripcion'},
                                {name: 'unidadMedida'},
                                {name: 'costoUnitario'},
                                {name: 'descuentoUnitario'},
                                {name: 'cantidad'},
                                {name: 'subtotal'},
                                {name: 'iva'},
                                {name: 'tasaIVA'},
                                {name: 'total'},
                                {name: 'descuentoTotal'},
                                {name: 'cabecera'},
                                {name: 'clave'}
                                
                            ]
                        )

	var cmbTipoComprobante=crearComboExt('cmbTipoComprobante',arrTipoComprobante,160,100,200);
	cmbTipoComprobante.setValue('1');
	var oConf=	{
    					idCombo:'cmbRFC',
                        anchoCombo:130,
                        anchoLista:400,
                        campoDesplegar:'rfc',
                        campoID:'idEmpresa',
                        funcionBusqueda:43,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:150,
                        posY:0,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idEmpresaFacturacion=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRFC').getRawValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        gEx('cmbRazonSocial').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idEmpresaFacturacion=registro.get('idEmpresa');
                                       	gEx('cmbRazonSocial').setRawValue(registro.data.razonSocial); 
                                        
                                    }  
    				};

    
	var cmbRFC=crearComboExtAutocompletar(oConf);
    
    var oConf=	{
    					idCombo:'cmbRazonSocial',
                        anchoCombo:270,
                        campoDesplegar:'razonSocial',
                        campoID:'idEmpresa',
                        funcionBusqueda:43,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:290,
                        posY:0,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idEmpresaFacturacion=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRazonSocial').getRawValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        gEx('cmbRFC').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idEmpresaFacturacion=registro.get('idEmpresa');
                                       	gEx('cmbRFC').setRawValue(registro.data.rfc); 
                                        
                                    }  
    				};
    
	var cmbRazonSocial=crearComboExtAutocompletar(oConf);

	var cmbEmpresa=crearComboExt('cmbEmpresa',arrEmpresas,135,0,500);
    
    cmbEmpresa.on('select',function(cmb,registro)
    						{
                            
                            	
                            	
                            
                            	gEx('txtLugarExpedicion').setValue(registro.data.valorComp);
                            
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        cmbCertificado.reset();
                                        var arrDatos=eval(arrResp[1]);
                                        cmbCertificado.getStore().loadData(arrDatos);
                                        if(arrDatos.length==1)
                                        {
                                        
                                            cmbCertificado.setValue(arrDatos[0][0]);
                                            cmbCertificado.disable();
                                            dispararEventoSelectCombo('cmbCertificado');
                                            var iComplemento=gEx('cmbComplemento').getValue();
                                            var panel=gEx('panel_'+iComplemento);
                                            if((panel)&&(panel.visto))
                                            {
                                                eval('limpiarDatos_'+iComplemento+'();inicializar_'+iComplemento+'();');
                                            }
                                            
                                        }
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesRecursosHumanos.php',funcAjax, 'POST','funcion=62&idEmpresa='+registro.data.id,true);
                            }
    			)
                
	var cmbCertificado=crearComboExt('cmbCertificado',[],135,30,200);
    cmbCertificado.on('select',function(cmb,registro)
    						{
                            	
                              cmbSerie.reset();
                             
                              cmbSerie.getStore().loadData(registro.data.valorComp);
                              
                              if(registro.data.valorComp.length==1)
                              {
                                  cmbSerie.setValue(registro.data.valorComp[0][0]);
                                  cmbSerie.disable();
                                 
                              }
                              
                                
                            }
    			)
    var cmbSerie=crearComboExt('cmbSerie',[],460,30,170);                
                
    
    var cmbMoneda=crearComboExt('cmbMoneda',arrMoneda,435,30,200);
    cmbMoneda.setValue('1');
    cmbMoneda.on('select',function(cmb,registro)
    						{
                            	if(registro.data.id=='1')
                                {
                                	gEx('txtTipoCambio').setValue('1.00');
                                    gEx('txtTipoCambio').disable();
                                }
                                else
                                {
                                	
                                    gEx('txtTipoCambio').enable();
                                }
                            }
    			)
                
              
    var cmbFormaPago=crearComboExt('cmbFormaPago',arrFormaPago,150,60,200);
    cmbFormaPago.on('select',function(cmb,registro)
    						{
                            	if(registro.data.id=='1')
                                {
                                	gEx('lblParcialidad').hide();
                                    gEx('noParcialidad').hide();
                                    gEx('lblParcialidadDe').hide();
                                    gEx('parcialidadAl').hide();
                                }
                                else
                                {
                                	gEx('lblParcialidad').show();
                                    gEx('noParcialidad').show();
                                    gEx('lblParcialidadDe').show();
                                    gEx('parcialidadAl').show();
                                    gEx('noParcialidad').focus(false,500);
                                }
                            }
    				)


	var cmbMetodoPago=crearComboExt('cmbMetodoPago',arrMetodoPago,150,120,200);   
    
    cmbMetodoPago.on('select',function(cmb,registro)
    						{
                            	if(registro.data.id!='5')
                                {
                                	gEx('lblEspecifique').hide();
                                    gEx('txtEspecifique').hide();
                                    
                                }
                                else
                                {
                                	gEx('lblEspecifique').show();
                                    gEx('txtEspecifique').show();
                                    gEx('txtEspecifique').focus(false,500);
                                }
                            }
    				)
                 
	var pFactura= new Ext.Panel	(
                                    {
                                        
                                        bodyStyle:' background-color: rgb(240, 240, 240);border-color: rgb(240,240,240) !important;',
                                        border:false,
                                        title:'Datos del comprobante',
                                        buttonAlign :'right',
                                        layout:'absolute',
                                        bbar:	[
                                                    {
                                                        icon:'../images/page_white_magnify.png',
                                                        cls:'x-btn-text-icon',
                                                        height:40,
                                                        text:'Generar vista previa de comprobante',
                                                        handler:function()
                                                                {
                                                                    if(cmbEmpresa.getValue()=='')
                                                                    {
                                                                        function resp1()
                                                                        {
                                                                            cmbEmpresa.focus();
                                                                        }
                                                                        msgBox('Debe especificar la empresa que emite el comprobante',resp1);
                                                                        return;
                                                                    }
                                                                    
                                                                    if(cmbCertificado.getValue()=='')
                                                                    {
                                                                        function resp2()
                                                                        {
                                                                            cmbCertificado.focus();
                                                                        }
                                                                        msgBox('Debe especificar el certificado con el cual ser&aacute; firmado el comprobante',resp2);
                                                                        return;
                                                                    }
                                                                    
                                                                    if(cmbSerie.getValue()=='')
                                                                    {
                                                                        function resp3()
                                                                        {
                                                                            cmbSerie.focus();
                                                                        }
                                                                        msgBox('Debe especificar la serie que ser&aacute; asignada al comprobante',resp3);
                                                                        return;
                                                                    }
                                                                    
                                                                    
                                                                    if(idEmpresaFacturacion==-1)
                                                                    {
                                                                        function resp12()
                                                                        {
                                                                            cmbRazonSocial.focus();
                                                                        }
                                                                        msgBox('Debe especificar el cliente al cual ser&aacute; dirigido el comprobante',resp12);
                                                                        return;
                                                                    }
                                                                    
                                                                    var dteFechaComprobante=gEx('dteFechaComprobante');
                                                                    
                                                                    
                                                                    if(dteFechaComprobante.getValue()=='')
                                                                    {
                                                                        function resp4()
                                                                        {
                                                                            dteFechaComprobante.focus();
                                                                        }
                                                                        msgBox('Debe especificar la fecha del comprobante',resp4);
                                                                        return;
                                                                    }
                                                                    
                                                                    
                                                                    var txtLugarExpedicion=gEx('txtLugarExpedicion');
                                                                    
                                                                    if(txtLugarExpedicion.getValue()=='')
                                                                    {
                                                                        function resp5()
                                                                        {
                                                                            txtLugarExpedicion.focus();
                                                                        }
                                                                        msgBox('Debe especificar el lugar de expedici&oacute;n del comprobante',resp5);
                                                                        return;
                                                                    }
                                                                    
                                                                    var txtTipoCambio=gEx('txtTipoCambio');
                                                                    if(txtTipoCambio.getValue()=='')
                                                                    {
                                                                        function resp6()
                                                                        {
                                                                            txtTipoCambio.focus();
                                                                        }
                                                                        msgBox('Debe especificar el tipo de cambio del comprobante',resp6);
                                                                        return;
                                                                    }
                                                                    
                                                                    if(cmbFormaPago.getValue()=='')
                                                                    {
                                                                        function resp7()
                                                                        {
                                                                            cmbFormaPago.focus();
                                                                        }
                                                                        msgBox('Debe especificar la forma de pago del comprobante',resp7);
                                                                        return;
                                                                    }
                                                                    
                                                                    var noParcialidad=gEx('noParcialidad');
                                                                    var parcialidadAl=gEx('parcialidadAl');
                                                                    
                                                                    if(cmbFormaPago.getValue()=='2')
                                                                    {
                                                                        if(noParcialidad.getValue()=='')
                                                                        {
                                                                            function resp8()
                                                                            {
                                                                                noParcialidad.focus();
                                                                            }
                                                                            msgBox('Debe especificar el n&uacute;mero de parcialidad que cubre el comprobante',resp8);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(parcialidadAl.getValue()=='')
                                                                        {
                                                                            function resp9()
                                                                            {
                                                                                parcialidadAl.focus();
                                                                            }
                                                                            msgBox('Debe especificar el n&uacute;mero de parcialidades al cual abona el comprobante',resp9);
                                                                            return;
                                                                        }
                                                                    }
                                                                    
                                                                    var txtCondicionesPago=gEx('txtCondicionesPago') ;
                                                                    
                                                                    
                                                                    if(cmbMetodoPago.getValue()=='')
                                                                    {
                                                                        function resp10()
                                                                        {
                                                                            cmbMetodoPago.focus();
                                                                        }
                                                                        msgBox('Debe especificar el m&eacute;todo de pago del comprobante',resp10);
                                                                        return;
                                                                    }
                                                                    
                                                                   
                                                                    
                                                                    var txtEspecifique=gEx('txtEspecifique') ;
                                                                    
                                                                     if(cmbMetodoPago.getValue()=='5')
                                                                     {
                                                                        if(txtEspecifique.getValue()=='')
                                                                        {
                                                                            function resp11()
                                                                            {
                                                                                txtEspecifique.focus();
                                                                            }
                                                                            msgBox('Debe especificar el m&eacute;todo de pago del comprobante',resp11);
                                                                            return;
                                                                        }
                                                                     }
                                                                    
                                                                    var txtNoCuenta=gEx('txtNoCuenta');
                                                                    
                                                                    
                                                                    var arrConceptos='';
                                                                    var fila;
                                                                    var gConceptos=gEx('gConcepto');
                                                                    var x;
                                                                    for(x=0;x<gConceptos.getStore().getCount();x++)
                                                                    {
                                                                        fila=gConceptos.getStore().getAt(x);
                                                                        
                                                                        if(fila.data.descripcion.trim()=='')
                                                                        {
                                                                            function resp100()
                                                                            {
                                                                                
                                                                                gConceptos.startEditing(x,1);
                                                                            }
                                                                            msgBox('Debe especificar la descripci&oacute;n del concepto',resp100);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(fila.data.unidadMedida.trim()=='')
                                                                        {
                                                                            function resp101()
                                                                            {
                                                                                
                                                                                gConceptos.startEditing(x,2);
                                                                            }
                                                                            msgBox('Debe especificar la unidad de medida del concepto',resp101);
                                                                            return;
                                                                        }
                                                                        
                                                                        o='{"clave":"'+fila.data.clave+'","cabecera":"'+cv(fila.data.cabecera)+'","idConcepto":"'+fila.data.idConcepto+'","tipoConcepto":"'+fila.data.tipoConcepto+'","descripcion":"'+cv(fila.data.descripcion,false,true)+'","unidadMedida":"'+fila.data.unidadMedida+
                                                                            '","costoUnitario":"'+fila.data.costoUnitario+'","descuentoUnitario":"'+fila.data.descuentoUnitario+'","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+
                                                                            '","iva":"'+fila.data.iva+'","tasaIVA":"'+fila.data.tasaIVA+'","total":"'+fila.data.total+'","descuentoTotal":"'+fila.data.descuentoTotal+'"}';
                                                                        if(arrConceptos=='')
                                                                            arrConceptos=o;
                                                                        else
                                                                            arrConceptos+=','+o;
                                                                    }
                                                                    
                                                                    var arrTotales='';
                                                                    var gMontosFinales=gEx('gMontosFinales');
                                                                    for(x=0;x<gMontosFinales.getStore().getCount();x++)
                                                                    {
                                                                        fila=gMontosFinales.getStore().getAt(x);
                                                                        o='{"etiqueta":"'+fila.data.lblConcepto+'","tipoConcepto":"'+fila.data.tipoConcepto+'","idConcepto":"'+fila.data.idConcepto+'","tasaConcepto":"'+fila.data.tasaConcepto+'","montoConcepto":"'+fila.data.montoConcepto+'"}';
                                                                        if(arrTotales=='')
                                                                            arrTotales=o;
                                                                        else
                                                                            arrTotales+=','+o;
                                                                    }
                                                                    
                                                                    var complemento=cmbComplemento.getValue();
                                                                    var objComplemento='{}';
                                                                    if(complemento!='0')
                                                                    {
                                                                    	eval('objComplemento=recolectarDatos_'+complemento+'();');
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    if(objComplemento!==false)
                                                                    {
                                                                    
                                                                        var cadObj='{"tipoComprobante":"'+cmbTipoComprobante.getValue()+'","motivoDescuento":"'+cv(gEx('txtMotivoDescuento').getValue(),false,true)+'","arrTotales":['+arrTotales+
                                                                                    '],"comentariosAdicionales":"'+cv(gEx('txtComentarios').getValue(),false,true)+'","arrConceptos":['+arrConceptos+'],"idEmpresa":"'+cmbEmpresa.getValue()+
                                                                                    '","idCertificado":"'+cmbCertificado.getValue()+'","idSerie":"'+cmbSerie.getValue()+'","idCliente":"'+idEmpresaFacturacion+'","fechaComprobante":"'+
                                                                                    dteFechaComprobante.getValue().format("Y-m-d")+'","lugarExpedicion":"'+cv(txtLugarExpedicion.getValue(),false,true)+'","moneda":"'+cmbMoneda.getValue()+'","tipoCambio":"'+txtTipoCambio.getValue()+
                                                                                    '","formaPago":"'+cmbFormaPago.getValue()+'","noParcialidad":"'+noParcialidad.getValue()+'","totalParcialidad":"'+parcialidadAl.getValue()+'","condicionesPago":"'+txtCondicionesPago.getValue()+
                                                                                    '","metodoPago":"'+cmbMetodoPago.getValue()+'","metodoPagoEspecifique":"'+cv(txtEspecifique.getValue(),false,true)+
                                                                                    '","noCuenta":"'+gEx('txtNoCuenta').getValue()+'","complemento":"'+complemento+'","datosComplemento":'+objComplemento+'}';
                                                                        
                                                                        var obj={};
                                                                        obj.titulo='Comprobante Fiscal (Vista previa)';
                                                                        obj.ancho=950;
                                                                        obj.alto=500;
                                                                        obj.url="../tesoreria/vistaPreviaFacturacion.php";
                                                                        obj.params=[['cPagina','sFrm=true'],['cadObj',bE(cadObj)]];
                                                                        abrirVentanaFancy(obj);
                                                                   
                                                                    }
                                                                    
                                                                }
                                                        
                                                    },'-',
                                                    {
                                                        icon:'../images/cross.png',
                                                        cls:'x-btn-text-icon',
                                                        height:40,
                                                        text:'Cancelar captura',
                                                        handler:function()
                                                                {
                                                                    function respCaptura(btn)
                                                                    {
                                                                        if(btn=='yes')
                                                                        {
                                                                            regresarPagina();
                                                                        }
                                                                    }
                                                                    msgConfirm('Est&aacute; seguro de querer cancelar la captura del comprobante?',respCaptura);
                                                                }
                                                    }
                                                ],
                                        items:	[
                                                    {
                                                        x:10,
                                                        y:5,
                                                        xtype:'fieldset',
                                                        width:945,
                                                        height:90,
                                                        layout:'absolute',
                                                        title:'Datos del Emisor',
                                                        items:	[
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:5,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Empresa emisora:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbEmpresa,
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:35,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Certificado (CSD):</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbCertificado,
                                                                    {
                                                                        xtype:'label',
                                                                        x:360,
                                                                        y:35,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Serie a ulizar:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbSerie
                                                                ]
                                                    },
                                                    
                                                    {
                                                        x:10,
                                                        y:105,
                                                        xtype:'label',
                                                        html:'<span class="letraAzulSimple" style="color:#000">Tipo de comprobante:</span><span style="color:#F00;font-size:13px">*</span>'
                                                    },
                                                    cmbTipoComprobante,
                                                    {
                                                        x:400,
                                                        y:105,
                                                        xtype:'label',
                                                        html:'<span class="letraAzulSimple" style="color:#000">Complemento/Adenda:</span><span style="color:#F00;font-size:13px"></span>'
                                                    },
                                                    cmbComplemento,
                                                    {
                                                        x:10,
                                                        y:135,
                                                        xtype:'fieldset',
                                                        width:945,
                                                        height:180,
                                                        layout:'absolute',
                                                        title:'Datos del Comprobante',
                                                        items:	[
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:5,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">RFC/Raz&oacute;n Social:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbRFC,
                                                                    cmbRazonSocial,
                                                                    {
                                                                        x:570,
                                                                        y:5,
                                                                        xtype:'label',
                                                                        html:'<a href="javascript:agregarCliente()"><img src="../images/add.png"></a>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:600,
                                                                        y:5,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Fecha del comprobante:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    
                                                                    {
                                                                        x:770,
                                                                        y:0,
                                                                        id:'dteFechaComprobante',
                                                                        xtype:'datefield',
                                                                        value:'<?php echo date("Y-m-d")?>'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:35,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Lugar de expedici&oacute;n:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    {
                                                                        x:150,
                                                                        y:30,
                                                                        xtype:'textfield',
                                                                        id:'txtLugarExpedicion',
                                                                        width:200
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:360,
                                                                        y:35,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Moneda:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbMoneda,
                                                                     {
                                                                        xtype:'label',
                                                                        x:660,
                                                                        y:35,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Tipo de cambio:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    {
                                                                        x:770,
                                                                        y:30,
                                                                        xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                                        decimalPrecision :2,
            
                                                                        id:'txtTipoCambio',
                                                                        width:90
                                                                    },
                                                                     {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:65,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Forma de pago:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbFormaPago,
                                                                    {
                                                                        xtype:'label',
                                                                        x:360,
                                                                        hidden:true,
                                                                        y:65,
                                                                        id:'lblParcialidad',
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Parcialidad No.:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    {
                                                                        x:470,
                                                                        y:60,
                                                                        width:40,
                                                                        hidden:true,
                                                                        xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                                        id:'noParcialidad'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:525,
                                                                        y:65,
                                                                        hidden:true,
                                                                        id:'lblParcialidadDe',
                                                                        html:'<span class="letraAzulSimple" style="color:#000">de:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    {
                                                                        x:560,
                                                                        y:60,
                                                                        width:40,
                                                                        hidden:true,
                                                                        xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                                        id:'parcialidadAl'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:95,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Condiciones de pago:</span><span style="color:#F00;font-size:13px"></span>'
                                                                    },
                                                                    {
                                                                        xtype:'textfield',
                                                                        x:150,
                                                                        y:90,
                                                                        width:300,
                                                                        id:'txtCondicionesPago'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:10,
                                                                        y:125,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">M&eacute;todo de pago:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    cmbMetodoPago,
                                                                    {
                                                                        xtype:'label',
                                                                        x:360,
                                                                        y:125,
                                                                        hidden:true,
                                                                        id:'lblEspecifique',
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Especifique:</span><span style="color:#F00;font-size:13px">*</span>'
                                                                    },
                                                                    {
                                                                        x:460,
                                                                        y:120,
                                                                        width:150,
                                                                        hidden:true,
                                                                        xtype:'textfield',
                                                                        id:'txtEspecifique'
                                                                    },
                                                                    {
                                                                        xtype:'label',
                                                                        x:630,
                                                                        y:125,
                                                                        html:'<span class="letraAzulSimple" style="color:#000">No. Cuenta de Pago:</span><span style="color:#F00;font-size:13px"></span>'
                                                                    },
                                                                    {
                                                                        x:770,
                                                                        y:120,
                                                                        xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                                        
            
                                                                        id:'txtNoCuenta',
                                                                        width:150
                                                                    }
                                                                    
                                                                ]
                                                    }
                                                    ,
                                                    {
                                                        x:10,
                                                        y:310,
                                                        xtype:'fieldset',
                                                        width:945,
                                                        height:650,
                                                        layout:'absolute',
                                                        border:false,
                                                        items:	[
                                                                    crearGridConceptos(),
                                                                    {
                                                                        x:10,
                                                                        y:335,
                                                                        xtype:'label',
                                                                        html:'<span class="letraAzulSimple" style="color:#000">Motivo del descuento:</span>'
                                                                    },
                                                                    {
                                                                        x:150,
                                                                        y:330,
                                                                        width:370,
                                                                        disabled:true,
                                                                        xtype:'textfield',
                                                                        id:'txtMotivoDescuento'
                                                                       
                                                                    },
                                                                    {
                                                                        x:10,
                                                                        y:360,
                                                                        xtype:'fieldset',
                                                                        title:'Comentarios adicionales:',
                                                                        width:510,
                                                                        height:140,
                                                                        layout:'absolute',
                                                                        items:	[
                                                                                    {
                                                                                        xtype:'textarea',
                                                                                        width:480,
                                                                                        height:100,
                                                                                        id:'txtComentarios'
                                                                                    }
                                                                                ]
                                                                    },
                                                                    crearGridTotal()
                                                                   
                                                                    
                                                                    
                                                                ]
                                                    }
                                                    
                                                    
                                                    
                                                ]
                                        
                                    }
                                )
                   
                   
	new  Ext.TabPanel		(
    							{
                                	id:'tPanelFactura',
                                	renderTo:'tblFactura',
                                    activeTab:0,
                                    width:960,
                                    height:900,
                                    items:	[
                                    			pFactura
                                    		]
                                }
    						)                   
                    
		if(arrEmpresas.length==1)
        {
            cmbEmpresa.setValue(arrEmpresas[0][0]);
            cmbEmpresa.disable();
            dispararEventoSelectCombo('cmbEmpresa');
        }                    
		dispararEventoSelectCombo('cmbMoneda');                      
}

function crearGridConceptos()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'codigoAlternativo'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'unidadMedida'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'descuentoUnitario'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'iva'},
                                                                    {name: 'tasaIVA'},
                                                                    {name: 'total'},
                                                                    {name: 'descuentoTotal'},
                                                                    {name: 'cabecera'},
                                                                    {name: 'clave'}
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    cmbUnidadMedida=crearComboExt('cmbUnidadMedida',arrUnidadesMedida);
    
    
    var expander = new Ext.ux.grid.RowExpander({
                                                column:2,
                                                tpl : new Ext.Template(
                                                    '<br><table width="850px">'+
                                                    '<tr><td colspan="2" height="10"><span ><b>Cabecera del concepto:</b></span></td></tr>'+
                                                    '<tr><td><span style="font-size:11px">{cabecera}</span></td></tr>'+
                                                    '</tr>'+
                                                    '</table><br><br>'
                                                )
                                            });
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														expander,
                                                        {
															header:'Clave',
															width:50,
															sortable:true,
															dataIndex:'clave',
                                                            renderer:mostrarValorDescripcion,
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Concepto',
															width:210,
															sortable:true,
															dataIndex:'descripcion',
                                                            renderer:mostrarValorDescripcion,
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Unidad de medida',
															width:100,
															sortable:true,
															dataIndex:'unidadMedida',
                                                            editor:cmbUnidadMedida,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrUnidadesMedida,val);    
                                                                    }
														},
														{
															header:'Costo Unitario',
															width:85,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Desc. Unitario',
															width:80,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'descuentoUnitario',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Cantidad',
															width:60,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'cantidad',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Subtotal',
															width:70,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'subtotal',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
                                                        {
															header:'Tasa IVA',
															width:60,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'tasaIVA',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'IVA',
															width:80,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'iva',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Total',
															width:90,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:0,
                                                            id:'gConcepto',
                                                            x:0,
                                                            plugins:[expander],
                                                            clicksToEdit:1,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            border:true,
                                                            columnLines : true,
                                                            height:320,
                                                           	width:920,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            
                                                                            text:'Agregar concepto de almac&eacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	buscarPorProductoNombre(1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar concepto abierto',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                                                
                                                                                                                
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	idConcepto:'',
                                                                                                                tipoConcepto:1,
                                                                                                                codigoAlternativo:'',
                                                                                                                descripcion:'',
                                                                                                                unidadMedida:'',
                                                                                                                costoUnitario:0,
                                                                                                                descuentoUnitario:0,
                                                                                                                cantidad:1,
                                                                                                                subtotal:0,
                                                                                                                iva:0,
                                                                                                                tasaIVA:0,
                                                                                                                total:0,
                                                                                                                descuentoTotal:0,
                                                                                                                cabecera:'',
                                                                                                                clave:''
                                                                                                            	
                                                                                                            }
                                                                                        				)                            
                                                                                    
                                                                                    
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover concepto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el concepto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);
                                                                                                calcularTotales();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el concepto seleccionado?',resp);
                                                                                        return;
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/tablero.gif',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar cabecera del concepto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el concepto cuya cabecera desea modificar');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        mostrarVentanaCabeceraConcepto(fila);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                                        ,'-',
                                                                        {
                                                                        	xtype:'checkbox',
                                                                            boxLabel:'Deshabilitar calculo autom&aacute;tico',
                                                                            id:'chkDesCalculo'
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
                
                
                
	tblGrid.on('beforeedit',function(e)                                                    
    						{
                            	switch(e.field)
                                {
                                	case 'subtotal':
                                    case 'iva':
                                    case 'total':
                                    	if(!gEx('chkDesCalculo').getValue())
                                    	{
                                        	e.cancel=true
                                        }
                                    
                                    break;
                                }
                            }
			)                            		
                                                    
	tblGrid.on('afteredit',function(e)
    						{
                            
                            	if(e.record.data.costoUnitario=='')
                                {
                                	e.record.set('costoUnitario',0);
                                }
                                
                                if(e.record.data.cantidad=='')
                                {
                                	e.record.set('cantidad',0);
                                }
                                
                                if(e.record.data.subtotal=='')
                                {
                                	e.record.set('subtotal',0);
                                }
                                
                                if(e.record.data.iva=='')
                                {
                                	e.record.set('iva',0);
                                }
                                
                                
                                if(e.record.data.tasaIVA=='')
                                {
                                	e.record.set('tasaIVA',0);
                                }
                                
                                if(e.record.data.total=='')
                                {
                                	e.record.set('total',0);
                                }
                            
                            	if(parseFloat(e.record.data.descuentoUnitario)>parseFloat(e.record.data.costoUnitario))
                                {
                                	e.record.set(e.field,e.originalValue);
                                    e.grid.stopEditing();
                                    function respAux()
                                    {
                                    	e.grid.startEditing(e.row,e.column);
                                    }
                                    msgBox('Costo unitario NO puede ser menor que el descuento unitario',respAux);
                                	return;
                                }
                            
                            	if(!gEx('chkDesCalculo').getValue())
                                {    	
                                    switch(e.field)
                                    {
                                        case 'costoUnitario':
                                        case 'descuentoUnitario':
                                        case 'cantidad':
                                        case 'tasaIVA':
                                        
                                                var costoDescuento=parseFloat(e.record.data.costoUnitario)-parseFloat(e.record.data.descuentoUnitario);
                                                var subtotal=parseFloat(Ext.util.Format.number(costoDescuento*parseFloat(e.record.data.cantidad),'0.00'));
                                                var iva=parseFloat(Ext.util.Format.number(subtotal*(parseFloat(e.record.data.tasaIVA)/100),'0.00'));
                                                var total=subtotal+iva;
                                                var descuentoTotal=(parseFloat(e.record.data.costoUnitario)*parseFloat(e.record.data.cantidad))-subtotal;
                                        
                                        
                                                e.record.set('subtotal',subtotal);
                                                e.record.set('iva',iva);
                                                e.record.set('total',total);
                                                e.record.set('descuentoTotal',descuentoTotal);
                                        
                                        break;
                                        
                                    
                                    }
                            	}
                                else
                                {
                                	
                                	descuentoTotal=parseFloat(e.record.data.cantidad)*parseFloat(e.record.data.descuentoUnitario);
                                    e.record.set('descuentoTotal',descuentoTotal);
                                }
                                
                                calcularTotales();
                            }
    			)                                                    
                                                    
                                                    
	return 	tblGrid;	
}

function crearGridTotal()
{
	var dsDatos=arrConceptosDefault;

    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                    	sortInfo: {
                                                                        field: 'orden',
                                                                        direction: 'ASC' 
                                                                    },
                                                        fields:	[
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tasaConcepto'},
                                                                    {name: 'montoConcepto'},
                                                                    {name: 'orden', type:'int'},
                                                                    {name: 'porcentajeModificable'},
                                                                    {name: 'comentarios'},
                                                                    {name: 'removible'},
                                                                    {name: 'lblConcepto'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														{
															header:'',
															width:150,
															sortable:false,
                                                            hidden:true,
                                                            css:'text-align:right;',
															dataIndex:'idConcepto',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	var arr;
                                                                        
                                                                        var comp='';
                                                                        if(registro.data.removible=='1')
                                                                        {
                                                                        	comp='<a href="javascript:removerConcepto('+nFila+')"><img src="../images/delete.png" width="10" height="10" title="Remover concepto" alt="Remover concepto"/></a>&nbsp;';
                                                                        }
                                                                    	switch(registro.data.tipoConcepto)
                                                                        {
                                                                        	case '1':
                                                                            	arr=arrImpuestos;
                                                                               	
	                                                                        	
                                                                            break;
                                                                            case '2':
                                                                            	arr=arrRetenciones;
	                                                                        	
                                                                            break;
                                                                            case '0':
                                                                            	arr=arrIntermedios;
                                                                            	
                                                                            break;
                                                                            case '3':
                                                                            	arr=arrDescuentos;
                                                                            	
                                                                            break;
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        return comp+'<a href="javascript:habilitarEdicion(\''+bE(nFila)+'\')"><span style="font-size:12px"  ><b>'+mostrarValorDescripcion(formatearValorRenderer(arr,val))+'</b></span></a>';
                                                                        	
                                                                        
                                                                    }
														},
                                                        {
															header:'',
															width:150,
															sortable:false,
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		},
                                                            css:'text-align:right;',
															dataIndex:'lblConcepto',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	var comp='';
                                                                        if(registro.data.removible=='1')
                                                                        {
                                                                        	comp='<a href="javascript:removerConcepto('+nFila+')"><img src="../images/delete.png" width="10" height="10" title="Remover concepto" alt="Remover concepto"/></a>&nbsp;';
                                                                        }
                                                                    	
                                                                        
                                                                        return comp+'<span style="font-size:12px" ondblclick="habilitarEdicion(\''+bE(nFila)+'\')"  ><b>'+mostrarValorDescripcion(val)+'</b></span>';
                                                                        	
                                                                        
                                                                    }
														},
														{
															header:'',
															width:55,
															sortable:false,
															dataIndex:'tasaConcepto',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                                        
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val) && (val!=''))
	                                                                    	return Ext.util.Format.number(val,'0.00')+' %';
                                                                    }
														},
														{
															header:'',
															width:120,
                                                            css:'text-align:right;',
															sortable:false,
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                                        
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	return  '<span style="font-size:12px">'+Ext.util.Format.usMoney(val)+'</span>';
                                                                    },
                                                                    
															dataIndex:'montoConcepto'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            x:560,
                                                            y:330,
                                                            border:false,
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:250,
                                                            id:'gMontosFinales',
                                                            width:360,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'checkbox',
                                                                            //checked:true,
                                                                            boxLabel:'Deshabilitar c&aacute;lculo autom&aacute;tico',
                                                                            id:'chkDesCalculoTotal',
                                                                            listener:	{
                                                                            				change:function(chk,valor)
                                                                                            	{
                                                                                                	if(!valor)
                                                                                                    	gEx('gMontosFinales').getView().refresh();
                                                                                                    	
                                                                                                }
                                                                            			}
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            
                                                                            text:'Agregar concepto detalle...',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaConceptosDetalle();
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	tblGrid.on('beforeedit',function(e)
    						{
                            	
                            	if(e.field=='lblConcepto')
                                {
                                }
                                else
                                {
                                    if((!gEx('chkDesCalculoTotal').getValue())&&(e.field!='tasaConcepto'))
                                        e.cancel=true;
                                    if((e.field=='tasaConcepto') &&(e.record.data.porcentajeModificable=='0'))
                                    {
                                        e.cancel=true;
                                    }
                                }
                            }
    			)     
                
	tblGrid.on('afteredit',function(e)
    						{
                            	
                            	if(e.field=='montoConcepto')
                                {
                                	if(e.record.data.idConcepto=='1')
                                    {
                                    
                                    	var x=0;
                                        var gConcepto=gEx('gConcepto');
                                        var fila;
                                        for(x=0;x<gConcepto.getStore().getCount();x++)
                                        {
                                        	fila=gConcepto.getStore().getAt(x);
                                            fila.set('tasaIVA',0);
                                            fila.set('iva',0);
                                            fila.set('total',fila.data.subtotal);
                                            
                                        }
                                        gConcepto.getView().refresh();
                                        
                                    }
                                }
                            
                            	if((e.record.data.tipoConcepto!='0')&&(e.record.data.tasaConcepto==''))
                                	e.record.set('tasaConcepto',0);
                            	if(e.record.data.montoConcepto=='')
                                	e.record.set('montoConcepto',0);

                                if(!gEx('chkDesCalculoTotal').getValue())
	                             	calcularTotales();   
                            }
    			)                     
                                                               
	return 	tblGrid;		
}


function habilitarEdicion(f)
{
	
	gEx('gMontosFinales').startEditing(parseInt(bD(f)),1);
}


function mostrarVentanaCabeceraConcepto(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                           html:'<span style="clor:#000"><b>Concepto:</b></span>'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:10,
                                                           html:'<span style="clor:#900">'+fila.data.descripcion+'</span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:60,
                                                           html:'<span style="clor:#000"><b>Cabecera del concepto:</b></span>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:80,
                                                            xtype:'textarea',
                                                            width:650,
                                                            height:130,
                                                            id:'txtCabecera',
                                                            value:escaparBR(fila.data.cabecera)
                                                        }
                                                       
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar cabecera del concepto',
										width: 700,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCabecera').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var gConcepto=gEx('gConcepto');
																		fila.set('cabecera',escaparEnter(gEx('txtCabecera').getValue().trim()));
                                                                        var pos;

                                                                        for(pos=0;pos<gConcepto.getStore().getCount();pos++)
                                                                        {
                                                                        	 gConcepto.plugins[0].collapseRow(pos);
                                                                        }
                                                                        
                                                                        gConcepto.getView().refresh();
                                                                       
                                                                        
                                                                        
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

///

function buscarPorProductoNombre(idZona)
{

	

	var regProducto=null;

	
    
    var gridProductoBuscar=crearGridBuscarProducto();
    
    
    
    
    var oConf=	{
    					idCombo:'cmbCodigoAlterno',
                        anchoCombo:200,
                        campoDesplegar:'codigoAlterno',
                        campoID:'idProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:5,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    
                                    	
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbCodigoAlterno').getRawValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        
                                        
                                        
                                        gEx('cmbDescripcion').setValue(''); 
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	var fila=registro;
                                        function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                var arrDatos=eval(arrResp[1]);
                                                var oProducto=arrDatos[0];
                                                var r=new reg	(
                                                                    {
                                                                        idConcepto:oProducto.idProducto+"_"+oProducto.llave,
                                                                        tipoConcepto:2,
                                                                        codigoAlternativo:'',
                                                                        descripcion:oProducto.descripcion,
                                                                        unidadMedida:oProducto.unidadMedida,
                                                                        costoUnitario:oProducto.costoUnitario,
                                                                        descuentoUnitario:oProducto.descuento,
                                                                        cantidad:0,
                                                                        subtotal:0,
                                                                        iva:0,
                                                                        tasaIVA:oProducto.porcentajeIVA,
                                                                        total:0,
                                                                        descuentoTotal:0,
                                                                        cabecera:'',
                                                                        clave:oProducto.codigoAlterno
                                                                        
                                                                    }
                                                                )                            
                                            
                                            
                                                gEx('gConcepto').getStore().add(r);
                                                gEx('gConcepto').startEditing(gEx('gConcepto').getStore().getCount()-1,7);
                                               
                                                gEx('vBuscarDescripcion').close();
                                                
                                            
                                               
                                                
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=190&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave,true);
                                    
                                     
                                     
                                           
                                           gEx('vBuscarDescripcion').close();
                                        
                                    }  
    				};

    
	var cmbCodigoAlterno=crearComboExtAutocompletar(oConf);
    
    
    
     var oConf=	{
    					idCombo:'cmbDescripcion',
                        anchoCombo:330,
                        campoDesplegar:'descripcion',
                        campoID:'idProducto',
                        funcionBusqueda:189,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:170,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesAlmacen.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{codigoAlterno}] {descripcion}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idProducto'},
                                    {name:'llave'},
                                    {name:'codigoAlterno'},
                                    {name:'descripcion'},
                                    {name: 'tasaIVA'}
                                    
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	var arrChek=gEN('chkSel');
                                        var arrCategorias='';
                                        var x;
                                        for(x=0;x<arrChek.length;x++)
                                        {
                                            if(arrChek[x].checked)
                                            {
                                                if(arrCategorias=='')
                                                    arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
                                                else
                                                    arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
                                            }
                                        }
                                    	
                                        
                                        dSet.baseParams.valorBusqueda=gEx('cmbDescripcion').getRawValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        
                                        dSet.baseParams.arrCategorias=arrCategorias;
                                        dSet.baseParams.idZona=idZona;
                                        gEx('cmbCodigoAlterno').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    

                                    	var fila=registro;
                                        function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                                var arrDatos=eval(arrResp[1]);
                                                var oProducto=arrDatos[0];
                                                var r=new reg	(
                                                                    {
                                                                        idConcepto:oProducto.idProducto+"_"+oProducto.llave,
                                                                        tipoConcepto:2,
                                                                        codigoAlternativo:'',
                                                                        descripcion:oProducto.descripcion,
                                                                        unidadMedida:oProducto.unidadMedida,
                                                                        costoUnitario:oProducto.costoUnitario,
                                                                        descuentoUnitario:oProducto.descuento,
                                                                        cantidad:0,
                                                                        subtotal:0,
                                                                        iva:0,
                                                                        tasaIVA:oProducto.porcentajeIVA,
                                                                        total:0,
                                                                        descuentoTotal:0,
                                                                        cabecera:'',
                                                                        clave:oProducto.codigoAlterno
                                                                        
                                                                    }
                                                                )                            
                                            
                                            
                                                gEx('gConcepto').getStore().add(r);
                                                gEx('gConcepto').startEditing(gEx('gConcepto').getStore().getCount()-1,7);
                                               
                                                gEx('vBuscarDescripcion').close();
                                                
                                            
                                               
                                                
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=190&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave,true);
                                    
                                     
                                     
                                       
                                        
                                       	
                                    }  
    				};

    
	var cmbDescripcion=crearComboExtAutocompletar(oConf);
    
 
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'fieldset',
                                                            defaultType: 'label',
                                                            x:10,
                                                            y:10,
                                                            layout:'absolute',
                                                            width:550,
                                                            height:100,
                                                            title:'B&uacute;squeda de producto',
                                                            items:	[
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'<b>C&oacute;digo alterno:</b>'
                                                                        },
                                                                        cmbCodigoAlterno,
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            id:'lblDescripcion',
                                                                            html:'<b>Descripci&oacute;n del producto:</b>'
                                                                        },
                                                                        cmbDescripcion
                                                                        
                                                                    ]
                                                        },
                                                        gridProductoBuscar,
                                                        crearGridCategorias()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Buscar producto',
                                        id:'vBuscarDescripcion',
										width: 840,
										height:440,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbCodigoAlterno').focus(false,500);
																}
															}
												},
										buttons:	[
														
														{
															text: 'Cerrar',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    //cargarProductosBusqueda();
      
}

function crearGridCategorias()
{
	var store = new Ext.ux.maximgb.tg.AdjacencyListStore(	
      														{
                                                                autoLoad : true,
                                                                url: '../paginasFunciones/funcionesAlmacen.php',
                                                                reader: new Ext.data.JsonReader(
                                                                                                    {
                                                                                                        id: 'idCategoria',
                                                                                                        root: 'registros',
                                                                                                        totalProperty: 'numReg',
                                                                                                        fields:	[
                                                                                                                    {name: 'idCategoria'},
                                                                                                                    {name: 'nombreCategoria'},
                                                                                                                    {name: 'descripcion'},
                                                                                                                    {name: '_parent'},
                                                                                                                    {name: '_is_leaf', type: 'bool'},
                                                                                                                    {name: 'nivel'},
                                                                                                                    {name: 'llave'}
                                                                                                                ]
                                                                                                    }
                                                                                                    
                                                                                                )
                                                         	}
                                                          ); 
                
	
    store.on('beforeload',function(proxy)
    						{
                            	
                                proxy.baseParams.funcion=158;
                                proxy.baseParams.idAlmacen='<?php echo $listAlmacenes?>';
                                
                            }
    		)
    				
                    
	store.on('load',function(almacen)
    				{
                    	//gEx('arbolCategorias').getStore().expandAll();
                    }    
            )
    			                    
                    
    grid=new  Ext.ux.maximgb.tg.GridPanel(	{
                                                    x:570,
                                                    y:15,
                                                    enableDD: false,
                                                    border:true,
                                                    autoScroll:true,
                                                    disableSelection:true,
                                                    
                                                    id:'arbolCategorias',
													store: store,
                                                    stripeRows: true,
                                                    
                                                    columnLines :true,
													loadMask :true,
                                                    width:220,
                                                    height:310,
                                                    columns:	[
                                                    				
                                                                    {
                                                                        header: 'Categor&iacute;a del producto',
                                                                        dataIndex: 'nombreCategoria',
                                                                        width: 180,
                                                                        renderer:function(v,meta,record)
                                                                        {
                                                                        	if(record.data._is_leaf)
                                                                            {
                                                                        		var checado='';
                                                                        		
                                                                                return [
                                                                                           '<img src="', Ext.BLANK_IMAGE_URL, '" class="ux-maximgb-tg-mastercol-icon" />',
                                                                                           '<input type="checkbox" onclick="checkSelBusqueda(this)" class="ux-maximgb-tg-mastercol-cb" ext:record-id="', record.id, '" id="'+record.data.llave+
                                                                                           '" '+checado+' name="chkSel"  />&nbsp;',
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                        
                                                                  			}
                                                                            else
                                                                            {
                                                                            	var checado='';
                                                                        		
                                                                                return [
                                                                                           
                                                                                           '<span class="ux-maximgb-tg-mastercol-editorplace">', mostrarValorDescripcion(v), '</span>'
                                                                                        ].join('');
                                                                            }
                                                                            
                                                                        }
                                                                    }
                                                         		]
                                                     
                                            
                                                    
                                                }
                                          );
                                          
	
    
     
     
    
                                          
	return grid;  
   
}

function checkSelBusqueda()
{
	cargarProductosBusqueda(1);
}

function cargarProductosBusqueda(idZona)
{
	var arrChek=gEN('chkSel');
    var arrCategorias='';
    var x;
    for(x=0;x<arrChek.length;x++)
    {
		if(arrChek[x].checked)
        {
            if(arrCategorias=='')
                arrCategorias="'"+arrChek[x].getAttribute('id')+"'";
            else
                arrCategorias+=',\''+arrChek[x].getAttribute('id')+"'";
        }
    }
	var gProductosBuscados=gEx('gProductosBuscados');
    gProductosBuscados.getStore().load	(
    										{
                                            	url: '../paginasFunciones/funcionesAlmacen.php',
                                                
                                                params:	{
                                                			funcion:175,
                                                            categorias:arrCategorias,
                                                			criterio:'2',
                                                            valor:'',
                                                            idZona:idZona
                                                		}
                                            }
    									)
}

function crearGridBuscarProducto()
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idProducto'},
		                                                {name: 'nombreProducto'},
                                                        {name: 'llave'},
                                                        {name: 'tasaIVA'},
                                                        {name: 'precioUnitario'},
                                                        {name: 'codigoBarras'},
                                                        {name: 'codigoAlterno'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesAlmacen.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombreProducto', direction: 'ASC'},
                                                            groupField: 'nombreProducto',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            
                                                            {
                                                                header:'Producto',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'nombreProducto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Precio unitario',
                                                                width:150,
                                                                hidden:true,
                                                                sortable:true,
                                                                dataIndex:'precioUnitario',
                                                                renderer:'usMoney',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo de barras',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoBarras',
                                                                css:'text-align:right'
                                                            },
                                                            {
                                                                header:'C&oacute;digo alterno',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'codigoAlterno',
                                                                css:'text-align:right'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gProductosBuscados',
                                                                store:alDatos,
                                                                x:10,
                                                                y:120,
                                                                width:550,
                                                                height:200,
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true, 
                                                                listeners:	{
                                                                				rowdblclick:function(grid,nFila)
                                                                                            {
                                                                                                var fila=grid.getStore().getAt(nFila);
                                                                                                
                                                                                               
                                                                                                
                                                                                               	
                                                                                                 
                                                                                               
                                                                                                    function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            var arrDatos=eval(arrResp[1]);
                                                                                                            var oProducto=arrDatos[0];
                                                                                                            var r=new reg	(
                                                                                                                                {
                                                                                                                                    idConcepto:oProducto.idProducto+"_"+oProducto.llave,
                                                                                                                                    tipoConcepto:2,
                                                                                                                                    codigoAlternativo:'',
                                                                                                                                    descripcion:oProducto.descripcion,
                                                                                                                                    unidadMedida:oProducto.unidadMedida,
                                                                                                                                    costoUnitario:oProducto.costoUnitario,
                                                                                                                                    descuentoUnitario:oProducto.descuento,
                                                                                                                                    cantidad:0,
                                                                                                                                    subtotal:0,
                                                                                                                                    iva:0,
                                                                                                                                    tasaIVA:oProducto.porcentajeIVA,
                                                                                                                                    total:0,
                                                                                                                                    descuentoTotal:0,
                                                                                                                                    cabecera:'',
                                                                        															clave:oProducto.codigoAlterno
                                                                                                                                    
                                                                                                                                }
                                                                                                                            )                            
                                                                                                        
                                                                                                        
                                                                                                            gEx('gConcepto').getStore().add(r);
                                                                                                            gEx('gConcepto').startEditing(gEx('gConcepto').getStore().getCount()-1,7);
                                                                                                           
                                                                                                           	gEx('vBuscarDescripcion').close();
                                                                                                            
                                                                                                        
                                                                                                           
                                                                                                            
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesAlmacen.php',funcAjax, 'POST','funcion=190&idProducto='+fila.data.idProducto+'&llave='+fila.data.llave,true);
                                                                                                
                                                                                                 
                                                                                                 
                                                                                                  
                                                                                                
                                                                                            }
                                                                			} ,                                                              
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
                                                        
	                                                      
                                                        
        return 	tblGrid;
}

function mostrarVentanaConceptosDetalle()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Especifique los conceptos detalle que desea agregar:</span>'
                                                        },
														crearGridDetalleConcepto()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar concepto detalle',
										width: 720,
										height:450,
                                        y:500,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var gConceptosDetalle=gEx('gConceptosDetalle');
                                                                        var filas=gConceptosDetalle.getSelectionModel().getSelections();
                                                                        if(filas.length==0)	
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos un concepto a agregar');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var reg=crearRegistro	(
                                                                        							[
                                                                                                        {name: 'tipoConcepto'},
                                                                                                        {name: 'idConcepto'},
                                                                                                        {name: 'tasaConcepto'},
                                                                                                        {name: 'montoConcepto'},
                                                                                                        {name: 'orden'},
                                                                                                        {name: 'porcentajeModificable'},
                                                                                                        {name: 'comentarios'},
                                                                                                        {name: 'lblConcepto'}
                                                                                                    ]
                                                                                                  )
                                                                         
                                                                         
                                                                         
                                                                         
                                                                      	var gMontosFinales=gEx('gMontosFinales') ; 
                                                                        
                                                                        
                                                                        var x;
                                                                        var fila;
                                                                        var r;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	fila=filas[x];
                                                                            r=new reg	(
                                                                            				{
                                                                                            	tipoConcepto:fila.data.tipoConcepto,
                                                                                                idConcepto:fila.data.idConcepto,
                                                                                                tasaConcepto:fila.data.porcentajeDefault,
                                                                                                montoConcepto:0,
                                                                                                orden:parseInt(fila.data.orden),
                                                                                                porcentajeModificable:fila.data.porcentajeModificable,
                                                                                                comentarios:fila.data.comentarios,
                                                                                                removible:fila.data.removible,
                                                                                                lblConcepto:fila.data.descripcion
                                                                                                
                                                                                            }
                                                                            			)
                                                                            
                                                                        	gMontosFinales.getStore().add(r);    
                                                                        }
                                                                        gMontosFinales.getStore().sort	(
                                                                        									[
                                                                                                                {
                                                                                                                    field:'orden',
                                                                                                                    direction: 'ASC'
                                                                                                                }
                                                                                                            ]
                                                                        								)
                                                                       
                                                                    	calcularTotales();  
                                                                        ventanaAM.close();                                  
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDetalleConcepto()
{
	var dsDatos=[];
    
    var gMontosFinales=gEx('gMontosFinales');
    var x;
    var fila;
    var obj;
    for(x=0;x<arrConceptosImpRet.length;x++)
    {
    	fila=arrConceptosImpRet[x];
        if((fila[0]=='19')||(fila[0]=='18')||(fila[0]=='20')||(obtenerPosFila(gMontosFinales.getStore(),'idConcepto',fila[0])==-1))
        {
        	obj=[fila[0],fila[1],fila[2],fila[3],fila[5],fila[6],fila[7],fila[8],fila[9]];
        	dsDatos.push(obj);
        }
    
    }
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                    	sortInfo: {
                                                                        field: 'orden',
                                                                        direction: 'ASC' 
                                                                    },
                                                        fields:	[
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'funcionCalculo'},
                                                                    {name: 'porcentajeDefault'},
                                                                    {name: 'orden', type:'int'},
                                                                    {name: 'porcentajeModificable'},
                                                                    {name: 'comentarios'},
                                                                    {name: 'removible'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Concepto detalle',
															width:300,
															sortable:true,
                                                            renderer:mostrarValorDescripcion,
															dataIndex:'descripcion'
														},
                                                        {
															header:'Valor tasa',
															width:60,
															sortable:true,
															dataIndex:'porcentajeDefault',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val) && (val!=''))
                                                                        	return Ext.util.Format.number(val,'0.00')+' %';
                                                                    }
														},
														{
															header:'Comentarios',
															width:330,
															sortable:true,
															dataIndex:'comentarios',
                                                            renderer:mostrarValorDescripcion
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            y:40,
                                                            x:10,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gConceptosDetalle',
                                                            columnLines : true,
                                                            height:310,
                                                            width:675,
                                                            sm:chkRow
                                                            
                                                        }
                                                    );
	return 	tblGrid;	

}

function removerConcepto(nFila)
{
	var gMontosFinales=gEx('gMontosFinales');
    var fila=gMontosFinales.getStore().getAt(nFila);
    gMontosFinales.getStore().remove(fila);
    calcularTotales();
    gEx('gMontosFinales').getView().refresh();
}

function agregarCliente()
{
	var accionCancelar=bE('{msgConfirm(\'Est&aacute; seguro de querer cancelar la operaci&oacute;n?\',function(btn){if(btn==\'yes\'){window.parent.cerrarVentanaFancy();}})}');
	var obj={};
    obj.titulo='Alta de cliente';
    obj.ancho=920;
    obj.alto=750;
    obj.url="../modeloAlmacenes/proveedores.php";
    obj.complete=	function()
    				{
				      $("#fancybox-wrap").css({'top':'20px', 'bottom':'auto'});    
                    }      
    obj.params=[['esCliente','true'],['cPagina','sFrm=true'],['eJs',bE('window.parent.asignarProveedor(@idRegistro);')],['accionCancelar',accionCancelar],['idEmpresa',-1],['referencia','<?php echo $referenciaFiltros?>']];
    abrirVentanaFancy(obj);
}

function asignarProveedor(iRegistro)
{
	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            idEmpresaFacturacion=iRegistro;
            gEx('cmbRFC').setRawValue(arrResp[1]); 
            gEx('cmbRazonSocial').setRawValue(arrResp[2]); 
            
            cerrarVentanaFancy();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=45&idEmpresa='+iRegistro,true);
}