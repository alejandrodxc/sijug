<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT claveElemento,nombreElemento FROM 1018_catalogoVarios WHERE tipoElemento=1";
	$arrSituacion=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT cveEstado,estado FROM 820_estados ORDER BY estado";
	$arrEstados=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idMoneda,moneda FROM 603_tipoMoneda ORDER BY moneda";
	$arrMonedas=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoCuentaBancaria,tipoCuentaBancaria FROM 6007_tiposCuentaBancaria";
	$arrTipoCuenta=$con->obtenerFilasArreglo($consulta);
?>
var arrMonedas=<?php echo $arrMonedas?>;
var arrEstados=<?php echo $arrEstados?>;
var arrSituacion=<?php echo $arrSituacion?>;
var  arrTipoCuenta=<?php echo $arrTipoCuenta?>;
var nodoSel=null;

Ext.onReady(inicializar);

function inicializar()
{


	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
											{
												baseParams:{
																funcion:'5'
															},
												dataUrl:'../paginasFunciones/funcionesTesoreria.php'
											}
										)	
	cargadorArbol.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.idBanco=gE('idBanco').value;
                                        nodoSel=null;
                                        gEx('btnModifDatosBanco').disable();
                                        gEx('btnModifDatosSurcursal').disable();
                                        gEx('btnModifDatosCuenta').disable();
                                        gEx('btnModifDatosChequera').disable();
                                    }
                     )
	var arbolOpciones=new Ext.tree.TreePanel	(
														{
                                                        	id:'arbolBancos',
															renderTo:'tblContenedor',
															useArrows:true,
															autoScroll:true,
															animate:true,
															enableDD:true,
															containerScroll: true,
															root:raiz,
                                                            width:960,
                                                            height:450,
                                                            border:true,
															loader: cargadorArbol,
															rootVisible:false,
															tbar:
															[
                                                            	
                                                                {
                                                                    icon:'../images/pencil.png',
                                                                    cls:'x-btn-text-icon',
                                                                    id:'btnModifDatosBanco',
                                                                    disabled:true,
                                                                    text:'Modificar datos del Banco',
                                                                    handler:function()
                                                                            {
                                                                             	modificarBanco(nodoSel) ;  
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/sitemap_color.png',
                                                                    cls:'x-btn-text-icon',
                                                                    id:'btnModifDatosSurcursal',
                                                                    disabled:true,
                                                                    text:'Agregar/modificar datos de Sucursal',
                                                                    handler:function()
                                                                            {
                                                                            	if(nodoSel.attributes.tipo=='0')
	                                                                                mostrarVentanaSucursal();
                                                                                else
                                                                                	mostrarVentanaSucursal(nodoSel);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/coins.png',
                                                                    cls:'x-btn-text-icon',
                                                                    id:'btnModifDatosCuenta',
                                                                    disabled:true,
                                                                    text:'Agregar/modificar datos de Cuenta',
                                                                    handler:function()
                                                                            {
                                                                            	
                                                                                if(nodoSel.attributes.tipo=='1')
	                                                                                mostrarVentanaCuenta();
                                                                                else
                                                                                	mostrarVentanaCuenta(nodoSel);
                                                                            }
                                                                    
                                                                },'-',
                                                                {
                                                                    icon:'../images/money_bookers.png',
                                                                    cls:'x-btn-text-icon',
                                                                    id:'btnModifDatosChequera',
                                                                    disabled:true,
                                                                    text:'Agregar/modificar datos de Chequera',
                                                                    handler:function()
                                                                            {
                                                                            	
                                                                            	if(nodoSel.attributes.tipo=='2')
	                                                                                mostrarVentanaChequera();
                                                                                else
                                                                                	mostrarVentanaChequera(nodoSel);
                                                                                
                                                                            }
                                                                    
                                                                }
                                                            ]
                                                       }
												)           
 
 	arbolOpciones.on('click',funcClikArbol);
    arbolOpciones.expandAll();                         
	
}

function funcClikArbol(nodo, evento)
{
	nodoSel=nodo;
    gEx('btnModifDatosBanco').disable();
    gEx('btnModifDatosSurcursal').disable();
    gEx('btnModifDatosCuenta').disable();
    gEx('btnModifDatosChequera').disable();
    
    switch(nodoSel.attributes.tipo)
    {
    	case '0':
        	gEx('btnModifDatosBanco').enable();
            gEx('btnModifDatosSurcursal').enable();
            
        break;
        case '1':
        	gEx('btnModifDatosSurcursal').enable();
            gEx('btnModifDatosCuenta').enable();
        break;
        case '2':
        	
            gEx('btnModifDatosCuenta').enable();
            if(nodoSel.attributes.cuentaCheques=='1')
	            gEx('btnModifDatosChequera').enable();
        break;
        case '3':
        	 gEx('btnModifDatosChequera').enable();
        break;
    }
}

function modificarBanco(objDatos)
{
	var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion,140,125,200);
    cmbSituacion.setValue(nodoSel.attributes.situacion);
	var lblTitulo='Agregar Banco';
    if(objDatos)
    	lblTitulo='Modificar datos de Banco ['+objDatos.attributes.nombreBanco+']';
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre del Banco: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'nombreBanco',
                                                            value:nodoSel.attributes.nombreBanco,
                                                            width:400
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre corto:'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'nombreCorto',
                                                            value:nodoSel.attributes.nombreCorto,
                                                            width:200
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Funci&oacute;n generadora de Layout de transferencia:'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:65,
                                                            xtype:'textfield',
                                                            readOnly:true,
                                                            id:'idFuncionEscritorLayOut',
                                                            value:nodoSel.attributes.nombreFuncionTransferencia,
                                                            idFuncion:nodoSel.attributes.idFuncionLayOutTranferencia,
                                                            width:230
                                                        },
                                                         {
                                                        	xtype:'label',
                                                            y:65,
                                                            x:510,
                                                            html:'<a href="javascript:mostrarVentanaFuncion(1)"><img src="../images/pencil.png" title="Asignar funci&oacute;n" alt="Asignar funci&oacute;n"></a>&nbsp;&nbsp;<a href="javascript:removerFuncion(1)"><img src="../images/delete.png" title="Remover funci&oacute;n" alt="Remover funci&oacute;n"></a>'
                                                        },
                                                         {
                                                        	x:10,
                                                            y:100,
                                                            html:'Funci&oacute;n lectora de Layout de importacion:'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:95,
                                                            xtype:'textfield',
                                                            readOnly:true,
                                                            idFuncion:nodoSel.attributes.idFuncionLayOutRecepcion,
                                                            value:nodoSel.attributes.nombreFuncionRecepcion,
                                                            id:'idFuncionLectorLayOut',
                                                            width:230
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            y:95,
                                                            x:510,
                                                            html:'<a href="javascript:mostrarVentanaFuncion(2)"><img src="../images/pencil.png" title="Asignar funci&oacute;n" alt="Asignar funci&oacute;n"></a>&nbsp;&nbsp;<a href="javascript:removerFuncion(2)"><img src="../images/delete.png" title="Remover funci&oacute;n" alt="Remover funci&oacute;n"></a>'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            html:'Situaci&oacute;n:'
                                                        },				
                                                        cmbSituacion

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 600,
										height:260,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nombreBanco').focus(500,false);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var nombreBanco=gEx('nombreBanco');
                                                                        var nombreCorto=gEx('nombreCorto');
                                                                        var idFuncionEscritorLayOut=gEx('idFuncionEscritorLayOut');
                                                                        var idFuncionLectorLayOut=gEx('idFuncionLectorLayOut');
                                                                        if(nombreBanco.getValue().trim()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	nombreBanco.focus();
                                                                            }
                                                                            msgBox('El nombre del banco es obligatorio',resp);
                                                                            return;
                                                                        }
                                                                        var idFuncionEsc=-1;
                                                                        if(idFuncionEscritorLayOut.getValue()!="")
                                                                        	idFuncionEsc=idFuncionEscritorLayOut.idFuncion;
                                                                        var idFuncionLect=-1;
                                                                        if(idFuncionLectorLayOut.getValue()!="")
                                                                        	idFuncionLect=idFuncionLectorLayOut.idFuncion;
                                                                        
                                                                        var cadObj='{"situacion":"'+cmbSituacion.getValue()+'","idBanco":"'+nodoSel.id+'","nombreBanco":"'+cv(nombreBanco.getValue())+'","nombreCorto":"'+cv(nombreCorto.getValue())+'","idFuncionEscritorLayOut":"'+idFuncionEsc+'","idFuncionLectorLayOut":"'+idFuncionLect+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	recargarArbolBanco();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=4&cadObj='+cadObj,true);

                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaFuncion(tFuncion)
{
	if(tFuncion==1)
    {
    	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                                {
                                                    gEx('idFuncionEscritorLayOut').setValue(nombre);
												    gEx('idFuncionEscritorLayOut').idFuncion=idConsulta;                           
                                                    gEx('vAgregarExp').close();
                                                };
    
  		mostrarVentanaExpresion(function(fila,ventana)
        						{
                                	gEx('idFuncionEscritorLayOut').setValue(fila.get('nombreConsulta'));
								    gEx('idFuncionEscritorLayOut').idFuncion=fila.get('idConsulta');
                                    ventana.close();
                                }
        						,true);  
    }
    else
    {
    	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                                {
                                                    gEx('idFuncionLectorLayOut').setValue(nombre);
												    gEx('idFuncionLectorLayOut').idFuncion=idConsulta;                           
                                                    gEx('vAgregarExp').close();
                                                };
    	mostrarVentanaExpresion(function(fila,ventana)
        						{
                                	gEx('idFuncionLectorLayOut').setValue(fila.get('nombreConsulta'));
								    gEx('idFuncionLectorLayOut').idFuncion=fila.get('idConsulta');
                                    ventana.close();
                                }
        						,true);  
    }
}

function removerFuncion(tFuncion)
{
	if(tFuncion==1)
    {
    	gEx('idFuncionEscritorLayOut').setValue('');
	    gEx('idFuncionEscritorLayOut').idFuncion=-1;
    }
    else
    {
    	gEx('idFuncionLectorLayOut').setValue('');
		gEx('idFuncionLectorLayOut').idFuncion=-1;
    }
}

function recargarArbolBanco()
{
	gEx('arbolBancos').getRootNode().reload();
	gEx('arbolBancos').expandAll();    
}

function mostrarVentanaSucursal(objSucursal)
{

	var lblSucursal='Agregar Sucursal';
    if(objSucursal)
    	lblSucursal='Modificar Sucursal ['+objSucursal.attributes.nombreSucursal+']';
	var cmbEstado=crearComboExt('cmbEstado',arrEstados,260,35,140);
    cmbEstado.on('select',function(cmb,registro)
    					{
							gEx('cmbMunicipio').reset();
                        	obtenerMunicipio(registro.get('id'),'cmbMunicipio');
                        }
    			)
    var cmbMunicipio=crearComboExt('cmbMunicipio',[],500,35,170);
     cmbMunicipio.on('select',function(cmb,registro)
    					{
                        	gEx('cmbLocalidad').reset();
                        	obtenerLocalidad(registro.get('id'),'cmbLocalidad');
                        }
    			)
    var cmbLocalidad=crearComboExt('cmbLocalidad',[],80,65,170);
    var gridTelefono=crearGridTelefono();
    var gridEmail=crearGridEMail();
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. Sucursal:'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            allowNegative:false,
                                                            allowDecimals:false,
                                                            id:'noSucursal',
                                                            width:80
                                                        },
                                                        {
                                                        	x:225,
                                                            y:10,
                                                            html:'Nombre de Sucursal: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:350,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'nombreSucursal',
                                                            width:350
                                                        },
                                                        {
                                                        	xtype:'fieldset',
                                                            title:'Domicilio',
                                                            width:700,
                                                            height:130,
                                                            x:10,
                                                            y:40,
                                                            layout:'absolute',
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Calle:'
                                                                        },
                                                                        {
                                                                        	x:80,
                                                                            y:5,
                                                                            xtype:'textfield',
                                                                            id:'calle',
                                                                            width:200
                                                                        },
                                                                        {
                                                                        	x:300,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'N&uacute;mero:'
                                                                        },
                                                                        {
                                                                        	x:350,
                                                                            y:5,
                                                                            xtype:'textfield',
                                                                            id:'numero',
                                                                            width:100
                                                                        },
                                                                         {
                                                                        	x:470,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Colonia:'
                                                                        },
                                                                        {
                                                                        	x:530,
                                                                            y:5,
                                                                            xtype:'textfield',
                                                                            id:'colonia',
                                                                            width:140
                                                                        },
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'C.P.:'
                                                                        },
                                                                        {
                                                                        	x:80,
                                                                            y:35,
                                                                            xtype:'textfield',
                                                                            id:'cp',
                                                                            width:80
                                                                        },
                                                                        {
                                                                        	x:200,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Estado:'
                                                                        },
                                                                        cmbEstado,
                                                                        {
                                                                        	x:430,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Municipio:'
                                                                        },
                                                                        cmbMunicipio,
                                                                        {
                                                                        	x:10,
                                                                            y:70,
                                                                            xtype:'label',
                                                                            html:'Localidad:'
                                                                        },
                                                                        cmbLocalidad
                                                            		]
                                                        },
                                                        {
                                                        	x:10,
                                                            y:180,
                                                            height:220,
                                                            xtype:'fieldset',
                                                            title:'Datos de contacto',
                                                            items:	[
                                                            			{
                                                                        	xtype:'tabpanel',
                                                                            id:'tPanel',
                                                                            activeTab:0,
                                                                            width:685,
                                                                            height:180,
                                                                            items:	[
                                                                            			gridTelefono,
                                                                                        gridEmail
                                                                            		] 
                                                                        }
                                                            		]
                                                            
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblSucursal,
										width: 750,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noSucursal').focus(500,false);
																}
															}
												},
										buttons:	[
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var noSucursal=gEx('noSucursal');
                                                                        var nombreSucursal=gEx('nombreSucursal');
                                                                        var calle=gEx('calle');
                                                                        var numero=gEx('numero');
                                                                        var colonia=gEx('colonia');
                                                                        var cp=gEx('cp');
                                                                        var idSucursal=-1;
                                                                        if(objSucursal)
                                                                        	idSucursal=objSucursal.attributes.idSucursal;
                                                                        
                                                                        if(nombreSucursal.getValue()=='')
                                                                        {
                                                                            function resp()
                                                                            {
                                                                            	nombreSucursal.focus();
                                                                            }
                                                                            msgBox('Debe indicar el nombre de la sucursal',resp);
                                                                            return;
                                                                      	}
                                                                        
                                                                        var x;
                                                                        var fila;
                                                                        var arrTelefonos='';
                                                                        var obj='';
                                                                        for(x=0;x<gridTelefono.getStore().getCount();x++)  
                                                                        {
                                                                        	fila=gridTelefono.getStore().getAt(x);
                                                                            if(fila.get('telefono')=='')
                                                                            {
                                                                            	function resp2()
                                                                                {
                                                                                	gEx('tPanel').setAtiveTab(0);
                                                                                	gridTelefono.startEditing(x,3);
                                                                                }
                                                                                msgBox('El n&uacute;mero de tel&eacute;fono es obligatorio',resp2);
                                                                                return;
                                                                            }
                                                                            obj='{"codigoInternacional":"'+cv(fila.get('codigoInternacional'))+'","lada":"'+cv(fila.get('lada'))+'","telefono":"'+
                                                                            	cv(fila.get('telefono'))+'","extension":"'+cv(fila.get('extension'))+'","nombreContacto":"'+cv(fila.get('nombreContacto'))+'"}';
                                                                            
                                                                          
                                                                            if(arrTelefonos=='')
                                                                            	arrTelefonos=obj;
                                                                            else
                                                                            	arrTelefonos+=','+obj;
                                                                            
                                                                        }
                                                                        var arrMail='';
                                                                        for(x=0;x<gridEmail.getStore().getCount();x++)  
                                                                        {
                                                                        	fila=gridEmail.getStore().getAt(x);
                                                                            if(fila.get('emailContacto').trim()=='')
                                                                            {
                                                                            	function resp3()
                                                                                {
                                                                                	gEx('tPanel').setAtiveTab(1);
                                                                                	gridEmail.startEditing(x,1);
                                                                                }
                                                                                msgBox('La direcci&oacute;n de E-mail es obligatoria',resp3);
                                                                                return;
                                                                            }
                                                                            if(!validarCorreo(fila.get('emailContacto').trim()))
                                                                            {
                                                                            	function resp4()
                                                                                {
                                                                                	gEx('tPanel').setAtiveTab(1);
                                                                                	gridEmail.startEditing(x,1);
                                                                                }
                                                                                msgBox('La direcci&oacute;n de E-mail no es v&aacute;lida',resp4);
                                                                                return;
                                                                            }
                                                                            obj='{"emailContacto":"'+cv(fila.get('emailContacto'))+'","titularEmail":"'+cv(fila.get('titularEmail'))+'"}';
                                                                            if(arrMail=='')
                                                                            	arrMail=obj;
                                                                            else
                                                                            	arrMail+=','+obj;
                                                                            
                                                                        }
                                                                        
                                                                        var cadObj='{"idBanco":"'+gE('idBanco').value+'","idSucursal":"'+idSucursal+'","noSucursal":"'+noSucursal.getValue()+'","nombreSucursal":"'+cv(nombreSucursal.getValue())+
                                                                        			'","calle":"'+cv(calle.getValue())+'","numero":"'+cv(numero.getValue())+'","colonia":"'+cv(colonia.getValue())+
                                                                                    '","cp":"'+cp.getValue()+'","estado":"'+cmbEstado.getValue()+'","municipio":"'+cmbMunicipio.getValue()+'","localidad":"'+cmbLocalidad.getValue()+
                                                                                    '","telefonos":['+arrTelefonos+'],"email":['+arrMail+']}';
                                                                        
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                recargarArbolBanco();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=6&cadObj='+cadObj,true);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    if(objSucursal)
    {
    	var noSucursal=gEx('noSucursal');
        noSucursal.setValue(objSucursal.attributes.noSucursal);
        var nombreSucursal=gEx('nombreSucursal');
        nombreSucursal.setValue(objSucursal.attributes.nombreSucursal);
        var calle=gEx('calle');
        calle.setValue(objSucursal.attributes.calle);
        var numero=gEx('numero');
        numero.setValue(objSucursal.attributes.numero);
        var colonia=gEx('colonia');
        colonia.setValue(objSucursal.attributes.colonia);
        var cp=gEx('cp');
        cp.setValue(objSucursal.attributes.cp);
        gridTelefono.getStore().loadData(objSucursal.attributes.arrTelefonos);
        gridEmail.getStore().loadData(objSucursal.attributes.arrMail);
        cmbEstado.setValue(objSucursal.attributes.estado);
        
        function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                var arrDatos=eval(arrResp[1]);
                cmbMunicipio.getStore().loadData(arrDatos);
                cmbMunicipio.setValue(objSucursal.attributes.municipio);
                function funcAjax2()
                {
                    var resp=peticion_http.responseText;
                    arrResp=resp.split('|');
                    if(arrResp[0]=='1')
                    {
                        var arrDatos=eval(arrResp[1]);
                        cmbLocalidad.getStore().loadData(arrDatos);
                        cmbLocalidad.setValue(objSucursal.attributes.localidad);
                            
                    }
                    else
                    {
                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                    }
                }
                obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax2, 'POST','funcion=60&accion=2&codigo='+objSucursal.attributes.municipio,true);
                    
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesOrganigrama.php',funcAjax, 'POST','funcion=60&accion=1&codigo='+objSucursal.attributes.estado,true);
        
    }
}

function crearGridTelefono()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'codigoInternacional'},
                                                                {name: 'lada'},
                                                                {name: 'telefono'},
                                                                {name: 'extension'},
                                                                {name: 'nombreContacto'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,
														{
															header:'C&oacute;d. de &aacute;rea',
															width:100,
															sortable:true,
															dataIndex:'codigoInternacional',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														},
														{
															header:'Lada',
															width:80,
															sortable:true,
															dataIndex:'lada',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														},
                                                        {
															header:'Tel&eacute;fono<span class="letraRoja">*</span>',
															width:100,
															sortable:true,
															dataIndex:'telefono',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														},
                                                        {
															header:'Extensi&oacute;n',
															width:80,
															sortable:true,
															dataIndex:'extension',
                                                            editor:{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														},
                                                        {
															header:'Nombre del contacto',
															width:250,
															sortable:true,
															dataIndex:'nombreContacto',
                                                            editor:{xtype:'textfield'}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridTelefono',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            clicksToEdit:1,
                                                           	title:'E-mail de contacto',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar tel&eacute;fono',
                                                                            handler:function()
                                                                            		{
                                                                                    	var regGrid=crearRegistro([
                                                                                                                    {name: 'codigoInternacional'},
                                                                                                                    {name: 'lada'},
                                                                                                                    {name: 'telefono'},
                                                                                                                    {name: 'extension'},
                                                                                                                    {name: 'nombreContacto'}
                                                                                                                ])
                                                                                        var r=new regGrid	(
                                                                                        						{
                                                                                                                	codigoInternacional:'',
                                                                                                                    lada:'',
                                                                                                                    telefono:'',
                                                                                                                    extension:'',
                                                                                                                    nombreContacto:''
                                                                                                                }
                                                                                        					)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover tel&eacute;fono',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos un elemento a remover')
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover los elementos seleccionados?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function crearGridEMail()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'emailContacto'},
                                                                {name: 'titularEmail'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	chkRow,
														{
															header:'E-mail<span class="letraRoja">*</span>',
															width:250,
															sortable:true,
															dataIndex:'emailContacto',
                                                            editor:{xtype:'textfield'}
														},
														
                                                        {
															header:'Nombre del contacto',
															width:250,
															sortable:true,
															dataIndex:'titularEmail',
                                                            editor:{xtype:'textfield'}
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridMail',
                                                            store:alDatos,
                                                            frame:true,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            clicksToEdit:1,
                                                           	title:'Tel&eacute;fonos de contacto',
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar tel&eacute;fono',
                                                                            handler:function()
                                                                            		{
                                                                                    	var regGrid=crearRegistro([
                                                                                                                    {name: 'emailContacto'},
													                                                                {name: 'titularEmail'}
                                                                                                                ])
                                                                                        var r=new regGrid	(
                                                                                        						{
                                                                                                                	emailContacto:'',
                                                                                                                    titularEmail:''
                                                                                                                }
                                                                                        					)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover tel&eacute;fono',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos un elemento a remover')
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover los elementos seleccionados?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function mostrarVentanaCuenta(objCuenta)
{
	var arrSiNo=[['0','No'],['1','S\xED']];
	var lblTitulo='Agregar Cuenta Bancaria';
    if(objCuenta)
    	lblTitulo='Modificar Cuenta Bancaria [Cta. '+objCuenta.attributes.noCuenta+', '+objCuenta.attributes.descripcionCuenta+']';
    var arrDias=[];
    for(x=1;x<32;x++)
    {
    	arrDias.push([x,x]);
    }
    var cmbDiaCorte=crearComboExt('cmbDiaCorte',arrDias,370,185,130);
    var cmbTipoMoneda=crearComboExt('cmbTipoMoneda',arrMonedas,130,215,150);
    cmbTipoMoneda.setValue('1');
    var cmbChequera=crearComboExt('cmbChequera',arrSiNo,420,215,150);
    cmbChequera.setValue('1');
    
	cmbChequera.on('select',function(combo,registro)
    						{
                            	var gridFirmantes=gEx('gridFirmantes');
                            	if(registro.get('id')=='1')
                                {
                                	gridFirmantes.enable();
                                }
                                else
                                {
                                	gridFirmantes.disable();
                                }
                            }
    				)
    
    var cmbTipoCuenta=crearComboExt('cmbTipoCuenta',arrTipoCuenta,130,155,200);
    var gridFirmantesChequera=crearGridFirmantesNomina();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. de Cliente:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'noCliente',
                                                            width:100
                                                        },
                                                        {
                                                        	x:270,
                                                            y:10,
                                                            html:'No. de Cuenta:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:360,
                                                            y:5,
                                                            xtype:'numberfield',
                                                            id:'noCuenta',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:150
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre del Cliente:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'nombreCliente',
                                                            width:400
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n Cuenta:<span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:65,
                                                            xtype:'textarea',
                                                            id:'descripcion',
                                                            width:400,
                                                            height:80
                                                        },
                                                        {
                                                        	x:10,
                                                            y:160,
                                                            html:'Tipo de cuenta:'
                                                        },
                                                        cmbTipoCuenta,
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            html:'Fecha de apertura:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:185,
                                                            xtype:'datefield',
                                                            id:'fechaApertura'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:190,
                                                            html:'D&iacute;a de Corte:'
                                                        },
                                                        cmbDiaCorte,
                                                        {
                                                        	x:10,
                                                            y:220,
                                                            html:'Tipo de moneda:<span class="letraRoja">*</span>'
                                                        },
                                                        cmbTipoMoneda,
                                                        {
                                                        	x:320,
                                                            y:220,
                                                            html:'Maneja chequera:<span class="letraRoja">*</span>'
                                                        },
                                                        cmbChequera,
                                                        {
                                                        	x:10,
                                                            y:250,
                                                            html:'CLABE Interbancaria:'
                                                        },
                                                        {
                                                        	x:130,
                                                            y:245,
                                                            xtype:'textfield',
                                                            id:'clabe',
                                                            width:220,
                                                            maskRe:/^[0-9]$/
                                                            
                                                        },
                                                        gridFirmantesChequera

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 750,
										height:550,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('noCliente').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var noCliente=gEx('noCliente');
                                                                        var noCuenta=gEx('noCuenta');
                                                                        var nombreCliente=gEx('nombreCliente');
                                                                        var descripcion=gEx('descripcion');
                                                                        var fechaApertura=gEx('fechaApertura');
                                                                        var clabe=gEx('clabe');
                                                                        if(noCuenta.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	noCuenta.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de cuenta',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(nombreCliente.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	nombreCliente.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el nombre del cliente titular de la cuenta',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(descripcion.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	descripcion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la descripci&oacute;n de la cuenta',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbTipoCuenta.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	cmbTipoCuenta.focus();
                                                                            }
                                                                            msgBox('Debe indicar la categor&iacute;a a la cual pertenece la cuenta',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        var clabeInt=clabe.getValue();
                                                                        clabeInt=clabeInt+' ';
                                                                        if((clabeInt.trim()!='')&&(clabeInt.trim().length!=18))
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	clabe.focus();
                                                                            }
                                                                            msgBox('La CLABE Interbancaria debe tener una longitud de 18 d&iacute;gitos',resp4);
                                                                            return;
                                                                        }
                                                                        var fApertura='';
                                                                        if(fechaApertura.getValue()!='')
                                                                        	fApertura=fechaApertura.getValue().format("Y-m-d");
                                                                        var idCuenta=-1;
                                                                        var idSucursal=-1;
                                                                        if(objCuenta)
                                                                        	idCuenta=objCuenta.attributes.idCuenta;
                                                                        else
                                                                        	idSucursal=nodoSel.attributes.idSucursal;
                                                                        
                                                                        
                                                                        
                                                                        var x;
                                                                        var cadFirmantes='';
                                                                        var obj='';
                                                                        var fila;
                                                                        if(cmbChequera.getValue()=='1')
                                                                        {
                                                                            for(x=0;x<gridFirmantesChequera.getStore().getCount();x++)
                                                                            {
                                                                                fila=gridFirmantesChequera.getStore().getAt(x);
                                                                                obj='{"idUsuario":"'+fila.get('idUsuario')+'"}';
                                                                                if(cadFirmantes=='')
                                                                                    cadFirmantes=obj;
                                                                                else
                                                                                    cadFirmantes+=','+obj;
                                                                            }
                                                                        }   
                                                                        var cadObj='{"responsablesFirma":['+cadFirmantes+'],"tipoCuenta":"'+cmbTipoCuenta.getValue()+'","idSucursal":"'+idSucursal+'","idCuenta":"'+idCuenta+'","nombreCliente":"'+cv(nombreCliente.getValue().trim())+'","noCliente":"'+noCliente.getValue()+
                                                                        			'","noCuenta":"'+noCuenta.getValue()+'","descripcionCuenta":"'+cv(descripcion.getValue().trim())+
                                                                                    '","clabeInterbancaria":"'+clabe.getValue()+'","fechaApertura":"'+fApertura+'","monedaCuenta":"'+cmbTipoMoneda.getValue()+
                                                                                    '","diaCorte":"'+cmbDiaCorte.getValue()+'","cuentaCheques":"'+cmbChequera.getValue()+'"}';
                                                                        
																		 
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                recargarArbolBanco();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=7&cadObj='+cadObj,true);
                                                                        
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    if(objCuenta)
    {
    	var noCliente=gEx('noCliente');
        noCliente.setValue(objCuenta.attributes.noCliente);
        var noCuenta=gEx('noCuenta');
        noCuenta.setValue(objCuenta.attributes.noCuenta);
        var nombreCliente=gEx('nombreCliente');
        nombreCliente.setValue(objCuenta.attributes.nombreCliente);
        var descripcion=gEx('descripcion');
        descripcion.setValue(objCuenta.attributes.descripcionCuenta);
        var fechaApertura=gEx('fechaApertura');
        fechaApertura.setValue(objCuenta.attributes.fechaApertura);
        var clabe=gEx('clabe');
        clabe.setValue(objCuenta.attributes.clabeInterbancaria);
        cmbDiaCorte.setValue(objCuenta.attributes.diaCorte);
        cmbChequera.setValue(objCuenta.attributes.cuentaCheques);
        cmbTipoMoneda.setValue(objCuenta.attributes.monedaCuenta);
        cmbTipoCuenta.setValue(objCuenta.attributes.tipoCuenta);
        gridFirmantesChequera.getStore().loadData(objCuenta.attributes.responsablesFirma);
        var gridFirmantes=gEx('gridFirmantes');
        if(objCuenta.attributes.cuentaCheques=='1')
        {
            gridFirmantes.enable();
        }
        else
        {
            gridFirmantes.disable();
        }
    }
}

function crearGridFirmantesNomina()
{
	
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'idUsuario'},
                                                                {name: 'nombreUsuario'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Persona que puede firmar en chequera',
															width:450,
															sortable:true,
															dataIndex:'nombreUsuario'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                        	id:'gridFirmantes',
                                                            title:'Especifique las personas que pueden firmar cheques de esta cuenta',
                                                            store:alDatos,
                                                            frame:true,
                                                            y:280,
                                                            x:80,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:180,
                                                            width:550,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar Responsable de Firma',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaResponsableFirma();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Responsable de Firma',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                        	msgBox('Debe seleccionar al menos un responsable a remover');
                                                                                            return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Es&aacute; seguro de querer remover a los responsables de firma seleccionados?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

var rdoPaterno;
var rdoMaterno;
var rdoNom;
var cBusquedaP=1;
var idUsuario=-1;

function mostrarVentanaResponsableFirma()
{

	cBusquedaP=1;
	
	var parametros2=	{
							funcion:'6',
							criterio:''
						};
                        
	var lector=new Ext.data.JsonReader 	(
										 	{
												root:'personas',
												totalProperty:'num',
												id:'idUsuario'
											},
											[
											 	{name:'idUsuario', mapping:'idUsuario'},
												{name:'Paterno', mapping:'Paterno'},
												{name:'Materno', mapping:'Materno'},
												{name:'Nom', mapping:'Nom'},
												{name:'Nombre', mapping:'Nombre'},
												{name:'Status', mapping:'Status'},
                                                {name:'idUsuario',mapping:'idUsuario'}
											]
										);                       
	
    var pPagina=new Ext.data.HttpProxy	(
										 	{
												url:'../Usuarios/procesarbUsuario.php',
												method:'POST'
											}
										 );
    
	var comboPapa=inicializarComboEmpleado(pPagina,lector,parametros2);

	

	var form = new Ext.form.FormPanel(	
										 	{
												baseCls: 'x-plain',
												layout:'absolute',
												defaultType: 'textfield',
												items: 	[
														 	new Ext.form.Label	(
																				 	{
																						x:25,
																						y:50,
																						text:'Empleado: '
																					}
																				)
															,
															comboPapa,
															new Ext.form.Radio	(
																					{
																						x:5,
																						y:5,
																						id:'rdoPaterno',
																						boxLabel:'Ap. Paterno',
																						checked:true,
																						value:1
																						
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:100,
																						y:5,
																						id:'rdoMaterno',
																						boxLabel:'Ap. Materno',
																						value:2
																					}
																				),
															new Ext.form.Radio	(
																					{
																						x:195,
																						y:5,
																						id:'rdoNombre',
																						boxLabel:'Nombre',
																						value:3
																					}
																				),
                                                            new Ext.form.Radio	(
																					{
																						x:290,
																						y:5,
																						id:'rdoClave',
																						boxLabel:'Cve. Empleado',
																						value:4
																					}
																				),
                                                             new Ext.form.Radio	(
																					{
																						x:395,
																						y:5,
																						id:'rdoCualquier',
																						boxLabel:'Cualquier parte del nombre / Clave    ',
																						value:5
																					}
																				)                    
															
														]
											}
										);
	
	ventana = new Ext.Window	(
									{
										title: lblAplicacion,
										width: 650,
										height:160,
										minWidth: 280,
										minHeight: 100,
										layout: 'fit',
										plain:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										modal:true,
										listeners : {
														show : {
																	buffer : 10,
																	fn : function() 
																	{
																		comboPapa.focus();
																	}
																}
													},
										buttons:	[
														{
															text: 'Aceptar',
															handler:function()
																	{
																		
																		var idEmpleado=idUsuario;
																		if(idEmpleado==-1)
																		{
																			function funcResp()
																			{

																				comboPapa.focus();
																			}
																			Ext.MessageBox.alert(lblAplicacion,'Debe seleccionar el usuario que ser&aacute; responsable de firma de cheques',funcResp)
																			return;
																		}
                                                                        var x;
                                                                        var r;
                                                                        var gridFirmantes=gEx('gridFirmantes');
                                                                       	var registroGridEntidad=crearRegistro([
                                                                                                                    {name: 'idUsuario'},
                                                                                                                    {name: 'nombreUsuario'}
                                                                                                                ])
                                                                        if(obtenerPosFila(gridFirmantes.getStore(),'idUsuario',idUsuario)==-1)
                                                                        {
                                                                        	var idUsr=idUsuario;
                                                                            r=new registroGridEntidad(
                                                                                                        {
                                                                                                            idUsuario:idUsr,
                                                                                                            nombreUsuario:comboPapa.getRawValue(),
                                                                                                            
                                                                                                        }
                                                                                                    );
                                                                            gridFirmantes.getStore().add(r);
                                                                        }
                                                                        ventana.close();
																	}
														},
														{
															text: 'Cancelar',
															handler:function()
																	{
																		ventana.close();
																	}
														}
													]
    								}
								);

	var rdoPaterno=Ext.getCmp('rdoPaterno');
	var rdoMaterno=Ext.getCmp('rdoMaterno');
	var rdoNombre=Ext.getCmp('rdoNombre');
    var rdoClave=Ext.getCmp('rdoClave');
	var rdoCualquier=Ext.getCmp('rdoCualquier');
	rdoPaterno.on('check',cambiarRadioSel);									
	rdoMaterno.on('check',cambiarRadioSel);									
	rdoNombre.on('check',cambiarRadioSel);	
    rdoClave.on('check',cambiarRadioSel);									
	rdoCualquier.on('check',cambiarRadioSel);							
    ventana.show();
}

function cambiarRadioSel(chk, valor)
{
	if(valor==true)
	{
		var rdoPaterno=Ext.getCmp('rdoPaterno');
		var rdoMaterno=Ext.getCmp('rdoMaterno');
		var rdoNom=Ext.getCmp('rdoNombre');
        var rdoClave=gEx('rdoClave');
        var rdoCualquier=gEx('rdoCualquier');
		if(rdoPaterno.id!=chk.id)
			rdoPaterno.setValue(false);
		if(rdoMaterno.id!=chk.id)
			rdoMaterno.setValue(false);
		if(rdoNom.id!=chk.id)
			rdoNom.setValue(false);
        if(rdoClave.id!=chk.id)
        	rdoClave.setValue(false);
        if(rdoCualquier.id!=chk.id)    
        	rdoCualquier.setValue(false);
		cBusquedaP.value=chk.value;
	}
}


function inicializarComboEmpleado(pagina,lector, parametros2)
{

	var ds=new Ext.data.Store	(
								 	{
										proxy:pagina,
										reader:lector,
										baseParams:parametros2
									}
								 );
	
	function cargarDatos(dSet)
	{
		idUsuario='-1';
		var aNombre=Ext.getCmp('cmbNombreEmpleado').getValue();
		dSet.baseParams.criterio=aNombre;
		dSet.baseParams.campoBusqueda=cBusquedaP;
        dSet.baseParams.listRoles="'-1000_0'";
       
	}
	
	ds.on('beforeload',cargarDatos);

	var resultTpl=new Ext.XTemplate	(
									 	'<tpl for="."><div class="search-item">',
											'{Paterno}&nbsp;{Materno}&nbsp;{Nom}&nbsp;<br>{Status}<br>---<br>',
										'</div></tpl>'
									 );
	
	var comboNombre= new Ext.form.ComboBox	(
												 	{
														x:100,
														y:45,
														id:'cmbNombreEmpleado',
														store:ds,
														displayField:'Nombre',
														typeAhead:false,
														minChars:1,
														loadingText:'Procesando, por favor espere...',
														width:350,
                                                        listWidth :350,
														pageSize:10,
														hideTrigger:true,
														tpl:resultTpl,
														itemSelector:'div.search-item',
														listWidth :300
													}
												 );

	function funcElemSeleccionado(combo,registro)
	{	
		idUsuario=registro.get('idUsuario');	
        
	
	}
	
	comboNombre.on('select',funcElemSeleccionado);	
	return comboNombre;
}

function mostrarVentanaChequera(objCheque)
{

	var lblCheque='Agregar chequera';
    if(objCheque)
    	lblCheque='Modificar chequera';
    var cmbSituacion=crearComboExt('cmbSituacion',arrSituacion,310,35,150);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'No. chequera: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:5,
                                                            id:'nChequera',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:100
                                                        },
                                                        {
                                                        	x:230,
                                                            y:10,
                                                            html:'Folio Inicial: <span class="letraRoja">*</span>'
                                                            
                                                        },
                                                        {
                                                        	x:310,
                                                            y:5,
                                                            id:'folioInicial',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:80
                                                        },
                                                        {
                                                        	x:420,
                                                            y:10,
                                                            html:'Folio Final: <span class="letraRoja">*</span>'
                                                            
                                                        },
                                                        {
                                                        	x:500,
                                                            y:5,
                                                            id:'folioFinal',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:80
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Folio Actual: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:110,
                                                            y:35,
                                                            id:'folioActual',
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            width:80
                                                        },
                                                        {
                                                        	x:230,
                                                            y:40,
                                                            html:'Situaci&oacute;n: <span class="letraRoja">*</span>'
                                                            
                                                        },
                                                        cmbSituacion,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Descripci&oacute;n:'
                                                            
                                                        },
                                                        {
                                                        	x:110,
                                                            y:65,
                                                            xtype:'textarea',
                                                            width:450,
                                                            height:70,
                                                            id:'txtDescripcion'
                                                        }
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblCheque,
										width: 650,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nChequera').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var nChequera=gEx('nChequera');
                                                                        var folioInicial=gEx('folioInicial');
                                                                        var folioFinal=gEx('folioFinal');
                                                                        var folioActual=gEx('folioActual');
                                                                        var txtDescripcion=gEx('txtDescripcion');
                                                                        
                                                                        if(nChequera.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	nChequera.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el n&uacute;mero de chequera',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(folioInicial.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	folioInicial.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el folio inicial de la chequera',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(folioFinal.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	folioFinal.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el folio final de la chequera',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(folioActual.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	folioActual.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el folio actual de la chequera',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(folioInicial.getValue()>folioFinal.getValue())
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	folioInicial.focus();
                                                                            }
                                                                            msgBox('El folio inicial NO puede ser mayor que el folio final de la chequera',resp6);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if((folioActual.getValue()>folioFinal.getValue())||(folioActual.getValue()<folioInicial.getValue()))
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	folioActual.focus();
                                                                            }
                                                                            msgBox('El folio actual se encuentra fuera del rango de folios permitidos de la chequera',resp7);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(cmbSituacion.getValue()=='')
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	cmbSituacion.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la situaci&oacute;n actual de la chequera',resp5);
                                                                            return;
                                                                        }
                                                                        var idChequera=-1;
                                                                        var idCuentaBancaria=-1;
                                                                        if(objCheque)
                                                                        {
                                                                        	
                                                                        	idChequera=objCheque.id.replace('ch','');
                                                                            idCuentaBancaria=nodoSel.parentNode.id.replace('c','');
                                                                        }
                                                                        else
                                                                        {
                                                                        	idCuentaBancaria=nodoSel.id.replace('c','');
                                                                        }   
                                                                        var cadObj='{"idCuentaBancaria":"'+idCuentaBancaria+'","nChequera":"'+nChequera.getValue()+'","folioInicial":"'+folioInicial.getValue()+'","folioFinal":"'+folioFinal.getValue()+
                                                                        			'","folioActual":"'+folioActual.getValue()+'","descripcion":"'+cv(txtDescripcion.getValue())+'","situacion":"'+cmbSituacion.getValue()+'","idChequera":"'+idChequera+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                                recargarArbolBanco();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=10&cadObj='+cadObj,true);
            
                                                                                    
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    if(objCheque)
    {

    	var nChequera=gEx('nChequera');
        nChequera.setValue(objCheque.attributes.noChequera);
        var folioInicial=gEx('folioInicial');
        folioInicial.setValue(objCheque.attributes.folioInicial);
        var folioFinal=gEx('folioFinal');
        folioFinal.setValue(objCheque.attributes.folioFinal);
        var folioActual=gEx('folioActual');
        folioActual.setValue(objCheque.attributes.folioActual);
        folioActual.setReadOnly(true);
        var txtDescripcion=gEx('txtDescripcion');
        txtDescripcion.setValue(objCheque.attributes.descripcion);
        cmbSituacion.setValue(objCheque.attributes.situacion);
    }
}