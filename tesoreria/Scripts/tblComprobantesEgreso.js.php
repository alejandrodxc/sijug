<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

function sincronizarBuzon()
{
	window.parent.parent.mostrarMensajeProcesando('Sincronizando con buz&oacute;n, por favor espere...');
	function funcAjax(peticion_http)
    {
    	window.parent.parent.ocultarMensajeProcesando();
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            switch(arrResp[1])   
            {
                case '0':
                    msgBox('No se ha podido llevar a cabo la conexi&oacute;n debido al siguiente problema: <br><br>'+arrResp[2],null,Ext.MessageBox.ERROR);
                break;
                case '1':
                    gEx('grid_tblTabla').getStore().reload();
                break;
                
            }
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWebV2('../paginasFunciones/funcionesModulosEspeciales_Neofact.php',funcAjax, 'POST','funcion=4&idEmpresa='+gE('idEmpresa').value,false);
    
}