<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idSituacion,descripcion,imagen FROM 712_situacionComprobantesFiscales";
	$arrSituacionComprobante=$con->obtenerFilasArreglo($consulta);
?>	
var arrSituacionComprobante=<?php echo $arrSituacionComprobante?>;

function formatearRecibo(val,meta,registro)
{
	switch(registro.data.situacion)
    {
    	case '2':
		return '<a href="javascript:mostrarComprobante(\''+bE(val)+'\')"><img src="../images/page_white_magnify.png" width="16" height="16" title="Ver comprobante" alt="Ver comprobante"></a>&nbsp;&nbsp;'+
        		'<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(val)+'"><img src="../images/Icono_xml.gif" width="16" height="16" title="Obtener XML del CFDI" alt="Obtener XML del CFDI"></a>&nbsp;&nbsp;'+
                '<a href="javascript:reenviarComprobante(\''+bE(val)+'\')"><img src="../images/email_go.png" width="16" height="16" title="Reenviar Comprobante por E-mail" alt="Reenviar Comprobante por E-mail"></a>'
		break;
        case '5':
		return '<a href="javascript:mostrarComprobante(\''+bE(val)+'\')"><img src="../images/page_white_magnify.png" width="16" height="16" title="Ver comprobante" alt="Ver comprobante"></a>&nbsp;&nbsp;'+
        		'<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(val)+'"><img src="../images/Icono_xml.gif" width="16" height="16" title="Obtener XML del CFDI" alt="Obtener XML del CFDI"></a>'

		break;                
    }           
}

function mostrarComprobante(iC)
{
	var obj={};
    obj.titulo='Comprobante fiscal';
    obj.ancho=900;
    obj.alto=450;
    obj.params=[['iC',iC],['cPagina','sFrm=true']];
    obj.url='../formatosFacturasElectronicas/vistaPreviaCFDI.php';
    	abrirVentanaFancy(obj);
}	

function formatearSituacionComprobante(val,meta,registro)
{
	var pos=existeValorMatriz(arrSituacionComprobante,val);
    var reg=arrSituacionComprobante[pos];
    return '<span alt="'+escaparBR(registro.data.comentarios)+'" title="'+escaparBR(registro.data.comentarios)+'"><img src="'+arrSituacionComprobante[pos][2]+'" width="14" height="14" > '+arrSituacionComprobante[pos][1]+'</span>';
}

function reenviarComprobante(val)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    msgBox('Se ha reenviado el comprobante');
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=53&iC='+bD(val),true);

        }
    }
    msgConfirm('Est&aacute; seguro de querer reenviar el comprobante?',resp);
}

function formatearPagado(val)
{
	if(val=='1')
    	return '<img src="../images/icon_big_tick.gif" width="14" width="14" />';
    else
    	return '<img src="../images/cross.png" width="14" width="14"/>';
}


function marcarComprobante(val)
{
	var grid_tblTabla=gEx('grid_tblTabla');
	var fila=grid_tblTabla.getSelectionModel().getSelected();
    if(!fila)
    {
    	msgBox('Debe seleccionar el comprobante cuya situaci&oacute;n de pago desea modificar');
    	return;
    }
    
    
    function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            fila.set('pagado',val);
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=54&iC='+fila.data.idComprobanteFactura+'&val='+val,true);

    
    
}