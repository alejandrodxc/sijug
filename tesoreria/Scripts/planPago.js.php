<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
?>


var registroDimension;
var registroPlanPago;
Ext.onReady(inicializar);

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	var x;
    	var id=gE('id').value;
        var gridPagosPlan=gEx('gridPagosPlan');
        var fila;
        var cadCategoria='';
        var obj='';
        var idConceptoPago=-1;
        for(x=0;x<gridPagosPlan.getStore().getCount();x++)
        {
        	fila=gridPagosPlan.getStore().getAt(x);
            idConceptoPago=-1;
            if(fila.get('idConceptoPago')!="")
            	idConceptoPago=fila.get('idConceptoPago');
            obj='{"etiquetaPago":"'+fila.get('etiquetaPago')+'","noPagosAgrupa":"'+fila.get('noPagosAgrupa')+'","noPago":"'+fila.get('noPago')+'"}';
            if(cadCategoria=='')
            	cadCategoria=obj;
            else
            	cadCategoria+=','+obj;
        }
       	if(cadCategoria=='')
        {
        	msgBox('Debe ingresar al menos un n&uacute;mero de pago');
        	return;
        }
        var objArr='{"arrPagos":['+cadCategoria+']}';
        if(id=='-1')
        {
        	gE('funcPHPEjecutarNuevo').value=bE('asociarPagosPlanPagos(@idRegPadre,\''+bE(objArr)+'\')');
        }
        else
        {
        	gE('funcPHPEjecutarModif').value=bE('asociarPagosPlanPagos('+id+',\''+bE(objArr)+'\')');
        }
    	
    	gE('frmEnvio').submit();
    }
}

function inicializar()
{
	
    registroPlanPago=crearRegistro([{name: 'etiquetaPago'},{name: 'idConceptoPago'},{name: 'noPagosAgrupa'},{name: 'noPago'}]);
	
    crearGridPlanPagos();
}


function crearGridPlanPagos()
{
	
	var dsDatos=eval(bD(gE('arrPagos').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'etiquetaPago'},
                                                                    {name: 'noPagosAgrupa'},
                                                                    {name: 'noPago'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Etiqueta Pago <span class="letraRoja">*</span>',
															width:200,
															sortable:true,
															dataIndex:'etiquetaPago',
                                                            editor:	{xtype:'textfield'}
														},
                                                        
                                                        {
															header:'# Pagos agrupa <span class="letraRoja">*</span>',
															width:110,
															sortable:true,
															dataIndex:'noPagosAgrupa',
                                                            editor:	{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														},
                                                        {
															header:'No. Pago <span class="letraRoja">*</span>',
															width:110,
															sortable:true,
															dataIndex:'noPago',
                                                            editor:	{xtype:'numberfield',allowDecimals:false,allowNegative:false}
														}
													]
												);
    
    
    var editorFila=new Ext.ux.grid.RowEditor	(
    												{
														id:'editor',
                                                        saveText: 'Guardar',
                                                        cancelText:'Cancelar',
                                                        clicksToEdit:2
                                                    }
                                                );
	
    editorFila.on('beforeedit',funcEditorFilaBeforeEdit)
    editorFila.on('validateedit',funcEditorValida);
    editorFila.on('canceledit',funcEditorCancelEdit);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            id:'gridPagosPlan',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            renderTo:'tblPlanesPago',
                                                            height:270,
                                                            plugins:[editorFila],
                                                            width:770,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAddPlanPago',
                                                                            icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar Pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	 var tblGrid=gEx('gridPagosPlan');
                                  
                                                                                          var editorFila=gEx('editor');
                                                                                          
                                                                                          
                                                        
                                                                                          
                                                                                          var nReg=new registroPlanPago	(
                                                                                                                          	{
                                                                                                                                etiquetaPago:'',
                                                                                                                                idConceptoPago:'',
                                                                                                                                noPagosAgrupa:'',
                                                                                                                                noPago:(tblGrid.getStore().getCount()+1)
                                                                                                                            }
                                                                                                                      	)
                                                                                          
                                                                                          editorFila.stopEditing();
                                                                                          tblGrid.getStore().add(nReg);
                                                                                          tblGrid.nuevoRegistro=true;
                                                                                          editorFila.startEditing(tblGrid.getStore().getCount()-1);	
                                                                                          Ext.getCmp('btnAddPlanPago').disable();
                                                                                          Ext.getCmp('btnDelPlanPago').disable();	
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnDelPlanPago',
                                                                            icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover Pago',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!filas)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el elemento que desea remover');
                                                                                            return;
                                                                                        }
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                            	tblGrid.getStore().remove(filas);
                                                                                            }
                                                                                           
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el elemento seleccionado?',resp);
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;
}

function funcEditorFilaBeforeEdit(rowEdit,fila)
{
	var grid=Ext.getCmp('gridPagosPlan');
    grid.copiaRegistro=grid.getStore().getAt(fila).copy();
    grid.registroEdit=grid.getStore().getAt(fila);
}

function funcEditorValida(rowEdit,obj,registro,nFila)
{
	var grid=Ext.getCmp('gridPagosPlan');
	var cm=grid.getColumnModel();
	var nColumnas=cm.getColumnCount(false);
	var x;
	var editor;
	var dataIndex;
	var valor;
	for(x=0;x<nColumnas;x++)
	{
		if(cm.getColumnHeader(x).indexOf('*')!=-1)
		{
			dataIndex=cm.getDataIndex(x);
			valor=(eval('obj.'+dataIndex));
			if(valor=='')
			{
				function funcResp()
				{
					var ctrl=gEx('editor_'+dataIndex);
					ctrl.focus();
				}
				msgBox('La columna "'+cm.getColumnHeader(x).replace('*','')+'" no puede ser vac&iacute;a',funcResp);
				return false;
			}
		}	
	}
   
   	if(Ext.getCmp('btnDelPlanPago')!=null)
        Ext.getCmp('btnDelPlanPago').enable();	
    if(Ext.getCmp('btnAddPlanPago')!=null)            
        Ext.getCmp('btnAddPlanPago').enable();
    grid.nuevoRegistro=false;
    return true;
	
}

function funcEditorCancelEdit(rowEdit,cancelado)
{
	
	
	var grid=Ext.getCmp('gridPagosPlan');
	if(grid.nuevoRegistro)
		grid.getStore().removeAt(grid.getStore().getCount()-1);
	Ext.getCmp('btnDelPlanPago').enable();
    Ext.getCmp('btnAddPlanPago').enable();
    var copiaRegistro=grid.copiaRegistro;
    
    var x=0;
    var arrCampos=grid.getStore().fields;
    var filaDestino=grid.registroEdit;

    for(x=0;x<arrCampos.items.length;x++)
    {
    	filaDestino.set(arrCampos.items[x].name,copiaRegistro.get(arrCampos.items[x].name));

    }

    
	grid.nuevoRegistro=false;
	
}


