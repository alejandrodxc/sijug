<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT('[',if(cveEmpresa is null,'',cveEmpresa),'] RFC: ',concat(rfc1,'-',rfc2,'-',rfc3),', ',razonSocial,' ',apPaterno,' ',apMaterno),
				concat('[',if(cveEmpresa is null,'',cveEmpresa),'] RFC: ',concat(rfc1,'-',rfc2,'-',rfc3),', ',razonSocial)) AS nombreEmpresa
				  FROM 6927_empresas e WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1";
	$arrEmpresas=$con->obtenerFilasArreglo($consulta);
?>

var arrPanelVisto=[];
var arrEmpresas=<?php echo $arrEmpresas?>;
Ext.onReady(inicializar);

function inicializar()
{

	var cmbEmpresas=crearComboExt('cmbEmpresas',arrEmpresas,0,0,500);
    cmbEmpresas.setValue(arrEmpresas[0][0]);
    cmbEmpresas.on('select',function(cmb,registro)
    						{
                            	gEx('frameEgreso').load	(
                                							{
                                                            	url:'../tesoreria/tblComprobantesEgreso.php',
                                                                params:	{
                                                                			idEmpresa:registro.data.id,
                                                                            cPagina:'sFrm=true'
                                                                		}
                                                            }
                                						)
                            
                            	if(gE('iframe-frameIngreso'))
                                {
                                	gEx('frameIngreso').load	(
                                                                    {
                                                                        url:'../tesoreria/tblComprobantesIngreso.php',
                                                                        params:	{
                                                                                    idEmpresa:gEx('cmbEmpresas').getValue(),
                                                                                    cPagina:'sFrm=true'
                                                                                }
                                                                    }
                                                                )
                                }
                            
                            	
                            
                            }
    				)
	new Ext.Panel	(
    					{
                        	renderTo:'tblPanel',
                            width:960,
                            height:1100,
                            layout:'anchor',
                            tbar:	[
                            			{
                                            xtype:'label',
                                            html:'&nbsp;&nbsp;<span class="letraAzulSimple" style="font-size:13px;color:#900"><b>Empresa:</b></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                                        },
                                        cmbEmpresas
                            		],
                            items:	[
                            			{
                                        	xtype:'tabpanel',
                                            x:0,
                                            y:0,
                                            activeTab:0,
                                            border:false,
                                            anchor:'100% 100%',
                                            
                                            items:	[
                                            			{
                                                        	xtype:'panel',
                                                            anchor:'100% 100%',
                                                            border:false,
                                                            id:'tblEgresos',
                                                            title:'Control de Egresos',
                                                            items:	[
                                                            			new Ext.ux.IFrameComponent({ 
                
                                                                                                            id: 'frameEgreso', 
                                                                                                            anchor:'100% 100%',
                                                                                                            border:false,
                                                                                                            url: '../paginasFunciones/white.php',
                                                                                                            style: 'width:100%;height:100%' 
                                                                                                    }),
                                                                        
                                                            		]
                                                            
                                                        },
                                                        {
                                                        	xtype:'panel',
                                                            border:false,
                                                            anchor:'100% 100%',
                                                            id:'tblIngresos',
                                                            title:'Control de Ingresos',
                                                            listeners:	{
                                                                             activate:function(p)   				
                                                                                        {
                                                                                        	if(existeValorArreglo(arrPanelVisto,p.id)==-1)
                                                                                            {
	                                                                                        	arrPanelVisto.push(p.id);
                                                                                               gEx('frameIngreso').load	(
                                                                                                                            {
                                                                                                                                url:'../tesoreria/tblComprobantesIngreso.php',
                                                                                                                                params:	{
                                                                                                                                            idEmpresa:gEx('cmbEmpresas').getValue(),
                                                                                                                                            cPagina:'sFrm=true'
                                                                                                                                        }
                                                                                                                            }
                                                                                                                        )
                                                                                         	}  
                                                                                        }
                                                                        },
                                                            items:	[
                                                            			new Ext.ux.IFrameComponent({ 
                
                                                                                                            id: 'frameIngreso', 
                                                                                                            anchor:'100% 100%',
                                                                                                            border:false,
                                                                                                            url: '../paginasFunciones/white.php',
                                                                                                            style: 'width:100%;height:100%' 
                                                                                                    }),
                                                            		]
                                                            
                                                        }
                                            		]
                                            
                                            
                                        }
                            		]
                        }	
    				)
                    
	dispararEventoSelectCombo('cmbEmpresas');
                    
}
