<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


var reg;

Ext.onReady(inicializar);

function inicializar()
{
	reg=crearRegistro	(
    						[
                                {name: 'idSerie'},
                                {name: 'serie'},
                                {name: 'folioInicial'},
                                {name: 'folioActual'},
                                {name: 'noRegistros'}
                            ]
    					)
	if(gE('tblSeries'))
		crearGridSeries();
}

function crearGridSeries()
{
	var dsDatos=eval(bD(gE('arrSeries').value));
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idSerie'},
                                                                    {name: 'serie'},
                                                                    {name: 'folioInicial'},
                                                                    {name: 'folioActual'},
                                                                    {name: 'noRegistros'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});
	chkRow.on('rowselect',function(sm,nFila,registro)
    						{
                            	if((registro.data.noRegistros=='0')||(registro.data.folioInicial==registro.data.folioActual))
                                	gEx('btnRemover').enable();
                                else
                                	gEx('btnRemover').disable();
                            }
    			)
   	chkRow.on('rowdeselect',function(sm,nFila,registro)
    						{
                            	gEx('btnRemover').disable();
                            }
    			)
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Serie',
															width:200,
															sortable:true,
															dataIndex:'serie',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Folio Inicial',
															width:110,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'folioInicial'
														},
														{
															header:'Folio Actual',
															width:110,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'folioActual'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridSeries',
                                                            store:alDatos,
                                                            frame:false,
															renderTo:'tblSeries',
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:170,
                                                            width:550,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	id:'btnAgregar',
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar serie',
                                                                            handler:function()
                                                                            		{
                                                                                    	 
                                                                                    	var r=new reg(	
                                                                                        				{
                                                                                                        	idSerie:-1,
                                                                                                            serie:'',
                                                                                                            folioInicial:1,
                                                                                                            folioActual:1,
                                                                                                            noRegistros:'0'
                                                                                                        }
                                                                                        			)
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,2);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	id:'btnRemover',
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover serie',
                                                                            disabled:true,
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();

                                                                                        if(fila)
                                                                                        {
                                                                                        	function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	function funcAjax()
                                                                                                    {
                                                                                                        var resp=peticion_http.responseText;
                                                                                                        arrResp=resp.split('|');
                                                                                                        if(arrResp[0]=='1')
                                                                                                        {
                                                                                                            tblGrid.getStore().remove(fila);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                        }
                                                                                                    }
                                                                                                    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=41&idSerie='+fila.data.idSerie,true);
                                                                                                	
                                                                                                }
                                                                                                
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover la serie seleccionada?',resp);
                                                                                        }
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
                                                    
	tblGrid.on('afteredit',function(e)
    						{
                            	if(e.value.trim()=='')
                                {
                                	if(e.record.idSerie!=-1)
                                    {
                                    	e.record.set('serie',e.originalValue);
                                        return;
                                        
                                    }
                                    
	
                               	}
                                
                                var fila;
                                var x;
                                for(x=0;x<e.grid.getStore().getCount();x++)
                                {
                                	fila=e.grid.getStore().getAt(x);
                                    if(x!=e.row)
                                    {
                                    	if(fila.data.serie.trim()==e.value.trim())
                                        {
                                        	function respErr()
                                            {
                                        		e.record.set('serie',e.originalValue);
		                                    	e.startEditing(e.row,2);
                                            }
                                            msgBox('La serie ingresada ya se encuentra registrada para este certificado',respErr)
                                            return;
                                        }
                                   	}
                                }
                                
                                var cadObj='{"idSerie":"'+e.record.data.idSerie+'","serie":"'+cv(e.value.trim())+'","folioInicial":"'+e.record.data.folioInicial+'","idCertificado":"'+gE('idCertificado').value+'"}';
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                    	e.record.set('idSerie',arrResp[1]);
                                        msgBox('La operaci&oacute;n ha sido realizada satisfactoriamente');
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=42&cadObj='+cadObj,true);
                                
                            }
    		)
    		                                                    
	return 	tblGrid;		
}

function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	var contrasena=gE('contrasena');
        var contrasena2=gE('contrasena2');
        if(contrasena.value!=contrasena2.value)
        { 
        	function resp()
            {
            	contrasena2.focus();
            }
        	msgBox('Las contrase&ntilde;as no coinciden',resp);
        	return;
        	
        }
        gE('frmEnvio').submit();
    }
}