<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	$fechaActual=date("Y-m-d");
	$consulta="SELECT idFuncionOperacion,etiqueta FROM 6023_funcionesCaja WHERE tipoFuncion=2";// and idFuncionOperacion not in (4,5,6,10);
	$arrFormasPago=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idSituacion,descripcion,imagen FROM 712_situacionComprobantesFiscales ORDER BY descripcion";
	$arrSituacionComprobantes=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT('[',if(cveEmpresa is null,'',cveEmpresa),'] ',razonSocial,' ',apPaterno,' ',apMaterno),CONCAT('[',if(cveEmpresa is null,'',cveEmpresa),'] ',razonSocial)) AS nombreEmpresa  FROM 6927_empresas where esEmpresaUsuario=1 order by rfc1,rfc2,rfc3";
	$arrEmpresas="";
	
	$resEmpresas=$con->obtenerFilas($consulta);
	while($fEmpresa=mysql_fetch_row($resEmpresas))
	{
		$arrCertificados="";
		$consulta="SELECT idCertificado,noCertificado FROM 687_certificadosSelloDigital WHERE idReferencia=".$fEmpresa[0]." AND fechaInicioVigencia<='".$fechaActual."' AND fechaFinVigencia>='".$fechaActual."' ORDER BY fechaInicioVigencia";
		$rCertificado=$con->obtenerFilas($consulta);
		while($fCertificado=mysql_fetch_row($rCertificado))
		{
			$consulta="SELECT idSerieCertificado,serie FROM 688_seriesCertificados WHERE idCertificado=".$fCertificado[0]." ORDER BY serie";
			
			$oCertificado="['".$fCertificado[0]."','".$fCertificado[1]."',".$con->obtenerFilasArreglo($consulta)."]";
			if($arrCertificados=="")
				$arrCertificados=$oCertificado;
			else
				$arrCertificados.=",".$oCertificado;
		}
		
		$o="['".$fEmpresa[0]."','".cv($fEmpresa[1])."',[".$arrCertificados."]]";	
		if($arrEmpresas=="")
			$arrEmpresas=$o;
		else
			$arrEmpresas.=",".$o;
	}
	$arrEmpresas='['.$arrEmpresas.']';
	
	
	
	
	
	$diaSemana=date("N");
	$fechaInicioSemana=date("Y-m-d",strtotime("-".($diaSemana-1)." days",strtotime(date("Y-m-d"))));
	

	
	
		
	/*$consulta="SELECT horasFacturacion,facturaIndividual,leyendaFactura FROM _1025_tablaDinamica";
	$fConfiguracion=$con->obtenerPrimeraFila($consulta);
	$fechaActual=strtotime(date("Y-m-d H:i:s"));
	$fechaLimiteFacturacion=date("Y-m-d",strtotime("-".$fConfiguracion[0]." hours",$fechaActual));*/
		
	$consulta="SELECT date_format(MIN(fechaVenta),'%Y-%m-%d') FROM 6008_ventasCaja where idFactura IS NULL";//WHERE formaPago IN (".$lFormaPago.") AND fechaVenta<='".$fechaLimiteFacturacion."' AND

	$ultimaFechaVenta=$con->obtenerValor($consulta);
	if($ultimaFechaVenta=='')
		$ultimaFechaVenta=$fechaLimiteFacturacion;
	
	$fechaLimiteFacturacion="null";	
?>
var aFormaPago=[<?php echo $aFormaPago?>];
var arrEmpresas=<?php echo $arrEmpresas?>;
var arrSiNo=[['1','S&iacute;'],['0','No']]
var arrSituacionVenta=[['1','Activa'],['2','Cancelada']];
var arrFormasPago=<?php echo $arrFormasPago?>;
var arrFormasPagoFiltro=<?php echo $arrFormasPago?>;
var arrSituacionComprobantes=<?php echo $arrSituacionComprobantes?>;

var fechaInicioSemana='<?php echo $fechaInicioSemana?>';
var fechaActual='<?php echo date("Y-m-d")?>';
var fechaLimiteFacturacion='<?php echo $fechaLimiteFacturacion?>';
Ext.onReady(inicializar);

function inicializar()
{
	var summary = new Ext.ux.grid.HybridSummary();
	var summary2 = new Ext.ux.grid.GridSummary();  
    
    arrFormasPagoFiltro.splice(0,0,['0','Cualquier forma de pago']);
    var cmbFormaPago=crearComboExt('cmbFormaPago',arrFormasPagoFiltro,0,0,250);
    cmbFormaPago.setValue('0');
    cmbFormaPago.on('select',function()
    						{
                            	cargarVentasPeriodo();
                            }
    				)
    
    
    var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idVenta'},
		                                                {name: 'fecha', type:'date', dateFormat:'Y-m-d'},
                                                        {name:'situacionVenta'},
		                                                {name:'folioVenta'},
                                                        {name:'montoTotal'},
                                                        {name:'tipoVenta'},
                                                        {name:'idComprobante'},
                                                        {name:'situacionComprobante'},
                                                        {name:'rfcReceptor'},
                                                        {name:'nombreReceptor'},
                                                        {name:'comentariosComprobante'},
                                                        {name: 'facturado'},
                                                        {name: 'concepto'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fecha', direction: 'ASC'},
                                                            groupField: 'tipoVenta',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	gEx('btnFacturar').disable();
                                        gEx('btnCancelar').disable();
                                        gEx('btnReintentar').disable();
                                    	proxy.baseParams.funcion='55';
                                        
                                    }
                        )   
              
	var filters = new Ext.ux.grid.GridFilters	(
    												{
                                                    	filters:	[ 
                                                        				{type: 'date', dataIndex: 'fecha'},
                                                                        {type: 'int', dataIndex: 'folioVenta'},
                                                                        {type: 'list', dataIndex: 'tipoVenta',phpMode:true, options:arrFormasPago},
                                                                        {type: 'list', dataIndex: 'situacionComprobante',phpMode:true, options:arrSituacionComprobantes},
                                                                        {type: 'list', dataIndex: 'situacionVenta',phpMode:true, options:arrSituacionVenta},
                                                                        {type: 'string', dataIndex: 'rfcReceptor'},
                                                                        {type: 'list', dataIndex: 'facturado',phpMode:true, options:arrSiNo},
                                                                        {type: 'string', dataIndex: 'nombreReceptor'}
                                                                        
                                                                        
                                                        			]
                                                    }
                                                );           
       
       
	var chkRow=new Ext.grid.CheckboxSelectionModel({singleSelect:true});     
    chkRow.on('rowselect',function(sm,nFila,registro)
    						{
                            	gEx('btnFacturar').disable();
                                gEx('btnCancelar').disable();
                                gEx('btnReintentar').disable();
                                
                            	if(registro.data.idComprobante=='')
                                {
                                	if(registro.data.situacionVenta=='1')
	                                	gEx('btnFacturar').enable();
                                }
                                else
                                {
                                	switch(registro.data.situacionComprobante)
                                    {
                                    	case '2'://Timbrado
                                        	if(registro.data.rfcReceptor!='XEXX010101010101')
	                                        	gEx('btnCancelar').enable();
                                        break;
                                        case '3': //Cancelado
                                        	gEx('btnFacturar').enable();
                                        break;
                                        case '5': //Errores
                                        	gEx('btnReintentar').enable();
                                        break;
                                    }
                                }
                            }
    		)  
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer({width:35}),
                                                            chkRow,
                                                            
                                                            {
                                                                header:'Folio venta',
                                                                width:130,
                                                                css:'text-align:right;',
                                                                sortable:true,
                                                                dataIndex:'folioVenta'
                                                            },
                                                            {
                                                                header:'Fecha de la venta',
                                                                width:110,
                                                                css:'text-align:right;',
                                                                sortable:true,
                                                                dataIndex:'fecha',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            {
                                                                header:'Concepto',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'concepto',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Monto total',
                                                                width:110,
                                                                sortable:true,
                                                                css:'text-align:right;',
                                                                dataIndex:'montoTotal',
                                                                renderer:'usMoney',
                                                                summaryType:'sum',
                                                            },
                                                            {
                                                                header:'Situaci&oacute;n venta',
                                                                width:100,
                                                                css:'text-align:right;',
                                                                sortable:true,
                                                                dataIndex:'situacionVenta',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSituacionVenta,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Forma de pago',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'tipoVenta',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrFormasPago,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Facturado',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'facturado',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrSiNo,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'idComprobante',
                                                                renderer:formatearRecibo
                                                            },
                                                            {
                                                                header:'RFC cliente',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'rfcReceptor'
                                                            },
                                                            {
                                                                header:'Cliente',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'nombreReceptor'
                                                            },
                                                           {
                                                                header:'Situaci&oacute;n factura',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'situacionComprobante',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	if(val=='')
                                                                            	return;
                                                                        	var pos=existeValorMatriz(arrSituacionComprobantes,val,0,true);
                                                                            if(registro.data.idComprobante!='5')
                                                                            	registro.data.comentariosComprobante='';
                                                                            return '<span title="'+registro.data.comentariosComprobante+'" alt="'+registro.data.comentariosComprobante+'"><img width="13" width="height" src="'+arrSituacionComprobantes[pos][2]+'" > '+arrSituacionComprobantes[pos][1]+'</span>';
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
    var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gVentas',
                                                                store:alDatos,
                                                                frame:false,
                                                                width:960,
                                                                height:450,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                sm:chkRow,
                                                                columnLines : true,      
                                                                tbar:	[
                                                                            {
                                                                                icon:'../images/Icono_txt.gif',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Facturar venta',
                                                                                disabled:true,
                                                                                hidden:true,
                                                                                id:'btnFacturar',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la venta cuya factura desea generar');
                                                                                            	return;
                                                                                            }
                                                                                            mostrarVentanaFacturacion(fila.data.idVenta);
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/page_remove.png',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnCancelar',
                                                                                text:'Cancelar factura',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la venta cuya factura desea cancelar');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                         	mostrarVentaCancelarCFDI  (fila); 
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/arrow_refresh.PNG',
                                                                                cls:'x-btn-text-icon',
                                                                                disabled:true,
                                                                                id:'btnReintentar',
                                                                                hidden:true,
                                                                                text:'Reintentar timbrado',
                                                                                handler:function()
                                                                                        {
                                                                                        	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                            if(!fila)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar la venta cuya factura desea intentar el timbrado');
                                                                                            	return;
                                                                                            }
                                                                                            
                                                                                            function funcAjax()
                                                                                            {
                                                                                                var resp=peticion_http.responseText;
                                                                                                arrResp=resp.split('|');
                                                                                                if(arrResp[0]=='1')
                                                                                                {
                                                                                                    gEx('gVentas').getStore().reload();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                }
                                                                                            }
                                                                                            obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=56&idComprobante='+fila.data.idComprobante,true);
                                                                                            
                                                                                         	
                                                                                        }
                                                                                
                                                                            },
                                                                            '-',
                                                                            {
                                                                                icon:'../images/user_go.png',
                                                                                cls:'x-btn-text-icon',
                                                                                hidden:true,
                                                                                text:'Facturar a p&uacute;blico general',
                                                                                id:'btnFacturarPG',
                                                                                handler:function()
                                                                                        {
                                                                                        	mostrarVentanaFacturacionPublicoGeneral();
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ],  
                                                                plugins:	[filters,summary,summary2],                                                        
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:true,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
      
      
	new Ext.Panel	(
    					{
                        	 renderTo:'tListadoVentas',
                             width:960,
                             height:460,
                             tbar:	[
                             			{
                                        	xtype:'label',
                                            html:'<span style="color:#000"><b>Ventas del&nbsp;&nbsp;&nbsp;</b></span>'
                                        },
                                        {
                                        	xtype:'datefield',
                                            id:'dteDel',
                                            value:fechaInicioSemana,
                                            listeners:	{
                                            				select:function()
                                                            	{
                                                                	cargarVentasPeriodo();
                                                                }
                                            			}
                                        },
                                        {
                                        	xtype:'label',
                                            html:'<span style="color:#000"><b>&nbsp;&nbsp;&nbsp;al&nbsp;&nbsp;&nbsp;</b></span>'
                                        },
                                        {
                                        	xtype:'datefield',
                                            id:'dteAl',
                                            value:fechaActual,
                                            listeners:	{
                                            				select:function()
                                                            	{
                                                                	cargarVentasPeriodo();
                                                                }
                                            			}
                                        },
                                        '-',
                                        {
                                        	xtype:'label',
                                            html:'<span style="color:#000"><b>Mostrar ventas pagadas mediante:&nbsp;&nbsp;&nbsp;</b></span>'
                                        },
                                        cmbFormaPago
                                        
                             		],
                             items:	[
                             			tblGrid
                             		]
                        }
    				)      
      	

	cargarVentasPeriodo();
    asignarEvento('iImprimir','load',imprimirIFrame);
}

function mostrarVentanaFacturacionPublicoGeneral()
{
	

	var cmbEmpresa=crearComboExt('cmbEmpresa',arrEmpresas,120,5,400);
    cmbEmpresa.on('select',function(cmb,registro)
    						{
                            	
                            	cmbCertificado.getStore().loadData(registro.data.valorComp);
                                if(registro.data.valorComp.length==1)
                                {
                                	cmbCertificado.setValue(registro.data.valorComp[0][0]);
                                    cmbCertificado.disable();
                                    dispararEventoSelectCombo('cmbCertificado');
                               	}
                            }
    			)
   
	
    
    var cmbCertificado=crearComboExt('cmbCertificado',[],185,35,250);
    cmbCertificado.on('select',function(cmb,registro)
    							{
                                    cmbSerie.getStore().loadData(registro.data.valorComp);
                                    if(registro.data.valorComp.length==1)
                                    {
                                        cmbSerie.setValue(registro.data.valorComp[0][0]);
                                        cmbSerie.disable();
                                        
                                    }
                                }
                     )
    			
    
    
    var cmbSerie=crearComboExt('cmbSerie',[],185,65,250);
	
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:5,
                                                            y:0,
                                                            layout:'absolute',
                                                            xtype:'fieldset',
                                                            title:'Datos del emisor',
                                                            height:165,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Empresa emisora:'
                                                                        },
                                                                        cmbEmpresa,
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Certificado de Sello Digital (CSD):'
                                                                        },
                                                                        cmbCertificado,
                                                                        {
                                                                        	x:10,
                                                                            y:70,
                                                                            xtype:'label',
                                                                            html:'Serie:'
                                                                        },
                                                                        cmbSerie,
                                                                        {
                                                                        	x:10,
                                                                            y:100,
                                                                            xtype:'label',
                                                                            html:'Facturar del:'
                                                                        },
                                                                        {
                                                                        	x:120,
                                                                            y:95,
                                                                            xtype:'datefield',
                                                                            id:'fechaInicio',
                                                                            value:'<?php echo $ultimaFechaVenta?>'
                                                                           
                                                                        },
                                                                        {
                                                                        	x:240,
                                                                            y:100,
                                                                            xtype:'label',
                                                                            html:'al:'
                                                                        },
                                                                        {
                                                                        	x:270,
                                                                            y:95,
                                                                            xtype:'datefield',
                                                                            id:'fechaFin',
                                                                            value:'<?php echo $fechaLimiteFacturacion?>'
                                                                        }
                                                                        
                                                            
                                                            		]
                                                        },
                                                        {
                                                        	x:5,
                                                            y:180,
                                                            layout:'absolute',
                                                            xtype:'fieldset',
                                                            title:'Especifique las formas de pago que ser&aacute;n consideradas para facturaci&oacute;n',
                                                            height:120,
                                                            items:	[
                                                            			{
                                                                        	id:'chk_1',
                                                                            xtype:'checkbox',
                                                                            boxLabel:'Efectivo',
                                                                            x:10,
                                                                            checked:(existeValorArreglo(aFormaPago,'1')!=-1),
                                                                            y:10
                                                                        },
                                                                        {
                                                                        	id:'chk_2',
                                                                            xtype:'checkbox',
                                                                            boxLabel:'Tarjeta de crédito/débito',
                                                                            x:190,
                                                                            checked:(existeValorArreglo(aFormaPago,'2')!=-1),
                                                                            y:10
                                                                        },
                                                                        
                                                                        {
                                                                        	id:'chk_3',
                                                                            xtype:'checkbox',
                                                                            boxLabel:'Venta a crédito',
                                                                            x:370,
                                                                            checked:(existeValorArreglo(aFormaPago,'3')!=-1),
                                                                            y:10
                                                                        },
                                                                        
                                                                        
                                                                        {
                                                                        	id:'chk_9',
                                                                            xtype:'checkbox',
                                                                            checked:(existeValorArreglo(aFormaPago,'9')!=-1),
                                                                            boxLabel:'Combinado (Efectivo-Tarjeta)',
                                                                            x:10,
                                                                            y:40
                                                                        },  
                                                                        {
                                                                        	id:'chk_7',
                                                                            xtype:'checkbox',
                                                                            checked:(existeValorArreglo(aFormaPago,'7')!=-1),
                                                                            boxLabel:'Transferencia bancaria',
                                                                            x:190,
                                                                            y:40
                                                                        }

                                                                        
                                                            
                                                            		]
                                                        }

													]
										}
									);
	
    alto=390;
	var ventanaAM = new Ext.Window(
									{
										title: 'Facturar Venta',
										width: 700,
										height:alto,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		if(cmbEmpresa.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbEmpresa.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar la empresa emisora de la factura',resp);
                                                                           return;	
                                                                        }
                                                                        
                                                                        var fechaInicio=gEx('fechaInicio');
                                                                        var fechaFin=gEx('fechaFin');
                                                                        
                                                                        
                                                                        if(fechaInicio.getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	fechaInicio.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de inicio del periodo a facturar',resp1);
                                                                            return;
                                                                            
                                                                        }
                                                                        
                                                                        if(fechaFin.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	fechaFin.focus();
                                                                            }
                                                                            msgBox('Debe ingresar la fecha de fin del periodo a facturar',resp2);
                                                                            return;
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                        if(fechaInicio.getValue()>fechaFin.getValue())
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	fechaInicio.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio el periodo no puede ser mayor que la fecha de fin del periodo a facturar',resp2);
                                                                            return;
                                                                       	}
                                                                        
                                                                       var listaFormasPago='';
                                                                       if(gEx('chk_1').getValue())
                                                                       {
                                                                       		if(listaFormasPago=='')
                                                                            	listaFormasPago='1';
                                                                            else
                                                                            	listaFormasPago+=',1';
                                                                       }
                                                                       
                                                                       if(gEx('chk_2').getValue())
                                                                       {
                                                                       		if(listaFormasPago=='')
                                                                            	listaFormasPago='2';
                                                                            else
                                                                            	listaFormasPago+=',2';
                                                                       }
                                                                       
                                                                       if(gEx('chk_3').getValue())
                                                                       {
                                                                       		if(listaFormasPago=='')
                                                                            	listaFormasPago='3';
                                                                            else
                                                                            	listaFormasPago+=',3';
                                                                       }
                                                                       
                                                                       if(gEx('chk_9').getValue())
                                                                       {
                                                                       		if(listaFormasPago=='')
                                                                            	listaFormasPago='9';
                                                                            else
                                                                            	listaFormasPago+=',9';
                                                                       }
                                                                       
                                                                       if(gEx('chk_7').getValue())
                                                                       {
                                                                       		if(listaFormasPago=='')
                                                                            	listaFormasPago='7';
                                                                            else
                                                                            	listaFormasPago+=',7';
                                                                       }
                                                                       
                                                                       
                                                                       function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gVentas').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=63&fInicio='+fechaInicio.getValue().format('Y-m-d')+'&fFin='+fechaFin.getValue().format('Y-m-d')+'&idCertificado='+cmbCertificado.getValue()+'&idSerie='+cmbSerie.getValue()+'&idEmpresaEmisor='+gEx('cmbEmpresa').getValue()+'&listaFormasPago='+listaFormasPago,true);
                                                                        
                                                                       
                                                                       
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    if(arrEmpresas.length==1)
    {
    	cmbEmpresa.setValue(arrEmpresas[0][0]);
        cmbEmpresa.disable();
        dispararEventoSelectCombo('cmbEmpresa');
        
    }	
    
    
}

function cargarVentasPeriodo()
{
	gEx('gVentas').getStore().load(
    								{
                                    	url:'../paginasFunciones/funcionesTesoreria.php',
                                        params:	{
                                        			funcion:55,
                                                    periodoDel:gEx('dteDel').getValue().format('Y-m-d'),
                                                    periodoAl:gEx('dteAl').getValue().format('Y-m-d'),
                                                    formaPago:gEx('cmbFormaPago').getValue()
                                                    
                                                    
                                        		}
                                    }
    							);
}

function mostrarVentanaFacturacion(idVenta)
{
	
	var oConf=	{
    					idCombo:'cmbRFC',
                        anchoCombo:220,
                        campoDesplegar:'rfc',
                        campoID:'idEmpresa',
                        funcionBusqueda:43,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:140,
                        posY:5,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idEmpresaFacturacion=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRFC').getValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        gEx('cmbRazonSocial').setValue(''); 
                                        gE('lblDomicilio').innerHTML='';
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idEmpresaFacturacion=registro.get('idEmpresa');
                                       	gEx('cmbRazonSocial').setRawValue(registro.data.razonSocial); 
                                        gE('lblDomicilio').innerHTML=registro.data.domicilioFiscal;
                                    }  
    				};

    
	var cmbRFC=crearComboExtAutocompletar(oConf);
    
    
    var oConf=	{
    					idCombo:'cmbRazonSocial',
                        anchoCombo:400,
                        campoDesplegar:'razonSocial',
                        campoID:'idEmpresa',
                        funcionBusqueda:43,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:140,
                        posY:35,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idEmpresaFacturacion=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRazonSocial').getValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        gEx('cmbRFC').setValue(''); 
                                        gE('lblDomicilio').innerHTML='';
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idEmpresaFacturacion=registro.get('idEmpresa');
                                       	gEx('cmbRFC').setRawValue(registro.data.rfc); 
                                        gE('lblDomicilio').innerHTML=registro.data.domicilioFiscal;
                                    }  
    				};

    
	var cmbRazonSocial=crearComboExtAutocompletar(oConf);


	var cmbEmpresa=crearComboExt('cmbEmpresa',arrEmpresas,120,5,400);
    cmbEmpresa.on('select',function(cmb,registro)
    						{
                            	
                            }
    			)
   
	if(arrEmpresas.length==1)
    {
    	cmbEmpresa.setValue(arrEmpresas[0][0]);
        cmbEmpresa.disable();
    }

	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:5,
                                                            y:0,
                                                            layout:'absolute',
                                                            xtype:'fieldset',
                                                            title:'Datos del emisor',
                                                            height:80,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Empresa emisora:'
                                                                        },
                                                                        cmbEmpresa
                                                                        
                                                            
                                                            		]
                                                        },
                                                        {
                                                        	x:5,
                                                            y:90,
                                                            layout:'absolute',
                                                            xtype:'fieldset',
                                                            title:'Datos del cliente',
                                                            height:150,
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'RFC del Cliente:'
                                                                        },
                                                                        cmbRFC,
                                                                        {
                                                                        	x:370,
                                                                            y:7,
                                                                            xtype:'label',
                                                                            html:'<a href="javascript:registrarCliente()"><img src="../images/add.png"></a>'
                                                                        },
                                                            			{
                                                                        	x:10,
                                                                            y:40,
                                                                            xtype:'label',
                                                                            html:'Nombre/Raz&oacute;n Social:'
                                                                        },
                                                                        cmbRazonSocial,
                                                            			{
                                                                        	x:10,
                                                                            y:70,
                                                                            xtype:'label',
                                                                            html:'Domicilio Fiscal:'
                                                                        },
                                                                        {
                                                                        	x:130,
                                                                            y:70,
                                                                            xtype:'label',
                                                                            html:'<span id="lblDomicilio"></span>'
                                                                        }

                                                                        
                                                            
                                                            		]
                                                        }

													]
										}
									);
	
    alto=330;
	var ventanaAM = new Ext.Window(
									{
										title: 'Facturar Venta',
										width: 700,
										height:alto,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('cmbRFC').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
                                                                        var obj={};
                                                                        obj.titulo='Facturar Venta (Vista previa)';
                                                                        obj.ancho=950;
                                                                        obj.alto=460;
                                                                        obj.url="../tesoreria/vistaPreviaFacturacionCaja.php";
                                                                        obj.params=[['vistaListadoVentas','1'],['cPagina','sFrm=true'],['idCliente',idEmpresaFacturacion],['idVenta',idVenta]];
                                                                        abrirVentanaFancy(obj);
                                                                        ventanaAM.close();
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function registrarCliente()
{

	var accionCancelar=bE('{msgConfirm(\'Est&aacute; seguro de querer cancelar la operaci&oacute;n?\',function(btn){if(btn==\'yes\'){window.parent.cerrarVentanaFancy();}})}');
	var obj={};
    obj.titulo='Alta de cliente';
    obj.ancho=920;
    obj.alto=460;
    obj.url="../modeloAlmacenes/proveedores.php";
    obj.params=[['cPagina','sFrm=true'],['eJs',bE('window.parent.asignarProveedor(@idRegistro);')],['accionCancelar',accionCancelar],['idEmpresa',-1],['esCliente',1],['referenciaFiltros','<?php echo $referenciaFiltros?>']];
    abrirVentanaFancy(obj);
}

function asignarProveedor(iRegistro)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            idEmpresaFacturacion=iRegistro;
            gEx('cmbRFC').setRawValue(arrResp[1]); 
            gEx('cmbRazonSocial').setRawValue(arrResp[2]); 
            gE('lblDomicilio').innerHTML=arrResp[3];
            cerrarVentanaFancy();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=45&idEmpresa='+iRegistro,true);
}

function formatearRecibo(val,meta,registro)
{
	switch(registro.data.situacionComprobante)
    {
    	case '2':
		return '<a href="javascript:mostrarComprobante(\''+bE(registro.data.idVenta)+'\')"><img src="../images/page_white_magnify.png" width="16" height="16" title="Ver comprobante" alt="Ver comprobante"></a>&nbsp;&nbsp;'+
		        '<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(val)+'"><img src="../images/Icono_xml.gif" width="16" height="16" title="Obtener XML del CFDI" alt="Obtener XML del CFDI"></a>&nbsp;&nbsp;'+
                '<a href="javascript:reenviarComprobante(\''+bE(val)+'\')"><img src="../images/email_go.png" width="16" height="16" title="Reenviar Comprobante por E-mail" alt="Reenviar Comprobante por E-mail"></a>'
		break;
        case '5':
		return '<a href="javascript:mostrarComprobante(\''+bE(registro.data.idVenta)+'\')"><img src="../images/page_white_magnify.png" width="16" height="16" title="Ver comprobante" alt="Ver comprobante"></a>&nbsp;&nbsp;'+
        		'<a href="../paginasFunciones/obtenerXMLComprobante.php?iC='+bE(val)+'"><img src="../images/Icono_xml.gif" width="16" height="16" title="Obtener XML del CFDI" alt="Obtener XML del CFDI"></a>'

		break;                
    }           
}

function mostrarComprobante(iV)
{
	var arrParam=[['idVenta',bD(iV)],['empresaEmisora',-1],['idCertificado',-1],['idSerie',-1]];
    enviarFormularioDatosV('../tesoreria/imprimirComprobanteCFDI.php',arrParam,'POST','iImprimir');	
}	

function mostrarVentaCancelarCFDI(fila)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                        	xtype:'label',
                                                            html:'Especifique el motivo de la cancelaci&oacute;n:'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            xtype:'textarea',
                                                            width:480,
                                                            height:100,
                                                            id:'txtMotivo'
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Cancelar CFDI',
										width: 530,
										height:250,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtMotivo').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	if(gEx('txtMotivo').getValue()=='')
                                                                        {
                                                                        
                                                                        	function resp1()
                                                                            {
                                                                            	gEx('txtMotivo').focus();
                                                                            }
                                                                            msgBox('Debe especificar el motivo de la cancelaci&oacute;n',resp1);
                                                                            return;
                                                                        }
																		function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                    	gEx('gVentas').getStore().reload();
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=48&motivo='+cv(gEx('txtMotivo').getValue())+'&iC='+fila.data.idComprobante,true);
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer cancelar el CFDI?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	

	
	   
	
}

function regresar1Pagina()
{
	gEx('gVentas').getStore().reload();
}

function reenviarComprobante(val)
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                    msgBox('Se ha reenviado el comprobante');
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=53&iC='+bD(val),true);

        }
    }
    msgConfirm('Est&aacute; seguro de querer reenviar el comprobante?',resp);
}

function descargarComprobantePDF(iC)
{
	var arrParam=[['iC',(iC)],['getPDF',true]];
	enviarFormularioDatos('../formatosFacturasElectronicas/cfdi_1.php',arrParam);
}

function imprimirIFrame()
{
    var iImprimir=gE('iImprimir');
    if(iImprimir.getAttribute('esPrimerVez')=='0')
        gE('iImprimir').contentWindow.print();
    else
        iImprimir.setAttribute('esPrimerVez','0')
}
