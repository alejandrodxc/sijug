<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Convenios</b></span>',
                                               	tbar:	[
                                                			{
                                                                  icon:'../images/add.png',
                                                                  cls:'x-btn-text-icon',
                                                                  text:'Registrar convenio',
                                                                  handler:function()
                                                                          {
                                                                              registrarConvenio();
                                                                          }
                                                                  
                                                              } 
                                                		],
                                                items:	[
                                                           	
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function registrarConvenio()
{
	var gridAdeudos=crearGridAdeudos();
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'fieldset',
                                                            width:900,
                                                            height:80,
                                                            title:'Datos del alumno'
                                                        },
                                                        gridAdeudos

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Registro de convenio',
										width: 950,
										height:480,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	


}


function crearGridAdeudos()
{
	
}
