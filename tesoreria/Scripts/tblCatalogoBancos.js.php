<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>


function agregarBanco(objDatos)
{
	var lblTitulo='Agregar Banco';
    if(objDatos)
    	lblTitulo='Modificar datos de Banco ['+objDatos.nombreBanco+']';
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Nombre del Banco: <span class="letraRoja">*</span>'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:5,
                                                            xtype:'textfield',
                                                            id:'nombreBanco',
                                                            width:400
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Nombre corto:'
                                                        },
                                                        {
                                                        	x:140,
                                                            y:35,
                                                            xtype:'textfield',
                                                            id:'nombreCorto',
                                                            width:200
                                                        },
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Funci&oacute;n generadora de Layout de transferencia:'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:65,
                                                            xtype:'textfield',
                                                            readOnly:true,
                                                            id:'idFuncionEscritorLayOut',
                                                            width:230
                                                        },
                                                         {
                                                        	xtype:'label',
                                                            y:65,
                                                            x:510,
                                                            html:'<a href="javascript:mostrarVentanaFuncion(1)"><img src="../images/pencil.png" title="Asignar funci&oacute;n" alt="Asignar funci&oacute;n"></a>&nbsp;&nbsp;<a href="javascript:removerFuncion(1)"><img src="../images/delete.png" title="Remover funci&oacute;n" alt="Remover funci&oacute;n"></a>'
                                                        },
                                                         {
                                                        	x:10,
                                                            y:100,
                                                            html:'Funci&oacute;n lectora de Layout de importacion:'
                                                        },
                                                        {
                                                        	x:270,
                                                            y:95,
                                                            xtype:'textfield',
                                                            readOnly:true,
                                                            id:'idFuncionLectorLayOut',
                                                            width:230
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            y:95,
                                                            x:510,
                                                            html:'<a href="javascript:mostrarVentanaFuncion(2)"><img src="../images/pencil.png" title="Asignar funci&oacute;n" alt="Asignar funci&oacute;n"></a>&nbsp;&nbsp;<a href="javascript:removerFuncion(2)"><img src="../images/delete.png" title="Remover funci&oacute;n" alt="Remover funci&oacute;n"></a>'
                                                        }				
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: lblTitulo,
										width: 600,
										height:230,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('nombreBanco').focus(500,false);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var nombreBanco=gEx('nombreBanco');
                                                                        var nombreCorto=gEx('nombreCorto');
                                                                        var idFuncionEscritorLayOut=gEx('idFuncionEscritorLayOut');
                                                                        var idFuncionLectorLayOut=gEx('idFuncionLectorLayOut');
                                                                        if(nombreBanco.getValue().trim()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	nombreBanco.focus();
                                                                            }
                                                                            msgBox('El nombre del banco es obligatorio',resp);
                                                                            return;
                                                                        }
                                                                        var idFuncionEsc=-1;
                                                                        if(idFuncionEscritorLayOut.getValue()!="")
                                                                        	idFuncionEsc=idFuncionEscritorLayOut.idFuncion;
                                                                        var idFuncionLect=-1;
                                                                        if(idFuncionLectorLayOut.getValue()!="")
                                                                        	idFuncionLect=idFuncionLectorLayOut.idFuncion;
                                                                        
                                                                        var cadObj='{"idBanco":"-1","nombreBanco":"'+cv(nombreBanco.getValue())+'","nombreCorto":"'+cv(nombreCorto.getValue())+'","idFuncionEscritorLayOut":"'+idFuncionEsc+'","idFuncionLectorLayOut":"'+idFuncionLect+'"}';
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	var arrParam=[['idBanco',arrResp[1]]];
                                                                                enviarFormularioDatos('../tesoreria/admonBancos.php',arrParam);
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=4&cadObj='+cadObj,true);

                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaFuncion(tFuncion)
{
	if(tFuncion==1)
    {
    	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                                {
                                                    gEx('idFuncionEscritorLayOut').setValue(nombre);
												    gEx('idFuncionEscritorLayOut').idFuncion=idConsulta;                           
                                                    gEx('vAgregarExp').close();
                                                };
    
  		mostrarVentanaExpresion(function(fila,ventana)
        						{
                                	gEx('idFuncionEscritorLayOut').setValue(fila.get('nombreConsulta'));
								    gEx('idFuncionEscritorLayOut').idFuncion=fila.get('idConsulta');
                                    ventana.close();
                                }
        						,true);  
    }
    else
    {
    	asignarFuncionNuevoConceptoInyeccion=function(idConsulta,nombre)
                                                {
                                                    gEx('idFuncionLectorLayOut').setValue(nombre);
												    gEx('idFuncionLectorLayOut').idFuncion=idConsulta;                           
                                                    gEx('vAgregarExp').close();
                                                };
    	mostrarVentanaExpresion(function(fila,ventana)
        						{
                                	gEx('idFuncionLectorLayOut').setValue(fila.get('nombreConsulta'));
								    gEx('idFuncionLectorLayOut').idFuncion=fila.get('idConsulta');
                                    ventana.close();
                                }
        						,true);  
    }
}

function removerFuncion(tFuncion)
{
	if(tFuncion==1)
    {
    	gEx('idFuncionEscritorLayOut').setValue('');
	    gEx('idFuncionEscritorLayOut').idFuncion=-1;
    }
    else
    {
    	gEx('idFuncionLectorLayOut').setValue('');
		gEx('idFuncionLectorLayOut').idFuncion=-1;
    }
}