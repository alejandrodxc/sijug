<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var tipoBusqueda=1;
Ext.onReady(inicializar);


function inicializar()
{
	 var oConf=	{
                    idCombo:'cmbProfesor',
                    posX:0,
                    posY:0,
                    raiz:'personas',
                    nRegistros:'num',
                    renderTo:'cmbNombreProfesor',
                    anchoCombo:400,
                    campoDesplegar:'Nombre',
                    campoID:'idUsuario',
                    campoHDestino:'idUsuario',
                    funcionBusqueda:2,
                    paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                    confVista:'<tpl for="."><div class="search-item">[{noEmpleado}] {Paterno} {Materno} {Nom}</div></tpl>',
                    campos:	[
                                {name:'idUsuario'},
                                {name:'Nombre'},
                                {name: 'Paterno'},
                                {name: 'Materno'},
                                {name: 'Nom'},
                                {name: 'noEmpleado'},
                                {name: 'situacionEmpleado'}
                            ],
                    funcAntesCarga:function(dSet,combo)
                                {
                                    
                                    gE('idUsuario').value='-1';
                                    var aValor=combo.getRawValue();
                                    dSet.baseParams.criterio=aValor;
                                    dSet.baseParams.campoBusqueda=tipoBusqueda;
                                    dSet.baseParams.idNomina=gE('idNomina').value;
                                     
                                    
                                    
                                    oE('tblEmpleado');
                                    oE('btnAceptar');
                                    mE('btnCancelar');
                                    
                                },
                    funcElementoSel:function(combo,registro)
                                {
                                	gE('imgEmpleado').src="../Usuarios/verFoto.php?Id="+registro.get('idUsuario');
                                    gE('lblEmpleado').innerHTML=registro.get('Paterno')+' '+registro.get('Materno')+' '+registro.get('Nom');
                                    gE('lblNoEmpleado').innerHTML=registro.get('idUsuario');
                                    
                                    var lblSituacion='<span style="color:#030">En espera de pago</span>';
                                    
                                    if(registro.get('situacionEmpleado')=='1')
                                    {
                                    	lblSituacion='<span style="color:#F00">Pagado</span>';
                                        oE('btnRegpago');
                                        mE('btnReimprime');
                                        mE('btnAceptar');
                                        oE('btnCancelar');
                                    }
                                    else
                                    {
                                    	mE('btnRegpago');
                                        oE('btnReimprime');
                                        oE('btnAceptar');
                                        mE('btnCancelar');
                                    }
                                    gE('lblSituacion').innerHTML=lblSituacion;
                                    mE('tblEmpleado');
                                    gE('idUsuario').value=registro.get('idUsuario');
                                   
                                }  
                };
	crearComboExtAutocompletar(oConf);  
}

function reimprimirTalon()
{

    var arrParam=[['idNomina',gE('idNomina').value],['idUsuario',gE('idUsuario').value],['codigoUnidad','<?php echo $_SESSION["codigoInstitucion"] ?>']];
    enviarFormularioDatos('../reportes/generarTalonPago.php',arrParam);
}

function cancelar()
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	oE('tblEmpleado');
            gEx('cmbProfesor').setRawValue('');
            gEx('cmbProfesor').focus('');
            gE('idUsuario').value='';
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer cancelar la operaci&oacute;n?',resp);
}

function aceptar()
{
    oE('tblEmpleado');
    gEx('cmbProfesor').setRawValue('');
    gEx('cmbProfesor').focus('');
    gE('idUsuario').value='';
}

function generarPago()
{
	function resp(btn)
    {
    	if(btn=='yes')
        {
        	function funcAjax()
            {
                var resp=peticion_http.responseText;
                arrResp=resp.split('|');
                if(arrResp[0]=='1')
                {
                	var arrParam=[['idNomina',gE('idNomina').value],['idUsuario',gE('idUsuario').value],['codigoUnidad','<?php echo $_SESSION["codigoInstitucion"] ?>']];
				    enviarFormularioDatos('../reportes/generarTalonPago.php',arrParam);
                    mE('btnAceptar');
                    oE('btnCancelar');
                    var lblSituacion='<span style="color:#F00">Pagado</span>';
                    gE('lblSituacion').innerHTML=lblSituacion;
                    oE('btnRegpago');
                  	mE('btnReimprime');
                    
                }
                else
                {
                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                }
            }
            obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=3&idUsuario='+gE('idUsuario').value+'&idNomina='+gE('idNomina').value,true);
            
            
        }
    }
    msgConfirm('Est&aacute; seguro de querer generar el pago del empleado?',resp);
}

function criterioChange(rdo)
{
	tipoBusqueda=rdo.value;	
}

