<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT id__742_tablaDinamica,agendarDiaInhabil,accionDiaNoHabil FROM _742_tablaDinamica";
	$fConfiguracion=$con->obtenerPrimeraFila($consulta);
	$agendarDiaInhabil=$fConfiguracion[1];
	$accionDiaNoHabil=$fConfiguracion[2];
	$consulta="SELECT dia FROM _742_diasValidosFechaPago WHERE idReferencia=".$fConfiguracion[0];
	$listDias=$con->obtenerListaValores($consulta);
	
	$fechasNoHabiles="";
	if($fConfiguracion[1]==0)
	{
		$consulta="SELECT fechaInicio,fechaFin FROM 4525_fechaCalendarioDiaHabil WHERE afectaPago=1 ORDER BY fechaInicio";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			$obj="[Date.parseDate('".$fila[0]."','Y-m-d'),Date.parseDate('".$fila[1]."','Y-m-d')]";
			if($fechasNoHabiles=="")
				$fechasNoHabiles=$obj;
			else
				$fechasNoHabiles.=",".$obj;
		}
	}
?>
var arrDiasPermitidos=[<?php echo $listDias?>];
var agendarDiaInhabil=<?php echo $agendarDiaInhabil?>;
var accionDiaNoHabil=<?php echo $accionDiaNoHabil?>;
var fechasNoHabiles=[<?php echo $fechasNoHabiles?>];


function mostrarVentanaFechasIncripcion()
{
	var grid=gEx('gridGrados');
	var filas=grid.getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox('Debe seleccionar al menos un grado para la asignaci&oacute;n de las fechas de inscripci&oacute;n');
        return;
    }
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:10,
                                                            html:'Fecha de inicio:'
                                                        },
                                                        {
                                                        	id:'dteFechaInicio',
                                                        	xtype:'datefield',
                                                            x:120,
                                                            y:5
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:255,
                                                            y:10,
                                                            html:'Fecha de t&eacute;rmino:'
                                                        },
                                                        {
                                                        	id:'dteFechaTermino',
                                                        	xtype:'datefield',
                                                            x:360,
                                                            y:5
                                                        }	

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Asignaci&oacute;n de fechas de inscripci&oacute;n',
										width: 520,
										height:120,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var dteFechaInicio=gEx('dteFechaInicio');
                                                                        var dteFechaTermino=gEx('dteFechaTermino');
                                                                        if(dteFechaInicio.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaInicio.focus();
                                                                            }
                                                                            msgBox('Debe indicar la fecha de inicio de la inscripci&oacute;n',resp);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaTermino.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteFechaTermino.focus();
                                                                            }
                                                                            msgBox('Debe indicar la fecha de t&eacute;rmino de la inscripci&oacute;n',resp2);
                                                                        	return;
                                                                        }
                                                                        
                                                                        if(dteFechaInicio.getValue()>dteFechaTermino.getValue())
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	dteFechaInicio.focus();
                                                                            }
                                                                            msgBox('La fecha de inicio de la inscripci&oacute;n no puede ser mayor que la fecha de t&eacute;rmino',resp3);
                                                                        	return;
                                                                        }
                                                                        var arrValores='';
                                                                        
                                                                        var x;
                                                                        var f;
                                                                        var arrValores='';
                                                                        var obj;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	f=filas[x];
                                                                        	obj='{"idConcepto":"1","tipoConcepto":"2","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
                                                                            	'","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'","valor":"'+dteFechaInicio.getValue().format('Y-m-d')+'"}';
                                                                            obj+=',{"idConcepto":"2","tipoConcepto":"2","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
                                                                            	'","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'","valor":"'+dteFechaTermino.getValue().format('Y-m-d')+'"}';
                                                                        	
                                                                            if(arrValores=='')
                                                                            	arrValores=obj;
                                                                            else
                                                                            	arrValores+=','+obj;
                                                                        }
                                                                        
                                                                        var cadObj='{"plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+'","arrValores":['+arrValores+']}';
                                                                        function respFinal(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                        		guardarArregloValores(cadObj,ventanaAM);
                                                                            }
                                                                            
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer modificar las fechas de inscripci&oacute;n?',respFinal);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
}

function guardarArregloValores(cadObj,ventana)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	gEx('gridGrados').getStore().reload();
            ventana.close();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=13&cadObj='+cadObj,true);
}

function mostrarVentanaCosto(c,o)
{
	var cadObj='['+bD(o)+']';
    var objConf=eval(cadObj)[0];
    var arrConcepto=bD(c).split('_');
	var grid=gEx('gridGrados');
	var filas=grid.getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox(objConf.msgNoFilas);
        return;
    }
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:objConf.etCosto
                                                        },
                                                        {
                                                        	x:235,
                                                            y:5,
                                                            id:'txtCosto',
                                                            xtype:'numberfield',
                                                            allowdecimals:true,
                                                            allowNegative:false
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: objConf.tituloVentana,
										width: 450,
										height:120,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtCosto').focus(true,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
                                                                    	var txtCosto=gEx('txtCosto');
                                                                        
                                                                        if(txtCosto.getRawValue()=='')
                                                                        {
                                                                            function resp1()
                                                                            {
                                                                                txtCosto.focus();
                                                                            }
                                                                            msgBox('El monto ingresado no es v&aacute;lido',resp1);
                                                                            return;
                                                                        }
                                                                    	var x;
                                                                        var f;
                                                                        var arrValores='';
                                                                        var obj;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	f=filas[x];
                                                                        	obj='{"idConcepto":"'+arrConcepto[2]+'","tipoConcepto":"'+arrConcepto[1]+'","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
                                                                            	'","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'","valor":"'+txtCosto.getValue()+'"}';
                                                                            if(arrValores=='')
                                                                            	arrValores=obj;
                                                                            else
                                                                            	arrValores+=','+obj;
                                                                        }
                                                                        
                                                                        var cadObj='{"plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+'","arrValores":['+arrValores+']}';
                                                                    
																		function respFinal(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                        		guardarArregloValores(cadObj,ventanaAM);
                                                                            }
                                                                            
                                                                        }
                                                                        msgConfirm(objConf.msgConfirm,respFinal);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
    
    
}

function visualizarPlanesPago(val,meta,registro,nFila,nCol)
{
	if((val!='')&&(parseFloat(val)>0))
    {
    	return "<a href='javascript:mostrarVentanaPlanPago(\""+bE(nFila)+"\",\""+bE(nCol)+"\")'>"+Ext.util.Format.usMoney(val)+"</a>";
    }
    return '$0.00';
}

function mostrarVentanaPlanPago(nFila,nCol)
{

	var gridFechas=crearGridFechasGradosReplica();
	var gridTabuladorPlanesPago=crearGridTabuladorPlanPago(bD(nFila),bD(nCol));
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'tabpanel',
                                                            activeTab:0,
                                                            baseCls: 'x-plain',
                                                            items:	[
                                                            			gridTabuladorPlanesPago,
                                                                        {
                                                                        	xtype:'panel',
                                                                            layout:'absolute',
                                                                            height:420,
                                                                            baseCls: 'x-plain',
                                                                            title:'Replicar esquema de fechas',
                                                                            items:	[	
                                                                            			{
                                                                                        	
                                                                                        	xtype:'label',
                                                                                            x:10,
                                                                                            y:10,
                                                                                            html:'Espeficique los elementos en los cuales desea replicar el esquema de fechas:&nbsp;&nbsp;(<span class="letraRoja">*</span><span style="color:#000">Las fechas asignadas previamente a los elementos seleccionados ser&aacute;n sobreescritas</span>)'
                                                                                        },
                                                                                        gridFechas
                                                                                        
                                                                                        
                                                                            		]
                                                                        }
                                                                        
                                                            		]
                                                        }
														

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Fechas de Planes de pago',
										width: 900,
										height:520,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'left',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															icon:'../images/guardar.PNG',
                                                           	cls:'x-btn-text-icon',
                                                            height:40,
                                                            width:120,
															text: 'Registrar fechas',
															handler: function()
																	{
                                                                    	function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	var cadPlanPagos='';	
                                                                                var x;
                                                                                var fila;
                                                                                var objFechas;
                                                                                var cadArrFechas='';
                                                                                var fechaMaxima='';
                                                                                for(x=0;x<gridTabuladorPlanesPago.getStore().getCount();x++)
                                                                                {
                                                                                	fila=gridTabuladorPlanesPago.getStore().getAt(x);
                                                                                    if(fila.get('fechaMaximaDescuento')!='')
                                                                                    {
	                                                                                    if(typeof(fila.get('fechaMaximaDescuento'))!='string')
                                                                                        	fechaMaxima=fila.get('fechaMaximaDescuento').format('Y-m-d');
                                                                                        else
                                                                                        	fechaMaxima=fila.get('fechaMaximaDescuento');
                                                                                        objFechas='{"noPago":"'+fila.get('noPago')+'","idPlanPago":"'+fila.get('idPlanPago')+'","fecha":"'+fechaMaxima+'"}';
                                                                                        if(cadArrFechas=='')
                                                                                            cadArrFechas=objFechas;
                                                                                        else
                                                                                            cadArrFechas+=','+objFechas;
                                                                                 	}   
                                                                                }
                                                                                var gridGrados=gEx('gridGrados');
                                                                                var nCampo=gridGrados.getColumnModel().config[parseInt(bD(nCol))].dataIndex;
                                        										var arrConcepto=nCampo.split('_');
										                                    	var f=gridGrados.getStore().getAt(parseInt(bD(nFila)));
                                                                                
                                                                                var arrElementos='{"idConcepto":"'+arrConcepto[2]+'","tipoConcepto":"'+arrConcepto[1]+'","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
		                                                                                            '","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'"}';
                                                                                
                                                                                
                                                                                var arrFilasClon=gridFechas.getSelectionModel().getSelections();
                                                                                for(x=0;x<arrFilasClon.length;x++)
                                                                                {
                                                                                	f=arrFilasClon[x];
                                                                                	arrElementos+=',{"idConcepto":"'+arrConcepto[2]+'","tipoConcepto":"'+arrConcepto[1]+'","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
		                                                                                            '","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'"}';
                                                                                }
                                                                                
                                                                                var cadObj='{"plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+
                                                                                            '","arrFechas":['+cadArrFechas+'],"arrElementos":['+arrElementos+']}';
                                                                                
                                                                                function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        ventanaAM.close();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=16&cadObj='+cadObj,true);
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer guardar las fechas indicadas',resp);
                                                                    }
                                                        },
                                                        {
															text: 'Cerrar',
                                                            height:40,
                                                            width:120,
                                                            icon:'../images/salir.gif',
                                                           	cls:'x-btn-text-icon',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
    
    var gridGrados=gEx('gridGrados');
    var x;
    var fila;
    var arrGrados=new Array();
    var numFila=parseInt(bD(nFila));
    for(x=0;x<gridGrados.getStore().getCount();x++)
    {
    	if(x!=numFila)
        {
            fila=gridGrados.getStore().getAt(x);
            arrGrados.push([fila.get('idElemento'),fila.get('tipoElemento'),fila.get('idInstanciaPlan'),fila.get('planEstudios'),fila.get('descripcion')]);
		}
    }
    gridFechas.getStore().loadData(arrGrados);
}


function crearGridTabuladorPlanPago(nFila,nCol)
{

	var gridGrados=gEx('gridGrados');
    var f=gridGrados.getStore().getAt(parseInt(nFila));
    var nCampo=gridGrados.getColumnModel().config[nCol].dataIndex;
    var arrConcepto=nCampo.split('_');
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'noPago'},
		                                                {name: 'idPlanPago'},
		                                                {name: 'nombrePlanPago'},
		                                                {name: 'etiquetaPago'},
                                                        {name: 'pagoIndividual'},
                                                        {name: 'montoDescuento1'},
                                                        {name: 'pagoDescuento1'},
                                                        {name: 'fechaMaximaDescuento'},
                                                        {name: 'montoDescuento2'},
                                                        {name: 'pagoDescuento2'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	var chkRow=new Ext.grid.CheckboxSelectionModel(); 
    var summary = new Ext.ux.grid.GroupSummary();                                                                                  
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombrePlanPago', direction: 'ASC'},
                                                            groupField: 'nombrePlanPago',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                    	var cadObj='{"plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+
                                        			'","idConcepto":"'+arrConcepto[2]+'","tipoConcepto":"'+arrConcepto[1]+'","idElemento":"'+f.get('idElemento')+'","tipoElemento":"'+f.get('tipoElemento')+
                                                    '","idInstanciaPlanEstudio":"'+f.get('idInstanciaPlan')+'"}';
                                    	proxy.baseParams.funcion='14';
                                        proxy.baseParams.cadObj=cadObj;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            chkRow,
                                                            {
                                                                header:'No. Pago',
                                                                width:65,
                                                                align:'center',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                dataIndex:'noPago'
                                                                
                                                            },
                                                            {
                                                                header:'Plan de pagos',
                                                                width:250,
                                                                align:'left',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:left !important;',
                                                                dataIndex:'nombrePlanPago'
                                                            },
                                                            {
                                                                header:'Pago',
                                                                width:250,
                                                                align:'left',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:left !important;',
                                                                dataIndex:'etiquetaPago',
                                                                summaryRenderer:function()
                                                                				{
                                                                                	return '<span class="letraRojaSubrayada8"><b>Total a pagar:</b></span>'
                                                                                }
                                                            },
                                                            {
                                                                header:'Pago Neto',
                                                                width:90,
                                                                dataIndex:'pagoIndividual',
                                                                align:'left',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:'usMoney',
                                                                summaryType:'sum'
                                                            },
                                                            {
                                                                header:'Descuento por plan',
                                                                width:110,
                                                                dataIndex:'montoDescuento1',
                                                                align:'center',
                                                                sortable:false,
                                                                hidden:true,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:'usMoney',
                                                                summaryType:'sum'
                                                            },
                                                            {
                                                                header:'Pago con descuento<br>por Plan de pago',
                                                                width:110,
                                                                dataIndex:'pagoDescuento1',
                                                                align:'center',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:'usMoney',
                                                                summaryType:'sum'
                                                            },
                                                            {
                                                                header:'Fecha m&aacute;xima para descuento<br> por Pronto Pago <a href="javascript:mostrarVentanaAsignarFechaPlan()"><img src="../images/pencil.png" width="13" height="13"></a>',
                                                                width:160,
                                                                dataIndex:'fechaMaximaDescuento',
                                                                align:'center',
                                                                sortable:false,
                                                                editor:{xtype:'datefield'},
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:formatearSoloFecha
                                                            }
                                                            ,
                                                            {
                                                                header:'Descuento por <br>Pronto Pago',
                                                                width:110,
                                                                dataIndex:'montoDescuento2',
                                                                align:'center',
                                                                sortable:false,
                                                                hidden:true,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:'usMoney',
                                                                summaryType:'sum'
                                                            },
                                                            {
                                                                header:'Pago con descuento<br>por Pronto Pago',
                                                                width:110,
                                                                dataIndex:'pagoDescuento2',
                                                                align:'center',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:'usMoney',
                                                                summaryType:'sum'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridTabuladorPlanPago',
                                                                store:alDatos,
                                                                title:'Planes de pago',
                                                                height:390,
                                                                frame:true,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[summary],
                                                                sm:chkRow,
                                                                tbar :	[
                                                                			{
                                                                                icon:'../images/table_row_insert.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Importar Esquema de Fechas',
                                                                                handler:function()
                                                                                        {
                                                                                            mostrarVentanaImportarEsquemaFechas(arrConcepto[2],arrConcepto[1]);
                                                                                        }
                                                                                
                                                                            }
                                                                		],
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}

function mostrarVentanaAsignarFechaPlan()
{
	var arrCriterio=[['1','Primer d\xEDa de cada mes'],['2','\xDAltimo d\xEDa de cada mes'],['3','Intervalo de tiempo'],['4','Mismo d\xEDa de cada mes']];
	var cmbCriterio=crearComboExt('cmbCriterio',arrCriterio,250,5,250);
    cmbCriterio.on('select',function(cmb,registro)
    								{
                                    	var lblIndique=gEx('lblIndique');
                                        var txtIntervalo=gEx('txtIntervalo');
                                        var cmbPeriodoTiempo=gEx('cmbPeriodoTiempo');
                                    	lblIndique.hide();
                                        txtIntervalo.hide();
                                        cmbPeriodoTiempo.hide();
                                        txtIntervalo.setValue('');
                                        if(registro.get('id')=='3')
                                        {
                                        	lblIndique.show();
                                            txtIntervalo.show();
                                            cmbPeriodoTiempo.show();
                                            txtIntervalo.focus(false,500);
                                        }
                                    }
    					)
    
    
    var arrPeriodo=[['1','Dias'],['2','Meses'],['3','A\xF1os']];
    var cmbPeriodoTiempo=crearComboExt('cmbPeriodoTiempo',arrPeriodo,310,35,150);
    cmbPeriodoTiempo.setValue('1');
    cmbPeriodoTiempo.hide();
   
	var gridTabuladorPlanPago=gEx('gridTabuladorPlanPago');
	var filas=gridTabuladorPlanPago.getSelectionModel().getSelections();
    if(filas.length==0)
    {
    	msgBox('Debe seleccionar al menos un elemento como objetivo de asignaci&oacute;n de fecha');
    	return;
    }
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Fecha m&aacute;xima para descuento:'
                                                        },
                                                        {
                                                        	x:190,
                                                            y:5,
                                                            xtype:'datefield',
                                                            id:'dteFechaDescuento'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            id:'chkCalcular',
                                                            xtype:'checkbox',
                                                            boxLabel:'<span class="letraExt"><b>Deseo asignar la fecha seleccionada y adem&aacute;s considerarla como fecha base para el resto de los pagos</b></span>',
                                                            listeners:	{
                                                            				check:function(chk,valor)
                                                                            		{
                                                                                    	if(valor)
	                                                                                    	gEx('fCriterio').enable();
                                                                                        else
                                                                                        	gEx('fCriterio').disable();
                                                                                    }
                                                            			}
                                                        },
                                                        {
                                                        	x:10,
                                                            y:80,
                                                            xtype:'fieldset',
                                                            layout:'absolute',
                                                            title:'Criterio de c&aacute;lculo de siguientes pagos',
                                                            width:650,
                                                            height:150,
                                                            id:'fCriterio',
                                                            disabled:true,
                                                            items:	[	
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            xtype:'label',
                                                                            html:'Elija un criterio de generaci&oacute;n de fechas:'
                                                                        },
                                                                        cmbCriterio,
                                                                        {
                                                                        	x:10,
                                                                            y:40,
                                                                            id:'lblIndique',
                                                                            xtype:'label',
                                                                            hidden:true,
                                                                            html:'Indique el intervalo de tiempo de entre pagos:'
                                                                        },
                                                                        {
                                                                        	xtype:'numberfield',
                                                                            id:'txtIntervalo',
                                                                            width:50,
                                                                            hidden:true,
                                                                            x:250,
                                                                            y:35
                                                                        },
                                                                        cmbPeriodoTiempo,
                                                                        {
                                                                        	xtype:'label',
                                                                            x:35,
                                                                            y:70,
                                                                            html:'<span class="letraRoja">*</span> <span style="color: #000">Las fechas de los pagos se calcular&aacute;n a partir del primer elemento seleccionado de cada plan de pago</span>'
                                                                        },
                                                                        {
                                                                        	xtype:'label',
                                                                            x:35,
                                                                            y:90,
                                                                            html:'<span class="letraRoja">**</span> <span style="color: #000">Si la fecha del pago excede al &uacute;ltimoltimo d&iacute;a del mes, se asignar&aacute; como fecha de pago el ultimo d&iacute;a del mismo</span>'
                                                                        }
                                                            		]
                                                            
                                                        }
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Asignaci&oacute;n de fechas para descuesto por Pronto Pago',
										width: 700,
										height:320,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var x;
                                                                        var dteFechaDescuento=gEx('dteFechaDescuento');
                                                                        if(dteFechaDescuento.getValue()=='')	
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	dteFechaDescuento.focus();
                                                                            }
                                                                            msgBox('Debe indicar la fecha m&aacute;xima para descuento',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        var fCriterio=gEx('fCriterio');
                                                                        var chkCalcular=gEx('chkCalcular');
                                                                        var cmbCriterio=gEx('cmbCriterio');
                                                                        var cmbPeriodoTiempo=gEx('cmbPeriodoTiempo');
                                                                        var txtIntervalo=gEx('txtIntervalo');
                                                                        if(chkCalcular.getValue())
                                                                        {
                                                                        	if(cmbCriterio.getValue()=='')
                                                                            {
                                                                            	function resp2()
                                                                                {
                                                                                	cmbCriterio.focus();
                                                                                }
                                                                            	msgBox('Debe indicar el criterio de generaci&oacute;n de fechas',resp2);
                                                                                return;
                                                                            }
                                                                            if(cmbCriterio.getValue()=='3')
                                                                            {
                                                                            	if(txtIntervalo.getValue()=='')
                                                                                {
                                                                                	function resp3()
                                                                                    {
                                                                                    	txtIntervalo.focus();
                                                                                    }
                                                                                    msgBox('Debe indicar el intervalo de tiempo a aplicar entre pagos',resp3);
                                                                                    return;
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        var ultimoIDPlan=-1;
                                                                        var ultimaFecha;
                                                                        var fecha;
                                                                        var txtFecha;
                                                                        var dia;
                                                                        var mes;
                                                                        var anio;
                                                                        var nFecha;
                                                                        var cadFechaAux;
                                                                        var nFechaAux;
                                                                        var intervalo;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	if(chkCalcular.getValue())
                                                                            {
                                                                                if(ultimoIDPlan!=filas[x].get('idPlanPago'))
                                                                                {
                                                                                	ultimoIDPlan=filas[x].get('idPlanPago');
                                                                                	ultimaFecha=dteFechaDescuento.getValue();
                                                                                	filas[x].set('fechaMaximaDescuento',dteFechaDescuento.getValue());
                                                                                    
                                                                                }
                                                                                else
                                                                                {
                                                                                	fecha=ultimaFecha;
                                                                                    dia=parseInt(fecha.format('d'));
                                                                                    mes=parseInt(fecha.format('m'));
                                                                                    anio=parseInt(fecha.format('Y'));
                                                                                    switch(cmbCriterio.getValue())
                                                                                    {
                                                                                    	case '1'://Primer dia de cada mes
                                                                                        	mes++;
                                                                                            if(mes>12)
                                                                                            {
                                                                                            	mes=1;
                                                                                                anio++;
                                                                                            }
                                                                                            
                                                                                            nFecha=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                            fecha=Date.parseDate(nFecha,'Y-m-d');
                                                                                        break;
                                                                                        case '2'://Ultimo dia de cada mes
                                                                                        	mes++;
                                                                                            if(mes>12)
                                                                                            {
                                                                                            	mes=1;
                                                                                                anio++;
                                                                                            }
                                                                                            
                                                                                            nFecha=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                            fecha=Date.parseDate(nFecha,'Y-m-d');
                                                                                            fecha=fecha.getLastDateOfMonth();
                                                                                        break;
                                                                                        case '3'://Intervalo de tiempo
                                                                                        	
                                                                                            switch(cmbPeriodoTiempo.getValue())
                                                                                            {
                                                                                            	case '1':
                                                                                                	intervalo=	Date.DAY;
                                                                                                break;
                                                                                                case '2':
                                                                                                	intervalo=	Date.MONTH;
                                                                                                break;
                                                                                                case '3':
                                                                                                	intervalo=	Date.YEAR;
                                                                                                break;
                                                                                            }
                                                                                            
                                                                                            fecha=fecha.add(intervalo,txtIntervalo.getValue());
                                                                                            
                                                                                            
                                                                                        break;
                                                                                        case '4':  //Mismo dia de cada mes
                                                                                        	mes++;
                                                                                            if(mes>12)
                                                                                            {
                                                                                            	mes=1;
                                                                                                anio++;
                                                                                            }
                                                                                            cadFechaAux=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-01';
                                                                                            fecha=Date.parseDate(cadFechaAux,'Y-m-d');
                                                                                            fecha=fecha.getLastDateOfMonth();
                                                                                            cadFechaAux=anio+'-'+rellenarCadena(mes,2,'0',-1)+'-'+rellenarCadena(dia,2,'0',-1);
                                                                                            nFechaAux=Date.parseDate(cadFechaAux,'Y-m-d');
                                                                                            
                                                                                            if(fecha.format('m')==nFechaAux.format('m'))
                                                                                            {
                                                                                            	fecha=nFechaAux;
                                                                                            }
                                                                                        break;
                                                                                    }
                                                                                    fecha=obtenerDiaHabil(fecha,accionDiaNoHabil);
                                                                                	filas[x].set('fechaMaximaDescuento',fecha);
                                                                                    ultimaFecha=fecha;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                            	filas[x].set('fechaMaximaDescuento',dteFechaDescuento.getValue());
                                                                            }
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridFechasGradosReplica()
{
	var lector= new Ext.data.ArrayReader({
                                            
                                           
                                            fields: [
                                               			{name:'idElemento'},
                                                        {name:'tipoElemento'},
		                                                {name: 'idInstanciaPlan'},
                                                        {name: 'planEstudios'},
		                                                {name:'descripcion'}
                                            		]
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesPlanteles.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'planEstudios', direction: 'ASC'},
                                                            groupField: 'planEstudios',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:false
                                                            
                                                        }) 
	
		                     
    var chkRow=new Ext.grid.CheckboxSelectionModel();   
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        chkRow,
                                                        {
                                                            header:'Plan Estudios',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'planEstudios'
                                                        },
                                                        {
                                                            header:'',
                                                            width:350,
                                                            sortable:true,
                                                            dataIndex:'descripcion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	switch(registro.data.tipoElemento)
                                                                        {
                                                                        	case '1':
                                                                            	return '<img src="../images/table_row_insert.png"  width="13" height="13">&nbsp;<span style="color:#030"><b>'+val+'</b></span></span>';
                                                                            break;
                                                                            case '2':
                                                                            	return '&nbsp;&nbsp;&nbsp;&nbsp;<img src="../images/text_lowercase.png" width="13" height="13">&nbsp;<span style="color:#003"><b>'+val+'</b></span></span>';
                                                                            break;
                                                                        }
                                                                    }
                                                        }
                                                        
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'griFechasdGradoReplica',
                                                            store:alDatos,
                                                            x:10,
                                                            y:40,
                                                            frame:true,
                                                            border:true,
                                                            cm: cModelo,
                                                            height:360,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,
                                                            sm:chkRow,
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: true,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
	return tblGrid;                                                    
}

function mostrarVentanaImportarEsquemaFechas(iC,tC)
{
	var cmbPlantel2=crearComboExt('cmbPlantel2',arrPlanteles,130,5,300);
    cmbPlantel2.setValue(plantel);
    cmbPlantel2.on('select',function (cmb,registro)
    						{
                            	cmbProgramaEducativo2.getStore().removeAll();
                            	cmbProgramaEducativo2.setValue('');
                                cmbPeriodo2.getStore().removeAll();
                                cmbPeriodo2.setValue('');
                                cmbPlanEstudio2.getStore().removeAll();
                                cmbPlanEstudio2.setValue('');
                                gEx('cmbGrados').setValue('');
                                gEx('gridEsquemaFechasImportar').getStore().removeAll();
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                     	gEx('cmbProgramaEducativo2').getStore().loadData(eval(arrResp[1]));   
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=76&plantel='+registro.get('id'),true);
                                
                                
                            }
    				)

	var arrProgramaEducativo=[];
    var cmbProgramaEducativo=gEx('cmbProgramaEducativo');
    var x;
    var fila;
    for(x=0;x<cmbProgramaEducativo.getStore().getCount();x++)
    {
    	fila=cmbProgramaEducativo.getStore().getAt(x);
        arrProgramaEducativo.push([fila.get('id'),fila.get('nombre')]);
    }
    
	var cmbProgramaEducativo2=crearComboExt('cmbProgramaEducativo2',arrProgramaEducativo,130,35,350);
    cmbProgramaEducativo2.setValue(cmbProgramaEducativo.getValue());
    cmbProgramaEducativo2.on('select',function(cmb,registro)
    									{
                                        	cmbPeriodo2.getStore().removeAll();
                                        	cmbPeriodo2.setValue('');
                                            cmbPlanEstudio2.getStore().removeAll();
                                            cmbPlanEstudio2.setValue('');
                                            gEx('cmbGrados').setValue('');
                                            gEx('gridEsquemaFechasImportar').getStore().removeAll();
                                            function funcAjax()
                                            {
                                                var resp=peticion_http.responseText;
                                                arrResp=resp.split('|');
                                                if(arrResp[0]=='1')
                                                {
                                                 	
                                                    var arrDatos=eval(arrResp[1]);
                                                    var idPeriodo=cmbPeriodo2.getValue();
                                                    cmbPeriodo2.getStore().loadData(arrDatos);
                                                    cmbPeriodo2.reset();
                                                    if(existeValorMatriz(arrDatos,idPeriodo)!=-1)
                                                    {
                                                    	cmbPeriodo2.setValue(idPeriodo);
                                                    }
                                                    gEx('cmbPlanEstudio2').getStore().loadData(eval(arrResp[2]));

                                                }
                                                else
                                                {
                                                    msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                }
                                            }
                                            obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=73&plantel='+gEx('cmbPlantel2').getValue()+'&idPrograma='+registro.get('id'),true);
                                        }
    						)
    var arrCiclo=[];
    var cmbCiclo=gEx('cmbCiclo');
    for(x=0;x<cmbCiclo.getStore().getCount();x++)
    {
    	fila=cmbCiclo.getStore().getAt(x);
        arrCiclo.push([fila.get('id'),fila.get('nombre')]);
    }
    
   	var cmbCiclo2=crearComboExt('cmbCiclo2',arrCiclo,130,65,120);
    cmbCiclo2.setValue(cmbCiclo.getValue());
    cmbCiclo2.on('select',function(cmb,registro)
    						{
                            	cargarEsquemasFechas();	
                            }
    			)
    var cmbPeriodo=gEx('cmbPeriodo');
    var arrPeriodo=[];
    for(x=0;x<cmbPeriodo.getStore().getCount();x++)
    {
    	fila=cmbPeriodo.getStore().getAt(x);
        arrPeriodo.push([fila.get('id'),fila.get('nombre')]);
    }
    var cmbPeriodo2=crearComboExt('cmbPeriodo2',arrPeriodo,350,65,320);
    cmbPeriodo2.setValue(cmbPeriodo.getValue());
    cmbPeriodo2.on('select',function(cmb,registro)
    						{
                            	cargarEsquemasFechas();	
                            }
    			)
    var arrPlanEstudio=[];
    var cmbPlanEstudio=gEx('cmbPlanEstudio');
    for(x=0;x<cmbPlanEstudio.getStore().getCount();x++)
    {
    	fila=cmbPlanEstudio.getStore().getAt(x);
        if(fila.get('id')!=cmbPlanEstudio.getValue())
	        arrPlanEstudio.push([fila.get('id'),fila.get('nombre')]);
    }
    var cmbPlanEstudio2=crearComboExt('cmbPlanEstudio2',arrPlanEstudio,130,95,450);
   	cmbPlanEstudio2.on('select',function(cmb,registro)
    							{
                                	cargarEsquemasFechas();
                                    gEx('cmbGrados').setValue('');
                                    gEx('gridEsquemaFechasImportar').getStore().removeAll();
                                    function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                         	gEx('cmbGrados').getStore().loadData(eval(arrResp[1]));   
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesPlanteles.php',funcAjax, 'POST','funcion=77&idInstancia='+registro.get('id'),true);
                                    
                                }
    					)
	var cmbGrados=crearComboExt('cmbGrados',[],130,125,450);
	cmbGrados.on('select',function(cmb,registro)
    							{
                                	cargarEsquemasFechas();
                                }
    					)
	var gridEsquemasF=crearGridEsquemaFechas(iC,tC);    
    
    var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            xtype:'fieldset',
                                                            width:700,
                                                            height:190,
                                                            layout:'absolute',
                                                            defaultType: 'label',
                                                            title:'Datos del plan de estudios origen',
                                                            items:	[
                                                            			{
                                                                        	x:10,
                                                                            y:10,
                                                                            html:'Plantel:'
                                                                        },
                                                                        cmbPlantel2,
                                                            			{
                                                                        	x:10,
                                                                            y:40,
                                                                            html:'Programa Educativo:'
                                                                        },
                                                                        cmbProgramaEducativo2,
                                                                        {
                                                                        	x:10,
                                                                            y:70,
                                                                            html:'Ciclo:'
                                                                        },
                                                                        cmbCiclo2,
                                                                        {
                                                                        	x:280,
                                                                            y:70,
                                                                            html:'Periodo:'
                                                                        },
                                                                        cmbPeriodo2,
                                                                        {
                                                                        	x:10,
                                                                            y:100,
                                                                            html:'Plan de Estudios:'
                                                                        },
                                                                        cmbPlanEstudio2,
                                                                        {
                                                                        	x:10,
                                                                            y:130,
                                                                            html:'Grado:'
                                                                        },
                                                                        cmbGrados
                                                                        
                                                            		]
                                                        },
                                                        gridEsquemasF
                                                        

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Importar Esquema de Fechas',
										width: 750,
										height:520,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                     					if(gridEsquemasF.getStore().getCount()==0)
                                                                        {
                                                                        	msgBox('No existe un esquema de fechas a importar');
                                                                        	return;
                                                                        }
																		function resp(btn)	
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	var x=0;
                                                                                var fila;
                                                                                var gridTabuladorPlanPago=gEx('gridTabuladorPlanPago');
                                                                                var pos;
                                                                                var filaAux;
                                                                                var z;
                                                                                var fila2;

                                                                                for(x=0;x<gridEsquemasF.getStore().getCount();x++)
                                                                                {
                                                                                	fila=gridEsquemasF.getStore().getAt(x);
                                                                                    
                                                                                    for(z=0;z<gridTabuladorPlanPago.getStore().getCount();z++)
                                                                                    {
                                                                                    	filaAux=gridTabuladorPlanPago.getStore().getAt(z);
                                                                                        if((fila.get('idPlanPago')==filaAux.get('idPlanPago'))&&(fila.get('noPago')==filaAux.get('noPago')))
                                                                                        {
                                                                                        	filaAux.set('fechaMaximaDescuento',fila.get('fechaMaximaDescuento'));
                                                                                        }
                                                                                    }
                                                                                    
                                                                                  
                                                                                    
                                                                                }
                                                                                ventanaAM.close();
                                                                                	
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer importar el Esquema de Fechas de pago seleccionado?',resp);
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function cargarEsquemasFechas()
{
	gEx('gridEsquemaFechasImportar').getStore().reload();
}


function crearGridEsquemaFechas(iConcepto,tConcepto)
{
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'noPago'},
		                                                {name: 'idPlanPago'},
		                                                {name: 'nombrePlanPago'},
		                                                {name: 'etiquetaPago'},
                                                        {name: 'pagoIndividual'},
                                                        {name: 'montoDescuento1'},
                                                        {name: 'pagoDescuento1'},
                                                        {name: 'fechaMaximaDescuento'},
                                                        {name: 'montoDescuento2'},
                                                        {name: 'pagoDescuento2'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	var chkRow=new Ext.grid.CheckboxSelectionModel(); 
    var summary = new Ext.ux.grid.GroupSummary();                                                                                  
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'nombrePlanPago', direction: 'ASC'},
                                                            groupField: 'nombrePlanPago',
                                                            remoteGroup:false,
				                                            remoteSort: false,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                        
                                        var arrConcepto=new Array();
                                        arrConcepto[0]='';
                                        arrConcepto[1]=tConcepto;
                                        arrConcepto[2]=iConcepto;
                                        
                                    	var cadObj='{"plantel":"'+plantel+'","idCiclo":"'+gEx('cmbCiclo').getValue()+'","idPeriodo":"'+gEx('cmbPeriodo').getValue()+
                                        			'","idConcepto":"'+arrConcepto[2]+'","tipoConcepto":"'+arrConcepto[1]+'","idElemento":"'+gEx('cmbGrados').getValue()+'","tipoElemento":"1","idInstanciaPlanEstudio":"'+gEx('cmbPlanEstudio2').getValue()+'"}';
                                    	proxy.baseParams.funcion='14';
                                        proxy.baseParams.cadObj=cadObj;
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            chkRow,
                                                            {
                                                                header:'No. Pago',
                                                                width:65,
                                                                align:'center',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                dataIndex:'noPago'
                                                                
                                                            },
                                                            {
                                                                header:'Plan de pagos',
                                                                width:250,
                                                                align:'left',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:left !important;',
                                                                dataIndex:'nombrePlanPago'
                                                            },
                                                            {
                                                                header:'Pago',
                                                                width:250,
                                                                align:'left',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:left !important;',
                                                                dataIndex:'etiquetaPago'
                                                            },
                                                            {
                                                                header:'Fecha m&aacute;xima para descuento<br> por Pronto Pago',
                                                                width:160,
                                                                dataIndex:'fechaMaximaDescuento',
                                                                align:'center',
                                                                sortable:false,
                                                                menuDisabled :true,
                                                                css:'text-align:right !important;',
                                                                renderer:formatearSoloFecha
                                                            }
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                            {
                                                                id:'gridEsquemaFechasImportar',
                                                                store:alDatos,
                                                                title:'Esquemas de Fechas de pago',
                                                                height:220,
                                                                y:220,
                                                                frame:true,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                plugins:[summary],
                                                                sm:chkRow,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: true,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}


function obtenerDiaHabil(fecha,accion)
{
	var dia=fecha.getDay();
    if(dia==0)
	   	dia=7;
    if(existeValorArreglo(arrDiasPermitidos,dia)!=-1)
    {
    	if(!esDiaInhabil(fecha))
        	return fecha;
        
    }
    
    if(accion=='0')
        fecha=fecha.add(Date.DAY,1);
   	else
        fecha=fecha.add(Date.DAY,-1);
   	
    return obtenerDiaHabil(fecha,accion);
    
    
    
}

function esDiaInhabil(fecha)
{
	var x;
    for(x=0;x<fechasNoHabiles.length;x++)
    {
    	if((fecha>=fechasNoHabiles[x][0])&&(fecha<=fechasNoHabiles[x][1]))
        	return true;
        if(fechasNoHabiles[x][0]>fecha)
        	return false
    }
    return false;
}