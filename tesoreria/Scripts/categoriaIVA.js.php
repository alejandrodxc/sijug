<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idZona,zona FROM 6937_zonas";
	$arrZonas=$con->obtenerFilasArreglo($consulta);
	
?>
var arrZonas=<?php  echo $arrZonas?>;

Ext.onReady(inicializar);

function inicializar()
{
 	var tabs = $( "#tabs" ).tabs(
    								{
    									activate: function( event, ui ) 
                                        		{

                                                	if(ui.newTab.context.innerHTML=='Administración de Zonas/Porcentajes')
                                                    {
                                                    	gEx('gZona').getView().refresh();
                                                    }
                                                }
                                    }
                                 )
	crearGridZonasPorcentaje();                                 
}


function crearGridZonasPorcentaje()
{
	
    
    var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idZona'},
                                                        {name: 'porcentaje'},
                                                        {name: 'fechaInicio', type:'date', dateFormat:'Y-m-d'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'idZona', direction: 'ASC'},
                                              groupField: 'idZona',
                                              remoteGroup:false,
                                              remoteSort: true,
                                              autoLoad:true
                                              
                                          }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='26';
                                        proxy.baseParams.idCategoria=gE('id').value;
                                        
                                    }
                        )   
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Zona',
															width:290,
															sortable:true,
															dataIndex:'idZona',
                                                            renderer:function(val)		
                                                            		{
                                                                    	return formatearValorRenderer(arrZonas,val);
                                                                    }
                                                            		
														},
														{
															header:'&Uacute;ltimo porcentaje IVA',
															width:130,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'porcentaje',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val!='')
                                                                        {
                                                                        	return Ext.util.Format.number(val,'0.00')+' %';
                                                                        }
                                                                    }
														},
                                                        {
                                                            header: 'Vigencia a partir del',
                                                            dataIndex: 'fechaInicio',
                                                            css:'text-align:right;',
                                                            width: 120,
                                                            renderer:function(val,meta,registro)
                                                                    {
                                                                        
                                                                        if(val)
                                                                            return '<a href="javascript:mostrarVentanaHistorialPorcentaje(\''+bE(registro.data.idZona)+'\',\''+bE(formatearValorRenderer(arrZonas,registro.data.idZona))+'\')"><img width=13 height=13 src="../images/book_spelling.png" title="Ver historial de precios" alt="Ver historial de precios"></a> '+val.format('d/m/Y');
                                                                    }
                                                            
                                                            
                                                            
                                                        }
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gZona',
                                                            store:alDatos,
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            renderTo:'tblZonaPorcentaje',
                                                            columnLines : true,
                                                            height:160,
                                                            width:650,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar zona',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaAgregarZona();
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover zonas',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=gEx('gZona').getSelectionModel().getSelections();
                                                                                        if(filas.length==0)
                                                                                        {
                                                                                            msgBox('Debe seleccionar las zonas que desea remover');
                                                                                            return;
                                                                                        }
                                                                                        var zonas='';
                                                                                        var fila;
                                                                                        var x;
                                                                                        for(x=0;x<filas.length;x++)
                                                                                        {
                                                                                            fila=filas[x];
                                                                                            if(zonas=='')
                                                                                                zonas=fila.data.idZona;
                                                                                            else
                                                                                                zonas+=','+fila.data.idZona;
                                                                                            
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                        	if(btn=='yes')
                                                                                            {
                                                                                                function funcAjax()
                                                                                                {
                                                                                                    var resp=peticion_http.responseText;
                                                                                                    arrResp=resp.split('|');
                                                                                                    if(arrResp[0]=='1')
                                                                                                    {
                                                                                                        gEx('gZona').getStore().reload()
                                                                                                        ventanaAM.close();
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                                    }
                                                                                                }
                                                                                                obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=29&idCategoria='+gE('id').value+'&zonas='+zonas,true);
                                                                                        	}        
                                                                                    	}
                                                                                        msgConfirm('Est&aacute; seguro de querer remover las zonas seleccionadas',resp);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/pencil.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Modificar porcentaje',
                                                                            handler:function()
                                                                            		{
                                                                                    	var filas=gEx('gZona').getSelectionModel().getSelected();
                                                                                        if(!filas)
                                                                                        {
                                                                                            msgBox('Debe seleccionar la zona que desea modificar');
                                                                                            return;
                                                                                        }
                                                                                        mostrarVentanaModificarPorcentaje(filas);
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	return 	tblGrid;	
}       

function mostrarVentanaAgregarZona()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														crearGridZonasAdd()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar zona',
										width: 650,
										height:350,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var filas=gEx('gZonaAdd').getSelectionModel().getSelections();
                                                                        if(filas.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos una zona a agregar');
                                                                        	return;
                                                                        }
                                                                        var zonas='';
                                                                        var fila;
                                                                        var x;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	fila=filas[x];
                                                                            if(zonas=='')
                                                                            	zonas=fila.data.idZona;
                                                                            else
                                                                            	zonas+=','+fila.data.idZona;
                                                                            
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gZona').getStore().reload()
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=28&idCategoria='+gE('id').value+'&zonas='+zonas,true);
                                                                        
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}                          


function crearGridZonasAdd()
{
	
    
    var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idZona'},
                                                        {name: 'zona'}
                                                       
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                              reader: lector,
                                              proxy : new Ext.data.HttpProxy	(

                                                                                {

                                                                                    url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                }

                                                                            ),
                                              sortInfo: {field: 'idZona', direction: 'ASC'},
                                              groupField: 'idZona',
                                              remoteGroup:false,
                                              remoteSort: true,
                                              autoLoad:true
                                              
                                          }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='27';
                                        proxy.baseParams.idCategoria=gE('id').value;
                                        
                                    }
                        )   
    
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Zona',
															width:220,
															sortable:true,
															dataIndex:'zona'
                                                            		
														},
														{
															header:'Descripci&oacute;n',
															width:300,
															sortable:true,
                                                            css:'text-align:right;',
															dataIndex:'descripcion'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gZonaAdd',
                                                            store:alDatos,
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            
                                                            columnLines : true,
                                                            height:260,
                                                            width:620,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;	
}       

function mostrarVentanaModificarPorcentaje(filaZona)
{
	var fechaActual=Date.parseDate('<?php echo date('Y-m-d')?>','Y-m-d');
    var minFecha=fechaActual;
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														{
                                                        	x:10,
                                                            y:10,
                                                            html:'Porcentaje IVA:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:5,
                                                            id:'txtPorcentaje',
                                                            xtype:'numberfield',
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        },
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Vigente a partir del:'
                                                        },
                                                        {
                                                        	x:135,
                                                            y:35,
                                                            xtype:'datefield',
                                                            id:'dteVigencia',
                                                            minValue:minFecha,
                                                            allowDecimals:true,
                                                            allowNegative:false
                                                        }

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Modificar porcentaje IVA',
										width: 340,
										height:150,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtPorcentaje').focus(false,500);
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
                                                                    	var cadObj='';
                                                                        var obj='';
																		var x;
                                                                        var dteVigencia=gEx('dteVigencia');
                                                                        var txtPorcentaje=gEx('txtPorcentaje');
                                                                        
                                                                        if(txtPorcentaje.getRawValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	txtPorcentaje.focus();
                                                                            }
                                                                            msgBox('Debe ingresar el porcentaje de IVA a asignar a la zona',resp);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        if(dteVigencia.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	dteVigencia.focus();
                                                                            }
                                                                            msgBox('Dee ingresar la fecha inicio de vigencia del porcentaje de IVA',resp2);
                                                                            return;
                                                                        	
                                                                        }
                                                                        
                                                                        function funcAjax()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	gEx('gZona').getStore().reload();
                                                                                ventanaAM.close();
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=30&idCategoria='+gE('id').value+'&idZona='+filaZona.data.idZona+
                                                                        				'&porcentaje='+txtPorcentaje.getValue()+'&fechaInicio='+dteVigencia.getValue().format('Y-m-d'),true);
                                                                        
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function mostrarVentanaHistorialPorcentaje(idZona,z)
{
	
   
    
	var gridHistorialPorcentaje=crearGridHistorialPorcentaje(idZona);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<b><span>Zona: </span><Span class="letraRojaSubrayada8">'+bD(z)+'</span></b>'
                                                        },
														gridHistorialPorcentaje
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Historial de porcentaje IVA',
										width: 450,
										height:300,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
                                                        show : {
                                                                    buffer : 10,
                                                                    fn : function() 
                                                                    {
                                                                    }
                                                                }
                                                    },
										buttons:	[
														
														{
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function crearGridHistorialPorcentaje(idZona)
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idPorcentaje'},
		                                                {name: 'porcentaje'},
		                                                {name:'fechaInicio', type:'date', dateFormat:'Y-m-d'}
                                                        
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesTesoreria.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaInicio', direction: 'DESC'},
                                                            groupField: 'fechaInicio',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='31';
                                        proxy.baseParams.idCategoria=gE('id').value;
                                        proxy.baseParams.idZona=bD(idZona);
                                    }
                        )   
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer(),
                                                            {
                                                                header:'Porcentaje IVA',
                                                                width:180,
                                                                sortable:true,
                                                                dataIndex:'porcentaje',
                                                                css:'text-align:right;',
                                                                renderer:function(val)
                                                                	{
                                                                    	if(val!='')
                                                                        {
                                                                        	return Ext.util.Format.number(val,'0.00')+' %';
                                                                        }
                                                                    }
                                                            },
                                                            {
                                                                header:'Vigencia a partir del',
                                                                width:120,
                                                                sortable:true,
                                                                dataIndex:'fechaInicio',
                                                                css:'text-align:right;',
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        }
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridHistorialPrecio',
                                                                store:alDatos,
                                                                frame:false,
                                                                border:true,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                height:220,
                                                                y:40,
                                                                columnLines : true,
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );
        return 	tblGrid;	
}
