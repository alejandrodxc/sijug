<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	
	$consulta="SELECT idEmpresa,IF(tipoEmpresa=1,CONCAT(razonSocial,' ',apPaterno,' ',apMaterno),razonSocial) AS nombreEmpresa,
				(SELECT CONCAT(municipio,', ',UPPER(estado)) FROM  821_municipios m,820_estados e WHERE cveMunicipio=e.municipio AND m.cveEstado=e.cveEstado)  FROM 6927_empresas e WHERE referencia='".$referenciaFiltros."' and esEmpresaUsuario=1";
	$arrEmpresas=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idMoneda,moneda FROM 603_tipoMoneda ORDER BY moneda";
	$arrMoneda=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idFormaPago,formaPago FROM 710_metodoPagoComprobante ORDER BY idFormaPago";
	$arrMetodoPago=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idTipoComprobante,tipoComprobante FROM 705_tiposComprobantes ORDER BY tipoComprobante";
	$arrTipoComprobante=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idUnidadMedida,unidadMedida FROM 6923_unidadesMedida ORDER BY unidadMedida";
	$arrUnidadesMedida=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=1 ORDER BY descripcion";
	$arrImpuestos=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=2 ORDER BY descripcion";
	$arrRetenciones=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idConcepto,descripcion,funcionCalculo FROM 711_catalogoImpuestosRetenciones WHERE tipoConcepto=0 ORDER BY descripcion";
	$arrIntermedios=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT valor,texto FROM 1004_siNo";
	$arrSiNo=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT * FROM 711_catalogoImpuestosRetenciones ORDER BY prioridad";
	$arrConceptosImpRet=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT tipoConcepto,idConcepto,porcentajeDefault,'0' AS monto,prioridad,porcentajeModificable,comentarios,removible FROM 711_catalogoImpuestosRetenciones WHERE 
			idConcepto IN (1,5,6,7,12) ORDER BY prioridad";
			
	$arrConceptosDefault=$con->obtenerFilasArreglo($consulta);
	
	
?>


var arrConceptosDefault=<?php echo $arrConceptosDefault?>;
var arrConceptosImpRet=<?php echo $arrConceptosImpRet?>;
var arrSiNo=<?php echo $arrSiNo?>;
var arrIntermedios=<?php echo $arrIntermedios?>;
var arrImpuestos=<?php echo $arrImpuestos?>;
var arrRetenciones=<?php echo $arrRetenciones?>;
var arrUnidadesMedida=<?php echo $arrUnidadesMedida?>;
var arrTipoComprobante=<?php echo $arrTipoComprobante?>;
var idEmpresaFacturacion=-1;
var arrMetodoPago=<?php echo $arrMetodoPago?>;
var arrFormaPago=[['1','Pago en una sola exhibici\xF3n'],['2','Parcialidad']];
var arrMoneda=<?php echo $arrMoneda?>;
var arrEmpresas=<?php echo $arrEmpresas?>;
var reg=null;
var idProveedor=-1;
Ext.onReady(inicializar);

function inicializar()
{
	idEmpresaFacturacion=gE('idEmpresa').value;

	 reg=crearRegistro	(
                            [
                                {name: 'idConcepto'},
                                {name: 'tipoConcepto'},
                                {name: 'codigoAlternativo'},
                                {name: 'descripcion'},
                                {name: 'unidadMedida'},
                                {name: 'costoUnitario'},
                                {name: 'descuentoUnitario'},
                                {name: 'cantidad'},
                                {name: 'subtotal'},
                                {name: 'iva'},
                                {name: 'tasaIVA'},
                                {name: 'total'},
                                {name: 'descuentoTotal'},
                                {name: 'ivaDeducible'}
                                
                            ]
                        )

	var cmbTipoComprobante=crearComboExt('cmbTipoComprobante',arrTipoComprobante,670,100,200);
	cmbTipoComprobante.setValue('1');
	

	
    var oConf=	{
    					idCombo:'cmbRFC',
                        anchoCombo:150,
                        anchoLista:400,
                        campoDesplegar:'rfc',
                        campoID:'idEmpresa',
                        funcionBusqueda:50,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:150,
                        posY:0,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProveedor=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRFC').getRawValue();
                                        dSet.baseParams.tipoBusqueda=1;
                                        gEx('cmbRazonSocial').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProveedor=registro.get('idEmpresa');
                                       	gEx('cmbRazonSocial').setRawValue(registro.data.razonSocial); 
                                        
                                    }  
    				};

    
	var cmbRFCEmisor=crearComboExtAutocompletar(oConf);
    
    
    var oConf=	{
    					idCombo:'cmbRazonSocial',
                        anchoCombo:420,
                        campoDesplegar:'razonSocial',
                        campoID:'idEmpresa',
                        funcionBusqueda:50,
                        raiz:'registros',
                        nRegistros:'numReg',
                        posX:310,
                        posY:0,
                        paginaProcesamiento:'../paginasFunciones/funcionesTesoreria.php',
                        confVista:	'<tpl for="."><div class="search-item"><table><tr><td width="380">[{rfc}] {razonSocial}<br>---<br>{domicilioFiscal}</td><td width="50"></td></tr></table></div></tpl>',
                        campos:	[
                                    {name:'idEmpresa'},
                                    {name:'rfc'},
                                    {name:'razonSocial'},
                                    {name:'domicilioFiscal'}
                                   
                                ],
                       	funcAntesCarga:function(dSet,combo)
                    				{
                                    	idProveedor=-1;
                                        dSet.baseParams.valorBusqueda=gEx('cmbRazonSocial').getRawValue();
                                        dSet.baseParams.tipoBusqueda=2;
                                        gEx('cmbRFC').setValue(''); 
                                        
                                    },
                      	funcElementoSel:function(combo,registro)
                    				{
                                    	idProveedor=registro.get('idEmpresa');
                                       	gEx('cmbRFC').setRawValue(registro.data.rfc); 
                                        
                                    }  
    				};

    
	var cmbRazonSocialEmisor=crearComboExtAutocompletar(oConf);      
              
    


	var cmbMetodoPago=crearComboExt('cmbMetodoPago',arrMetodoPago,150,120,170);   
    
    cmbMetodoPago.on('select',function(cmb,registro)
    						{
                            	if(registro.data.id!='5')
                                {
                                	gEx('lblEspecifique').hide();
                                    gEx('txtEspecifique').hide();
                                    
                                }
                                else
                                {
                                	gEx('lblEspecifique').show();
                                    gEx('txtEspecifique').show();
                                    gEx('txtEspecifique').focus(false,500);
                                }
                            }
    				)
                 
	new Ext.Panel	(
    					{
                            width:900,
                            height:950,
                            bodyStyle:' background-color: rgb(240, 240, 240);border-color: rgb(240,240,240) !important;',
                            border:false,
                            buttonAlign :'right',
                            layout:'absolute',
                            bbar:	[
                            			{
                                            icon:'../images/icon_big_tick.gif',
                                            cls:'x-btn-text-icon',
                                            height:40,
                                            text:'Registrar comprobante',
                                            handler:function()
                                                    {
                                                       
                                                        
                                                        if(idProveedor==-1)
                                                        {
                                                        	function resp12()
                                                            {
                                                                cmbRazonSocial.focus();
                                                            }
                                                            msgBox('Debe especificar el proveedor que emiti&oacute; el comprobante',resp12);
                                                            return;
                                                        }
                                                        
                                                        var dteFechaComprobante=gEx('dteFechaComprobante');
                                                        
                                                        
                                                        if(dteFechaComprobante.getValue()=='')
                                                        {
                                                        	function resp4()
                                                            {
                                                            	dteFechaComprobante.focus();
                                                            }
                                                            msgBox('Debe especificar la fecha del comprobante',resp4);
                                                            return;
                                                        }
                                                        
                                                        if(gEx('txtFolio').getValue()=='')
                                                        {
                                                        	function respFolio()
                                                            {
                                                            	gEx('txtFolio').focus();
                                                            }
                                                            msgBox('Debe especificar el folio del comprobante',respFolio);
                                                            return;
                                                        
                                                        }
                                                        
                                                        var txtCondicionesPago=gEx('txtCondicionesPago') ;
                                                        
                                                        
                                                        if(cmbMetodoPago.getValue()=='')
                                                        {
                                                        	function resp10()
                                                            {
                                                            	cmbMetodoPago.focus();
                                                            }
                                                            msgBox('Debe especificar el m&eacute;todo de pago del comprobante',resp10);
                                                            return;
                                                        }
                                                        
                                                       
                                                        
                                                       	var txtEspecifique=gEx('txtEspecifique') ;
                                                        
                                                         if(cmbMetodoPago.getValue()=='5')
                                                         {
                                                         	if(txtEspecifique.getValue()=='')
                                                            {
                                                                function resp11()
                                                                {
                                                                    txtEspecifique.focus();
                                                                }
                                                                msgBox('Debe especificar el m&eacute;todo de pago del comprobante',resp11);
                                                                return;
                                                            }
                                                         }
                                                        
                                                        var txtNoCuenta=gEx('txtNoCuenta');
                                                        
                                                        
                                                        var arrConceptos='';
                                                        var fila;
                                                        var gConceptos=gEx('gConcepto');
                                                        var x;
                                                        for(x=0;x<gConceptos.getStore().getCount();x++)
                                                        {
                                                        	fila=gConceptos.getStore().getAt(x);
                                                            
                                                            if(fila.data.descripcion.trim()=='')
                                                            {
                                                            	function resp100()
                                                                {
                                                                	
                                                                    gConceptos.startEditing(x,1);
                                                                }
                                                            	msgBox('Debe especificar la descripci&oacute;n del concepto',resp100);
                                                            	return;
                                                            }
                                                            
                                                            
                                                            o='{"idConcepto":"'+fila.data.idConcepto+'","tipoConcepto":"'+fila.data.tipoConcepto+'","descripcion":"'+cv(fila.data.descripcion,false,true)+'","costoUnitario":"'+
                                                            	fila.data.costoUnitario+'","cantidad":"'+fila.data.cantidad+'","subtotal":"'+fila.data.subtotal+
                                                                '","iva":"'+fila.data.iva+'","tasaIVA":"'+fila.data.tasaIVA+'","total":"'+fila.data.total+'","deducible":"'+fila.data.ivaDeducible+
                                                                '","descuentoUnitario":"'+fila.data.descuentoUnitario+'","descuentoTotal":"'+fila.data.descuentoTotal+'"}';
                                                            if(arrConceptos=='')
                                                            	arrConceptos=o;
                                                            else
                                                            	arrConceptos+=','+o;
                                                        }
                                                        if(arrConceptos=='')
                                                        {
                                                        	msgBox('Debe especificar almenos un producto perteneciente al comprobante');
                                                        	return;
                                                        }
                                                        var arrTotales='';
                                                        var gMontosFinales=gEx('gMontosFinales');
                                                        for(x=0;x<gMontosFinales.getStore().getCount();x++)
                                                        {
                                                        	fila=gMontosFinales.getStore().getAt(x);
                                                        	o='{"tipoConcepto":"'+fila.data.tipoConcepto+'","idConcepto":"'+fila.data.idConcepto+'","tasaConcepto":"'+fila.data.tasaConcepto+'","montoConcepto":"'+fila.data.montoConcepto+'"}';
                                                            if(arrTotales=='')
                                                            	arrTotales=o;
                                                            else
                                                            	arrTotales+=','+o;
                                                        }
                                                        
                                                        
                                                        
                                                        var cadObj='{"idComprobante":"'+gE('idComprobante').value+'","tipoComprobante":"'+cmbTipoComprobante.getValue()+'","motivoDescuento":"'+cv(gEx('txtMotivoDescuento').getValue(),false,true)+'","arrTotales":['+arrTotales+
                                                        			'],"comentariosAdicionales":"'+cv(gEx('txtComentarios').getValue(),false,true)+'","arrConceptos":['+arrConceptos+'],"idEmpresa":"'+idEmpresaFacturacion+
                                                                    '","folioUUID":"'+gEx('txtUUID').getValue()+'","folio":"'+gEx('txtFolio').getValue()+'","serie":"'+gEx('txtSerie').getValue()+'","idProveedor":"'+idProveedor+'","fechaComprobante":"'+
                                                        			dteFechaComprobante.getValue().format("Y-m-d")+'","condicionesPago":"'+txtCondicionesPago.getValue()+
                                                                    '","metodoPago":"'+cmbMetodoPago.getValue()+'","metodoPagoEspecifique":"'+cv(txtEspecifique.getValue(),false,true)+'","noCuenta":"'+gEx('txtNoCuenta').getValue()+'"}';
                                                        
                                                        
                                                        
                                                       
                                                        function funcAjax()
                                                        {
                                                            var resp=peticion_http.responseText;
                                                            arrResp=resp.split('|');
                                                            if(arrResp[0]=='1')
                                                            {
                                                            	function respFinal()
                                                                {
                                                                	inicializarCaptura();
                                                                    if(gE('idComprobante').value!='-1')
                                                                    {
                                                                    	regresarPagina();
                                                                    }
                                                                }
                                                                msgBox('La informaci&oacute;n ha sido guardada satisfactoriamente',respFinal);
                                                            }
                                                            else
                                                            {
                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                            }
                                                        }
                                                        obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=51&cadObj='+cadObj,true);
                                                        
                                                        
                                                    }
                                            
                                        },'-',
                                        {
                                            icon:'../images/cross.png',
                                            cls:'x-btn-text-icon',
                                            height:40,
                                            text:'Cancelar captura',
                                            handler:function()
                                            		{
                                                    	function respCaptura(btn)
                                                        {
                                                        	if(btn=='yes')
                                                            {
                                                            	regresarPagina();
                                                            }
                                                        }
                                                        msgConfirm('Est&aacute; seguro de querer cancelar la captura del comprobante?',respCaptura);
                                                    }
                                        }
                            		],
                            items:	[
                            			{
                                        	x:10,
                                            y:5,
                                            xtype:'fieldset',
                                            width:880,
                                            height:80,
                                            layout:'absolute',
                                            title:'Datos del Emisor',
                                            items:	[
                                            			{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:5,
                                                            html:'<span class="letraAzulSimple" style="color:#000">RFC/Raz&oacute;n Social:</span><span style="color:#F00;font-size:13px">*</span>'
                                                        },
                                                        cmbRFCEmisor,
                                                        cmbRazonSocialEmisor,
                                                        {
                                                        	x:740,
                                                            y:5,
                                                            xtype:'label',
                                                            html:'<a href="javascript:agregarProveedor()"><img src="../images/add.png"></a>'
                                                        }
                                                    ]
                                        },
                                        {
                                        	x:520,
                                            y:105,
                                            xtype:'label',
                                            html:'<span class="letraAzulSimple" style="color:#000">Tipo de comprobante:</span><span style="color:#F00;font-size:13px">*</span>'
                                        },
                                        cmbTipoComprobante,
                                        {
                                        	x:10,
                                            y:135,
                                            xtype:'fieldset',
                                            width:880,
                                            height:180,
                                            layout:'absolute',
                                            title:'Datos del Receptor',
                                            items:	[
                                            			{
                                                        	xtype:'label',
                                                            x:10,
                                                            y:5,
                                                            html:'<span class="letraAzulSimple" style="color:#000">RFC/Raz&oacute;n Social:</span><span style="color:#F00;font-size:13px"></span>'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            width:150,
                                                            x:150,
                                                            y:0,
                                                            value:gE('rfc').value,
                                                            readOnly:true,
                                                            id:'txtRFC',
                                                        },
                                                         {
                                                        	xtype:'textfield',
                                                            width:270,
                                                            x:310,
                                                            y:0,
                                                            readOnly:true,
                                                            value:gE('razonSocial').value,
                                                            id:'razonSocial',
                                                        }
                                                        
                                                        ,
                                                        {
                                                        	xtype:'label',
                                                            x:600,
                                                            y:5,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Fecha del comprobante:</span><span style="color:#F00;font-size:13px">*</span>'
                                                        },
                                                        
                                                        {
                                                        	x:750,
                                                            y:0,
                                                            id:'dteFechaComprobante',
                                                            xtype:'datefield',
                                                            value:'<?php echo date("Y-m-d")?>'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:35,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Folio UUID:</span><span style="color:#F00;font-size:13px"></span>'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:30,
                                                            xtype:'textfield',
                                                            id:'txtUUID',
                                                            width:400
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:65,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Folio del comprobante:</span><span style="color:#F00;font-size:13px">*</span>'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:60,
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            id:'txtFolio',
                                                            width:100
                                                        },
                                                         {
                                                        	xtype:'label',
                                                            x:300,
                                                            y:65,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Serie:</span><span style="color:#F00;font-size:13px"></span>'
                                                        },
                                                        {
                                                        	x:360,
                                                            y:60,
                                                            xtype:'textfield',
                                                            id:'txtSerie',
                                                            width:90
                                                        },
                                                         
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:95,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Condiciones de pago:</span><span style="color:#F00;font-size:13px"></span>'
                                                        },
                                                        {
                                                        	xtype:'textfield',
                                                            x:150,
                                                            y:90,
                                                            width:300,
                                                            id:'txtCondicionesPago'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:10,
                                                            y:125,
                                                            html:'<span class="letraAzulSimple" style="color:#000">M&eacute;todo de pago:</span><span style="color:#F00;font-size:13px">*</span>'
                                                        },
                                                        cmbMetodoPago,
                                                        {
                                                        	xtype:'label',
                                                            x:340,
                                                            y:125,
                                                            hidden:true,
                                                            id:'lblEspecifique',
                                                            html:'<span class="letraAzulSimple" style="color:#000">Especifique:</span><span style="color:#F00;font-size:13px">*</span>'
                                                        },
                                                        {
                                                        	x:430,
                                                            y:120,
                                                            width:140,
                                                            hidden:true,
                                                            xtype:'textfield',
                                                            id:'txtEspecifique'
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            x:585,
                                                            y:125,
                                                            html:'<span class="letraAzulSimple" style="color:#000">No. Cuenta de Pago:</span><span style="color:#F00;font-size:13px"></span>'
                                                        },
                                                        {
                                                        	x:720,
                                                            y:120,
                                                            xtype:'numberfield',
                                                            allowDecimals:false,
                                                            allowNegative:false,
                                                            

                                                            id:'txtNoCuenta',
                                                            width:135
                                                        }
                                                        
                                            		]
                                        }
                                        ,
                                        {
                                        	x:0,
                                            y:310,
                                            xtype:'fieldset',
                                            width:900,
                                            height:700,
                                            layout:'absolute',
                                            border:false,
                                            items:	[
                                            			crearGridConceptos(),
                                                        {
                                                        	x:10,
                                                            y:335,
                                                            xtype:'label',
                                                            html:'<span class="letraAzulSimple" style="color:#000">Motivo del descuento:</span>'
                                                        },
                                                        {
                                                        	x:150,
                                                            y:330,
                                                            width:330,
                                                            //disabled:true,
                                                            xtype:'textfield',
                                                            id:'txtMotivoDescuento'
                                                           
                                                        },
                                                        {
                                                        	x:10,
                                                            y:360,
                                                            xtype:'fieldset',
                                                            title:'Comentarios adicionales:',
                                                            width:470,
                                                            height:140,
                                                            layout:'absolute',
                                                            items:	[
                                                            			{
                                                                        	xtype:'textarea',
                                                                            width:445,
                                                                            height:100,
                                                                            id:'txtComentarios'
                                                                        }
                                                            		]
                                                        },
                                                        crearGridTotal()
                                                       
                                                        
                                                        
                                            		]
                                        }
                                        
                            		],
                            renderTo:'tblFactura'
                        }
    				)
                    
		/*if(arrEmpresas.length==1)
        {
            cmbEmpresa.setValue(arrEmpresas[0][0]);
            cmbEmpresa.disable();
            dispararEventoSelectCombo('cmbEmpresa');
        }    */                
		
        
        if(gE('idComprobante').value!='-1')
        {
        	var obj=eval('['+bD(gE('objFactura').value)+']')[0];
			cargarObjFactura(obj);
        }
        
}

function crearGridConceptos()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                        fields:	[
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'codigoAlternativo'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'unidadMedida'},
                                                                    {name: 'costoUnitario'},
                                                                    {name: 'descuentoUnitario'},
                                                                    {name: 'cantidad'},
                                                                    {name: 'subtotal'},
                                                                    {name: 'iva'},
                                                                    {name: 'tasaIVA'},
                                                                    {name: 'total'},
                                                                    {name: 'descuentoTotal'},
                                                                    {name: 'ivaDeducible'}
                                                                    
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
    cmbUnidadMedida=crearComboExt('cmbUnidadMedida',arrUnidadesMedida);
    
    
    var cmbSiNo=crearComboExt('cmbSiNo',arrSiNo);
    
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														chkRow,
														
														{
															header:'Concepto',
															width:210,
															sortable:true,
															dataIndex:'descripcion',
                                                            editor:	{
                                                            			xtype:'textfield'
                                                            		}
														},
														{
															header:'Unidad de medida',
															width:100,
															sortable:true,
															dataIndex:'unidadMedida',
                                                            editor:cmbUnidadMedida,
                                                            hidden:true,
                                                            renderer:function(val)
                                                            		{
                                                                    	return formatearValorRenderer(arrUnidadesMedida,val);    
                                                                    }
														},
														{
															header:'Costo Unitario',
															width:80,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'costoUnitario',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Desc. Unitario',
															width:80,
                                                            //hidden:true,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'descuentoUnitario',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Cantidad',
															width:60,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'cantidad',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:false,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Subtotal',
															width:85,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'subtotal',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
                                                        {
															header:'Tasa IVA',
															width:60,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'tasaIVA',
                                                            renderer:function(val)
                                                            		{
                                                                    	return Ext.util.Format.number(val,'0.00');
                                                                    },
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'IVA',
															width:80,
                                                            css:'text-align:right;',
															sortable:true,
                                                            renderer:'usMoney',
															dataIndex:'iva',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
														{
															header:'Total',
															width:90,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'total',
                                                            renderer:'usMoney',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false,
                                                            		}
														},
                                                        {
															header:'Deducible',
															width:60,
                                                            css:'text-align:right;',
															sortable:true,
															dataIndex:'ivaDeducible',
                                                            renderer:function(val)
                                                            		{
                                                                    	var color='';
                                                                    	if(val=='1')
                                                                        	color='030';
                                                                        else
                                                                        	color='F00';
                                                                    	return '<span style="color:#'+color+'"><b>'+formatearValorRenderer(arrSiNo,val)+'</b></span>';
                                                                    }
                                                            	,
                                                            editor:	cmbSiNo
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            y:0,
                                                            id:'gConcepto',
                                                            x:0,
                                                            clicksToEdit:1,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            border:true,
                                                            columnLines : true,
                                                            height:320,
                                                           	width:880,
                                                            sm:chkRow,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            hidden:true,
                                                                            text:'Agregar concepto de almac&eacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	buscarPorProductoNombre(1);
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar concepto',
                                                                            handler:function()
                                                                            		{
                                                                                    	
                                                                                                                
                                                                                                                
                                                                                    	var r=new reg	(
                                                                                        					{
                                                                                                            	idConcepto:'',
                                                                                                                tipoConcepto:1,
                                                                                                                codigoAlternativo:'',
                                                                                                                descripcion:'',
                                                                                                                unidadMedida:'',
                                                                                                                costoUnitario:0,
                                                                                                                descuentoUnitario:0,
                                                                                                                cantidad:1,
                                                                                                                subtotal:0,
                                                                                                                iva:0,
                                                                                                                tasaIVA:0,
                                                                                                                total:0,
                                                                                                                descuentoTotal:0,
                                                                                                                ivaDeducible:'1'
                                                                                                            	
                                                                                                            }
                                                                                        				)                            
                                                                                    
                                                                                    
                                                                                    	tblGrid.getStore().add(r);
                                                                                        tblGrid.startEditing(tblGrid.getStore().getCount()-1,1);
                                                                                    
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/delete.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Remover concepto',
                                                                            handler:function()
                                                                            		{
                                                                                    	var fila=tblGrid.getSelectionModel().getSelected();
                                                                                        if(!fila)	
                                                                                        {
                                                                                        	msgBox('Debe seleccionar el concepto que desea remover');
                                                                                        	return;
                                                                                        }
                                                                                        
                                                                                        function resp(btn)
                                                                                        {
                                                                                            if(btn=='yes')
                                                                                            {
                                                                                                tblGrid.getStore().remove(fila);
                                                                                                calcularTotales();
                                                                                            }
                                                                                        }
                                                                                        msgConfirm('Est&aacute; seguro de querer remover el concepto seleccionado?',resp);
                                                                                        return;
                                                                                    }
                                                                            
                                                                        },'-',
                                                                        {
                                                                        	xtype:'checkbox',
                                                                            boxLabel:'Deshabilitar calculo autom&aacute;tico',
                                                                            id:'chkDesCalculo'
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
                                                    
                
                
                
	tblGrid.on('beforeedit',function(e)                                                    
    						{
                            	switch(e.field)
                                {
                                	case 'subtotal':
                                    case 'iva':
                                    case 'total':
                                    	if(!gEx('chkDesCalculo').getValue())
                                    	{
                                        	e.cancel=true
                                        }
                                    
                                    break;
                                }
                            }
			)                            		
                                                    
	tblGrid.on('afteredit',function(e)
    						{
                            
                            	if(e.record.data.costoUnitario=='')
                                {
                                	e.record.set('costoUnitario',0);
                                }
                                
                                if(e.record.data.cantidad=='')
                                {
                                	e.record.set('cantidad',0);
                                }
                                
                                if(e.record.data.subtotal=='')
                                {
                                	e.record.set('subtotal',0);
                                }
                                
                                if(e.record.data.iva=='')
                                {
                                	e.record.set('iva',0);
                                }
                                
                                
                                if(e.record.data.tasaIVA=='')
                                {
                                	e.record.set('tasaIVA',0);
                                }
                                
                                if(e.record.data.total=='')
                                {
                                	e.record.set('total',0);
                                }
                            
                            	if(parseFloat(e.record.data.descuentoUnitario)>parseFloat(e.record.data.costoUnitario))
                                {
                                	e.record.set(e.field,e.originalValue);
                                    e.grid.stopEditing();
                                    function respAux()
                                    {
                                    	e.grid.startEditing(e.row,e.column);
                                    }
                                    msgBox('Costo unitario NO puede ser menor que el descuento unitario',respAux);
                                	return;
                                }
                            
                            	if(!gEx('chkDesCalculo').getValue())
                                {    	
                                    switch(e.field)
                                    {
                                        case 'costoUnitario':
                                        case 'descuentoUnitario':
                                        case 'cantidad':
                                        case 'tasaIVA':
                                        
                                                var costoDescuento=parseFloat(e.record.data.costoUnitario)-parseFloat(e.record.data.descuentoUnitario);
                                               
                                                
                                                
                                                var subtotal=parseFloat(Ext.util.Format.number(costoDescuento*parseFloat(e.record.data.cantidad),'0.00'));
                                                var iva=parseFloat(Ext.util.Format.number(subtotal*(parseFloat(e.record.data.tasaIVA)/100),'0.00'));
                                                
                                                var total=subtotal+iva;
                                                var descuentoTotal=(parseFloat(e.record.data.costoUnitario)*parseFloat(e.record.data.cantidad))-subtotal;
                                        
                                        
                                                e.record.set('subtotal',subtotal);
                                                e.record.set('iva',iva);
                                                e.record.set('total',total);
                                                e.record.set('descuentoTotal',descuentoTotal);
                                        
                                        break;
                                        
                                    
                                    }
                            	}
                                else
                                {
                                	
                                	descuentoTotal=parseFloat(e.record.data.cantidad)*parseFloat(e.record.data.descuentoUnitario);
                                    e.record.set('descuentoTotal',descuentoTotal);
                                }
                                
                                calcularTotales();
                            }
    			)                                                    
                                                    
                                                    
	return 	tblGrid;	
}

function crearGridTotal()
{
	var dsDatos=arrConceptosDefault;

    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                    	sortInfo: {
                                                                        field: 'orden',
                                                                        direction: 'ASC' 
                                                                    },
                                                        fields:	[
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tasaConcepto'},
                                                                    {name: 'montoConcepto'},
                                                                    {name: 'orden', type:'int'},
                                                                    {name: 'porcentajeModificable'},
                                                                    {name: 'comentarios'},
                                                                    {name: 'removible'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	
														{
															header:'',
															width:150,
															sortable:false,
                                                            css:'text-align:right;',
															dataIndex:'idConcepto',
                                                            renderer:function(val,meta,registro,nFila)
                                                            		{
                                                                    	var arr;
                                                                        
                                                                        var comp='';
                                                                        if(registro.data.removible=='1')
                                                                        {
                                                                        	comp='<a href="javascript:removerConcepto('+nFila+')"><img src="../images/delete.png" width="10" height="10" title="Remover concepto" alt="Remover concepto"/></a>&nbsp;';
                                                                        }
                                                                    	switch(registro.data.tipoConcepto)
                                                                        {
                                                                        	case '1':
                                                                            	arr=arrImpuestos;
                                                                               	
	                                                                        	
                                                                            break;
                                                                            case '2':
                                                                            	arr=arrRetenciones;
	                                                                        	
                                                                            break;
                                                                            case '0':
                                                                            	arr=arrIntermedios;
                                                                            	
                                                                            break;
                                                                        }
                                                                        
                                                                        
                                                                        return comp+'<span style="font-size:12px"><b>'+formatearValorRenderer(arr,val)+'</b></span>';
                                                                        	
                                                                        
                                                                    }
														},
														{
															header:'',
															width:55,
															sortable:false,
															dataIndex:'tasaConcepto',
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                                        
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val) && (val!=''))
	                                                                    	return Ext.util.Format.number(val,'0.00')+' %';
                                                                    }
														},
														{
															header:'',
															width:120,
                                                            css:'text-align:right;',
															sortable:false,
                                                            editor:	{
                                                            			xtype:'numberfield',
                                                                        allowDecimals:true,
                                                                        allowNegative:false
                                                                        
                                                            		},
                                                            renderer:function(val)
                                                            		{
                                                                    	return  '<span style="font-size:12px">'+Ext.util.Format.usMoney(val)+'</span>';
                                                                    },
                                                                    
															dataIndex:'montoConcepto'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:true,
                                                            x:515,
                                                            y:330,
                                                            border:false,
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:250,
                                                            id:'gMontosFinales',
                                                            width:360,
                                                            tbar:	[
                                                            			{
                                                                        	xtype:'checkbox',
                                                                            //checked:true,
                                                                            boxLabel:'Deshabilitar c&aacute;lculo autom&aacute;tico',
                                                                            id:'chkDesCalculoTotal',
                                                                            listener:	{
                                                                            				change:function(chk,valor)
                                                                                            	{
                                                                                                	if(!valor)
                                                                                                    	gEx('gMontosFinales').getView().refresh();
                                                                                                    	
                                                                                                }
                                                                            			}
                                                                        },'-',
                                                                        {
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Agregar concepto detalle...',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaConceptosDetalle();
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		]
                                                        }
                                                    );
	tblGrid.on('beforeedit',function(e)
    						{
                            	if((!gEx('chkDesCalculoTotal').getValue())&&(e.field!='tasaConcepto'))
                                	e.cancel=true;
                                if((e.field=='tasaConcepto') &&(e.record.data.porcentajeModificable=='0'))
                                {
                                	e.cancel=true;
                                }
                            }
    			)     
                
	tblGrid.on('afteredit',function(e)
    						{
                            	if((e.record.data.tipoConcepto!='0')&&(e.record.data.tasaConcepto==''))
                                	e.record.set('tasaConcepto',0);
                            	if(e.record.data.montoConcepto=='')
                                	e.record.set('montoConcepto',0);

                                if(!gEx('chkDesCalculoTotal').getValue())
	                             	calcularTotales();   
                            }
    			)                     
                                                               
	return 	tblGrid;		
}

function inicializarCaptura()
{
	idProveedor=-1;
	gEx('gMontosFinales').getStore().loadData(arrConceptosDefault);
    
    gEx('gConcepto').getStore().removeAll();
    var cmbRFCEmisor=gEx('cmbRFC');
    var cmbRazonSocialEmisor=gEx('cmbRazonSocial');
    var cmbTipoComprobante=gEx('cmbTipoComprobante');

    var dteFechaComprobante=gEx('dteFechaComprobante');
    var txtUUID=gEx('txtUUID');
    
    var txtFolio=gEx('txtFolio');
    var txtSerie=gEx('txtSerie');
    var txtCondicionesPago=gEx('txtCondicionesPago');
    var txtEspecifique=gEx('txtEspecifique');
    var txtNoCuenta=gEx('txtNoCuenta');
    var txtMotivoDescuento=gEx('txtMotivoDescuento');
    var txtComentarios=gEx('txtComentarios');
    var cmbMetodoPago=gEx('cmbMetodoPago');
    cmbRFCEmisor.setRawValue('');
    cmbRazonSocialEmisor.setRawValue('');
    
    cmbTipoComprobante.setValue('1');
    
    dteFechaComprobante.setValue('<?php echo date("Y-m-d")?>');
    txtUUID.setValue('');
    
    
    txtFolio.setValue('');
    txtSerie.setValue('');
    txtCondicionesPago.setValue('');
    cmbMetodoPago.setValue('');
    txtEspecifique.setValue('');
    txtNoCuenta.setValue('');
    txtMotivoDescuento.setValue('');
    txtComentarios.setValue('');
    
    txtEspecifique.hide();
    gEx('lblEspecifique').hide();
    
}

function agregarProveedor()
{

	var accionCancelar=bE('{msgConfirm(\'Est&aacute; seguro de querer cancelar la operaci&oacute;n?\',function(btn){if(btn==\'yes\'){window.parent.cerrarVentanaFancy();}})}');
    
    
	var obj={};
    obj.titulo='Alta de proveedor';
    obj.ancho=920;
    obj.alto=750;
    obj.url="../modeloAlmacenes/proveedores.php";
    obj.complete=	function()
    				{
				      $("#fancybox-wrap").css({'top':'20px', 'bottom':'auto'});    
                    }      
    obj.params=[['esProveedor','true'],['cPagina','sFrm=true'],['eJs',bE('window.parent.asignarProveedor(@idRegistro);')],['accionCancelar',accionCancelar],['idEmpresa',-1],['referencia','<?php echo $referenciaFiltros?>']];
    abrirVentanaFancy(obj);
}

function asignarProveedor(iRegistro)
{
	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            idProveedor=iRegistro;
            gEx('cmbRFC').setRawValue(arrResp[1]); 
            gEx('cmbRazonSocial').setRawValue(arrResp[2]); 
            
            cerrarVentanaFancy();
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesTesoreria.php',funcAjax, 'POST','funcion=45&idEmpresa='+iRegistro,true);
}

function cargarObjFactura(obj)
{
	idProveedor=obj.idProveedor;
	gEx('gMontosFinales').getStore().loadData(obj.arrTotales);
    gEx('gConcepto').getStore().loadData(obj.arrConceptos);
    var cmbRFCEmisor=gEx('cmbRFC');
    var cmbRazonSocialEmisor=gEx('cmbRazonSocial');
    var cmbTipoComprobante=gEx('cmbTipoComprobante');

    var dteFechaComprobante=gEx('dteFechaComprobante');
    var txtUUID=gEx('txtUUID');
    
    var txtFolio=gEx('txtFolio');
    var txtSerie=gEx('txtSerie');
    var txtCondicionesPago=gEx('txtCondicionesPago');
    var txtEspecifique=gEx('txtEspecifique');
    var txtNoCuenta=gEx('txtNoCuenta');
    var txtMotivoDescuento=gEx('txtMotivoDescuento');
    var txtComentarios=gEx('txtComentarios');
    var cmbMetodoPago=gEx('cmbMetodoPago');
    cmbRFCEmisor.setRawValue(obj.rfc);
    cmbRazonSocialEmisor.setRawValue(obj.proveedor);
    
    cmbTipoComprobante.setValue(obj.tipoComprobante);
    
    dteFechaComprobante.setValue(obj.fechaComprobante);
    txtUUID.setValue(obj.folioUUID);
    
    
    txtFolio.setValue(obj.folio);
    txtSerie.setValue(obj.serie);
    txtCondicionesPago.setValue(obj.condicionesPago);
    cmbMetodoPago.setValue(obj.metodoPago);
    txtEspecifique.setValue(obj.otroMetodo);
    txtNoCuenta.setValue(obj.noCuentaPago);
    txtMotivoDescuento.setValue(obj.motivoDescuento);
    txtComentarios.setValue(obj.comentarios);
    if(obj.metodoPago=='5')
    {
    	txtEspecifique.show();
        gEx('lblEspecifique').show();
    }
    else
    {
    	txtEspecifique.hide();
        gEx('lblEspecifique').hide();
    }
    

}

function mostrarVentanaConceptosDetalle()
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'<span class="letraAzulSimple" style="color:#000">Especifique los conceptos detalle que desea agregar:</span>'
                                                        },
														crearGridDetalleConcepto()

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar concepto detalle',
										width: 720,
										height:450,
                                        y:500,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var gConceptosDetalle=gEx('gConceptosDetalle');
                                                                        var filas=gConceptosDetalle.getSelectionModel().getSelections();
                                                                        if(filas.length==0)	
                                                                        {
                                                                        	msgBox('Debe seleccionar almenos un concpeto a agregar');
                                                                        	return;
                                                                        }
                                                                        
                                                                        var reg=crearRegistro	(
                                                                        							[
                                                                                                        {name: 'tipoConcepto'},
                                                                                                        {name: 'idConcepto'},
                                                                                                        {name: 'tasaConcepto'},
                                                                                                        {name: 'montoConcepto'},
                                                                                                        {name: 'orden'},
                                                                                                        {name: 'porcentajeModificable'},
                                                                                                        {name: 'comentarios'}
                                                                                                    ]
                                                                                                  )
                                                                         
                                                                         
                                                                         
                                                                         
                                                                      	var gMontosFinales=gEx('gMontosFinales') ; 
                                                                        
                                                                        
                                                                        var x;
                                                                        var fila;
                                                                        var r;
                                                                        for(x=0;x<filas.length;x++)
                                                                        {
                                                                        	fila=filas[x];
                                                                            r=new reg	(
                                                                            				{
                                                                                            	tipoConcepto:fila.data.tipoConcepto,
                                                                                                idConcepto:fila.data.idConcepto,
                                                                                                tasaConcepto:fila.data.porcentajeDefault,
                                                                                                montoConcepto:0,
                                                                                                orden:parseInt(fila.data.orden),
                                                                                                porcentajeModificable:fila.data.porcentajeModificable,
                                                                                                comentarios:fila.data.comentarios,
                                                                                                removible:fila.data.removible
                                                                                                
                                                                                            }
                                                                            			)
                                                                            
                                                                        	gMontosFinales.getStore().add(r);    
                                                                        }
                                                                        gMontosFinales.getStore().sort	(
                                                                        									[
                                                                                                                {
                                                                                                                    field:'orden',
                                                                                                                    direction: 'ASC'
                                                                                                                }
                                                                                                            ]
                                                                        								)
                                                                       
                                                                    	calcularTotales();  
                                                                        ventanaAM.close();                                  
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridDetalleConcepto()
{
	var dsDatos=[];
    
    var gMontosFinales=gEx('gMontosFinales');
    var x;
    var fila;
    var obj;
    for(x=0;x<arrConceptosImpRet.length;x++)
    {
    	fila=arrConceptosImpRet[x];
        if(obtenerPosFila(gMontosFinales.getStore(),'idConcepto',fila[0])==-1)
        {
        	obj=[fila[0],fila[1],fila[2],fila[3],fila[5],fila[6],fila[7],fila[8],fila[9]];
        	dsDatos.push(obj);
        }
    
    }
    
    var alDatos=	new Ext.data.SimpleStore	(
                                                    {
                                                    	sortInfo: {
                                                                        field: 'orden',
                                                                        direction: 'ASC' 
                                                                    },
                                                        fields:	[
                                                                    {name: 'idConcepto'},
                                                                    {name: 'tipoConcepto'},
                                                                    {name: 'descripcion'},
                                                                    {name: 'funcionCalculo'},
                                                                    {name: 'porcentajeDefault'},
                                                                    {name: 'orden', type:'int'},
                                                                    {name: 'porcentajeModificable'},
                                                                    {name: 'comentarios'},
                                                                    {name: 'removible'}
                                                                ]
                                                    }
                                                );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
                                                        chkRow,
														{
															header:'Concepto detalle',
															width:200,
															sortable:true,
															dataIndex:'descripcion'
														},
                                                        {
															header:'Valor tasa',
															width:60,
															sortable:true,
															dataIndex:'porcentajeDefault',
                                                            renderer:function(val)
                                                            		{
                                                                    	if((val) && (val!=''))
                                                                        	return Ext.util.Format.number(val,'0.00')+' %';
                                                                    }
														},
														{
															header:'Comentarios',
															width:330,
															sortable:true,
															dataIndex:'comentarios',
                                                            renderer:mostrarValorDescripcion
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            
                                                            store:alDatos,
                                                            frame:false,
                                                            border:true,
                                                            y:40,
                                                            x:10,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            id:'gConceptosDetalle',
                                                            columnLines : true,
                                                            height:310,
                                                            width:675,
                                                            sm:chkRow
                                                            
                                                        }
                                                    );
	return 	tblGrid;	

}

function removerConcepto(nFila)
{
	var gMontosFinales=gEx('gMontosFinales');
    var fila=gMontosFinales.getStore().getAt(nFila);
    gMontosFinales.getStore().remove(fila);
    calcularTotales();
}