<?php
include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include_once("latis/cfdi/funcionesFacturacion.php");
include_once("latis/cfdi/cFactura.php");

$fechaActual=date("Y-m-d");
$idVenta=-1;

if(isset($_POST["idVenta"]))
	$idVenta=$_POST["idVenta"];



$consulta="SELECT idFactura,idCliente,idCaja FROM 6008_ventasCaja WHERE idVenta=".$idVenta;
$fVenta=$con->obtenerPrimeraFila($consulta);
$idComprobante=$fVenta[0];
$idCaja=$fVenta[2];
$idCliente=$fVenta[1];

$idEmpresa=-1;
$idCertificado=-1;
$idSerie=-1;


$urlComprobante=$urlSitio;
$urlComprobante.="/formatosFacturasElectronicas/cfdi_1.php";



$consulta="SELECT identificadorCaja,idPerfilCaja,idEmpresa FROM 6007_instanciasCaja WHERE idCaja=".$idCaja;
$fCaja=$con->obtenerPrimeraFila($consulta);
$idPerfilCaja=$fCaja[1];
$moduloComprobante="";
$consulta="SELECT moduloImpresionFactura FROM 6006_perfilesCaja WHERE idPerfilCaja=".$idPerfilCaja;
$fPerfil=$con->obtenerPrimeraFila($consulta);
if($fPerfil[0]!="")
{
	$consulta="SELECT libreriaModulo FROM 9501_modulosProcesamientoResultado WHERE idModuloProcesamiento=".$fPerfil[0];
	$moduloComprobante=$con->obtenerValor($consulta);
}

if($moduloComprobante!="")
{
	$urlComprobante=$urlSitio."/".str_replace("../","",$moduloComprobante);
}

$oFactura=new cFacturaCFDI();
$nombreArchivoTmp=generarNombreArchivoTemporal();

$xml="";

if(($idComprobante=="")||($idComprobante=="-1"))
{
	
	if(!isset($_POST["empresaEmisora"]))
	{
		$consulta="SELECT idEmpresa,idCertificado,idSerie FROM 6007_instanciasCaja WHERE idCaja=".$idCaja;
		$fCaja=$con->obtenerPrimeraFila($consulta);
		if($fCaja[0]!="")
		{
			if($fCaja[0]!="")
				$idEmpresa=$fCaja[0];
			
			if($fCaja[1]!="")
			{
				$idCertificado=$fCaja[1];
				$noCertificados=1;
			}
			
			if($fCaja[2]!="")
			{
				$idSerie=$fCaja[2];
				$noSeries=1;
			}
				
		}
	}
	else
	{
		$idEmpresa=$_POST["empresaEmisora"];

		$consulta="SELECT idCertificado FROM 687_certificadosSelloDigital WHERE idReferencia=".$idEmpresa." AND fechaInicioVigencia<='".$fechaActual."' AND fechaFinVigencia>='".$fechaActual."'";
		$idCertificado=$con->obtenerValor($consulta);	
		$noCertificados=$con->filasAfectadas;
		$consulta="SELECT idSerieCertificado FROM 688_seriesCertificados WHERE idCertificado=".$idCertificado;
		$idSerie=$con->obtenerValor($consulta);	
		$noSeries=$con->filasAfectadas;
		
	}
	
	
	if(isset($_POST["idCertificado"]))
	{
		$idCertificado=$_POST["idCertificado"];	
		$noCertificados=1;
	}
	
	if(isset($_POST["idSerie"]))
	{
		$idSerie=$_POST["idSerie"];	
		$noSeries=1;
	}
	$objFactura=generarXMLVentaCajaGeneralV2($idVenta,$idCliente,$idEmpresa,$idCertificado,$idSerie);							
	$oFactura->setObjFactura($objFactura);
							
	$xml=$oFactura->generarXML();
	
	
	
	

}
else
{
	$oFactura=new cFacturaCFDI();
	$xml=$oFactura->cargarComprobanteXML($idComprobante);

}

escribirContenidoArchivo($baseDir."/archivosTemporales/".$nombreArchivoTmp,utf8_decode($xml));

$parametros="almacenarPDF=1&iVenta=".$idVenta."&archivoXML=".$baseDir."/archivosTemporales/".$nombreArchivoTmp;

$comando="wget --post-data \"".$parametros."\" ".$urlComprobante." -O /dev/null";

$resultado=shell_exec($comando);

$archivoPDF=$baseDir."/archivosTemporales/v_".$idVenta.".pdf";
unlink($baseDir."/archivosTemporales/".$nombreArchivoTmp);



header("Content-length: ".filesize($archivoPDF));
header('Content-Type: application/pdf');
header("Content-Disposition: inline; filename=v_".$idVenta.".pdf");
readfile($archivoPDF);

	
unlink($archivoPDF);





?>