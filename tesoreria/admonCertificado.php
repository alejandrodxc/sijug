<?php include("latis/sesiones.php");
include("latis/conexionBD.php"); 
include("latis/configurarIdioma.php");
include("latis/funcionesPortal.php");?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" dir="ltr"><!-- InstanceBegin template="/Templates/Lhayas_B.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta name="description" content=""/>
<meta name="keywords" content="" />
<meta name="robots" content="index,follow" />
<link rel="stylesheet" type="text/css" href="../css/hayas.css.php" media="screen" />
<link rel="stylesheet" type="text/css" href="../estilos/estilos.css" media="screen" />
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE7.css" media="screen" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE6.css" media="screen" /><![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="../estilos/estilosExtIE8.css" media="screen" /><![endif]-->
<?php
if(!isset($excluirExt))
{
?>
<link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all.css.cgz"/>
<script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/ext-all.js.jgz"></script>
<script type="text/javascript" src="../Scripts/ext/idioma/ext-lang-es.js"></script>
<?php
}
?>
<script type="text/javascript" src="../Scripts/funcionesAjax.js.jgz"></script>
<script type="text/javascript" src="../Scripts/funcionesGenerales.js"></script>


<?php 
$idEstiloMenu=0;
$procesoName="";
$mostrarRegresar=true;
$ocultarRegresar=!$mostrarOpcionRegresar;
$soloContenido=false;
$mostrarRegresarBajo=false;
$mostrarMenuNivel1=true;
$mostrarMenuNivel2=true;
$mostrarMenuIzq=false;
$mostrarTitulo=true;
$respetarEspacioRegresar=false;
$tamColumIzq="210";
$mostrarUsuario=true;
$mostrarPiePag=true;
$ocultarFormulariosEnvio=false;
$paginaDestino="";

$sqlmax = "SELECT disenoBanner,textoInfIzq,textInfDerecho,tituloPagina,Menu,txTabla3,txTabla4,TiTabla FROM 4081_colorEstilo";
$unico= $con->obtenerPrimeraFila($sqlmax);
$banner=$unico[0];
$textoInfIzq=$unico[1];
$textoInfDer=$unico[2];
$tituloPagina=$unico[3];
$nomPagina=$_SERVER["PHP_SELF"];
$arrPagina=	explode("/",$nomPagina);
$nElementos=sizeof($arrPagina);
$nomPagina=$arrPagina[$nElementos-1];
$rutaNomPagina=$arrPagina[$nElementos-2]."/".$arrPagina[$nElementos-1];
$arrPagina=explode(".",$nomPagina);
$nomPagina=$arrPagina[0];
$paramPOST=true;
$paramGET=false;
$guardarConfSession=false;
$arrPOST=array_values($_POST);
$ctPOST=sizeof($arrPOST);
$arrGET=array_values($_GET);
$ctGET=sizeof($arrGET);
$txMenuIncluye="";
$mostrarBotonesControlSistema=false;
$tituloModulo="";
?>

<!-- InstanceBeginEditable name="EditRegion5" -->
<?php
	$guardarConfSession=true;
	$tituloModulo="Certificados de Sello Digital (CSD)";
?>

<script type="text/javascript" src="../Scripts/base64.js"></script>
<script type="text/javascript" src="../tesoreria/Scripts/admonCertificado.js.php"></script>
<script type="text/javascript" src="../Scripts/funcionesValidacion.js"></script>
<!-- InstanceEndEditable -->

<?php



$arrValores=null;
$arrLlaves=null;
$nConfiguracion="-1";
if(($paramPOST)&&($ctPOST>0))
{
	$arrLlaves=array_keys($_POST);
	$arrValores=array_values($_POST);
}
else
{
	if(($paramGET)&&($ctGET>0))
	{
		$arrLlaves=array_keys($_GET);
		$arrValores=array_values($_GET);
	}
}


$ctParams=sizeof($arrLlaves);
$parametros='';
for($x=0;$x<$ctParams;$x++)
{
	if(gettype($arrValores[$x])=='array')
	{
		$cadAux="";
		foreach($arrValores[$x] as $v)
		{
			if($cadAux=="")
				$cadAux="'".$v."'";
			else
				$cadAux.=",'".$v."'";
		}
		$arrValores[$x]=$cadAux;
	}
	if($parametros=='')
	{
	  $parametros='"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';
	}
	else
	{
	  $parametros.=',"'.$arrLlaves[$x].'":"'.$arrValores[$x].'"';	
	}
}
if($parametros!='')
	$parametros.=',"paginaConf":"../'.$rutaNomPagina.'"';
else
	$parametros.='"paginaConf":"../'.$rutaNomPagina.'"';
$parametros='{'.$parametros.'}';
$objParametros=json_decode($parametros);
$pConfRegresar="";
$nConfRegresar="";


?>
<!-- InstanceBeginEditable name="doctitle" -->

<!-- InstanceEndEditable -->
<?php
if($guardarConfSession)
{
	if(isset($objParametros->configuracion))
	{
		$nConfiguracion=$objParametros->configuracion;
		$parametros=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];			
		$objParametros=json_decode($parametros);
		if(isset($objParametros->confReferencia))
		{
			if(isset($_SESSION["configuracionesPag"][$objParametros->confReferencia]))
			{
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
			//eliminarReferencia($nConfiguracion);
		}
	}
	else
	{
		if(isset($_SESSION["configuracionesPag"]))
		{
			$nConfiguracion=sizeof($_SESSION["configuracionesPag"])-1;
			
			$ultimaConf=$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"];
			if($ultimaConf!=$parametros)
				$nConfiguracion++;
		}
		else
			$nConfiguracion=0;
		$_SESSION["configuracionesPag"][$nConfiguracion]["parametros"]=$parametros;
		$_SESSION["configuracionesPag"][$nConfiguracion]["tituloModulo"]=$tituloModulo;
		if(isset($objParametros->confReferencia))
		{
			if($objParametros->confReferencia!="-1")
			{
				$_SESSION["configuracionesPag"][$nConfiguracion]["referencia"]=$objParametros->confReferencia;
				$configuracionAux=$_SESSION["configuracionesPag"][$objParametros->confReferencia]["parametros"];
				$objAux=json_decode($configuracionAux);
				$pConfRegresar=$objAux->paginaConf;
				$nConfRegresar=$objParametros->confReferencia;
				$pagRegresar="javascript:regresarPagina()";
			}
		}
	}
	
	
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
}
else
{
	if(($logSistemaAccesoPaginas)&&(isset($_SESSION["idUsr"])))
	{
		$parametros="";
		if($ctPOST>0)
		{
			$aLlaves=array_keys($_POST);
			$aValores=array_values($_POST);
			for($nCtParam=0;$nCtParam<$ctPOST;$nCtParam++)
			{
				$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
			}
		}
		else
		{
			if($ctGET>0)
			{
				$aLlaves=array_keys($_GET);
				$aValores=array_values($_GET);
				for($nCtParam=0;$nCtParam<$ctGET;$nCtParam++)
				{
					$parametros.="&".$aLlaves[$nCtParam]."=".$aValores[$nCtParam];
				}
			}
		}
		guardarBitacoraAccesoPagina($rutaNomPagina,$parametros);
	}
}
?>


<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->

<?php 
$tipoConsult="";
$permisosArray=array();
if($procesoName!="")
{
	$consulta="select permisos from 942_funcionesRoles where proceso='".$procesoName."' and rol in (".$_SESSION["idRol"].")";
	$procesoResult=$con->obtenerFilas($consulta);
	while($procesoRow=mysql_fetch_row($procesoResult))
	{
		$permisos=$procesoRow[0];
		$arrPermisosArray=explode("_",$permisos);
		$ctPermisos=sizeof($arrPermisosArray);
		for($x=0;$x<$ctPermisos;$x++)
		{
			$permisosArray[$arrPermisosArray[$x]]=true;
		}
	}
}


$configuracion="";
if(isset($objParametros->configuracion))
	$configuracion=$objParametros->configuracion;
$cPagina="";
$iFrame=false;
if(isset($objParametros->cPagina))
	$cPagina=$objParametros->cPagina;

if(isset($objParametros->iFrame))
	$iFrame=$objParametros->iFrame;

$arrParam=array();
	
if($cPagina!="")	
{
	$arrConf=explode("|",$cPagina);
	$nConf=sizeof($arrConf);
	for($x=0;$x<$nConf;$x++)
	{
		$arrDatosP=explode("=",$arrConf[$x]);
		$arrParam[$arrDatosP[0]]=$arrDatosP[1];
	}
	if(isset($arrParam["b"]))
		$mostrarTitulo=false;
	if(isset($arrParam["mI"]))
		$mostrarMenuIzq=false;
	if(isset($arrParam["mnu1"]))
		$mostrarMenuNivel1=false;
	if(isset($arrParam["mnu2"]))
		$mostrarMenuNivel2=false;
	if(isset($arrParam["mR1"]))
		$mostrarRegresar=false;
	if(isset($arrParam["mR2"]))
		$mostrarRegresarBajo=true;
	if(isset($arrParam["mPie"]))
		$mostrarPiePag=false;
	if(isset($arrParam["sFrm"]))
		$soloContenido=true;
	if(isset($arrParam["gConfS"]))
		$guardarConfSession=false;
	
}

$consulta="SELECT idIdioma,idioma,imagen FROM 8002_idiomas ORDER BY idioma";
$res=$con->obtenerFilas($consulta);
?>
<title><?php echo $tituloPagina ?></title>
<script language="javascript">
	function enviar(lenguaje)
	{
		gE('leng').value=lenguaje;
		gE('formLenguaje').submit();
	}
	
	function regresarPagina()
	{
		if(gE('configuracionRegresar').value!='')
			gE('frmRegresar').submit();
		else
			recargarPagina();
	}
	
	function recargarPagina()
	{
		gE('frmRefrescarPagina').submit();
	}
	
</script>
<?php
	if($soloContenido)
	{
?>
	<style>
	#main_content
	{
		padding: 0px !important;
	}
	.p15
	{
		padding: 0px !important;
	}
	#example_content
	{
		padding: 0px !important;
		margin-bottom: 0px !important;
	}
	</style>
<?php		
	}
?>
</head>

<?php
		$main_single="main_single";
		$wrapper="wrapper";
		$main_hayas="main_hayas";
		$bgColor="";
		if($soloContenido)
		{
			$main_single="";
			$wrapper="";
			$main_hayas="";
			$bgColor='style=" background:#FFFFFF !important"';
		}
		
	?>

<body <?php echo $bgColor ?>>
<script type="text/javascript" src="../Scripts/funcionesUtiles.js.php"></script>
<?php
	if(!$soloContenido)
	{
?>
<div id="main_title">
  <div class="wrapper">
  <?php
  	if($mostrarTitulo)
	{
		echo $banner; 
	} 
	?>
    <div class="transparencia" >
    	<table >
        	<tr height="25">
            	<td >
                	<?php
					if(!esUsuarioLog()&&$mostrarBotonesControlSistema)
					{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:ingresarSistema()"><img src="../images/botonIngreso.png"  /></a><a href="javascript:mostrarVentanaDuda()"><img src="../images/botonSoporte.png"  /></a>
                                </td>
                                <td align="left">
	                                
                                </td>
                            </tr>
                        </table>
                    <?php
					}
					else
					{
						if($mostrarBotonesControlSistema)
						{
					?>
                    	<table class="transparencia2">
                        	<tr>
                            	<td align="right">
			                		<a href="javascript:cerrarSesion()"><img src="../images/botonSalir.png" /></a>
                                </td>
                                <td align="left">
	                               <!-- &nbsp;<a href="javascript:cerrarSesion()">Cerrar sesión</a>-->
                                </td>
                            </tr>
                        </table>
						
					<?php
						}
					}
					
                    ?>
                </td>
                <td>
                </td>
            </tr>
       </table>
    </div>
  </div>
</div>
	
	<div id="navigation_hayas">
	<div class="wrapper_hayas">
    	
    	<div class="links_hayas">
		<ul class="tabs_hayas"  style="z-index:200 !important">
			<?php 
				
				if($mostrarMenuNivel1)
				{
					
					genearOpcionesMenusPrincipal($nomPagina,1,$idEstiloMenu);
				}
				
			?>
		</ul>
		<div class="clearer">&nbsp;</div>
		</div>

	</div>
	</div>
	<div id="subnavigation_hayas">
	<div class="wrapper">
		<div class="content">
			<div class="links">
            	<ul class="menu2" >
			<?php
				if($mostrarMenuNivel2)
				{
					genearOpcionesMenusPrincipal($nomPagina,2,$idEstiloMenu);
				}
			
			?>
            	</ul>
			<div class="clearerx">&nbsp;</div>
		  </div>
	  </div>
	</div>
</div>
<?php
	}
?>
	

	<div id="<?php echo $main_hayas ?>">
	<div class="<?php echo $wrapper ?>">

		<div id="main_content">		
		<div id="<?php echo $main_single ?>" class="p15">
		<div > 
			  <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
               
			    <tr>    
					<?php  
					if((($mostrarMenuIzq)&&(!$soloContenido))||((isset($arrParam["mI"]))&&($arrParam["mI"]=='true')))
					{
					?>	
                    <td valign="top" style="width:<?php echo $tamColumIzq?>px" id="tdMenuIzq">
					
               		<div id="example_content" style="width:<?php echo $tamColumIzq?>px">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                        	<?php
                            genearOpcionesMenusPrincipal($nomPagina,3,$idEstiloMenu);
                            ?>
						</td>
						
                      </tr>
					  <tr>
					  <td>
					 
					  <!-- InstanceBeginEditable name="menu_left2" -->
					  
					  <!-- InstanceEndEditable -->
					  </td>
					  </tr>
                    </table>
					 </div>
                
                     </td>
					 <?php
					 }
					 ?>
                  <td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
                  <td width="81%" bgcolor="#FFFFFF" valign="top">
				  <div id="example_content">  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  	<td align="right">
						<table width="100%">
						<tr>
						<td>
							<?php 	
									if(!$ocultarRegresar)
									{

										if((($mostrarRegresar)&&(!$soloContenido))||((isset($arrParam["mR1"]))&&($arrParam["mR1"]=='true')))
										{
								?> 
											<table align="left" id="tblRegresar1">
											<tr>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img width="24" height="24" src="../images/flechaizq.gif" border="0" /></a>
											</td>
											<td>
											<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
											</td>
											</tr>
											</table>
											<br />
								<?php 
										}
									}
									if($respetarEspacioRegresar)
										echo "<br><br>";
									echo "<input type=\"hidden\" value=\"".$_SESSION["leng"]."\" id=\"hLeng\"> ";
									
							?>
						</td>
						</tr>
						</table>
					</td>
				</tr>
				  <!-- InstanceBeginEditable name="content2" --> 
                   <tr>
                        <td align="center">
                        <?php
							$idCertificado=-1;
							if(isset($objParametros->idCertificado))
								$idCertificado=$objParametros->idCertificado;
								
								
							if($idCertificado!=-1)		
							{
								$tituloModulo="Administración de Series";
	                        	echo formatearTituloPagina($tituloModulo);
							}
							else
	                        	echo formatearTituloPagina($tituloModulo,true,$idCertificado);
							
							$codigoUnidad="";
							if(isset($objParametros->codigoUnidad))
								$codigoUnidad=$objParametros->codigoUnidad;
							
							$idFormulario="-1";
							if(isset($objParametros->idFormulario))
								$idFormulario=$objParametros->idFormulario;
								
							$idReferencia="-1";
							if(isset($objParametros->idReferencia))
								$idReferencia=$objParametros->idReferencia;
							
							$error=0;
							if(isset($objParametros->err))
								$error=$objParametros->err;
							
							
							
							$consulta="SELECT idSerieCertificado,serie,folioInicial,folioActual,0 AS  noRegistros FROM 688_seriesCertificados WHERE idCertificado=".$idCertificado." ORDER BY serie";
							$arrSeries=$con->obtenerFilasArreglo($consulta);

							
                        ?>
                        	<table width="730">
                            	<tr>
                                	<td align="left">
                                    	<?php
											if($idCertificado==-1)
											{
										
												if($error!=0)
												{
													$etiqueta="";
													switch($error)
													{
														case 1:
															$etiqueta="NO es un archivo de Certificado de Sello Digital v&aacute;lido (CSD)";
														break;
														case 2:
															$etiqueta="La contraseña de su Clave Privada no coincide con su Clave Privada del Certificado de Sello Digital (CSD)";
														break;
														case 3:
															$etiqueta="La vigencia del Certificado de Sello Digital (CSD) ingresado ya expir&oacute;";
														break;
														case 4:
															$etiqueta="El Certificado de Sello Digital (CSD) ingresado ya ha sido registrado previamente";
														break;	
														case 5:
															$etiqueta="La Clave Privada (archivo .key) NO corresponde con el Certificado de Sello Digital (CSD)";
														break;	
													}	
													
													
											?>
												<br />
												<span id="tblLeyenda" class="error" style="color: #D8000C;background-color: #FFBABA;background-image: url('../Scripts/mensajesNotificacion/images/error.png'); border:none;font-family:Arial, Helvetica, sans-serif; 
												    font-size:13px;      margin: 10px 0px;    padding:15px 30px 15px 50px;    background-repeat: no-repeat;    background-position: 10px center;">
													<?php 
														echo $etiqueta;
													?>
												</span>
                                                <br />
                                                <br />
                                                <br />

											<?php
													
												}
											?>
                                        	
                                    	<fieldset class="frameHijoV3">
                                        
                                        
                                        
                                    	<table>
                                        <tr>
                                        	<td align="left">
                                        		<form method="post" action="../paginasFunciones/guardarDatosCertificado.php" id="frmEnvio" enctype="multipart/form-data">
                                                <table>
                                                    <tr height="21">
                                                        <td width="180"><span class="letraAzulSimple">Certificado (.cer):</span> <span style="color:#F00">*</span></td>
                                                        <td width="550"><input type="file"  name="archivoCer" id="archivoCer" val="obl" campo="Certificado (.cer)" /></td>
                                                    </tr>
                                                    <tr>
                                                    	<td colspan="2">
                                                        <span class="letraAzulSimple" style="color:rgb(9, 58, 2);">La extensión del certificado del CSD (Certificado de Sello Digital) emitido por el SAT es: ".cer"</span>
                                                        </td>
                                                    </tr>
                                                    <tr height="13">
                                                    	<td colspan="10">
                                                        </td>
                                                    </tr>
                                                   <tr height="21">
                                                        <td ><span class="letraAzulSimple">Clave Privada (.key):</span> <span style="color:#F00">*</span></td>
                                                        <td ><input type="file"  name="archivoKey" id="archivoKey"  val="obl" campo="Clave Privada (.key)" /></td>
                                                    </tr>
                                                    <tr>
                                                    	<td colspan="2">
                                                        <span class="letraAzulSimple" style="color:rgb(9, 58, 2);">Seleccione por favor su Clave Privada del Certificado de Sello Digital. La extensión de este archivo es ".key"</span>
                                                        </td>
                                                    </tr>
                                                    <tr height="13">
                                                    	<td colspan="10">
                                                        </td>
                                                    </tr>
                                                    <tr height="21">
                                                        <td ><span class="letraAzulSimple">Contraseña de clave privada:</span> <span style="color:#F00">*</span></td>
                                                        <td ><input type="password" size="25" name="contrasena" id="contrasena" value="" val="obl" campo="Contraseña de clave privada" /></td>
                                                    </tr>
                                                    <tr >
                                                    	<td colspan="2">
                                                        <span class="letraAzulSimple" style="color:rgb(9, 58, 2);">Escriba por favor la contraseña de su clave privada (Registrada en el SAT)</span>
                                                        </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr height="21">
                                                        <td ><span class="letraAzulSimple">Confirme la Contraseña:</span> <span style="color:#F00">*</span></td>
                                                        <td ><input type="password" size="25" name="contrasena2" id="contrasena2" value="" val="obl" campo="Confirmación de contraseña de clave privada" /></td>
                                                    </tr>
                                                    
                                                </table>
                                                <input type="hidden" name="codigoUnidad" value="<?php echo $codigoUnidad?>" />
                                                <input type="hidden" name="idFormulario" value="<?php echo $idFormulario?>" />
                                                <input type="hidden" name="idReferencia" value="<?php echo $idReferencia?>" />
                                                <input type="hidden" name="idResponsable" value="<?php echo $_SESSION["idUsr"]?>" />
                                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                                
                                                </form>
                                            </td>
                                       	</tr>
                                        </table>
                                        </fieldset>
                                        <?php
											}
											else
											{
												
												$consulta="SELECT noCertificado,fechaInicioVigencia,fechaFinVigencia FROM 687_certificadosSelloDigital WHERE idCertificado=".$idCertificado;
												$fCertificado=$con->obtenerPrimeraFila($consulta);
												
										?>
                                        	<fieldset class="frameHijoV3">
                                            <table>
                                       		<tr>
                                        	<td align="left">
                                        		
                                                <table>
                                                    <tr height="21">
                                                        <td width="130"><span class="letraAzulSimple">No. de Certificado:</span> <span style="color:#F00"></span></td>
                                                        <td width="450"><span class="letraExt"><b><?php echo $fCertificado[0]?></b></span></td>
                                                    </tr>
                                                    
                                                    <tr height="21">
                                                        <td ><span class="letraAzulSimple">Vigencia:</span> <span style="color:#F00"></span></td>
                                                        <td ><span class="letraExt"><?php echo "Del <b>".date("d/m/Y H:i:s",strtotime($fCertificado[1]))."</b> al <b>".date("d/m/Y H:i:s",strtotime($fCertificado[2])) ?></b></span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            </tr>
                                            </table>
                                            </fieldset>
                                        <?php	
											}
										?>
                                        <br />
                                        <?php
											if($idCertificado!=-1)
											{
										?>
                                        <fieldset class="frameHijoV3"><legend>Series</legend>
                                        <table width="100%">
                                        <tr>
                                        	<td align="center">
                                            	<table>
                                                <tr>
                                                	<td align="left">
                                                        <table>
                                                            <tr height="21">
                                                                <td width="630">
                                                                    <span id="tblSeries"></span>
                                                                </td>
                
                                                            </tr>
                                                        </table>
                                                   </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </table><br /><br />
                                        </fieldset>
                                        <?php
											}
										?>
                                    </td>
                                </tr>
                                <tr>
                                  <td align="center" colspan="2">	
                                  <?php
								  	if($idCertificado==-1)
									{
								  ?>
                                  <br />
                                  <input type="button" class="btnAceptar" value="Aceptar" onclick="validarFrm()" />
                                  <input type="button" class="btnCancelar" value="Cancelar" onclick="cancelarOperacion()" />
                                  <?php
									}
									else
									{
								  ?>
                                  
                                  <input type="button" class="btnAceptar" value="Aceptar" onclick="regresarPagina()" />
                                  <?php
									}
								  ?>
                                  <input type="hidden" id="idCertificado" value="<?php echo $idCertificado?>" />
                                  <input type="hidden" id="arrSeries" value="<?php echo bE($arrSeries)?>" />
                                  </td>
                              </tr>
                            </table>
                        </td>
                   </tr>
				  <!-- InstanceEndEditable -->
				  <tr>
				  <td><br />
                  <?php
						if(!$ocultarFormulariosEnvio)
						{
				  ?>
                            <form method="post"	action="" id='frmEnvioDatos'>
                                <input type="hidden" name="confReferencia" value="<?php echo $nConfiguracion ?>" />
                                
                                <?php
									if($soloContenido)
									{
								?>
                                 <input type="hidden" name="cPagina" value="sFrm=true|mR1=true" />
                                <?php
									}
								?>
                                
                            </form>
                            <form method="post"	action="<?php echo $pConfRegresar?>" id='frmRegresar'>
                                <input type="hidden" name="configuracion" id="configuracionRegresar" value="<?php echo $nConfRegresar ?>" />
                            </form>
                            <form method="post"	action="<?php echo "../".$rutaNomPagina ?>" id='frmRefrescarPagina'>
                                <input type="hidden" name="configuracion" value="<?php echo $nConfiguracion ?>" />
                            </form>    
                        
				  	<?php 	
						}
					
							if(($mostrarRegresarBajo)&&(!$soloContenido))
							{
							?> 
							<table align="left" id="tblRegresar2">
							<tr>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde"><img src="../images/flechaizq.gif" border="0" /></a>
							</td>
							<td>
							<a href="<?php echo $pagRegresar ?>" class="letraVerde">&nbsp;&nbsp;<?php echo $et["regresar"] ?></a>
							</td>
							</tr>
							</table>
							<?php 
									}
							?>
				  </td>
				  </tr> 
				   </table>
				 </div>
				  </td>
				
                </tr>
              </table>
			  <div class="clearer">&nbsp;</div>
		 


			<div class="clearer">&nbsp;</div>
		</div>
	</div>
</div>
<?php
if(($mostrarPiePag)&&(!$soloContenido))
{
?>
<div id="footer">
	<div class="wrapper">
		<div class="content">

			<table width="100%">
            	<tr>
                	<td width="33%" align="left">
                        <span class="small">
                        <?php
                            echo $textoInfIzq;
                        ?>
                        </span>
                    </td>
                    <td width="34%"></td>
                    <td width="33%" align="right">
                    	<span class="small">
						<?php
                            echo $textoInfDer;
                        ?>
                        </span>
                    </td>
                    
                </tr>
            </table>
	  </div>
	</div>
</div>
<?php
}
?>
</body>
<!-- InstanceEnd --></html>
