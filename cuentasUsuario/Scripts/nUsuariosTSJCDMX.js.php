<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

var keyMap = new Ext.KeyMap(document, 
									{
										key: 13, 
										fn: guardarUsuario,
										scope: this
									}
							);


var keyMap2 = new Ext.KeyMap(document, 
									{
										key: 27, 
										fn: cancelar,
										scope: this
									}
							);

Ext.onReady(Inicializar);

function Inicializar()
{
	gE('Contrasena').focus();
}

function guardarUsuario(form)
{
	
	var pwd1=gE('Contrasena');
	var pwd2=gE('Contrasena2');
	if(pwd1.value!=pwd2.value)
	{
		function ponerFoco()
		{
			pwd1.focus();
		}
		msgBox('Las contrase&ntilde;as introducidas son diferentes',ponerFoco);
		return;
	}
    
    if(pwd1.value==bD(gE('versionSistema').value))
    {
        function resp2()
        {
            gE('pwd1').focus();
        }
        msgBox('Por seguridad es necesario que cambie sus contrase&ntilde;a de acceso',resp2);
        return;
    }
    
    if(pwd1.value.length<8)
    {
    	function respAux()
        {
        	pwd1.focus();
        }
        msgBox('La contrase&ntilde;a debe ser de almenos 8 caracteres',respAux);
        return;
    }
    
    
    var x;
    
    tNumerico=0;
    tAlfaNumerico=0;
    for(x=0;x<pwd1.value.length;x++)
    {
    	if((pwd1.value[x]>='0')&&(pwd1.value[x]<='9'))
        	tNumerico++;
         else
         	tAlfaNumerico++;
            
    }
    
    if(tNumerico==0)
    {
    	function respAux2()
        {
        	pwd1.focus();
        }
        msgBox('La contrase&ntilde;a debe contener almenos 1 caracter num&eacute;rico',respAux2);
        return;
    }
    
    if(tAlfaNumerico==0)
    {
    	function respAux3()
        {
        	pwd1.focus();
        }
        msgBox('La contrase&ntilde;a debe contener almenos 1 caracter alfanum&eacute;rico',respAux3);
        return;
    }


	
	if (!validarFormularios(form))
	{
		return;
	}
	else
	{
		var formulario=gE(form);
        gE('pContrasena').value=AES_Encrypt(gE('Contrasena').value);
        
		formulario.submit();
	}
}

function cancelar()
{
	function resp(btn)
	{
		if(btn=='yes')
		{
			cerrarSesion();	
		}
	}
	msgConfirm('Est&aacute; seguro de querer cancelar la actualizaci&oacute;n de su cuenta?',resp)
}


function cerrarSesionPrincipal()
{
	function procResp()
	{
    	
		document.location.href="<?php echo $paginaCierreLogin?>";		
        
	}
	obtenerDatosWebV2('../paginasFunciones/funciones.php',procResp,'POST','funcion=2',true);
	
}