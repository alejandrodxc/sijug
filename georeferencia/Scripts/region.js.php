<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT cveEstado,estado FROM 820_estadosV2 ORDER BY estado";
	$arrEstados=$con->obtenerFilasArreglo($consulta);
	
?>

var regLocalidad=null;

var arrEstados=<?php echo $arrEstados?>;

Ext.onReady(inicializar);

function inicializar()
{
	regLocalidad=crearRegistro	(
									[
										{name:'cveLocalidad'},
		                                {name: 'estado'},
                                        {name:'municipio'},
                                        {name: 'localidad'}
                                    ]
								);
	crearGridLocalidades();
}

function crearGridLocalidades()
{
	var lector= new Ext.data.ArrayReader({
                                            
                                            
                                            fields: [
                                               			{name:'cveLocalidad'},
		                                                {name: 'estado'},
		                                                {name:'municipio'},
                                                        {name: 'localidad'}
                                            		]
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                data:eval(bD(gE('arrLocalidades').value)),
                                                groupField: 'estado',
                                                remoteGroup:false,
                                                remoteSort: false,
                                                autoLoad:false
                                                
                                            }) 
	
       var chkRow=new Ext.grid.CheckboxSelectionModel();
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                           
                                                            chkRow,
                                                            {
                                                                header:'Estado',
                                                                width:160,
                                                                sortable:true,
                                                                dataIndex:'estado'
                                                            },
                                                            {
                                                                header:'Municipio',
                                                                width:200,
                                                                sortable:true,
                                                                dataIndex:'municipio'
                                                            },
                                                            {
                                                                header:'Localidad',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'localidad'
                                                            }
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridLocalidades',
                                                                store:alDatos,
                                                                width:670,
                                                                frame:false,
                                                                height:250,
                                                                sm:chkRow,
                                                                cm: cModelo,
                                                                stripeRows :true,
                                                                loadMask:true,
                                                                columnLines : true,
                                                                renderTo:'tblLocalidades',
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :false,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                }),
                                                                                                
                                                                                                
                                                            	 tbar:	[
                                                                            {
                                                                                icon:'../images/add.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Agregar Localidad',
                                                                                handler:function()
                                                                                        {
                                                                                         	mostrarVentanaAgregarLocalidad();   
                                                                                        }
                                                                                
                                                                            },'-',
                                                                            {
                                                                                icon:'../images/delete.png',
                                                                                cls:'x-btn-text-icon',
                                                                                text:'Remover Localidad',
                                                                                handler:function()
                                                                                        {
                                                                                            var filas=tblGrid.getSelectionModel().getSelections();
                                                                                            if(filas.length==0)
                                                                                            {
                                                                                            	msgBox('Debe seleccionar las localidades que desea remover');
                                                                                            	return;
                                                                                            }
                                                                                            function resp(btn)
                                                                                            {
                                                                                            	if(btn=='yes')
                                                                                                {
                                                                                                	tblGrid.getStore().remove(filas);
                                                                                                }
                                                                                            }
                                                                                            msgConfirm('Est&aacute; seguro de querer remover las localidades seleccionadas?',resp);
                                                                                        }
                                                                                
                                                                            }
                                                                            
                                                                        ]                                    
                                                            }
                                                        );
        return 	tblGrid;	

}

function mostrarVentanaAgregarLocalidad()
{
	var cmbEstado=crearComboExt('cmbEstado',arrEstados,110,5,250);
    cmbEstado.on('select',function(cmb,registro)
    						{
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                    	gEx('cmbMunicipio').reset();
                                        gEx('cmbMunicipio').getStore().loadData(eval(arrResp[1]));
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesEspeciales.php',funcAjax, 'POST','funcion=22&estado='+registro.get('id'),true);
                            }
                  )
    var cmbMunicipio=crearComboExt('cmbMunicipio',[],110,35,350);
    cmbMunicipio.on('select',function(cmb,registro)
    						{
                            	function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                    	
                                        gEx('gridLocalidadesAdd').getStore().loadData(eval(arrResp[1]));
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesEspeciales.php',funcAjax, 'POST','funcion=23&municipio='+registro.get('id'),true);
                            }
                  )
                  
                  
	var gridLocalidadesAdd=crearGridLocalidadesAdd();                  
                  
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
														
                                                        {
                                                        	x:10,
                                                            y:10,
                                                            html:'Estado:'
                                                        },
                                                        cmbEstado,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Municipio:'
                                                        },
                                                        cmbMunicipio,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'Seleccione las localidades que desea agregar:'
                                                        },
                                                        gridLocalidadesAdd

													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
										title: 'Agregar Localidad',
										width: 500,
										height:450,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
                                                            
															handler: function()
																	{
																		var arrLoc=gridLocalidadesAdd.getSelectionModel().getSelections();
                                                                        if(arrLoc.length==0)
                                                                        {
                                                                        	msgBox('Debe seleccionar las localidades que desea agregar');
                                                                            return;
                                                                        }
                                                                        
                                                                        var x;
                                                                        var fila;
                                                                        var pos;
                                                                        for(x=0;x<arrLoc.length;x++)
                                                                        {
                                                                        	fila=arrLoc[x];
                                                                            pos=obtenerPosFila(gEx('gridLocalidades').getStore(),'cveLocalidad',fila.get('cveLocalidad'));
                                                                            if(pos==-1)
                                                                            {
                                                                            	var r=new regLocalidad	(			
                                                                                							{
                                                                                                            	cveLocalidad:fila.data.cveLocalidad,
                                                                                                                estado: cmbEstado.getRawValue(),
                                                                                                                municipio:cmbMunicipio.getRawValue(),
                                                                                                                localidad: fila.data.localidad
                                                                                                            }
                                                                                						)
                                                                            	gEx('gridLocalidades').getStore().add(r);
                                                                            }
                                                                            
                                                                        }
                                                                        ventanaAM.close();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function crearGridLocalidadesAdd()
{
	var dsDatos=[];
    var alDatos=	new Ext.data.SimpleStore	(
                                                {
                                                    fields:	[
                                                                {name: 'cveLocalidad'},
                                                                {name: 'localidad'}
                                                            ]
                                                }
                                            );

    alDatos.loadData(dsDatos);
	var chkRow=new Ext.grid.CheckboxSelectionModel();
	
	var cModelo= new Ext.grid.ColumnModel   	(
												 	[
													 	new  Ext.grid.RowNumberer(),
														chkRow,
														{
															header:'Localidad',
															width:350,
															sortable:true,
															dataIndex:'localidad'
														}
													]
												);
                                                
	var tblGrid=	new Ext.grid.EditorGridPanel	(
                                                        {
                                                            id:'gridLocalidadesAdd',
                                                            store:alDatos,
                                                            frame:true,
                                                            y:100,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            stripeRows :true,
                                                            columnLines : true,
                                                            height:260,
                                                            width:450,
                                                            sm:chkRow
                                                        }
                                                    );
	return 	tblGrid;	
}


function validarFrm()
{
	if(validarFormularios('frmEnvio'))
    {
    	var x;
    	var id=gE('id').value;
        var gridLocalidades=gEx('gridLocalidades');
        var fila;
        var cadCategoria='';
        var obj='';
        for(x=0;x<gridLocalidades.getStore().getCount();x++)
        {
        	fila=gridLocalidades.getStore().getAt(x);
            obj='{"cveLocalidad":"'+fila.get('cveLocalidad')+'"}';
            if(cadCategoria=='')
            	cadCategoria=obj;
            else
            	cadCategoria+=','+obj;
        }
       
        var objArr='{"arrLocalidades":['+cadCategoria+']}';
        
        if(id=='-1')
        {
        	gE('funcPHPEjecutarNuevo').value=bE('asociarLocalidadesZona(@idRegPadre,\''+bE(objArr)+'\')');
        }
        else
        {
        	gE('funcPHPEjecutarModif').value=bE('asociarLocalidadesZona('+id+',\''+bE(objArr)+'\')');
        }
    	
    	gE('frmEnvio').submit();
    }
}