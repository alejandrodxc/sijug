<?php  session_start();

	include("latis/conexionBD.php");
	include_once("latis/PHPWord.php");
	include_once("latis/zip.lib.php"); 
	include_once("latis/cCodigoBarras.php");

	

	
	$cadObj="";bD("eyJjb2RpZ29HZW5lcmFkbyI6IjI5NzctMjkyOC01ODY2LTY0NjkiLCJ0aXBvQXVkaWVuY2lhIjoiQXVkaWVuY2lhIGRlIEFjY2nDs24gUGVuYWwgUHJpdmFkYSIsInVuaWRhZEdlc3Rpb24iOiJVTklEQUQgREUgR0VTVEnDk04gSlVESUNJQUwgMSIsImlkRXZlbnRvIjoiMzQ5OTUyIiwiZmVjaGFBdWRpZW5jaWEiOiIyMDIwLTEwLTI0IDEwOjMwIn0=");
	if(isset($_POST["cadObj"]))
		$cadObj=bD($_POST["cadObj"]);
	
	$obj=json_decode($cadObj);

	$consulta="SELECT * FROM 7006_usuariosVSAudienciasCodigoGenerado WHERE codigoGenerado='".$obj->codigoGenerado."' and idEvento=".$obj->idEvento;
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);	

	$fDatosServidor=obtenerURLComunicacionServidorMateria($fRegistro["cveMateria"]);
	
	/*$cBarras=new cCodigoBarras("http://".$fDatosServidor[0].":".$fDatosServidor[1].
							"/modulosEspeciales_SGJP/validarCodigoAccesoAudienciaVirtual.php?codigo=".$obj->codigoGenerado.
							"&idEvento=".$obj->idEvento,"QR","",1,8,60);*/
							
	$cBarras=new cCodigoBarras($urlSitioDNS."/modulosEspeciales_SICORE/validarCodigoAccesoAudienciaVirtual.php?codigo=".$obj->codigoGenerado.
							"&idEvento=".$obj->idEvento,"QR","",1,8,60);
														
	$nombreArchivoCBB=$cBarras->generarCodigoBarrasImagenArchivo();
	$urlArchivoCBB=$baseDir."/archivosTemporalesCodigoBarras/".$nombreArchivoCBB;
	
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate($baseDir.'\\modulosEspeciales_SICORE\\plantillas\\formatoCodigoAudienciaVirtual.docx');	
	
	

	$rValores=array();
	$fechaAudiencia=strtotime($obj->fechaAudiencia);
	$fechaActual=strtotime($fRegistro["fechaGeneracion"]);
	$arrValores["hora"]=date("H:i",$fechaActual);
	$arrValores["dia"]=date("d",$fechaActual);
	$arrValores["mes"]=$arrMesLetra[(date("m",$fechaActual)*1)-1];
	$arrValores["anio"]=date("Y",$fechaActual);
	$arrValores["carpetaAdministrativa"]=$fRegistro["carpetaAdministrativa"];
	$arrValores["tipoAudiencia"]=$obj->tipoAudiencia;
	$arrValores["fechaAudiencia"]=date("d/m/Y",$fechaAudiencia);
	$arrValores["horaAudiencia"]=date("H:i",$fechaAudiencia);
	$arrValores["unidadGestion"]=$obj->unidadGestion;
	$arrValores["codigoAcceso"]=$obj->codigoGenerado;
	$arrValores["nombreSolicitante"]=obtenerNombreUsuario($fRegistro["idUsuario"]);
	foreach($arrValores as $llave=>$valor)
	{
		$document->setValue("[".$llave."]",utf8_decode(urldecode($valor)));	
	}
	
	$nombreAleatorio=generarNombreArchivoTemporal();
	$nomArchivo=$nombreAleatorio.".docx";
	$document->save($nomArchivo);
	$objZip=new ZipArchive();
		
		
	if($objZip->open($nomArchivo))
	{
		$objZip->addFile($urlArchivoCBB,"word/media/image1.png");

		$objZip->close();
	}
	
	$nombreFinal=str_replace(".docx",".pdf",$nomArchivo);
	generarDocumentoPDF($nomArchivo,false,false,false,$nombreFinal,"MS_OFFICE","./");

	if($utilizarServidorQR)
	{
		$consulta="SELECT unidad FROM 817_organigrama WHERE codigoUnidad='".$_SESSION["codigoInstitucion"]."'";
		$areaEmisora=$con->obtenerValor($consulta);
		
		$consulta="SELECT Nombre FROM 800_usuarios WHERE idUsuario=".$_SESSION["idUsr"];
		$usuarioEmisor=$con->obtenerValor($consulta);
		
		$objDocumentoPDF=array();
		$objDocumentoPDF["areaEmisora"]=$areaEmisora;
		$objDocumentoPDF["usuarioEmisor"]=$usuarioEmisor;
		$objDocumentoPDF["documentoPDF"]=bE(leerContenidoArchivo($nombreFinal));
		$objDocumentoPDF["nombreDocumento"]=$nombreFinal;
		$objDocumentoPDF["fechaDocumento"]=date("Y-m-d");
		$objDocumentoPDF["posX"]=182;
		
		$objDocumentoPDF["posY"]=260;
	
		$respuestaResultado=generarCodigoQRPDF($objDocumentoPDF);
		
		unlink($urlArchivoCBB);
		unlink($nombreFinal);
		if($respuestaResultado["estatus"]==1)
		{
			escribirContenidoArchivo($nombreFinal,bD($respuestaResultado["pdfSellado"]));
			
			header("Content-type:application/pdf"); 
			header("Content-length: ".filesize($nombreFinal)); 
			header("Content-Disposition: inline; filename=".$nombreFinal);
			readfile($nombreFinal);	
			
			unlink($urlArchivoCBB);
			unlink($nombreFinal);
			
		}
	}
	else
	{
			header("Content-type:application/pdf"); 
			header("Content-length: ".filesize($nombreFinal)); 
			header("Content-Disposition: inline; filename=".$nombreFinal);
			readfile($nombreFinal);	
			
			unlink($nombreFinal);
	}
?>