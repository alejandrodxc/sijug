<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT DISTINCT anioExpediente,anioExpediente FROM 7006_usuariosVSCarpetasAdministrativas WHERE idUsuario=".$_SESSION["idUsr"].
				" ORDER BY anioExpediente";
	$arrAnio=$con->obtenerFilasArreglo($consulta);
	if($arrAnio=="")
		$arrAnio="[['".date("Y")."','".date("Y")."']]";
	
	$consulta="SELECT anioExpediente FROM 7006_usuariosVSCarpetasAdministrativas WHERE idUsuario=".$_SESSION["idUsr"].
				" ORDER BY anioExpediente desc";
	$anioExpediente=$con->obtenerValor($consulta);
	if($anioExpediente=="")
		$anioExpediente=date("Y");
	
	$consulta="SELECT claveMateria,materia,promocioneFirmadas FROM _480_tablaDinamica";
	$arrMateriasPromociones=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT claveUnidad,UPPER(nombreUnidad) FROM _17_tablaDinamica";
	$arrJuzgados=$con->obtenerFilasArreglo($consulta);
	

	$consulta="SELECT materia,promocionRequiereFirma FROM _482_tablaDinamica WHERE CONCAT(rol,'_0') IN(".$_SESSION["idRol"].")";
	$arrConfPromociones=$con->obtenerFilasArreglo($consulta);
	
	

	$arrJuzgadosFamiliares["00050001"]=1;
	$arrJuzgadosFamiliares["00050002"]=1;
	$arrJuzgadosFamiliares["00050003"]=1;
	$esJuzgadoFamiliar=isset($arrJuzgadosFamiliares[$_SESSION["codigoInstitucion"]])?"true":"false";
?>
var uploadControl;
var esJuzgadoFamiliar=<?php echo $esJuzgadoFamiliar?>;
var registraPromociones=false;//<?php echo existeRol("'152_0'")?"true":"false"?>;
var registrarLAMVLV=<?php echo existeRol("'157_0'")?"true":"false"?>;
var arrConfPromociones=<?php echo $arrConfPromociones?>;
var arrTipoSeguimiento=[['1','Acuerdo'],['2','Promoci&oacute;n registrada'],['3','Solicitud LAMVLV']];
var enviarPromocion=-1;
var arrJuzgados=<?php echo $arrJuzgados?>;
var IdDocumento='';
var nombreDocumento='';
var arrMateriasPromociones=<?php echo $arrMateriasPromociones?>;
var anioExpediente='<?php echo $anioExpediente?>';
var arrAnio=<?php echo $arrAnio?>;
var arrSituacionPromocion=[['1','En Espera de Env&iacute;o a Juzgado','../images/bullet-grey.png'],['2','En Espera de Atenci\xF3n','../images/bullet-yellow.png'],['3','Atendida','../images/bullet-green.png']];
var arrSituacionLAMVLV=[['1','En Espera de Env&iacute;o a Unidad de Gesti&oacute;n','../images/bullet-grey.png'],['2','En Espera de Atenci\xF3n','../images/bullet-yellow.png'],['3','Atendida','../images/bullet-green.png']];
var arrGrids=[];
var nodoExpedienteSel=null;


var primeraCargaFramePrincipal=true;


function frameLoadPrincipal(iFrame)
{
	if(!primeraCargaFramePrincipal)
    {
    	setTimeout(function()
        			{
				    	iFrame.contentWindow.print();
                     },500)
    }
    else
    	primeraCargaFramePrincipal=false;
	
}


Ext.onReady(inicializar);

function inicializar()
{
	var arrPaneles=[];
    
    arrAnio.splice(0,0,['0','Cualquiera']);

	Ext.QuickTips.init();
	var cmbAnio=crearComboExt('cmbAnio',arrAnio,0,0,110);
    cmbAnio.setValue('0');
    
    cmbAnio.on('select',function(cmb,registro)
    					{
                        	gEx('arbolExpedientes').getRootNode().reload();
                        }
    			)
                
                
                
                
                
                
                
                
	if(registraPromociones)
    {                
    	arrGrids.push('gPromociones');
        arrPaneles.push	(
                            {
                                xtype:'panel',
                                title:'Promociones',
                                layout:'border',
                                items:	[
                                            {
                                                xtype:'panel',
                                                width:300,
                                                region:'west',
                                                layout:'border',
                                                tbar:	[
                                                            {
                                                                xtype:'label',
                                                                html:'<b>A&ntilde;o del expediente:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                
                                                            },
                                                            cmbAnio
                                                        ],
                                                items:	[
                                                                
                                                            crearArbolExpedientes()
                                                           ]
                                            },
                                            crearGridPromociones()
                                            
                                        ]
                            }
                        );                
	}
    
    if(registrarLAMVLV)
    {        
    	arrGrids.push('gSolicitudesLAMVLV');
        arrPaneles.push	(
                            {
                                xtype:'panel',
                                title:'Ley de Acceso de las Mujeres a una Vida Libre de Violencia',
                                layout:'border',
                                items:	[
                                            crearGridSolicitudesLAMVLV()
                                            
                                        ]
                            }	
                        );                	                
                    
	}                
                
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Solicitudes Registradas</b></span> <span id="lblExpediente" style="color:#000; font-weight:bold;font-size:14px"></span><span id="lblNoExpediente" style="color:#900; font-weight:bold;font-size:14px"></span>',
                                                items:	[
                                                			{
                                                            	xtype:'tabpanel',
                                                                region:'center',
                                                                activeTab:0,
                                                                items:	arrPaneles
                                                            }
                                                
                                                
                                                			
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearArbolExpedientes()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'1'
                                                                    
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesModulosEspeciales_SICORE.php'
                                                }
                                            )		
										
	cargadorArbol.on('beforeload',function(c)
    						{
                            	gEx('btnAddPromocion').disable();
                            	c.baseParams.anio=gEx('cmbAnio').getValue();
                                c.baseParams.noExpediente=gEx('txtNumeroExpediente').getValue();
                            	nodoExpedienteSel=null;
                            }
    				)	
    										
	cargadorArbol.on('load',function(c,nodoCarga)
    						{
                            	
                            }
    				)										
	var arbolExpedientes=new Ext.tree.TreePanel	(
                                                            {
                                                                id:'arbolExpedientes',
                                                                useArrows:true,
                                                                animate:true,
                                                                enableDD:false,
                                                                ddScroll:true,
                                                                containerScroll: true,
                                                                autoScroll:true,
                                                                border:false,
                                                                region:'center',
                                                                root:raiz,
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                                html:'<b>N&uacute;mero de expediente:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                                
                                                                            },
                                                                            {
                                                                            	xtype:'textfield',
                                                                                width:140,
                                                                                enableKeyEvents:true,
                                                                                id:'txtNumeroExpediente',
                                                                                listeners:	{
                                                                                				keypress:function(txt,e)
                                                                                                	{
                                                                                                    	if(e.charCode=='13')
                                                                                                        {
                                                                                                        	if(txt.ultimaBusqueda!=txt.getValue())
                                                                                                            {
                                                                                                            	gEx('arbolExpedientes').getRootNode().reload();
                                                                                                        		txt.ultimaBusqueda=txt.getValue();
                                                                                                            }
                                                                                                        }
                                                                                                    },
                                                                                                blur:function(txt)
                                                                                                	{
                                                                                                    	
                                                                                                    	if(txt.ultimaBusqueda!=txt.getValue())
                                                                                                        {
                                                                                                        	gEx('arbolExpedientes').getRootNode().reload();
                                                                                                        	txt.ultimaBusqueda=txt.getValue();
                                                                                                        }
                                                                                                        
                                                                                                    }
                                                                                			}
                                                                            }
                                                                		],
                                                                loader: cargadorArbol,
                                                                rootVisible:false
                                                            }
                                                        )
         
         
                                                    
	arbolExpedientes.on('click',funcExpediente);
	return  arbolExpedientes;
}

function funcExpediente(nodo, evento)
{
	gEx('btnAddPromocion').disable();
	nodoExpedienteSel=nodo;
    if(nodoExpedienteSel.attributes.tipo=='3')
    {
    	gEx('btnAddPromocion').enable();
        gEx('gPromociones').getStore().reload();
        gE('lblExpediente').innerHTML=' [Expediente: ';
        gE('lblNoExpediente').innerHTML=nodoExpedienteSel.attributes.expediente+', '+nodoExpedienteSel.parentNode.attributes.juzgado+'<span style="color:#000; font-weight:bold">]</span>';
    }
    else
    {
    	gEx('btnAddPromocion').disable();
        gEx('gPromociones').getStore().removeAll();
        gE('lblExpediente').innerHTML='';
        gE('lblNoExpediente').innerHTML='';
    }
    
    
    
}

function crearGridPromociones()
{
   
   var lector= new Ext.data.JsonReader({
                                        
                                        totalProperty:'numReg',
                                        fields: [
                                                    {name:'idRegistro'},
                                                    {name:'expediente'},
                                                    {name:'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                    {name: 'asunto'},
                                                    {name: 'situacionPromocion'},
                                                    {name: 'fechaRespuesta', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                   	{name: 'fechaRecepcionJuzgado', type:'date', dateFormat:'Y-m-d H:i:s'}, 
                                                    {name: 'idDocumentoPromocion'},
                                                    {name: 'nombreDocumentoPromocion'},
                                                    {name: 'idDocumentoRespuesta'},
                                                    {name: 'nombreDocumentoRespuesta'},
                                                    {name: 'juzgado'}
                                                ],
                                        root:'registros'
                                        
                                    }
                                  );
 
                                                                                  
    var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
    
                                                                                  {
    
                                                                                      url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'
    
                                                                                  }
    
                                                                              ),
                                                sortInfo: {field: 'fechaRegistro', direction: 'ASC'},
                                                groupField: 'fechaRegistro',
                                                remoteGroup:false,
                                                remoteSort: true,
                                                autoLoad:false
                                                
                                            }) 
    alDatos.on('beforeload',function(proxy)
                                    {
                                        proxy.baseParams.funcion='2';
                                        if(nodoExpedienteSel)
                                        {
                                            proxy.baseParams.iE=bE(nodoExpedienteSel.attributes.idExpediente);
                                            proxy.baseParams.nE=bE(nodoExpedienteSel.attributes.expediente);
                                            proxy.baseParams.cM=bE(nodoExpedienteSel.attributes.idMateria);
                                            proxy.baseParams.cG=bE(nodoExpedienteSel.attributes.unidadGestion);
										}                                        
                                    }
                        )   

	alDatos.on('load',function(proxy)
                                {
                                    if(enviarPromocion!=-1)
                                    {
                                    	enviarPromocionJuzgado(bE(enviarPromocion));
                                        enviarPromocion=-1;
                                    }
                                    
                                }
                    )
   
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(), 
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'fechaRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrSituacionPromocion,registro.data.situacionPromocion);
                                                                    	return '<img src="'+arrSituacionPromocion[pos][2]+'" width="20" height="20">';
                                                                    }
                                                        }, 
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	
                                                                    	return '<a href="javascript:obtenerAcuseSolicitud(\''+bE(val)+'\')"><img src="../images/download.png" title="Descargar acuse de ev&iacute;o" alt="Descargar acuse de ev&iacute;o" width="14" height="14"></a>';
                                                                    }
                                                        },                                                       
                                                        {
                                                            header:'Fecha de registro',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'fechaRegistro',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'Expediente',
                                                            width:145,
                                                            sortable:true,
                                                            dataIndex:'expediente'
                                                        },
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idDocumentoPromocion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return '<a href="javascript:mostrarDocumento(\''+bE(registro.data.idRegistro)+'\',\''+bE(1)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de promoci&oacute;n" alt="Ver documento de promoci&oacute;n"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'Asunto',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'asunto',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Situaci&oacute;n actual',
                                                            width:210,
                                                            sortable:true,
                                                            dataIndex:'situacionPromocion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(parseInt(val))
                                                                        {
                                                                        	if(val==1)
                                                                            {
                                                                            	comp+='&nbsp;&nbsp;<a href="javascript:enviarPromocionJuzgado(\''+bE(registro.data.idRegistro)+'\')"><img src="../images/document_go.png" width="14" height="14" title="Enviar promoci&oacute;n a Juzgado" alt="Enviar promoci&oacute;n a Juzgado"></a>';
                                                                            }
                                                                        }
                                                                    	return formatearValorRenderer(arrSituacionPromocion,val)+comp;
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de recepci&oacute;n<br> en juzgado',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'fechaRecepcionJuzgado',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                    		return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de respuesta',
                                                            width:130,
                                                            sortable:true,
                                                            hidden:true,
                                                            dataIndex:'fechaRespuesta',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                    		return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idDocumentoRespuesta',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(val!='')
	                                                                    	return '<a href="javascript:mostrarDocumento(\''+bE(registro.data.idRegistro)+'\',\''+bE(2)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de respuesta" alt="Ver documento de respuesta"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'Juzgado',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'juzgado',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrJuzgados,val));
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gPromociones',
                                                            //title:'Promociones registradas',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,  
                                                            tbar:	[
                                                            			
                                                            			{
                                                                        	icon:'../images/add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            //hidden:!registraPromociones,
                                                                            disabled:true,
                                                                            id:'btnAddPromocion',
                                                                            text:'Registrar Nueva Promoci&oacute;n',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaNuevaPromocion();
                                                                                    }
                                                                            
                                                                        }
                                                            		] ,                                                         
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}

function mostrarVentanaNuevaPromocion()
{
	IdDocumento='';
	var tabla='<div><input type="text" id="txtFileName" disabled="true" style="border: solid 1px; background-color: #FFFFFF; width: 290px" /></div><div class="flash" id="fsUploadProgress">'+ 
					'</div><input type="hidden" name="hidFileID" id="hidFileID" value="" /> ';  
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<b>Materia:</b>',
                                                            x:10,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblMateria" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.parentNode.attributes.materia+'</span>',
                                                            x:120,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Juzgado:</b>',
                                                            x:10,
                                                            y:40
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblJuzgado" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.attributes.juzgado+'</span>',
                                                            x:120,
                                                            y:40
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Expediente:</b>',
                                                            x:10,
                                                            y:70
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblExpediente" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.attributes.expediente+'</span>',
                                                            x:120,
                                                            y:70
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Asunto:</b>',
                                                            x:10,
                                                            y:100
                                                        },
                                                        {
                                                        	x:10,
                                                            y:130,
                                                            xtype:'textarea',
                                                            width:700,
                                                            height:60,
                                                            id:'txtAsunto'
                                                        },
                                                        
                                                        {
                                                        	x:10,
                                                            y:210,
                                                            xtype:'label',
                                                            html:'<b>Documento a promover:</b>'
                                                        },
                                                        {
                                                            x:160,
                                                            y:205,
                                                            html:	'<table width="290"><tr><td><div id="uploader"><p>Your browser doesn\'t have Flash, Silverlight or HTML5 support.</p></div></td></tr><tr id="filaAvance" style="display:none"><td align="right">Porcentaje de avance: <span id="porcentajeAvance"> 0%</span></td></tr></table>'
                                                        },
                                                       
                                                        {
                                                            x:455,
                                                            y:206,
                                                            id:'btnUploadFile',
                                                            xtype:'button',
                                                            text:'Seleccionar...',
                                                            handler:function()
                                                                    {
                                                                        $('#containerUploader').click();
                                                                    }
                                                        },
                                                        {
                                                            x:185,
                                                            y:10,
                                                            hidden:true,
                                                            html:	'<div id="containerUploader"></div>'
                                                        },
                                                        
                                                        {
                                                            x:290,
                                                            y:0,
                                                            xtype:'hidden',
                                                            id:'idArchivo'

                                                        },
                                                        {
                                                            x:290,
                                                            y:0,
                                                            xtype:'hidden',
                                                            id:'nombreArchivo'
                                                        } 
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vDocumento',
										title: 'Registrar nueva promoci&oacute;n [1.- Datos generales]',
										width: 750,
										height:340,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	gEx('txtAsunto').focus(false,500);
                                                                    
                                                                    var cObj={
                                                                    // Backend settings
                                                                                            upload_url: "../modulosEspeciales_SGJP/procesarComprobante.php", //lquevedor
                                                                                            file_post_name: "archivoEnvio",
                                                                             
                                                                                            // Flash file settings
                                                                                            file_size_limit : "1000 MB",
                                                                                            file_types : "*.pdf;*.doc;*.docx",			// or you could use something like: "*.doc;*.wpd;*.pdf",
                                                                                            file_types_description : "Todos los archivos",
                                                                                            file_upload_limit : 0,
                                                                                            file_queue_limit : 1,
                                                                             
                                                                                            
                                                                                            upload_success_handler : subidaCorrecta
                                                                                        };
                                                                    
																	crearControlUploadHTML5(cObj);
                                                                }
															}
												},
										buttons:	[
														{
															
															text: 'Siguiente >>',
                                                            
															handler: function()
																	{
                                                                    
                                                                    	if(gEx('txtAsunto').getValue()=='')
                                                                        {
                                                                        	function resp1()
                                                                            {
                                                                            	gEx('txtAsunto').focus();
                                                                            }
                                                                            msgBox('Debe indicar el asunto que promueve',resp1);
                                                                        	return;
                                                                        }
                                                                    
																		if((uploadControl.files.length==0)&&(IdDocumento==''))
                                                                        {
                                                                            msgBox('Debe ingresar el documento que desea adjuntar');
                                                                            return;
                                                                        }
                                                                        
                                                                        
                                                                        if(uploadControl.files.length>0)
                                                                        {
                                                                           uploadControl.start();
                                                                        }
                                                                        else
                                                                        {
                                                                        	gEx('vDocumento').hide();                
        																	if(gEx('vDocumento2'))
                                                                                gEx('vDocumento2').show();
                                                                            else
                                                                                mostrarVentanaNuevaPromocion2()
                                                                        }
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}


function subidaCorrecta(file, serverData) 
{


    file.id = "singlefile";	// This makes it so FileProgress only makes a single UI element, instead of one for each file
    var arrDatos=serverData.split('|');

    if ( arrDatos[0]!='1') 
    {
    	
        
    } 
    else 
    {
        
        IdDocumento=arrDatos[1];
        nombreDocumento=(arrDatos[2]);
        gEx('vDocumento').hide();         
       	if(gEx('vDocumento2'))
        {
        	gEx('vDocumento2').mostrado=false;
            gEx('vDocumento2').show();
        }
        else
	        mostrarVentanaNuevaPromocion2()
        
        
    }
		
	
}


function mostrarVentanaNuevaPromocion2()
{
	var pos=existeValorMatriz(arrMateriasPromociones,nodoExpedienteSel.attributes.idMateria);
    var solicitarFirma=arrMateriasPromociones[pos][2]=='1';
    
	pos=existeValorMatriz(arrConfPromociones,nodoExpedienteSel.attributes.idMateria);
	if(pos!=-1)
    {
	    solicitarFirma=arrConfPromociones[pos][1]=='1';
    }
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<b>Materia:</b>',
                                                            x:10,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblMateria2" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.parentNode.attributes.materia+'</span>',
                                                            x:120,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Expediente:</b>',
                                                            x:600,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblExpediente2" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.attributes.expediente+'</span>',
                                                            x:680,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Juzgado:</b>',
                                                            x:10,
                                                            y:30
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblJuzgado2" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.attributes.juzgado+'</span>',
                                                            x:120,
                                                            y:30
                                                        },
                                                        new Ext.ux.IFrameComponent({ 
        																				x:10,
                                                                                        y:50,
                                                                                        width:810,
                                                                                        height:295,
                                                                                        id: 'hSpVisor', 
                                                                                        url: '../paginasFunciones/white.php'
                                                                                })
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vDocumento2',
										title: 'Registrar nueva promoci&oacute;n [2.- Validaci&oacute;n documento de promoci&oacute;n]',
										width: 850,
										height:420,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	if(!gEx('vDocumento2').mostrado)
                                                                    {
                                                                        var arrNombreDoc=nombreDocumento.split('.');
                                                                        extensionDoc=arrNombreDoc[arrNombreDoc.length-1];
                                                                        gEx('hSpVisor').load	(
                                                                                                    {
                                                                                                        url:'../visoresGaleriaDocumentos/visorDocumentosGeneral.php',
                                                                                                        params:	{
                                                                                                                    iD:bE(IdDocumento),
                                                                                                                    extension:extensionDoc,
                                                                                                                    oBarra:1,
                                                                                                                    cPagina:'sFrm=true'
                                                                                                                }
                                                                                                    }
                                                                                                );
                                                                    
                                                                        gEx('vDocumento2').mostrado=true;
                                                                  	}
                                                                
                                                                }
															}
												},
										buttons:	[
                                        				{
															
															text: '<< Anterior',  
															handler: function()
																	{
                                                                    	gEx('vDocumento').show();
																		ventanaAM.hide();
                                                                        
																	}
														},
														{
															
															text: 'Enviar promoci&oacute;n',  
                                                            hidden:solicitarFirma,                                                          
															handler: function()
																	{
																		function resp(btn)
                                                                        {
                                                                        	if(btn=='yes')
                                                                            {
                                                                            	var cadObj='{"idDocumento":"'+IdDocumento+'","nombreDocumento":"'+cv(nombreDocumento)+
                                                                                            '","expediente":"'+nodoExpedienteSel.attributes.expediente+'","idExpediente":"'+nodoExpedienteSel.attributes.idExpediente+
                                                                                            '","asunto":"'+cv(gEx('txtAsunto').getValue())+'","cveUnidadGestion":"'+nodoExpedienteSel.attributes.unidadGestion+
                                                                                            '","cveMateria":"'+nodoExpedienteSel.attributes.idMateria+'"}';			
                                                                            
                                                                            	function funcAjax()
                                                                                {
                                                                                    var resp=peticion_http.responseText;
                                                                                    arrResp=resp.split('|');
                                                                                    if(arrResp[0]=='1')
                                                                                    {
                                                                                        gEx('vDocumento').close();
                                                                                        gEx('vDocumento2').close();
                                                                                        enviarPromocion=arrResp[1];
                                                                                        gEx('gPromociones').getStore().reload();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                                    }
                                                                                }
                                                                                obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=3&cadObj='+cadObj,true);
                                                                            
                                                                            }
                                                                        }
                                                                        msgConfirm('Est&aacute; seguro de querer enviar su promoci&oacute;n al juzgado?',resp)
																	}
														},
                                                        {
															
															text: 'Siguiente >>',  
                                                            hidden:!solicitarFirma,                                                            
															handler: function()
																	{
																		ventanaAM.hide();
                                                                        mostrarVentanaNuevaPromocion3();
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	gEx('vDocumento').close();
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function mostrarVentanaNuevaPromocion3()
{
	var arrAccionesFirma=[['1','Firmar median FIEL'],['6','Firmar median FIREL']];
	var cmbAccionFirma=crearComboExt('cmbAccionFirma',arrAccionesFirma,220,55,220);
    cmbAccionFirma.on('select',function(cmb,registro)
    							{
                                	gEx('fSetFirma').hide();
									gEx('fSetFirmaFirel').hide();
									
									//gEx('vDocumento3').setHeight(250);
                                	switch(registro.data.id)
                                    {
                                    	case '1':                                        	
											gEx('fSetFirmaFirel').hide();
											gEx('fSetFirma').show();
											gEx('vDocumento').setHeight(380);
                                        break;
                                       	case '6':                                        	
											gEx('fSetFirmaFirel').show();
											gEx('fSetFirma').hide();
											gEx('vDocumento').setHeight(380);
                                        break;
                                       
                                    }
									
								//	gEx('vDocumento').center();
                                }
    				)
    
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
                                            id:'vFormPanel',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	xtype:'label',
                                                            html:'<b>Materia:</b>',
                                                            x:10,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblMateria3" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.parentNode.attributes.materia+'</span>',
                                                            x:120,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Expediente:</b>',
                                                            x:600,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblExpediente3" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.attributes.expediente+'</span>',
                                                            x:680,
                                                            y:10
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<b>Juzgado:</b>',
                                                            x:10,
                                                            y:30
                                                        },
                                                        {
                                                        	xtype:'label',
                                                            html:'<span id="lblJuzgado3" style="color:#900; font-weight:bold;">'+nodoExpedienteSel.parentNode.attributes.juzgado+'</span>',
                                                            x:120,
                                                            y:30
                                                        },
                                                        {
                                                        	
                                                            x:10,
                                                            y:60,
                                                            html:'<b>Medio de firma de promoci&oacute;n:</b>'
                                                            
                                                        },
                                                        cmbAccionFirma,
                                                        {
                                                        	id:'fSetFirma',
                                                        	xtype:'fieldset',
                                                            width:790,
                                                            x:10,
                                                            y:90,
                                                            height:150,
															hidden:true,
                                                            defaultType: 'label',
                                                            layout:'absolute',
                                                            items:	[
                                                         				
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Ingrese su archivo de certificado digital (*.cer):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:10,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="fileCer" accept=".cer" style="width:250px">'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Ingrese su archivo de llave privada (*.key):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:40,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="fileKey" accept=".key" style="width:250px">'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:70,
                                                                            html:'Ingrese la contrase&ntilde;a de llave privada:'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:65,
                                                                            width:250,
                                                                            id:'txtPassword',
                                                                            xtype:'textfield',
                                                                            inputType:'password'
                                                                        }   
                                                            		]
                                                        },
                                                        {
                                                        	id:'fSetFirmaFirel',
                                                        	xtype:'fieldset',
                                                            width:790,
                                                            x:10,
                                                            y:90,
                                                            height:120,
															hidden:true,
                                                            defaultType: 'label',
                                                            layout:'absolute',
                                                            items:	[
                                                         				{
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Ingrese su archivo de llave privada (*.pfx):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:10,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="filePFX" accept=".pfx" style="width:250px">'
                                                                        },
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Ingrese la contrase&ntilde;a de llave privada:'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:35,
                                                                            width:250,
                                                                            id:'txtPasswordFirel',
                                                                            xtype:'textfield',
                                                                            inputType:'password'
                                                                        }   
                                                            		]
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vDocumento3',
										title: 'Registrar nueva promoci&oacute;n [3.- Firma de promoci&oacute;n]',
										width: 850,
										height:330,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	
																}
															}
												},
										buttons:	[
                                        				{
															
															text: '<< Anterior',  
                                                            handler: function()
																	{
																		ventanaAM.close();
                                                                        gEx('vDocumento2').show();
																	}
														},
														{
															
															text: 'Firmar documento',  
                                                            handler: function()
																	{
																		firmarDocumento();
																	}
														},
                                                        {
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	gEx('vDocumento').close();
                                                                        gEx('vDocumento2').close();
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function firmarDocumento()
{
	var form=gEx('vFormPanel');
	var cmbAccionFirma=gEx('cmbAccionFirma');
	if(cmbAccionFirma.getValue()=='')
    {
        function resp()
        {
            cmbAccionFirma.focus();
        }
        msgBox('Debe indicar el medio de firma de la promoci&oacute;n',resp);
        return;
    }
    var objResultado={};
    objResultado.accion=cmbAccionFirma.getValue();
    objResultado.comentarios='';
    objResultado.cadenaFirma='';
    
    var documentoFinal=1;
    
    switch(parseInt(cmbAccionFirma.getValue()))
    {
        case 1:
            if(gE('fileCer').value=='')
            {
                function resp1Cer()
                {
                    gE('fileCer').focus();
                }
                msgBox('Debe ingresar el archivo de certificado digital (*.cer)',resp1Cer);
                return;
            }
            
            if(gE('fileKey').value=='')
            {
                function resp2Cer()
                {
                    gE('fileKey').focus();
                }
                msgBox('Debe ingresar el archivo de llave privada (*.key)',resp2Cer);
                return;
            }
            
            if(gEx('txtPassword').getValue().trim()=='')
            {
                function resp3Cer()
                {
                    gEx('txtPassword').focus();
                }
                msgBox('Debe ingresar la contrase&ntilde;a de llave privada',resp3Cer);
                return;
            }
            objResultado.cadenaFirma=''
            var cadObj='{"documentoFinal":"1","idDocumento":"'+IdDocumento+'","nombreDocumento":"'+cv(nombreDocumento)+
            		'","tipoFirma":"1","expediente":"'+nodoExpedienteSel.attributes.expediente+'","idExpediente":"'+nodoExpedienteSel.attributes.idExpediente+
                    '","asunto":"'+cv(gEx('txtAsunto').getValue())+'","cveUnidadGestion":"'+nodoExpedienteSel.attributes.unidadGestion+
                    '","cveMateria":"'+nodoExpedienteSel.attributes.idMateria+'"}';															
            
            var oObj=eval('['+cadObj+']')[0];
            
            
            var formData = new FormData();
            
            formData.append('passwd',AES_Encrypt(gEx('txtPassword').getValue()));
            
            
            for(var campo in oObj)
            {
                
                formData.append(campo,oObj[campo]);
                
                
                
            }
            
            
            
            formData.append('fCer',gE('fileCer').files[0]);
            formData.append('fKey',gE('fileKey').files[0]);
            mostrarMensajeProcesando('Firmando documento, &eacute;sta operaci&oacute;n puede tardar unos minutos...');
            $.ajax	({
                        url: "../paginasFunciones/procesarDocumentoFirmaElectronicaSICORE.php",
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(data)
                                {
                                    ocultarMensajeProcesando();
                                    var oResp=eval('['+data+']')[0];
                                    if(oResp.resultado=='1')
                                    {
                                        gEx('vDocumento').close();
                                        gEx('vDocumento2').close();
                                        gEx('vDocumento3').close();
                                        enviarPromocion=oResp.idPromocion;
                                        gEx('gPromociones').getStore().reload();
                                    }
                                    else
                                    {
                                        msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br><br>'+bD(oResp.mensaje));
                                    }
                                }
                    });
            
            
            
        break
        case 6:
            if(gE('filePFX').value=='')
            {
                function resp1CerFirel()
                {
                    gE('filePFX').focus();
                }
                msgBox('Debe ingresar el archivo de llave privada (*.pfx)',resp1CerFirel);
                return;
            }
            
            
            
            if(gEx('txtPasswordFirel').getValue().trim()=='')
            {
                function resp2CerFirel()
                {
                    gEx('txtPasswordFirel').focus();
                }
                msgBox('Debe ingresar la contrase&ntilde;a de llave privada',resp2CerFirel);
                return;
            }
            objResultado.cadenaFirma=''
            
            var cadObj='{"documentoFinal":"1","idDocumento":"'+IdDocumento+'","nombreDocumento":"'+cv(nombreDocumento)+
            		'","tipoFirma":"2","expediente":"'+nodoExpedienteSel.attributes.expediente+'","idExpediente":"'+nodoExpedienteSel.attributes.idExpediente+
                    '","asunto":"'+cv(gEx('txtAsunto').getValue())+'","cveUnidadGestion":"'+nodoExpedienteSel.attributes.unidadGestion+
                    '","cveMateria":"'+nodoExpedienteSel.attributes.idMateria+'"}';																
            
            
            var oObj=eval('['+cadObj+']')[0];
            
            
            var formData = new FormData();
            
            formData.append('passwd',AES_Encrypt(gEx('txtPasswordFirel').getValue()));
            
            
            for(var campo in oObj)
            {
                formData.append(campo,oObj[campo]);
            }
            
            
            
            formData.append('fCer',gE('filePFX').files[0]);
            mostrarMensajeProcesando('Firmando documento, &eacute;sta operaci&oacute;n puede tardar unos minutos...');
            
            $.ajax	({
                        url: "../paginasFunciones/procesarDocumentoFirmaElectronicaSICORE.php",
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(data)
                                {
                                    ocultarMensajeProcesando();
                                    var oResp=eval('['+data+']')[0];
                                    if(oResp.resultado=='1')
                                    {
                                        gEx('vDocumento').close();
                                        gEx('vDocumento2').close();
                                        gEx('vDocumento3').close();
                                        enviarPromocion=oResp.idPromocion;
                                        gEx('gPromociones').getStore().reload();
                                        
                                        
                                    }
                                    else
                                    {
                                        msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br><br>'+bD(oResp.mensaje));
                                    }
                                }
                    });
        break;
        
    }
}

function mostrarDocumento(iR,t)
{
	var pos=obtenerPosFila(gEx('gPromociones').getStore(),'idRegistro',bD(iR));
    
    var fila=gEx('gPromociones').getStore().getAt(pos);
    
	var nombreCampo='';
    var nombreIdDocumento='';
    if(bD(t)=='1')
    {
    	nombreCampo='nombreDocumentoPromocion';
        nombreIdDocumento='idDocumentoPromocion';
    }
    else
    {
    	nombreCampo='nombreDocumentoRespuesta';
        nombreIdDocumento='idDocumentoRespuesta';
    }
    
    var idDocumento=fila.get(nombreIdDocumento);
    var arrNombre=fila.get(nombreCampo).split('.');
    if(window.parent && window.parent.mostrarVisorDocumentoProceso)
    {
    	window.parent.mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    }
    else
    	mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    
}

function mostrarDocumentoLMVLV(iR,t)
{
	var pos=obtenerPosFila(gEx('gSolicitudesLAMVLV').getStore(),'idRegistro',bD(iR));
    
    var fila=gEx('gSolicitudesLAMVLV').getStore().getAt(pos);
    
	var nombreCampo='';
    var nombreIdDocumento='';
    if(bD(t)=='1')
    {
    	nombreCampo='nombreDocumentoPromocion';
        nombreIdDocumento='idDocumentoPromocion';
    }
    else
    {
    	nombreCampo='nombreDocumentoRespuesta';
        nombreIdDocumento='idDocumentoRespuesta';
    }
    
    var idDocumento=fila.get(nombreIdDocumento);
    var arrNombre=fila.get(nombreCampo).split('.');
    if(window.parent && window.parent.mostrarVisorDocumentoProceso)
    {
    	window.parent.mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    }
    else
    	mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    
}


function enviarSolicitudUGJ(iP)
{
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
    		gEx('gSolicitudesLAMVLV').getStore().reload();
            //obtenerAcuseSolicitud(iP);
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=206&iP='+iP,true);
}



function enviarPromocionJuzgado(iP)
{

	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var pos=obtenerPosFila(gEx('gPromociones').getStore(),'idRegistro',bD(iP));
    
    		var fila=gEx('gPromociones').getStore().getAt(pos);
            fila.set('fechaRecepcionJuzgado',Date.parseDate(arrResp[1],'Y-m-d H:i:s'));
            fila.set('situacionPromocion','2');
            obtenerAcuseSolicitud(iP);
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=4&iP='+iP,true);
}

function obtenerAcuseSolicitud(iS)
{
	 enviarFormularioDatos('../modulosEspeciales_SICORE/generarAcusePromocion.php',[['idRegistro',bD(iS)]],'POST','framePrincipal');
}

function obtenerAcuseSolicitudLAMVLV(iS)
{
	 enviarFormularioDatos('../modulosEspeciales_SICORE/generarAcuseLAMVLV.php',[['idRegistro',bD(iS)]],'POST','framePrincipal');
}


function mostrarRegistroSolicitudLAMVLV(iR)
{
	var iFormulario=483;
    var params='';
    var obj={};
    
    if(iR=='-1')
    {
    	params=[['idRegistro',-1],['idFormulario',(iFormulario)],['dComp',bE('agregar')],['actor',bE(407)]]; 
        obj.ancho='100%';
        obj.alto='100%';
        obj.url='../modeloPerfiles/vistaDTDv3.php';
        obj.params=params;
        obj.modal=true;
        
        abrirVentanaFancySuperior(obj);  
    }
    else
    {
    	var cadObj='{"idFormulario":"'+iFormulario+'","idRol":"157_0","idRegistro":"'+iR+'"}';
    	function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
                params=[['idRegistro',(iR)],['idFormulario',(iFormulario)],['dComp',bE('auto')],['actor',bE(arrResp[1])]]; 
                obj.ancho='100%';
                obj.alto='100%';
                obj.url='../modeloPerfiles/vistaDTDv3.php';
                obj.params=params;
                obj.modal=true;
                
                abrirVentanaFancySuperior(obj);  
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SGP.php',funcAjax, 'POST','funcion=35&cadObj='+cadObj,true);
              
    }
    
    
}


function recargarContenedorCentral()
{
	var x;
    for(x=0;x<arrGrids.length;x++)
    {
    	gEx(arrGrids[x]).getStore().reload();
    }	
}


function crearGridSolicitudesLAMVLV()
{
   
   var lector= new Ext.data.JsonReader({
                                        
                                        totalProperty:'numReg',
                                        fields: [
                                                    {name:'idRegistro'},
                                                    {name:'expediente'},
                                                    {name:'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                    {name: 'victima'},
                                                    {name: 'situacionPromocion'},
                                                    {name: 'fechaRespuesta', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                   	{name: 'fechaRecepcionJuzgado', type:'date', dateFormat:'Y-m-d H:i:s'}, 
                                                    {name: 'idDocumentoPromocion'},
                                                    {name: 'nombreDocumentoPromocion'},
                                                    {name: 'idDocumentoRespuesta'},
                                                    {name: 'nombreDocumentoRespuesta'},
                                                    {name: 'juzgado'},
                                                    {name: 'folioRecepcion'}
                                                ],
                                        root:'registros'
                                        
                                    }
                                  );
 
                                                                                  
    var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(
    
                                                                                  {
    
                                                                                      url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'
    
                                                                                  }
    
                                                                              ),
                                                sortInfo: {field: 'fechaRegistro', direction: 'ASC'},
                                                groupField: 'fechaRegistro',
                                                remoteGroup:false,
                                                remoteSort: true,
                                                autoLoad:true
                                                
                                            }) 
    alDatos.on('beforeload',function(proxy)
                                    {
                                        proxy.baseParams.funcion='205';
                                       
                                        
                                    }
                        )   

	alDatos.on('load',function(proxy)
                                {
                                    if(enviarPromocion!=-1)
                                    {
                                    	enviarPromocionJuzgado(bE(enviarPromocion));
                                        enviarPromocion=-1;
                                    }
                                    
                                }
                    )
   
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(), 
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            hidden:esJuzgadoFamiliar,
                                                            dataIndex:'fechaRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrSituacionPromocion,registro.data.situacionPromocion,0,true);
                                                                    	return '<img src="'+arrSituacionPromocion[pos][2]+'" width="20" height="20">';
                                                                    }
                                                        }, 
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	
                                                                    	return '<a href="javascript:abrirRegistroProceso(\''+bE(val)+'\')"><img src="../images/magnifier.png" title="Abrir registro" alt="Abrir registro" width="14" height="14"></a>';
                                                                    }
                                                        },  
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idRegistro',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	
                                                                    	return '<a href="javascript:obtenerAcuseSolicitudLAMVLV(\''+bE(val)+'\')"><img src="../images/download.png" title="Descargar acuse de ev&iacute;o" alt="Descargar acuse de ev&iacute;o" width="14" height="14"></a>';
                                                                    }
                                                        },                                                       
                                                        {
                                                            header:'Fecha de Registro',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'fechaRegistro',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:esJuzgadoFamiliar?'Expediente':'Carpeta Judicial',
                                                            width:145,
                                                            sortable:true,
                                                            dataIndex:'expediente',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='N/E')
                                                                        	return '----';
                                                                        return val;
                                                                    }
                                                        },
                                                       /* {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idDocumentoPromocion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return '<a href="javascript:mostrarDocumento(\''+bE(registro.data.idRegistro)+'\',\''+bE(1)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de promoci&oacute;n" alt="Ver documento de promoci&oacute;n"></a>';
                                                                    }
                                                        },*/
                                                        {
                                                            header:esJuzgadoFamiliar?'Actor':'V&iacute;ctima',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'victima',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Situaci&oacute;n Actual',
                                                            width:280,
                                                            sortable:true,
                                                            dataIndex:'situacionPromocion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(parseInt(val))
                                                                        {
                                                                        	if(val==1)
                                                                            {
                                                                            	comp+='&nbsp;&nbsp;<a href="javascript:enviarSolicitudUGJ(\''+bE(registro.data.idRegistro)+'\')"><img src="../images/document_go.png" width="14" height="14" title="Enviar promoci&oacute;n a Juzgado" alt="Enviar promoci&oacute;n a Juzgado"></a>';
                                                                            }
                                                                        }
                                                                    	return formatearValorRenderer(arrSituacionLAMVLV,val,1,true)+comp;
                                                                    }
                                                        },
                                                        {
                                                            header:esJuzgadoFamiliar?'Fecha de Recepci&oacute;n<br> en Juzgado':'Fecha de Recepci&oacute;n<br> en Unidad de Gesti&oacute;n',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'fechaRecepcionJuzgado',
                                                            renderer:function(val)
                                                            		{
                                                                    	
                                                                    	if(!val)
                                                                        	return '----';
                                                                    	if(val)
                                                                    		return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:esJuzgadoFamiliar?'Folio de Recepci&oacute;n<br> en Juzgado':'Folio de Recepci&oacute;n<br> en Unidad de Gesti&oacute;n',
                                                            width:140,
                                                            sortable:true,
                                                            dataIndex:'folioRecepcion',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='N/E')
                                                                        	return '----';
                                                                    	return val;
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de Respuesta',
                                                            width:130,
                                                            sortable:true,
                                                            hidden:true,
                                                            dataIndex:'fechaRespuesta',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
                                                                    		return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            hidden:esJuzgadoFamiliar,
                                                            dataIndex:'idDocumentoRespuesta',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if((val!='')&&(val!='N/E'))
	                                                                    	return '<a href="javascript:mostrarDocumentoLMVLV(\''+bE(registro.data.idRegistro)+'\',\''+bE(2)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de respuesta" alt="Ver documento de respuesta"></a>'+val;
                                                                    }
                                                        },
                                                        {
                                                            header:esJuzgadoFamiliar?'Juzgado':'Unidad de Gesti&oacute;n Judicial',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'juzgado',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val=='N/E')
                                                                        	return '----';
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrJuzgados,val));
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gSolicitudesLAMVLV',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,  
                                                            tbar:	[
                                                            			
                                                            			
                                                                        {
                                                                        	icon:'../images/user_add.png',
                                                                            cls:'x-btn-text-icon',
                                                                            //hidden:!registrarLAMVLV,
                                                                            id:'btnAddLAMVLV',
                                                                            text:'Registrar Nueva Solicitud',
                                                                            tooltip:'Ley de Acceso de las Mujeres a una Vida Libre de Violencia',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarRegistroSolicitudLAMVLV(-1);
                                                                                    }
                                                                            
                                                                        }
                                                            		] ,                                                         
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}


function abrirRegistroProceso(iR)
{
	mostrarRegistroSolicitudLAMVLV(bD(iR));	
}