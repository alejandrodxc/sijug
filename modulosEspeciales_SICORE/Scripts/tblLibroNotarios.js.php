<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT id__477_tablaDinamica,tipoJuicio FROM _477_tablaDinamica ORDER BY tipoJuicio";
	$arrTipoJuicio=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT DISTINCT anio,anio FROM 7044_procesosLibrosGobierno WHERE tipoLibro=4";
	$arrAnios=$con->obtenerFilasArreglo($consulta);
	$anioActual=date('Y');
	
	$consulta="SELECT cveEstado,estado FROM 820_estados ORDER BY estado";
	$arrEstados=$con->obtenerFilasArreglo($consulta);
	$consulta="SELECT idTipoCarpeta,nombreTipoCarpeta FROM 7020_tipoCarpetaAdministrativa";
	$arrTipoExpediente=$con->obtenerFilasArreglo($consulta);
?>
var arrTipoExpediente=<?php echo $arrTipoExpediente?>;
var arrEstados=<?php echo $arrEstados?>;
var anioActual='<?php echo $anioActual?>';
var arrAnios=<?php echo $arrAnios?>;
var arrTipoJuicio=<?php echo $arrTipoJuicio?>;

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Libro de Registro para Notarios</b></span>',
                                                items:	[
                                                            crearGridLibroGobierno()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridLibroGobierno()
{
	var cmbAnio=crearComboExt('cmbAnio',arrAnios,0,0,150);
    var pos=obtenerPosFila(gEx('cmbAnio').getStore(),'id',anioActual);
    if(pos!=-1)
    {
    	cmbAnio.setValue(anioActual);
    }
    else
    {
    	if(arrAnios.length>0)
        {
        	cmbAnio.setValue(arrAnios[arrAnios.length-1]);
        }
    }
    
    cmbAnio.on('select',function()
    					{
                        	gEx('gLibro').getStore().reload();
                        }
    			)
    
    
    
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'idFormulario'},
                                                        {name:'folio'},
		                                                {name: 'noExpediente'},
		                                                {name:'actor'},
		                                                {name:'demandado'},
                                                        {name: 'tipoJuicio'},
                                                        {name: 'fechaRegistro',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'nombreNotario'},
                                                        {name: 'datosParticipantes'},
                                                        {name: 'noNotaria'},
                                                        {name: 'estadoNotaria'},
                                                        {name: 'documentosEntregados'},
                                                        {name: 'fechaAcuse',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'fechaDevolucin',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'fechaConocimiento',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'documentosDevueltos'},
                                                        {name: 'comentariosEntrega'},
                                                        {name: 'secretaria'},
                                                        {name: 'tipoExpediente'},
                                                        {name: 'comentariosDevolucion'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='13';
                                        proxy.baseParams.anioJudicial=cmbAnio.getValue()==''?anioActual:cmbAnio.getValue();
                                        
                                    }
                        )   
    
    var paginador=	new Ext.PagingToolbar	(
                                              {
                                                    pageSize: 100,
                                                    store: alDatos,
                                                    displayInfo: true,
                                                    disabled:false
                                                }
                                             )       
    
	var expander = new Ext.ux.grid.RowExpander({
                                                    column:2,
                                                    tpl : new Ext.Template(
                                                        '<table width="100%" >'+
                                                        '<tr><td  style="padding:10px">{datosParticipantes}</td></tr>'+
                                                        '</table>'
                                                    )
                                                }); 
       
	var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'Folio',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'folio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href="javascript:abrirFormularioProcesoLibro(\''+bE(registro.data.idFormulario)+'\',\''+bE(registro.data.idRegistro)+'\')">'+val+'</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:120,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        },
                                                                dataIndex:'fechaRegistro'
                                                            }
                                                            
                                                            ,
                                                            {
                                                                header:'No. Expediente',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'noExpediente'
                                                            },
                                                             {
                                                                header:'Tipo',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'tipoExpediente',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrTipoExpediente,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Secretar&iacute;a',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'secretaria'
                                                            },
                                                            {
                                                                header:'Actor',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'actor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Demandado',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'demandado',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Tipo de Juicio',
                                                                width:260,
                                                                sortable:true,
                                                                dataIndex:'tipoJuicio',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrTipoJuicio,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de conocimiento<br />a notario',
                                                                width:135,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y');
                                                                        },
                                                                dataIndex:'fechaConocimiento'
                                                            },
                                                            {
                                                                header:'Nombre del notario',
                                                                width:250,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'nombreNotario'
                                                            },
                                                            {
                                                                header:'No. de notar&iacute;a',
                                                                width:110,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'noNotaria'
                                                            },
                                                            {
                                                                header:'Estado notar&iacute;a',
                                                                width:180,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrEstados,val);
                                                                        },
                                                                dataIndex:'estadoNotaria'
                                                            },
                                                            {
                                                                header:'Documentos Entregados',
                                                                width:350,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'documentosEntregados'
                                                            },
                                                            {
                                                                header:'Comentarios de entrega',
                                                                width:400,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'comentariosEntrega'
                                                            },
                                                            {
                                                                header:'Fecha de acuse<br /> y firma del notario',
                                                                width:120,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
	                                                                        	return val.format('d/m/Y');
                                                                        },
                                                                dataIndex:'fechaAcuse'
                                                            },
                                                            {
                                                                header:'Fecha de devoluci&oacute;n',
                                                                width:120,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
	                                                                        	return val.format('d/m/Y');
                                                                        },
                                                                dataIndex:'fechaDevolucin'
                                                            },
                                                            {
                                                                header:'Documentos devueltos',
                                                                width:350,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'documentosDevueltos'
                                                            },
                                                            {
                                                                header:'Comentarios de devoluci&oacute;n',
                                                                width:400,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'comentariosDevolucion'
                                                            }
                                                           
                                                        ]
                                              	);
                                                    
	var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gLibro',
                                                    store:alDatos,
                                                    border:false,
                                                    region:'center',
                                                    frame:false,
                                                    cm: cModelo,
                                                    bbar:[paginador],
                                                    plugins:[expander],
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    tbar:	[
                                                    			{
                                                                	xtype:'label',
                                                                    html:'<b>A&ntilde;o Judicial:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                },
                                                                cmbAnio
                                                    		],
                                                    columnLines : true,                                                    
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: false,
                                                                                        enableGrouping :false,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: false,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
	
    
    tblGrid.getStore().load(	{
    								params:	{
                                    			url:'../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php',
                                                start:0, 
                                                limit:100
                                                
                                    		}
    							}
                           )  
    
    return 	tblGrid;
}


function abrirFormularioProcesoLibro(iF,iR)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",bE(0)],['dComp',bE(accion)]];
    
    
    var obj={};
    obj.url="../modeloPerfiles/vistaDTDv3.php";
    obj.params=arrDatos;
    obj.ancho='100%';
    obj.alto='100%';
    
    abrirVentanaFancySuperior(obj);
    
    
    
}