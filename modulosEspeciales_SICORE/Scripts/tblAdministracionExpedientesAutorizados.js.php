<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT DISTINCT anioExpediente,anioExpediente FROM 7006_usuariosVSCarpetasAdministrativas WHERE idUsuario=".$_SESSION["idUsr"].
				" ORDER BY anioExpediente";
	$arrAnio=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT anioExpediente FROM 7006_usuariosVSCarpetasAdministrativas WHERE idUsuario=".$_SESSION["idUsr"].
				" ORDER BY anioExpediente desc";
	$anioExpediente=$con->obtenerValor($consulta);
	
	$consulta="SELECT claveMateria,materia,promocioneFirmadas FROM _480_tablaDinamica";
	$arrMateriasPromociones=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT id__17_tablaDinamica,UPPER(nombreUnidad) FROM _17_tablaDinamica";
	$arrJuzgados=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idSituacion,icono,tamano FROM 7011_situacionEventosAudiencia";
	$arrSituaciones=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idSituacion,descripcionSituacion FROM 7011_situacionEventosAudiencia";
	$arrSituacionEvento=$con->obtenerFilasArreglo($consulta);
?>
var arrSituacionEvento=<?php echo $arrSituacionEvento?>;
var arrSemaforo=<?php echo $arrSituaciones?>;
var arrTipoSeguimiento=[['1','Acuerdo'],['2','Promoci&oacute;n registrada']];
var enviarPromocion=-1;
var arrJuzgados=<?php echo $arrJuzgados?>;
var IdDocumento='';
var nombreDocumento='';
var arrMateriasPromociones=<?php echo $arrMateriasPromociones?>;
var anioExpediente='<?php echo $anioExpediente?>';
var arrAnio=<?php echo $arrAnio?>;

var arrSituacionPromocion=[['1','En espera de env&iacute;o a juzgado','../images/bullet-grey.png'],['2','En espera de atenci\xF3n','../images/bullet-yellow.png'],
							['3','Atendida','../images/bullet-green.png']];
var nodoExpedienteSel=null;

Ext.onReady(inicializar);

function inicializar()
{
	arrAnio.splice(0,0,['0','Cualquiera']);
	var cmbAnio=crearComboExt('cmbAnio',arrAnio,0,0,110);
    cmbAnio.setValue('0');
    
    cmbAnio.on('select',function(cmb,registro)
    					{
                        	gEx('arbolExpedientes').getRootNode().reload();
                        }
    			)
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Expedientes</b></span> <span id="lblExpediente" style="color:#000; font-weight:bold;font-size:14px"></span><span id="lblNoExpediente" style="color:#900; font-weight:bold;font-size:14px"></span>',
                                                items:	[
                                                			{
                                                            	xtype:'panel',
                                                                width:300,
                                                                region:'west',
                                                                layout:'border',
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                                html:'<b>A&ntilde;o del expediente:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                                
                                                                            },
                                                                            cmbAnio
                                                                		],
                                                                items:	[
                                                                				
                                                                			crearArbolExpedientes()
                                                                           ]
                                                            },
                                                            {
                                                                  xtype:'tabpanel',
                                                                  region:'center',
                                                                  id:'panelGrids',
                                                                  activeTab:0,
                                                                  items:	[crearGridSeguimientoExpediente(),crearGridEventos()]
                                                              }
                                                            
                                                        ]
                                            }
                                         ]
                            }
                        )   
}


function crearArbolExpedientes()
{
	var raiz=new  Ext.tree.AsyncTreeNode(
											{
												id:'-1',
												text:'Raiz',
												draggable:false,
												expanded :false,
												cls:'-1'
											}
										)
										
	var cargadorArbol=new Ext.tree.TreeLoader(
                                                {
                                                    baseParams:{
                                                                    funcion:'1'
                                                                    
                                                                },
                                                    dataUrl:'../paginasFunciones/funcionesModulosEspeciales_SICORE.php'
                                                }
                                            )		
										
	cargadorArbol.on('beforeload',function(c)
    						{
                            	
                            	c.baseParams.anio=gEx('cmbAnio').getValue();
                                c.baseParams.noExpediente=gEx('txtNumeroExpediente').getValue();
                            	nodoExpedienteSel=null;
                            }
    				)	
    										
	cargadorArbol.on('load',function(c,nodoCarga)
    						{
                            	
                            }
    				)										
	var arbolExpedientes=new Ext.tree.TreePanel	(
                                                            {
                                                                id:'arbolExpedientes',
                                                                useArrows:true,
                                                                animate:true,
                                                                enableDD:false,
                                                                ddScroll:true,
                                                                containerScroll: true,
                                                                autoScroll:true,
                                                                border:false,
                                                                region:'center',
                                                                root:raiz,
                                                                tbar:	[
                                                                			{
                                                                            	xtype:'label',
                                                                                html:'<b>N&uacute;mero de expediente:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                                
                                                                            },
                                                                            {
                                                                            	xtype:'textfield',
                                                                                width:140,
                                                                                enableKeyEvents:true,
                                                                                id:'txtNumeroExpediente',
                                                                                listeners:	{
                                                                                				keypress:function(txt,e)
                                                                                                	{
                                                                                                    	if(e.charCode=='13')
                                                                                                        {
                                                                                                        	if(txt.ultimaBusqueda!=txt.getValue())
                                                                                                            {
                                                                                                            	gEx('arbolExpedientes').getRootNode().reload();
                                                                                                        		txt.ultimaBusqueda=txt.getValue();
                                                                                                            }
                                                                                                        }
                                                                                                    },
                                                                                                blur:function(txt)
                                                                                                	{
                                                                                                    	
                                                                                                    	if(txt.ultimaBusqueda!=txt.getValue())
                                                                                                        {
                                                                                                        	gEx('arbolExpedientes').getRootNode().reload();
                                                                                                        	txt.ultimaBusqueda=txt.getValue();
                                                                                                        }
                                                                                                        
                                                                                                    }
                                                                                			}
                                                                            }
                                                                		],
                                                                loader: cargadorArbol,
                                                                rootVisible:false
                                                            }
                                                        )
         
         
                                                    
	arbolExpedientes.on('click',funcExpediente);
	return  arbolExpedientes;
}

function funcExpediente(nodo, evento)
{

	nodoExpedienteSel=nodo;
    console.log(nodoExpedienteSel);
    if(nodoExpedienteSel.attributes.tipo=='3')
    {

        if(nodoExpedienteSel.attributes.accesoVideograbaciones==0)
        {
        	gEx('panelGrids').hideTabStripItem('gridAudiencias');
            gEx('panelGrids').setActiveTab(0);
        }
        else
        {
        	gEx('panelGrids').unhideTabStripItem('gridAudiencias');
        
        }
        gEx('gSeguimiento').getStore().reload();
        recargarGridAudiencias();
        gE('lblExpediente').innerHTML=' [Expediente: ';
        gE('lblNoExpediente').innerHTML=nodoExpedienteSel.attributes.expediente+', '+nodoExpedienteSel.parentNode.attributes.juzgado+'<span style="color:#000; font-weight:bold">]</span>';
    }
    else
    {

        
        gEx('gSeguimiento').getStore().removeAll();
        gEx('gridAudiencias').getStore().removeAll();
        
        gE('lblExpediente').innerHTML='';
        gE('lblNoExpediente').innerHTML='';
    }
    
    
    
}

function crearGridSeguimientoExpediente()
{
	var lector= new Ext.data.JsonReader({
                                        
                                        totalProperty:'numReg',
                                        fields: [
                                                    {name:'idRegistro'},
                                                    {name:'fechaRegistro', type:'date', dateFormat:'Y-m-d H:i:s'},                                                    
                                                    {name: 'idDocumentoAcuerdo'},
                                                    {name: 'nombreDocumentoAcuerdo'},
                                                    {name: 'idDocumentoPromocion'},
                                                    {name: 'nombreDocumentoPromocion'},
                                                    {name: 'tipo'},
                                                    {name: 'asunto'}
                                                ],
                                        root:'registros'
                                        
                                    }
                                  );
 
                                                                                  
var alDatos=new Ext.data.GroupingStore({
                                                        reader: lector,
                                                        proxy : new Ext.data.HttpProxy	(

                                                                                          {

                                                                                              url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'

                                                                                          }

                                                                                      ),
                                                        sortInfo: {field: 'fechaRegistro', direction: 'ASC'},
                                                        groupField: 'fechaRegistro',
                                                        remoteGroup:false,
                                                        remoteSort: true,
                                                        autoLoad:false
                                                        
                                                    }) 
alDatos.on('beforeload',function(proxy)
                                {
                                    proxy.baseParams.funcion='14';
                                    proxy.baseParams.iE=bE(nodoExpedienteSel.attributes.idExpediente);
                                    proxy.baseParams.nE=bE(nodoExpedienteSel.attributes.expediente);
                                    proxy.baseParams.cM=bE(nodoExpedienteSel.attributes.idMateria);
                                    proxy.baseParams.cG=bE(nodoExpedienteSel.attributes.unidadGestion);
                                    
                                }
                    )   

	alDatos.on('load',function(proxy)
                                {
                                  
                                    
                                }
                    )
   
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(), 
                                                                                                              
                                                        {
                                                            header:'Fecha de registro',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'fechaRegistro',
                                                            renderer:function(val)
                                                            		{
                                                                    	return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'Tipo',
                                                            width:145,
                                                            sortable:true,
                                                            dataIndex:'tipo',
                                                            renderer:function(val)
                                                            			{
                                                                        	return formatearValorRenderer(arrTipoSeguimiento,val);
                                                                        }
                                                                        
                                                        },
                                                        {
                                                            header:'Acuerdo/Promoci&oacute;n',
                                                            width:120,
                                                            sortable:true,
                                                            align:'center',
                                                            dataIndex:'idDocumentoAcuerdo',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return '<a href="javascript:mostrarDocumentoSeguimiento(\''+bE(registro.data.idRegistro)+'\',\''+bE(1)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de promoci&oacute;n" alt="Ver documento de promoci&oacute;n"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'',
                                                            width:400,
                                                            sortable:true,
                                                            dataIndex:'asunto',
                                                            renderer:mostrarValorDescripcion
                                                        },
                                                        {
                                                            header:'Documento<br />de respuesta',
                                                            width:110,
                                                            sortable:true,
                                                            align:'center',
                                                            dataIndex:'idDocumentoPromocion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(registro.data.tipo=='1')
                                                                        	return 'N/A';
                                                                    	if(val!='')
                                                                    		return '<a href="javascript:mostrarDocumentoSeguimiento(\''+bE(registro.data.idRegistro)+'\',\''+bE(2)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de promoci&oacute;n" alt="Ver documento de promoci&oacute;n"></a>';
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gSeguimiento',
                                                            title:'Expediente Digital',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            columnLines : true,                                                         
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :false,
                                                                                                enableNoGroups:false,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: false,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;
}

function mostrarDocumentoSeguimiento(iR,t)
{
	var pos=obtenerPosFila(gEx('gSeguimiento').getStore(),'idRegistro',bD(iR));
    
    var fila=gEx('gSeguimiento').getStore().getAt(pos);
    
	var nombreCampo='';
    var nombreIdDocumento='';
    if(bD(t)=='2')
    {
    	nombreCampo='nombreDocumentoPromocion';
        nombreIdDocumento='idDocumentoPromocion';
    }
    else
    {
    	nombreCampo='nombreDocumentoAcuerdo';
        nombreIdDocumento='idDocumentoAcuerdo';
    }
    
    var idDocumento=fila.get(nombreIdDocumento);
    var arrNombre=fila.get(nombreCampo).split('.');
    if(window.parent && window.parent.mostrarVisorDocumentoProceso)
    {
    	window.parent.mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    }
    else
    	mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    
}

function crearGridEventos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idEvento'},
                                                        {name: 'carpetaAdministrativa'},
		                                                {name: 'fechaEvento', type:'date', dateFormat:'Y-m-d'},
		                                                {name: 'horaInicial', type:'date', dateFormat:'Y-m-d H:i:s'},
		                                               	{name: 'horaFinal', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'horaInicioReal', type:'date', dateFormat:'Y-m-d H:i:s'},
		                                               	{name: 'horaTerminoReal', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'urlMultimedia'},
                                                        {name: 'tipoAudiencia'},
                                                        {name: 'sala'},
                                                        {name: 'unidadGestion'},
                                                        {name: 'situacion'},
                                                        {name: 'juez'}  ,
                                                        {name: 'edificio'},
                                                        {name: 'comentariosAdicionales'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(

                                                                                  {

                                                                                      url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'

                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'fechaEvento', direction: 'ASC'},
                                                groupField: 'fechaEvento',
                                                remoteGroup:false,
                                                remoteSort: false,
                                                autoLoad:false
                                                
                                            }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	
                                    }
                        )   
       
       
       
       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer({width:40}),
                                                            {
                                                                header:'',
                                                                width:30,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var icono='';
                                                                            meta.attr='style="padding: 0px !important;"';
                                                                        	icono=formatearValorRenderer(arrSemaforo,val);    
                                                                            var tamano=formatearValorRenderer(arrSemaforo,val,2);                                                                            
                                                                            return '<img src="'+icono+'" width="'+tamano+'" height="'+tamano+'" title="'+formatearValorRenderer(arrSituacionEvento,val)+'" alt="'+formatearValorRenderer(arrSituacionEvento,val)+'">';
                                                                        }
                                                            },
                                                             {
                                                                header:'Situaci&oacute;n audiencia',
                                                                width:170,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        
                                                                        	var comp='';
                                                                            if(registro.data.comentariosAdicionales!='')
                                                                            {
                                                                            	comp='&nbsp;&nbsp;<img src="../images/icon_comment.gif" title="'+cv(registro.data.comentariosAdicionales,true,true)+'" alt="'+cv(registro.data.comentariosAdicionales,true,true)+'" />';
                                                                            }
                                                                            return mostrarValorDescripcion(formatearValorRenderer(arrSituacionEvento,val))+comp;
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'',
                                                                width:60,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                css:'text-align:left;vertical-align:middle !important;',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	
                                                                            var comp2='';
                                                                           	switch(val)
                                                                            {
                                                                            	/*case '4':
                                                                                	if(registro.data.urlCanal!='')
                                                                                		comp2='<a href="javascript:abrirVentanaSala(\''+bE(registro.data.sala)+'\')"><img src="../images/film_go.png" title="Visualizar audiencia" alt="Visualizar audiencia" /></a>'
                                                                                break;*/
                                                                                case '2':
                                                                                	if(registro.data.urlMultimedia!='')
                                                                                		comp2='<a href="javascript:abrirVideoGrabacion(\''+bE(registro.data.urlMultimedia)+'\')"><img src="../images/control_play_blue.png" title="Visualizar grabaci&oacute;n" alt="Visualizar grabaci&oacute;n" /></a>'
                                                                              	break;
                                                                            }
                                                                        	return comp2;
                                                                        	
                                                                        }
                                                            },
                                                            {
                                                                header:'Expediente',
                                                                width:150,
                                                                sortable:true,
                                                                hidden:true,
                                                                dataIndex:'carpetaAdministrativa'
                                                            },
                                                            {
                                                                header:'Fecha audiencia',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'fechaEvento',
                                                                renderer:function(val)
                                                                	{
                                                                    	return val.format('d/m/Y');
                                                                    }
                                                            },
                                                            {
                                                                header:'Hora programada de audiencia',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'horaInicial',
                                                                renderer:function(val,meta,registro)
                                                                	{

                                                                    	var comp='';
                                                                        if(val.format('d')!=registro.data.horaFinal.format('d'))
                                                                        {
                                                                        	comp=' del '+registro.data.horaFinal.format('d/m/Y');
                                                                        }

                                                                    	return 'De las '+val.format('H:i')+' hrs. a las '+registro.data.horaFinal.format('H:i')+' hrs.'+comp
                                                                    }
                                                            },
                                                            
                                                            {
                                                                header:'Hora de realizaci&oacute;n de audiencia',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'horaInicioReal',
                                                                renderer:function(val,meta,registro)
                                                                	{

                                                                    	if(!val)
                                                                        {
                                                                        	return '(Datos no disponibles)';
                                                                        }
                                                                    	var comp='';
                                                                        if(val.format('d')!=registro.data.horaTerminoReal.format('d'))
                                                                        {
                                                                        	comp=' del '+registro.data.horaTerminoReal.format('d/m/Y');
                                                                        }

                                                                    	return 'De las '+val.format('H:i')+' hrs. a las '+registro.data.horaTerminoReal.format('H:i')+' hrs.'+comp
                                                                    }
                                                            },
                                                            {
                                                                header:'Tipo de audiencia',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'tipoAudiencia',
                                                                renderer:function(val)
                                                                		{
                                                                        
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Edificio',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'edificio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var lblSala=mostrarValorDescripcion(val);
                                                                        	return lblSala;
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'Sala',
                                                                width:110,
                                                                sortable:true,
                                                                dataIndex:'sala',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Juez',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'juez'
                                                            }
                                                            
                                                           
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAudiencias',
                                                                store:alDatos,
                                                                region:'center',
                                                                title:'Audiencias',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :false,
                                                                loadMask:true,
                                                                columnLines : true,      
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );

	tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
    											{
                                                	
                                                    
                                                }
    							)

	tblGrid.getSelectionModel().on('rowdeselect',function()
    											{
                                                	
                                                }
    							)

	return 	tblGrid;

}

function recargarGridAudiencias()
{
	gEx('gridAudiencias').getStore().load	(
                                                {
                                                    url:'../paginasFunciones/funcionesModulosEspeciales_SICORE.php',
                                                    params:	{
                                                                funcion:'15',
                                                                cveMateria:nodoExpedienteSel.attributes.idMateria,
                                                                exp:nodoExpedienteSel.attributes.expediente,
                                                                iC:nodoExpedienteSel.attributes.idExpediente
                                                            }
                                                }
                                            );
}

function abrirVideoGrabacion(url)
{
	var obj={};    
    obj.ancho='100%';
    obj.alto='100%';
    obj.url='../modulosEspeciales_SICORE/visorGrabacionAudienciaSicor.php';
    obj.params=[['urlMultimedia',url],['cPagina','sFrm=true'],[]]
    if(window.parent)
    	window.parent.abrirVentanaFancy(obj);
    else
	    abrirVentanaFancy(obj);
}