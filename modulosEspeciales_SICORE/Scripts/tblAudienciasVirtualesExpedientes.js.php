<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idSituacion,icono,tamano FROM 7011_situacionEventosAudiencia";
	$arrSituaciones=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT idSituacion,descripcionSituacion FROM 7011_situacionEventosAudiencia";
	$arrSituacionEvento=$con->obtenerFilasArreglo($consulta);
	
	$consulta="SELECT id__17_tablaDinamica,UPPER(nombreUnidad) FROM _17_tablaDinamica";
	$arrJuzgados=$con->obtenerFilasArreglo($consulta);

?>

var primeraCargaFrame=true;
var arrJuzgados=<?php echo $arrJuzgados?>;
var arrSituacionEvento=<?php echo $arrSituacionEvento?>;
var arrSemaforo=<?php echo $arrSituaciones?>;


Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Audiencias Virtuales Agendadas</b></span>',
                                                items:	[
                                                            crearGridEventos()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridEventos()
{
	 var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name: 'idEvento'},
                                                        {name: 'carpetaAdministrativa'},
		                                                {name: 'fechaEvento', type:'date', dateFormat:'Y-m-d'},
		                                                {name: 'horaInicial', type:'date', dateFormat:'Y-m-d H:i:s'},
		                                               	{name: 'horaFinal', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'horaInicioReal', type:'date', dateFormat:'Y-m-d H:i:s'},
		                                               	{name: 'horaTerminoReal', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'urlMultimedia'},
                                                        {name: 'tipoAudiencia'},
                                                        {name: 'sala'},
                                                        {name: 'unidadGestion'},
                                                        {name: 'situacion'},
                                                        {name: 'juez'}  ,
                                                        {name: 'edificio'},
                                                        {name: 'comentariosAdicionales'},
                                                        {name: 'codigoGenerado'},
                                                        {name: 'idExpediente'},
                                                        {name: 'cveMateria'},
                                                        {name: 'urlReunionVirtual'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                reader: lector,
                                                proxy : new Ext.data.HttpProxy	(

                                                                                  {

                                                                                      url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'

                                                                                  }

                                                                              ),
                                                sortInfo: {field: 'fechaEvento', direction: 'ASC'},
                                                groupField: 'fechaEvento',
                                                remoteGroup:false,
                                                remoteSort: false,
                                                autoLoad:true
                                                
                                            }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='209';
                                    
                                    }
                        )   
       
       
       
       
       
        var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            new  Ext.grid.RowNumberer({width:40}),
                                                             
                                                            {
                                                                header:'',
                                                                width:30,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var icono='';
                                                                            meta.attr='style="padding: 0px !important;"';
                                                                        	icono=formatearValorRenderer(arrSemaforo,val);    
                                                                            var tamano=formatearValorRenderer(arrSemaforo,val,2);                                                                            
                                                                            return '<img src="'+icono+'" width="'+tamano+'" height="'+tamano+'" title="'+formatearValorRenderer(arrSituacionEvento,val)+'" alt="'+formatearValorRenderer(arrSituacionEvento,val)+'">';
                                                                        }
                                                            },
                                                            
                                                             {
                                                                header:'Situaci&oacute;n audiencia',
                                                                width:170,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        
                                                                        	var comp='';
                                                                            if(registro.data.comentariosAdicionales!='')
                                                                            {
                                                                            	comp='&nbsp;&nbsp;<img src="../images/icon_comment.gif" title="'+cv(registro.data.comentariosAdicionales,true,true)+'" alt="'+cv(registro.data.comentariosAdicionales,true,true)+'" />';
                                                                            }
                                                                            return mostrarValorDescripcion(formatearValorRenderer(arrSituacionEvento,val))+comp;
                                                                        }
                                                            },
                                                            {
                                                                header:'C&oacute;digo<br>Generado',
                                                                width:110,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'codigoGenerado',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var comp='';
                                                                            
                                                                            if((registro.data.urlReunionVirtual!='')&&(val!=''))
                                                                            	comp='&nbsp;&nbsp;&nbsp;<a href="'+registro.data.urlReunionVirtual+'" target="_blank" ><img src="../images/user_go.png" width="14" height="14" title="Ingresar a reunión" alt="Ingresar a reunión" /></a>';
                                                                            
                                                                        	if(val=='')
                                                                            	return '<span style="color:#900; font-weight:bold">No</span>&nbsp;&nbsp;&nbsp;<a href="javascript:generarCodigoAcceso(\''+bE(registro.data.idEvento)+'\')"><img width="14" height="14" src="../images/key_go.png" title="Generar c&oacute;digo de acceso" alt="Generar c&oacute;digo de acceso"></a>';
                                                                             return '<span style="color:#900; font-weight:bold">S&iacute;</span>&nbsp;&nbsp;&nbsp;<a href="javascript:imprimirCodigoAcceso(\''+bE(registro.data.idEvento)+'\')"><img width="14" height="14" src="../images/printer.png" title="Reimprimir c&oacute;digo de acceso" alt="Reimprimir c&oacute;digo de acceso"></a>'+comp;
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'',
                                                                width:60,
                                                                sortable:true,
                                                                dataIndex:'situacion',
                                                                css:'text-align:left;vertical-align:middle !important;',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	
                                                                            var comp2='';
                                                                           	switch(val)
                                                                            {
                                                                            	/*case '4':
                                                                                	if(registro.data.urlCanal!='')
                                                                                		comp2='<a href="javascript:abrirVentanaSala(\''+bE(registro.data.sala)+'\')"><img src="../images/film_go.png" title="Visualizar audiencia" alt="Visualizar audiencia" /></a>'
                                                                                break;*/
                                                                                case '2':
                                                                                	if(registro.data.urlMultimedia!='')
                                                                                		comp2='<a href="javascript:abrirVideoGrabacion(\''+bE(registro.data.urlMultimedia)+'\')"><img src="../images/control_play_blue.png" title="Visualizar grabaci&oacute;n" alt="Visualizar grabaci&oacute;n" /></a>'
                                                                              	break;
                                                                            }
                                                                        	return comp2;
                                                                        	
                                                                        }
                                                            },
                                                            {
                                                                header:'Carpeta Judicial',
                                                                width:150,
                                                                sortable:true,
                                                                hidden:false,
                                                                dataIndex:'carpetaAdministrativa'
                                                            },
                                                            {
                                                                header:'Unidad de Gesti&oacute;n',
                                                                width:300,
                                                                sortable:true,
                                                                hidden:true,
                                                                dataIndex:'unidadGestion',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrJuzgados,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de<br>la audiencia',
                                                                width:110,
                                                                sortable:true,
                                                                align:'center',
                                                                dataIndex:'fechaEvento',
                                                                renderer:function(val)
                                                                	{
                                                                    	return val.format('d/m/Y');
                                                                    }
                                                            },
                                                            {
                                                                header:'Hora programada de audiencia',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'horaInicial',
                                                                renderer:function(val,meta,registro)
                                                                	{

                                                                    	var comp='';
                                                                        if(val.format('d')!=registro.data.horaFinal.format('d'))
                                                                        {
                                                                        	comp=' del '+registro.data.horaFinal.format('d/m/Y');
                                                                        }

                                                                    	return 'De las '+val.format('H:i')+' hrs. a las '+registro.data.horaFinal.format('H:i')+' hrs.'+comp
                                                                    }
                                                            },
                                                            
                                                            {
                                                                header:'Hora de realizaci&oacute;n de audiencia',
                                                                width:280,
                                                                sortable:true,
                                                                dataIndex:'horaInicioReal',
                                                                renderer:function(val,meta,registro)
                                                                	{

                                                                    	if(!val)
                                                                        {
                                                                        	return '(Datos no disponibles)';
                                                                        }
                                                                    	var comp='';
                                                                        if(val.format('d')!=registro.data.horaTerminoReal.format('d'))
                                                                        {
                                                                        	comp=' del '+registro.data.horaTerminoReal.format('d/m/Y');
                                                                        }

                                                                    	return 'De las '+val.format('H:i')+' hrs. a las '+registro.data.horaTerminoReal.format('H:i')+' hrs.'+comp
                                                                    }
                                                            },
                                                            {
                                                                header:'Tipo de audiencia',
                                                                width:350,
                                                                sortable:true,
                                                                dataIndex:'tipoAudiencia',
                                                                renderer:function(val)
                                                                		{
                                                                        
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Edificio',
                                                                width:250,
                                                                sortable:true,
                                                                dataIndex:'edificio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	var lblSala=mostrarValorDescripcion(val);
                                                                        	return lblSala;
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'Sala',
                                                                width:220,
                                                                sortable:true,
                                                                dataIndex:'sala',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Juez',
                                                                width:320,
                                                                sortable:true,
                                                                dataIndex:'juez'
                                                            }
                                                            
                                                           
                                                            
                                                        ]
                                                    );
                                                    
        var tblGrid=	new Ext.grid.GridPanel	(
                                                            {
                                                                id:'gridAudiencias',
                                                                store:alDatos,
                                                                region:'center',
                                                                frame:false,
                                                                cm: cModelo,
                                                                stripeRows :false,
                                                                loadMask:true,
                                                                columnLines : true,      
                                                                view:new Ext.grid.GroupingView({
                                                                                                    forceFit:false,
                                                                                                    showGroupName: false,
                                                                                                    enableGrouping :true,
                                                                                                    enableNoGroups:false,
                                                                                                    enableGroupingMenu:false,
                                                                                                    hideGroupedColumn: false,
                                                                                                    startCollapsed:false
                                                                                                })
                                                            }
                                                        );

	tblGrid.getSelectionModel().on('rowselect',function(sm,nFila,registro)
    											{
                                                	
                                                    
                                                }
    							)

	tblGrid.getSelectionModel().on('rowdeselect',function()
    											{
                                                	
                                                }
    							)

	return 	tblGrid;

}

function recargarGridAudiencias()
{
	gEx('gridAudiencias').getStore().load	(
                                                {
                                                    url:'../paginasFunciones/funcionesModulosEspeciales_SICORE.php',
                                                    params:	{
                                                                funcion:'209'
                                                            }
                                                }
                                                
                                            );
}

function abrirVideoGrabacion(url)
{
	var obj={};    
    obj.ancho='100%';
    obj.alto='100%';
    obj.url='../modulosEspeciales_SICORE/visorGrabacionAudienciaSicor.php';
    obj.params=[['urlMultimedia',url],['cPagina','sFrm=true'],[]]
    if(window.parent)
    	window.parent.abrirVentanaFancy(obj);
    else
	    abrirVentanaFancy(obj);
}


function generarCodigoAcceso(idEvento)
{
	var pos=obtenerPosFila(gEx('gridAudiencias').getStore(),'idEvento',bD(idEvento));
    var fila=gEx('gridAudiencias').getStore().getAt(pos);
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
        	fila.set('codigoGenerado',arrResp[1]);
            gEx('gridAudiencias').getStore().reload();
            imprimirCodigoAcceso(idEvento);
            
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=210&idExpediente='+fila.data.idExpediente+'&cveMateria='+fila.data.cveMateria+'&idEvento='+bD(idEvento)+'&carpetaAdministrativa='+fila.data.carpetaAdministrativa,true);
}


function imprimirCodigoAcceso(idEvento)
{
	
	var pos=obtenerPosFila(gEx('gridAudiencias').getStore(),'idEvento',bD(idEvento));
    var fila=gEx('gridAudiencias').getStore().getAt(pos);	
    var cadObj='{"codigoGenerado":"'+fila.data.codigoGenerado+'","tipoAudiencia":"'+fila.data.tipoAudiencia+'","unidadGestion":"'+
    			formatearValorRenderer(arrJuzgados,fila.data.unidadGestion)+'","idEvento":"'+fila.data.idEvento+
                '","fechaAudiencia":"'+fila.data.horaInicial.format('Y-m-d H:i')+'"}';
    var arrParametros=[['cadObj',bE(cadObj)]]
    enviarFormularioDatos('../modulosEspeciales_SICORE/generarCodigoAccesoAudienciaVirtual.php',arrParametros,'POST','frameDTD');
    primeraCargaFrame=false;
}


function frameLoad(iFrame)
{
	if(!primeraCargaFrame)
    {
    	setTimeout(function()
        			{
                       
                        iFrame.contentWindow.print();
                    },2000
                   );

    }
    else
    	primeraCargaFrame=false;
	
}