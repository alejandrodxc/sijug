<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT id__477_tablaDinamica,tipoJuicio FROM _477_tablaDinamica ORDER BY tipoJuicio";
	$arrTipoJuicio=$con->obtenerFilasArreglo($consulta);
	
	
	$consulta="SELECT DISTINCT anio,anio FROM 7044_procesosLibrosGobierno WHERE tipoLibro=4";
	$arrAnios=$con->obtenerFilasArreglo($consulta);
	$anioActual=date('Y');
	$consulta="SELECT idTipoCarpeta,nombreTipoCarpeta FROM 7020_tipoCarpetaAdministrativa";
	$arrTipoExpediente=$con->obtenerFilasArreglo($consulta);
?>
var arrTipoExpediente=<?php echo $arrTipoExpediente?>;

var anioActual='<?php echo $anioActual?>';
var arrAnios=<?php echo $arrAnios?>;
var arrTipoJuicio=<?php echo $arrTipoJuicio?>;

Ext.onReady(inicializar);

function inicializar()
{
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Libro de env&iacute;o de Expediente a Archivo Judicial para destrucci&oacute;n</b></span>',
                                                items:	[
                                                            crearGridLibroGobierno()
                                                        ]
                                            }
                                         ]
                            }
                        )   
}

function crearGridLibroGobierno()
{
	var cmbAnio=crearComboExt('cmbAnio',arrAnios,0,0,150);
    var pos=obtenerPosFila(gEx('cmbAnio').getStore(),'id',anioActual);
    if(pos!=-1)
    {
    	cmbAnio.setValue(anioActual);
    }
    else
    {
    	if(arrAnios.length>0)
        {
        	cmbAnio.setValue(arrAnios[arrAnios.length-1]);
        }
    }
    
    cmbAnio.on('select',function()
    					{
                        	gEx('gLibro').getStore().reload();
                        }
    			)
    
    
    
    
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                               			{name:'idRegistro'},
                                                        {name:'idFormulario'},
                                                        {name:'folio'},
		                                                {name: 'noExpediente'},
		                                                {name:'actor'},
		                                                {name:'demandado'},
                                                        {name: 'tipoJuicio'},
                                                        {name: 'secretaria'},
                                                        {name: 'tipoExpediente'},
                                                        {name: 'fechaRegistro',type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'fechaEnvio',type:'date', dateFormat:'Y-m-d'},
                                                        {name: 'comentariosAdicionales'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'fechaRecepcion', direction: 'ASC'},
                                                            groupField: 'fechaRecepcion',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:false
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='18';
                                        proxy.baseParams.anioJudicial=cmbAnio.getValue()==''?anioActual:cmbAnio.getValue();
                                        
                                    }
                        )   
    
    var paginador=	new Ext.PagingToolbar	(
                                              {
                                                    pageSize: 100,
                                                    store: alDatos,
                                                    displayInfo: true,
                                                    disabled:false
                                                }
                                             )       
    
	var expander = new Ext.ux.grid.RowExpander({
                                                    column:2,
                                                    tpl : new Ext.Template(
                                                        '<table width="100%" >'+
                                                        '<tr><td  style="padding:10px">{datosParticipantes}</td></tr>'+
                                                        '</table>'
                                                    )
                                                }); 
       
	var cModelo= new Ext.grid.ColumnModel   	(
                                                        [
                                                            
                                                            new  Ext.grid.RowNumberer(),
                                                            expander,
                                                            {
                                                                header:'Folio',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'folio',
                                                                renderer:function(val,meta,registro)
                                                                		{
                                                                        	return '<a href="javascript:abrirFormularioProcesoLibro(\''+bE(registro.data.idFormulario)+'\',\''+bE(registro.data.idRegistro)+'\')">'+val+'</a>';
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de registro',
                                                                width:150,
                                                                sortable:true,
                                                                renderer:function(val)
                                                                		{
                                                                        	return val.format('d/m/Y H:i');
                                                                        },
                                                                dataIndex:'fechaRegistro'
                                                            }
                                                            
                                                            ,
                                                            {
                                                                header:'No. Expediente',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'noExpediente'
                                                            },
                                                             {
                                                                header:'Tipo',
                                                                width:150,
                                                                sortable:true,
                                                                dataIndex:'tipoExpediente',
                                                                renderer:function(val)
                                                                		{
                                                                        	return formatearValorRenderer(arrTipoExpediente,val);
                                                                        }
                                                            },
                                                            {
                                                                header:'Secretar&iacute;a',
                                                                width:80,
                                                                sortable:true,
                                                                dataIndex:'secretaria'
                                                            },
                                                            {
                                                                header:'Actor',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'actor',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Demandado',
                                                                width:300,
                                                                sortable:true,
                                                                dataIndex:'demandado',
                                                                renderer:mostrarValorDescripcion
                                                            },
                                                            {
                                                                header:'Tipo de Juicio',
                                                                width:260,
                                                                sortable:true,
                                                                dataIndex:'tipoJuicio',
                                                                renderer:function(val)
                                                                		{
                                                                        	return mostrarValorDescripcion(formatearValorRenderer(arrTipoJuicio,val));
                                                                        }
                                                            },
                                                            {
                                                                header:'Fecha de env&iacute;o a archivo',
                                                                width:140,
                                                                sortable:true,
                                                                dataIndex:'fechaEnvio',
                                                                renderer:function(val)
                                                                		{
                                                                        	if(val)
                                                                        		return val.format('d/m/Y');
                                                                        }
                                                            },
                                                            
                                                            {
                                                                header:'Comentarios adicionales',
                                                                width:350,
                                                                sortable:true,
                                                                renderer:mostrarValorDescripcion,
                                                                dataIndex:'comentariosAdicionales'
                                                            }
                                                        ]
                                              	);
                                                    
	var tblGrid=	new Ext.grid.GridPanel	(
                                                {
                                                    id:'gLibro',
                                                    store:alDatos,
                                                    border:false,
                                                    region:'center',
                                                    frame:false,
                                                    cm: cModelo,
                                                    bbar:[paginador],
                                                    plugins:[expander],
                                                    stripeRows :true,
                                                    loadMask:true,
                                                    tbar:	[
                                                    			{
                                                                	xtype:'label',
                                                                    html:'<b>A&ntilde;o Judicial:</b>&nbsp;&nbsp;&nbsp;&nbsp;'
                                                                },
                                                                cmbAnio
                                                    		],
                                                    columnLines : true,                                                    
                                                    view:new Ext.grid.GroupingView({
                                                                                        forceFit:false,
                                                                                        showGroupName: false,
                                                                                        enableGrouping :false,
                                                                                        enableNoGroups:false,
                                                                                        enableGroupingMenu:false,
                                                                                        hideGroupedColumn: false,
                                                                                        startCollapsed:false
                                                                                    })
                                                }
                                            );
	
    
    tblGrid.getStore().load(	{
    								params:	{
                                    			url:'../paginasFunciones/funcionesModulosEspeciales_LibrosGobierno.php',
                                                start:0, 
                                                limit:100
                                                
                                    		}
    							}
                           )  
    
    return 	tblGrid;
}


function abrirFormularioProcesoLibro(iF,iR)
{
	var accion='auto';
    if(bD(iR)=='-1')
    	accion="agregar";
	var arrDatos=[["idFormulario",bD(iF)],["idRegistro",bD(iR)],["actor",bE(0)],['dComp',bE(accion)]];
    
    
    var obj={};
    obj.url="../modeloPerfiles/vistaDTDv3.php";
    obj.params=arrDatos;
    obj.ancho='100%';
    obj.alto='100%';
    
    abrirVentanaFancySuperior(obj);
    
    
    
}