<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
	$idFormulario=$con->obtenerValor($consulta);
	
	$consulta="SELECT claveMateria,materia,promocioneFirmadas FROM _".$idFormulario."_tablaDinamica WHERE materiaElegible=1 ORDER BY materia";
	$arrMaterias=$con->obtenerFilasArreglo($consulta);
	
	$arrAnios="";
	$oAnio="";
	for($x=2015;$x<=date("Y");$x++)
	{
		$oAnio="['".$x."','".$x."']";
		if($arrAnios=="")
			$arrAnios=$oAnio;
		else
			$arrAnios.=",".$oAnio;
	}
	
	$arrPaquetes="";
	$consulta="SELECT idRegistro,folioPaquete,cp.nombrePaquete,p.totalExpedientes,
				(SELECT COUNT(*) FROM 7006_usuariosVSCarpetasAdministrativas WHERE idPaquete=p.idRegistro) AS asignados
				 FROM 7006_paquetesContratados p,_481_tablaDinamica cp 
				WHERE idUsuario=100006 AND p.situacion=1 AND cp.id__481_tablaDinamica=idPaquete";
	$res=$con->obtenerFilas($consulta);			
	while($fila=mysql_fetch_row($res))
	{
		if($fila[3]-$fila[4]>0)
		{
			$o="['".$fila[0]."','[".$fila[1]."] ".$fila[2]." (".($fila[3]-$fila[4])." expedientes libres)']";
			if($arrPaquetes=="")
				$arrPaquetes=$o;
			else
				$arrPaquetes.=",".$o;
		}
	}

	$arrPaquetes='['.$arrPaquetes.']';

	$consulta="SELECT claveMateria,materia,promocioneFirmadas FROM _".$idFormulario."_tablaDinamica  ORDER BY materia";
	$arrMateriasTotal=$con->obtenerFilasArreglo($consulta);

?>
var objDetalleExpediente='';
var arrMateriasTotal=<?php echo $arrMateriasTotal?>;
var filaSeleccionada='';
var arrSituacionExpedientes=[['1','Acceso autorizado','../images/accept_green.png'],['2','En espera de env&iacute;o a juzgado','../images/control_pause.png'],
							['3','En espera de autorizaci&oacute;n del juzgado','../images/user_gray.png'],['4','Acceso revocado por el juzgado','../images/lock.png'],
                            ['5','Acceso suspendido, vigencia expirada','../images/calendar_delete.png'],['6','Acceso rechazado','../images/cross.png']];
var arrPaquetes=<?php echo $arrPaquetes?>;
var arrAnios=[<?php echo $arrAnios?>];
var arrMaterias=<?php echo $arrMaterias?>;

Ext.onReady(inicializar);

function inicializar()
{
	new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                title: '<span class="letraRojaSubrayada8" style="font-size:14px"><b>Solicitudes de acceso a Expedientes</b></span> <span id="lblExpediente" style="color:#000; font-weight:bold;font-size:14px"></span><span id="lblNoExpediente" style="color:#900; font-weight:bold;font-size:14px"></span>',
                                                items:	[
                                                			crearGridExpedientes()
                                                		]
                                             }
                                        ]
						}
                     )                                        
                                        
}	

function crearGridExpedientes()
{
	      
	var lector= new Ext.data.JsonReader({
                                            
                                            totalProperty:'numReg',
                                            fields: [
                                            			{name: 'idRegistro'},
                                                        {name: 'folioSolicitud'},
                                               			{name:'idCarpetaAdministrativa'},
		                                                {name: 'carpetaAdministrativa'},
		                                                {name:'cveMateria'},
		                                                {name:'fechaVencimiento', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'situacion'},
                                                        {name: 'nombreUnidad'},
                                                        {name: 'anioExpediente'},
                                                        {name: 'figuraExpediente'},
                                                        {name: 'detalleExpediente'},
                                                        {name: 'lblDetalleFigura'},
                                                        {name: 'lblPersonaRelacion'},
                                                        {name: 'noPaquete'},
                                                        {name: 'fechaRespuesta', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'fechaSolicitud', type:'date', dateFormat:'Y-m-d H:i:s'},
                                                        {name: 'idDocumentoSolicitud'},
                                                        {name: 'nombreDocumentoSolicitud'}
                                            		],
                                            root:'registros'
                                            
                                        }
                                      );
	 
    
    var filters = new Ext.ux.grid.GridFilters	(
                                                        {
                                                            filters:	[
                                                            				{
                                                                            	type:'string',
                                                                                dataIndex:'carpetaAdministrativa'
                                                                            },
                                                                            {
                                                                            	type:'string',
                                                                                dataIndex:'nombreUnidad'
                                                                            },
                                                                            {
                                                                            	type:'list',
                                                                                dataIndex:'anioExpediente',
                                                                                options:arrAnios,
                                                                                phpMode:true
                                                                            },
                                                                            {
                                                                            	type:'list',
                                                                                dataIndex:'cveMateria',
                                                                                options:arrMaterias,
                                                                                phpMode:true
                                                                            },
                                                                            {
                                                                            	type:'list',
                                                                                dataIndex:'situacion',
                                                                                options:arrSituacionExpedientes,
                                                                                phpMode:true
                                                                            }
                                                            			]
                                                        }
                                                    ); 
    
                                                                                      
	var alDatos=new Ext.data.GroupingStore({
                                                            reader: lector,
                                                            proxy : new Ext.data.HttpProxy	(

                                                                                              {

                                                                                                  url: '../paginasFunciones/funcionesModulosEspeciales_SICORE.php'

                                                                                              }

                                                                                          ),
                                                            sortInfo: {field: 'cveMateria', direction: 'ASC'},
                                                            groupField: 'cveMateria',
                                                            remoteGroup:false,
				                                            remoteSort: true,
                                                            autoLoad:true
                                                            
                                                        }) 
	alDatos.on('beforeload',function(proxy)
    								{
                                    	proxy.baseParams.funcion='10';
                                        
                                    }
                        )   
       
    alDatos.on('load',function(proxy)
    								{
                                    		if(filaSeleccionada!='')
                                            {
                                            	var pos=obtenerPosFila(gEx('gExpedientes').getStore(),'idRegistro',filaSeleccionada);
                                                gEx('gExpedientes').getSelectionModel().selectRow(pos);
                                                mostrarDocumento(bE(filaSeleccionada));
                                            	filaSeleccionada='';
                                            }
                                        
                                    }
                        )   
       
       
    var cModelo= new Ext.grid.ColumnModel   	(
                                                    [
                                                        new  Ext.grid.RowNumberer(),
                                                        {
                                                            header:'A&ntilde;o del<br />expediente',
                                                            width:90,
                                                            sortable:true,
                                                            dataIndex:'anioExpediente'
                                                        },
                                                        {
                                                            header:'Expediente',
                                                            width:130,
                                                            sortable:true,
                                                            dataIndex:'carpetaAdministrativa'
                                                        },
                                                        {
                                                            header:'Materia',
                                                            width:300,
                                                            sortable:true,
                                                            dataIndex:'cveMateria',
                                                            renderer:function(val)
                                                            		{
                                                                    	return mostrarValorDescripcion(formatearValorRenderer(arrMateriasTotal,val));
                                                                    }
                                                        },
                                                        {
                                                            header:'Juzgado',
                                                            width:450,
                                                            sortable:true,
                                                            dataIndex:'nombreUnidad'
                                                        },
                                                        
                                                        
                                                        
                                                        {
                                                            header:'Situaci&oacute;n',
                                                            width:220,
                                                            sortable:true,
                                                            dataIndex:'situacion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var comp='';
                                                                        if(parseInt(val))
                                                                        {
                                                                        	if(val==2)
                                                                            {
                                                                            	comp+='&nbsp;&nbsp;<a href="javascript:enviarSolicitudJuzgado(\''+bE(registro.data.idRegistro)+'\')"><img src="../images/document_go.png" width="14" height="14" title="Enviar promoci&oacute;n a Juzgado" alt="Enviar promoci&oacute;n a Juzgado"></a>';
                                                                            }
                                                                        }
                                                                    	return formatearValorRenderer(arrSituacionExpedientes,val)+comp;
                                                                    }
                                                        },
                                                        {
                                                            header:'Folio de Solicitud',
                                                            width:120,
                                                            sortable:true,
                                                            dataIndex:'folioSolicitud',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	return val;
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'situacion',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	var pos=existeValorMatriz(arrSituacionExpedientes,val);
                                                                        return '<img src="'+arrSituacionExpedientes[pos][2]+'" />';
                                                                    }
                                                            
                                                        },
                                                        {
                                                            header:'',
                                                            width:30,
                                                            sortable:true,
                                                            dataIndex:'idDocumentoSolicitud',
                                                            renderer:function(val,meta,registro)
                                                            		{
                                                                    	if(val=='')
                                                                        	return '';
                                                                    	return '<a href="javascript:mostrarDocumento(\''+bE(registro.data.idRegistro)+'\')"><img src="../imagenesDocumentos/16/file_extension_pdf.png" title="Ver documento de solicitud" alt="Ver documento de solicitud"></a>';
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de solicitud',
                                                            width:110,
                                                            sortable:true,
                                                            dataIndex:'fechaSolicitud',
                                                            renderer:function(val)
                                                            		{
                                                                    	if(val)
	                                                                    	return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                         {
                                                            header:'Fecha de respuesta',
                                                            width:140,
                                                            //hidden:true,
                                                            sortable:true,
                                                            dataIndex:'fechaRespuesta',
                                                            renderer:function(val)
                                                            		{	
                                                                    	if(val)
	                                                                    	return val.format('d/m/Y H:i');
                                                                    }
                                                        },
                                                        {
                                                            header:'No. de paquete',
                                                            width:110,
                                                            hidden:true,
                                                            sortable:true,
                                                            dataIndex:'noPaquete',
                                                           	renderer:function(val)	
                                                            		{
                                                                    	return val;
                                                                    }
                                                        },
                                                        {
                                                            header:'Fecha de vencimiento<br />del paquete',
                                                            hidden:true,
                                                            
                                                            width:150,
                                                            sortable:true,
                                                            dataIndex:'fechaVencimiento',
                                                           	renderer:function(val)	
                                                            		{
                                                                    	if(val)
	                                                                    	return val.format('d/m/Y');
                                                                    }
                                                        }
                                                    ]
                                                );
                                                
    var tblGrid=	new Ext.grid.GridPanel	(
                                                        {
                                                            id:'gExpedientes',
                                                            store:alDatos,
                                                            region:'center',
                                                            frame:false,
                                                            cm: cModelo,
                                                            plugins:[filters],
                                                            stripeRows :true,
                                                            loadMask:true,
                                                            tbar:	[
                                                            			{
                                                                        	icon:'../images/user_go.png',
                                                                            cls:'x-btn-text-icon',
                                                                            text:'Solicitar acceso a nuevo expediente',
                                                                            handler:function()
                                                                            		{
                                                                                    	mostrarVentanaSolicitudAcceso()
                                                                                    }
                                                                            
                                                                        }
                                                                        
                                                            		],
                                                            columnLines : true,                                                            
                                                            view:new Ext.grid.GroupingView({
                                                                                                forceFit:false,
                                                                                                showGroupName: false,
                                                                                                enableGrouping :true,
                                                                                                enableNoGroups:false,
                                                                                                enableRowBody:true,
	                                                                                            getRowClass:formatearFilaExpediente,
                                                                                                enableGroupingMenu:false,
                                                                                                hideGroupedColumn: true,
                                                                                                startCollapsed:false
                                                                                            })
                                                        }
                                                    );
    return 	tblGrid;	
}

function formatearFilaExpediente(record, rowIndex, p, ds)
{
	var xf = Ext.util.Format;
    var lblDetalle='';
    var oDetalle=eval('['+bD(record.data.detalleExpediente)+']')[0];
    if(oDetalle.actor)
    {
    	lblDetalle=(oDetalle.actor==''?'(NO ESPECIFICADO)':oDetalle.actor)+' <span style="color:#900"><b>VS</b></span> '+
                                        			(oDetalle.demandado==''?'(NO ESPECIFICADO)':oDetalle.demandado)+
                                                    '. <b>Tipo de Juicio:</b>&nbsp;'+oDetalle.tipoJuicio;
	    
    }
    else
    {
    	                                            
    	lblDetalle='<b>Imputado:&nbsp;</b>'+(oDetalle.imputado==''?'(NO ESPECIFICADO)':oDetalle.imputado)+', <b>V&iacute;ctima:&nbsp;</b> '+
                                        			(oDetalle.victima==''?'(NO ESPECIFICADO)':oDetalle.victima)+
                                                    '. <b>Delito:</b>&nbsp;'+oDetalle.delito;
    }
    lblDetalle+='<br /><b>Participaci&oacute;n:&nbsp;</b>'+record.data.figuraExpediente;
    if(record.data.lblDetalleFigura!='')
    {
    	lblDetalle+=' ('+record.data.lblDetalleFigura+')';
    }
    
    if(record.data.lblPersonaRelacion!='')
    {
    	lblDetalle+=' de '+record.data.lblPersonaRelacion;
    }
    
    
    p.body = '<br><table width="100%"><tr><td width="30"></td><td style="line-height:160%">'+lblDetalle+'</td></tr></table><br>';
    return 'x-grid3-row-expanded';
}

function mostrarVentanaSolicitudAcceso()
{
	var cmbMateria=crearComboExt('cmbMateria',arrMaterias,150,5,350);
    
    cmbMateria.on('select',function(cmb,registro)
    						{
                            	gEx('cmbExpediente').getStore().removeAll();
                            	gEx('cmbExpediente').setValue('');
                                gE('lblDetalles').innerHTML='';
                                gEx('cmbParticipante').getStore().removeAll();
                            	gEx('cmbParticipante').setValue('');
                                function funcAjax()
                                {
                                    var resp=peticion_http.responseText;
                                    arrResp=resp.split('|');
                                    if(arrResp[0]=='1')
                                    {
                                        var arrDatos=eval(arrResp[1]);
                                        gEx('cmbJuzgado').setValue('');
                                        gEx('cmbJuzgado').getStore().loadData(arrDatos);
                                        
                                        var arrFiguras=eval(arrResp[2]);
                                        
                                        gEx('cmbParticipacion').getStore().loadData(arrFiguras);
                                        gEx('cmbParticipacion').setValue('');
                                        gEx('cmbDetalles').hide();
                                        gEx('cmbDetalles').getStore().removeAll();
                                         gEx('cmbDetalles').setValue('');
                                        
                                    }
                                    else
                                    {
                                        msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                    }
                                }
                                obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=6&cveMateria='+registro.data.id,true);
                            }
    				)
    
    var cmbJuzgado=crearComboExt('cmbJuzgado',[],150,35,420);
    cmbJuzgado.on('select',buscarExpedientesJuzgado);
    var cmbAnio=crearComboExt('cmbAnio',arrAnios,150,65,110);
    cmbAnio.on('select',buscarExpedientesJuzgado);
    
    
    var cmbExpediente=crearComboExt('cmbExpediente',[],420,65,200);
    cmbExpediente.on('select',function(cmb,registro)
    							{
                                
                                	
                                	
                                    
                                    
                                    function funcAjax()
                                    {
                                        var resp=peticion_http.responseText;
                                        arrResp=resp.split('|');
                                        if(arrResp[0]=='1')
                                        {
                                        	objDetalleExpediente=arrResp[1];
                                        	var lblDetalle='';
                                            var oDetalle=eval('['+bD(arrResp[1])+']')[0];
                                            if(oDetalle.actor)
                                            {
                                                lblDetalle=(oDetalle.actor==''?'NO ESPECIFICADO':oDetalle.actor)+' <b><span style="color:#900">VS</span></b> '+
                                                            (oDetalle.demandado==''?'NO ESPECIFICADO':oDetalle.demandado)+
                                                            '<br><br /><b>Tipo de Juicio:</b>&nbsp;'+oDetalle.tipoJuicio;
                                                
                                            }
                                            else
                                            {
                                                lblDetalle=	'<b><span style="color:#900">Imputado:</span></b>&nbsp;'+oDetalle.imputado+'<br><br />'+
                                                            '<b><span style="color:#900">V&iacute;ctima:</span></b>&nbsp;'+oDetalle.victima+'<br /><br />'+
                                                            '<b><span style="color:#900">Delito:</span></b>&nbsp;'+oDetalle.delito+'<br />'
                                                
                                            }
                                        	gE('lblDetalles').innerHTML=lblDetalle;
                                            buscarPartesRelacion();
                                            
                                        }
                                        else
                                        {
                                            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                        }
                                    }
                                    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=208&idExpediente='+registro.data.id+'&cveMateria='+gEx('cmbMateria').getValue(),true);
                                    
                                }
    					);
    var cmbParticipacion=crearComboExt('cmbParticipacion',[],240,185,150);
    cmbParticipacion.on('select',function(cmb,registro)
    								{
                                    	gEx('cmbDetalles').hide();
                                    	if(registro.data.valorComp.length>0)
                                        {
                                            gEx('cmbDetalles').getStore().loadData(registro.data.valorComp);
                                            gEx('cmbDetalles').show();
                                        }
                                        
                                        if(registro.data.valorComp2!='')
                                        {
                                        	gEx('lblRelacionado').show();
                                            gEx('cmbParticipante').setValue('');
                                            gEx('cmbParticipante').show();
                                        }
                                        else
                                        {
                                            gEx('lblRelacionado').hide();
                                            gEx('cmbParticipante').hide();
                                            gEx('cmbParticipante').setValue('');
                                        }
                                    	buscarPartesRelacion();
                                    }
    					)
    var cmbDetalles=crearComboExt('cmbDetalles',[],400,185,120);
    cmbDetalles.hide();
    var cmbParticipante=crearComboExt('cmbParticipante',[],150,215,300);
    cmbParticipante.hide();
    var cmbPaquete=crearComboExt('cmbPaquete',arrPaquetes,240,245,400);
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			{
                                                        	x:10,
                                                            y:10,
                                                            html:'Materia:'
                                                        },
                                                        cmbMateria,
                                                        {
                                                        	x:10,
                                                            y:40,
                                                            html:'Juzgado:'
                                                        },
                                                        cmbJuzgado,
                                                        {
                                                        	x:10,
                                                            y:70,
                                                            html:'A&ntilde;o del expediente:'
                                                        },
                                                        cmbAnio,
                                                        {
                                                        	x:280,
                                                            y:70,
                                                            html:'N&uacute;mero de expediente:'
                                                        },
                                                        cmbExpediente,
                                                        {
                                                        	x:10,
                                                            y:100,
                                                            xtype:'label',
                                                            html:'<div id="lblDetalles" style="width:610px; height:75px; background-color:#FFF; "></div>',
                                                            id:'txtDetalles'
                                                        },
                                                        {
                                                        	x:10,
                                                            y:190,
                                                            html:'Su participaci&oacute;n dentro del expediente es:'
                                                        },
                                                        cmbParticipacion,
                                                        cmbDetalles,
                                                        {
                                                        	x:10,
                                                            y:220,
                                                            hidden:true,
                                                            id:'lblRelacionado',
                                                            html:'Relacionado con:'
                                                        },
                                                        cmbParticipante/*,
                                                        {
                                                        	x:10,
                                                            y:250,
                                                            html:'Seleccione el paquete contratado:'
                                                        },
                                                        cmbPaquete*/
                                                        

                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vSolicitud_1',
										title: 'Solicitar acceso a seguimiento a expediente',
										width: 680,
										height:370,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
																}
															}
												},
										buttons:	[
														{
															
															text: '<?php echo $etj["lblBtnAceptar"]?>',
															handler: function()
																	{
																		var cmbMateria=gEx('cmbMateria');
                                                                        var cmbJuzgado=gEx('cmbJuzgado');
                                                                        var cmbExpediente=gEx('cmbExpediente');
                                                                        var cmbParticipacion=gEx('cmbParticipacion');
                                                                        var cmbDetalles=gEx('cmbDetalles');
                                                                        var cmbParticipante=gEx('cmbParticipante');
                                                                        var cmbPaquete=gEx('cmbPaquete');
                                                                        
                                                                        if(cmbMateria.getValue()=='')
                                                                        {
                                                                        	function resp()
                                                                            {
                                                                            	cmbMateria.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar la materia a la cual pertenece el expediente cuyo acceso desea solicitar',resp);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbJuzgado.getValue()=='')
                                                                        {
                                                                        	function resp2()
                                                                            {
                                                                            	cmbJuzgado.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar el juzgado al cual pertenece el expediente cuyo acceso desea solicitar',resp2);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbExpediente.getValue()=='')
                                                                        {
                                                                        	function resp3()
                                                                            {
                                                                            	cmbExpediente.focus();
                                                                            }
                                                                            msgBox('Debe seleccionar el expediente cuyo acceso desea solicitar',resp3);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbParticipacion.getValue()=='')
                                                                        {
                                                                        	function resp4()
                                                                            {
                                                                            	cmbParticipacion.focus();
                                                                            }
                                                                            msgBox('Debe indicar el papel que desempe&ntilde;a en el expediente cuyo acceso desea solicitar',resp4);
                                                                            return;
                                                                        }
                                                                        
                                                                        if(cmbDetalles.isVisible() && (cmbDetalles.getValue()==''))
                                                                        {
                                                                        	function resp5()
                                                                            {
                                                                            	cmbDetalles.focus();
                                                                            }
                                                                            msgBox('Debe indicar el papel que desempe&ntilde;a en el expediente cuyo acceso desea solicitar',resp5);
                                                                            return;
                                                                        }
                                                                        
                                                                        if((cmbParticipante.isVisible())&&(cmbParticipante.getValue()==''))
                                                                        {
                                                                        	function resp6()
                                                                            {
                                                                            	cmbParticipante.focus();
                                                                            }
                                                                            msgBox('Debe indicar la persona con la cual se relaciona dentro del expediente',resp6);
                                                                            return;
                                                                        }
                                                                        
                                                                        /*if(cmbPaquete.getValue()=='')
                                                                        {
                                                                        	function resp7()
                                                                            {
                                                                            	cmbPaquete.focus();
                                                                            }
                                                                            msgBox('Debe indicar el paquete con el cual el expediente ser&aacute; asociado',resp7);
                                                                            return;
                                                                        }*/
                                                                        var pos=obtenerPosFila(gEx('cmbExpediente').getStore(),'id',cmbExpediente.getValue());
                                                                        var registro=gEx('cmbExpediente').getStore().getAt(pos);
                                                                        var objDetalles=registro.data.valorComp2;
                                                                        var cadObj='{"materia":"'+cmbMateria.getValue()+'","juzgado":"'+cmbJuzgado.getValue()+'","expediente":"'+cmbExpediente.getValue()+
                                                                        			'","figuraJuridica":"'+cmbParticipacion.getValue()+'","detalleFiguraJuridica":"'+cmbDetalles.getValue()+
                                                                                    '","relacionadoCon":"'+cmbParticipante.getValue()+'","idPaquete":"0","objDetalles":"'+objDetalles+'","lblExpediente":"'+cmbExpediente.getRawValue()+
                                                                                    '","anio":"'+gEx('cmbAnio').getValue()+'","lblFiguraExpediente":"'+cv(cmbParticipacion.getRawValue())+
                                                                                    '","lblDetalleFigura":"'+cmbDetalles.getRawValue()+'","lblRelacionadoCon":"'+cmbParticipante.getRawValue()+
                                                                                    '","idDocumentoSolicitud":"@idDocumento","folioSICORE":"@folioSICORE","detalleExpediente":"'+(objDetalleExpediente)+'"}';
                                                                        
                                                                        var pos=existeValorMatriz(arrMaterias,cmbMateria.getValue());
                                                                        
                                                                        var cadSol='{"cveMateria":"'+cmbMateria.getValue()+'","unidadGestion":"'+cmbJuzgado.getValue()+
                                                                            		'","detalleExpediente":'+bD(objDetalleExpediente)+',"carpetaAdministrativa":"'+cmbExpediente.getRawValue()+
                                                                                    '","asociarDocumento":"'+(arrMaterias[pos][2]=='1'?0:1)+'"}';
                                                                        
                                                                        
                                                                        
                                                                        function funcAjax2()
                                                                        {
                                                                            var resp=peticion_http.responseText;
                                                                            arrResp=resp.split('|');
                                                                            if(arrResp[0]=='1')
                                                                            {
                                                                            	
                                                                                if(arrMaterias[pos][2]=='0')
                                                                        		{
                                                                                    cadObj=cadObj.replace('@idDocumento',arrResp[1]);
                                                                                    cadObj=cadObj.replace('@folioSICORE',arrResp[2]);
                                                                                    guardarSolicitudAcceso(cadObj);
                                                                    			}
                                                                                else
                                                                                {
                                                                                	cadObj=cadObj.replace('@folioSICORE',arrResp[2]);
                                                                                    ventanaAM.close();
                                                                                	mostrarVentanFirmaElectronicaSolicitud(cadObj,arrResp[1]);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                                                            }
                                                                        }
                                                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax2, 'POST','funcion=11&cadObj='+cadSol,true);
                                                                        
                                                                    }
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
																		 ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();	
}

function buscarExpedientesJuzgado()
{
	gEx('cmbExpediente').getStore().removeAll();
    gEx('cmbExpediente').setValue('');
    gE('lblDetalles').innerHTML='';
    gEx('cmbParticipante').getStore().removeAll();
    gEx('cmbParticipante').setValue('');
    gEx('cmbParticipacion').setValue();
    gEx('cmbDetalles').setValue('');
    gEx('cmbDetalles').hide('');
	var cmbMateria=gEx('cmbMateria');
	var cmbAnio=gEx('cmbAnio');
    var cmbJuzgado=gEx('cmbJuzgado');
    var cmbExpediente=gEx('cmbExpediente');
    
    
    
    if((cmbAnio.getValue()=='')||(cmbJuzgado.getValue()==''))
    {
    	return;
    }
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var arrDatos=eval(arrResp[1]);
            cmbExpediente.getStore().loadData(arrDatos);
            cmbExpediente.setValue('');
            gE('lblDetalles').innerHTML='';
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=7&cveMateria='+cmbMateria.getValue()+'&anio='+cmbAnio.getValue()+'&cveJuzgado='+cmbJuzgado.getValue(),true);	
}

function buscarPartesRelacion()
{
	if((gEx('cmbExpediente').getValue()=='')||(gEx('cmbParticipacion').getValue()==''))
    {
    	return;
    }
    var pos;
    var registro;

	pos=obtenerPosFila(gEx('cmbParticipacion').getStore(),'id',gEx('cmbParticipacion').getValue());
    registro=gEx('cmbParticipacion').getStore().getAt(pos);
    if(registro.data.valorComp2!='')
    {
    
        function funcAjax()
        {
            var resp=peticion_http.responseText;
            arrResp=resp.split('|');
            if(arrResp[0]=='1')
            {
            
                var arrDatos=eval(arrResp[1]);
                gEx('cmbParticipante').getStore().loadData(arrDatos);
                gEx('cmbParticipante').setValue('');
                
            }
            else
            {
                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
            }
        }
        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=8&cveMateria='+
                    gEx('cmbMateria').getValue()+'&iC='+gEx('cmbExpediente').getValue()+'&partes='+registro.data.valorComp2,true);
    
        
    }
    
}

function enviarSolicitudJuzgado(iR)
{
	 
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
            var pos=obtenerPosFila(gEx('gExpedientes').getStore(),'idRegistro',bD(iR));
    
    		var fila=gEx('gExpedientes').getStore().getAt(pos);
            fila.set('fechaRecepcionJuzgado',Date.parseDate(arrResp[1],'Y-m-d H:i:s'));
            fila.set('situacion','3');
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=12&iR='+iR,true);
}


function mostrarDocumento(iR)
{
	var pos=obtenerPosFila(gEx('gExpedientes').getStore(),'idRegistro',bD(iR));
    
    var fila=gEx('gExpedientes').getStore().getAt(pos);
    
	var nombreCampo='nombreDocumentoSolicitud';
    var nombreIdDocumento='idDocumentoSolicitud';
    
    
    var idDocumento=fila.get(nombreIdDocumento);
    var arrNombre=fila.get(nombreCampo).split('.');
    if(window.parent && window.parent.mostrarVisorDocumentoProceso)
    {
    	window.parent.mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    }
    else
    	mostrarVisorDocumentoProceso(arrNombre[arrNombre.length-1],idDocumento);
    
}

function mostrarVentanFirmaElectronicaSolicitud(cadObj,idArchivo)
{
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
											defaultType: 'label',
											items: 	[
                                            			
                                                        
                                                        new Ext.ux.IFrameComponent({ 
        																				x:10,
                                                                                        y:0,
                                                                                        width:810,
                                                                                        height:330,
                                                                                        id: 'hSpVisor', 
                                                                                        url: '../paginasFunciones/white.php'
                                                                                })
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vSolicitud_2',
										title: 'Firmar solicitud de acceso',
										width: 850,
										height:430,

										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	var nombreDocumento='solicitudAcceso.pdf';
                                                                    var arrNombreDoc=nombreDocumento.split('.');
                                                                    extensionDoc=arrNombreDoc[arrNombreDoc.length-1];
                                                                    gEx('hSpVisor').load	(
                                                                                                {
                                                                                                    url:'../visoresGaleriaDocumentos/visorDocumentosGeneral.php',
                                                                                                    params:	{
                                                                                                                iD:bE(idArchivo),
                                                                                                                extension:extensionDoc,
                                                                                                                oBarra:1,
                                                                                                                cPagina:'sFrm=true'
                                                                                                            }
                                                                                                }
                                                                                            );
                                                                
                                                                    
                                                                
                                                                }
															}
												},
										buttons:	[
                                        				{
															
															text: 'Firmar',  
                                                           	handler: function()
																	{
																		mostrarVentanaFirma(cadObj,idArchivo);
                                                                        
																	}
														},
														{
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}


function mostrarVentanaFirma(cadObj,idArchivo)
{
	var arrAccionesFirma=[['1','Firmar median FIEL'],['6','Firmar median FIREL']];
	var cmbAccionFirma=crearComboExt('cmbAccionFirma',arrAccionesFirma,220,5,220);
    cmbAccionFirma.on('select',function(cmb,registro)
    							{
                                	gEx('fSetFirma').hide();
									gEx('fSetFirmaFirel').hide();
									
									//gEx('vDocumento3').setHeight(250);
                                	switch(registro.data.id)
                                    {
                                    	case '1':                                        	
											gEx('fSetFirmaFirel').hide();
											gEx('fSetFirma').show();
											gEx('vDocumento').setHeight(330);
                                        break;
                                       	case '6':                                        	
											gEx('fSetFirmaFirel').show();
											gEx('fSetFirma').hide();
											gEx('vDocumento').setHeight(330);
                                        break;
                                       
                                    }
									
								//	gEx('vDocumento').center();
                                }
    				)
    
	var form = new Ext.form.FormPanel(	
										{
											baseCls: 'x-plain',
											layout:'absolute',
                                            id:'vFormPanel',
											defaultType: 'label',
											items: 	[
                                            			
                                                        {
                                                        	
                                                            x:10,
                                                            y:10,
                                                            html:'<b>Medio de firma de la solicitud:</b>'
                                                            
                                                        },
                                                        cmbAccionFirma,
                                                        {
                                                        	id:'fSetFirma',
                                                        	xtype:'fieldset',
                                                            width:790,
                                                            x:10,
                                                            y:40,
                                                            height:150,
															hidden:true,
                                                            defaultType: 'label',
                                                            layout:'absolute',
                                                            items:	[
                                                         				
                                                                        {
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Ingrese su archivo de certificado digital (*.cer):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:10,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="fileCer" accept=".cer" style="width:250px">'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Ingrese su archivo de llave privada (*.key):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:40,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="fileKey" accept=".key" style="width:250px">'
                                                                        },
                                                                        {
                                                                            x:10,
                                                                            y:70,
                                                                            html:'Ingrese la contrase&ntilde;a de llave privada:'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:65,
                                                                            width:250,
                                                                            id:'txtPassword',
                                                                            xtype:'textfield',
                                                                            inputType:'password'
                                                                        }   
                                                            		]
                                                        },
                                                        {
                                                        	id:'fSetFirmaFirel',
                                                        	xtype:'fieldset',
                                                            width:790,
                                                            x:10,
                                                            y:40,
                                                            height:120,
															hidden:true,
                                                            defaultType: 'label',
                                                            layout:'absolute',
                                                            items:	[
                                                         				{
                                                                            x:10,
                                                                            y:10,
                                                                            html:'Ingrese su archivo de llave privada (*.pfx):'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:10,
                                                                            html:'<input style="font-size:11px !important;" type="file" id="filePFX" accept=".pfx" style="width:250px">'
                                                                        },
                                                                        
                                                                        {
                                                                            x:10,
                                                                            y:40,
                                                                            html:'Ingrese la contrase&ntilde;a de llave privada:'
                                                                        },
                                                                        {
                                                                            x:250,
                                                                            y:35,
                                                                            width:250,
                                                                            id:'txtPasswordFirel',
                                                                            xtype:'textfield',
                                                                            inputType:'password'
                                                                        }   
                                                            		]
                                                        }
                                                        
													]
										}
									);
	
	var ventanaAM = new Ext.Window(
									{
                                    	id:'vSolicitud_3',
										title: 'Firmar solicitud de acceso',
										width: 850,
										height:280,
										layout: 'fit',
										plain:true,
										modal:true,
										bodyStyle:'padding:5px;',
										buttonAlign:'center',
										items: form,
										listeners : {
													show : {
																buffer : 10,
																fn : function() 
																{
                                                                	
																}
															}
												},
										buttons:	[
                                        				
														{
															
															text: 'Aceptar',  
                                                            handler: function()
																	{
																		firmarDocumento(cadObj,idArchivo);
																	}
														},
                                                        {
															text: '<?php echo $etj["lblBtnCancelar"]?>',
															handler:function()
																	{
                                                                    	
																		ventanaAM.close();
																	}
														}
													]
									}
								);
	ventanaAM.show();
}

function firmarDocumento(cadObj,idArchivo)
{
	var form=gEx('vFormPanel');
	var cmbAccionFirma=gEx('cmbAccionFirma');
	if(cmbAccionFirma.getValue()=='')
    {
        function resp()
        {
            cmbAccionFirma.focus();
        }
        msgBox('Debe indicar el medio de firma de la solicitud',resp);
        return;
    }
    var objResultado={};
    objResultado.accion=cmbAccionFirma.getValue();
    objResultado.comentarios='';
    objResultado.cadenaFirma='';
    
    var documentoFinal=1;
    
    switch(parseInt(cmbAccionFirma.getValue()))
    {
        case 1:
            if(gE('fileCer').value=='')
            {
                function resp1Cer()
                {
                    gE('fileCer').focus();
                }
                msgBox('Debe ingresar el archivo de certificado digital (*.cer)',resp1Cer);
                return;
            }
            
            if(gE('fileKey').value=='')
            {
                function resp2Cer()
                {
                    gE('fileKey').focus();
                }
                msgBox('Debe ingresar el archivo de llave privada (*.key)',resp2Cer);
                return;
            }
            
            if(gEx('txtPassword').getValue().trim()=='')
            {
                function resp3Cer()
                {
                    gEx('txtPassword').focus();
                }
                msgBox('Debe ingresar la contrase&ntilde;a de llave privada',resp3Cer);
                return;
            }
            objResultado.cadenaFirma=''
            var cadObjFirma='{"documentoFinal":"1","idDocumento":"'+idArchivo+'","nombreDocumento":"solicitudAcceso.pdf","tipoFirma":"1"}';															
            
            var oObj=eval('['+cadObjFirma+']')[0];
            
            
            var formData = new FormData();
            
            formData.append('passwd',AES_Encrypt(gEx('txtPassword').getValue()));
            
            
            for(var campo in oObj)
            {
                
                formData.append(campo,oObj[campo]);
                
                
                
            }
            
            
            
            formData.append('fCer',gE('fileCer').files[0]);
            formData.append('fKey',gE('fileKey').files[0]);
            mostrarMensajeProcesando('Firmando documento, &eacute;sta operaci&oacute;n puede tardar unos minutos...');
            $.ajax	({
                        url: "../paginasFunciones/procesarDocumentoFirmaElectronicaSICOREGeneral.php",
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(data)
                                {
                                    ocultarMensajeProcesando();
                                    var oResp=eval('['+data+']')[0];
                                    if(oResp.resultado=='1')
                                    {
                                    	cadObj=cadObj.replace('@idDocumento',oResp.idDocumento);
                                        guardarSolicitudAcceso(cadObj)
                                    }
                                    else
                                    {
                                        msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br><br>'+bD(oResp.mensaje));
                                    }
                                }
                    });
            
            
            
        break
        case 6:
            if(gE('filePFX').value=='')
            {
                function resp1CerFirel()
                {
                    gE('filePFX').focus();
                }
                msgBox('Debe ingresar el archivo de llave privada (*.pfx)',resp1CerFirel);
                return;
            }
            
            
            
            if(gEx('txtPasswordFirel').getValue().trim()=='')
            {
                function resp2CerFirel()
                {
                    gEx('txtPasswordFirel').focus();
                }
                msgBox('Debe ingresar la contrase&ntilde;a de llave privada',resp2CerFirel);
                return;
            }
            objResultado.cadenaFirma=''
            
            var cadObjFirma='{"documentoFinal":"1","idDocumento":"'+idArchivo+'","nombreDocumento":"solicitudAcceso.pdf","tipoFirma":"2"}';															
            														
            
            
            var oObj=eval('['+cadObjFirma+']')[0];
            
            
            var formData = new FormData();
            
            formData.append('passwd',AES_Encrypt(gEx('txtPasswordFirel').getValue()));
            
            
            for(var campo in oObj)
            {
                formData.append(campo,oObj[campo]);
            }
            
            
            
            formData.append('fCer',gE('filePFX').files[0]);
            mostrarMensajeProcesando('Firmando documento, &eacute;sta operaci&oacute;n puede tardar unos minutos...');
            
            $.ajax	({
                        url: "../paginasFunciones/procesarDocumentoFirmaElectronicaSICOREGeneral.php",
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(data)
                                {
                                    ocultarMensajeProcesando();
                                    var oResp=eval('['+data+']')[0];
                                    if(oResp.resultado=='1')
                                    {
                                        cadObj=cadObj.replace('@idDocumento',oResp.idDocumento);
                                        guardarSolicitudAcceso(cadObj)
                                        
                                        
                                    }
                                    else
                                    {
                                        msgBox('No se ha podido llevar a cabo la operaci&oacute;n debido al siguiente problema:<br><br>'+bD(oResp.mensaje));
                                    }
                                }
                    });
        break;
        
    }
}

function guardarSolicitudAcceso(cadObj)
{
	
	function funcAjax()
    {
        var resp=peticion_http.responseText;
        arrResp=resp.split('|');
        if(arrResp[0]=='1')
        {
           
            filaSeleccionada=arrResp[1];
            gEx('gExpedientes').getStore().reload();
            if(gEx('vSolicitud_1'))
	            gEx('vSolicitud_1').close();
                
            if(gEx('vSolicitud_2'))
	            gEx('vSolicitud_2').close();
            
            if(gEx('vSolicitud_3'))
	            gEx('vSolicitud_3').close();
            enviarSolicitudJuzgado(bE(filaSeleccionada));    
        }
        else
        {
            msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
        }
    }
    obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=9&cadObj='+cadObj,true);
}

