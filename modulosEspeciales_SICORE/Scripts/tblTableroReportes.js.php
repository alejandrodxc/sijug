<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
	
	$consulta="SELECT claveUnidad,nombreUnidad FROM _17_tablaDinamica where cmbCategoria=1";
	$arrUnidades=$con->obtenerFilasArreglo($consulta);
	
?>

var arrInformes= [
						['1','Reporte de ingresos al sistema'],
                        ['2','Reporte de registro de promociones'],
                        ['3','Reporte de consultas de videograbaciones'],
                        ['4','Demanda de expedientes por materia']
                 ]
                  
              
                  
var arrUnidades=<?php echo $arrUnidades?>;

Ext.onReady(inicializar);

function inicializar()
{
	
	
    var cmbTipoInforme=crearComboExt('cmbTipoInforme',arrInformes,0,0,290);
    
    cmbTipoInforme.on('select',tipoReporteChange);
    
    
    new Ext.Viewport(	{
                                layout: 'border',
                                items: [
                                            {
                                                xtype:'panel',
                                                region:'center',
                                                layout:'border',
                                                tbar:	[
                                                			{
                                                            	xtype:'label',
                                                                html:'<b>&nbsp;&nbsp;&nbsp;Tipo de informe:&nbsp;&nbsp;&nbsp;</b>'
                                                            },
                                                            cmbTipoInforme,'-',
                                                			{
                                                            	xtype:'label',
                                                                html:'<b>&nbsp;&nbsp;&nbsp;Periodo del:&nbsp;&nbsp;&nbsp;</b>'
                                                            },
                                                            {
                                                            	xtype:'datefield',
                                                                id:'dtrFechaInicio',
                                                                value:'<?php echo date("Y-m-d")?>',
                                                                listeners:	{
                                                                				//select:cargarReporte
                                                                			}
                                                            },
                                                            {
                                                            	xtype:'label',
                                                                html:'<b>&nbsp;&nbsp;&nbsp;al:&nbsp;&nbsp;&nbsp;</b>'
                                                            },
                                                            {
                                                            	xtype:'datefield',
                                                                id:'dtrFechaFin',
                                                                value:'<?php echo date("Y-m-d")?>',
                                                                listeners:	{
                                                                				//select:cargarReporte
                                                                			}
                                                            },'-',
                                                            {
                                                                icon:'../images/icon_big_tick.gif',
                                                                cls:'x-btn-text-icon',
                                                                text:'Generar',
                                                                handler:function()
                                                                        {
                                                                        	if(cmbTipoInforme.getValue()=='')
                                                                            {
                                                                            	msgBox('Debe seleccionar el tipo de informe a obtener');
                                                                            	return;
                                                                            }
                                                                            cargarReporte();
                                                                        }
                                                                
                                                            }
                                                		],
                                                items:	[
                                                            new Ext.ux.IFrameComponent({ 
                
                                                                                          id: 'frameContenidoHijo', 
                                                                                          anchor:'100% 100%',
                                                                                          region:'center',
                                                                                          loadFuncion:function(iFrame)
                                                                                                      {
                                                                                                          
                                                                                                      },

                                                                                          url: '../paginasFunciones/white.php',
                                                                                          style: 'width:100%;height:100%' 
                                                                                  })
                                                        ]
                                            }
                                         ]
                            }
                        )
                        
	
    
    
    
                           
}


function tipoReporteChange()
{
	
}


function cargarReporte()
{

    var cmbTipoInforme=gEx('cmbTipoInforme');
    var oParams={};
  
    switch(cmbTipoInforme.getValue())
    {
    	case '1':
        	urlLiga='../modulosEspeciales_SICORE/reportes/ingresosASistema.php';
        	
            if((gEx('dtrFechaInicio').getValue()=='')||(gEx('dtrFechaFin').getValue()==''))
            {
            	urlLiga='../paginasFunciones/white.php';
            }
            else
            {
            	
                oParams.fechaInicio=gEx('dtrFechaInicio').getValue().format("Y-m-d");
                oParams.fechaFin=gEx('dtrFechaFin').getValue().format("Y-m-d");
                oParams.cPagina='sFrm=true';
            }
        break;
        case '2':
        	urlLiga='../modulosEspeciales_SICORE/reportes/registroPromociones.php';
        	
            if((gEx('dtrFechaInicio').getValue()=='')||(gEx('dtrFechaFin').getValue()==''))
            {
            	urlLiga='../paginasFunciones/white.php';
            }
            else
            {
            	
                oParams.fechaInicio=gEx('dtrFechaInicio').getValue().format("Y-m-d");
                oParams.fechaFin=gEx('dtrFechaFin').getValue().format("Y-m-d");
                oParams.cPagina='sFrm=true';
            }
        break;
        case '3':
        	urlLiga='../modulosEspeciales_SICORE/reportes/consultasVideograbaciones.php';
        	
            if((gEx('dtrFechaInicio').getValue()=='')||(gEx('dtrFechaFin').getValue()==''))
            {
            	urlLiga='../paginasFunciones/white.php';
            }
            else
            {
            	
                oParams.fechaInicio=gEx('dtrFechaInicio').getValue().format("Y-m-d");
                oParams.fechaFin=gEx('dtrFechaFin').getValue().format("Y-m-d");
                oParams.cPagina='sFrm=true';
            }
        break;
        case '4':
        	urlLiga='../modulosEspeciales_SICORE/reportes/demandaExpedientesMateria.php';
        	
            if((gEx('dtrFechaInicio').getValue()=='')||(gEx('dtrFechaFin').getValue()==''))
            {
            	urlLiga='../paginasFunciones/white.php';
            }
            else
            {
            	
                oParams.fechaInicio=gEx('dtrFechaInicio').getValue().format("Y-m-d");
                oParams.fechaFin=gEx('dtrFechaFin').getValue().format("Y-m-d");
                oParams.cPagina='sFrm=true';
            }
        break;
        default:
        	urlLiga='../paginasFunciones/white.php';
        break;
        
    }
    
    
    gEx('frameContenidoHijo').load	(
    									{
                                        	url:urlLiga,
                                            params:oParams
                                        }
    								)
    
    
}

