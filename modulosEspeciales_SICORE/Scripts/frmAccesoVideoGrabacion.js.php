<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>

Ext.onReady(inicializar);

function inicializar()
{
	new Ext.Button(
                    {
                          icon:'../images/magnifier.png',
                          cls:'x-btn-text-icon',
                          text:'Visualizar',
                          renderTo:'btnBuscar',
                          handler:function()
                                  {
                                  		oE('filaError');
                                      	var codigoAcceso=gE('codigoAcceso').value;
                                      	if(codigoAcceso=='')
                                      	{
	                                      	mostrarError('Debe ingresar un c&oacute;digo de acceso');
                                           	return;
                                      	}
                                        
                                        
                                        function funcAjax()
                                        {
                                            var resp=peticion_http.responseText;
                                            arrResp=resp.split('|');
                                            if(arrResp[0]=='1')
                                            {
                                               switch(arrResp[1])
                                               {
                                               		case '1':
                                                    	abrirVideoGrabacion(bE(arrResp[2]),codigoAcceso);
                                                    break;
                                                    case '2':
                                                    	mostrarError('El c&oacute;digo de acceso ingresado NO existe');
                                                    break;
                                                    case '3':
                                                    	mostrarError('El c&oacute;digo de acceso ingresado ha expirado');
                                                    break;
                                                    
                                               } 
                                            }
                                            else
                                            {
                                                msgBox('<?php echo $etj["errOperacion"]?>'+' <br />'+arrResp[0]);
                                            }
                                        }
                                        obtenerDatosWeb('../paginasFunciones/funcionesModulosEspeciales_SICORE.php',funcAjax, 'POST','funcion=16&codigoAcceso='+bE(codigoAcceso),true);
                                        
                                        
                                  }
                          
                      }
                    )
	gE('codigoAcceso').focus();                    
}


function mostrarError(err)
{
	gE('lblError').innerHTML=err;
    mE('filaError');
}

function abrirVideoGrabacion(url,codigoAcceso)
{
	var obj={};    
    obj.ancho='100%';
    obj.alto='100%';
    obj.url='../modulosEspeciales_SICORE/visorGrabacionAudienciaSicor.php';
    obj.params=[['urlMultimedia',url],['cPagina','sFrm=true'],['codigoAcceso',bE(codigoAcceso)]]
    if(window.parent)
    	window.parent.abrirVentanaFancy(obj);
    else
	    abrirVentanaFancy(obj);
}