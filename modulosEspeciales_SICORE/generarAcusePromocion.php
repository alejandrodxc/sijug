<?php  session_start();

	include("latis/conexionBD.php");
	include_once("latis/PHPWord.php");
	include_once("latis/zip.lib.php"); 
	
	$idRegistro=-1;
	if(isset($_POST["idRegistro"]))
		$idRegistro=$_POST["idRegistro"];
		
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate($baseDir.'\\modulosEspeciales_SICORE\\plantillas\\acusePromocion.docx');	
	
	$consulta="SELECT * FROM 7071_promocionesRegistradasUsuario WHERE idRegistro=".$idRegistro;
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);

	$arrValores=array();
	$arrValores["fechaRegistro"]=date("d/m/H H:i",strtotime($fRegistro["fechaRegistro"]))." hrs.";
	$arrValores["fechaRecepcion"]=date("d/m/H H:i",strtotime($fRegistro["fechaRecepcionJuzgado"]))." hrs.";
	$arrValores["folioRegistro"]=str_pad($fRegistro["folioRegistroJuzgado"],10,"0",STR_PAD_LEFT);
	
	$consulta="SELECT nombreUnidad FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistro["cveUnidadGestion"]."'";
	$arrValores["unidadGestion"]=$con->obtenerValor($consulta);
	$arrValores["carpetaJudicial"]=$fRegistro["expediente"];
	$consulta="SELECT nomArchivoOriginal FROM 908_archivos WHERE idArchivo=".$fRegistro["idDocumentoPromocion"];
	
	$arrValores["documentosAdjuntos"]=$con->obtenerValor($consulta);
	$arrValores["nombreUsr"]=obtenerNombreUsuario($fRegistro["idUsuarioRegistro"]);

	foreach($arrValores as $llave=>$valor)
	{
		$document->setValue("[".$llave."]",utf8_decode(urldecode($valor)));	
	}
	
	$nombreAleatorio=generarNombreArchivoTemporal();
	$nomArchivo=$nombreAleatorio.".docx";
	$document->save($nomArchivo);

	$nombreFinal=str_replace(".docx",".pdf",$nomArchivo);
	generarDocumentoPDF($nomArchivo,false,false,true,$nombreFinal,"","./");
	
	header("Content-type:application/pdf"); 
	header("Content-length: ".filesize($nombreFinal)); 
	header("Content-Disposition: inline; filename=".$nombreFinal);
	readfile($nombreFinal);	
	
	unlink($nombreFinal);
	return $nombreFinal;
	
?>