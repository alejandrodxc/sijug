<?php  session_start();

	include("latis/conexionBD.php");
	include_once("latis/PHPWord.php");
	include_once("latis/zip.lib.php"); 
	
	$idRegistro=7;
	if(isset($_POST["idRegistro"]))
		$idRegistro=$_POST["idRegistro"];
		
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate($baseDir.'\\modulosEspeciales_SICORE\\plantillas\\acuseSolicitudLMVLV.docx');	
	
	$consulta="SELECT * FROM _483_tablaDinamica WHERE id__483_tablaDinamica=".$idRegistro;
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);

	$arrValores=array();
	$arrValores["fechaRegistro"]=date("d/m/H H:i",strtotime($fRegistro["fechaCreacion"]))." hrs.";
	$arrValores["fechaRecepcion"]=date("d/m/H H:i",strtotime($fRegistro["fechaRecepcion"]))." hrs.";
	$arrValores["folioRegistro"]=$fRegistro["folioRecepcion"];
	
	$consulta="SELECT nombreUnidad FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistro["unidadGestion"]."'";
	$arrValores["unidadGestion"]=$con->obtenerValor($consulta);
	$arrValores["carpetaJudicial"]=$fRegistro["carpetaAdministrativa"];
	
	
	$arrDocumentos="";
	$consulta="SELECT g.documento,g.descripcion,a.nomArchivoOriginal,g.documento FROM _483_gDocumentosComplementarios g,908_archivos a WHERE g.idReferencia=".$idRegistro."
				AND g.documento=a.idArchivo";
	$res=$con->obtenerFilas($consulta);
	while($filaDoc=mysql_fetch_assoc($res))
	{
		if($arrDocumentos=="")
			$arrDocumentos=$filaDoc["nomArchivoOriginal"];
		else
			$arrDocumentos.=", ".$filaDoc["nomArchivoOriginal"];
	}
	
	$arrValores["documentosAdjuntos"]=$arrDocumentos;
	$arrValores["nombreUsr"]=obtenerNombreUsuario($fRegistro["responsable"]);

	foreach($arrValores as $llave=>$valor)
	{
		$document->setValue("[".$llave."]",utf8_decode($valor));	
	}
	
	$nombreAleatorio=generarNombreArchivoTemporal();
	$nomArchivo=$nombreAleatorio.".docx";
	$document->save($nomArchivo);

	$nombreFinal=str_replace(".docx",".pdf",$nomArchivo);
	generarDocumentoPDF($nomArchivo,false,false,true,$nombreFinal,"","./");
	
	header("Content-type:application/pdf"); 
	header("Content-length: ".filesize($nombreFinal)); 
	header("Content-Disposition: inline; filename=".$nombreFinal);
	readfile($nombreFinal);	
	
	unlink($nombreFinal);
	return $nombreFinal;
	
?>