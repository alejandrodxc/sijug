<?php
	session_start();
	include("latis/configurarIdiomaJS.php");
	include("latis/conexionBD.php");
?>






function cRectanguloTexto(obj)
{
	var objRectangulo=new Kinetic.Layer();
	this.objConf=obj;
    this.anchoObj=0;
    var tamMargen=2;
    var tamMargenSup=3;
    var ancho=0;
    
     var group = new Kinetic.Group	(	
     									{
                                      		draggable: true
                                    	}
                                     );

	group.on("mouseover", function() 
                            {
                                  document.body.style.cursor = "pointer";
                            }
			);
	group.on("mouseout", function() 
        					{
          						document.body.style.cursor = "default";
        					}
			);
                                     
    
        var titulo=	new Kinetic.Text(	{
                                            text:this.objConf.titulo,
                                            fontSize:11,
                                            fontFamily:'Verdana',
                                            textFill:'#0F0B7A',
                                            x:this.objConf.x+tamMargen,
                                            y:this.objConf.y+tamMargenSup,
                                            draggable: true
                                        }
                                    );
        
                                    
    var anchoTexto=titulo.textWidth;
    this.altoLinea=titulo.textHeight+(tamMargenSup*2);
    
    this.anchoObj=anchoTexto+(tamMargen*2);
    var arrPuntos=[this.objConf.x, this.objConf.y,this.objConf.x+this.anchoObj, this.objConf.y,this.objConf.x+this.anchoObj, this.objConf.y+this.altoLinea,this.objConf.x, this.objConf.y+this.altoLinea,this.objConf.x,this.objConf.y];
    var o=	new Kinetic.Line	(
                                    {
                                          points: arrPuntos,
                                          stroke: "#000000",
                                          strokeWidth: 0.5,
                                          lineJoin: "round",
                                          dashArray: [3,3]
                                          
                                    }
                               );
    
    group.add(o);
    group.add(titulo);
        

    objRectangulo.add(group);
    return objRectangulo;
}

function cTabla(obj)
{
	this.contexto=obj.contexto;
    this.objConf=obj;
}

cTabla.prototype.dibujar=function()
                        {
                            
                            this.altoLinea=18;
                            this.anchoObj=0;
                            var tamMargen=20;
							var ancho=0;
                            var distanciaObj=7;
                            var distanciaPrimerHijo=10;
                            var anchoLineaHija=distanciaObj+this.altoLinea;
                            
                            this.contexto.fillStyle='#0B7A0F';
                            this.contexto.textBaseline='top';
                            this.contexto.font='14px Verdana';
                            this.contexto.fillText(this.objConf.nTabla,this.objConf.x+tamMargen,this.objConf.y+2);
                            var anchoTexto=this.contexto.measureText(this.objConf.nTabla);
                            this.anchoObj=anchoTexto.width+(tamMargen*2);
                            this.contexto.lineWidth=1;
                            this.contexto.beginPath();
                            this.contexto.moveTo(this.objConf.x,this.objConf.y);
                            this.contexto.lineTo(this.objConf.x+this.anchoObj,this.objConf.y);
                            this.contexto.lineTo(this.objConf.x+this.anchoObj,this.objConf.y+this.altoLinea);
                            this.contexto.lineTo(this.objConf.x,this.objConf.y+this.altoLinea);
                            this.contexto.lineTo(this.objConf.x,this.objConf.y);
                            this.contexto.closePath();
                            this.contexto.stroke();
                            
                            var x;
                            this.arregloHijos=new Array();
                            var oHijo={};
                            var posFinalY=this.objConf.y+this.altoLinea;
                            var  posY=posFinalY+distanciaPrimerHijo;
                            
                            var posYAux;
                            
                            var lineaFinal=0;
                            for(x=0;x<this.objConf.atributos.length;x++)
                            {
                            	oHijo={};
                                oHijo.y=posY;
                                oHijo.x=this.objConf.x+(this.anchoObj*0.5);
                                oHijo.titulo=this.objConf.atributos[x].nCampo;
								
                                oHijo.contexto=this.contexto;
                            	this.arregloHijos.push(new cRectanguloTexto(oHijo));
                                
                                this.contexto.beginPath();
                                posYAux=((posFinalY+distanciaPrimerHijo+(this.altoLinea/2))+((this.altoLinea+distanciaObj)*x));
                                lineaFinal=posYAux;
                                this.contexto.moveTo(this.objConf.x+(this.anchoObj*0.2)+1,posYAux);
                                this.contexto.lineTo(this.objConf.x+(this.anchoObj*0.5)-1,posYAux);
                                
                                this.contexto.closePath();
                                this.contexto.stroke();
                                
                                posY+=this.altoLinea+distanciaObj;
                                
                                
                            }
                            
                            this.contexto.beginPath();
                            this.contexto.moveTo(this.objConf.x+(this.anchoObj*0.2),posFinalY+1);
                            this.contexto.lineTo(this.objConf.x+(this.anchoObj*0.2),lineaFinal);
                            
                            this.contexto.closePath();
                            this.contexto.stroke();
                            
                            
                            
                        }


var started=false;
var context=null;
var canvas;
Ext.onReady(inicializar);

var xLineaAnt=0;
var xLineaAct=0;
var yLineaAnt=0;
var yLineaAct=0;

function inicializar()
{
	
	var lienzo=new cLienzo	(
    							{
                                	contenedor:'contenedor',
                                    ancho:1200,
                                    alto:600
                                }
    						);
                            
	var obj={}
	obj.x=10;
    obj.y=10;
    obj.titulo='800_Usuarios';
    obj.atributos=new Array();                            

	//for(ct=0;ct<=50;ct++)                            
    {
    	obj.y+=25;
        var p=cRectanguloTexto(obj);
        lienzo.agregarObjeto(p);
	}
	return;

	canvas=gE('prueba');
    var ctx=canvas.getContext('2d');
    context=ctx;
    
    
    
    canvas.addEventListener('mousedown', mousedown, false);
  	canvas.addEventListener('mousemove', mousemove, false);
  	canvas.addEventListener('mouseup',   mouseup, false);
    
    var obj={}
	obj.x=150;
    obj.y=50;
    obj.contexto=ctx;
    obj.nTabla='Usuarios';
    obj.atributos=new Array();
    
    var oA={};
    oA.nCampo='idUsuario';
    
    obj.atributos.push(oA);
    
    oA={};
    oA.nCampo='nombre';
    obj.atributos.push(oA);
    
    oA={};
    oA.nCampo='apPaterno';
    obj.atributos.push(oA);
    
    var o=new cTabla(obj);

    o.dibujar();
    obj.nTabla='900_formularios';
    obj.x=550;
    obj.y=120;
    o=new cTabla(obj);
    o.dibujar();
    
    /*dibujarLineaHorizontal(ctx,20.5);
    dibujarLineaVertical(ctx,100);*/
}

function mousedown(ev) 
{
	var pos=getMousePos(canvas,ev);
    
    ev._x = pos.x;
	ev._y = pos.y;
    context.beginPath();
    context.moveTo(ev._x, ev._y);
    started = true;
}

function mousemove(ev) 
{
	var pos=getMousePos(canvas,ev);
    
    ev._x = pos.x;
	ev._y = pos.y;
    if (started) 
    {
        context.lineTo(ev._x, ev._y);
        context.stroke();
    }
    limpiar(context);
    //dibujarLineaHorizontal(context,ev._y);
   // dibujarLineaVertical(context,ev._x);
}

function mouseup(ev) 
{
	var pos=getMousePos(canvas,ev);
    
    ev._x = pos.x;
	ev._y = pos.y;
	if (started) 
    {
      	mousemove(ev);
      	started = false;
    }
  
}

function getMousePos(canvas, evt)
{
    // get canvas position
    var obj = canvas;
    var top = 0;
    var left = 0;
    while (obj && obj.tagName != 'BODY') 
    {
        top += obj.offsetTop;
        left += obj.offsetLeft;
        obj = obj.offsetParent;
    }
 
    var mouseX = evt.clientX - left + window.pageXOffset;
    var mouseY = evt.clientY - top + window.pageYOffset;
    var obj={};
    obj.x=mouseX;
    obj.y=mouseY;
    
    
    return obj;
}



