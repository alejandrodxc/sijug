<?php	include_once("latis/conexionBD.php");

function obtenerTitularPuestoSegundaInstanciaCodigoUnico($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$cAdminitrativa=obtenerCarpetaAdministrativaProceso($idFormulario,$idRegistro);
	$rolActor=obtenerTituloRol($actorDestinatario);
	$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cAdminitrativa."' AND tipoCarpetaAdministrativa=2 ORDER BY fechaCreacion DESC";
	
	$unidadGestion=$con->obtenerValor($consulta);
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion='".$unidadGestion."'";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}

function obtenerTitularPuestoPrimeraInstanciaCodigoUnico($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$cAdminitrativa=obtenerCarpetaAdministrativaProceso($idFormulario,$idRegistro);
	$rolActor=obtenerTituloRol($actorDestinatario);
	$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cAdminitrativa."' AND tipoCarpetaAdministrativa=1 ORDER BY fechaCreacion DESC";
	
	$unidadGestion=$con->obtenerValor($consulta);
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion='".$unidadGestion."'";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}


function obtenerTitularPuestoPrimeraInstanciaSIUGJ($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$cAdminitrativa=obtenerCarpetaAdministrativaProceso($idFormulario,$idRegistro);
	$rolActor=obtenerTituloRol($actorDestinatario);
	
	$arrCarpetas=array();
	obtenerCarpetasPadre($cAdminitrativa,$arrCarpetas);
	if(sizeof($arrCarpetas)==0)
	{
		array_push($arrCarpetas,$cAdminitrativa);
	}
	
	$carpetaBase=$arrCarpetas[0];
	
	$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaBase."' AND tipoCarpetaAdministrativa=1 ORDER BY fechaCreacion DESC";
	
	$unidadGestion=$con->obtenerValor($consulta);
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion='".$unidadGestion."'";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}


function obtenerDestinatariosNotificacionCorreo($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	
	$rolActor=obtenerTituloRol($actorDestinatario);
	$arrDestinatario=array();
	
	$consulta="SELECT DISTINCT r.idCuentaAcceso FROM _665_gPersonasNotificar p,7005_relacionFigurasJuridicasSolicitud r WHERE p.idReferencia=".$idRegistro."
				AND r.idParticipante=p.idPersona AND r.idCuentaAcceso IS NOT NULL AND r.idCuentaAcceso<>-1";

	$res=$con->obtenerFilas($consulta);
	
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}

function obtenerTitularPuestoDespachoAsignado($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$rolActor=obtenerTituloRol($actorDestinatario);
	$consulta="select despachoAsignado from _".$idFormulario."_tablaDinamica where id__".$idFormulario."_tablaDinamica=".$idRegistro;
	$unidadGestion=$con->obtenerValor($consulta);
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion='".$unidadGestion."'";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}


function obtenerFuncionarioPonenteTutelaAsignada($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	
	
	$consulta="SELECT salaRevision FROM _917_tablaDinamica WHERE id__917_tablaDinamica=".$idRegistro;
	$salaRevision=$con->obtenerValor($consulta);
	
	
	$rolActor=obtenerTituloRol($actorDestinatario);
	
	$consulta="SELECT despachoAsigando FROM _993_tablaDinamica WHERE idReferencia=".$salaRevision." AND presideSala=1";
	$unidadGestion=$con->obtenerValor($consulta);
	
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion='".$unidadGestion."'";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}

function obtenerFuncionarioSalaTutelaAsignada($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$consulta="SELECT salaRevision FROM _917_tablaDinamica WHERE id__917_tablaDinamica=".$idRegistro;
	$salaRevision=$con->obtenerValor($consulta);
	
	
	$rolActor=obtenerTituloRol($actorDestinatario);
	
	$consulta="SELECT despachoAsigando FROM _993_tablaDinamica WHERE idReferencia=".$salaRevision." AND presideSala=0";
	$unidadGestion=$con->obtenerListaValores($consulta,"'");
	
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion in(".$unidadGestion.")";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}


function obtenerMagistradosSeleccionTutelas($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	$consulta="SELECT carpetaAdministrativa FROM _899_tablaDinamica WHERE id__899_tablaDinamica=".$idRegistro;
	$carpetaAdministrativa=$con->obtenerValor($consulta);
	
	
	$rolActor=obtenerTituloRol($actorDestinatario);
	
	$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativa."' AND tipoCarpetaAdministrativa=30";
	$unidadGestion=$con->obtenerListaValores($consulta,"'");
	
	
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion in(".$unidadGestion.")";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}


function determinarDestinatarioFirmaColegiada($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;

	$consulta="SELECT carpetaAdministrativa,idEstado FROM _696_tablaDinamica WHERE id__696_tablaDinamica=".$idRegistro;
	$fDatos=$con->obtenerPrimeraFilaAsoc($consulta);
	
	$carpetaAdministrativa=$fDatos["carpetaAdministrativa"];
	$idEstado=$fDatos["idEstado"];
	
	$posicion=0;
	switch($idEstado)
	{
		case 6:
			$posicion=1;
		break;
		case 7:
			$posicion=2;
		break;
	}

	$rolActor=obtenerTituloRol($actorDestinatario);
	
	$arrDestinatario=array();
	$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativa."' AND tipoCarpetaAdministrativa=30 ORDER BY idCarpeta limit ".$posicion.",1";
	$unidadGestion=$con->obtenerValor($consulta);
	
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion in('".$unidadGestion."')";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}


	return $arrDestinatario;
	
}

function obtenerTitularDespachoEnvioTutela($idFormulario,$idRegistro,$actorDestinatario)
{
	global $con;
	

	$consulta="SELECT carpetaAdministrativa FROM _".$idFormulario."_tablaDinamica WHERE id__".$idFormulario."_tablaDinamica=".$idRegistro;
	$carpetaAdministrativa=$con->obtenerValor($consulta);
	
	$consulta="SELECT codigoInstitucion FROM _917_tablaDinamica WHERE carpetaAdministrativa='".$carpetaAdministrativa."'";

	$unidadGestion=$con->obtenerValor($consulta);
	

	$rolActor=obtenerTituloRol($actorDestinatario);
	$arrDestinatario=array();
	$consulta="SELECT ad.idUsuario FROM 801_adscripcion ad,807_usuariosVSRoles r WHERE r.idUsuario=ad.idUsuario AND 
				r.codigoRol='".$actorDestinatario."' AND ad.Institucion in('".$unidadGestion."')";

	$res=$con->obtenerFilas($consulta);
	while($fila=mysql_fetch_row($res))
	{
		
		$nombreUsuario=obtenerNombreUsuario($fila[0])." (".$rolActor.")";
		$o='{"idUsuarioDestinatario":"'.$fila[0].'","nombreUsuarioDestinatario":"'.$nombreUsuario.'"}';
		$o=json_decode($o);
		array_push($arrDestinatario,$o);
	}

	return $arrDestinatario;
	
}
?>