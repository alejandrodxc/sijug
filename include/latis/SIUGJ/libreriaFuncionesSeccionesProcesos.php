<?php
	function mostrarSeccionEdicionDocumentoSeleccionFormatoInformeSecretarial($idFormulario,$idRegistro,$idFormularioEvaluacion)
	{
		global $con;
		
		$documentoBloqueado=0;
		$consulta="SELECT idRegistro FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario.
					" AND idReferencia=".$idRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
	
		$iRegistro=$con->obtenerValor($consulta);	
		if($iRegistro!="")
		{
			$consulta="SELECT documentoBloqueado FROM 3000_formatosRegistrados WHERE idFormulario=-2 AND idRegistro=".$iRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
			
			$documentoBloqueado=$con->obtenerValor($consulta);	
		}
		if($documentoBloqueado==1)
			return 0;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
			
		if($tipoApelacion==2)
			return 1;
				
		$consulta="SELECT COUNT(*) FROM _966_tablaDinamica WHERE idReferencia=".$idRegistro;	
		$numReg=$con->obtenerValor($consulta);
			
		return $numReg>0?1:0;
		
	}
	
	function mostrarSeccionEdicionDocumentoSeleccionFormatoGeneraAuto($idFormulario,$idRegistro,$idFormularioEvaluacion)
	{
		global $con;
		
		$documentoBloqueado=0;
		$consulta="SELECT idRegistro FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario.
					" AND idReferencia=".$idRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
	
		$iRegistro=$con->obtenerValor($consulta);	
		if($iRegistro!="")
		{
			$consulta="SELECT documentoBloqueado FROM 3000_formatosRegistrados WHERE idFormulario=-2 AND idRegistro=".$iRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
			
			$documentoBloqueado=$con->obtenerValor($consulta);	
		}
		if($documentoBloqueado==1)
			return 0;
			
			
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
	
		$tipoDocumento=$con->obtenerValor($consulta);	
		
		switch($tipoDocumento)
		{
			case 606:
				$consulta="SELECT COUNT(*) FROM _975_tablaDinamica WHERE idReferencia=".$idRegistro;
				$numReg=$con->obtenerValor($consulta);
				if($numReg==0)
					return 0;
				
			break;
			case 608:
				$consulta="SELECT COUNT(*) FROM _984_tablaDinamica WHERE idReferencia=".$idRegistro;
				$numReg=$con->obtenerValor($consulta);
				if($numReg==0)
					return 0;
				
			break;
		}
			
		return 1;
		
	}
	
	function mostrarSeccionEdicionDocumentoSeleccionFormatoProvidencia($idFormulario,$idRegistro,$idFormularioEvaluacion)
	{
		global $con;
		
		$documentoBloqueado=0;
		$consulta="SELECT idRegistro FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario.
					" AND idReferencia=".$idRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
	
		$iRegistro=$con->obtenerValor($consulta);	
		if($iRegistro!="")
		{
			$consulta="SELECT documentoBloqueado FROM 3000_formatosRegistrados WHERE idFormulario=-2 AND idRegistro=".$iRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
			
			$documentoBloqueado=$con->obtenerValor($consulta);	
		}
		if($documentoBloqueado==1)
			return 0;
			
			
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
		
		$providenciaAplica=$con->obtenerValor($consulta);	
		$consulta="SELECT plantillaAsociada FROM _624_tablaDinamica WHERE id__624_tablaDinamica=".$providenciaAplica;
		
		$plantillaAsociada=$con->obtenerValor($consulta);
		if(($plantillaAsociada=="")||($plantillaAsociada=="-1"))
		{
			return 0;
		}
		
		switch($providenciaAplica)
		{
			case 22:
				$consulta="SELECT COUNT(*) FROM _1018_tablaDinamica WHERE idReferencia=".$idRegistro;
				$numElementos=$con->obtenerValor($consulta);
				if($numElementos==0)
					return 0;
			break;
		}
		

			
		return 1;
		
	}
	
	
	function esProvidenciaConAuto($idFormulario,$idRegistro)
	{
		global $con;
		
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
		
		$providenciaAplica=$con->obtenerValor($consulta);	
		$consulta="SELECT plantillaAsociada FROM _624_tablaDinamica WHERE id__624_tablaDinamica=".$providenciaAplica;
		
		$plantillaAsociada=$con->obtenerValor($consulta);
		if(($plantillaAsociada=="")||($plantillaAsociada=="-1"))
		{
			return 0;
		}
		
		$consulta="SELECT COUNT(*) FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro;

		$numReg=$con->obtenerValor($consulta);
		return $numReg>0?1:0;
	}
	
	
	function esProvidenciaSinAuto($idFormulario,$idRegistro)
	{
		global $con;
		
		
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
		
		$providenciaAplica=$con->obtenerValor($consulta);	
		$consulta="SELECT plantillaAsociada FROM _624_tablaDinamica WHERE id__624_tablaDinamica=".$providenciaAplica;
		
		$plantillaAsociada=$con->obtenerValor($consulta);
		if(($plantillaAsociada=="")||($plantillaAsociada=="-1"))
		{
			return 1;
		}
		return 0;
	}
	
	function esSecretarioSustanciadorApelacionOrdinarioAuto($idRegistro,$actor)
	{
		global $con;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
		
		if($tipoApelacion!=1)
			return 0;
		
			
		$arrActor["1633"]=1;
		$arrActor["1634"]=1;
		$arrActor["1636"]=1;
		$arrActor["1635"]=1;
		
		if(!isset($arrActor[$actor]))
		{
			return 0;
		}
		return 1;
		
	}
	
	function esSecretarioSustanciadorApelacionOrdinarioSentencia($idRegistro,$actor)
	{
		global $con;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
		
		if($tipoApelacion!=2)
			return 0;
		
			
		$arrActor["1633"]=1;
		$arrActor["1634"]=1;
		$arrActor["1635"]=1;
		$arrActor["1636"]=1;
		
		if(!isset($arrActor[$actor]))
		{
			return 0;
		}
		return 1;
		
	}
	
	
	function esProcesoApelacionSobreAuto($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
		
		return $tipoApelacion==1?1:0;
		
	}
	
	function esProcesoApelacionSobreSentencia($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
		
		return $tipoApelacion==2?1:0;
		
	}
	
	function esProcesoApelacionSobreSentenciaSeccion($idFormulario,$idRegistro)
	{
		global $con;
		
		return esProcesoApelacionSobreSentencia($idFormulario,$idRegistro);
		
	}
	
	function _esSecretarioApelacion200($idFormulario,$idRegistro,$actor)
	{
		global $con;
		
		$consulta="SELECT tipoApelacion FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$tipoApelacion=$con->obtenerValor($consulta);
		
		if($tipoApelacion==2)
			return 0;
		
			
		$arrActor["1638"]=1;
		
		
		if(!isset($arrActor[$actor]))
		{
			return 0;
		}
		return 1;
	}
	
	
	function mostrarSentidoFalloGeneracionAuto($idFormulario,$idRegistro)
	{
		global $con;
		$consulta="SELECT * FROM _696_tablaDinamica WHERE id__696_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		if(($fRegistro["tipoDocumento"]!=608)||($fRegistro["idProcesoPadre"]!=0))
		{
			return 0;
		}
		
		
		return 1;
		
	}
	
	function mostrarSeccionGeneraConstanciaPrueba($idFormulario,$idRegistro,$idFormularioEvaluacion,$actor)
	{
		global $con;
		
		$documentoBloqueado=0;
		$consulta="SELECT idRegistro FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario.
					" AND idReferencia=".$idRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
	
		$iRegistro=$con->obtenerValor($consulta);	
		if($iRegistro!="")
		{
			$consulta="SELECT documentoBloqueado FROM 3000_formatosRegistrados WHERE idFormulario=-2 AND idRegistro=".$iRegistro." AND idFormularioProceso=".$idFormularioEvaluacion;
			
			$documentoBloqueado=$con->obtenerValor($consulta);	
		}
		if($documentoBloqueado==1)
			return 0;
			
		if($actor!=1449)
			return  0;	
		return 1;
		
	}
	
	
	function esProvidenciaMedidaProvisional($idFormulario,$idRegistro)
	{
		global $con;
		
		
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
		
		$providenciaAplica=$con->obtenerValor($consulta);	
		
		if($providenciaAplica==22)
		{
			return 1;
		}
		return 0;
	}
	
	function esProvidenciaMedidaProvisionalSeccion($idFormulario,$idRegistro)
	{
		global $con;
		
		
		$nombreTabla=obtenerNombreTabla($idFormulario);
		$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
		
		$providenciaAplica=$con->obtenerValor($consulta);	
		
		if($providenciaAplica==22)
		{
			return 1;
		}
		return 0;
	}
?>