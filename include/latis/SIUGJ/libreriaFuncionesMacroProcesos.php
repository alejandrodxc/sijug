<?php include_once("latis/conexionBD.php");

	function obtenerProcesoJudicialDespachoAsignado($idFormulario,$idRegistro)
	{
		global $con;
		$arrResultado=array();
		
		$consulta="SELECT codigoInstitucion FROM _".$idFormulario."_tablaDinamica WHERE id__".$idFormulario."_tablaDinamica=".$idRegistro;
		$arrResultado["codigoInstitucion"]=$con->obtenerValor($consulta);
		$arrResultado["carpetaAdministrativa"]=obtenerCarpetaAdministrativaProceso($idFormulario,$idRegistro);
		
		
		switch($idFormulario)
		{
			case 944:
				$consulta="SELECT despachoAsignado,carpetaAdministrativa2daInstancia FROM _".$idFormulario."_tablaDinamica WHERE id__".$idFormulario."_tablaDinamica=".$idRegistro;
				$fRegistroBase=$con->obtenerPrimeraFilaAsoc($consulta);
				if(($fRegistroBase["carpetaAdministrativa2daInstancia"]!="")&&($fRegistroBase["carpetaAdministrativa2daInstancia"]!="N/E"))
				{
					$arrResultado["codigoInstitucion"]=$fRegistroBase["despachoAsignado"];
					$arrResultado["carpetaAdministrativa"]=$fRegistroBase["carpetaAdministrativa2daInstancia"];
				}
			break;
		}
		
		
		return $arrResultado; 
		
	}

	function obtenerDetalleProcesoJudicialApelacionAuto($idFormulario,$idRegistro)
	{
		global $con;
		
		if($idFormulario!=944)
		{
			$arrDatosBase=obtenerRegistroPadre($idFormulario,$idRegistro);
			$idFormulario=$arrDatosBase["idFormulario"];
			$idRegistro=$arrDatosBase["idRegistro"];
			
		}
		$consulta="SELECT * FROM _944_tablaDinamica WHERE id__944_tablaDinamica=".$idRegistro;
		$fDatosApelacion=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$lblTipoApelacion="Sobre Sentencia";
		if($fDatosApelacion["tipoApelacion"]==1)
		{
			$lblTipoApelacion="Sobre Auto";
			$consulta="SELECT nomArchivoOriginal FROM 908_archivos WHERE idArchivo=".$fDatosApelacion["autoRecurso"];
			$nombreAuto=$con->obtenerValor($consulta);
			$lblTipoApelacion.=" (".$nombreAuto.")";

		}
		$lblDetalle="'Folio: ".$fDatosApelacion["codigo"].", Tipo Apelación: ".$lblTipoApelacion."'";
		return $lblDetalle;
	}
	
	function obtenerDetalleProcesoActuacion($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT * FROM _699_tablaDinamica WHERE id__699_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$nombreInterviniente=obtenerNombreParticipante($fRegistro["promovente"]);
		
		$tipoApelacion="";
		if($fRegistro["tipoActuacion"]==25)
		{
			$consulta="SELECT nomArchivoOriginal FROM 908_archivos WHERE idArchivo=".$fRegistro["autoApelacion"];
			$nombreAuto=$con->obtenerValor($consulta);
			$tipoApelacion="<br>Auto: ".$nombreAuto;
		}
		
		$lblDetalle="'Folio: ".$fRegistro["codigo"].", Interviniente: ".$nombreInterviniente.$tipoApelacion."'";
		return $lblDetalle;
	}
	
	
	function obtenerDetalleProcesoApelacion($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT * FROM _930_tablaDinamica WHERE id__930_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$nombreInterviniente=obtenerNombreParticipante($fRegistro["promovente"]);
		
		$tipoApelacion="";
		if($fRegistro["tipoApelacion"]==1)
		{
			$consulta="SELECT nomArchivoOriginal FROM 908_archivos WHERE idArchivo=".$fRegistro["autoRecurso"];
			$nombreAuto=$con->obtenerValor($consulta);
			$tipoApelacion="<br>Apelación sobre Auto: ".$nombreAuto;
		}
		else
		{
			$tipoApelacion="<br>Apelación sobre Sentencia";
		}
		
		$lblDetalle="'Folio: ".$fRegistro["codigo"].", Interviniente: ".$nombreInterviniente.$tipoApelacion."'";
		return $lblDetalle;
	}
	
	function obtenerDetalleProcesoGeneracionAuto($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT * FROM _696_tablaDinamica WHERE id__696_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		
		$consulta="SELECT nombreFormato FROM _10_tablaDinamica WHERE id__10_tablaDinamica=".$fRegistro["tipoDocumento"];
		$nombreFormato=$con->obtenerValor($consulta);
		
		$lblDetalle="'Folio: ".$fRegistro["codigo"].", Tipo de Auto: ".$nombreFormato."'";
		return $lblDetalle;
	}
	
	function obtenerDetalleProcesoNotificacion($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="SELECT * FROM _665_tablaDinamica WHERE id__665_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		
		$consulta="SELECT plantillaMensajeEnvio FROM _666_tablaDinamica WHERE id__666_tablaDinamica=".$fRegistro["tipoNotificacion"];
		$fTipoNotificacion=$con->obtenerPrimeraFilaAsoc($consulta);
	
		
		$consulta="SELECT asunto FROM 2011_mensajesEnvio WHERE idMensajeEnvio=".$fTipoNotificacion["plantillaMensajeEnvio"];
		$asuntoMensaje=$con->obtenerValor($consulta);
		
		$detalleAdicional="<br>Destinatarios: ";
		
		$arrNotificados="";
		$consulta="SELECT * FROM _665_gPersonasNotificar WHERE idReferencia=".$fRegistro["id__665_tablaDinamica"];
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$fDetalle=((($fila["idPersona"]!=-1) && ($fila["idPersona"]!=""))?obtenerNombreParticipante($fila["idPersona"]):$fila["nombrePersona"]). " (".$fila["email"].")";
			if($arrNotificados=="")
				$arrNotificados=$fDetalle;
			else
				$arrNotificados.="<br>".$fDetalle;
		}
		
		$detalleAdicional.="<br><br>".$arrNotificados;
		$detalleAdicional.="<br><br>Documentos Adjuntos:";
		
		$arrNotificados="";
		$consulta="SELECT * FROM _665_gridDocumentosNotificar WHERE idReferencia=".$fRegistro["id__665_tablaDinamica"];
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_assoc($res))
		{
			$fDetalle=$fila["nombreDocumento"];
			if($arrNotificados=="")
				$arrNotificados=$fDetalle;
			else
				$arrNotificados.="<br>".$fDetalle;
		}
		
		$detalleAdicional.=($arrNotificados==""?"<br><br>(Sin Adjuntos)":("<br><br>".$arrNotificados));
		
		$lblDetalle="'Folio: ".$fRegistro["codigo"].", Asunto: ".$asuntoMensaje.$detalleAdicional."'";
		return $lblDetalle;
	}

	function obtenerDetalleProcesoProvidencia($idFormulario,$idRegistro)
	{
		global $con;
		
		
		
		$consulta="SELECT * FROM _899_tablaDinamica WHERE id__899_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		
		$consulta="SELECT nombreActuacion FROM _624_tablaDinamica WHERE id__624_tablaDinamica=".$fRegistro["providenciaAplicar"];
		$nombreProvidencia=$con->obtenerValor($consulta);
		
		if($fRegistro["providenciaAplicar"]==15)
		{
			$consulta="SELECT contenido FROM 902_opcionesFormulario WHERE idGrupoElemento=13376 AND valor=".$fRegistro["sentidoFalloSentencia"];
			$sentidoFalloSentencia=$con->obtenerValor($consulta);
			$nombreProvidencia.=", Sentido del Fallo: ".$sentidoFalloSentencia;
		}
		
		$lblDetalle="'Folio: ".$fRegistro["codigo"].", Providencia: ".$nombreProvidencia."'";

		return $lblDetalle;
	}
?>