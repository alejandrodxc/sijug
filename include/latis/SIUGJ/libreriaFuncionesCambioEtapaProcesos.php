<?php
	function actualizarEnvioMagistrado($idFormulario,$idRegistro)
	{
		global $con;
		
		$consulta="UPDATE _944_tablaDinamica SET enviadoAMagistrado=1 WHERE id__944_tablaDinamica=".$idRegistro;
		return $con->ejecutarConsulta($consulta);
	}
	
	
	function registrarPartesAplicaProvidencia($idFormulario,$idRegistro)
	{
		global $con;
		
		$arrPersonas=array();
		$consulta="SELECT personasAplica FROM _899_tablaDinamica WHERE id__899_tablaDinamica=".$idRegistro;
		$personasAplica=$con->obtenerValor($consulta);
		
		if(($personasAplica==0)||($personasAplica==""))
			return true;
		
		$cAdminitrativa=obtenerCarpetaAdministrativaProceso($idFormulario,$idRegistro);
		
		$consulta="SELECT idActividad FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cAdminitrativa."'";
		$idActividad=$con->obtenerValor($consulta);
		
		switch($personasAplica)
		{
			case 1://Actor
			
				$consulta="SELECT * FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$idActividad." AND idFiguraJuridica IN(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='A')";
				$rPartes=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_assoc($rPartes))
				{
					array_push($arrPersonas,$fila["idParticipante"]);
					$consulta="SELECT idParticipante FROM 7005_relacionParticipantes WHERE idActividad=".$idActividad." AND idActorRelacionado= ".$fila["idParticipante"];
					$rPartesAux=$con->obtenerFilas($consulta);
					while($filaAux=mysql_fetch_assoc($rPartesAux))
					{
						array_push($arrPersonas,$filaAux["idParticipante"]);
					}
				}
			break;
			case 2: //Demandados
				$consulta="SELECT * FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$idActividad." AND idFiguraJuridica IN(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='D')";
				$rPartes=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_assoc($rPartes))
				{
					array_push($arrPersonas,$fila["idParticipante"]);
					$consulta="SELECT idParticipante FROM 7005_relacionParticipantes WHERE idActividad=".$idActividad." AND idActorRelacionado= ".$fila["idParticipante"];
					$rPartesAux=$con->obtenerFilas($consulta);
					while($filaAux=mysql_fetch_assoc($rPartesAux))
					{
						array_push($arrPersonas,$filaAux["idParticipante"]);
					}
				}
			break;
			case 3: //Todos
				$consulta="SELECT * FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$idActividad;
				$rPartes=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_assoc($rPartes))
				{
					array_push($arrPersonas,$fila["idParticipante"]);
				}
			break;
		}
		
		$x=0;
		$query=array();
		$query[$x]="begin";
		$x++;
		
		foreach($arrPersonas as $p)
		{
			$query[$x]="INSERT INTO _899_chkPartesJuridicasAplica(idPadre,idOpcion) VALUES(".$idRegistro.",".$p.")";
			$x++;
		}
		$query[$x]="commit";
		$x++;
		
		return $con->ejecutarBloque($query);
	}
	
	
	function registroDocumentoPrueba($idFormulario,$idRegistro)
	{
		global $con;
		$consulta="INSERT INTO 9503_documentosRegistradosProceso(idActividad,idTipoDocumento,idFormulario,idReferencia) VALUES(-1,34,".$idFormulario.",".$idRegistro.")";
		return $con->ejecutarConsulta($consulta);
		
	}
	
	
	
	function ejecutarAccionComplementariaProvidencias($idFormulario,$idRegistro)
	{
		global $con;
		$consulta="SELECT * FROM _899_tablaDinamica WHERE id__899_tablaDinamica=".$idRegistro;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);

		
		switch($fRegistro["providenciaAplicar"])
		{
			case 20:
				$consulta="SELECT idFormulario,idRegistro FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$fRegistro["carpetaAdministrativa"]."'";
				$fRegistroCarpeta=$con->obtenerPrimeraFilaAsoc($consulta);
				
				
				$consulta="SELECT * FROM _917_tablaDinamica WHERE idProcesoPadre=339 AND idReferencia=".$fRegistroCarpeta["idRegistro"];
				$resFilas=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_assoc($resFilas))
				{
					$arrDocumentos=NULL;
					$arrDatos=array();
					$arrDatos["medioNotificacion"]=1;
					$arrDatos["carpetaAdministrativa"]=$fila["carpetaAdministrativa"];
					$arrDatos["tipoEnvioNotificacion"]=1;
					$arrDatos["tipoNotificacion"]=2;
					$arrDatos["idProcesoPadre"]=323;
					$arrDatos["figuraNotifica"]=3;
					$arrDatos["idReferencia"]=$idRegistro;
					$arrDatos["tipoDocumentosAdjunta"]="N/E";
					$idRegistroInstancia=crearInstanciaRegistroFormulario(665,$idRegistro,1,$arrDatos,$arrDocumentos,-1,0);
				}
			break;
		}
			
			
			 
			 
			 
			 
			 
			 
			 
			
		
		
	}
	
	
	function triggerDocumentoFormatoAutorizado($idFormulario,$idRegistro)
	{
		global $con;
		

		$consulta="SELECT tipoDocumento FROM _696_tablaDinamica WHERE id__696_tablaDinamica=".$idRegistro;
		$tipoDocumento=$con->obtenerValor($consulta);
		
		
		switch($tipoDocumento)
		{
			case 623:
			
				$consulta="SELECT idRegistro FROM 7035_informacionDocumentos WHERE idFormulario=".$idFormulario." AND idReferencia=".$idRegistro;
				$idReferenciaDocumento=$con->obtenerValor($consulta);
				
				
				$consulta="SELECT * FROM 941_bitacoraEtapasFormularios WHERE idFormulario=".$idFormulario." AND idRegistro=".$idRegistro." ORDER BY idRegistroEstado DESC";
				$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);				

				switch($fRegistro["etapaActual"])
				{
					case 5:

						$consulta="UPDATE 3000_formatosRegistrados SET documentoBloqueado=2,idDocumentoAdjunto=idDocumento WHERE idFormulario=-2 AND idRegistro=".$idReferenciaDocumento;
						$con->ejecutarConsulta($consulta);
						cambiarEtapaFormulario($idFormulario,$idRegistro,6,"",-1,"NULL","NULL",0);
					break;
					case 6.5:
						$consulta="UPDATE 3000_formatosRegistrados SET documentoBloqueado=2,idDocumentoAdjunto=idDocumento WHERE idFormulario=-2 AND idRegistro=".$idReferenciaDocumento;
						$con->ejecutarConsulta($consulta);
				
						cambiarEtapaFormulario($idFormulario,$idRegistro,7,"",-1,"NULL","NULL",0);
					break;
				}
				

					
			break;
		}
		
		
	}
?>