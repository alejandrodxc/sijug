<?php

function esAutoAceptacionApelacionOrdinarioTribunalSuperior($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return $tipoDocumento==597?1:0;
}


function esProvidenciaFalloTribunalSuperior($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return $tipoDocumento==15?1:0;
}

function esAutoAutorizaDesistimientoApelacionAuto($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return ($tipoDocumento==609)?1:0;
}


function esAutoNiegaApelacionAuto($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return ($tipoDocumento==598)?1:0;
}

function esAutoImpedimientoApelacionAuto($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return ($tipoDocumento==606)?1:0;
}

function esAutoRechazaImpedimento($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return ($tipoDocumento==611)?1:0;
}

function esProvidenciaPerdidaCompetencia($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
	$tipoDocumento=$con->obtenerValor($consulta);
	return $tipoDocumento==18?1:0;
}

function esReasignacionPerdidaCompetencia($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT motivoReasignacion FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
	$motivoReasignacion=$con->obtenerValor($consulta);
	return $motivoReasignacion==2?1:0;
}

function esAutoPerdidaCompetencia($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT tipoDocumento FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;

	$tipoDocumento=$con->obtenerValor($consulta);
	return ($tipoDocumento==611)?1:0;
}


function esProvidenciaAgendaAudiencia($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
	$tipoDocumento=$con->obtenerValor($consulta);
	return $tipoDocumento==19?1:0;
}


function esProvidenciaCorreTrasladoAgendaAudiencia($idFormulario,$idRegistro)
{
	global $con;
	$nombreTabla=obtenerNombreTabla($idFormulario);
	$consulta="SELECT providenciaAplicar FROM ".$nombreTabla." WHERE id_".$nombreTabla."=".$idRegistro;
	$tipoDocumento=$con->obtenerValor($consulta);
	return $tipoDocumento==20?1:0;
}



?>