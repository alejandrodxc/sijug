<?php 
include_once("latis/conexionBD.php");
include_once("latis/utiles.php");
include_once("latis/numeroToLetra.php");


function funcionLlenadoDocumentoBaseGeneral($idFormularioBase,$idRegistroBase)
{
	global $arrMesLetra;
	global $con;
	global $arrMesLetra;
	global $leyendaTribunal;
	
	

	/*$consulta="SELECT * FROM _632_tablaDinamica WHERE id__632_tablaDinamica=".$idRegistroBase;
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);	*/

	$carpetaAdministrativo=obtenerCarpetaAdministrativaProceso($idFormularioBase,$idRegistroBase);
	$consulta="SELECT carpetaAdministrativa,unidadGestion,idActividad,carpetaAdministrativaBase,tipoProceso FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativo."'";
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
	
	$arrValores=array();
	$consulta="SELECT upper(nombreUnidad) FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistro["unidadGestion"]."'";
	$nombreDespacho=$con->obtenerValor($consulta);
	$fechaActual=date("Y-m-d");

	$codigoUnicoProcesoTribunalSuperior="";
	$codigoUnicoProcesoCorteSuprema="";
	if($idFormularioBase==672)
	{
		$consulta="SELECT carpetaAdministrativa2aInstancia,despachoAsignado FROM _672_tablaDinamica WHERE id__672_tablaDinamica=".$idRegistroBase;
		$fRegistroApelacion=$con->obtenerPrimeraFilaAsoc($consulta);
		$codigoUnicoProcesoTribunalSuperior=$fRegistroApelacion["carpetaAdministrativa2aInstancia"];
		
		$consulta="SELECT nombreUnidad FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistroApelacion["despachoAsignado"]."'";
		$nombreDespacho=$con->obtenerValor($consulta);
	}
	
	switch($idFormularioBase)
	{
		case 682:
		case 677:
			$codigoUnicoProcesoTribunalSuperior=$carpetaAdministrativo;
			
			$arrCarpetas=array();
			obtenerCarpetasPadre($carpetaAdministrativo,$arrCarpetas);
			
			if(sizeof($arrCarpetas)==0)
			{
				array_push($arrCarpetas,$carpetaAdministrativo);
			}
			
	
			
			$carpetaAdministrativo=$arrCarpetas[0];	
		break;
		
	}
	
	$demantante="";
	$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
				FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
				AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='A') ORDER BY nombre,nombre,apellidoMaterno";
	
	$res=$con->obtenerFilas($consulta);
	while($filaImputado=mysql_fetch_row($res))
	{
		$nombre=trim($filaImputado[0]);
		if($demantante=="")
			$demantante=$nombre;
		else
			$demantante.=", ".$nombre;
	}

	$demandados="";
	$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
				FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
				AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in(SELECT id__5_tablaDinamica FROM _5_tablaDinamica WHERE naturalezaFigura='D') ORDER BY nombre,nombre,apellidoMaterno";
	
	$res=$con->obtenerFilas($consulta);
	while($filaImputado=mysql_fetch_row($res))
	{
		$nombre=trim($filaImputado[0]);
		if($demandados=="")
			$demandados=$nombre;
		else
			$demandados.=", ".$nombre;
	}

	$arrValores["nombreDespacho"]=trim($nombreDespacho);
	$arrValores["fechaActual"]=convertirFechaLetra($fechaActual,false,false);
	$arrValores["demandante"]=$demantante;
	$arrValores["demandado"]=$demandados;
	$arrValores["lblDemandado"]=$fRegistro["tipoProceso"]==6?"ACCIONADO":"DEMANDADO";
	$arrValores["lblActor"]=$fRegistro["tipoProceso"]==6?"ACCIONANTE":"ACTOR";
	$arrValores["codigoUnicoProceso"]=$carpetaAdministrativo;
	$arrValores["codigoUnicoProcesoTribunalSuperior"]=$codigoUnicoProcesoTribunalSuperior;
	$arrValores["codigoUnicoProcesoCorteSuprema"]=$codigoUnicoProcesoCorteSuprema;
	return $arrValores;
}

function funcionLlenadoDocumentoBaseGeneralCorteSuprema($idFormularioBase,$idRegistroBase)
{
	global $arrMesLetra;
	global $con;
	global $arrMesLetra;
	global $leyendaTribunal;
	
	
	
	
	$carpetaAdministrativo=obtenerCarpetaAdministrativaProceso($idFormularioBase,$idRegistroBase);
	$consulta="SELECT carpetaAdministrativa,unidadGestion,idActividad,carpetaAdministrativaBase FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaAdministrativo."'";
	$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
	
	
	$arrValores=array();
	$consulta="SELECT upper(nombreUnidad) FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistro["unidadGestion"]."'";
	$nombreDespacho=$con->obtenerValor($consulta);
	$fechaActual=date("Y-m-d");

	$codigoUnicoProcesoTribunalSuperior="";
	$codigoUnicoProcesoCorteSuprema="";
	
	$codigoUnicoProcesoCorteSuprema=$carpetaAdministrativo;
	
	$arrCarpetas=array();
	obtenerCarpetasPadre($carpetaAdministrativo,$arrCarpetas);
	
	if(sizeof($arrCarpetas)==0)
	{
		array_push($arrCarpetas,$carpetaAdministrativo);
	}
	$carpetaAdministrativo=$arrCarpetas[0];	
	$codigoUnicoProcesoTribunalSuperior=$fRegistro["carpetaAdministrativaBase"];
	
	$demantante="";
	$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
				FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
				AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in(2,7) ORDER BY nombre,nombre,apellidoMaterno";
	
	$res=$con->obtenerFilas($consulta);
	while($filaImputado=mysql_fetch_row($res))
	{
		$nombre=trim($filaImputado[0]);
		if($demantante=="")
			$demantante=$nombre;
		else
			$demantante.=", ".$nombre;
	}
	
	$demandados="";
	$consulta="SELECT upper(CONCAT(IF(nombre IS NULL,'',nombre),' ',IF(apellidoPaterno IS NULL,'',apellidoPaterno),' ',IF(apellidoMaterno IS NULL,'',apellidoMaterno))) 
				FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idParticipante=p.id__47_tablaDinamica
				AND r.idActividad=".$fRegistro["idActividad"]." AND r.idFiguraJuridica in(4,8) ORDER BY nombre,nombre,apellidoMaterno";
	
	$res=$con->obtenerFilas($consulta);
	while($filaImputado=mysql_fetch_row($res))
	{
		$nombre=trim($filaImputado[0]);
		if($demandados=="")
			$demandados=$nombre;
		else
			$demandados.=", ".$nombre;
	}

	$arrValores["nombreDespacho"]=trim($nombreDespacho);
	$arrValores["fechaActual"]=convertirFechaLetra($fechaActual,false,false);
	$arrValores["demandante"]=$demantante;
	$arrValores["demandado"]=$demandados;
	$arrValores["codigoUnicoProceso"]=$carpetaAdministrativo;
	$arrValores["codigoUnicoProcesoTS"]=$codigoUnicoProcesoTribunalSuperior;
	$arrValores["codigoUnicoProcesoSC"]=$codigoUnicoProcesoCorteSuprema;
	return $arrValores;
}





?>