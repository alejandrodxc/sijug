<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	
	if(isset($_GET["tipoMetodo"]))
		$tipoMetodos=$_GET["tipoMetodo"];
	
	if($tipoMetodos==1)
	{
		function notificarAudiencia1($datosEvento)
		{
			
		}
		
		
		function notificarAudiencia2($datosEvento)
		{
			
		}
		
		
		$server->register('notificarAudiencia1',array('datosEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Notificacion  audiencia');
		$server->register('notificarAudiencia2',array('datosEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Notificacion audiencia');
	}
	else
	{
		function notificarVideoEventoAudiencia($datosEvento)
		{
			
		}
		
		
		function notificarVideoEventoAudiencia2($datosEvento)
		{
			
		}
		
		
		$server->register('notificarVideoEventoAudiencia',array('datosEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Notificacion de video de audiencia');
		$server->register('notificarVideoEventoAudiencia2',array('datosEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Notificacion de video de audiencia');
	}
	
	

	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
	
?>	