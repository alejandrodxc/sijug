<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/latisErrorHandler.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	
	function busquedaPersona($nombres,$primApellido,$segApellido="",$porcentaje=74,$edad="",$celular="",$fechaNacimiento="",$curp="",$rfc="")
	{
		global $tipoMateria;
		global $con;
		try
		{
			if($porcentaje=="")
				$porcentaje=74;
			if($tipoMateria=="P")
			{
				if(trim($nombres)=="")
				{
					return '{"resultado":"0","registros":[],"comentarios":"Debe ingresar el nombre de la persona a buscar"}';	
				}
				
				if(trim($primApellido)=="")
				{
					return '{"resultado":"0","registros":[],"comentarios":"Debe ingresar el primer apellido de la persona a buscar"}';	
				}
				
				
				$valor=trim($nombres)." ".trim($primApellido)." ".trim($segApellido);
				$arrValoresBusqueda=explode(" ",$valor);
				for($x=0;$x<sizeof($arrValoresBusqueda);$x++)
				{
					$arrValoresBusqueda[$x]=normalizaToken($arrValoresBusqueda[$x]);
				}
				$resultado=buscarCoincidenciasCriterio(1,$valor,$porcentaje,2);

				$res=$resultado[0];
				$arrRegistros="";
				$numReg=0;
				if(sizeof($resultado[1])>0)
				{
					foreach($resultado[1] as $idActividad => $porcentaje)
					{
						
						$consulta="SELECT DISTINCT apellidoPaterno, apellidoMaterno,nombre,id__47_tablaDinamica,edad,fechaNacimiento,curp,rfcEmpresa 
									FROM _47_tablaDinamica t where id__47_tablaDinamica=".$resultado[2][$idActividad];
									
						if($fechaNacimiento!="")
						{
							$consulta.=" and fechaNacimiento='".date("Y-m-d",strtotime($fechaNacimiento))."'";
						}
						
						if(trim($curp)!="")
						{
							$consulta.=" and curp='".cv(trim($curp))."'";
						}
						
						if(trim($rfc)!="")
						{
							$consulta.=" and rfcEmpresa='".cv(trim($rfc))."'";
						}
									
						$fBusqueda=$con->obtenerPrimeraFila($consulta);						
						if($fBusqueda[5]!="")
							$fBusqueda[4]=obtenerEdad(strtotime($fBusqueda[5]));
						
						
						$oDireccion=obtenerUltimoDomicilioFiguraJuridica($fBusqueda[3]);
						$oDireccion=json_decode($oDireccion);
							
						$celular="";
						foreach($oDireccion->telefonos as $t)	
						{
							if($t->tipoTelefono==2)
							{
								if($celular=="")
									$celular=($t->lada!=""?"(".$t->lada.") ":"").$t->numero;
								else
									$celular-=", ".($t->lada!=""?"(".$t->lada.") ":"").$t->numero;
							}
						}
						
						$consulta="SELECT idCarpeta,fechaCreacion,carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE idActividad=".$idActividad." ORDER BY idCarpeta";
						$fDatosCarpeta=	$con->obtenerPrimeraFila($consulta);						
						
						$arrAgresores="";
						$consulta="SELECT p.*  FROM _47_tablaDinamica p,7005_relacionFigurasJuridicasSolicitud r WHERE r.idActividad=".$idActividad."
									AND p.id__47_tablaDinamica=r.idParticipante AND idFiguraJuridica=4 ORDER BY p.nombre,apellidoPaterno,apellidoMaterno";
						$rImputados=$con->obtenerFilas($consulta);
						while($fImputados=mysql_fetch_assoc($rImputados))
						{
							$consulta="SELECT COUNT(*) FROM 7005_relacionFigurasJuridicasSolicitud r,7006_carpetasAdministrativas c
										WHERE r.idParticipante=".$fImputados["id__47_tablaDinamica"].
										" AND r.idActividad=c.idActividad AND c.tipoCarpetaAdministrativa=6";
							$sentenciados=$con->obtenerValor($consulta);
							
							$arrMedidasProteccion="";
							$consulta="SELECT idEventoAudiencia,(SELECT medidaProteccion FROM _333_tablaDinamica where id__333_tablaDinamica=r.tipoMedida) AS medida,
										comentariosAdicionales,e.fechaEvento FROM 3014_registroMedidasProteccion r,7000_eventosAudiencia e 
										WHERE idImputado=".$fImputados["id__47_tablaDinamica"]." AND idEventoAudiencia IN 
										(
										SELECT idRegistroContenidoReferencia FROM 7007_contenidosCarpetaAdministrativa WHERE 
										carpetaAdministrativa='".$fDatosCarpeta[2]."' AND tipoContenido=3
										)
										AND e.idRegistroEvento=r.idEventoAudiencia";
							$rMedidas=$con->obtenerFilas($consulta);			
							while($fMedidaProteccion=mysql_fetch_row($rMedidas))
							{
								$oMedida='{"medidaProteccion":"'.cv($fMedidaProteccion[1]).'","fechaImposicion":"'.$fMedidaProteccion[3].
										'","comentariosAdicionales":"'.cv($fMedidaProteccion[2]).'","otorgada":"1","solicitadaPor":""}';
								if($arrMedidasProteccion=="")
									$arrMedidasProteccion=$oMedida;
								else
									$arrMedidasProteccion.=",".$oMedida;
							}
							
							$consulta="SELECT COUNT(*) FROM 7005_relacionFigurasJuridicasSolicitud r,7006_carpetasAdministrativas c
										WHERE r.idParticipante=".$fImputados["id__47_tablaDinamica"].
										" AND r.idActividad=c.idActividad AND c.tipoCarpetaAdministrativa=1 and
										c.carpetaAdministrativa<>'".$fDatosCarpeta[2]."'";
							$nProcesos=$con->obtenerValor($consulta);
							
							$oAgresor='{"nombres":"'.cv($fImputados["nombre"]).'","primApellido":"'.cv($fImputados["apellidoPaterno"]).
										'","segApellido":"'.cv($fImputados["apellidoMaterno"]).'","genero":"'.$fImputados["genero"].
										'","rfc":"'.$fImputados["rfcEmpresa"].'","noExpedientesApertura":"1",
										"medidasProteccion":['.$arrMedidasProteccion.'],"sentenciado":"'.($sentenciados>0?1:0).
										'","procesoJudicial":"'.($nProcesos>0?1:0).'"}';
							if($arrAgresores=="")
								$arrAgresores=$oAgresor;
							else
								$arrAgresores.=",".$oAgresor;
						}
						
							
						$oResultado='{"idente":"'.$fBusqueda[3].'","folioente":"'.$fDatosCarpeta[0].'","fechaatencion":"'.date("Y-m-d",strtotime($fDatosCarpeta[1])).
									'","nombres":"'.cv($fBusqueda[2]).'","primApellido":"'.cv($fBusqueda[0]).'","segApellido":"'.cv($fBusqueda[1]).
									'","edad":"'.$fBusqueda[4].'","celular":"'.$celular.'","fechanacimiento":"'.$fBusqueda[5].'","curp":"'.$fBusqueda[6].
									'","rfc":"'.$fBusqueda[7].'","porcentajeCoincidencia":"'.$porcentaje.
									'","carpetaJudicial":"'.$fDatosCarpeta[2].'","fechaHechoViolento":"","arrAgresores":['.$arrAgresores.']}';
						
						
						if($arrRegistros=="")
							$arrRegistros=$oResultado;
						else
							$arrRegistros.=",".$oResultado;
		
					}
				}
				
				return '{"resultado":"1","registros":['.utf8_encode($arrRegistros).'],"comentarios":""}';	
			}
			else
			{
				$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
				$iFormularioMaterias=$con->obtenerValor($consulta);
				
				$consulta="SELECT claveOPC,materia,idCsDocs,ipServidor,puerto FROM _".$iFormularioMaterias."_tablaDinamica WHERE claveOPC='P'";
				$fMateria=$con->obtenerPrimeraFila($consulta);
				
				$urlWebServices="http://".$fMateria[3].":".$fMateria[4]."/webServices/wsInmujeres.php?wsdl";
	
				$client = new nusoap_client($urlWebServices,"wsdl"); 
				$parametros=array();
				$parametros["nombres"]=$nombres;
				$parametros["primApellido"]=$primApellido;
				$parametros["segApellido"]=$segApellido;
				$parametros["porcentaje"]=$porcentaje;
				$parametros["edad"]=$edad;
				$parametros["celular"]=$celular;
				$parametros["fechaNacimiento"]=$fechaNacimiento;
				$parametros["curp"]=$curp;
				$parametros["rfc"]=$rfc;
				$resultado = $client->call("busquedaPersona", $parametros);
				return $resultado;
			}
		}
		catch(Exception $e)
		{
			return '{"resultado":"0","registros":[],"comentarios":"'.cv($e->getMessage()).'"}';	
		}
	}

	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register(	'busquedaPersona',array('nombres'=>'xsd:string','primApellido'=>'xsd:string','segApellido'=>'xsd:string',
						'porcentaje'=>'xsd:string','edad'=>'xsd:string','celular'=>'xsd:string','fechaNacimiento'=>'xsd:string','curp'=>'xsd:string',
						'rfc'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>