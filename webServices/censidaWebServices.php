<?php session_start();
	include("latis/conexionBD.php");
	include("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	ini_set('default_socket_timeout', 160000);
	
	function obtenerComprobantesPendientesValidacion()
	{
		global $con;
		$cadDatos="";
		$consulta="SELECT c.idFactura,(SELECT CONCAT(rfc1,rfc2,rfc3) FROM 595_proveedores WHERE idProveedor=c.idProveedor)AS rfcEmisor ,
					( SELECT CONCAT(o.rfc1,rfc4,rfc3) AS rfc FROM _370_tablaDinamica t,_367_tablaDinamica o WHERE 
					id__370_tablaDinamica= c.idReferencia  AND t.codigoInstitucion=o.codigoInstitucion) AS rfcReceptor,
					tipoComprobante,folioComprobante,numeroAprobacion,anioAprobacion,noSerie FROM 101_comprobantesPresupuestales c
					WHERE (situacion=3 OR(situacion=0 AND validacionSAT=0)) AND tipoComprobante<>3";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			$obj=$fila[0]."|".$fila[1]."|".$fila[2]."|".$fila[3]."|".$fila[4]."|".$fila[5]."|".$fila[6]."|".$fila[7];
			if($cadDatos=="")
				$cadDatos=$obj;
			else
				$cadDatos.="@".$obj;
		}
		return $cadDatos;
		
	}
	
	function  actualizacionSituacionComprobante($idComprobante,$situacionSAT)
	{
		global $con;
		$consulta="UPDATE 101_comprobantesPresupuestales SET situacion=0,validacionSAT=".$situacionSAT." WHERE idFactura=".$idComprobante;
		if($con->ejecutarConsulta($consulta))
			return "1";
		return 0;
		
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('obtenerComprobantesPendientesValidacion',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Obtener comprobantes pendientes de validación');
	$server->register('actualizacionSituacionComprobante',array('idComprobante'=>'xsd:string','situacionSAT'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Actualizar comprobante');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	$server->service($input);
?>