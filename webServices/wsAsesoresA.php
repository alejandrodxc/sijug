<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	
	include_once("latis/funcionesNeotrai.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');
	
	
	
	
	function registrarSolicitudInicialAsesores($cadXML)
	{
		global $con;
		global $directorioInstalacion;	
		global $servidorPruebas;
		
		$fechaActual=strtotime(date("Y-m-d H:i:s"));
		try
		{
			$_SESSION["idUsr"]=2390;
			$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
			$resRoles=$con->obtenerFilas($consulta);
			$listaGrupo="";
			while($fRoles=mysql_fetch_row($resRoles))
			{
				$arrRol=explode("_",$fRoles[0]);
				$rol="'".$fRoles[0]."'";
				if($arrRol[1]!="0")
					$rol.=",'".$arrRol[0]."_-1'";
				
				if($listaGrupo=="")
					$listaGrupo=$rol;
				else
					$listaGrupo.=",".$rol;
			}
			if($listaGrupo=="")
				$listaGrupo='-1';
			$_SESSION["idRol"]=$listaGrupo.",'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";
			
			
			
			
			/*$consulta="insert into temporal(cuerpoXML) values('".base64_encode($cadXML)."')";
			$con->ejecutarConsulta($consulta);
			*/
			
			$docXML=($cadXML);
			
			$cXML=simplexml_load_string($docXML);
			
			$consulta="INSERT INTO 3011_solicitudRecibidasPGJ(fechaSolicitud,xmlSolicitud,carpetaInvestigacion) VALUES('".date("Y-m-d H:i:s",$fechaActual).
					"','".bE($cadXML)."','".((string)$cXML->DatosSolicitud[0]->carpetainvestigacion)."')";
			$con->ejecutarConsulta($consulta);
			
			$consulta="SELECT COUNT(*) FROM _46_tablaDinamica WHERE ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrlsolicitud."' 
					AND idSolicitud= ".((string)$cXML->DatosSolicitud[0]->idsolicitud);
			
			$nRegistro=$con->obtenerValor($consulta);
			if($nRegistro>0)
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La solicitud ya ha sido registrada anteriormente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			
			
			$consulta="SELECT carpetaAdministrativa,delitoGrave FROM _46_tablaDinamica WHERE folioCarpetaInvestigacion='".(string)$cXML->DatosSolicitud[0]->carpetainvestigacion.
						"' and idEstado>=1.4 order by fechaCreacion DESC";					
			$fDatosCarpetaBase=$con->obtenerPrimeraFila($consulta);
			
			/*$nRegistro=$con->obtenerValor($consulta);
			if($nRegistro>0)
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La carpeta de investigacion ya ha sido registrada previamente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}*/
			
			$cAdministrativa=$fDatosCarpetaBase[0];
			$arrSolicitudesPermitidas=array();
			$arrSolicitudesPermitidas[70]=1;
			$arrSolicitudesPermitidas[26]=1;
			$arrSolicitudesPermitidas[12]=1;
			$arrSolicitudesPermitidas[13]=1;
			$arrSolicitudesPermitidas[14]=1;
			$arrSolicitudesPermitidas[31]=1;
			$arrSolicitudesPermitidas[42]=1;
			$arrSolicitudesPermitidas[51]=1;
			$arrSolicitudesPermitidas[52]=1;
			$arrSolicitudesPermitidas[60]=1;
			$arrSolicitudesPermitidas[61]=1;
			$arrSolicitudesPermitidas[63]=1;
			
			$cveSolicitud=(string)$cXML->DatosSolicitud[0]->cvesolicitud;
			
			if(($cAdministrativa=="")&&(!isset($arrSolicitudesPermitidas[$cveSolicitud])))			
			{
				$declaratoria=(string)$cXML->DatosSolicitud[0]->declaratoria;
				if(($declaratoria<1)||($declaratoria>4))
				{
					$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
								<idTipoAcuse>1</idTipoAcuse>
								<idAcuse>0</idAcuse>
								<idCtrProcedimiento>0</idCtrProcedimiento>
								<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
								<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
								<resultado>0</resultado>
								<carpetaAdministrativa></carpetaAdministrativa>
								<comentarios>El valor del nodo declaratoria NO es valido</comentarios>
								<documentoAdjunto></documentoAdjunto>
							</acuse>';
					return $resultado;
				}
			}
			
			/*$fechaVencimiento=(string)$cXML->DatosSolicitud[0]->fechaFenece;
			
			
			if(strtotime($fechaVencimiento)<$fechaActual)
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La solicitud ya ha fenecido</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}*/
			
			$arrDocumentosReferencia=array();
			$idActividad=generarIDActividad(46,-1);
			
			foreach($cXML->Delitos[0] as $delito)
			{

				$arrDocumentosReferenciaDelito=array();
				$arrValoresDelito=array();
				$arrValoresDelito["tituloDelito"]=-1;
				$arrValoresDelito["capituloDelito"]=-1;
				$arrValoresDelito["denominacionDelito"]=(string)$delito->cvedelito;
				$arrValoresDelito["calificativo"]=(string)$delito->formacomision;
				$arrValoresDelito["gradoRealizacion"]=(string)$delito->gradorealizacion;
				$arrValoresDelito["idActividad"]=$idActividad;
				$arrValoresDelito["modalidadDelito"]=(string)$delito->cvemodalidad;
				
				
				
				$idRegistroDelito=crearInstanciaRegistroFormulario(61,-1,1,$arrValoresDelito,$arrDocumentosReferenciaDelito,-1,264);
				$x=0;
				$query[$x]="begin";
				$x++;
				
				//Nuevo
				$cveDelito=(string)$delito->cvedelito;
				$descDelito=(string)$delito->descdelito;
				$cveModalidad=(string)$delito->cvemodalidad;
				$modalidad=(string)$delito->descmodalidad;
				
				$consulta="SELECT id__35_denominacionDelito FROM _35_denominacionDelito WHERE claveDenominacionDelito='".$cveDelito."'";
				$iDelito=$con->obtenerValor($consulta);
				if($iDelito=="")
				{
					$query[$x]="INSERT INTO _35_denominacionDelito(id__35_denominacionDelito,idReferencia,claveDenominacionDelito,denominacionDelito)
								VALUES(".$cveDelito.",1,'".$cveDelito."','".cv($descDelito)."')";
					$x++;
					
					
					$iDelito=$cveDelito;
					$query[$x]="INSERT INTO _62_tablaDinamica(id__62_tablaDinamica,idReferencia,codigo,delito) VALUES(".$iDelito.
								",1,'".$iDelito."','".$iDelito."')";	
					$x++;	
				}
				
				$consulta="SELECT id__62_clasificacion FROM _62_clasificacion WHERE idReferencia=".$iDelito." AND clave='".$cveModalidad."'";
				$iModalidad=$con->obtenerValor($consulta);
				if($iModalidad=="")
				{
					$query[$x]="INSERT INTO _62_clasificacion(idReferencia,clave,nombreModalidad) 
								VALUES(".$iDelito.",'".$cveModalidad."','".cv($modalidad)."')";
					$x++;
					
				}
				
				
				//.--
				$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$idActividad." AND idFiguraJuridica=4";
				$resParticipanteDelito=$con->obtenerFilas($consulta);
				while($fParticipante=mysql_fetch_row($resParticipanteDelito))
				{
					$query[$x]="INSERT INTO _61_chkDelitosImputado(idPadre,idOpcion) VALUES(".$idRegistroDelito.",".$fParticipante[0].")";
					$x++;
				}
				
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
				
			}
			
			$arrValores=array();
			$idRegistroSolicitud=-1;
			
			
			$datosDocumento=array();			
			$datosDocumento["nombreDocumento"]=(string)$cXML->documentoAnexo[0]->nombreDocumento;
			$datosDocumento["descripcionDocumento"]=(string)$cXML->documentoAnexo[0]->descripcionDocumento;
			$datosDocumento["contenido"]=(string)$cXML->documentoAnexo[0]->contenido;
			
			$idDocumentoServidor=-1;
			if($datosDocumento["contenido"]!="")
			{
				$idDocumento=generarNombreArchivoTemporal();
				$directorioDestino=$directorioInstalacion.'\\archivosTemporales\\'.$idDocumento;
				$datos=bD($datosDocumento["contenido"]);
				$f=file_put_contents($directorioDestino,$datos);
				
				if($f)
				{
					$idDocumentoServidor=registrarDocumentoServidor($idDocumento,$datosDocumento["nombreDocumento"]);
					$consulta="UPDATE 908_archivos SET descripcion='".cv($datosDocumento["descripcionDocumento"])."' WHERE idArchivo=".$idDocumentoServidor;
					
					$con->ejecutarConsulta($consulta);
				}
			}		
			
			$arrValores["folioCarpetaInvestigacion"]=(string)$cXML->DatosSolicitud[0]->carpetainvestigacion;
			$arrValores["tipoProgramacionAudiencia"]=1;
			$tipoAudiencia=1;
			$idEtapa=1.4;
			
			
			$consulta="SELECT tipoAudiencia FROM _285_tablaDinamica WHERE cveTipoSolicitud='".(string)$cXML->DatosSolicitud[0]->cvesolicitud."'";
			$tipoAudiencia=$con->obtenerValor($consulta);
			if($tipoAudiencia=="")
				$tipoAudiencia=103;
			else
			{
				if($tipoAudiencia==1)
					$idEtapa=1.4;
			}
			$fechaFenece=(string)$cXML->DatosSolicitud[0]->fechaFenece;
			$arrValores["fechaFenece"]=($fechaFenece=="")?"NULL":$fechaFenece;
			$arrValores["declaratoria"]=(string)$cXML->DatosSolicitud[0]->declaratoria;
			
			if(isset($arrSolicitudesPermitidas[$cveSolicitud]))
			{
				$arrValores["declaratoria"]=1;
			}
			
			$arrValores["tipoAudiencia"]=$tipoAudiencia;
			$arrValores["idActividad"]=$idActividad;
			$arrValores["delitoGrave"]=($arrValores["declaratoria"]<3)?0:1;
			$arrValores["ctrlSolicitud"]=(string)$cXML->DatosSolicitud[0]->ctrlsolicitud;
			$arrValores["idSolicitud"]=(string)$cXML->DatosSolicitud[0]->idsolicitud;
			$arrValores["cveSolicitud"]=(string)$cXML->DatosSolicitud[0]->cvesolicitud;
			$arrValores["solicitudXML"]=bE($cadXML);			
			$arrValores["ctrluinv"]=(string)$cXML->DatosSolicitud[0]->ctrluinv;
			if($cAdministrativa!="")
			{
				
				$arrValores["carpetaAdministrativa"]=$cAdministrativa;
				$arrValores["delitoGrave"]=$fDatosCarpetaBase[1];
			}
			
			//
			
			
			$idRegistroSolicitud=crearInstanciaRegistroFormulario(46,-1,$idEtapa,$arrValores,$arrDocumentosReferencia,-1,264);
			
			
			
			if($idDocumentoServidor!=-1)
			{
				convertirDocumentoUsuarioDocumentoResultadoProceso($idDocumentoServidor,46,$idRegistroSolicitud,($datosDocumento["nombreDocumento"]).".pdf",14);
			}
			
			if(!$servidorPruebas)
				@enviarCorreoWebServicesSolicitudInicial($idRegistroSolicitud);
			
			if($cAdministrativa!="")
			{
				
				$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cAdministrativa."'";
				$unidadGestion=$con->obtenerValor($consulta);
				
				$consulta="SELECT id__17_tablaDinamica,idReferencia FROM _17_tablaDinamica WHERE claveUnidad='".$unidadGestion."'";
				$fRegistroUnidad=$con->obtenerPrimeraFila($consulta);
				
				$consulta="INSERT INTO 7000_eventosAudiencia(situacion,fechaAsignacion,idEdificio,idCentroGestion,
						idFormulario,idRegistroSolicitud,idReferencia,idEtapaProcesal,tipoAudiencia)
						values(0,'".date("Y-m-d H:i:s")."',".$fRegistroUnidad[1].",".$fRegistroUnidad[0].",46,".
						$idRegistroSolicitud.",-1,1,".$tipoAudiencia.")";
				$con->ejecutarConsulta($consulta);
			}
			
			$arrDocumentosReferencia=array();
			$arrValores=array();
			
			$arrValores["claveFiscalia"]=(string)$cXML->DatosSolicitud[0]->cvefiscalia;
			if($arrValores["claveFiscalia"]==62)
				$arrValores["claveFiscalia"]=33;
			
			if($arrValores["claveFiscalia"]==44)
				$arrValores["claveFiscalia"]=20;
			$arrValores["claveAgencia"]=(string)$cXML->DatosSolicitud[0]->cveagencia;
			$arrValores["claveUnidad"]=(string)$cXML->DatosSolicitud[0]->cveunidad;
			$arrValores["nombre"]=(string)$cXML->DatosSolicitud[0]->mpSolicitante;
			$arrValores["claveCoorTerMP"]=(string)$cXML->DatosSolicitud[0]->cvecoorterrMP;
			
			$idRegistroMP=crearInstanciaRegistroFormulario(100,$idRegistroSolicitud,1,$arrValores,$arrDocumentosReferencia,-1,264);
	
			$query=array();
			$x=0;
			$query[$x]="begin";
			$x++;
			$correo=(string)$cXML->DatosSolicitud[0]->correoMP;
			if($correo!="")
			{
				$query[$x]="INSERT INTO _100_gridCorreosFiscal(idReferencia,correoElectronico) VALUES(".$idRegistroMP.",'".$correo."')";
				$x++;
			}
			
			$query[$x]="commit";
			$x++;
			
			$con->ejecutarBloque($query);	
			
			foreach($cXML->Personas[0] as $p)
			{
				$arrDocumentosReferencia=array();
				$iFigura=(string)$p->figurajuridica;
				$consulta="SELECT figuraEquivalente FROM _284_tablaDinamica WHERE id__284_tablaDinamica=".$iFigura;
				$figuraJuridica=$con->obtenerValor($consulta);
				if($figuraJuridica=="")
					$figuraJuridica=9;
				$arrValores=array();
				$idRegistroParticipante=-1;				
				$arrValores["tipoPersona"]=1;
				$arrValores["apellidoPaterno"]=(string)$p->paterno;
				$arrValores["apellidoMaterno"]=(string)$p->materno;
				$sexo=trim((string)$p->cvesexo);
				if($sexo=="")
					$sexo=2;
				else
					$sexo=$sexo-1;
				$arrValores["genero"]=$sexo;//Revisar
				
				$edad=trim((string)$p->edad);
	
				$arrValores["edad"]=($edad=="")?"NULL":$edad;
				$arrValores["curp"]=trim((string)$p->curp);
				
				$fechaNacimiento=trim((string)$p->fechanacimiento);
				$arrValores["fechaNacimiento"]=($fechaNacimiento=="")?"NULL":$fechaNacimiento;
				$arrValores["estadoCivil"]=(string)$p->cveestadocivil;
				$arrValores["tipoIdentificacion"]=(string)$p->cveidentificacion;
				$arrValores["folioIdentificacion"]=(string)$p->folioidentificacion;
				
				$arrValores["nombre"]=(string)$p->nombre;
				$arrValores["esMexicano"]=((string)$p->nacionalidadmexicana=="2")?"3":(string)$p->nacionalidadmexicana;
				$cvenacionalidad=trim((string)$p->cvenacionalidad);
				
				if($cvenacionalidad==1)
				{
					$cvenacionalidad="NULL";
					$arrValores["esMexicano"]=1;
				}
				$arrValores["nacionalidad"]=($cvenacionalidad=="")?"NULL":$cvenacionalidad;
				
				$arrValores["idActividad"]=$idActividad;
				$arrValores["figuraJuridica"]=$figuraJuridica;
				$tipoDefensor=0;
				if($figuraJuridica==5)
				{
					if($iFigura==52)
						$tipoDefensor=2;
					else
						$tipoDefensor=1;
				}
				$arrValores["tipoDefensor"]=$tipoDefensor;
				$arrValores["tipoFiguraPGJ"]=$iFigura;
	
				$idRegistroParticipante=crearInstanciaRegistroFormulario(47,-1,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
				
				$consulta="INSERT INTO 7005_relacionFigurasJuridicasSolicitud(idActividad,idParticipante,idFiguraJuridica) 
							VALUES(".$idActividad.",".$idRegistroParticipante.",".$figuraJuridica.")";
				$con->ejecutarConsulta($consulta);
				$query=array();
				$x=0;
				$query[$x]="begin";
				$x++;
				if(sizeof($p->nombrealterno)>0)
				{
					foreach($p->nombrealterno[0] as $a)
					{
						$query[$x]="INSERT INTO _47_gridAlias(idReferencia,alias) VALUES(".$idRegistroParticipante.",'".cv((string)$a)."')";
						$x++;
						
					}
				}
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
				
				foreach($p->Direcciones[0] as $d)
				{
					$arrDocumentosReferencia=array();
					
					$arrValores=array();
								
					$arrValores["calle"]=(string)$d->calle;
					$arrValores["noExt"]=(string)$d->numeroext;
					$arrValores["noInterior"]=(string)$d->numeroint;
					$arrValores["colonia"]=(string)$d->colonia;
					$arrValores["entreCalle"]=(string)$d->entrecalle;
					$arrValores["yCalle"]=(string)$d->ycalle;
					$arrValores["otrasReferencias"]=(string)$d->referencia;
					$arrValores["entidadFederativa"]=(string)$d->cveentidadfederativa;
					if($arrValores["entidadFederativa"]=="---")
						$arrValores["entidadFederativa"]="IND";
						
					$consulta="SELECT cveEstado FROM 820_estados WHERE cvePGJ='".$arrValores["entidadFederativa"]."'";	
					$arrValores["entidadFederativa"]=$con->obtenerValor($consulta);	
					
					$arrValores["municipio"]=(string)$d->cvemunicipio;
					$arrValores["localidad"]="NULL";
					$codigoPostal="NULL";
					
					if(trim((string)$d->codigopostal)!="")
					{
						$codigoPostal=trim((string)$d->codigopostal);
					}
					$arrValores["codigoPostal"]=$codigoPostal;
	
	
					$idRegistroDomicilio=crearInstanciaRegistroFormulario(48,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
					
					$query=array();
					$x=0;
					$query[$x]="begin";
					$x++;
					$correo=(string)$d->correoelectronico;
					if($correo!="")
					{
						$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
						$x++;
					}
					
					$telefono=(string)$d->telefono;
					if($telefono!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
						$x++;
					}
					$celular=(string)$d->celular;
					if($celular!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
						$x++;
					}
					$query[$x]="commit";
					$x++;
					
					$con->ejecutarBloque($query);
					
				}
				
				
				$arrValores=array();
				$arrValores["nivelEscolaridad"]=(string)$p->cveescolaridad;
				if($arrValores["nivelEscolaridad"]==0)
					$arrValores["nivelEscolaridad"]=101;
				/*else
				{
					$consulta="SELECT claveNivelEscolaridad FROM _30_tablaDinamica WHERE id__30_tablaDinamica=".$arrValores["nivelEscolaridad"];
					$arrValores["nivelEscolaridad"]=$con->obtenerValor($consulta);
				}*/
				$tipoOcupacion="NULL";
				if(trim((string)$p->ocupacion)!="")
					$tipoOcupacion=(string)$p->ocupacion;
				$arrValores["tipoOcupacion"]=99;
				$arrValores["otraOcupacion"]=$tipoOcupacion;
				
				$arrValores["religion"]=(string)$p->cvereligion;
				
				$arrValores["grupoEtnico"]=(string)$p->cvegrupoetnico;
				
				$perteneceGrupoEtnico=0;
				switch($arrValores["grupoEtnico"])
				{
					case 888:
						$perteneceGrupoEtnico=0;
					break;
					
					default:
						$perteneceGrupoEtnico=1;
					break;
						
				}
				$arrValores["perteneceGrupoEtnico"]=$perteneceGrupoEtnico;
				$lgbttti="2";
				if(trim((string)$p->lgbttti)!="")
				{
					$lgbttti=(string)$p->lgbttti;
				}
				$arrValores["lgbttti"]=$lgbttti;
				
				$poblacionLGBTTI="NULL";
				if(trim((string)$p->tipoLGBTTTI)!="")
				{
					$poblacionLGBTTI=(string)$p->tipoLGBTTTI;
				}
				$arrValores["poblacion"]=$poblacionLGBTTI;
				
				$requiereTraductor="2";
				if(trim((string)$p->requiereTraductor)!="")
				{
					$requiereTraductor=(string)$p->requiereTraductor;
				}
				
				$arrValores["requiereTraductor"]=$requiereTraductor;
				
				$requiereInterprete="2";
				if(trim((string)$p->requiereinterprete)!="")
				{
					$requiereInterprete=(string)$p->requiereinterprete;
				}
				
				$arrValores["requiereInterprete"]=$requiereInterprete;
				$arrValores["lengua"]=trim((string)$p->cvelenguadialecto)==""?"NULL":trim((string)$p->cvelenguadialecto);
				
				$capacidadesDiferente="2";
				if(trim((string)$p->capacidadesdiferentes)!="")
				{
					$capacidadesDiferente=(string)$p->capacidadesdiferentes;
				}
				$arrValores["capacidadesDiferente"]=$capacidadesDiferente;
				
				$arrValores["descripcionDiscapacidad"]=trim((string)$p->desccapacidaddiferente);
				
				$poblacionCallejera=2;
				if(trim((string)$p->poblacioncallejera)!="")
				{
					$poblacionCallejera=(string)$p->poblacioncallejera;
				}
				$arrValores["poblacionCallejera"]=$poblacionCallejera;
	
				$idRegistroDomicilio=crearInstanciaRegistroFormulario(49,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
			}
			
			foreach($cXML->PersonasJuridicas[0] as $p)
			{
				$arrDocumentosReferencia=array();
				$iFigura=(string)$p->figurajuridica;
				
				if($iFigura==0)
					continue;
				
				$consulta="SELECT figuraEquivalente FROM _284_tablaDinamica WHERE id__284_tablaDinamica=".$iFigura;
				$figuraJuridica=$con->obtenerValor($consulta);
				if($figuraJuridica=="")
					$figuraJuridica=9;
				$arrValores=array();
				$idRegistroParticipante=-1;				
				$arrValores["tipoPersona"]=2;
				$arrValores["tipoFiguraPGJ"]=$iFigura;
				$arrValores["figuraJuridica"]=$figuraJuridica;
				$arrValores["rfcEmpresa"]=(string)$p->rfc;
				$arrValores["idActividad"]=$idActividad;

				
				$idRegistroParticipante=crearInstanciaRegistroFormulario(47,-1,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
				foreach($p->Direcciones[0] as $d)
				{
					$arrDocumentosReferencia=array();
					
					$arrValores=array();
								
					$arrValores["calle"]=(string)$d->calle;
					$arrValores["noExt"]=(string)$d->numeroext;
					$arrValores["noInterior"]=(string)$d->numeroint;
					$arrValores["colonia"]=(string)$d->calle;
					$arrValores["entreCalle"]=(string)$d->entrecalle;
					$arrValores["yCalle"]=(string)$d->ycalle;
					$arrValores["otrasReferencias"]=(string)$d->referencia;
					$arrValores["entidadFederativa"]=(string)$d->cveentidadfederativa;
					if($arrValores["entidadFederativa"]=="---")
						$arrValores["entidadFederativa"]="IND";
						
					$consulta="SELECT cveEstado FROM 820_estados WHERE cvePGJ='".$arrValores["entidadFederativa"]."'";	
					$arrValores["entidadFederativa"]=$con->obtenerValor($consulta);	
					
					$arrValores["municipio"]=(string)$d->cvemunicipio;
					$arrValores["localidad"]="NULL";
					
					
					$codigoPostal="NULL";
					
					if(trim((string)$d->codigopostal)!="")
					{
						$codigoPostal=trim((string)$d->codigopostal);
					}
					$arrValores["codigoPostal"]=($codigoPostal=="00000B")?"NULL":$codigoPostal;
	
					$idRegistroDomicilio=crearInstanciaRegistroFormulario(48,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
					
					$query=array();
					$x=0;
					$query[$x]="begin";
					$x++;
					$correo=(string)$d->correoelectronico;
					if($correo!="")
					{
						$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
						$x++;
					}
					
					$telefono=(string)$d->telefono;
					if($telefono!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
						$x++;
					}
					$celular=(string)$d->celular;
					if($celular!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
						$x++;
					}
					$query[$x]="commit";
					$x++;
					
					$con->ejecutarBloque($query);
					
				}
				
				$query=array();
				$x=0;
				$query[$x]="begin";
				$x++;
				$correo=(string)$d->correoelectronico;
				if($correo!="")
				{
					$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
					$x++;
				}
				
				$telefono=(string)$d->telefono;
				if($telefono!="")
				{
					$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
					$x++;
				}
				$celular=(string)$d->celular;
				if($celular!="")
				{
					$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
					$x++;
				}
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
			}		
			
			/*$consulta="SELECT carpetaAdministrativa FROM _46_tablaDinamica WHERE id__46_tablaDinamica=".$idRegistroSolicitud;
			$carpetaAdministrativa=$con->obtenerValor($consulta);
			if($carpetaAdministrativa=="")*/
				$carpetaAdministrativa="000/0000/2016";
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>'.$idRegistroSolicitud.'</idAcuse>
							<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>1</resultado>
						 	<carpetaAdministrativa >'.$carpetaAdministrativa.'</carpetaAdministrativa>
							<comentarios></comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
		}
		catch(Exception $e)
		{
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>'.$e->getMessage().'</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
			
		}
		return ($resultado);
	}
	
	

	function registrarSolicitudPromocionAsesores($cadXML)
	{

		global $con;
		global $directorioInstalacion;	
		global $servidorPruebas;
		$fechaActual=strtotime(date("Y-m-d H:i:s"));
		try
		{
			$_SESSION["idUsr"]=2390;
			$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
			
			$resRoles=$con->obtenerFilas($consulta);
			$listaGrupo="";
			while($fRoles=mysql_fetch_row($resRoles))
			{
				$arrRol=explode("_",$fRoles[0]);
				$rol="'".$fRoles[0]."'";
				if($arrRol[1]!="0")
					$rol.=",'".$arrRol[0]."_-1'";
				
				if($listaGrupo=="")
					$listaGrupo=$rol;
				else
					$listaGrupo.=",".$rol;
			}
			if($listaGrupo=="")
				$listaGrupo='-1';
			$_SESSION["idRol"]=$listaGrupo.",'-100_0'";
			$_SESSION["codigoUnidad"]="0001";
			$_SESSION["codigoInstitucion"]="0001";
			
			
			$cXML=simplexml_load_string($cadXML);
			
			$consulta="INSERT INTO 3011_solicitudRecibidasPGJ(fechaSolicitud,xmlSolicitud,carpetaInvestigacion,idFormulario) 
					VALUES('".date("Y-m-d H:i:s",$fechaActual)."','".bE($cadXML)."','".((string)$cXML->DatosSolicitud[0]->carpetainvestigacion)."',96)";
			$con->ejecutarConsulta($consulta);
			
			$arrDocumentosReferencia=array();			
			
			$arrValores=array();
			$idRegistroSolicitud=-1;			
			
			$consulta="SELECT COUNT(*) FROM _96_tablaDinamica WHERE ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrSolicitud."' 
					AND idSolicitud= ".((string)$cXML->DatosSolicitud[0]->idSolicitud);
			
			$nRegistro=$con->obtenerValor($consulta);
			if($nRegistro>0)
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La solicitud ya ha sido registrada anteriormente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			

			$datosDocumento=array();
			
			$datosDocumento["nombreDocumento"]=(string)$cXML->DatosSolicitud[0]->nombreDocumento;
			$datosDocumento["nombreDocumento"]=str_replace(".","",$datosDocumento["nombreDocumento"]);
			$datosDocumento["descripcionDocumento"]="";//(string)$cXML->DatosSolicitud[0]->descripcionDocumento;
			$datosDocumento["contenido"]=(string)$cXML->DatosSolicitud[0]->documentoAdjunto;
			
			$idDocumentoServidor=-1;
			if($datosDocumento["contenido"]!="")
			{
				$idDocumento=generarNombreArchivoTemporal();
				$directorioDestino=$directorioInstalacion.'\\archivosTemporales\\'.$idDocumento;
				$datos=bD($datosDocumento["contenido"]);
				$f=file_put_contents($directorioDestino,$datos);
				
				if($f)
				{
					$idDocumentoServidor=registrarDocumentoServidor($idDocumento,$datosDocumento["nombreDocumento"]);
					$consulta="UPDATE 908_archivos SET descripcion='".cv($datosDocumento["descripcionDocumento"])."' WHERE idArchivo=".$idDocumentoServidor;
					
					$con->ejecutarConsulta($consulta);
				}
			}			
			$arrValores["asuntoPromocion"]=utf8_decode(((string)$cXML->DatosSolicitud[0]->cuerpoTexto));//(string)$cXML->DatosSolicitud[0]->carpetainvestigacion;
			
			$arrValores["numeroPromocion"]=(string)$cXML->DatosSolicitud[0]->idSolicitud;
			/*$tipoAudiencia=24;
			switch((string)$cXML->DatosSolicitud[0]->cveSolicitud)
			{
				case 28:
					$tipoAudiencia=1;
				break;
				case 33:
					$tipoAudiencia=26;
				break;
			}*/
			$consulta="SELECT tipoAudiencia,tipoTratamientoSolicitud FROM _285_tablaDinamica WHERE cveTipoSolicitud='".(string)$cXML->DatosSolicitud[0]->cveSolicitud."'";
			$fTipoAudiencia=$con->obtenerPrimeraFila($consulta);
			
			$tipoAudiencia=$fTipoAudiencia[0];
			
			
			if($tipoAudiencia=="")
				$tipoAudiencia=103;
			
			$arrValores["tipoPromociones"]=$fTipoAudiencia[1];
			
			$arrValores["tipoAudiencia"]=$tipoAudiencia;
			
			
			$tipoAtencion=0;
			if($arrValores["tipoPromociones"]==2)
			{
				$consulta="SELECT tipoAtencion FROM _4_tablaDinamica WHERE id__4_tablaDinamica=".$tipoAudiencia;
				$tipoAtencion=$con->obtenerValor($consulta);
				if($tipoAtencion=="")
					$tipoAtencion=0;
			}
			
			$tipoSolicitud=0;
			if($tipoAtencion==2)
				$tipoSolicitud=2;
			else
				$tipoSolicitud=3;
			
			$arrValores["carpetaAdministrativa"]=(string)$cXML->DatosSolicitud[0]->carpetaadministrativa;
			
			if(($arrValores["carpetaAdministrativa"]=="")||($arrValores["carpetaAdministrativa"]=="SIN CARPETA"))
			{
				
				$consulta="SELECT carpetaAdministrativa FROM _46_tablaDinamica WHERE folioCarpetaInvestigacion='".
							cv(((string)$cXML->DatosSolicitud[0]->carpetainvestigacion))."' and idEstado>=1.4  order by fechaCreacion DESC";

				$cAdministrativa=$con->obtenerValor($consulta);
				if($cAdministrativa!="")
				{
					$arrValores["carpetaAdministrativa"]=$cAdministrativa;
				}
				
				
			}
			
			
			if($arrValores["carpetaAdministrativa"]=="")
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<comentarios>Debe especificar la carpeta administrativa a la cual pertenece la promocion</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			
			
			$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativaBase='".$arrValores["carpetaAdministrativa"]."' ORDER BY fechaCreacion DESC";
			$cAdministrativaAux=$con->obtenerValor($consulta);
			
			if($cAdministrativaAux!="")
			{
				$arrValores["carpetaAdministrativa"]=$cAdministrativaAux;
			}
			
			
			$consulta="SELECT count(*) FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$arrValores["carpetaAdministrativa"]."'";
			$nCarpeta=$con->obtenerValor($consulta);
			if($nCarpeta==0)
			{
				$consulta="SELECT carpetaAdministrativa FROM _46_tablaDinamica WHERE folioCarpetaInvestigacion='".cv(((string)$cXML->DatosSolicitud[0]->carpetainvestigacion))."' and idEstado>=1.4  order by fechaCreacion DESC";

				$cAdministrativa=$con->obtenerValor($consulta);
				if($cAdministrativa!="")
				{
					$arrValores["carpetaAdministrativa"]=$cAdministrativa;
				}
				else
				{
					$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
								<idTipoAcuse>1</idTipoAcuse>
								<idAcuse>0</idAcuse>
								<idCtrProcedimiento>0</idCtrProcedimiento>
								<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
								<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
								<resultado>0</resultado>
								<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
								<comentarios>La carpeta administrativa ingresada NO existe</comentarios>
								<documentoAdjunto></documentoAdjunto>
							</acuse>';
					return $resultado;
				}
			}
			
			$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$arrValores["carpetaAdministrativa"]."'";
			$unidadGestionCarpeta=$con->obtenerValor($consulta);		

			$fechaCreacion=$fechaActual;
			
			
			
			if($tipoSolicitud!=2)
			{
				if(esDiaHabil(date("Y-m-d",$fechaCreacion)))
				{
					if($fechaCreacion>strtotime(date("Y-m-d",$fechaCreacion)." 15:00:00"))
					{
						$fechaHabil=obtenerProximoDiaHabil(date("Y-m-d",strtotime("+1 days",$fechaCreacion)));
						$fechaCreacion=strtotime($fechaHabil." 09:00:00");
					}
					else
					{
						if($fechaCreacion<strtotime(date("Y-m-d",$fechaCreacion)." 09:00:00"))
						{
							$fechaCreacion=strtotime(date("Y-m-d",$fechaCreacion)." 09:00:00");
						}
					}
				}
				else
				{
					$fechaHabil=obtenerProximoDiaHabil(date("Y-m-d",strtotime("+1 days",$fechaCreacion)));
					$fechaCreacion=strtotime($fechaHabil." 09:00:00");
				}
			}
			
			$idEtapaCambio=1.4;
			
			$arrValores["codigoInstitucion"]=$unidadGestionCarpeta;
			$arrValores["codigoUnidad"]=$unidadGestionCarpeta;
			$arrValores["relacionPromocion"]=1;
			$arrValores["figuraPromovente"]=10;
			$arrValores["fechaCreacion"]=date("Y-m-d H:i:s",$fechaCreacion);
			$arrValores["fechaRecepcion"]=date("Y-m-d",$fechaCreacion);
			$arrValores["horaRecepcion"]=date("H:i:s",$fechaCreacion);		
			$arrValores["ctrlSolicitud"]=(string)$cXML->DatosSolicitud[0]->ctrSolicitud;
			$arrValores["idSolicitud"]=(string)$cXML->DatosSolicitud[0]->idSolicitud;
			$arrValores["cveSolicitud"]=(string)$cXML->DatosSolicitud[0]->cveSolicitud;
			$arrValores["ctrluinv"]=(string)$cXML->DatosSolicitud[0]->ctrluinv;
			$arrValores["solicitudXML"]=bE($cadXML);
			
			switch($tipoAudiencia)
			{
				case 100:
					$arrValores["relacionPromocion"]=2;
					$arrValores["carpetaAdministrativaReferida"]=$arrValores["carpetaAdministrativa"];
					$arrValores["carpetaAdministrativa"]="";
				break;
				
			}
			//
			/*if($idDocumentoServidor!=-1)
			{
				array_push($arrDocumentosReferencia,$idDocumentoServidor);
			}*/
			
			
			
			
			$idRegistroSolicitud=crearInstanciaRegistroFormulario(96,-1,$idEtapaCambio,$arrValores,$arrDocumentosReferencia,-1,613);

			if($idDocumentoServidor!=-1)
			{
				
				convertirDocumentoUsuarioDocumentoResultadoProceso($idDocumentoServidor,96,$idRegistroSolicitud,($datosDocumento["nombreDocumento"]).".pdf",14);
				
			}
			
			
			$cambiarEtapa=true;
			
			switch($tipoAudiencia)
			{
				case 100:
					$idUnidadGestionDestino=obtenerUnidadGestionDestinatariaDocumento($idRegistroSolicitud);
					if($idUnidadGestionDestino==-1)
					{
						@enviarCorreoWebServicesSolicitudPromocionNOIdentificada($idRegistroSolicitud);
						$cambiarEtapa=false;
					}
					else
					{
						$arrValoresUnidad=array();
						$consulta="SELECT claveUnidad FROM _17_tablaDinamica WHERE id__17_tablaDinamica=".$idUnidadGestionDestino;
						$codigoUnidadGestion=$con->obtenerValor($consulta);
						$arrValoresUnidad["unidadGestion"]=$codigoUnidadGestion;
						$arrDocumentosReferenciaAux=array();
						$idRegistroSolicitudAux=crearInstanciaRegistroFormulario(360,$idRegistroSolicitud,1,$arrValoresUnidad,$arrDocumentosReferenciaAux,-1,613);
					}
				break;
				case 103:
					if(!$servidorPruebas)
					{
						
						@enviarCorreoWebServicesSolicitudPromocionNOIdentificada($idRegistroSolicitud);
						$cambiarEtapa=false;
					}
				break;
			}
			
			
			if(!$servidorPruebas)
			{
				if($tipoSolicitud==2)
				{
					
					$unidadGestion=asignarCarpetaGuardia($arrValores["carpetaAdministrativa"],$arrValores["fechaCreacion"]);
					if($unidadGestion!=-1)
					{
						@asignarCarpetaUnidadGestionGuardia($arrValores["carpetaAdministrativa"],$unidadGestion);
					}
					@enviarCorreoWebServicesSolicitudPromocionUrgente($idRegistroSolicitud);
					
					//cambiarEtapaFormulario(96,$idRegistroSolicitud,5,"",-1,"NULL","NULL",704);
					
				}
				
				
			}
			if($cambiarEtapa)
				cambiarEtapaFormulario(96,$idRegistroSolicitud,5,"",-1,"NULL","NULL",704);
			
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>'.$idRegistroSolicitud.'</idAcuse>
							<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>1</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<comentarios></comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
		}
		catch(Exception $e)
		{
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<comentarios>'.$e->getMessage().'</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
			
		}
		return $resultado;
	}
	
	class soap_serverPGJ extends nusoap_server 
	{
		function parseRequest($headers, $data) 
		{
			
			$this->debug('Entering parseRequest() for data of length ' . strlen($data) . ' headers:');
			$this->appendDebug($this->varDump($headers));
			if (!isset($headers['content-type'])) {
				$this->setError('Request not of type text/xml (no content-type header)');
				return false;
			}
			if (!strstr($headers['content-type'], 'text/xml')) {
				$this->setError('Request not of type text/xml');
				return false;
			}
			if (strpos($headers['content-type'], '=')) {
				$enc = str_replace('"', '', substr(strstr($headers["content-type"], '='), 1));
				$this->debug('Got response encoding: ' . $enc);
				if(preg_match('/^(ISO-8859-1|US-ASCII|UTF-8)$/i',$enc)){
					$this->xml_encoding = strtoupper($enc);
				} else {
					$this->xml_encoding = 'US-ASCII';
				}
			} else {
				// should be US-ASCII for HTTP 1.0 or ISO-8859-1 for HTTP 1.1
				$this->xml_encoding = 'ISO-8859-1';
			}
			$this->debug('Use encoding: ' . $this->xml_encoding . ' when creating nusoap_parser');
			// parse response, get soap parser obj
			$parser = new nusoap_parser(utf8_encode($data),$this->xml_encoding,'',$this->decode_utf8);
			// parser debug
			$this->debug("parser debug: \n".$parser->getDebug());
			// if fault occurred during message parsing
			if($err = $parser->getError()){
				$this->result = 'fault: error in msg parsing: '.$err;
				$this->fault('SOAP-ENV:Client',"error in msg parsing:\n".$err);
			// else successfully parsed request into soapval object
			} else {
				// get/set methodname
				$this->methodURI = $parser->root_struct_namespace;
				$this->methodname = $parser->root_struct_name;
				$this->debug('methodname: '.$this->methodname.' methodURI: '.$this->methodURI);
				$this->debug('calling parser->get_soapbody()');
				$this->methodparams = $parser->get_soapbody();
				// get SOAP headers
				$this->requestHeaders = $parser->getHeaders();
				// get SOAP Header
				$this->requestHeader = $parser->get_soapheader();
				// add document for doclit support
				$this->document = $parser->document;
			}
		 }
	}
	
	
	$arrParam=array();
	$server = new soap_serverPGJ;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('registrarSolicitudInicialAsesores',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias iniciales');
	$server->register('registrarSolicitudPromocionAsesores',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias, promociones');
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);