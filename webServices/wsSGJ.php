<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	
	
	include_once("latis/sgjp/siajop.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	
	function obtenerEventosSala($idSala,$start,$end,$idEvento)
	{
		global $con;
		$arrFilasEventos=array();
		$consulta="SELECT if(horaInicioReal is null,horaInicioEvento,horaInicioReal),if(horaTerminoReal is null,horaFinEvento,horaTerminoReal),
				(SELECT tipoAudiencia FROM _4_tablaDinamica WHERE id__4_tablaDinamica=a.tipoAudiencia),idRegistroEvento,fechaEvento,idCentroGestion FROM 7000_eventosAudiencia a
				WHERE idSala=".$idSala." AND fechaEvento>='".$start."' AND fechaEvento<='".$end."' and 
				a.situacion in (SELECT idSituacion FROM 7011_situacionEventosAudiencia WHERE considerarDiponibilidad=1) 
				and idRegistroEvento<>".$idEvento;
	
	
		if($idSala==156)
		{
			$consulta="SELECT if(horaInicioReal is null,horaInicioEvento,horaInicioReal),if(horaTerminoReal is null,horaFinEvento,horaTerminoReal),
				(SELECT tipoAudiencia FROM _4_tablaDinamica WHERE id__4_tablaDinamica=a.tipoAudiencia),idRegistroEvento,fechaEvento,idCentroGestion FROM 7000_eventosAudiencia a
				WHERE idSala in(156,3021) AND fechaEvento>='".$start."' AND fechaEvento<='".$end."' and 
				a.situacion in (SELECT idSituacion FROM 7011_situacionEventosAudiencia WHERE considerarDiponibilidad=1) 
				and idRegistroEvento<>".$idEvento;
		}
	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			array_push($arrFilasEventos,$fila);
		}
		return json_encode($arrFilasEventos);
		//return $respuesta;
	}


	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('obtenerEventosSala',array('idSala'=>'xsd:string','start'=>'xsd:string','end'=>'xsd:string','idEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>