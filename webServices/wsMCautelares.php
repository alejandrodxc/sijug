<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	
	
	include_once("latis/sgjp/siajop.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');
	
	function obtenerMedidasCautelaresEvento($idEvento)
	{
		global $con;
		
		return utf8_encode(generarNotificacionMedidaCautelar($idEvento));
		
	}
	
	function notificarRespuestaSupervicionMedidaCautelar($notificacion)
	{
		global $con;
		try
		{

			$fechaActual=strtotime(date("Y-m-d H:i:s"));
			$consulta="INSERT INTO 3024_notificacionesRecibidadMedidasCautelares(fechaSolicitud,idFormulario,situacion) 
						VALUES('".date("Y-m-d H:i:s",$fechaActual)."',-1,-1)";
			$con->ejecutarConsulta($consulta);
			$idFolioSolicitud=$con->obtenerUltimoID();
			@registrarDatosPeticion(3024,$idFolioSolicitud,$notificacion);
			
			$cXML=simplexml_load_string($notificacion);
			$folioSolicitud=(string)$cXML->folioSolicitud[0];
			$tipoNotificacion=(string)$cXML->tipoNotificacion[0];
			$carpetaJudicial=(string)$cXML->carpetaJudicial[0];
			$numOficio=(string)$cXML->numOficio[0];
			$fechaRedaccion=(string)$cXML->fechaRedaccion[0];
			$documento=(string)$cXML->documento[0];
			$existeAntecedente=(string)$cXML->existeAntecedente[0];
			$data=(string)$cXML->documento[0]->data;
			$nombreDocumento=(string)$cXML->documento[0]->nombreDocumento;
			
			
			if($folioSolicitud==-1)
			{
				if($carpetaJudicial=="")
				{
					$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<Respuesta>
							<resultado>0</resultado>
							<folioAcuse>Debe especificar la Carpeta Judicial al cual pertenece el informe</folioAcuse>
							<mensajeError></mensajeError>
						</Respuesta>';
					return $resultado;
				}
				
				$consulta="SELECT count(*) FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$carpetaJudicial."'";
				$numReg=$con->obtenerValor($consulta);
				if($numReg==0)
				{
					$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<Respuesta>
							<resultado>0</resultado>
							<folioAcuse>La Carpeta Judicial NO Existe</folioAcuse>
							<mensajeError></mensajeError>
						</Respuesta>';
					return $resultado;
				}
			}
			else
			{
				$consulta="SELECT idRegistroFormato FROM 3000_formatosRegistrados WHERE folioControlDocumento=".$folioSolicitud;
				$idRegistroFormato=$con->obtenerValor($consulta);
				if($idRegistroFormato=="")
				{
					$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<Respuesta>
							<resultado>0</resultado>
							<folioAcuse>El Folio de Solicitud NO Existe</folioAcuse>
							<mensajeError></mensajeError>
						</Respuesta>';
					return $resultado;
				}
				
				$consulta="UPDATE 3024_notificacionesRecibidadMedidasCautelares SET idFormulario=3000, 
							idRegistro=".$idRegistroFormato." where idRegistroSolicitud=".$idFolioSolicitud;
				$con->obtenerValor($consulta);
			}
			
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<Respuesta>
							<resultado>1</resultado>
							<folioAcuse>'.$idFolioSolicitud.'</folioAcuse>
							<mensajeError></mensajeError>
						</Respuesta>';
			return $resultado;
		}
		catch(Exception $e)
		{
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<Respuesta>
							<resultado>1</resultado>
							<folioAcuse>0</folioAcuse>
							<mensajeError>'.cv($e->getMessage()).'</mensajeError>
						</Respuesta>';
			return $resultado;
		}
		
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('obtenerMedidasCautelaresEvento',array('idEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	$server->register('notificarRespuestaSupervicionMedidaCautelar',array('notificacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);