<?php 
	include("latis/conexionBD.php");
	include("latis/nusoap/nusoap.php");
	ini_set('default_socket_timeout', 160000);
	
	
	function saludoUsuario($nombre)
	{
		$cad="Hola ".$nombre;
		return new soapval('return', 'xsd:string', $cad);
		
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;

	$server->register('saludoUsuario',array('nombre'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Buscar paciente participante de la campaña');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	$server->service($input);
	

	
?>	