<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/sgjp/siajop.php");
	include_once("latis/latisErrorHandler.php");
	
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	
	function registrarAsunto($xml,$tipoMateria,$iFormulario,$iRegistro)
	{
		global $con;
		$_SESSION["idUsr"]=2390;
		$_SESSION["idRol"]="'-100_0'";
		$_SESSION["codigoUnidad"]="001";
		$_SESSION["codigoInstitucion"]="001";
		$docXML=($xml);


		$consulta="SELECT * FROM _478_tablaDinamica WHERE iFormulario=".$iFormulario." AND iRegistro=".$iRegistro;
		$fRegistro=$con->obtenerPrimeraFila($consulta);
		if($fRegistro)
		{
			$cosulta="SELECT id__17_tablaDinamica FROM _17_tablaDinamica WHERE claveUnidad='".$fRegistro[8]."'";
			$cveUnidad=$con->obtenerValor($consulta);
			
			$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
								'<respuesta>'.
									'<resultado>1</resultado>'.
									'<expediente>'.$fRegistro[10].'</expediente>'.
									'<juzgado>'.$cveUnidad.'</juzgado>'.
								'</respuesta>';
			return $xmlResp;
			
		}

		$consulta="INSERT INTO 3011_solicitudRecibidasPGJ(fechaSolicitud,xmlSolicitud,carpetaInvestigacion,idFormulario)
				VALUES('".date("Y-m-d H:i:s")."','".bE($xml)."','".$tipoMateria."',478)";
		$con->ejecutarConsulta($consulta);

		$arrRegParticipantes=array();
		$cXML=simplexml_load_string($docXML);

		$idActividad=generarIDActividad(478);

		$arrValores=array();
		$arrValores["tipoJuicio"]=(String)$cXML->tipoJuicio;
		$arrValores["fechaRecepcionOP"]=(String)$cXML->fechaRecepcion;
		$arrValores["iRegistro"]=$iRegistro;
		$arrValores["iFormulario"]=$iFormulario;
		$arrValores["idActividad"]=$idActividad;
		$arrDocumentosReferencia=NULL;
		$idRegistroSolicitud=crearInstanciaRegistroFormulario(478,-1,1,$arrValores,$arrDocumentosReferencia,-1,849);

		foreach($cXML->partes[0] as $p)
		{
			if((string)$p->participacion!="")
			{
				$arrValoresParticipante=array();

				$arrValoresParticipante["apellidoPaterno"]=utf8_decode((String)$p->apPaterno);
				$arrValoresParticipante["apellidoMaterno"]=utf8_decode((String)$p->apMaterno);
				$arrValoresParticipante["nombre"]=utf8_decode((String)$p->nombre);
				$arrValoresParticipante["genero"]=(String)$p->genero;
				$arrValoresParticipante["figuraJuridica"]=(String)$p->participacion;
				$arrValoresParticipante["idActividad"]=$idActividad;
				
				$idRegistroParticipante=crearInstanciaRegistroFormulario(47,$idRegistroSolicitud,0,$arrValoresParticipante,$arrDocumentosReferencia,-1,849);
				$arrRegParticipantes[(String)$p->idParticipante]=$idRegistroParticipante;

				$consulta="INSERT INTO 7005_relacionFigurasJuridicasSolicitud(idActividad,idParticipante,idFiguraJuridica) 
						VALUES(".$idActividad.",".$idRegistroParticipante.",".$arrValoresParticipante["figuraJuridica"].")";
				$con->ejecutarConsulta($consulta);
			}

		}

		foreach($cXML->partes[0] as $p)
		{
			foreach($p->participantesAsociados as $pA)
			{
				if((String)$pA->idParticipante!="")
				{
					$consulta="INSERT INTO _47_personasAsocia(idPadre,idOpcion) VALUES(".$arrRegParticipantes[(String)$p->idParticipante].
							",".$arrRegParticipantes[(String)$pA->idParticipante].")";
					$con->ejecutarConsulta($consulta);
				}
			}
		}


		$idUGA=asignarJuzgadoAsignacionBalanceada(478,$idRegistroSolicitud,$tipoMateria);
	
		if($idUGA>-1)
		{
			$consulta="SELECT claveUnidad FROM _17_tablaDinamica WHERE id__17_tablaDinamica=".$idUGA;
			$cveUnidad=$con->obtenerValor($consulta);
			$consulta="SELECT usuarioJuez FROM _26_tablaDinamica WHERE idReferencia=".$idUGA;
			$juez=$con->obtenerValor($consulta);
			$consulta="UPDATE _478_tablaDinamica SET codigoInstitucion='".$cveUnidad."',juez=".$juez." WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
			$con->ejecutarConsulta($consulta);

			if(cambiarEtapaFormulario(478,$idRegistroSolicitud,1.6,"",-1,"NULL","NULL",857))
			{
				$consulta="SELECT carpetaAdministrativa FROM _478_tablaDinamica WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
				$carpeta=$con->obtenerValor($consulta);
				if($carpeta!="")
				{
					$arrCarpeta=explode("/",$carpeta);
					$consulta="UPDATE _478_tablaDinamica SET noExpediente=".$arrCarpeta[0].",anioExpediente=".$arrCarpeta[1]." WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
					$con->ejecutarConsulta($consulta);
				}
				$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
								'<respuesta>'.
									'<resultado>1</resultado>'.
									'<expediente>'.$carpeta.'</expediente>'.
									'<juzgado>'.$idUGA.'</juzgado>'.
								'</respuesta>';
				return $xmlResp;

			}


		}

		$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
								'<respuesta>'.
									'<resultado>0</resultado>'.
									'<expediente></expediente>'.
									'<juzgado></juzgado>'.
								'</respuesta>';
		return $xmlResp;

	}
	

	function registrarAsuntoOficialiaPartes($xml)
	{
		global $con;
		global $tipoMateria;
		$xml=str_replace("?<?","<?",$xml);
		$idFolioSolicitud="";
		try
		{
			$_SESSION["idUsr"]=2390;
			$_SESSION["idRol"]="'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";
			
			$consulta="INSERT INTO 8000_notificacionesOPC(fechaRegistro,valorRecibido,situacion,iFormulario) 
					VALUES('".date("Y-m-d H:i:s")."','".bE($xml)."',0,478)";
			$con->ejecutarConsulta($consulta);
			
			$idFolioSolicitud=$con->obtenerUltimoID();
			$arrRegParticipantes=array();
			$docXML=str_replace('encoding="UTF-8"','encoding="ISO-8859-1"',$xml);
			$cXML=simplexml_load_string($docXML);

			$cveMateria=(String)$cXML->idMateria;
			
			$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
			$iFormularioMaterias=$con->obtenerValor($consulta);
			
			$consulta="SELECT claveOPC,materia,idCsDocs,ipServidor,puerto,claveMateria FROM _".$iFormularioMaterias."_tablaDinamica WHERE claveOPC='".$cveMateria."'";
			$fMateria=$con->obtenerPrimeraFila($consulta);
			if(!$fMateria)
			{
				$consulta="UPDATE 8000_notificacionesOPC SET situacion=2 WHERE idRegistro=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>1</noError>'.
							  '<mensajeError>La clave de materia NO existe</mensajeError>'.
						  '</respuesta>';
				return $xmlResp;
			}

			if($fMateria[3]=='127.0.0.1')
			{
				$cveJuzgado=(String)$cXML->juzgado;
				
				$consulta="SELECT id__17_tablaDinamica,nombreUnidad,claveUnidad FROM _17_tablaDinamica WHERE  claveOPC='".$cveJuzgado."'";
				$fJuzgado=$con->obtenerPrimeraFila($consulta);
				if(!$fJuzgado)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=3 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>2</noError>'.
							  '<mensajeError>La clave del juzgado NO existe</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				
				
				$cveTipoJuicio=(String)$cXML->tipoJuicio;
				
				$consulta="SELECT id__477_tablaDinamica FROM _477_tablaDinamica WHERE claveOPC='".$cveTipoJuicio."'";
				$fTipoJuicio=$con->obtenerPrimeraFila($consulta);
				
				if(!$fTipoJuicio)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=4 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>3</noError>'.
							  '<mensajeError>La clave del tipo de juicio NO existe</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				$nExpediente=str_pad(((String)$cXML->noExpediente),4,"0",STR_PAD_LEFT);
				
				$expediente=$nExpediente."/".((String)$cXML->anioExpediente);
				$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".
						$expediente."' AND unidadGestion='".$fJuzgado[2]."'";
				$fExpediente=$con->obtenerPrimeraFila($consulta);

				if($fExpediente)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=5 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>4</noError>'.
							  '<mensajeError>El expediente ya ha sido registrado previamente</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				
				$idActividad=generarIDActividad(478);
		
				$arrValores=array();
				$arrValores["tipoJuicio"]=$fTipoJuicio[0];
				$arrValores["fechaRecepcionOP"]=(String)$cXML->fechaRecepcion;
				$arrValores["idActividad"]=$idActividad;
				$arrValores["noExpediente"]=(String)$cXML->noExpediente;
				$arrValores["anioExpediente"]=(String)$cXML->anioExpediente;
				$arrValores["tipoExpediente"]=1;
				$arrValores["materia"]=$fMateria[5];
				
		
				$secretaria=($arrValores["noExpediente"]%2)==1?"A":"B";
				
				$arrValores["secretariaAsignada"]=$secretaria;
				$arrValores["idExpedienteOP"]=(String)$cXML->idExpediente;
				
				if($tipoMateria=="SC")
				{
					$arrValores["fechaRecepcion"]=date("Y-m-d",strtotime($arrValores["fechaRecepcionOP"]));
					$arrValores["horaRecepcion"]=date("H:i",strtotime($arrValores["fechaRecepcionOP"]));;
				}
				
				$arrDocumentosReferencia=NULL;
				$idRegistroSolicitud=crearInstanciaRegistroFormulario(478,-1,1,$arrValores,$arrDocumentosReferencia,-1,849);
				
				if($idRegistroSolicitud!=-1)
				{
					$IDDocumentoExpediente=generarIDDocumentoEscanner(478,$idRegistroSolicitud,51);
					$idDocumentoOPC=(isset($cXML->idDocumento)&&(((String)$cXML->idDocumento)!=""))?(String)$cXML->idDocumento:"-1";
					$consulta="UPDATE 908_archivos SET idDocumentoOPC=".$idDocumentoOPC." WHERE idArchivo=".$IDDocumentoExpediente;
					$con->ejecutarConsulta($consulta);
					
					foreach($cXML->partes[0] as $p)
					{
						if((string)$p->participacion!="")
						{
							$arrValoresParticipante=array();
			
							if(((String)$p->tipoPersona)=="M")
							{
								$arrValoresParticipante["tipoPersona"]=2;
								$arrValoresParticipante["apellidoPaterno"]="";
								$arrValoresParticipante["apellidoMaterno"]="";
								$arrValoresParticipante["nombre"]=((String)$p->razonSocial);
							}
							else
							{
								$arrValoresParticipante["tipoPersona"]=1;
								$arrValoresParticipante["apellidoPaterno"]=((String)$p->apPaterno);
								$arrValoresParticipante["apellidoMaterno"]=((String)$p->apMaterno);
								$arrValoresParticipante["nombre"]=((String)$p->nombre);
								
							}
							$arrValoresParticipante["idOPC"]=(isset($p->idPersona)&&(((String) $p->idPersona)!=""))?(String) $p->idPersona:"-1";
							$arrValoresParticipante["figuraJuridica"]=(String)$p->participacion;
							$arrValoresParticipante["idActividad"]=$idActividad;
							
							$idRegistroParticipante=crearInstanciaRegistroFormulario(47,$idRegistroSolicitud,0,$arrValoresParticipante,$arrDocumentosReferencia,-1,849);

			
							$consulta="INSERT INTO 7005_relacionFigurasJuridicasSolicitud(idActividad,idParticipante,idFiguraJuridica) 
									VALUES(".$idActividad.",".$idRegistroParticipante.",".$arrValoresParticipante["figuraJuridica"].")";
							
							$con->ejecutarConsulta($consulta);
						}
			
					}
					
					$idUGA=$fJuzgado[0];
					$cveUnidad=$fJuzgado[2];
					
					
					
					if($idUGA>-1)
					{
						
						$consulta="SELECT usuarioJuez FROM _26_tablaDinamica WHERE idReferencia=".$idUGA;
						$juez=$con->obtenerValor($consulta);
						if($juez=="")
							$juez=-1;
						$consulta="UPDATE _478_tablaDinamica SET codigoInstitucion='".$cveUnidad."',juez=".$juez." WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
						
						$con->ejecutarConsulta($consulta);
						guardarCarpetasAdministrativaRegistro(478,$idRegistroSolicitud);
						
						if(cambiarEtapaFormulario(478,$idRegistroSolicitud,1.2,"",-1,"NULL","NULL",0))
						{
							$consulta="SELECT secretariaAsignada FROM _478_tablaDinamica WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
							$secretariaAsignada=$con->obtenerValor($consulta);
							$idExpediente=(isset($cXML->idExpediente)&&(((String)$cXML->idExpediente)!=""))?(String)$cXML->idExpediente:"-1";
	
							$consulta="update 7006_carpetasAdministrativas set idExpedienteOPC=".$idExpediente.", secretariaAsignada='".$secretariaAsignada."' WHERE carpetaAdministrativa='".
										$expediente."' AND unidadGestion='".$fJuzgado[2]."' ";
							$con->ejecutarConsulta($consulta);
							$consulta="UPDATE 8000_notificacionesOPC SET situacion=1,iRegistro=".$idRegistroSolicitud." WHERE idRegistro=".$idFolioSolicitud;
							$con->ejecutarConsulta($consulta);
							$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
											'<respuesta>'.
												'<resultado>1</resultado>'.
												'<idFolioRegistro>'.$idRegistroSolicitud.'</idFolioRegistro>'.
												'<noExpediente>'.$expediente.'</noExpediente>'.
												'<noError></noError>'.
												'<mensajeError></mensajeError>'.
											'</respuesta>';
							return $xmlResp;
			
						}
			
			
					}
				}
				else
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=6 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
											'<respuesta>'.
												'<resultado>0</resultado>'.
												'<idFolioRegistro>-1</idFolioRegistro>'.
												'<noExpediente>0000/0000</noExpediente>'.
												 '<noError>5</noError>'.
												 '<mensajeError>Ha ocurrido un problema al registrar la carpeta</mensajeError>'.
											'</respuesta>';
					return $xmlResp;
				}
			}
			else
			{
				$idExpediente=(isset($cXML->idExpediente)&&(((String)$cXML->idExpediente)!=""))?(String)$cXML->idExpediente:"-1";
				$idDocumento=(isset($cXML->idDocumento)&&(((String)$cXML->idDocumento)!=""))?(String)$cXML->idDocumento:"-1";
				$consulta="INSERT INTO _478_expedientesVSArchivos(idDocumento,idCsDocs,idExpediente) VALUES(".$idDocumento.",".$fMateria[2].",".$idExpediente.")";
				
				$con->ejecutarConsulta($consulta);
				
				$urlWebServices="http://".$fMateria[3].":".$fMateria[4]."/webServices/wsJuzgados.php?wsdl";
	
				$client = new nusoap_client($urlWebServices,"wsdl"); 
				$parametros=array();
				$parametros["xml"]=$xml;
				$docXML = $client->call("registrarAsuntoOficialiaPartes", $parametros);
				$cXML=simplexml_load_string($docXML);
				
				/*if((((String)$cXML->resultado)==1)||(((String)$cXML->resultado)==0))
				{
					$consulta="delete from 8000_notificacionesOPC  WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
				}*/
				
				return $docXML;
				
			}
		
		}
		catch(Exception $e)
		{
			$consulta="UPDATE 8000_notificacionesOPC SET situacion=7 WHERE idRegistro=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);
			$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
					  '<respuesta>'.
						  '<resultado>0</resultado>'.
						  '<idFolioRegistro>-1</idFolioRegistro>'.
						  '<noExpediente>0000/0000</noExpediente>'.
						  '<noError>6</noError>'.
						  '<mensajeError>'.cv($e->getMessage()).'</mensajeError>'.
					  '</respuesta>';
			return $xmlResp;
				
			

		}
	}
	
	function registrarExhortoOficialiaPartes($xml)
	{
		global $con;
		global $tipoMateria;
		$idFolioSolicitud="";
		try
		{
			$_SESSION["idUsr"]=2390;
			$_SESSION["idRol"]="'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";

			$consulta="INSERT INTO 8000_notificacionesOPC(fechaRegistro,valorRecibido,situacion,iFormulario) 
					VALUES('".date("Y-m-d H:i:s")."','".bE($xml)."',0,491)";
			$con->ejecutarConsulta($consulta);
			
			$idFolioSolicitud=$con->obtenerUltimoID();
			$arrRegParticipantes=array();
			$docXML=str_replace('encoding="UTF-8"','encoding="ISO-8859-1"',$xml);
			$cXML=simplexml_load_string($docXML);
			
			$cveMateria=(String)$cXML->idMateria;
			
			$consulta="SELECT idFormulario FROM 900_formularios WHERE categoriaFormulario=1";
			$iFormularioMaterias=$con->obtenerValor($consulta);
			
			$consulta="SELECT claveOPC,materia,idCsDocs,ipServidor,puerto,claveMateria FROM _".$iFormularioMaterias."_tablaDinamica WHERE claveOPC='".$cveMateria."'";
			$fMateria=$con->obtenerPrimeraFila($consulta);
			if(!$fMateria)
			{
				$consulta="UPDATE 8000_notificacionesOPC SET situacion=2 WHERE idRegistro=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>1</noError>'.
							  '<mensajeError>La clave de materia NO existe</mensajeError>'.
						  '</respuesta>';
				return $xmlResp;
			}

			if($fMateria[3]=='127.0.0.1')
			{
				$cveJuzgado=(String)$cXML->juzgado;
				
				$consulta="SELECT id__17_tablaDinamica,nombreUnidad,claveUnidad FROM _17_tablaDinamica WHERE  claveOPC='".$cveJuzgado."'";
				$fJuzgado=$con->obtenerPrimeraFila($consulta);
				if(!$fJuzgado)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=3 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>2</noError>'.
							  '<mensajeError>La clave del juzgado NO existe</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				
				
				$cveTipoJuicio=(String)$cXML->tipoJuicio;
				
				$consulta="SELECT id__477_tablaDinamica FROM _477_tablaDinamica WHERE claveOPC='".$cveTipoJuicio."'";
				$fTipoJuicio=$con->obtenerPrimeraFila($consulta);
				
				if(!$fTipoJuicio)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=4 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>3</noError>'.
							  '<mensajeError>La clave del tipo de juicio NO existe</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				$nExpediente=str_pad(((String)$cXML->noExpediente),4,"0",STR_PAD_LEFT);
				
				$expediente=$nExpediente."/".((String)$cXML->anioExpediente);
				$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".
						$expediente."' AND unidadGestion='".$fJuzgado[2]."'";
				$fExpediente=$con->obtenerPrimeraFila($consulta);
				
				if($fExpediente)
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=5 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
						  '<respuesta>'.
							  '<resultado>0</resultado>'.
							  '<idFolioRegistro>-1</idFolioRegistro>'.
							  '<noExpediente>0000/0000</noExpediente>'.
							  '<noError>4</noError>'.
							  '<mensajeError>El expediente ya ha sido registrado previamente</mensajeError>'.
						  '</respuesta>';
					return $xmlResp;
				}
				
				$idActividad=generarIDActividad(478);
		
				$arrValores=array();
				$arrValores["tipoJuicio"]=$fTipoJuicio[0];
				$arrValores["fechaRecepcionOP"]=(String)$cXML->fechaRecepcion;
				$arrValores["idActividad"]=$idActividad;
				$arrValores["noExpediente"]=(String)$cXML->noExpediente;
				$arrValores["anioExpediente"]=(String)$cXML->anioExpediente;
				$arrValores["tipoExpediente"]=2;
				$arrValores["idExpedienteOP"]=(String)$cXML->idExpediente;
				$arrValores["materia"]=$fMateria[5];
				$secretaria=($arrValores["noExpediente"]%2)==1?"A":"B";
				$arrValores["secretariaAsignada"]=$secretaria;
				
				if($tipoMateria=="SC")
				{
					$arrValores["fechaRecepcion"]=date("Y-m-d",strtotime($arrValores["fechaRecepcionOP"]));
					$arrValores["horaRecepcion"]=date("H:i",strtotime($arrValores["fechaRecepcionOP"]));;
				}
				$arrDocumentosReferencia=NULL;
				$idRegistroSolicitud=crearInstanciaRegistroFormulario(478,-1,1,$arrValores,$arrDocumentosReferencia,-1,849);
				if($idRegistroSolicitud!=-1)
				{
					$IDDocumentoExpediente=generarIDDocumentoEscanner(478,$idRegistroSolicitud,4);
					$idDocumentoOPC=(isset($cXML->idDocumento)&&(((String)$cXML->idDocumento)!=""))?(String)$cXML->idDocumento:"-1";
					$consulta="UPDATE 908_archivos SET idDocumentoOPC=".$idDocumentoOPC." WHERE idArchivo=".$IDDocumentoExpediente;
					$con->ejecutarConsulta($consulta);
					
					foreach($cXML->partes[0] as $p)
					{
						if((string)$p->participacion!="")
						{
							$arrValoresParticipante=array();
			
							if(((String)$p->tipoPersona)=="M")
							{
								$arrValoresParticipante["tipoPersona"]=2;
								$arrValoresParticipante["apellidoPaterno"]="";
								$arrValoresParticipante["apellidoMaterno"]="";
								$arrValoresParticipante["nombre"]=((String)$p->razonSocial);
							}
							else
							{
								$arrValoresParticipante["tipoPersona"]=1;
								$arrValoresParticipante["apellidoPaterno"]=utf8_decode((String)$p->apPaterno);
								$arrValoresParticipante["apellidoMaterno"]=utf8_decode((String)$p->apMaterno);
								$arrValoresParticipante["nombre"]=utf8_decode((String)$p->nombre);
								
							}
							$arrValoresParticipante["idOPC"]=(isset($p->idPersona)&&(((String) $p->idPersona)!=""))?(String) $p->idPersona:"-1";
							$arrValoresParticipante["figuraJuridica"]=(String)$p->participacion;
							$arrValoresParticipante["idActividad"]=$idActividad;
							$idRegistroParticipante=crearInstanciaRegistroFormulario(47,$idRegistroSolicitud,0,$arrValoresParticipante,$arrDocumentosReferencia,-1,849);
			
			
							$consulta="INSERT INTO 7005_relacionFigurasJuridicasSolicitud(idActividad,idParticipante,idFiguraJuridica) 
									VALUES(".$idActividad.",".$idRegistroParticipante.",".$arrValoresParticipante["figuraJuridica"].")";
							$con->ejecutarConsulta($consulta);
						}
			
					}
			
					$idUGA=$fJuzgado[0];
					$cveUnidad=$fJuzgado[2];
					
					
					if($idUGA>-1)
					{
						
						$consulta="SELECT usuarioJuez FROM _26_tablaDinamica WHERE idReferencia=".$idUGA;
						$juez=$con->obtenerValor($consulta);
						if($juez=="")
							$juez=-1;
						$consulta="UPDATE _478_tablaDinamica SET codigoInstitucion='".$cveUnidad."',juez=".$juez." WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
						$con->ejecutarConsulta($consulta);
						guardarCarpetasAdministrativaRegistro(478,$idRegistroSolicitud);	
						
						$arrValores=array();
						$arrValores["tipoIngreso"]=((String)$cXML->datosExhorto[0]->tipoIngreso)=="C"?1:2;
						$arrValores["noOficio"]=(String)$cXML->datosExhorto[0]->noOficio;
						$arrValores["noExpediente"]=(String)$cXML->datosExhorto[0]->expediente;
						$arrValores["autoridadExhortante"]=(String)$cXML->datosExhorto[0]->autoridadExhortante;
						$arrValores["remitente"]=(String)$cXML->datosExhorto[0]->remitente;
						
						crearInstanciaRegistroFormulario(491,$idRegistroSolicitud,0,$arrValores,$arrDocumentosReferencia,-1,0);
						
						if(cambiarEtapaFormulario(478,$idRegistroSolicitud,1.2,"",-1,"NULL","NULL",857))
						{
							$consulta="SELECT secretario FROM _478_tablaDinamica WHERE id__478_tablaDinamica=".$idRegistroSolicitud;
							$secretariaAsignada=$con->obtenerValor($consulta);
							$idExpediente=(isset($cXML->idExpediente)&&(((String)$cXML->idExpediente)!=""))?(String)$cXML->idExpediente:"-1";
	
							$consulta="update 7006_carpetasAdministrativas set idExpedienteOPC=".$idExpediente.",secretariaAsignada='".$secretariaAsignada."' WHERE carpetaAdministrativa='".
										$expediente."' AND unidadGestion='".$fJuzgado[2]."'";
							$con->ejecutarConsulta($consulta);
							$consulta="UPDATE 8000_notificacionesOPC SET situacion=1,iRegistro=".$idRegistroSolicitud." WHERE idRegistro=".$idFolioSolicitud;
							$con->ejecutarConsulta($consulta);
							$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
											'<respuesta>'.
												'<resultado>1</resultado>'.
												'<idFolioRegistro>'.$idRegistroSolicitud.'</idFolioRegistro>'.
												'<noExpediente>'.$expediente.'</noExpediente>'.
												'<noError></noError>'.
												'<mensajeError></mensajeError>'.
											'</respuesta>';
							return $xmlResp;
			
						}
			
			
					}
					
					
					
		
				}
				else
				{
					$consulta="UPDATE 8000_notificacionesOPC SET situacion=6 WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
					$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
											'<respuesta>'.
												'<resultado>0</resultado>'.
												'<idFolioRegistro>-1</idFolioRegistro>'.
												'<noExpediente>0000/0000</noExpediente>'.
												 '<noError>5</noError>'.
												 '<mensajeError>Ha ocurrido un problema al registrar la carpeta</mensajeError>'.
											'</respuesta>';
					return $xmlResp;
				}
			}
			else
			{
				$idExpediente=(isset($cXML->idExpediente)&&(((String)$cXML->idExpediente)!=""))?(String)$cXML->idExpediente:"-1";
				$idDocumento=(isset($cXML->idDocumento)&&(((String)$cXML->idDocumento)!=""))?(String)$cXML->idDocumento:"-1";
				$consulta="INSERT INTO _478_expedientesVSArchivos(idDocumento,idCsDocs,idExpediente) VALUES(".$idDocumento.",".$fMateria[2].",".$idExpediente.")";
				
				$con->ejecutarConsulta($consulta);
				
				$urlWebServices="http://".$fMateria[3].":".$fMateria[4]."/webServices/wsJuzgados.php?wsdl";
	
				$client = new nusoap_client($urlWebServices,"wsdl"); 
				$parametros=array();
				$parametros["xml"]=$xml;
				$docXML = $client->call("registrarExhortoOficialiaPartes", $parametros);
				$cXML=simplexml_load_string($docXML);
				
				/*if((((String)$cXML->resultado)==1)||(((String)$cXML->resultado)==0))
				{
					$consulta="delete from 8000_notificacionesOPC  WHERE idRegistro=".$idFolioSolicitud;
					$con->ejecutarConsulta($consulta);
				}*/
				
				return $docXML;
				
			}
		
		}
		catch(Exception $e)
		{
			$consulta="UPDATE 8000_notificacionesOPC SET situacion=7 WHERE idRegistro=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);
			$xmlResp='<?xml version="1.0" encoding="ISO-8859-1"?>'.
					  '<respuesta>'.
						  '<resultado>0</resultado>'.
						  '<idFolioRegistro>-1</idFolioRegistro>'.
						  '<noExpediente>0000/0000</noExpediente>'.
						  '<noError>6</noError>'.
						  '<mensajeError>'.cv($e->getMessage()).'</mensajeError>'.
					  '</respuesta>';
			return $xmlResp;
				
			

		}
	}
	
	
	function registrarDiligenciaNotificacionDomiciliaria($cadNotificacion)
	{
		global $con;
		global $directorioInstalacion;
		$_SESSION["idUsr"]=1;
		$_SESSION["idRol"]="'-100_0'";
		$_SESSION["codigoUnidad"]="001";
		$_SESSION["codigoInstitucion"]="001";
		try
		{
			$registroConluido=false;
			$oNotificacion=json_decode($cadNotificacion);
			$idFolioSolicitud=$oNotificacion->idOrdenNotificacion;
			@registrarDatosPeticion(7029,$idFolioSolicitud,$cadNotificacion);
			$consulta="SELECT * FROM 7029_diligenciaActaNotificacion WHERE idAcuseEnvioCentralNotificadores=".$idFolioSolicitud;
			$fSolicitud=$con->obtenerPrimeraFilaAsoc($consulta);
			if(!$fSolicitud)
			{
				return '{"resultado":"0","mensaje":"El ID de Orden de Notificaci&oacute;n NO existe"}'	;
			}
			$notificado=0;
			if($oNotificacion->tipoNotificacion==1)
			{
				$resultadoDiligencia="";
				if($oNotificacion->resultado==1)
				{
					$resultadoDiligencia=1;
					$notificado=1;
					$registroConluido=true;
				}
				else
				{
					$resultadoDiligencia=2;
				}
				$cadResultado='{"fechaDiligencia":"'.$oNotificacion->fechaDiligencia.'","horaDiligencia":"'.$oNotificacion->horaDiligencia.
							'","resultado":"1","citatorio":"'.($resultadoDiligencia==1?0:1).
							'","tipoDomilio":"","tipoIdentificacion":"'.$oNotificacion->identificacionPresentada.'",
							"especifiqueTipoIdentificacion":"'.cv($oNotificacion->otraIdentificacion).
							'","resultadoDiligencia":"'.$resultadoDiligencia.'","fecha2daVisita":"'.$oNotificacion->fecha2daVisita.
							'","hora2daVisita":"'.$oNotificacion->hora2daVisita.'","personaCitatorio":"'.cv($oNotificacion->personaRecibeCitatorio).'",
							"relacionPersonaCitatorio":"'.cv($oNotificacion->relacionCargoPersonaRecibeCitatorio).
							'","lugarFijoCitatorio":"","personaRecibeNotificacion2daVisita":"","relacionPersona2daVisita":"","tipoIdentificacion2daVisita":"",
							"lblTipoIdentificacion2daVisita":"","lugarFijoCitatorio2daVisita":"","resultado2daVisita":""}';
				
			}
			else
			{
				switch($oNotificacion->resultado2daVisita)
				{
					case 1:
					case 2:
						$notificado=1;
						
					break;
					default:
						$notificado=0;
					break;
				}
				$cadResultado=bD($fSolicitud["objDiligencia"]);
				$cadResultado=setAtributoCadJson($cadResultado,"fecha2daVisita",$oNotificacion->fechaDiligencia);
				$cadResultado=setAtributoCadJson($cadResultado,"hora2daVisita",$oNotificacion->horaDiligencia);
				$cadResultado=setAtributoCadJson($cadResultado,"resultado2daVisita",$oNotificacion->resultado2daVisita);
				$cadResultado=setAtributoCadJson($cadResultado,"tipoIdentificacion2daVisita",$oNotificacion->identificacionPresentada);
				$cadResultado=setAtributoCadJson($cadResultado,"lblTipoIdentificacion2daVisita",$oNotificacion->otraIdentificacion);
				$cadResultado=setAtributoCadJson($cadResultado,"personaRecibeNotificacion2daVisita",$oNotificacion->personaRecibeNotificacion);
				$cadResultado=setAtributoCadJson($cadResultado,"relacionPersona2daVisita",$oNotificacion->relacionCargoPersonaRecibeNotificacion);
				$cadResultado=setAtributoCadJson($cadResultado,"lugarFijoCitatorio2daVisita",$oNotificacion->lugarDondeFijaNotificacion);
				$registroConluido=true;
				
			}
			
			
			foreach($oNotificacion->arrDocumentosProbatorios as $d)
			{
				$idDocumento=generarNombreArchivoTemporal();
				$directorioDestino=$directorioInstalacion.'\\archivosTemporales\\'.$idDocumento;
				$datos=bD($d->contenidoDocumento);
				$f=file_put_contents($directorioDestino,$datos);
				
				if($f)
				{
					$idDocumentoServidor=registrarDocumentoServidorRepositorio($idDocumento,$d->nombreDocumento);
					if(isset($d->descripcion))
					{
						$consulta="UPDATE 908_archivos SET descripcion='".cv($d->descripcion)."' WHERE idArchivo=".$idDocumentoServidor;
						
						$con->ejecutarConsulta($consulta);
					}
					
					$consulta="INSERT INTO 7030_documentosAdjuntosDiligencia(idDiligencia,idDocumento)
							values(".$fSolicitud["idRegistro"].",".$idDocumentoServidor.")";
					$con->ejecutarConsulta($consulta);
					
				}
			}
			
			
			$consulta="UPDATE 7029_diligenciaActaNotificacion SET notificado=".$notificado.",objDiligencia='".bE($cadResultado).
					"' WHERE idRegistro=".$fSolicitud["idRegistro"];
			if($con->ejecutarConsulta($consulta))
			{
				if($registroConluido)
				{
					$consulta="SELECT carpetaJudicial FROM 7042_ordenesNotificacion WHERE idOrden=".$fSolicitud["idOrden"];
					$carpetaAdministrativa=$con->obtenerValor($consulta);
					$consulta="SELECT idUsuarioDestinatario FROM 9060_tableroControl_4 WHERE iFormulario=-7042 AND iRegistro=".$fSolicitud["idOrden"];
					$res=$con->obtenerFilas($consulta);
					while($fila=mysql_fetch_row($res))
					{
						registrarRespuestaNotificacionOrdenNotificacionNotificador(4,$fila[0],"32_0",$carpetaAdministrativa,$fSolicitud["idOrden"],$fSolicitud["idRegistro"]);
					}
				}
				return '{"resultado":"1","mensaje":""}'	;
			}
		}
		catch(Exception $e)
		{
			return '{"resultado":"0","mensaje":"'.cv($e->getMessage()).'"}'	;
		}
		
	}

	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('registrarAsunto',array('xml'=>'xsd:string','tipoMateria'=>'xsd:string','iFormulario'=>'xsd:string','iRegistro'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	$server->register('registrarAsuntoOficialiaPartes',array('xml'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	$server->register('registrarExhortoOficialiaPartes',array('xml'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	$server->register('registrarDiligenciaNotificacionDomiciliaria',array('cadNotificacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>