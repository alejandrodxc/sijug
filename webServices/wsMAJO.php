<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	
	include_once("latis/funcionesNeotrai.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');
	
	
	function notificarVideoEventoAudiencia($datosEvento)
	{
		global $con;
		$resultado="";
		try
		{
	
			
			$consulta="INSERT INTO 3009_bitacoraNotificacionesVideoGrabacion(cuerpoXML) VALUES('".bE($datosEvento)."')";
			$cXML=simplexml_load_string($datosEvento);	
			$con->ejecutarConsulta($consulta);			
			/*
			<datosEventosAudiencia>
				<idEvento></idEvento>
				<fechaInicio></fechaInicio>      
				<horaInicio></horaInicio>        
				<fechaFin></fechaFin>      
				<horaFin></horaFin>
				<idSala></idSala>
				<idSituacion></idSituacion>
				<urlVideo></urlVideo>      
				<duracionAudiencia></duracionAudiencia>
			</datosEventosAudiencia>
			*/
			
			$idEventoAudiencia=(string)$cXML->idEvento[0];
			$idSituacion=(string)$cXML->idSituacion[0];
			
			
			$consulta="SELECT direccionIP,puerto FROM 000_instanciasSistema WHERE ".$idEventoAudiencia.">=idEventoInicial AND ".$idEventoAudiencia."<=idEventoFinal";
			$fEvento=$con->obtenerPrimeraFila($consulta);
			
			if($fEvento)
			{
				$client = new nusoap_client("http://".$fEvento[0].":".($fEvento[1]==""?"80":$fEvento[1])."/webServices/wsMAJO.php?wsdl","wsdl"); 
				$parametros=array();
				$parametros["datosEvento"]=$datosEvento;
				$response = $client->call("notificarVideoEventoAudiencia", $parametros);
				return $response;
			}
			$consulta="select count(*) from 7000_eventosAudiencia WHERE idRegistroEvento=".$idEventoAudiencia;
			
			$nRegistros=$con->obtenerValor($consulta);
		
		
			if($nRegistros==0)
			{
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<datosResultado><resultado>0</resultado><datosComplementarios>El ID de evento reportado NO existe</datosComplementarios></datosResultado>';
			
				return;	
			}
			$horaInicioReal=(string)$cXML->fechaInicio[0]." ".(string)$cXML->horaInicio[0];
			if($horaInicioReal=="")
				$horaInicioReal="NULL";
			else
				$horaInicioReal="'".$horaInicioReal."'";
				
			$horaTerminoReal=(string)$cXML->fechaFin[0]." ".(string)$cXML->horaFin[0];
			if($horaTerminoReal=="")
				$horaTerminoReal="NULL";
			else
				$horaTerminoReal="'".$horaTerminoReal."'";
				
			$urlMultimedia=(string)$cXML->urlVideo[0];
			$duracionAudiencia=(string)$cXML->duracionAudiencia[0];
			if($duracionAudiencia=="")
				$duracionAudiencia="NULL";
				
			$idSalaReal=(string)$cXML->idSala[0];
			if($idSalaReal=="")
				$idSalaReal="NULL";
			
			$updateComplementario="";
			$consulta="SELECT horaInicioReal FROM 7000_eventosAudiencia WHERE idRegistroEvento=".$idEventoAudiencia;
			$fInicio=$con->obtenerValor($consulta);
			if($fInicio=="")
			{
				$updateComplementario=", horaInicioReal=".$horaInicioReal.",horaTerminoReal=".$horaTerminoReal.",duracionAudiencia=".$duracionAudiencia;
			}
			
			$consulta="UPDATE 7000_eventosAudiencia SET 
					horaInicioRealMAJO=".$horaInicioReal.",horaTerminoRealMAJO=".$horaTerminoReal.",urlMultimedia='".$urlMultimedia.
					"',fechaNotificacionMultimedia='".date("Y-m-d H:i:s")."',situacion=".$idSituacion.",
					duracionAudienciaMAJO=".$duracionAudiencia.",idSalaReal=".$idSalaReal." ".$updateComplementario.
					" WHERE idRegistroEvento=".$idEventoAudiencia;
			
	
			if($con->ejecutarConsulta($consulta))
			{
				@registrarTerminacionRecursosEventos($idEventoAudiencia,$horaInicioReal,$horaTerminoReal);
				if($urlMultimedia!="")
					@reportarURLAudiencia($idEventoAudiencia);
				//@reportarURLAudiencia($idEventoAudiencia);
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><datosResultado><resultado>1</resultado><datosComplementarios></datosComplementarios></datosResultado>';
			}
			
			
		}
		catch(Exception $e)
		{
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><datosResultado><resultado>0</resultado><datosComplementarios>'.$e->getMessage().'</datosComplementarios></datosResultado>';
			
		}
		return ($resultado);
	}

	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('notificarVideoEventoAudiencia',array('datosEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Notificacion de video de audiencia');

	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);