<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/latisErrorHandler.php");
	include_once("latis/funcionesNeotrai.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');
	
	function registrarSolicitudInicialPGJ($cadXML,$sistema=1)
	{
		global $con;
		global $directorioInstalacion;	
		global $servidorPruebas;
		$turnarAumatico=true;
		$fechaActual=strtotime(date("Y-m-d H:i:s"));
		$fechaCreacion=$fechaActual;
		$txtFechaActual=date("Y-m-d H:i:s",$fechaActual);
		$idFolioSolicitud="";
		try
		{
			$_SESSION["idUsr"]=3789;
			$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
			$resRoles=$con->obtenerFilas($consulta);
			$listaGrupo="";
			while($fRoles=mysql_fetch_row($resRoles))
			{
				$arrRol=explode("_",$fRoles[0]);
				$rol="'".$fRoles[0]."'";
				if($arrRol[1]!="0")
					$rol.=",'".$arrRol[0]."_-1'";
				
				if($listaGrupo=="")
					$listaGrupo=$rol;
				else
					$listaGrupo.=",".$rol;
			}
			if($listaGrupo=="")
				$listaGrupo='-1';
			$_SESSION["idRol"]=$listaGrupo.",'-100_0'";
			$_SESSION["codigoUnidad"]="001";
			$_SESSION["codigoInstitucion"]="001";			
			
			$docXML=($cadXML);
			
			$cXML=simplexml_load_string($docXML);
			
			$consulta="INSERT INTO 3011_solicitudRecibidasPGJ(fechaSolicitud,carpetaInvestigacion,idFormulario,situacion,sistema) VALUES('".date("Y-m-d H:i:s").
					"','".((string)$cXML->DatosSolicitud[0]->carpetainvestigacion)."',46,0,".$sistema.")";
			$con->ejecutarConsulta($consulta);
			$idFolioSolicitud=$con->obtenerUltimoID();
			
			@registrarDatosPeticion(3011,$idFolioSolicitud,$cadXML);
			
			
			
			
			$consulta="SELECT COUNT(*) FROM _46_tablaDinamica WHERE ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrlsolicitud."' 
					AND idSolicitud= ".((string)$cXML->DatosSolicitud[0]->idsolicitud)." and sistema=".$sistema;
			
			$nRegistro=$con->obtenerValor($consulta);

			if($nRegistro>0)
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=2 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La solicitud ya ha sido registrada anteriormente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			
			
			
			$consulta="SELECT COUNT(*) FROM 3011_solicitudRecibidasPGJ WHERE ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrlsolicitud."' 
					AND idSolicitud= '".((string)$cXML->DatosSolicitud[0]->idsolicitud)."' and idRegistro is not null";
			
			$nRegistro=$con->obtenerValor($consulta);

			if($nRegistro>0)
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=-2 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>La solicitud ya ha sido registrada anteriormente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			
			
			$fDatosCarpetaBase[0]="";
			
			$llaveCarpeta=generarLlaveCarpetaInvestigacion((string)$cXML->DatosSolicitud[0]->carpetainvestigacion);
			
			$esMateriaAdolescentes=((string)$cXML->DatosSolicitud[0]->adolescente)==1;
			
			$cAdministrativa="";
			
			$consulta="SELECT tipoAudiencia FROM _285_tablaDinamica WHERE cveTipoSolicitud='".(string)$cXML->DatosSolicitud[0]->cvesolicitud."'";
			$tipoAudiencia=$con->obtenerValor($consulta);
			switch($tipoAudiencia)
			{
				case 9:
				case 56:
				case 32:
				case 97:
				case 69:
				case 35:
					if($llaveCarpeta!="")
					{
						$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE llaveCarpetaInvestigacion='".
							cv($llaveCarpeta)."' and tipoCarpetaAdministrativa=1 and unidadGestion ='012' order by fechaCreacion DESC";
  
						$cAdministrativa=$con->obtenerValor($consulta);
						if($cAdministrativa=="")
						{
							$llaveCarpeta="";		
						}
					}
					
				break;
			}
						
			if(($llaveCarpeta!="")&&($cAdministrativa==""))
			{
				if(!$esMateriaAdolescentes)
				{
					
					$consulta="SELECT c.carpetaAdministrativa FROM 7006_carpetasAdministrativas c,_46_tablaDinamica s WHERE 
								s.carpetaAdministrativa=c.carpetaAdministrativa and llaveCarpetaInvestigacion='".
								cv($llaveCarpeta)."' and s.tipoAudiencia in (1,102,114,136,26,91,52) and tipoCarpetaAdministrativa=1 
								and unidadGestion not in ('012','301','302') order by c.fechaCreacion DESC";
				}
				else
				{
					$consulta="SELECT c.carpetaAdministrativa FROM 7006_carpetasAdministrativas c,_46_tablaDinamica s WHERE 
								s.carpetaAdministrativa=c.carpetaAdministrativa and llaveCarpetaInvestigacion='".
								cv($llaveCarpeta)."' and s.tipoAudiencia in (1,102,114,136,26,91,52) and tipoCarpetaAdministrativa=1 
								and unidadGestion in ('301') order by c.fechaCreacion DESC";
				}
				$cAdministrativa=$con->obtenerValor($consulta);
				
			}

			if($cAdministrativa!="")
			{
				$fDatosCarpetaBase[0]=$cAdministrativa;
				$consulta="select delitoGrave,tipoAudiencia FROM _46_tablaDinamica WHERE carpetaAdministrativa='".$cAdministrativa.
						"' and idEstado>=1.4 order by fechaCreacion DESC"; 
				$fDatosSolicitudInicial=$con->obtenerPrimeraFila($consulta);
				$fDatosCarpetaBase[1]=$fDatosSolicitudInicial[0];
			}
						
			$arrSolicitudesPermitidas=array();
			$arrSolicitudesPermitidas[12]=1;
			$arrSolicitudesPermitidas[13]=1;
			
			$arrSolicitudesPermitidas[31]=1;
			$arrSolicitudesPermitidas[42]=1;
			/*$arrSolicitudesPermitidas[51]=1;
			$arrSolicitudesPermitidas[52]=1;*/
			
			/*$arrSolicitudesPermitidas[14]=1;
			$arrSolicitudesPermitidas[60]=1;
			$arrSolicitudesPermitidas[61]=1;*/
			/*$arrSolicitudesPermitidas[63]=1;*/
			
			$cveSolicitud=(string)$cXML->DatosSolicitud[0]->cvesolicitud;
			$cvefiscalia=(string)$cXML->DatosSolicitud[0]->cvefiscalia;
			$cveagencia=(string)$cXML->DatosSolicitud[0]->cveagencia;
			//$esMateriaAdolescentes=(($cvefiscalia==45)&&($cveagencia==5));
			
			if(($cAdministrativa=="")&&(!isset($arrSolicitudesPermitidas[$cveSolicitud]))&& !$esMateriaAdolescentes)
			{
				$declaratoria=(string)$cXML->DatosSolicitud[0]->declaratoria;
				if(!$esMateriaAdolescentes)
				{
					
					if(($declaratoria<1)||($declaratoria>4))
					{
						$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=3 WHERE idRegistroSolicitud=".$idFolioSolicitud;
						$con->ejecutarConsulta($consulta);
						$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
									<idTipoAcuse>1</idTipoAcuse>
									<idAcuse>0</idAcuse>
									<idCtrProcedimiento>0</idCtrProcedimiento>
									<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
									<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
									<resultado>0</resultado>
									<carpetaAdministrativa></carpetaAdministrativa>
									<comentarios>El valor del nodo declaratoria NO es valido</comentarios>
									<documentoAdjunto></documentoAdjunto>
								</acuse>';
						return $resultado;
					}
				}
			}
			
			$arrDocumentosReferencia=array();
			$idActividad=generarIDActividad(46,-1);
			
			foreach($cXML->Delitos[0] as $delito)
			{

				$arrDocumentosReferenciaDelito=array();
				$arrValoresDelito=array();
				$arrValoresDelito["tituloDelito"]=-1;
				$arrValoresDelito["capituloDelito"]=-1;
				$arrValoresDelito["denominacionDelito"]=(string)$delito->cvedelito;
				$arrValoresDelito["calificativo"]=(string)$delito->formacomision;
				$arrValoresDelito["gradoRealizacion"]=(string)$delito->gradorealizacion;
				$arrValoresDelito["idActividad"]=$idActividad;
				$arrValoresDelito["modalidadDelito"]=(string)$delito->cvemodalidad;
				
				
				
				$idRegistroDelito=crearInstanciaRegistroFormulario(61,-1,1,$arrValoresDelito,$arrDocumentosReferenciaDelito,-1,264);
				$x=0;
				$query[$x]="begin";
				$x++;
				
				//Nuevo
				$cveDelito=(string)$delito->cvedelito;
				$descDelito=(string)$delito->descdelito;
				$cveModalidad=(string)$delito->cvemodalidad;
				$modalidad=(string)$delito->descmodalidad;
				
				$consulta="SELECT id__35_denominacionDelito FROM _35_denominacionDelito WHERE claveDenominacionDelito='".$cveDelito."'";
				$iDelito=$con->obtenerValor($consulta);
				if($iDelito=="")
				{
					$query[$x]="INSERT INTO _35_denominacionDelito(id__35_denominacionDelito,idReferencia,claveDenominacionDelito,denominacionDelito)
								VALUES(".$cveDelito.",1,'".$cveDelito."','".cv($descDelito)."')";
					$x++;
					
					
					$iDelito=$cveDelito;
					$query[$x]="INSERT INTO _62_tablaDinamica(id__62_tablaDinamica,idReferencia,codigo,delito) VALUES(".$iDelito.
								",1,'".$iDelito."','".$iDelito."')";	
					$x++;	
				}
				
				$consulta="SELECT id__62_clasificacion FROM _62_clasificacion WHERE idReferencia=".$iDelito." AND clave='".$cveModalidad."'";
				$iModalidad=$con->obtenerValor($consulta);
				if($iModalidad=="")
				{
					$query[$x]="INSERT INTO _62_clasificacion(idReferencia,clave,nombreModalidad) 
								VALUES(".$iDelito.",'".$cveModalidad."','".cv($modalidad)."')";
					$x++;
					
				}
				
				
				//.--
				$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$idActividad." AND idFiguraJuridica=4";
				$resParticipanteDelito=$con->obtenerFilas($consulta);
				while($fParticipante=mysql_fetch_row($resParticipanteDelito))
				{
					$query[$x]="INSERT INTO _61_chkDelitosImputado(idPadre,idOpcion) VALUES(".$idRegistroDelito.",".$fParticipante[0].")";
					$x++;
				}
				
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
				
			}
			
			$arrValores=array();
			$idRegistroSolicitud=-1;
			
			$datosDocumento=array();			
			$datosDocumento["nombreDocumento"]=str_replace(".","",(string)$cXML->documentoAnexo[0]->nombreDocumento);
			$datosDocumento["descripcionDocumento"]=(string)$cXML->documentoAnexo[0]->descripcionDocumento;
			$datosDocumento["contenido"]=(string)$cXML->documentoAnexo[0]->contenido;
			
			$idDocumentoServidor=-1;
			if($datosDocumento["contenido"]!="")
			{
				$idDocumento=generarNombreArchivoTemporal();
				$directorioDestino=$directorioInstalacion.'\\archivosTemporales\\'.$idDocumento;
				$datos=bD($datosDocumento["contenido"]);
				$f=file_put_contents($directorioDestino,$datos);
				
				if($f)
				{
					$idDocumentoServidor=registrarDocumentoServidor($idDocumento,$datosDocumento["nombreDocumento"]);
					$consulta="UPDATE 908_archivos SET descripcion='".cv($datosDocumento["descripcionDocumento"])."' WHERE idArchivo=".$idDocumentoServidor;
					
					$con->ejecutarConsulta($consulta);
				}
			}		
			
			$arrValores["folioCarpetaInvestigacion"]=(string)$cXML->DatosSolicitud[0]->carpetainvestigacion;
			$arrValores["tipoProgramacionAudiencia"]=1;
			$tipoAudiencia=26;
			$idEtapa=2.7;			
			
			$consulta="SELECT tipoAudiencia,tipoSolicitud,naturalezaSolicitud FROM _285_tablaDinamica WHERE cveTipoSolicitud='".(string)$cXML->DatosSolicitud[0]->cvesolicitud."'";
			$fTipoAudiencia=$con->obtenerPrimeraFila($consulta);
			if(!$fTipoAudiencia)
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=5 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<comentarios>El valor de la clave de solicitud NO es valida</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}	
			
			$tAtencion=$fTipoAudiencia[2];
			$tipoAudiencia=$fTipoAudiencia[0];
			if($tipoAudiencia<>-1)
			{
				$consulta="SELECT tipoAtencion FROM _4_tablaDinamica WHERE id__4_tablaDinamica=".$tipoAudiencia;
				$tAtencion=$con->obtenerValor($consulta);			
			}
			/*
			$unidadGestion=asignarCarpetaGuardiaV3($arrValores["carpetaAdministrativa"],$arrValores["fechaCreacion"]);
			if($unidadGestion!=-1)
			{
				@asignarCarpetaUnidadGestionGuardia($arrValores["carpetaAdministrativa"],$unidadGestion);
				$consulta="UPDATE _96_tablaDinamica SET codigoInstitucion='".$unidadGestion."' WHERE id__96_tablaDinamica=".$idRegistroSolicitud;
				$con->ejecutarConsulta($consulta);
			}
			*/
			
			
			if($tAtencion==1)
			{				
				$idEtapa=1.4;
				if(esHorarioNormalDiaHabil($txtFechaActual,$esMateriaAdolescentes?2:1))
				{
					$idEtapa=2.7;
				}
				else
				{
					$fechaActual=strtotime(obtenerFechaRecepcionHorarioNormal($txtFechaActual,$esMateriaAdolescentes?2:1));
				}
			}
			
			
			if(!$turnarAumatico)
			{
				$idEtapa=1.4;
			}
			
			if(	
				(strpos($arrValores["folioCarpetaInvestigacion"],"724/09-2018")!==false)||
				($cAdministrativa=="012/1218/2019-AI")||
				(strpos($arrValores["folioCarpetaInvestigacion"],"794/10-2019")!==false)||
				(strpos($arrValores["folioCarpetaInvestigacion"],"539/07-2018")!==false)||
				(strpos($arrValores["folioCarpetaInvestigacion"],"2730/12-2020")!==false)
				)
			{
				sendMensajeWhatApp("2288248558","Llego carpeta de investigación ".$arrValores["folioCarpetaInvestigacion"]);
				sendMensajeWhatApp("5548116399","Llego carpeta de investigación ".$arrValores["folioCarpetaInvestigacion"]);
				$idEtapa=1.8;
			}
			
			if($arrValores["folioCarpetaInvestigacion"]=="CI-FIDCSP/B/UI-3 C/D/2730/12-2020")
			{
				$idEtapa=1.4;
			}
			
			$fechaFenece=(string)$cXML->DatosSolicitud[0]->fechaFenece;
			$arrValores["fechaFenece"]=($fechaFenece=="")?"NULL":$fechaFenece;
			$arrValores["declaratoria"]=(string)$cXML->DatosSolicitud[0]->declaratoria;
			
			if(isset($arrSolicitudesPermitidas[$cveSolicitud]))
			{
				$arrValores["declaratoria"]=1;
			}
			$arrValores["tipoAudiencia"]=$tipoAudiencia;
			$arrValores["fechaCreacion"]=date("Y-m-d H:i:s",$fechaActual);
			$arrValores["delitoGrave"]=($arrValores["declaratoria"]<3)?0:1;
			$tipificacion=$arrValores["delitoGrave"];
			
			///
			/*$nOtrosDelitos=0;
			$nDelitosNarco=0;
			$nDelitosNarcoComercio=0;
			
			$consulta="SELECT COUNT(*) FROM _61_tablaDinamica WHERE  idActividad=".$idActividad." AND denominacionDelito=10241 AND 
						modalidadDelito IN(21752,20718)";
			$nDelitosNarco=$con->obtenerValor($consulta);
			
			$consulta="SELECT COUNT(*) FROM _61_tablaDinamica WHERE  idActividad=".$idActividad." AND denominacionDelito=10241 AND 
						modalidadDelito not IN(21752,20718)";
			$nDelitosNarcoComercio=$con->obtenerValor($consulta);	
			
			$consulta="SELECT COUNT(*) FROM _61_tablaDinamica WHERE  idActividad=".$idActividad." AND denominacionDelito<>10241";
			$nOtrosDelitos=$con->obtenerValor($consulta);
			
			
			if(($tipoAudiencia!=91)&&($tipoAudiencia!=102)&&($tipoAudiencia!=114))
			{
				if($tipificacion==1)
				{
					if($nDelitosNarcoComercio>0)
					{
						$tipificacion=1;
						$arrValores["delitoGrave"]=1;
					}
					else
					{
						if(($nDelitosNarco>0)&&($nOtrosDelitos==0))
						{
							$tipificacion=0;
							$arrValores["delitoGrave"]=0;
						}
						
						
						
					}
				}
			}
		
			if($esMateriaAdolescentes)
			{
				$tipificacion=4;
			}
		
		
			$nMujeres=0;
			$nHombres=0;
		
			//$consulta="SELECT COUNT(genero) FROM _47_tablaDinamica i,7005_relacionFigurasJuridicasSolicitud r 
//								WHERE i.id__47_tablaDinamica=r.idParticipante AND r.idFiguraJuridica IN(4) AND 
//								r.idActividad=".$idActividad." AND genero=1";
//		
//			$nMujeres=$con->obtenerValor($consulta);
//			$consulta="SELECT COUNT(genero) FROM _47_tablaDinamica i,7005_relacionFigurasJuridicasSolicitud r 
//								WHERE i.id__47_tablaDinamica=r.idParticipante AND r.idFiguraJuridica IN(4) AND 
//								r.idActividad=".$idActividad." AND genero=0";
//			$nHombres=$con->obtenerValor($consulta);
			
			
			
			////
			$nivel="";
			$tipoHorario=0;
	
			if($tipificacion==0)
			{
				$nivel="A";		
				$tipoHorario=determinarTipoHorarioGeneral($fechaActual);		
		
			}
			else
			{
				if($tipificacion==1)
				{
				
					$nivel="B";
					$tipoHorario=determinarTipoHorarioGeneral($fechaActual);		
					
				}
				else
				{
					if($tipificacion==4)
					{
						$nivel="D";
						$tipoHorario=1;
					}
					else
					{
						$nivel="X";
						$tipoHorario=1;
					}
				}
			}
		
			
				
			if($tipoHorario<>2)
			{
				if(($nivel=="A")||($nivel=="B"))
				{
					if(($nMujeres>0)&&($nHombres==0))
					{
						if((($tipoAudiencia!=1)&&($tipoAudiencia!=52))||($nivel=="B"))
						{
							$nivel="M";
						}
						
					}
				}
			}
			*/	
				
			
			////
			
			$arrValores["fechaRecepcionSistema"]=date("Y-m-d H:i:s",$fechaCreacion);
			
			
			$arrValores["idActividad"]=$idActividad;
			
			$arrValores["ctrlSolicitud"]=(string)$cXML->DatosSolicitud[0]->ctrlsolicitud;
			$arrValores["idSolicitud"]=(string)$cXML->DatosSolicitud[0]->idsolicitud;
			$arrValores["cveSolicitud"]=(string)$cXML->DatosSolicitud[0]->cvesolicitud;
			$arrValores["solicitudXML"]='';			
			$arrValores["ctrluinv"]=(string)$cXML->DatosSolicitud[0]->ctrluinv;
			$arrValores["materiaDestino"]=1;
//			$arrValores["unidadDestino"]=$nivel;
			$cvefiscalia=(string)$cXML->DatosSolicitud[0]->cvefiscalia;
			$cveagencia=(string)$cXML->DatosSolicitud[0]->cveagencia;
			
			if($esMateriaAdolescentes)
			{
				$arrValores["materiaDestino"]=2;
				$arrValores["declaratoria"]=1;
			}
			
			$arrValores["fechaRecepcion"]=date("Y-m-d",$fechaActual);
			$arrValores["horaRecepcion"]=date("H:i:s",$fechaActual);
			if($cAdministrativa!="")
			{
				
				$arrValores["carpetaAdministrativa"]=$cAdministrativa;
				$arrValores["delitoGrave"]=$fDatosCarpetaBase[1];
			}
			$arrValores["sistema"]=$sistema;
			//
			
			$idRegistroSolicitud=crearInstanciaRegistroFormulario(46,-1,1.4,$arrValores,$arrDocumentosReferencia,-1,264);
			@registrarDatosPeticionRefiere(46,$idRegistroSolicitud,3011,$idFolioSolicitud);

			

			if($idDocumentoServidor!=-1)
			{
				convertirDocumentoUsuarioDocumentoResultadoProceso($idDocumentoServidor,46,$idRegistroSolicitud,($datosDocumento["nombreDocumento"]).".pdf",14);
			}
			
			if($cAdministrativa!="")
			{
				
				$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".$cAdministrativa."'";
				$unidadGestion=$con->obtenerValor($consulta);
				
				$consulta="SELECT id__17_tablaDinamica,idReferencia FROM _17_tablaDinamica WHERE claveUnidad='".$unidadGestion."'";
				$fRegistroUnidad=$con->obtenerPrimeraFila($consulta);
				
				$consulta="INSERT INTO 7000_eventosAudiencia(situacion,fechaAsignacion,idEdificio,idCentroGestion,
						idFormulario,idRegistroSolicitud,idReferencia,idEtapaProcesal,tipoAudiencia)
						values(0,'".date("Y-m-d H:i:s")."',".$fRegistroUnidad[1].",".$fRegistroUnidad[0].",46,".
						$idRegistroSolicitud.",-1,1,".$tipoAudiencia.")";
				$con->ejecutarConsulta($consulta);
			}
			
			$arrDocumentosReferencia=array();
			$arrValores=array();
			
			$arrValores["claveFiscalia"]=(string)$cXML->DatosSolicitud[0]->cvefiscalia;
			if($sistema==1)
			{
				if($arrValores["claveFiscalia"]==62)
					$arrValores["claveFiscalia"]=33;
				
				if($arrValores["claveFiscalia"]==44)
					$arrValores["claveFiscalia"]=20;
			}
			
			
			$consulta="SELECT id__286_tablaDinamica FROM _286_tablaDinamica WHERE cveFiscalia='".$arrValores["claveFiscalia"]."' AND sistema=".$sistema;
			$arrValores["claveFiscalia"]=$con->obtenerValor($consulta);
			$arrValores["claveAgencia"]=(string)$cXML->DatosSolicitud[0]->cveagencia;
			$arrValores["claveUnidad"]=(string)$cXML->DatosSolicitud[0]->cveunidad;
			$arrValores["nombre"]=utf8_decode((string)$cXML->DatosSolicitud[0]->mpsolicitante);
			$arrValores["claveCoorTerMP"]=(string)$cXML->DatosSolicitud[0]->cvecoorterrMP;
			$arrValores["sistema"]=$sistema;
			$idRegistroMP=crearInstanciaRegistroFormulario(100,$idRegistroSolicitud,1,$arrValores,$arrDocumentosReferencia,-1,264);
	
			$query=array();
			$x=0;
			$query[$x]="begin";
			$x++;
			$correo=(string)$cXML->DatosSolicitud[0]->correoMP;
			if($correo!="")
			{
				$query[$x]="INSERT INTO _100_gridCorreosFiscal(idReferencia,correoElectronico) VALUES(".$idRegistroMP.",'".$correo."')";
				$x++;
			}
			
			$query[$x]="commit";
			$x++;
			
			$con->ejecutarBloque($query);	
			
			
			
			
			
			foreach($cXML->Personas[0] as $p)
			{
				$arrDocumentosReferencia=array();
				$iFigura=(string)$p->figurajuridica;
				$consulta="SELECT figuraEquivalente FROM _284_tablaDinamica WHERE id__284_tablaDinamica=".$iFigura;
				$figuraJuridica=$con->obtenerValor($consulta);
				if($figuraJuridica=="")
					$figuraJuridica=9;
				$arrValores=array();
				$idRegistroParticipante=-1;				
				$arrValores["tipoPersona"]=1;
				$arrValores["apellidoPaterno"]=utf8_decode((string)$p->paterno);
				$arrValores["apellidoMaterno"]=utf8_decode((string)$p->materno);
				$sexo=trim((string)$p->cvesexo);
				if($sexo=="")
					$sexo=2;
				else
					$sexo=$sexo-1;
				$arrValores["genero"]=$sexo;//Revisar
				
				$edad=trim((string)$p->edad);
	
				$arrValores["edad"]=($edad=="")?"NULL":$edad;
				$arrValores["curp"]=trim((string)$p->curp);
				
				$fechaNacimiento=trim((string)$p->fechanacimiento);
				$arrValores["fechaNacimiento"]=($fechaNacimiento=="")?"NULL":$fechaNacimiento;
				$arrValores["estadoCivil"]=(string)$p->cveestadocivil;
				$arrValores["tipoIdentificacion"]=(string)$p->cveidentificacion;
				$arrValores["folioIdentificacion"]=(string)$p->folioidentificacion;
				
				$arrValores["nombre"]=utf8_decode((string)$p->nombre);
				$arrValores["esMexicano"]=((string)$p->nacionalidadmexicana=="2")?"3":(string)$p->nacionalidadmexicana;
				$cvenacionalidad=trim((string)$p->cvenacionalidad);
				
				if($cvenacionalidad==1)
				{
					$cvenacionalidad="NULL";
					$arrValores["esMexicano"]=1;
				}
				$arrValores["nacionalidad"]=($cvenacionalidad=="")?"NULL":$cvenacionalidad;
				
				$arrValores["idActividad"]=$idActividad;
				$arrValores["figuraJuridica"]=$figuraJuridica;
				$tipoDefensor=0;
				if($figuraJuridica==5)
				{
					if($iFigura==52)
						$tipoDefensor=1;
					else
						$tipoDefensor=2;
				}
				$arrValores["tipoDefensor"]=$tipoDefensor;
				$arrValores["tipoFiguraPGJ"]=$iFigura;
	
				$idRegistroParticipante=crearInstanciaRegistroFormulario(47,-1,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
				
				$consulta="INSERT INTO 7005_relacionFigurasJuridicasSolicitud(idActividad,idParticipante,idFiguraJuridica) 
							VALUES(".$idActividad.",".$idRegistroParticipante.",".$figuraJuridica.")";
				$con->ejecutarConsulta($consulta);
				$query=array();
				$x=0;
				$query[$x]="begin";
				$x++;
				if(sizeof($p->nombrealterno)>0)
				{
					foreach($p->nombrealterno[0] as $a)
					{
						$query[$x]="INSERT INTO _47_gridAlias(idReferencia,alias) VALUES(".$idRegistroParticipante.",'".cv(utf8_decode((string)$a))."')";
						$x++;
						
					}
				}
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
				
				foreach($p->Direcciones[0] as $d)
				{
					$arrDocumentosReferencia=array();
					
					$arrValores=array();
								
					$arrValores["calle"]=utf8_decode((string)$d->calle);
					$arrValores["noExt"]=(string)$d->numeroext;
					$arrValores["noInterior"]=(string)$d->numeroint;
					$arrValores["colonia"]=utf8_decode((string)$d->colonia);
					$arrValores["entreCalle"]=utf8_decode((string)$d->entrecalle);
					$arrValores["yCalle"]=utf8_decode((string)$d->ycalle);
					$arrValores["otrasReferencias"]=utf8_decode((string)$d->referencia);
					$arrValores["entidadFederativa"]=(string)$d->cveentidadfederativa;
					if($arrValores["entidadFederativa"]=="---")
						$arrValores["entidadFederativa"]="IND";
						
					$consulta="SELECT cveEstado FROM 820_estados WHERE cvePGJ='".$arrValores["entidadFederativa"]."'";	
					$arrValores["entidadFederativa"]=$con->obtenerValor($consulta);	
					
					$arrValores["municipio"]=(string)$d->cvemunicipio;
					$arrValores["localidad"]="NULL";
					$codigoPostal="NULL";
					
					if(trim((string)$d->codigopostal)!="")
					{
						$codigoPostal=trim((string)$d->codigopostal);
					}
					$arrValores["codigoPostal"]=$codigoPostal;
	
	
					$idRegistroDomicilio=crearInstanciaRegistroFormulario(48,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
					
					$query=array();
					$x=0;
					$query[$x]="begin";
					$x++;
					$correo=(string)$d->correoelectronico;
					if($correo!="")
					{
						$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
						$x++;
					}
					
					$telefono=(string)$d->telefono;
					if($telefono!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
						$x++;
					}
					$celular=(string)$d->celular;
					if($celular!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
						$x++;
					}
					$query[$x]="commit";
					$x++;
					
					$con->ejecutarBloque($query);
					
				}
				
				
				$arrValores=array();
				$arrValores["nivelEscolaridad"]=(string)$p->cveescolaridad;
				if($arrValores["nivelEscolaridad"]==0)
					$arrValores["nivelEscolaridad"]=101;
				/*else
				{
					$consulta="SELECT claveNivelEscolaridad FROM _30_tablaDinamica WHERE id__30_tablaDinamica=".$arrValores["nivelEscolaridad"];
					$arrValores["nivelEscolaridad"]=$con->obtenerValor($consulta);
				}*/
				$tipoOcupacion="NULL";
				if(trim((string)$p->ocupacion)!="")
					$tipoOcupacion=(string)$p->ocupacion;
				$arrValores["tipoOcupacion"]=99;
				$arrValores["otraOcupacion"]=$tipoOcupacion;
				
				$arrValores["religion"]=(string)$p->cvereligion;
				
				$arrValores["grupoEtnico"]=(string)$p->cvegrupoetnico;
				
				$perteneceGrupoEtnico=0;
				switch($arrValores["grupoEtnico"])
				{
					case 888:
						$perteneceGrupoEtnico=0;
					break;
					
					default:
						$perteneceGrupoEtnico=1;
					break;
						
				}
				$arrValores["perteneceGrupoEtnico"]=$perteneceGrupoEtnico;
				$lgbttti="2";
				if(trim((string)$p->lgbttti)!="")
				{
					$lgbttti=(string)$p->lgbttti;
				}
				$arrValores["lgbttti"]=$lgbttti;
				
				$poblacionLGBTTI="NULL";
				if(trim((string)$p->tipoLGBTTTI)!="")
				{
					$poblacionLGBTTI=(string)$p->tipoLGBTTTI;
				}
				$arrValores["poblacion"]=$poblacionLGBTTI;
				
				$requiereTraductor="2";
				if(trim((string)$p->requiereTraductor)!="")
				{
					$requiereTraductor=(string)$p->requiereTraductor;
				}
				
				$arrValores["requiereTraductor"]=$requiereTraductor;
				
				$requiereInterprete="2";
				if(trim((string)$p->requiereinterprete)!="")
				{
					$requiereInterprete=(string)$p->requiereinterprete;
				}
				
				$arrValores["requiereInterprete"]=$requiereInterprete;
				$arrValores["lengua"]=trim((string)$p->cvelenguadialecto)==""?"NULL":trim((string)$p->cvelenguadialecto);
				
				$capacidadesDiferente="2";
				if(trim((string)$p->capacidadesdiferentes)!="")
				{
					$capacidadesDiferente=(string)$p->capacidadesdiferentes;
				}
				$arrValores["capacidadesDiferente"]=$capacidadesDiferente;
				
				$arrValores["descripcionDiscapacidad"]=trim((string)$p->desccapacidaddiferente);
				
				$poblacionCallejera=2;
				if(trim((string)$p->poblacioncallejera)!="")
				{
					$poblacionCallejera=(string)$p->poblacioncallejera;
				}
				$arrValores["poblacionCallejera"]=$poblacionCallejera;
	
				$idRegistroDomicilio=crearInstanciaRegistroFormulario(49,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
			}
			
			foreach($cXML->PersonasJuridicas[0] as $p)
			{
				$arrDocumentosReferencia=array();
				$iFigura=(string)$p->figurajuridica;
				
				if($iFigura==0)
					continue;
				
				$consulta="SELECT figuraEquivalente FROM _284_tablaDinamica WHERE id__284_tablaDinamica=".$iFigura;
				$figuraJuridica=$con->obtenerValor($consulta);
				if($figuraJuridica=="")
					$figuraJuridica=9;
				$arrValores=array();
				$idRegistroParticipante=-1;				
				$arrValores["tipoPersona"]=2;
				$arrValores["tipoFiguraPGJ"]=$iFigura;
				$arrValores["figuraJuridica"]=$figuraJuridica;
				$arrValores["rfcEmpresa"]=(string)$p->rfc;
				$arrValores["idActividad"]=$idActividad;

				
				$idRegistroParticipante=crearInstanciaRegistroFormulario(47,-1,1,$arrValores,$arrDocumentosReferencia,-1,245);
				
				foreach($p->Direcciones[0] as $d)
				{
					$arrDocumentosReferencia=array();
					
					$arrValores=array();
								
					$arrValores["calle"]=(string)$d->calle;
					$arrValores["noExt"]=(string)$d->numeroext;
					$arrValores["noInterior"]=(string)$d->numeroint;
					$arrValores["colonia"]=(string)$d->calle;
					$arrValores["entreCalle"]=(string)$d->entrecalle;
					$arrValores["yCalle"]=(string)$d->ycalle;
					$arrValores["otrasReferencias"]=(string)$d->referencia;
					$arrValores["entidadFederativa"]=(string)$d->cveentidadfederativa;
					if($arrValores["entidadFederativa"]=="---")
						$arrValores["entidadFederativa"]="IND";
						
					$consulta="SELECT cveEstado FROM 820_estados WHERE cvePGJ='".$arrValores["entidadFederativa"]."'";	
					$arrValores["entidadFederativa"]=$con->obtenerValor($consulta);	
					
					$arrValores["municipio"]=(string)$d->cvemunicipio;
					$arrValores["localidad"]="NULL";
					
					
					$codigoPostal="NULL";
					
					if(trim((string)$d->codigopostal)!="")
					{
						$codigoPostal=trim((string)$d->codigopostal);
					}
					$arrValores["codigoPostal"]=($codigoPostal=="00000B")?"NULL":$codigoPostal;
	
					$idRegistroDomicilio=crearInstanciaRegistroFormulario(48,$idRegistroParticipante,1,$arrValores,$arrDocumentosReferencia,-1,245);
					
					$query=array();
					$x=0;
					$query[$x]="begin";
					$x++;
					$correo=(string)$d->correoelectronico;
					if($correo!="")
					{
						$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
						$x++;
					}
					
					$telefono=(string)$d->telefono;
					if($telefono!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
						$x++;
					}
					$celular=(string)$d->celular;
					if($celular!="")
					{
						$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
						$x++;
					}
					$query[$x]="commit";
					$x++;
					
					$con->ejecutarBloque($query);
					
				}
				
				$query=array();
				$x=0;
				$query[$x]="begin";
				$x++;
				$correo=(string)$d->correoelectronico;
				if($correo!="")
				{
					$query[$x]="INSERT INTO _48_correosElectronico(idReferencia,correo) VALUES(".$idRegistroDomicilio.",'".cv($correo)."')";
					$x++;
				}
				
				$telefono=(string)$d->telefono;
				if($telefono!="")
				{
					$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",1,'".$telefono."')";
					$x++;
				}
				$celular=(string)$d->celular;
				if($celular!="")
				{
					$query[$x]="INSERT INTO _48_telefonos(idReferencia,tipoTelefono,numero) VALUES(".$idRegistroDomicilio.",2,'".$celular."')";
					$x++;
				}
				$query[$x]="commit";
				$x++;
				
				$con->ejecutarBloque($query);
				
			}		
			
			if($idEtapa<>1.4)
			{
				cambiarEtapaFormulario(46,$idRegistroSolicitud,$idEtapa,"",-1,"NULL","NULL",264);
			}
			
			
			$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=1,idRegistro=".$idRegistroSolicitud.",ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.
					"',idSolicitud='".((string)$cXML->DatosSolicitud[0]->idsolicitud)."' WHERE idRegistroSolicitud=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);
			$consulta="SELECT carpetaAdministrativa FROM _46_tablaDinamica WHERE id__46_tablaDinamica=".$idRegistroSolicitud;
			$carpetaAdministrativa=$con->obtenerValor($consulta);
			
			
			
			$consulta="SELECT nombreUnidad FROM _17_tablaDinamica u,7006_carpetasAdministrativas c WHERE c.carpetaAdministrativa='".$carpetaAdministrativa."' AND 
					u.claveUnidad=c.unidadGestion";
			$unidadControl=$con->obtenerValor($consulta);
			
			if(!$servidorPruebas)
				@enviarCorreoWebServicesSolicitudInicial($idRegistroSolicitud);

			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>'.$idRegistroSolicitud.'</idAcuse>
							<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>1</resultado>
						 	<carpetaAdministrativa >'.$carpetaAdministrativa.'</carpetaAdministrativa>
							<unidadControl>'.$unidadControl.'</unidadControl>
							<comentarios></comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
			return ($resultado);						
		}
		catch(Exception $e)
		{
			$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=4,comentariosAdicionales='".cv($e->getMessage()).", Linea: ".$e->getLine()."' WHERE idRegistroSolicitud=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);

			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrlsolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idsolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<unidadControl></unidadControl>
							<comentarios>'.cv($e->getMessage()).'</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';

			return ($resultado);
		}
		
		return ($resultado);
	}
	
	
	function registrarSolicitudPromocionPGJ($cadXML,$sistema)
	{

		global $con;
		global $directorioInstalacion;	
		global $servidorPruebas;
		$fechaActual=strtotime(date("Y-m-d H:i:s"));
		$idFolioSolicitud="-1";
		$cXML=NULL;
		try
		{
			$_SESSION["idUsr"]=3789;
			$consulta="select codigoRol from 807_usuariosVSRoles where idUsuario=".$_SESSION["idUsr"];
			
			$resRoles=$con->obtenerFilas($consulta);
			$listaGrupo="";
			while($fRoles=mysql_fetch_row($resRoles))
			{
				$arrRol=explode("_",$fRoles[0]);
				$rol="'".$fRoles[0]."'";
				if($arrRol[1]!="0")
					$rol.=",'".$arrRol[0]."_-1'";
				
				if($listaGrupo=="")
					$listaGrupo=$rol;
				else
					$listaGrupo.=",".$rol;
			}
			if($listaGrupo=="")
				$listaGrupo='-1';
			$_SESSION["idRol"]=$listaGrupo.",'-100_0'";
			$_SESSION["codigoUnidad"]="0001";
			$_SESSION["codigoInstitucion"]="0001";
			
			
			$cXML=simplexml_load_string($cadXML);
			
			$consulta="INSERT INTO 3011_solicitudRecibidasPGJ(fechaSolicitud,carpetaInvestigacion,idFormulario,situacion,sistema) 
					VALUES('".date("Y-m-d H:i:s",$fechaActual)."','".((string)$cXML->DatosSolicitud[0]->carpetainvestigacion)."',96,0,".$sistema.")";
			$con->ejecutarConsulta($consulta);
			$idFolioSolicitud=$con->obtenerUltimoID();
			
			@registrarDatosPeticion(3011,$idFolioSolicitud,$cadXML);
			$arrDocumentosReferencia=array();			
			
			$arrValores=array();
			$idRegistroSolicitud=-1;			
			
			$consulta="SELECT COUNT(*) FROM _96_tablaDinamica WHERE ctrlSolicitud='".(string)$cXML->DatosSolicitud[0]->ctrSolicitud."' 
					AND idSolicitud= ".((string)$cXML->DatosSolicitud[0]->idSolicitud)." and sistema=".$sistema;
			
			$nRegistro=$con->obtenerValor($consulta);
			$nRegistro=0;
			if($nRegistro>0)
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=2 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa></carpetaAdministrativa>
							<unidadControl></unidadControl>
							<comentarios>La solicitud ya ha sido registrada anteriormente</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			

			$datosDocumento=array();
			
			$datosDocumento["nombreDocumento"]=(string)$cXML->DatosSolicitud[0]->nombreDocumento;
			$datosDocumento["nombreDocumento"]=str_replace(".","",$datosDocumento["nombreDocumento"]);
			$datosDocumento["descripcionDocumento"]="";//(string)$cXML->DatosSolicitud[0]->descripcionDocumento;
			$datosDocumento["contenido"]=(string)$cXML->DatosSolicitud[0]->documentoAdjunto;
			
			$idDocumentoServidor=-1;
			if($datosDocumento["contenido"]!="")
			{
				$idDocumento=generarNombreArchivoTemporal();
				$directorioDestino=$directorioInstalacion.'\\archivosTemporales\\'.$idDocumento;
				$datos=bD($datosDocumento["contenido"]);
				$f=file_put_contents($directorioDestino,$datos);
				
				if($f)
				{
					$idDocumentoServidor=registrarDocumentoServidor($idDocumento,$datosDocumento["nombreDocumento"]);
					$consulta="UPDATE 908_archivos SET descripcion='".cv($datosDocumento["descripcionDocumento"])."' WHERE idArchivo=".$idDocumentoServidor;
					
					$con->ejecutarConsulta($consulta);
				}
			}			
			$arrValores["asuntoPromocion"]=$sistema==1?(utf8_decode((string)$cXML->DatosSolicitud[0]->cuerpoTexto)):((string)$cXML->DatosSolicitud[0]->cuerpoTexto);//(string)$cXML->DatosSolicitud[0]->carpetainvestigacion;
			
			$arrValores["numeroPromocion"]=(string)$cXML->DatosSolicitud[0]->idSolicitud;
			/*$tipoAudiencia=24;
			switch((string)$cXML->DatosSolicitud[0]->cveSolicitud)
			{
				case 28:
					$tipoAudiencia=1;
				break;
				case 33:
					$tipoAudiencia=26;
				break;
			}*/
			$consulta="SELECT tipoAudiencia,tipoTratamientoSolicitud,naturalezaSolicitud FROM _285_tablaDinamica WHERE cveTipoSolicitud='".(string)$cXML->DatosSolicitud[0]->cveSolicitud."'";
			$fTipoAudiencia=$con->obtenerPrimeraFila($consulta);
			$tipoAudiencia=$fTipoAudiencia[0];
			$arrValores["tipoPromociones"]=$fTipoAudiencia[1];
			
			if($tipoAudiencia=="")
			{
				$tipoAudiencia=103;
				$arrValores["tipoPromociones"]=2;
			}

			
			
			$arrValores["tipoAudiencia"]=$tipoAudiencia;
			
			
			$tipoAtencion=0;
			if($arrValores["tipoPromociones"]==2)
			{
				$consulta="SELECT tipoAtencion FROM _4_tablaDinamica WHERE id__4_tablaDinamica=".$tipoAudiencia;
				$tipoAtencion=$con->obtenerValor($consulta);
				if($tipoAtencion=="")
					$tipoAtencion=0;
			}
			else
			{
				$tipoAtencion=$fTipoAudiencia[2];
			}
			
			$tipoSolicitud=0;
			if($tipoAtencion==2)
				$tipoSolicitud=2;
			else
				$tipoSolicitud=3;
			
			$arrValores["carpetaAdministrativa"]=(string)$cXML->DatosSolicitud[0]->carpetaadministrativa;
			
			if(($arrValores["carpetaAdministrativa"]=="")||($arrValores["carpetaAdministrativa"]=="SIN CARPETA"))
			{
				$llaveCarpeta=generarLlaveCarpetaInvestigacion(((string)$cXML->DatosSolicitud[0]->carpetainvestigacion));
				if($llaveCarpeta!="")
				{
					$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE llaveCarpetaInvestigacion='".
								cv($llaveCarpeta)."' and tipoCarpetaAdministrativa=1  order by fechaCreacion DESC";
	
					$cAdministrativa=$con->obtenerValor($consulta);
					if($cAdministrativa!="")
					{
						$arrValores["carpetaAdministrativa"]=$cAdministrativa;
					}
				}
				
			}
			
			
			if($arrValores["carpetaAdministrativa"]=="")
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=3 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<unidadControl></unidadControl>
							<comentarios>Debe especificar la carpeta administrativa a la cual pertenece la promocion</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
				return $resultado;
			}
			
			
			/*$consulta="SELECT carpetaAdministrativa FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativaBase='".$arrValores["carpetaAdministrativa"]."' ORDER BY fechaCreacion DESC";
			$cAdministrativaAux=$con->obtenerValor($consulta);
			
			if($cAdministrativaAux!="")
			{
				$arrValores["carpetaAdministrativa"]=$cAdministrativaAux;
			}*/
			
			
			$consulta="SELECT count(*) FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".cv($arrValores["carpetaAdministrativa"])."'";
			$nCarpeta=$con->obtenerValor($consulta);
			if($nCarpeta==0)
			{
				$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=4 WHERE idRegistroSolicitud=".$idFolioSolicitud;
				$con->ejecutarConsulta($consulta);
				$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
								  <idTipoAcuse>1</idTipoAcuse>
								  <idAcuse>0</idAcuse>
								  <idCtrProcedimiento>0</idCtrProcedimiento>
								  <ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
								  <idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
								  <resultado>0</resultado>
								  <carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
								  <unidadControl></unidadControl>
								  <comentarios>La carpeta administrativa ingresada NO existe</comentarios>
								  <documentoAdjunto></documentoAdjunto>
							  </acuse>';
				  
				 return $resultado;
			}
			
			$consulta="SELECT unidadGestion FROM 7006_carpetasAdministrativas WHERE carpetaAdministrativa='".cv($arrValores["carpetaAdministrativa"])."'";
			$unidadGestionCarpeta=$con->obtenerValor($consulta);		

			$fechaCreacion=$fechaActual;		
			
			$consulta="SELECT nombreUnidad,tipoMateria FROM _17_tablaDinamica u,7006_carpetasAdministrativas c WHERE c.carpetaAdministrativa='".$arrValores["carpetaAdministrativa"]."' AND 
					u.claveUnidad=c.unidadGestion";
			$fDatosUnidadGestion=$con->obtenerPrimeraFila($consulta);
			$unidadControl=$fDatosUnidadGestion[0];
			
			if($tipoSolicitud!=2)
			{	
				$fRecepcion=date("Y-m-d H:i:s",$fechaCreacion);
				if(!esHorarioNormalDiaHabil($fRecepcion,$fDatosUnidadGestion[1]))
				{
					$fechaCreacion=strtotime(obtenerFechaRecepcionHorarioNormal($fRecepcion,$fDatosUnidadGestion[1]));
				}
			}
			
			$idEtapaCambio=1.4;
			
			$arrValores["codigoInstitucion"]=$unidadGestionCarpeta;
			$arrValores["codigoUnidad"]=$unidadGestionCarpeta;
			$arrValores["relacionPromocion"]=1;
			$arrValores["figuraPromovente"]=10;
			$arrValores["fechaCreacion"]=date("Y-m-d H:i:s",$fechaCreacion);
			$arrValores["fechaRecepcion"]=date("Y-m-d",$fechaCreacion);
			$arrValores["horaRecepcion"]=date("H:i:s",$fechaCreacion);		
			$arrValores["ctrlSolicitud"]=(string)$cXML->DatosSolicitud[0]->ctrSolicitud;
			$arrValores["idSolicitud"]=(string)$cXML->DatosSolicitud[0]->idSolicitud;
			$arrValores["cveSolicitud"]=(string)$cXML->DatosSolicitud[0]->cveSolicitud;
			$arrValores["ctrluinv"]=(string)$cXML->DatosSolicitud[0]->ctrluinv;
			$arrValores["solicitudXML"]='';
			$arrValores["ctrSolicitudInvolucra"]=(string)$cXML->DatosSolicitud[0]->ctrSolicitudInvolucra;
			$arrValores["idSolicitudInvolucra"]=(string)$cXML->DatosSolicitud[0]->idSolicitudInvolucra;
			$arrValores["fechaHoraRecepcionPromocion"]=$arrValores["fechaCreacion"];
			$arrValores["sistema"]=$sistema;
			switch($tipoAudiencia)
			{
				case 100:
					$arrValores["relacionPromocion"]=2;
					$arrValores["carpetaAdministrativaReferida"]=$arrValores["carpetaAdministrativa"];
					$arrValores["carpetaAdministrativa"]="";
				break;
				
			}
			//
			/*if($idDocumentoServidor!=-1)
			{
				array_push($arrDocumentosReferencia,$idDocumentoServidor);
			}*/
			
			
			$idRegistroSolicitud=crearInstanciaRegistroFormulario(96,-1,$idEtapaCambio,$arrValores,$arrDocumentosReferencia,-1,613);
			@registrarDatosPeticionRefiere(96,$idRegistroSolicitud,3011,$idFolioSolicitud);
			if($idDocumentoServidor!=-1)
			{
				
				convertirDocumentoUsuarioDocumentoResultadoProceso($idDocumentoServidor,96,$idRegistroSolicitud,($datosDocumento["nombreDocumento"]).".pdf",14);
				
			}
			
			$cambiarEtapa=true;
			
			
			
			switch($tipoAudiencia)
			{
				case 100:
					
					$idUnidadGestionDestino=obtenerUnidadGestionDestinatariaDocumento($idRegistroSolicitud);
					if($idUnidadGestionDestino==-1)
					{
						@enviarCorreoWebServicesSolicitudPromocionNOIdentificada($idRegistroSolicitud);
						$cambiarEtapa=false;
					}
					else
					{
						$idEtapaCambio=2;
						$arrValoresUnidad=array();
						$consulta="SELECT claveUnidad FROM _17_tablaDinamica WHERE id__17_tablaDinamica=".$idUnidadGestionDestino;
						$codigoUnidadGestion=$con->obtenerValor($consulta);
						$arrValoresUnidad["unidadGestion"]=$codigoUnidadGestion;
						$arrDocumentosReferenciaAux=array();
						$idRegistroSolicitudAux=crearInstanciaRegistroFormulario(360,$idRegistroSolicitud,1,$arrValoresUnidad,$arrDocumentosReferenciaAux,-1,613);
					}
				break;
				default:
					$idEtapaCambio=2;
				break;
				/*case 103:
					if(!$servidorPruebas)
					{
						
						@enviarCorreoWebServicesSolicitudPromocionNOIdentificada($idRegistroSolicitud);
						$cambiarEtapa=false;
					}
				break;*/
			}			
			
			/*if($arrValores["carpetaAdministrativa"]=="005/1070/2020")
			{
				sendMensajeWhatApp("2288248558","Estimado/a *Marco*, se le notifica que se ha recibido una promoción urgente los detalles se describen a continuación: Carpeta Judicial *".$arrValores["carpetaAdministrativa"]."*\n");
				sendMensajeWhatApp("5554959064","Estimado/a *Osmar*, se le notifica que se ha recibido una promoción urgente los detalles se describen a continuación: Carpeta Judicial *".$arrValores["carpetaAdministrativa"]."*\n");
			
			}
			*/
			if(!$servidorPruebas)
			{
				if($tipoSolicitud==2)
				{
					
					$unidadGestion=asignarCarpetaGuardiaV3($arrValores["carpetaAdministrativa"],$arrValores["fechaCreacion"]);
					if($unidadGestion!=-1)
					{
						@asignarCarpetaUnidadGestionGuardia($arrValores["carpetaAdministrativa"],$unidadGestion);
						$consulta="UPDATE _96_tablaDinamica SET codigoInstitucion='".$unidadGestion."' WHERE id__96_tablaDinamica=".$idRegistroSolicitud;
						$con->ejecutarConsulta($consulta);
					}
					@enviarCorreoWebServicesSolicitudPromocionUrgente($idRegistroSolicitud);
					
					//cambiarEtapaFormulario(96,$idRegistroSolicitud,5,"",-1,"NULL","NULL",704);
					
				}
				
				
			}
			
			
			if($cambiarEtapa)
				cambiarEtapaFormulario(96,$idRegistroSolicitud,$idEtapaCambio,"",-1,"NULL","NULL",704);
			$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=1,idRegistro=".$idRegistroSolicitud." WHERE idRegistroSolicitud=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>'.$idRegistroSolicitud.'</idAcuse>
							<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>1</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<unidadControl>'.$unidadControl.'</unidadControl>
							<comentarios></comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
			return $resultado;
		}
		catch(Exception $e)
		{
			$consulta="UPDATE 3011_solicitudRecibidasPGJ SET situacion=1,comentariosAdicionales='".cv($e->getMessage())."' WHERE idRegistroSolicitud=".$idFolioSolicitud;
			$con->ejecutarConsulta($consulta);
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?><acuse>
							<idTipoAcuse>1</idTipoAcuse>
							<idAcuse>0</idAcuse>
							<idCtrProcedimiento>0</idCtrProcedimiento>
							<ctrSolicitud>'.(string)$cXML->DatosSolicitud[0]->ctrSolicitud.'</ctrSolicitud>
							<idSolicitud>'.(string)$cXML->DatosSolicitud[0]->idSolicitud.'</idSolicitud >
							<resultado>0</resultado>
							<carpetaAdministrativa >'.$arrValores["carpetaAdministrativa"].'</carpetaAdministrativa>
							<unidadControl></unidadControl>
							<comentarios>'.cv($e->getMessage()).'</comentarios>
							<documentoAdjunto></documentoAdjunto>
						</acuse>';
			return $resultado;

		}
		
		return $resultado;
	}
	
	function registrarSolicitudInicial($cadXML)
	{
		return registrarSolicitudInicialPGJ($cadXML,1);
	}
	
	function registrarSolicitudInicialFSIAP($cadXML)
	{
		return registrarSolicitudInicialPGJ($cadXML,2);
	}
	
	
	
	function registrarSolicitudPromocion($cadXML)
	{

		return registrarSolicitudPromocionPGJ($cadXML,1);
	}
		
	function registrarSolicitudPromocionFSIAP($cadXML)
	{
		
		return registrarSolicitudPromocionPGJ($cadXML,2);
	}
	
	
	
	function consultaCarpetasJudiciales($carpetaInvestigacion)
	{
		global $con;
		$resultado="";
		$llaveCarpeta=generarLlaveCarpetaInvestigacion($carpetaInvestigacion);
		if($llaveCarpeta!="")
		{
			$asunto="";
			$arrCarpetasAsociadas="";
			$consulta="SELECT carpetaAdministrativa,
					(SELECT nombreTipoCarpeta FROM 7020_tipoCarpetaAdministrativa WHERE idTipoCarpeta=c.tipoCarpetaAdministrativa) as lblTipoCarpetaAdministrativa,
					fechaCreacion,
					(SELECT nombreUnidad FROM _17_tablaDinamica WHERE claveUnidad=c.unidadGestion) as unidadGestion ,
					tipoCarpetaAdministrativa
					FROM 7006_carpetasAdministrativas c WHERE llaveCarpetaInvestigacion='".cv($llaveCarpeta)."'  order by fechaCreacion ASC";

			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				
				switch($fila[4])
				{
					case 1:
						$consulta="SELECT t.tipoAudiencia FROM _46_tablaDinamica s,_4_tablaDinamica t WHERE s.carpetaAdministrativa='".$fila[0]."'
									AND  t.id__4_tablaDinamica=s.tipoAudiencia AND s.idEstado>1.4 ORDER BY s.fechaCreacion ASC";
						$asunto=utf8_encode($con->obtenerValor($consulta));
						
					break;					
					case 5:
						$asunto="Envi&iacute;o a Tribunal de Enjuciamiento";
					break;
					case 6:
						$asunto="Envi&iacute;o a Unidad de Ejecuci&Oacute;n";
					break;
					default:
						$asunto=$fila[1];
					break;
				}

				
				$oCarpeta=	'<carpetaJudicial>
								<noCarpetaJudicial>'.$fila[0].'</noCarpetaJudicial>
								<asunto>'.$asunto.'</asunto>
								<fechaCreacion>'.$fila[2].'</fechaCreacion>
								<tipoCarpeta>'.utf8_encode($fila[1]).'</tipoCarpeta>
								<unidadGestion>'.utf8_encode($fila[3]).'</unidadGestion>
							</carpetaJudicial>';

				$arrCarpetasAsociadas.=$oCarpeta;
				
			}
			
			
			
			
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<respuesta>
							<resultado>1</resultado>
							<carpetasAsociadas>'.$arrCarpetasAsociadas.'</carpetasAsociadas>
							<carpetaInvestigacion>'.$carpetaInvestigacion.'</carpetaInvestigacion>
							<comentarios>'.($arrCarpetasAsociadas==""?"No existen coincidencias":"").'</comentarios>							
						</respuesta>';
			
		}
		else
		{
			$resultado='<?xml version="1.0" encoding="ISO-8859-1"?>
						<respuesta>
							<resultado>0</resultado>
							<carpetasAsociadas></carpetasAsociadas>
							<carpetaInvestigacion>'.$carpetaInvestigacion.'</carpetaInvestigacion>
							<comentarios>Debe ingresar la carpeta investigaci&oacute;n que desea consultar</comentarios>							
						</respuesta>';
		}
		
		return $resultado;
		
	}
	
	function obtenerAudienciaSolicitud($ctrlSolicitud,$idSolicitud)
	{
		global $con;

		$idFormulario="46";
		$consulta="SELECT id__46_tablaDinamica,cveSolicitud FROM _46_tablaDinamica WHERE ctrlSolicitud='".$ctrlSolicitud."' 
					AND idSolicitud= ".$idSolicitud;
		$fRegistroSolicitud=$con->obtenerPrimeraFila($consulta);
		$idRegistroSolicitud=$fRegistroSolicitud[0];
		$idRegistro=$idRegistroSolicitud;		
		$cveSolicitud=$fRegistroSolicitud[1];
		if(!$fRegistroSolicitud)
		{
			$idRegistroSolicitud=-2;
			$idFormulario="96";
			$consulta="SELECT id__96_tablaDinamica,cveSolicitud FROM _96_tablaDinamica WHERE ctrlSolicitud='".$ctrlSolicitud."' 
						AND idSolicitud= ".$idSolicitud;			
						
			$fRegistroSolicitud=$con->obtenerPrimeraFila($consulta);
			if($fRegistroSolicitud)
			{
				$idRegistroSolicitud=$fRegistroSolicitud[0];
				$cveSolicitud=$fRegistroSolicitud[1];			
				$idRegistro=$idRegistroSolicitud;		
				
			}
			
			if($idRegistro=="")
				$idRegistro=-1;	

			$consulta="SELECT id__185_tablaDinamica FROM _185_tablaDinamica WHERE iFormulario=".$idFormulario." AND iRegistro=".$idRegistro;
			$idRegistro=$con->obtenerValor($consulta);
			$idFormulario=185;
			if($idRegistro=="")
				$idRegistro=-1;
		}
		
			
		
		$consulta="SELECT carpetaAdministrativa FROM _".$idFormulario."_tablaDinamica WHERE id__".$idFormulario."_tablaDinamica=".$idRegistro;
		$carpetaAdministrativa=$con->obtenerValor($consulta);
			
		$consulta="SELECT * FROM 7000_eventosAudiencia WHERE  idFormulario=".$idFormulario." AND idRegistroSolicitud=".$idRegistro." and situacion in (1,2,4,5)";
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		if($fRegistro)
		{
			$idEvento=$fRegistro["idRegistroEvento"];
	
			
			$consulta="SELECT id__46_tablaDinamica FROM _46_tablaDinamica WHERE carpetaAdministrativa='".$carpetaAdministrativa."'";
			$idRegistroInicial=$con->obtenerValor($consulta);
			
			$consulta="SELECT claveFiscalia,claveUnidad,claveAgencia FROM _100_tablaDinamica WHERE idReferencia=".$idRegistroInicial;
			$fFiscalia=$con->obtenerPrimeraFila($consulta);
			
			
			$datosEvento=obtenerDatosEventoAudiencia($idEvento);
			
			
			$consulta="SELECT idJuez FROM 7001_eventoAudienciaJuez WHERE idRegistroEvento=".$idEvento;
			$res=$con->obtenerFilas($consulta);
			
			$arrJueces="";
			while($fJuez=mysql_fetch_row($res))
			{
				$consulta="SELECT Nom,Paterno,Materno FROM 802_identifica WHERE idUsuario=".$fJuez[0];
				$fNombreJuez=$con->obtenerPrimeraFila($consulta);
				
				$consulta="SELECT clave FROM _26_tablaDinamica WHERE usuarioJuez=".$fJuez[0];
		
				$clave=$con->obtenerValor($consulta);
		
				$arrClaveJuez=explode(" ",$clave);
				if(sizeof($arrClaveJuez)==1)
				{
					$arrClaveJuez[1]=$arrClaveJuez[0];
				}
				$o='<juez><cveJuez>'.($arrClaveJuez[1]*1).'</cveJuez><apPaterno>'.$fNombreJuez[1].'</apPaterno><apMaterno>'.$fNombreJuez[2].'</apMaterno><nombre>'.$fNombreJuez[0].'</nombre></juez>';
				$arrJueces.=$o;
			}
		
				
		
				
			
			$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
					<asignacionAudiencia>
						<tipoNotificacion>1</tipoNotificacion>
						<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
						<ctrSolicitud>'.$ctrlSolicitud.'</ctrSolicitud> 
						<cveSolicitud>'.$cveSolicitud.'</cveSolicitud> 
						<idSolicitud>'.$idSolicitud.'</idSolicitud> 
						<fechaAudiencia>'.date("Y-m-d H:i:s",strtotime($fRegistro["horaInicioEvento"])).'</fechaAudiencia>
						<fechaFinAudiencia>'.date("Y-m-d H:i:s",strtotime($fRegistro["horaFinEvento"])).'</fechaFinAudiencia>
						<cveFiscalia>'.$fFiscalia[0].'</cveFiscalia>
						<cveAgencia>'.$fFiscalia[2].'</cveAgencia>
						<cveUnidad>'.$fFiscalia[1].'</cveUnidad>
						<edificio>'.$datosEvento->edificio.'</edificio> 
						<sala>'.$datosEvento->sala.'</sala> 
						<jueces>'.$arrJueces.'</jueces> 
						<unidadControl>'.$datosEvento->unidadGestion.'</unidadControl> 
						<textoDocumento></textoDocumento>
						<documentoAdjunto></documentoAdjunto > 

						<comentario></comentario>
						<carpetaAdministrativa>'.$carpetaAdministrativa.'</carpetaAdministrativa>
					</asignacionAudiencia>';
					
			return $xml;					
			
		}
		else
		{
			if($idRegistroSolicitud!=-2)
				$idRegistroSolicitud=-1;
			
			
			$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
					<asignacionAudiencia>
						<tipoNotificacion>1</tipoNotificacion>
						<idCtrProcedimiento>'.$idRegistroSolicitud.'</idCtrProcedimiento>
						<ctrSolicitud>'.$ctrlSolicitud.'</ctrSolicitud> 
						<cveSolicitud>'.$cveSolicitud.'</cveSolicitud> 
						<idSolicitud>'.$idSolicitud.'</idSolicitud> 
						<fechaAudiencia></fechaAudiencia>
						<fechaFinAudiencia></fechaFinAudiencia>
						<cveFiscalia></cveFiscalia>
						<cveAgencia></cveAgencia>
						<cveUnidad></cveUnidad>
						<edificio></edificio> 
						<sala></sala> 
						<jueces></jueces> 
						<unidadControl></unidadControl> 
						<textoDocumento></textoDocumento>
						<documentoAdjunto></documentoAdjunto > 
						<comentario></comentario>
						<carpetaAdministrativa>'.$carpetaAdministrativa.'</carpetaAdministrativa>
					</asignacionAudiencia>';
					
			return $xml;
		}
		
	}
	
	
	function obtenerFolioAcuseSolicitud($ctrlSolicitud,$idSolicitud)
	{
		global $con;
		$xml="";
		try
		{
			$idFormulario="46";
			$consulta="SELECT id__46_tablaDinamica,cveSolicitud,fechaCreacion FROM _46_tablaDinamica WHERE ctrlSolicitud='".$ctrlSolicitud."' 
						AND idSolicitud= ".$idSolicitud;
			$fRegistroSolicitud=$con->obtenerPrimeraFila($consulta);
			$idRegistroSolicitud=$fRegistroSolicitud[0];
			$idRegistro=$idRegistroSolicitud;		
			$cveSolicitud=$fRegistroSolicitud[1];
			if(!$fRegistroSolicitud)
			{
				$idRegistroSolicitud=-2;
				$idFormulario="96";
				$consulta="SELECT id__96_tablaDinamica,cveSolicitud,fechaCreacion FROM _96_tablaDinamica WHERE ctrlSolicitud='".$ctrlSolicitud."' 
							AND idSolicitud= ".$idSolicitud;			
							
				$fRegistroSolicitud=$con->obtenerPrimeraFila($consulta);
				
			}
			
			
			if(!$fRegistroSolicitud)
			{
				$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
						<informacionAcuse>
							<mensaje>0</mensaje>
							<error>No existe informaci&oacute;n registrada</error>
							<idAcuse></idAcuse>
							<fechaRecepcion></fechaRecepcion>
						</informacionAcuse>';
			}
			else
			{
				$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
						<informacionAcuse>
							<mensaje>1</mensaje>
							<error></error>
							<idAcuse>'.$fRegistroSolicitud[0].'</idAcuse>
							<fechaRecepcion>'.$fRegistroSolicitud[2].'</fechaRecepcion>
						</informacionAcuse>';
			}
			
		}
		catch(Exception $e)
		{
			$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
						<informacionAcuse>
							<mensaje>0</mensaje>
							<error>'.cv($e->getMessage()).'</error>
							<idAcuse></idAcuse>
							<fechaRecepcion></fechaRecepcion>
						</informacionAcuse>';

		}
					
		return $xml;					
			
		
		
	}
	
	function notificarTotalAudiencias($fechaInicio,$fechaFin)
	{
		global $con;
		$xml="";
		try
		{
			$arrResultados='';
			
			$consulta="SELECT DISTINCT e.tipoAudiencia,tA.tipoAudiencia FROM 7000_eventosAudiencia e,_4_tablaDinamica tA 
						WHERE fechaEvento>='".$fechaInicio."' AND fechaEvento<='".$fechaFin."' AND tA.id__4_tablaDinamica=e.tipoAudiencia
						AND e.situacion IN(1,2,4,5) ORDER BY tA.tipoAudiencia";
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_row($res))
			{
				$consulta="SELECT COUNT(*) FROM 7000_eventosAudiencia WHERE fechaEvento>='".$fechaInicio."' AND fechaEvento<='".$fechaFin.
						"' and tipoAudiencia=".$fila[0]." AND situacion IN(1,2,4,5)";
				$totalAudiencias=$con->obtenerValor($consulta);		
				$oResultado='<datoAudiencia>
								<tipoAudiencia><![CDATA['.utf8_encode($fila[1]).']]></ tipoAudiencia >  
								<cantidad>'.$totalAudiencias.'</cantidad>  
							</datoAudiencia>';
				$arrResultados.=$oResultado;					
			}
			
			$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
					<datosResultado>
						<datosAudiencias>'.$arrResultados.'
						</datosAudiencias>
						<resultado>1</resultado>  
						<datosComplementarios></datosComplementarios>
					</datosResultado>';	
		}
		catch(Exception $e)
		{
			$xml='<?xml version="1.0" encoding=\'ISO-8859-1\'?>
					<datosResultado>
						<datosAudiencias>
						</datosAudiencias>
						<resultado>0</resultado>  
						<datosComplementarios>'.cv($e->getMessage()).'</datosComplementarios>
					</datosResultado>';						

		}
					
		return $xml;					
			
		
		
	}
	
	
	class soap_serverPGJ extends nusoap_server 
	{
		function parseRequest($headers, $data) 
		{
			
			$this->debug('Entering parseRequest() for data of length ' . strlen($data) . ' headers:');
			$this->appendDebug($this->varDump($headers));
			if (!isset($headers['content-type'])) {
				$this->setError('Request not of type text/xml (no content-type header)');
				return false;
			}
			if (!strstr($headers['content-type'], 'text/xml')) {
				$this->setError('Request not of type text/xml');
				return false;
			}
			if (strpos($headers['content-type'], '=')) {
				$enc = str_replace('"', '', substr(strstr($headers["content-type"], '='), 1));
				$this->debug('Got response encoding: ' . $enc);
				if(preg_match('/^(ISO-8859-1|US-ASCII|UTF-8)$/i',$enc)){
					$this->xml_encoding = strtoupper($enc);
				} else {
					$this->xml_encoding = 'US-ASCII';
				}
			} else {
				// should be US-ASCII for HTTP 1.0 or ISO-8859-1 for HTTP 1.1
				$this->xml_encoding = 'ISO-8859-1';
			}
			$this->debug('Use encoding: ' . $this->xml_encoding . ' when creating nusoap_parser');
			// parse response, get soap parser obj
			$parser = new nusoap_parser(utf8_encode($data),$this->xml_encoding,'',$this->decode_utf8);
			// parser debug
			$this->debug("parser debug: \n".$parser->getDebug());
			// if fault occurred during message parsing
			if($err = $parser->getError()){
				$this->result = 'fault: error in msg parsing: '.$err;
				$this->fault('SOAP-ENV:Client',"error in msg parsing:\n".$err);
			// else successfully parsed request into soapval object
			} else {
				// get/set methodname
				$this->methodURI = $parser->root_struct_namespace;
				$this->methodname = $parser->root_struct_name;
				$this->debug('methodname: '.$this->methodname.' methodURI: '.$this->methodURI);
				$this->debug('calling parser->get_soapbody()');
				$this->methodparams = $parser->get_soapbody();
				// get SOAP headers
				$this->requestHeaders = $parser->getHeaders();
				// get SOAP Header
				$this->requestHeader = $parser->get_soapheader();
				// add document for doclit support
				$this->document = $parser->document;
			}
		 }
	}
	
	
	$arrParam=array();
	$server = new soap_serverPGJ;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('registrarSolicitudInicial',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias iniciales');
	$server->register('registrarSolicitudInicialFSIAP',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias iniciales');
	
	$server->register('registrarSolicitudPromocion',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias, promociones');
	$server->register('registrarSolicitudPromocionFSIAP',array('cadXML'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Solicitud de audiencias, promociones');
	
	$server->register('consultaCarpetasJudiciales',array('carpetaInvestigacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Consulta de carpetas Judiciales por carpeta de investigacion');
	$server->register('obtenerAudienciaSolicitud',array('ctrlSolicitud'=>'xsd:string','idSolicitud'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Consulta de carpetas Judiciales por carpeta de investigacion');
	$server->register('obtenerFolioAcuseSolicitud',array('ctrlSolicitud'=>'xsd:string','idSolicitud'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Consulta de ID de acuse');
	$server->register('notificarTotalAudiencias',array('fechaInicio'=>'xsd:string','fechaFin'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Consulta del total de audiencias');
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);