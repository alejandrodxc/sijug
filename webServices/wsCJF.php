<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	include_once("latis/sgjp/siajop.php");
	include_once("latis/latisErrorHandler.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	
	function recibeNotificacionAsuntoAmparo($objNotificacion)
	{
		global $con;
		global $baseDir;
		
		$notificarNuevoIngreso=false;
		$esPrueba=true;
		$validarHorarioHabil=!$esPrueba;
		
		try
		{
			$objNotificacion= utf8_encode($objNotificacion);
			//@registrarDatosPeticion(999,1,$objNotificacion);	

			$arrDocumentos=array();
			$arrValores=array();
			$_SESSION["idUsr"]=3369;
			$_SESSION["codigoUnidad"]="000";
			$_SESSION["codigoInstitucion"]="000";
			
			$rutaDestino=$baseDir."\\archivosTemporales\\";
			$respuesta="";
			$idRegistro="";
			$fechaActual=date("Y-m-d H:i:s");
			$tmeFechaActual=strtotime($fechaActual);
			$obj=json_decode($objNotificacion);
			
			if($validarHorarioHabil)
			{
				if(!esDiaHabilInstitucion(date("Y-m-d",$tmeFechaActual)))
				{		
					$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"Solo se permite la recepci&oacute;n de asuntos en d&iacute;as h&aacute;biles","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
					return $respuesta;
				}
			}
			$horaFin="15:00";
			$dia=date("w",$tmeFechaActual);
			if($dia==5)
				$horaFin="14:00";
			
			$horaInicio=date("Y-m-d",$tmeFechaActual);
			$horaInicio.=" 09:00";
			$horaInicio=strtotime($horaInicio);
			$holaFin=date("Y-m-d",$tmeFechaActual);
			$holaFin.=" ".$horaFin;
			$holaFin=strtotime($holaFin);
			if($validarHorarioHabil)
			{
				if(($tmeFechaActual<$horaInicio)||($tmeFechaActual>=$holaFin))
				{
					$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"Fuera de horario permitido (Lunes a Jueves de 09:00 a 15:00 hrs., Viernes de 09:00 a 14:00 hrs.)","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
						return $respuesta;
				}
			}
			$query="SELECT COUNT(*) FROM _346_tablaDinamica WHERE folioEnvioCJF='".$obj->folioEnvio."'";
			$numReg=$con->obtenerValor($query);
			if($esPrueba)
				$numReg=0;
			if($numReg>0)
			{
				$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"El folio de envio ya ha sido registrado previamente","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
				return $respuesta;
			}
			
			
			$unidadDestino="";
			$query="SELECT idReferencia,usuarioJuez,id__26_tablaDinamica FROM _26_tablaDinamica WHERE claveCJF=".$obj->organo_impartidor_justicia;
			$fDestino=$con->obtenerPrimeraFila($query);
			if(!$fDestino)
			{
				$query="SELECT claveUnidad,id__17_tablaDinamica FROM _17_tablaDinamica WHERE claveCJF=".$obj->organo_impartidor_justicia;
				$fDatosDestinoUGA=$con->obtenerPrimeraFila($query);
				$unidadDestino=$fDatosDestinoUGA[0];
				$query="SELECT idReferencia,usuarioJuez,id__26_tablaDinamica FROM _26_tablaDinamica WHERE idReferencia=".($fDatosDestinoUGA[1]!=""?$fDatosDestinoUGA[1]:-1);
				$fDestino=$con->obtenerPrimeraFila($query);
			}
			else
			{
				$query="SELECT claveUnidad FROM _17_tablaDinamica WHERE id__17_tablaDinamica=".$fDestino[0];
				$unidadDestino=$con->obtenerValor($query);
			}
			
			
			
			if($unidadDestino=="")
			{
				$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"No se encuentra el organo impartidor de justicia","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
				return $respuesta;
			}
			
			
			$x=0;
			$consulta[$x]="begin";
			$x++;
			$fBusqueda2="";
			
			$carpetaAdministrativa="";
			$tipoRegistro=0; //2 Promocion de un neun y organo existente; 1 Acumulado de neun existente; 0 Nuevo registro
			
			$query="SELECT idRegistroAmparo FROM _346_organosImpartidoresJusticiaCJF WHERE neunCJF='".$obj->neun.
					"' AND organoImpartidorJusticia=".$obj->organo_impartidor_justicia;
			$fAmparoBusqueda=$con->obtenerPrimeraFilaAsoc($query);
			if($fAmparoBusqueda)
			{
				$tipoRegistro=2;
				$query="SELECT carpetaAmparo,codigoInstitucion FROM _346_tablaDinamica WHERE 
						id__346_tablaDinamica=".$fAmparoBusqueda["idRegistroAmparo"];
				$fDatosAmparoTmp=$con->obtenerPrimeraFila($query);
				$carpetaAdministrativa=$fDatosAmparoTmp[0];
				$fAmparoBusqueda["codigoInstitucion"]=$fDatosAmparoTmp[1];

			}
			else
			{
				$query="SELECT idRegistroAmparo FROM _346_organosImpartidoresJusticiaCJF WHERE neunCJF='".$obj->neun."' and unidadGestion='".$unidadDestino."'";
				$fBusqueda2=$con->obtenerPrimeraFila($query);
				if($fBusqueda2)
				{
					$tipoRegistro=1;
					
					$query="SELECT idEstado FROM _346_tablaDinamica WHERE id__346_tablaDinamica=".$fBusqueda2[0];
					$idEstado=$con->obtenerValor($query);
					if($idEstado>1.6)
					{
						$tipoRegistro=2;
						$consultaAux=array();
						$xAux=0;
						$consultaAux[$xAux]="begin";
						$xAux++;
						$consultaAux[$xAux]="set @idRegistro:=".$fBusqueda2[0];
						$xAux++;
						$consultaAux[$xAux]="INSERT INTO _346_organosImpartidoresJusticiaCJF(neunCJF,organoImpartidorJusticia,idRegistroAmparo,datosCJF,unidadGestion)
											VALUES('".$obj->neun."','".$obj->organo_impartidor_justicia."',@idRegistro,'".cv(bE($datosCJF))."','".$unidadDestino."')";
						$xAux++;
						$consultaAux[$xAux]="set @idRegistroOrgano:=(select last_insert_id())";
						$xAux++;
						$consultaAux[$xAux]="INSERT INTO _346_juecesAmparo(idPadre,idOpcion) VALUES(@idRegistro,".$fDestino[2].")";
						$xAux++;							
						$consultaAux[$xAux]="commit";
						$xAux++;
						$con->ejecutarBloque($consultaAux);
					}
					else
					{
						$query="SELECT COUNT(*) FROM 9060_tableroControl_4 WHERE iFormulario=346 AND  iRegistro=".$fBusqueda2[0].
							" AND fechaVisualizacion IS NOT NULL AND idUsuarioDestinatario NOT IN(1)";
						$nVisualizaciones=$con->obtenerValor($query);						
						if($nVisualizaciones>0)
						{
							$notificarNuevoIngreso=true;
							
						}
					}
					
					
					
					
				}
				
			}
			$ICOIJ_Solicitud="";
			
			foreach($obj->Solicitud as $s)
			{
				$oSolicitud='{"SolicitudId":"'.$s->SolicitudId.'","fec_envio":"'.$s->fec_envio.'"}';
				if($ICOIJ_Solicitud=="")
					$ICOIJ_Solicitud=$oSolicitud;
				else
					$ICOIJ_Solicitud.=",".$oSolicitud;
			}
			
			$ICOIJ_Solicitud="[".$ICOIJ_Solicitud."]";
			$datosCJF='{"fec_envio":"'.$obj->fec_envio.'","folioEnvio":"'.$obj->folioEnvio.
						'","idOrganoOrigen":"'.$obj->idOrganoOrigen.'","ICOIJ_Solicitud":'.$ICOIJ_Solicitud.
						',"organoImpartidorJusticia":"'.$obj->organo_impartidor_justicia.'"}';
				
			if($esPrueba)
				$tipoRegistro=0;
			
			switch($tipoRegistro)
			{
				case 0: // Nuevo registro
					$idActividad=generarIDActividad(346);
					$categoriaAmparo=0;

					$query="SELECT entidadFederativa FROM _468_tablaDinamica WHERE id__468_tablaDinamica=".$obj->idOrganoOrigen;
					$entidadFederativa=$con->obtenerValor($query);
					
					$consulta[$x]="INSERT INTO _346_tablaDinamica(fechaCreacion,responsable,idEstado,codigoInstitucion,fechaRecepcion,horaRecepcion,tipoAmparo,
								noOficio,carpetaAdministrativa,categoriaAmparo,organoJurisdiccionalRequiriente,entidadFederativa,folioEnvioCJF,
								expedienteElectronicoCJF,urlEECJF,tipoCuadernoCJF,neunCJF,numeroExpedienteCJF,tipoProcedimientoCJF,tipoSubNivelCJF,
								tipoMateriaCJF,idActividadCarpetaAmparo)
								VALUES('".$fechaActual."',3369,1,'".$unidadDestino."','".date("Y-m-d",strtotime($fechaActual))."','".date("H:i:s",strtotime($fechaActual))."',
								".$obj->tipoAsunto.",'".$obj->folioEnvio."','".$carpetaAdministrativa."','".$categoriaAmparo."',".$obj->idOrganoOrigen.",
								'".$entidadFederativa."','".$obj->folioEnvio."',".$obj->expedienteElectronico.",'".$obj->urlEE."','".cv($obj->tipoCuaderno)."','".
								$obj->neun."','".$obj->numeroExpediente."','".$obj->tipoProcedimiento."','".$obj->tipoSubNivel."','".
								$obj->tipoMateria."',".$idActividad.")";
					$x++;
					$consulta[$x]="set @idRegistro:=(select last_insert_id())";
					$x++;
					$consulta[$x]="INSERT INTO _346_organosImpartidoresJusticiaCJF(neunCJF,organoImpartidorJusticia,idRegistroAmparo,datosCJF,unidadGestion)
								 VALUES('".$obj->neun."','".$obj->organo_impartidor_justicia."',@idRegistro,'".cv(bE($datosCJF))."','".$unidadDestino."')";
					$x++;
					$consulta[$x]="set @idRegistroOrgano:=(select last_insert_id())";
					$x++;
					$consulta[$x]="INSERT INTO _346_juecesAmparo(idPadre,idOpcion) VALUES(@idRegistro,".$fDestino[2].")";
					$x++;
					
					foreach($obj->Documento as $d)
					{
						$idDocumento=generarNombreArchivoTemporal();
						$directorioDestino=$rutaDestino.$idDocumento;
						$datos=bD($d->FileBase64);
						$f=file_put_contents($directorioDestino,$datos);
			
						if($f)
						{	
							if($d->Pkcs7Base64!="")
								$f=file_put_contents($directorioDestino.".pkcs7",bD($d->Pkcs7Base64));
							$idDocumentoServidor=registrarDocumentoServidorRepositorio($idDocumento,$d->Nombre,45,"");
							array_push($arrDocumentos,$idDocumentoServidor);
							$consulta[$x]="INSERT INTO 9074_documentosRegistrosProceso(idFormulario,idRegistro,idDocumento,tipoDocumento) 
											VALUES(346,@idRegistro,".$idDocumentoServidor.",2)";
							$x++;
						}
					}
			
					$consulta[$x]="commit";
					$x++;
					
					if($con->ejecutarBloque($consulta))
					{
							
						$query="select @idRegistro";
						$idRegistro=$con->obtenerValor($query);
						
						$query="select @idRegistroOrgano";
						$idRegistroOrgano=$con->obtenerValor($query);
						
						$query="SELECT carpetaAmparo FROM _346_tablaDinamica WHERE id__346_tablaDinamica=".$idRegistro;
						$carpetaAdministrativa=$con->obtenerValor($query);
						
						@registrarDatosPeticion(346,$idRegistroOrgano,$objNotificacion);
						asignarFolioRegistro(346,$idRegistro);
						cambiarEtapaFormulario(346,$idRegistro,1.5,"",-1,"NULL","NULL",840);
						foreach($arrDocumentos as $iDocumento)
						{
							registrarDocumentoCarpetaAdministrativa($carpetaAdministrativa,$iDocumento,346,$idRegistro);
						}
						$respuesta='{"FolioConfirmacion":"346'.$idRegistroOrgano.'","CodigoRetorno":"1","Mensaje":"","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
								
					}
					else
					{
						$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
					}
					return $respuesta;
				break;
				case 1: //1 Acumulado de neun existente;
					$arrShaDocuments=array();
					
					$query="SELECT idDocumento FROM 9074_documentosRegistrosProceso WHERE idFormulario=346 AND idRegistro=".$fBusqueda2[0];
					$rDocumentos=$con->obtenerFilas($query);
					while($fDocumento=mysql_fetch_row($rDocumentos))
					{
						$rutaDocumento=obtenerRutaDocumento($fDocumento[0]);
						$arrShaDocuments[sha1_file($rutaDocumento)]=1;
					}
					
					$consulta[$x]="set @idRegistro:=".$fBusqueda2[0];
					$x++;
					$consulta[$x]="INSERT INTO _346_organosImpartidoresJusticiaCJF(neunCJF,organoImpartidorJusticia,idRegistroAmparo,datosCJF,unidadGestion)
								 VALUES('".$obj->neun."','".$obj->organo_impartidor_justicia."',@idRegistro,'".cv(bE($datosCJF))."','".$unidadDestino."')";
					$x++;
					$consulta[$x]="set @idRegistroOrgano:=(select last_insert_id())";
					$x++;
					$consulta[$x]="INSERT INTO _346_juecesAmparo(idPadre,idOpcion) VALUES(@idRegistro,".$fDestino[2].")";
					$x++;
					
					
					
					foreach($obj->Documento as $d)
					{
						$idDocumento=generarNombreArchivoTemporal();
						$directorioDestino=$rutaDestino.$idDocumento;
						$datos=bD($d->FileBase64);
						$f=file_put_contents($directorioDestino,$datos);
			
						if($f)
						{	
							$llaveArchivo=sha1_file($directorioDestino);
							if(!isset($arrShaDocuments[$llaveArchivo]))
							{
								if($d->Pkcs7Base64!="")
									$f=file_put_contents($directorioDestino.".pkcs7",bD($d->Pkcs7Base64));
								$idDocumentoServidor=registrarDocumentoServidorRepositorio($idDocumento,$d->Nombre,45,"");
								$consulta[$x]="INSERT INTO 9074_documentosRegistrosProceso(idFormulario,idRegistro,idDocumento,tipoDocumento) 
												VALUES(346,@idRegistro,".$idDocumentoServidor.",2)";
								$x++;
							}
						}
					}
					
					$consulta[$x]="commit";
					$x++;
					
					if($con->ejecutarBloque($consulta))
					{
						
						$query="select @idRegistroOrgano";
						$idRegistroOrgano=$con->obtenerValor($query);
						
						@registrarDatosPeticion(346,$idRegistroOrgano,$objNotificacion);	
						if($notificarNuevoIngreso)
						{
							cambiarEtapaFormulario(346,$fBusqueda2[0],1.6,"",-1,"NULL","NULL",840);
							
						}
						$respuesta='{"FolioConfirmacion":"346'.$idRegistroOrgano.'","CodigoRetorno":"1","Mensaje":"","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
								
					}
					else
					{
						$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
					}
					return $respuesta;
					
				break;
				case 2: //2 Promocion de un neun y organo existente;
					$arrDocumentosReferencia=array();
					foreach($obj->Documento as $d)
					{
						$idDocumento=generarNombreArchivoTemporal();
						$directorioDestino=$rutaDestino.$idDocumento;
						$datos=bD($d->FileBase64);
						$f=file_put_contents($directorioDestino,$datos);
			
						if($f)
						{	
							if($d->Pkcs7Base64!="")
								$f=file_put_contents($directorioDestino.".pkcs7",bD($d->Pkcs7Base64));
							$idDocumentoServidor=registrarDocumentoServidorRepositorio($idDocumento,$d->Nombre,45,"");
							array_push($arrDocumentosReferencia,$idDocumentoServidor);
						}
					}
					$arrValores=array();
					$arrValores["fechaRecepcion"]=date("Y-m-d");
					$arrValores["horaRecepcion"]=date("H:i:s");
					$arrValores["carpetaAdministrativa"]=$carpetaAdministrativa;
					$arrValores["idProcesoPadre"]=164;
					$arrValores["datosCJF"]=bE($datosCJF);
					$arrValores["codigoInstitucion"]=$fAmparoBusqueda["codigoInstitucion"];
					$arrValores["codigoUnidad"]=$fAmparoBusqueda["codigoInstitucion"];
					
					
					$query="SELECT idActorProcesoEtapa FROM 944_actoresProcesoEtapa WHERE idProceso=204 AND actor='150_0' AND numEtapa=1";
					$actorCJF=$con->obtenerValor($query);
					$idRegistroSolicitud=crearInstanciaRegistroFormulario(460,$fAmparoBusqueda["idRegistroAmparo"],1,$arrValores,$arrDocumentosReferencia,-1,$actorCJF);
					
					
					@registrarDatosPeticion(460,$idRegistroSolicitud,$objNotificacion);	
					
					$consulta="INSERT INTO _460_juezReferido(idPadre,idOpcion) VALUES(".$idRegistroSolicitud.",".$fDestino[2].")";
					$con->ejecutarConsulta($consulta);
					
					cambiarEtapaFormulario(460,$idRegistroSolicitud,1.5,"",-1,"NULL","NULL",$actorCJF);
					
					$respuesta='{"FolioConfirmacion":"460'.$idRegistroSolicitud.'","CodigoRetorno":"1","Mensaje":"","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
					return $respuesta;
				break;
			}
			
			
		}
		catch(Exception $e)
		{
			$respuesta='{"FolioConfirmacion":"0","CodigoRetorno":"0","Mensaje":"'.cv(utf8_encode($e->getMessage())).'","FechaRecepcion":"'.date("Y-m-d H:i:s").'"}';
			return $respuesta;

		}
	}


	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('recibeNotificacionAsuntoAmparo',array('objNotificacion'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>