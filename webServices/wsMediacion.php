<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	
	
	include_once("latis/sgjp/siajop.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');
	
	function obtenerMediacionesEvento($idEvento)
	{
		global $con;
		
		return utf8_encode(generarNotificacionMediacion($idEvento));
		
	}
	
	
	function generarNotificacionMediacion($idEvento)
	{
		global $con;
		
		$consulta="SELECT carpetaAdministrativa FROM 7007_contenidosCarpetaAdministrativa WHERE tipoContenido=3 
					AND idRegistroContenidoReferencia=".$idEvento;
	
		$cAdministrativa=$con->obtenerValor($consulta);
		$consulta="SELECT * FROM 7000_eventosAudiencia WHERE idRegistroEvento=".$idEvento;
		$fRegistro=$con->obtenerPrimeraFilaAsoc($consulta);
		
		$consulta="SELECT folioCarpetaInvestigacion,idActividad FROM _46_tablaDinamica WHERE carpetaAdministrativa='".$cAdministrativa."'";
		
		$fDatosSolicitud=$con->obtenerPrimeraFila($consulta);
		
		$folioCarpetaInv=$fDatosSolicitud[0];
		$idActividad=$fDatosSolicitud[1];
		
		$consulta="SELECT * FROM 7001_eventoAudienciaJuez WHERE idRegistroEvento=".$idEvento;
		$fJuez=$con->obtenerPrimeraFilaAsoc($consulta);	
		
		$consulta="SELECT Paterno,Materno,Nom FROM 802_identifica WHERE idUsuario=".$fJuez["idJuez"];
		$fDatosJuez=$con->obtenerPrimeraFilaAsoc($consulta);	
		
		$consulta="SELECT clave FROM _26_tablaDinamica WHERE usuarioJuez=".$fJuez["idJuez"];
		$noJuez=$con->obtenerValor($consulta);
		
		$arrImputados="";
		
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=4";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{
			
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrDelitos="";
			
			
			$consulta="SELECT d.id__35_denominacionDelito,d.denominacionDelito FROM _61_tablaDinamica t,_61_chkDelitosImputado i, _35_denominacionDelito d
						WHERE i.idPadre=t.id__61_tablaDinamica AND t.denominacionDelito=d.id__35_denominacionDelito AND t.idActividad= ".$idActividad."
						ORDER BY d.denominacionDelito";
			$rDelitos=$con->obtenerFilas($consulta);
			
			if($con->filasAfectadas==0)
			{
				$consulta="SELECT d.id__35_denominacionDelito,d.denominacionDelito FROM _61_tablaDinamica t, _35_denominacionDelito d
						WHERE  t.denominacionDelito=d.id__35_denominacionDelito AND t.idActividad= ".$idActividad."
						ORDER BY d.denominacionDelito";
				$rDelitos=$con->obtenerFilas($consulta);
			}
			
			while($fDelitos=mysql_fetch_row($rDelitos))
			{
				$oDelito='{
								"cveDelito":"'.$fDelitos[0].'",
								"delito":"'.cv($fDelitos[1]).'"
						  }';
				if($arrDelitos=="")
					$arrDelitos=$oDelito;
				else
					$arrDelitos.=",".$oDelito;
			}
			
			$arrAlias="";
			$consulta="SELECT alias FROM _47_gridAlias WHERE idReferencia=".$fParticipante[0];
			$rAlias=$con->obtenerFilas($consulta);
			while($fAlias=mysql_fetch_row($rAlias))
			{
				$a='{"alias":"'.cv($fAlias[0]).'"}';
				if($arrAlias=="")
					$arrAlias=$a;
				else
					$arrAlias.=",".$a;
			}
			
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			
			$arrMedidaCautelar="";
			$consulta="SELECT tipoMedida,comentariosAdicionales,valorComp1,valorComp2,valorComp3 
						FROM 3014_registroMedidasCautelares WHERE idEventoAudiencia=".$idEvento." AND idImputado=".$fParticipante[0];
			
			$rMedidaCautelar=$con->obtenerFilas($consulta);
			
			while($fMedidaCautelar=mysql_fetch_row($rMedidaCautelar))
			{
				$oMedida="";
				if($fMedidaCautelar[0]==2)
				{
					$oMedida='{
								  "cveMedidaCautelar":"'.$fMedidaCautelar[0].'",
								  "detalleMedida":"(No pagos/exhibiciones: '.$fMedidaCautelar[3].'). '.cv($fMedidaCautelar[1]).'",
								  "montoMedida":"'.$fMedidaCautelar[2].'" 
							  }';
				}
				else
				{
					$comp="";
					if($fMedidaCautelar[0]==1)
					{
	
						$consulta="select nombreAutoridad FROM _328_tablaDinamica where id__328_tablaDinamica=".$fMedidaCautelar[2];
						$nombreAutoridad=$con->obtenerValor($consulta);
						$comp=" (Autoridad ante la cual debe presentarse: ".cv($nombreAutoridad).").";
					}
					$oMedida='{
								  "cveMedidaCautelar":"'.$fMedidaCautelar[0].'",
								  "detalleMedida":"'.cv($fMedidaCautelar[1]).$comp.'",
								  "montoMedida":"" 
							  }';
				}
				if($arrMedidaCautelar=="")
					$arrMedidaCautelar=$oMedida;
				else
					$arrMedidaCautelar.=",".$oMedida;
			}
			
			if($arrMedidaCautelar=="")
				continue;
			
			$o='{
					"idImputado":"'.$fParticipante[0].'",
					"tipoPersona":"'.$fDatosParticipantes["tipoPersona"].'",   
					"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
					"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
					"nombre":"'.cv($fDatosParticipantes["nombre"]).'",   
					"curp":"'.$fDatosParticipantes["curp"].'",
					"rfc":"'.$fDatosParticipantes["rfcEmpresa"].'",
					"fechaNacimiento":"'.$fDatosParticipantes["fechaNacimiento"].'",
					"genero":"'.$fDatosParticipantes["genero"].'", 
					"delitos": 	['.$arrDelitos.'],
					"alias":	['.$arrAlias.'],
					"datosContacto":	{
											"domicilio":	{
																"calle":"'.cv($fDatosContacto["calle"]).'",
																"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																"localidad":"'.cv($fDatosContacto["localidad"]).'" 
															},
											"telefono":	[
															'.$arrTelefonos.'
														],
											"email":	[
															'.$arrMail.'
														]
										}         
			
				}';
			
			if($arrImputados=="")
				$arrImputados=$o;
			else
				$arrImputados.=",".$o;
		}
		
		$arrVictimas="";
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=2";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{
			
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			$o="";
			
			if($fDatosParticipantes["tipoPersona"]==1)
			{
			
				$o='{
						"idVictima":"'.$fParticipante[0].'",
						"tipoPersona":"'.$fDatosParticipantes["tipoPersona"].'",   
						"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
						"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
						"nombre":"'.cv($fDatosParticipantes["nombre"]).'",   
						"curp":"'.$fDatosParticipantes["curp"].'",
						"rfc":"'.$fDatosParticipantes["rfcEmpresa"].'",
						"fechaNacimiento":"'.$fDatosParticipantes["fechaNacimiento"].'",
						"genero":"'.$fDatosParticipantes["genero"].'", 
						"datosContacto":	{
												"domicilio":	{
																	"calle":"'.cv($fDatosContacto["calle"]).'",
																	"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																	"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																	"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																	"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																	"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																	"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																	"localidad":"'.cv($fDatosContacto["localidad"]).'" 
																},
												"telefono":	[
																'.$arrTelefonos.'
															],
												"email":	[
																'.$arrMail.'
															]
											}          
				
					}';
			}
			else
			{
				$o='{
						"idVictima":"'.$fParticipante[0].'",
						"tipoPersona":"'.$fDatosParticipantes["tipoPersona"].'",   
						"razoSocial":"'.cv($fDatosParticipantes["nombre"]).'",   
						"rfc":"'.$fDatosParticipantes["rfcEmpresa"].'",
						"datosContacto":	{
												"domicilio":	{
																	"calle":"'.cv($fDatosContacto["calle"]).'",
																	"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																	"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																	"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																	"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																	"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																	"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																	"localidad":"'.cv($fDatosContacto["localidad"]).'" 
																},
												"telefono":	[
																'.$arrTelefonos.'
															],
												"email":	[
																'.$arrMail.'
															]
											}          
				
					}';
			}
			if($arrVictimas=="")
				$arrVictimas=$o;
			else
				$arrVictimas.=",".$o;
		}
		
		
		$arrDefensores="";
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=5";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{	
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			$arrImputadoDefendido="";
			$consulta="SELECT i.idOpcion FROM _353_tablaDinamica fi,_353_chkImputados i WHERE i.idPadre=fi.id__353_tablaDinamica AND fi.idReferencia=".$fParticipante[0];
			$rImputadoDefendido=$con->obtenerFilas($consulta);
			while($fImputado=mysql_fetch_row($rImputadoDefendido))
			{
				$i='{
						"idImputado":"'.$fImputado[0].'"
					}';
				if($arrImputadoDefendido=="")
					$arrImputadoDefendido=$i;
				else
					$arrImputadoDefendido.=",".$i;
			}
			
			$o='{
					"idDefensor":"'.$fParticipante[0].'",
					"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
					"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
					"nombre":"'.cv($fDatosParticipantes["nombre"]).'", 
					"imputadosDefendidos":	[
												'.$arrImputadoDefendido.'
											],    
					"datosContacto":	{
											"domicilio":	{
																"calle":"'.cv($fDatosContacto["calle"]).'",
																"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																"localidad":"'.cv($fDatosContacto["localidad"]).'" 
															},
											"telefono":	[
															'.$arrTelefonos.'
														],
											"email":	[
															'.$arrMail.'
														]
										}          
			
				}';
			
			
			if($arrDefensores=="")
				$arrDefensores=$o;
			else
				$arrDefensores.=",".$o;
		}
		
		$arrAsesores="";
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=3";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{	
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			$arrVictimasAsesoradas="";
			$consulta="SELECT i.idOpcion FROM _354_tablaDinamica fi,_354_chkVictimas i WHERE i.idPadre=fi.id__354_tablaDinamica 
					AND fi.idReferencia=".$fParticipante[0];
			$rImputadoDefendido=$con->obtenerFilas($consulta);
			while($fImputado=mysql_fetch_row($rImputadoDefendido))
			{
				$i='{
						"idVictima":"'.$fImputado[0].'"
					}';
				if($arrVictimasAsesoradas=="")
					$arrVictimasAsesoradas=$i;
				else
					$arrVictimasAsesoradas.=",".$i;
			}
			
			$o='{
					"idAsesor":"'.$fParticipante[0].'",
					"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
					"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
					"nombre":"'.cv($fDatosParticipantes["nombre"]).'", 
					"victimasAsesoradas":	[
												'.$arrVictimasAsesoradas.'
											],    
					"datosContacto":	{
											"domicilio":	{
																"calle":"'.cv($fDatosContacto["calle"]).'",
																"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																"localidad":"'.cv($fDatosContacto["localidad"]).'" 
															},
											"telefono":	[
															'.$arrTelefonos.'
														],
											"email":	[
															'.$arrMail.'
														]
										}          
			
				}';
			
			
			if($arrAsesores=="")
				$arrAsesores=$o;
			else
				$arrAsesores.=",".$o;
		}
		
		$arrRepresentantes="";
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=6";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{	
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			$arrRepresentados="";
			$consulta="SELECT i.idOpcion FROM _355_tablaDinamica fi,_355_chkImplicados i WHERE i.idPadre=fi.id__355_tablaDinamica 
					AND fi.idReferencia=".$fParticipante[0];
			$rImputadoDefendido=$con->obtenerFilas($consulta);
			while($fImputado=mysql_fetch_row($rImputadoDefendido))
			{
				$i='{
						"idRepresentado":"'.$fImputado[0].'"
					}';
				if($arrRepresentados=="")
					$arrRepresentados=$i;
				else
					$arrRepresentados.=",".$i;
			}
			
			$o='{
					"idRepresentante":"'.$fParticipante[0].'",
					"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
					"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
					"nombre":"'.cv($fDatosParticipantes["nombre"]).'", 
					"representados":	[
												'.$arrRepresentados.'
											],    
					"datosContacto":	{
											"domicilio":	{
																"calle":"'.cv($fDatosContacto["calle"]).'",
																"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																"localidad":"'.cv($fDatosContacto["localidad"]).'" 
															},
											"telefono":	[
															'.$arrTelefonos.'
														],
											"email":	[
															'.$arrMail.'
														]
										}          
			
				}';
			
			
			if($arrRepresentantes=="")
				$arrRepresentantes=$o;
			else
				$arrRepresentantes.=",".$o;
		}
		
		$arrTestigos="";
		$consulta="SELECT idParticipante FROM 7005_relacionFigurasJuridicasSolicitud WHERE idActividad=".$fDatosSolicitud[1]." and idFiguraJuridica=7";
		$resParticipante=$con->obtenerFilas($consulta);
		while($fParticipante=mysql_fetch_row($resParticipante))
		{	
			$consulta="SELECT * FROM _47_tablaDinamica WHERE id__47_tablaDinamica=".$fParticipante[0];
			$fDatosParticipantes=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$consulta="SELECT * FROM _48_tablaDinamica WHERE idReferencia=".$fParticipante[0];
			$fDatosContacto=$con->obtenerPrimeraFilaAsoc($consulta);
			
			$arrTelefonos="";
			$consulta="SELECT tipoTelefono,lada,numero,extension FROM _48_telefonos WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rTelefono=$con->obtenerFilas($consulta);
			while($fTelefono=mysql_fetch_row($rTelefono))
			{
				$o='{
						"lada":"'.$fTelefono[1].'", 
						"numero":"'.$fTelefono[2].'", 
						"extension":"'.$fTelefono[3].'"
					}';
			
				if($arrTelefonos=="")
					$arrTelefonos=$o;
				else
					$arrTelefonos.=",".$o;
			
			}
			
			$arrMail="";
			$consulta="SELECT correo FROM _48_correosElectronico WHERE idReferencia=".$fDatosContacto["id__48_tablaDinamica"];
			$rMail=$con->obtenerFilas($consulta);
			while($fMail=mysql_fetch_row($rMail))
			{
				$o='{
							"email":"'.$fMail[0].'"
						}';
				if($arrMail=="")
					$arrMail=$o;
				else
					$arrMail.=",".$o;
			}
			
			$o='{
					"idTestigo":"'.$fParticipante[0].'",
					"apPaterno":"'.cv($fDatosParticipantes["apellidoPaterno"]).'",  
					"apMaterno":"'.cv($fDatosParticipantes["apellidoMaterno"]).'",	
					"nombre":"'.cv($fDatosParticipantes["nombre"]).'", 
					"datosContacto":	{
											"domicilio":	{
																"calle":"'.cv($fDatosContacto["calle"]).'",
																"numeroInt":"'.cv($fDatosContacto["noInterior"]).'",  
																"numeroExt":"'.cv($fDatosContacto["noExt"]).'", 
																"colonia":"'.cv($fDatosContacto["colonia"]).'",	
																"cp":"'.cv($fDatosContacto["codigoPostal"]).'",	
																"estado":"'.cv($fDatosContacto["entidadFederativa"]).'", 
																"municipio":"'.cv($fDatosContacto["municipio"]).'", 
																"localidad":"'.cv($fDatosContacto["localidad"]).'" 
															},
											"telefono":	[
															'.$arrTelefonos.'
														],
											"email":	[
															'.$arrMail.'
														]
										}          
			
				}';
			
			
			if($arrTestigos=="")
				$arrTestigos=$o;
			else
				$arrTestigos.=",".$o;
		}
		
		$cadJSON='{
					  "carpetaAdministrativa":"'.$cAdministrativa.'",
					  "carpetaInvestigacion":"'.$folioCarpetaInv.'",
					  "idEventoAudiencia":"'.$idEvento.'", 
					  "fechaAudienciaMedida":"'.$fRegistro["fechaEvento"].'",
					  "horaInicioAudienciaMedida":"'.date("H:i:s",strtotime($fRegistro["horaInicioEvento"])).'", 
					  "juezAudienciaMedida":	{
												  "idJuez":"'.$fJuez["idJuez"].'",
												  "noJuez":"'.$noJuez.'",
												  "apPaterno":"'.cv($fDatosJuez["Paterno"]).'",
												  "apMaterno":"'.cv($fDatosJuez["Materno"]).'",
												  "nombre": "'.cv($fDatosJuez["Nom"]).'"
												},
					  "imputados":	[
									  '.$arrImputados.'
									],
								  
					  "victimas":	[
									 '.$arrVictimas.'
								  ] ,
					  "defensores":	[
									  '.$arrDefensores.'
								  ] ,
								  
					  "asesores":	[
									'.$arrAsesores.'  
								  ] ,                
								  
					  "representatesLegales":	[
												'.$arrRepresentantes.'
												] ,
					
					
					  "testigos":	[
									  '.$arrTestigos.'
								 ]  
					
				  }   ';
		
		return $cadJSON;
		
	}
	
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('obtenerMediacionesEvento',array('idEvento'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);