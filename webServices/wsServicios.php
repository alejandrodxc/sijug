<?php session_start();
	
	include("latis/conexionBD.php");
	include_once("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	
	
	include_once("latis/sgjp/siajop.php");
	ini_set('default_socket_timeout', 160000);
	ini_set('post_max_size', '1024M');
	ini_set('upload_max_filesize', '1024M');

	
	function consultarAudiencias($materia,$idJuzgado,$noProceso,$periodoInicio,$periodoFin)
	{
		
		global $con;
		global $tipoMateria;
		
		if($periodoInicio=="")
			$periodoInicio="1900-01-01";
		if($periodoFin=="")
			$periodoFin="4000-12-31";
			
		if($materia=="")
			$materia="1,2,3,4" ;
		
		$consulta="";
		$arrEventos=array();
		$arrTipoMateria=array();
		
		$arrTipoMateria[1]["sistema"]="SGJP";
		$arrTipoMateria[1]["tipoMateria"]="P";
		$arrTipoMateria[2]["sistema"]="SGJC";
		$arrTipoMateria[2]["tipoMateria"]="C";
		$arrTipoMateria[3]["sistema"]="SGJC";
		$arrTipoMateria[3]["tipoMateria"]="C";
		$arrTipoMateria[4]["sistema"]="SGJF";	
		$arrTipoMateria[4]["tipoMateria"]="F";	
		
		$arrMateria=explode(",",$materia);
		foreach($arrMateria as $idMateria)
		{
			$e=array();
			if($arrTipoMateria[$idMateria]["tipoMateria"]==$tipoMateria)
			{
				$comp="";
				
				if($noProceso!="")
				{
					$comp=" and c.carpetaAdministrativa='".$noProceso."'";
				}
				
				if($idJuzgado!="")
				{
					
					$comp.=" and e.idCentroGestion=".$idJuzgado;
				}
				
				if($tipoMateria=="P")
				{
					$consulta="SELECT  fechaEvento AS fechaAudiencia,horaInicioEvento AS hora,
							c.carpetaAdministrativa AS proceso,(SELECT GROUP_CONCAT(d.denominacionDelito) FROM _61_tablaDinamica dl,_35_denominacionDelito d 
							WHERE idActividad=c.idActividad AND d.id__35_denominacionDelito=dl.denominacionDelito) AS delitoJuicio,
							(SELECT tipoAudiencia FROM _4_tablaDinamica WHERE id__4_tablaDinamica=e.tipoAudiencia) AS tipoAudiencia,
							j.nombreUnidad AS juzgado,'Penal' AS materia, (SELECT direccion FROM _1_tablaDinamica WHERE id__1_tablaDinamica=e.idEdificio) AS ubicacion
							 FROM 7000_eventosAudiencia e,7007_contenidosCarpetaAdministrativa con,7006_carpetasAdministrativas c,_17_tablaDinamica j							
							WHERE e.fechaEvento>='".$periodoInicio."' AND e.fechaEvento<='".$periodoFin."' and e.situacion in(1,2,4,5) and 
							con.tipoContenido=3 AND con.idRegistroContenidoReferencia=e.idRegistroEvento ".$comp."
							AND c.carpetaAdministrativa=con.carpetaAdministrativa AND j.claveUnidad=c.unidadGestion LIMIT 0,10";
				}
				else
				{
					$materia="";
					if($tipoMateria=="C")
					{
						$materia="if(s.tipoJuicio='35','Civil',if(s.tipoJuicio='36','Mercantil',if(s.tipoJuicio='38','Mercantil','Indefinido')))";
						if($idMateria!="")
						{
							switch($idMateria)
							{
								case 2:
									$comp.=" and s.tipoJuicio=35";
								break;
								case 3:
									$comp.=" and (s.tipoJuicio=36 or s.tipoJuicio=38)";
								break;
							}
						}
					}
					else
					{
						$materia="'Familiar'";
					}
					
					
					
					$consulta="SELECT  fechaEvento AS fechaAudiencia,horaInicioEvento AS hora,
							c.carpetaAdministrativa AS proceso,(SELECT tipoJuicio FROM _477_tablaDinamica WHERE id__477_tablaDinamica=s.tipoJuicio) AS delitoJuicio,
							(SELECT tipoAudiencia FROM _4_tablaDinamica WHERE id__4_tablaDinamica=e.tipoAudiencia) AS tipoAudiencia,
							j.nombreUnidad AS juzgado,".$materia." AS materia, (SELECT direccion FROM _1_tablaDinamica WHERE id__1_tablaDinamica=e.idEdificio) AS ubicacion
							 FROM 7000_eventosAudiencia e,7007_contenidosCarpetaAdministrativa con,7006_carpetasAdministrativas c,_17_tablaDinamica j,_478_tablaDinamica s							
							WHERE e.fechaEvento>='".$periodoInicio."' AND e.fechaEvento<='".$periodoFin."' and e.situacion in(1,2,4,5) and 
							con.tipoContenido=3 AND con.idRegistroContenidoReferencia=e.idRegistroEvento ".$comp."
							AND c.idCarpeta=con.idCarpetaAdministrativa AND j.claveUnidad=c.unidadGestion and s.id__478_tablaDinamica=c.idRegistro LIMIT 0,10";
				}
				
				$res=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_assoc($res))
				{
					$fechaEvento=date("d/m/Y",strtotime($fila["fechaAudiencia"]));
					$e=		'<audiencia>'. 
								'<fechaAudiencia>'.$fechaEvento.'</fechaAudiencia>'.
								'<hora>'.date("H:i",strtotime($fila["hora"])).'</hora>'.
								'<proceso>'.$fila["proceso"].'</proceso>'.
								'<delitoJuicio>'.($fila["delitoJuicio"]).'</delitoJuicio>'.
								'<tipoAudiencia>'.($fila["tipoAudiencia"]).'</tipoAudiencia>'.
								'<juzgado>'.($fila["juzgado"]).'</juzgado>'.
								'<ubicacion>'.($fila["ubicacion"]).'</ubicacion>'.
								'<materia>'.$fila["materia"].'</materia>'.
							'</audiencia>';
					
					if(!isset($arrEventos[$fechaEvento]))
						$arrEventos[$fechaEvento]=array();
						
					array_push($arrEventos[$fechaEvento],$e);
					
				}
				
				
			}
			else
			{
				$consulta="SELECT CONCAT(direccionIP,':',puerto) FROM 000_instanciasSistema WHERE nombreSistema='".$arrTipoMateria[$idMateria]["sistema"]."'";
				$direccion=$con->obtenerValor($consulta);
				$client = new nusoap_client("http://".$direccion."/webServices/wsServicios.php?wsdl","wsdl");
				$parametros=array();
				$parametros["idMateria"]=$idMateria;
				$parametros["idJuzgado"]=$idJuzgado;
				$parametros["noProceso"]=$noProceso;
				$parametros["periodoInicio"]=$periodoInicio;
				$parametros["periodoFin"]=$periodoFin;
				
				$docXML = $client->call("consultarAudiencias", $parametros);
				
				
				$cXML=simplexml_load_string($docXML);
				
				foreach($cXML->audiencia as $audiencia)
				{
					$fechaEvento=(string)$audiencia->fechaAudiencia;
					$e=		'<audiencia>'. 
								'<fechaAudiencia>'.$fechaEvento.'</fechaAudiencia>'.
								'<hora>'.date("H:i",strtotime((string)$audiencia->hora)).'</hora>'.
								'<proceso>'.((string)$audiencia->proceso).'</proceso>'.
								'<delitoJuicio>'.((string)$audiencia->delitoJuicio).'</delitoJuicio>'.
								'<tipoAudiencia>'.((string)$audiencia->tipoAudiencia).'</tipoAudiencia>'.
								'<juzgado>'.((string)$audiencia->juzgado).'</juzgado>'.
								'<ubicacion>'.((string)$audiencia->ubicacion).'</ubicacion>'.
								'<materia>'.((string)$audiencia->materia).'</materia>'.
							'</audiencia>';
					
					if(!isset($arrEventos[$fechaEvento]))
						$arrEventos[$fechaEvento]=array();
						
					array_push($arrEventos[$fechaEvento],$e);
				}
			}
			
			
		}
		$arrAudiencias="";
		ksort($arrEventos);
		foreach($arrEventos as $fecha=>$resto)
		{
			foreach($resto as $audiencia)
				$arrAudiencias.=$audiencia;
		}
		
		$arrAudiencias='<?xml version="1.0" encoding="ISO-8859-1"?><datosAudiencias>'.$arrAudiencias."</datosAudiencias>";
		/*
		1 Penal
		2 Civil
		3 Mercantil
		4 Familiar
		
		*/
		
		return $arrAudiencias;
	}
	
	function obtenerCatalogoJuzgados($idMateria)
	{
		global $con;
		
		global $con;
		global $tipoMateria;
		
		
		
		$consulta="";
		$arrJuzgados=array();
		$arrTipoMateria=array();
		
		$arrTipoMateria[1]["sistema"]="SGJP";
		$arrTipoMateria[1]["tipoMateria"]="P";
		$arrTipoMateria[2]["sistema"]="SGJC";
		$arrTipoMateria[2]["tipoMateria"]="C";
		$arrTipoMateria[3]["sistema"]="SGJC";
		$arrTipoMateria[3]["tipoMateria"]="C";
		$arrTipoMateria[4]["sistema"]="SGJF";	
		$arrTipoMateria[4]["tipoMateria"]="F";	
		
		
		
		$e=array();
		if($arrTipoMateria[$idMateria]["tipoMateria"]==$tipoMateria)
		{
			
			if($tipoMateria=="P")
			{
				$consulta="SELECT claveUnidad,upper(nombreUnidad) as nombreUnidad FROM _17_tablaDinamica WHERE cmbCategoria=1 ORDER BY prioridad";
			}
			else
			{
				$materia="";
				if($tipoMateria=="C")
				{
					switch($idMateria)
					{
						case 2:
							$consulta="SELECT claveUnidad,UPPER(nombreUnidad) as nombreUnidad FROM _17_tablaDinamica j,_17_gridDelitosAtiende d WHERE cmbCategoria=1
										AND d.idReferencia=id__17_tablaDinamica AND tipoDelito='MC' ORDER BY prioridad";
						break;
						case 3:
							$consulta="SELECT claveUnidad,UPPER(nombreUnidad) as nombreUnidad FROM _17_tablaDinamica j,_17_gridDelitosAtiende d WHERE cmbCategoria=1
										AND d.idReferencia=id__17_tablaDinamica AND (tipoDelito='MC' or tipoDelito='ME') ORDER BY prioridad";
						break;
					}
				}
				else
				{
					$consulta="SELECT claveUnidad,upper(nombreUnidad) as nombreUnidad FROM _17_tablaDinamica WHERE cmbCategoria=1 ORDER BY prioridad";
				}
			}
			
			$res=$con->obtenerFilas($consulta);
			while($fila=mysql_fetch_assoc($res))
			{
				
				$e=		'<juzgado>'. 
							'<cveJuzgado>'.$fila["claveUnidad"].'</cveJuzgado>'.
							'<nombreJuzgado>'.$fila["nombreUnidad"].'</nombreJuzgado>'.
						'</juzgado>';
				
				array_push($arrJuzgados,$e);
				
			}
			
			
		}
		else
		{
			$consulta="SELECT CONCAT(direccionIP,':',puerto) FROM 000_instanciasSistema WHERE nombreSistema='".$arrTipoMateria[$idMateria]["sistema"]."'";
			$direccion=$con->obtenerValor($consulta);
			$client = new nusoap_client("http://".$direccion."/webServices/wsServicios.php?wsdl","wsdl");
			$parametros=array();
			$parametros["idMateria"]=$idMateria;
			$docXML = $client->call("obtenerCatalogoJuzgados", $parametros);
			
			
			$cXML=simplexml_load_string($docXML);
			
			foreach($cXML->juzgado as $j)
			{
				
				$e=		'<juzgado>'. 
							'<cveJuzgado>'.((string)$j->cveJuzgado).'</cveJuzgado>'.
							'<nombreJuzgado>'.((string)$j->nombreJuzgado).'</nombreJuzgado>'.
						'</juzgado>';
				
				
					
				array_push($arrJuzgados,$e);
			}
		}
			
			
		
		$aJuzgados="";

		foreach($arrJuzgados as $juzgado)
		{
			$aJuzgados.=$juzgado;
		}
		
		$arrJuzgados='<?xml version="1.0" encoding="ISO-8859-1"?><datosJuzgados>'.$aJuzgados."</datosJuzgados>";
		/*
		1 Penal
		2 Civil
		3 Mercantil
		4 Familiar
		
		*/
		
		return $arrJuzgados;
	}


	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('consultarAudiencias',array('idMateria'=>'xsd:string','idJuzgado'=>'xsd:string','noProceso'=>'xsd:string','periodoInicio'=>'xsd:string','periodoFin'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	$server->register('obtenerCatalogoJuzgados',array('idMateria'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','');
	
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	
	
	$server->service($input);
?>