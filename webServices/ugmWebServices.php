<?php session_start();
	include("latis/conexionBD.php");
	include("latis/nusoap/nusoap.php");
	include_once("latis/funcionesNeotrai.php");
	ini_set('default_socket_timeout', 160000);
	
	function registrarAsistencia($objAsistencia)
	{
		$obj=json_decode($objAsistencia);
		$cadResp=validarHorarioAsistenciaProfesor($obj->hora,$obj->idUsuario,$obj->fecha,$obj->plantel);//1 Entrada,-1 Usuario no existe,2 Sin permisos para terminal, 3 Entrada con retardo,4 Salida anticipada, 5 Salida, 6 No materia para asistencia, 
																											//7 Tiempo limite excedido para asistencia, 8  Terminal no autorizada para regsitrar asistencia; 9 Terminal no autoizada para registrar asistencia d emateria plan estudio
		$arrDatos=explode("|",$cadResp);
		$obj=json_decode($arrDatos[1]);
		$cadResultado=$obj->resultado;
		if(isset($obj->nomMateria))
			$cadResultado.="|".$obj->nomMateria;
		if(isset($obj->tRetardo))
			$cadResultado.="|".$obj->tRetardo;
		return $cadResultado;
	}
	
	function registrarEvento($objAsistencia)
	{
		global $con;
		$obj=json_decode($objAsistencia);
		$consulta="INSERT INTO 9105_eventosRecibidos(idUsuario,fechaEvento,horaEvento,noTerminal,plantel,marcaSincronizacion) values(".$obj->idUsuario.",'".$obj->fecha."','".$obj->hora."',".$obj->noTerminal.",'".$obj->plantel."','".$obj->marcaSincronizacion."')";
		if($con->ejecutarConsulta($consulta))
			return 1;
		else
			return 0;
	}
	
	function procesarEventos($horaMarca)
	{
		global $con;
		if(procesarEventosBiometricos($horaMarca,true))
		{
			$marcaOriginal=$horaMarca;
			$horaMarca.=":00";
			$hMarca=strtotime($horaMarca);
			if($hMarca>=strtotime(date("Y-m-d",$hMarca)." 23:00:00"))
			{
				$consulta="SELECT DISTINCT plantel FROM 9105_eventosControlAsistencia WHERE (marcaProcesamiento='".$horaMarca."' or marcaProcesamiento='".$marcaOriginal."')";
				$res=$con->obtenerFilas($consulta);
				while($fila=mysql_fetch_row($res))
				{
					$consulta="SELECT DISTINCT fechaEvento FROM 9105_eventosControlAsistencia WHERE (marcaProcesamiento='".$horaMarca."' or marcaProcesamiento='".$marcaOriginal."') and plantel='".$fila[0]."'";
					$resFecha=$con->obtenerFilas($consulta);
					while($filaFecha=mysql_fetch_row($resFecha))
					{
						
						if(reprocesarEventosPlantelPeriodo($fila[0],$filaFecha[0],$filaFecha[0],true))
						{
							$consulta="INSERT INTO 9105_resincronizacionEventos(plantel,marcaReprocesamiento,fechaReprocesamiento) VALUES('".$fila[0]."','".$marcaOriginal."','".$filaFecha[0]."')";
							$con->ejecutarConsulta($consulta);	
						}
					}
				}
				
			}
			return 1;
		}
		else
			return 0;
	}
	
	function existenNuevosUsuariosPlantel($plantel)
	{
		global $con;
		$consulta="SELECT u.idUsuario,Nombre FROM 800_usuarios u,801_adscripcion a WHERE u.idUsuario=a.idUsuario AND a.Institucion='".$plantel."' AND u.registradoEnPlantel=0";
		$res=$con->obtenerFilas($consulta);
		$cadRes="";
		while($fila=mysql_fetch_row($res))
		{
			if($cadRes=="")
				$cadRes=$fila[0]."_".substr($fila[1],0,20);
			else
				$cadRes.="|".$fila[0]."_".substr($fila[1],0,20);
				
		}
		return $cadRes;
	}
	
	function marcarAltaUsuariosPlantel($listUsuarios)
	{
		global $con;
		if($listUsuarios=="")
			$listUsuarios="-1";
		$consulta="UPDATE 800_usuarios SET registradoEnPlantel=1 WHERE idUsuario IN (".$listUsuarios.")";
		if($con->ejecutarConsulta($consulta))
			return "1";
		return 0;
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('registrarAsistencia',array('objAsistencia'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Registro de asistencia docente');
	$server->register('registrarEvento',array('objAsistencia'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Registro de evento docente');
	$server->register('procesarEventos',array('horaMarca'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Procesar eventos biométricos');
	$server->register('existenNuevosUsuariosPlantel',array('plantel'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Obtener usuarios nuevos del plantel');
	$server->register('marcarAltaUsuariosPlantel',array('listUsuarios'=>'xsd:string'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Marcar usuarios nuevos dados de alta en plantel');
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	$server->service($input);
?>