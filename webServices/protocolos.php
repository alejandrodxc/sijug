<?php
	include("latis/conexionBD.php");
	include("latis/nusoap/nusoap.php");
	ini_set('default_socket_timeout', 160000);
	//$soapclient = new nusoap_client('http://localhost/latis/webServices/prueba.php?wsdl');

	function getListadoProtocolos()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaProtocolos>";
		$consulta="SELECT  id__278_tablaDinamica,tituloProyecto,codigo,
					(SELECT idUsuario FROM 246_autoresVSProyecto a WHERE idFormulario=278 AND 
					idReferencia=t.id__278_tablaDinamica AND responsable=1) AS idInventigadorPrincipal,idEstado,codigoUnidad FROM _278_tablaDinamica t order by id__278_tablaDinamica desc limit 0,300";
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			$consulta="select idOrganigrama,unidad from 817_organigrama where codigoUnidad='".$fila[5]."'";
			
			$filaUnidad=$con->obtenerPrimeraFila($consulta);
			$obj='<protocolo>'.
					'<idRegistro>'.$fila[0].'</idRegistro>'.
					'<tituloProtocolo><![CDATA['.$fila[1].']]></tituloProtocolo>'.
					'<folio>'.$fila[2].'</folio>'.
					'<idInventigadorPrincipal>'.$fila[3].'</idInventigadorPrincipal>'.
					'<estadoProtocolo>'.$fila[4].'</estadoProtocolo>'.
					'<idDepartamento>'.$filaUnidad[0].'</idDepartamento>'.
					'<departamento>'.$filaUnidad[1].'</departamento>'.
				'</protocolo>';
			$cad.=$obj;
		}
		$cad.="</listaProtocolos>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getInformacionProtocolo($idProtocolo)
	{
		global $con;
		$consulta="select * from _278_tablaDinamica where id__278_tablaDinamica=".$idProtocolo;
		$fila=$con->obtenerPrimeraFila($consulta);
		$fechaInicio="";
		if($fila[17]!="")
			$fechaInicio=date("d/m/Y",strtotime($fila[17]));
		$fechaTermino="";
		if($fila[18]!="")
			$fechaTermino=date("d/m/Y",strtotime($fila[18]));
		
		$cadGenerales="";
		$consulta="SELECT txtObjetivoGral FROM _259_tablaDinamica WHERE idReferencia=".$idProtocolo."  AND tipObjetivo=1";
		$resGeneral=$con->obtenerFilas($consulta);
		while($fGral=mysql_fetch_row($resGeneral))
		{
			$cadGenerales.="<objetivo><![CDATA[".strip_tags($fGral[0])."]]></objetivo>";
		}
		$cadEspecificos="";
		$consulta="SELECT txtObjetivoGral FROM _259_tablaDinamica WHERE idReferencia=".$idProtocolo."  AND tipObjetivo=2";
		$resGeneral=$con->obtenerFilas($consulta);
		while($fGral=mysql_fetch_row($resGeneral))
		{
			$cadEspecificos.="<objetivo><![CDATA[".strip_tags($fGral[0])."]]></objetivo>";
		}
		$consulta="SELECT metcriterioseleccion FROM _261_tablaDinamica WHERE idReferencia=".$idProtocolo;
		$criterios=strip_tags($con->obtenerValor($consulta));
		$patrocinadores="";
		
		$consulta="SELECT o.idOrganigrama,o.unidad,montoPatrocinio FROM 9036_patrocinadoresProyectos p,817_organigrama o 
					WHERE idFormulario=278 AND idReferencia=".$idProtocolo." AND o.codigoUnidad=p.codigoInstitucion";
		$resPatrocinadores=$con->obtenerFilas($consulta);
		while($fPatrocinio=mysql_fetch_row($resPatrocinadores))
		{
			$patrocinadores.=	'<patrocinador><idPatrocinador>'.$fPatrocinio[0].'</idPatrocinador><nombrePatrocinador><![CDATA['.$fPatrocinio[1].']]></nombrePatrocinador>'.
								'<montoPatrocinio>'.$fPatrocinio[2].'</montoPatrocinio></patrocinador>';
		}
		
		
		$consulta="SELECT fechaCambio FROM 941_bitacoraEtapasFormularios WHERE idFormulario=278 AND idRegistro=".$idProtocolo." AND etapaActual=6.1";
		$fechaAprobacionComite=$con->obtenerValor($consulta);
		if($fechaAprobacionComite!="")
			$fechaAprobacionComite=date("d/m/Y",strtotime($fechaAprobacionComite));
		$fechaInicioDesarrollo="";
		$consulta="SELECT fechaCambio FROM 941_bitacoraEtapasFormularios WHERE idFormulario=278 AND idRegistro=".$idProtocolo." AND etapaActual=7.1";
		$fechaInicioDesarrollo=$con->obtenerValor($consulta);
		if($fechaInicioDesarrollo!="")
			$fechaInicioDesarrollo=date("d/m/Y",strtotime($fechaInicioDesarrollo));
		$tipoApoyo=2;
		$consulta="SELECT COUNT(*) FROM 100_gridPresupuesto WHERE idFormulario=278 AND idReferencia=".$idProtocolo." AND idRubro=11";
		$nApoyo=$con->obtenerValor($consulta);
		if($nApoyo>0)
			$tipoApoyo=1;
		
		$investigadores="";
		$consulta="select * from 246_autoresVSProyecto where idFormulario=278 and idReferencia=".$idProtocolo;
		$resInvestigadores=$con->obtenerFilas($consulta);
		$nInvestigadores=$con->filasAfectadas;
		while($fInvestigador=mysql_fetch_row($resInvestigadores))
		{
			$consulta="select Nombre from 800_usuarios where idUsuario=".$fInvestigador[1];
			$nomInvestigador=$con->obtenerValor($consulta);
			$investigadores.='<investigador>'.
									'<idInvestigador>'.$fInvestigador[1].'</idInvestigador>'.
									'<nombre>'.$nomInvestigador.'</nombre>'.
									'<statusInvestigador>1</statusInvestigador>'.			
									'<idMotivoBaja></idMotivoBaja>'.
							'</investigador>';
		}
		
		$cad='<?xml version="1.0" encoding="UTF-8"?><protocolo>'.
				'<idProtocolo>'.$fila[0].'</idProtocolo>'.
				'<objetivoProtocolo>'.
					'<objetivosGenerales>'.$cadGenerales.'</objetivosGenerales>'.
					'<objetivosEspecificos>'.$cadEspecificos.'</objetivosEspecificos>'.
				'</objetivoProtocolo>'.
				'<tituloProtocolo><![CDATA['.$fila[10].']]></tituloProtocolo>'.
				'<fechaAprobacionComite>'.$fechaAprobacionComite.'</fechaAprobacionComite>'.
				'<fechaInicioDesarrollo>'.$fechaInicioDesarrollo.'</fechaInicioDesarrollo>'.
				'<criteriosExclusionInclusion><![CDATA['.$criterios.']]></criteriosExclusionInclusion>'.
				'<fechaInicioProtocolo>'.$fechaInicio.'</fechaInicioProtocolo>'.
				'<fechaTermino>'.$fechaTermino.'</fechaTermino>'.
				'<patrocinadores>'.$patrocinadores.'</patrocinadores>'.
				'<tipoApoyo>'.$tipoApoyo.'</tipoApoyo>'.
				'<nivelSocioEconomico>6</nivelSocioEconomico>'.
				'<noInvestigadores>'.$nInvestigadores.'</noInvestigadores>'.
				'<investigadores>'.$investigadores.'</investigadores>'.
				'<folioProtocolo>'.$fila[9].'</folioProtocolo>'.
				'<estadoProtocolo>'.$fila[6].'</estadoProtocolo>'.
			'</protocolo>';
		return new soapval('return', 'xsd:string', $cad);
	}
	
	function getListadoPatrocinadores()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaPatrocinadores>";
		$consulta="SELECT DISTINCT codigoInstitucion FROM 9036_patrocinadoresProyectos";
		$listPatrocinadores=$con->obtenerListaValores($consulta,"'");
		if($listPatrocinadores=="")
			$listPatrocinadores="-1";
		$consulta="select idOrganigrama,unidad from 817_organigrama where codigoUnidad in (".$listPatrocinadores.")";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='<patrocinador>'.
					'<idPatrocinador>'.$fila[0].'</idPatrocinador>'.
					'<nombrePatrocinador>'.$fila[1].'</nombrePatrocinador>'.
				'</patrocinador>';
			$cad.=$obj;
		}
		$cad.="</listaPatrocinadores>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getListadoEtapas()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaEtapas>";
		$consulta="SELECT numEtapa,nombreEtapa,situacion,descripcion FROM 4037_etapas WHERE idProceso=129 order by numEtapa";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='<etapa>'.
					'<idEtapa>'.$fila[0].'</idEtapa>'.
					'<nombreEtapa>'.$fila[1].'</nombreEtapa>'.
					'<activo>'.$fila[2].'</activo>'.
					'<descripcion>'.$fila[3].'</descripcion>'.
				'</etapa>';
			$cad.=$obj;
		}
		$cad.="</listaEtapas>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getListadoMotivosBaja()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaMotivosBaja>";
		

		$consulta="SELECT id__936_bajaInvestigadoresGrid,motivo FROM _936_bajaInvestigadoresGrid";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='<motivoBaja>'.
					'<idMotivo>'.$fila[0].'</idMotivo>'.
					'<descripcionMotivo>'.$fila[1].'</descripcionMotivo>'.
				'</motivoBaja>';
			$cad.=$obj;
		}
		$cad.="</listaMotivosBaja>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getListadoTiposApoyo()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaTiposApoyo>";
		$consulta="SELECT id__937_presupuestoParaPacientesGrid,manejaPresupuesto FROM _937_presupuestoParaPacientesGrid";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='<tipoApoyo>'.
					'<idTipoApoyo>'.$fila[0].'</idTipoApoyo>'.
					'<descripcionTipoApoyo>'.$fila[1].'</descripcionTipoApoyo>'.
				'</tipoApoyo>';
			$cad.=$obj;
		}
		$cad.="</listaTiposApoyo>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getListadoBajasProtocolo()
	{
		global $con;
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><listaBajasProtocolo>";
		

		$consulta="SELECT id__938_etapasBajaProcotolo,e.nombreEtapa FROM  _938_etapasBajaProcotolo b,4037_etapas e WHERE e.idEtapa=b.etapa";	
		$res=$con->obtenerFilas($consulta);
		while($fila=mysql_fetch_row($res))
		{
			
			$obj='<bajaProtocolo>'.
					'<idBajaProtocolo>'.$fila[0].'</idBajaProtocolo>'.
					'<descripcionBajaProtocolo>'.$fila[1].'</descripcionBajaProtocolo>'.
				'</bajaProtocolo>';
			$cad.=$obj;
		}
		$cad.="</listaBajasProtocolo>";
		return new soapval('return', 'xsd:string', $cad);

	}
	
	function getPresupuestoProtocolo($idProtocolo)
	{
		global $con;
		$consulta="SELECT idGridVSCalculo,concepto,total,total FROM 100_gridPresupuesto WHERE idFormulario=278 AND idReferencia=".$idProtocolo." AND idRubro=11";
		$resProtocolo=$con->obtenerFilas($consulta);
		$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><presupuestoProtocolo>";
		$cad.='	<idProtocolo>'.$idProtocolo.'</idProtocolo>'.
						'<apoyosPaciente>';
		while($filaProtocolo=mysql_fetch_row($resProtocolo))
		{
			$cad.='	<apoyo>'.
						'<idApoyo>'.$filaProtocolo[0].'</idApoyo>'.
						'<apoyoDescripcion>'.$filaProtocolo[1].'</apoyoDescripcion>'.
						'<montoAutorizado>'.$filaProtocolo[2].'</montoAutorizado>'.
						'<saldo>'.$filaProtocolo[2].'</saldo>'.
        			'</apoyo>';
		
		}
        $cad.='</apoyosPaciente>'.
        	"</presupuestoProtocolo>";
		return new soapval('return', 'xsd:string', $cad);
	}
	
	function getInformacionBajaProtocolo($idProtocolo)
	{
		global $con;
		$cad="";
		$consulta="SELECT numEtapa FROM _938_etapasBajaProcotolo e,4037_etapas et WHERE et.idEtapa=e.etapa";
		$listEtapas=$con->obtenerListaValores($consulta);
		if($listEtapas=="")
			$listEtapas="-1";
		$consulta="SELECT comentarios,fechaCambio,etapaActual FROM 941_bitacoraEtapasFormularios WHERE idFormulario=278 AND 
					idRegistro=".$idProtocolo." AND etapaActual in (".$listEtapas.") order by fechaCambio";
		$fila=$con->obtenerPrimeraFila($consulta);
		if($fila)
		{
			$consulta="SELECT ep.id__938_etapasBajaProcotolo FROM 4037_etapas e,_938_etapasBajaProcotolo ep 
						WHERE idProceso=129 and numEtapa=".$fila[2]." and ep.etapa=e.idEtapa";
			$idEtapa=$con->obtenerValor($consulta);	
			$cad=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?><datosBajaProtocolo>".
						"<idProtocolo>".$idProtocolo."</idProtocolo>".
						"<idBaja>".$idEtapa."</idBaja>".
						"<fechaBaja>".date("d/m/Y",strtotime($fila[1]))."</fechaBaja>".
						"<observaciones><![CDATA[".$fila[0]."]]></observaciones>".
					"</datosBajaProtocolo>";
		}
		else
		{
			$cad="<?xml version=\"1.0\" encoding=\"UTF-8\"?><datosBajaProtocolo><idProtocolo></idProtocolo><idBaja></idBaja><fechaBaja></fechaBaja><observaciones></observaciones></datosBajaProtocolo>";
		}
		return new soapval('return', 'xsd:string', $cad);
	}
	
	$arrParam=array();
	$server = new soap_server;
	$ns=$urlSitio."/webServices";
	$server->configurewsdl('ApplicationServices',$ns);
	$server->wsdl->schematargetnamespace=$ns;
	$server->register('getListadoProtocolos',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Listado de protocolos de investigación');
	$server->register('getInformacionProtocolo',array('idProtocolo'=>'xsd:int'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Información referente a un protocolo de investigación');
	$server->register('getListadoPatrocinadores',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Catálogo de patrocinadores');
	$server->register('getListadoEtapas',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Catálogo de etapas');
	$server->register('getListadoMotivosBaja',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Catálogo de motivos de baja de investigador');
	$server->register('getListadoTiposApoyo',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Catálogo de tipos de apoyo');
	$server->register('getListadoBajasProtocolo',$arrParam,array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Catálogo de tipos de baja de protocolos');
	$server->register('getPresupuestoProtocolo',array('idProtocolo'=>'xsd:int'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Información referente al presupuesto del protocolo en el ámbito de apoyo al paciente');
	$server->register('getInformacionBajaProtocolo',array('idProtocolo'=>'xsd:int'),array('return' => 'xsd:string'),$ns,false,'rpc','encoded','Información referente a la baja de un protocolo');
	if (isset($HTTP_RAW_POST_DATA)) 
	{
		$input = $HTTP_RAW_POST_DATA;
	}
	else 
	{
		$input = implode("rn", file('php://input'));
	}
	$server->service($input);
?>